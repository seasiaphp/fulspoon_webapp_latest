function getElementValue(e) {
    switch (e.tagName) {
    case "SELECT":
        var t = e.options[e.selectedIndex].value;
        return 1 == t.indexOf(";") ? t.substr(2) : t;
    case "INPUT":
        return e.value
    }
    return void 0
}
function DisplayDialog(e) {
    return new Promise(function(t) {
        var r = document.createElement("div");
        r.className = "dialog-overlay",
        e.classList.remove("dialog-content");
        var i = document.createElement("div");
        i.className = "dialog",
        i.appendChild(e),
        document.body.appendChild(r),
        document.body.appendChild(i),
        r.addEventListener("click", function() {
            document.body.removeChild(i),
            document.body.removeChild(r),
            t()
        })
    }
    )
}
function RequirementsBase(e) {
    this.requirementsElement_ = e,
    this.requirements_ = {},
    this.satisfied_ = {}
}
function GeneratorBase(e, t) {
    RequirementsBase.call(this, e),
    this.element_ = t,
    this.fields_ = {},
    this.serialized_state_ = {}
}
function NotificationGeneratorBase(e, t, r) {
    GeneratorBase.call(this, e, t),
    this.serviceWorker_ = r,
    this.addRequirement(NotificationGeneratorBase.REQUIREMENT_PERMISSION, "Requires permission to display notifications."),
    this.addRequirement(NotificationGeneratorBase.REQUIREMENT_SERVICE_WORKER, "Requires the Service Worker to be registered.")
}
Array.prototype.hasOwnProperty("includes") || (Array.prototype.includes = function(e) {
    for (var t in this)
        if (this[t] == e)
            return !0;
    return !1
}
),
String.prototype.hasOwnProperty("padRight") || (String.prototype.padRight = function(e, t) {
    if (e <= this.length)
        return this;
    for (var r = this, i = this.length; e > i; ++i)
        r += t;
    return r
}
),
Uint8Array.prototype.hasOwnProperty("slice") || (Uint8Array.prototype.slice = Uint8Array.prototype.subarray),
Uint8Array.prototype.hasOwnProperty("fill") || (Uint8Array.prototype.fill = function(e, t, r) {
    void 0 === t && (t = 0),
    void 0 === r && (r = this.length);
    for (var i = t; r > i; ++i)
        this[i] = e
}
),
RequirementsBase.prototype.addRequirement = function(e, t) {
    this.satisfied_.hasOwnProperty(e) && delete this.satisfied_[e],
    this.requirements_[e] = t,
    this.requirementsChanged()
}
,
RequirementsBase.prototype.satisfyRequirement = function(e) {
    this.requirements_.hasOwnProperty(e) && (this.satisfied_[e] = this.requirements_[e],
    delete this.requirements_[e],
    this.requirementsChanged())
}
,
RequirementsBase.prototype.verifyRequirements = function(e) {
    var t = "";
    for (var r in this.requirements_)
        t += "- " + this.requirements_[r] + "\n";
    return t.length ? (e || alert(t),
    !1) : !0
}
,
RequirementsBase.prototype.resetRequirements = function() {
    for (var e in this.satisfied_)
        this.requirements_[e] = this.satisfied_[e];
    this.satisfied_ = {},
    this.requirementsChanged()
}
,
RequirementsBase.prototype.requirementsChanged = function() {
    var e = [];
    for (var t in this.requirements_)
        e.push(this.requirements_[t]);
    if (!e.length)
        return void (this.requirementsElement_.style.display = "none");
    for (this.requirementsElement_.style.display = "block"; this.requirementsElement_.firstChild; )
        this.requirementsElement_.removeChild(this.requirementsElement_.firstChild);
    for (var r = 0; r < e.length; ++r) {
        var i = document.createElement("li");
        i.textContent = e[r],
        this.requirementsElement_.appendChild(i)
    }
}
,
GeneratorBase.prototype = Object.create(RequirementsBase.prototype),
GeneratorBase.FIELD_TYPE_STRING = 0,
GeneratorBase.FIELD_TYPE_BOOL = 1,
GeneratorBase.FIELD_TYPE_ARRAY = 2,
GeneratorBase.FIELD_TYPE_BUTTONS = 3,
GeneratorBase.FIELD_TYPE_TIME_OFFSET = 4,
GeneratorBase.SEPARATOR_FIELD = ";;",
GeneratorBase.SEPARATOR_VALUE = "=",
GeneratorBase.SEPARATOR_MULTI_VALUE = "$$",
GeneratorBase.prototype.serialize = function(e) {
    var t = [];
    return Object.keys(e).forEach(function(r) {
        var i = void 0 !== e[r].index ? e[r].index : e[r].value;
        t.push(r + GeneratorBase.SEPARATOR_VALUE + i)
    }),
    t.join(GeneratorBase.SEPARATOR_FIELD)
}
,
GeneratorBase.prototype.deserialize = function(e) {
    if (e.startsWith("#")) {
        e = e.substr(1);
        var t = e.split(GeneratorBase.SEPARATOR_FIELD)
          , r = this;
        t.forEach(function(e) {
            var t = e.indexOf(GeneratorBase.SEPARATOR_VALUE);
            -1 != t && (r.serialized_state_[e.substr(0, t)] = e.substr(t + 1))
        })
    }
}
,
GeneratorBase.prototype.setFields = function(e) {
    var t = this;
    Object.keys(e).forEach(function(r) {
        var i = e[r];
        t.fields_[r] = {
            element: t.element_.querySelector("#" + i[0]),
            elementCustom: t.element_.querySelector("#" + i[0] + "_custom"),
            type: i[1]
        },
        t.initializeField(r)
    })
}
,
GeneratorBase.prototype.initializeField = function(e) {
    var t = this.fields_[e];
    t.defaultValue = "",
    "SELECT" == t.element.tagName ? t.defaultValue = t.element.options[t.element.selectedIndex].getAttribute("data-id") : "checkbox" == t.element.type && (t.defaultValue = t.element.checked),
    t.element.addEventListener("change", function() {
        t.elementCustom && ("custom" == getElementValue(t.element) ? t.elementCustom.style.display = "initial" : t.elementCustom.style.display = "none")
    });
    var r = !1;
    if (this.serialized_state_.hasOwnProperty(e)) {
        var i = this.serialized_state_[e];
        switch (t.element.tagName) {
        case "INPUT":
            "checkbox" == t.element.type ? t.element.checked = "true" === i || "1" === i : t.element.value = i;
            break;
        case "SELECT":
            (option = t.element.querySelector('[data-id="' + i + '"]')) ? t.element.selectedIndex = option.index : t.elementCustom && ((option = t.element.querySelector("[data-custom]")) && (t.element.selectedIndex = option.index),
            t.elementCustom.value = i,
            r = !0)
        }
    }
    t.elementCustom && !r && (t.elementCustom.style.display = "none")
}
,
GeneratorBase.prototype.getField = function(e, t, r) {
    if (!e.hasOwnProperty(t))
        return r;
    switch (e[t].type) {
    case GeneratorBase.FIELD_TYPE_ARRAY:
        if (!e[t].value.length)
            return r;
        var i = [];
        return e[t].value.split(",").forEach(function(e) {
            i.push(parseInt(e, 10))
        }),
        i;
    case GeneratorBase.FIELD_TYPE_BUTTONS:
        if (!e[t].value.length)
            return r;
        for (var n = e[t].value.split(GeneratorBase.SEPARATOR_FIELD), s = [], a = 0; a < n.length; ++a) {
            var o = n[a].split(GeneratorBase.SEPARATOR_MULTI_VALUE)
              , l = {
                action: a,
                title: o[0]
            };
            o.length > 1 && (l.icon = o[1]),
            o.length > 2 && (l.type = o[2]),
            o.length > 3 && (l.placeholder = o[3]),
            s.push(l)
        }
        return s;
    case GeneratorBase.FIELD_TYPE_TIME_OFFSET:
        if (!e[t].value.length)
            return r;
        var u = Date.now()
          , c = parseInt(e[t].value);
        return u + c;
    case GeneratorBase.FIELD_TYPE_BOOL:
        return !!e[t].value;
    case GeneratorBase.FIELD_TYPE_STRING:
        return e[t].value
    }
    return r
}
,
GeneratorBase.prototype.resolveFieldState = function(e) {
    var t = this.fields_[e]
      , r = void 0
      , i = void 0;
    switch (t.element.tagName) {
    case "INPUT":
        i = "checkbox" == t.element.type ? t.element.checked : t.element.value;
        break;
    case "SELECT":
        var n = t.element.options[t.element.selectedIndex];
        n.hasAttribute("data-custom") && t.elementCustom ? i = t.elementCustom.value : (r = n.index,
        i = n.value)
    }
    return {
        index: r,
        value: i,
        type: t.type
    }
}
,
GeneratorBase.prototype.computeState = function(e) {
    var t = this
      , r = {};
    return Object.keys(this.fields_).forEach(function(i) {
        var n = t.fields_[i].defaultValue
          , s = t.resolveFieldState(i);
        ((void 0 === s.index || s.index != n) && s.value != n || e) && (r[i] = s)
    }),
    r
}
,
NotificationGeneratorBase.prototype = Object.create(GeneratorBase.prototype),
NotificationGeneratorBase.REQUIREMENT_PERMISSION = 1e3,
NotificationGeneratorBase.REQUIREMENT_SERVICE_WORKER = 1001,
NotificationGeneratorBase.prototype.registerServiceWorker = function(e) {
    navigator.serviceWorker.register(e + this.serviceWorker_, {
        scope: e
    })["catch"](function(e) {
        console.error("Unable to register the service worker: " + e)
    });
    var t = this;
    return navigator.serviceWorker.ready.then(function(e) {
        return t.satisfyRequirement(NotificationGeneratorBase.REQUIREMENT_SERVICE_WORKER),
        e
    })
}
,
NotificationGeneratorBase.prototype.requestPermission = function() {
    var e = this;
    Notification.requestPermission(function(t) {
        "granted" == t && e.satisfyRequirement(NotificationGeneratorBase.REQUIREMENT_PERMISSION)
    })
}
;
function NotificationGenerator(t, i) {
    NotificationGeneratorBase.call(this, t, i, "notification-generator-sw.js")
}
NotificationGenerator.prototype = Object.create(NotificationGeneratorBase.prototype),
NotificationGenerator.prototype.createNotificationOptions = function(t) {
    return {
        dir: this.getField(t, "dir", "auto"),
        body: this.getField(t, "body", ""),
        tag: this.getField(t, "tag", ""),
        image: this.getField(t, "image", void 0),
        icon: this.getField(t, "icon", void 0),
        badge: this.getField(t, "badge", void 0),
        vibrate: this.getField(t, "vibrate", void 0),
        timestamp: this.getField(t, "timestamp", void 0),
        renotify: this.getField(t, "renotify", !1),
        actions: this.getField(t, "actions", void 0),
        silent: this.getField(t, "silent", !1),
        persistent: this.getField(t, "persistent", !0),
        requireInteraction: this.getField(t, "requireInteraction", !1),
        sticky: this.getField(t, "sticky", !1),
        notificationCloseEvent: this.getField(t, "notificationCloseEvent", !1),
        data: {
            options: {
                action: this.getField(t, "action", "default"),
                close: this.getField(t, "close", !0),
                notificationCloseEvent: this.getField(t, "notificationCloseEvent", !1),
                url: document.location.toString()
            }
        }
    }
}
,
NotificationGenerator.prototype.display = function() {
    if (this.verifyRequirements()) {
        var t = this.computeState(!0);
        if (!t.hasOwnProperty("title"))
            return void alert("The notification must at least have a title.");
        var i = t.title.value
          , e = this.createNotificationOptions(t)
          , o = t.persistent.value
          , n = o ? this.displayPersistent(i, e) : this.displayNonPersistent(i, e)
          , a = this;
        return n.then(function() {
            document.location.hash = a.serialize(a.computeState(!1))
        })
    }
}
,
NotificationGenerator.prototype.displayPersistent = function(t, i) {
    return navigator.serviceWorker.ready.then(function(e) {
        return e.showNotification(t, i)
    })["catch"](function(t) {
        alert(t)
    })
}
,
NotificationGenerator.prototype.displayNonPersistent = function(t, i) {
    return new Promise(function(e) {
        var o = null ;
        try {
            o = new Notification(t,i)
        } catch (n) {
            return alert(n),
            e()
        }
        o.addEventListener("click", function() {
            var t = i.data.options.action;
            if ("message" == t) {
                var e = 'Clicked on "' + o.title + '"';
                alert(e)
            } else
                "default" == t || "focus-only" == t || alert("action not supported " + t);
            i.data.options.close && o.close()
        }),
        o.addEventListener("close", function() {
            if (i.data.options.notificationCloseEvent) {
                var t = 'Closed "' + o.title + '"';
                alert(t)
            }
        }),
        o.addEventListener("show", function() {
            e()
        }),
        o.addEventListener("error", function(t) {
            alert(t),
            e()
        })
    }
    )
}
;
