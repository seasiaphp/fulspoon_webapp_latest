   ++++++++++++++++++V0.0.1.7 Release +++++++++++++++++++++++

Mobile Changes
QG-160	
Mobile: Subscriptions > selections between change of filters don't persist 

QG-159	
Mobile: order > tax/total 

QG-158	
Mobile: Payment history > subscription payments 

QG-157	
Mobile: Change subscription > flashing checkmarks 

QG-156	
Mobile: Filter > Cuisines does not work 





Website Changes
	
QG-153	
Sengrid: Decline Emails 

QG-152	
Sengrid: Password Recovery 

QG-151	
Sengrid: User Registration Mail 

QG-150	
Crone Issue 

QG-139	
Stripe Issue 

Crone Issue: Weekly Order 

QG-137	
Twilio 

QG-135	
Twilio Issues 

QG-134	
Twilio Issues 

QG-130	
Edget Browser>> NewOrders 

	
QG-62	
Twilio > Voicemail message 

QG-58	
Twilio > order details pronunciation 

QG-39	
Web > Subscriber signup 

QG-5	
Prod Admin > Fake restaurants (PROD)


   ++++++++++++++++++V0.0.1.7 Release +++++++++++++++++++++++
