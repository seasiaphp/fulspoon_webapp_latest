// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'companyApp.controllers' is found in controllers.js

var OrderWeb = angular.module('OrderWeb', ['ngRoute']);

OrderWeb.config( function($routeProvider) {
   			
		  $routeProvider
	  		.when('/', {
					controller: 'homeCntrl',
					templateUrl: "templates/home.html"
	  		})
			.when('/profile', {
					controller: 'companyCntrl',
					templateUrl: "templates/profile.html"
	  		})
			.otherwise({redirectTo : '/'});		 
})

OrderWeb.factory('config', function() {
		return {
			siteName	: 'Forkours',
			clientUrl	: 'http://192.168.1.254/forkourse/weborder/',
			baseUrl		: 'http://192.168.1.254/forkourse/',
		};
})
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
	remove: function(key, value) {
      $window.localStorage.removeItem(key);
    }
	
  }
}])
;

