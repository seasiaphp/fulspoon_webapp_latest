OrderWeb.controller('homeCntrl', function($scope,$http,config,$localstorage) {
	var restaurant_id=1;
	$scope.restaurantList=[];
	$scope.resname='';
	$scope.tab = 1;
	$http({
            method : 'POST',
            url : config.clientUrl+'restaurantList',
			data : {'restaurant_id':restaurant_id}
        }).success(function(response){
			if(response.status=='success'){
				$scope.restaurantList=response.result;
				//console.log ($scope.restaurantList);
			}else{	
				
			}
		});		
				
	$scope.selectRes = function(resname,id){
		$scope.title=resname;
		$scope.location_id=id;
	}
	$scope.removeRes = function(){
		$scope.title='';
		$scope.orderTime='';
		$scope.orderType='';
		$scope.orderTimeLater='';
		$scope.laterDate='';
		$scope.orderTimeTitle='';
	}
	$scope.selectOrderTime = function(orderTime){
		$scope.orderTimeTitle=orderTime;
		if(orderTime=='Later'){

			/*Date.prototype.getWeek = function() {
				var onejan = new Date(this.getFullYear(), 0, 1);
				//alert(onejan);
				return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
			}
			var weekNumber = (new Date()).getWeek();*/
			
			var dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
			var now = new Date();
			var daysArray= ["Today", dayNames[now.getDay()+1],dayNames[now.getDay()+2], dayNames[now.getDay()+3],dayNames[now.getDay()+4],dayNames[now.getDay()+5],dayNames[now.getDay()-1]];
			var dateArray= [now.getDate(),now.getDate()+1,now.getDate()+2,now.getDate()+3,now.getDate()+4,now.getDate()+5,now.getDate()+6];
			var laterDates = new Array();
			
			
			var obj = {};
			for(var i=0;i<7;i++){
				obj[daysArray[i]] = dateArray[i];
			}
			laterDates.push(obj);
			
			//console.log(laterDates);
			$scope.laterDates=laterDates;

			$scope.orderTimeLater=orderTime;
		}else{
			$scope.orderTime=orderTime;
		}
	}
	$scope.selectLaterOrderTime = function(laterorderTime){
		$scope.orderTimeTitle='Later';
		$scope.orderTime='Later';
		$scope.orderTimeLater='';
		var latertime=laterorderTime;
		
	}
	
	
	$scope.removeOrderTime = function(){
		$scope.orderTime='';
		$scope.orderType='';
		$scope.orderTimeLater='';
		$scope.orderTimeTitle='';
		$scope.laterDate='';
		
	}
	$scope.selectOrderType = function(orderType){
		$scope.orderType=orderType;
	}
	$scope.selectLaterDate = function(laterDate){
		$scope.laterDate=laterDate;
		var location_id=$scope.location_id;
		var dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		var now = new Date();
		var today = dayNames[now.getDay()];
		if(laterDate=='Today'){
			laterDate=today;
		}
		//alert(today);
		//alert(laterDate);
		$http({
            method : 'POST',
            url : config.clientUrl+'gerRestaurantTimings',
			data : {'location_id':location_id,'day':laterDate}
        }).success(function(response){
			if(response.status=='success'){
				$scope.timeArray=response.result;
				//console.log ($scope.timeArray);
			}else{	
				
			}
		});		
		//$scope.timeArray={"time":[{"start":"8.00"},{"start":"9.00"},{"start":"10.00"}]};
	}

	
	$scope.showConfirm = function(timeAr)
	{
		
		//alert($scope.timeArray.time.length);
		timeAr.showfull = !timeAr.showfull;
		for (var i = 0; i < $scope.timeArray.time.length; i++)
		{
			var currentItem = $scope.timeArray.time[i];
			if (currentItem != timeAr)
			{
				currentItem .showfull = false;
			}
		}
	}
	
	
});

