<?php

error_reporting(1);
ini_set("display_errors", "on");
ini_set("memory_limit", "256M");

class Client extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('client_model');
        $this->load->model('member_model');
    }

    public function index() {
        if (isset($HTTP_RAW_POST_DATA)) {
            $json = $HTTP_RAW_POST_DATA;
        } else {
            $json = implode("\r\n", file('php://input'));
        }


        /*  $json='{"function":"subscription_pay", "parameters":{"order_data": {"user_id": 9,
          "add_date": "2015-12-01",
          "count_restaurants": 2,
          "discount": "22.00",
          "email": "asd@as123d.asd",
          "from_date": "2015-12-01",
          "number_end": "3",
          "number_start": "1",
          "password": "asdasd",
          "pay_token": "tok_17EM5dE6RvePU8B8dmP6uYaO",
          "reg_method": "general",
          "rest_details": {"0":"ChIJi9BURJFZwokRDcqvIgxdXds","1":"ChIJP_DysOT1wokRbj86nKeyM2M"},
          "sub_package_id": "21",
          "to_date": "2015-12-01",
          "user_fee": "50.00"
          }}}'; */
        /* $json='{"function":"get_discount_detail", "parameters":{member_id: "341"}}'; */




        $json = preg_replace("{\\\}", "", $json);
        $array = json_decode($json, TRUE);
        //$token = $array['token'];
        $function_name = $array['function'];
        $this->$function_name($array['parameters']);
    }

    #Authentication function to check by v(version of API),apv - version of application for statistical purposes,authentication key,session key
    #Format example: A-1.0 (A for Android, I fro iOS, W for web))

    function auth_url($arr) {
        $this->load->model('user_model');
        if ($arr['parameters']['v'] == '') {
            $ar = array('result' => 'null', 'errorMessage' => 'missing API version', 'errorCode' => '1790');
            echo(json_encode($ar));
            exit;
        } else if ($arr['parameters']['apv'] == '') {
            $ar = array('result' => 'null', 'errorMessage' => 'missing API version', 'errorCode' => '1790');
            echo(json_encode($ar));
            exit;
        } else if ($arr['parameters']['authKey'] == '') {
            $ar = array('result' => 'null', 'errorMessage' => ' unauthorized to use API (wrong authKey)', 'errorCode' => '1810 ');
            echo(json_encode($ar));
            exit;
        } else if ($arr['parameters']['function'] != 'openSession' && $arr['parameters']['sessionKey'] == "") {
            $ar = array('result' => 'null', 'errorMessage' => 'Session has expired!', 'errorCode' => '1080');
            echo(json_encode($ar));
            exit;
        }
        return;
    }

    #Function to get session key

    function openSession($arr) {
        $this->load->model('user_model');

        #delete entry with the device token
        $this->user_model->deleteSessionEntry($arr['device_token']);
        $preferences = $this->user_model->getPreferences();

        if ($preferences['version'] == '1')
            $preferences['version'] = '1.0';
        else
            $preferences['version'] = '1';
        $preferences['session_id'] = $this->randomSessionId();

        #update session ID
        $this->user_model->updateSessionID($arr['device_token'], $preferences['session_id']);
        echo json_encode($preferences);
    }

    function facebookLogin() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $data = $this->user_model->facebookExist($arr['id']);
        if (!$data['facebook_id']) {
            $data_mail = $this->user_model->facebookExist_email($arr);
            if (!$data_mail['email']) {
                $result = array('status' => 'false', 'message' => 'facebook id not exist ');
                echo json_encode($result);
            } else {
                if ($data_mail['profile_image'] != '') {
                    $data_mail['profile_image'] = base_url() . "uploads/members/" . $data_mail['member_id'] . "_thumb." . $data_mail['profile_image'] . '?' . date("his");
                }
                $result = array('status' => 'true', 'message' => 'Fb Login Successful', 'data' => $data_mail);
                echo json_encode($result);
            }
        } else {

            $result = array('status' => 'true', 'message' => 'Fb Login Successful', 'data' => $data);
            echo json_encode($result);
        }
    }

    #Registration Function

    function registration() {
        $this->load->model('user_model');
        $arr = array(
            'DOB' => $_POST['DOB'],
            'Fname' => $_POST['Fname'],
            'Pname' => $_POST['Pname'],
            'Lname' => $_POST['Lname'],
            'member_type' => $_POST['member_type'],
            'phone' => $_POST['phone'],
        );

        $get_data = $this->user_model->checkEmailExist($arr['Email']);

        if (!$get_data['Fname']) {

            $user_id = $this->user_model->createUser($arr);


            if ($_FILES['image']['name'] <> "") {
                #################  Upload ############

                $imagename = strtotime(date("Y-m-d H:i:s"));
                $tempFile = $_FILES['image']['tmp_name'];
                $fileParts = pathinfo($_FILES['image']['name']);
                $image_name = $user_id . '.' . $fileParts['extension'];
                $foldername = "upload/members/$image_name";
                $targetFolder = 'upload/members';
                //$targetFolder = '/home4/keetownc/www/keetownorg/api/upload/members';
                $targetFile = realpath($targetFolder) . '/' . $image_name;
                move_uploaded_file($tempFile, $targetFile);
                //chmod('upload/members/' . $user_id . $extension, 757);
                $imagethumb = "upload/members/" . $user_id . "_thumb." . $fileParts['extension'];
                //$imagethumb1 = "upload/members" . $user_id . "_thumb1." . $fileParts['extension'];
                list($width, $height, $type, $attr) = getimagesize($targetFile);
                $configThumb = array();
                $configThumb['image_library'] = 'gd2';
                $configThumb['source_image'] = $targetFile;
                $configThumb['new_image'] = $imagethumb;
                $configThumb['create_thumb'] = false;
                $configThumb['maintain_ratio'] = TRUE;
                $configThumb['width'] = 320;
                $configThumb['height'] = 320;

                $this->load->library('image_lib');
                $this->image_lib->initialize($configThumb);
                $this->image_lib->resize();
                chmod('upload/members/' . $user_id . "_thumb." . $extension, 757);
                $this->user_model->updateMemberImage($fileParts['extension'], $user_id);
            }


            $result = array('status' => 'true', 'message' => 'User Account created successfully', 'user_id' => $user_id, 'username' => $arr['Fname'], 'phone' => $arr['phone']);

            echo json_encode($result);
        } else {
            $result = array('status' => 'false', 'message' => 'Email Address/Phone no: already exist ');
            echo json_encode($result);
        }
    }

    #login function

    function Login() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        #Normal Login
        $result = $this->user_model->userLogin($arr);
        if ($result) {
            if ($result['profile_image'] != '') {
                if ($result['profile_image']) {
                    $result['profile_image'] = base_url() . "uploads/members/" . $result['member_id'] . "_thumb." . $result['profile_image'] . '?' . date("his");
                }
            }
            $result = array('status' => 'true', 'email' => $arr['email'], 'message' => 'Login Successful', 'data' => $result);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false', 'message' => "Incorrect Username or Password");
            echo json_encode($result);
        }
    }

    function pre_order() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
    }

    function registration_Exist() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->checkEmailExist($arr['email']);
        if (sizeof($get_data)) {
            $result = array('status' => 'false', 'message' => "Email Exist");
            echo json_encode($result);
        } else {
            $result = array('status' => 'true', 'message' => "Email Not Exist");
            echo json_encode($result);
        }
    }

    function get_subscription_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->get_subscription_detail($arr['count']);
        //$get_data['count_restaurants']=$arr['count'];
        if (sizeof($get_data) > 1) {
            $status = 'false';
        } else {
            if ($get_data[0]['number_of_restaurants'] != $arr['count']) {
                $status = 'false';
            } else {
                $status = 'true';
            }
        }
        $result = array('status' => $status, 'data' => $get_data);
        echo json_encode($result);
    }

    function get_admin_detail() {
        $this->load->model('user_model');
        $get_data = $this->user_model->get_Admin_detail();
        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    function get_user_status_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $cond = array(member_id => $arr['member_id']);
        $get_data = $this->user_model->get_where('member_master', $cond, 'member_id');
        $expired = $this->user_model->get_discount_detail($arr['member_id']);
        if ($expired['subscription_id'] != '') {
            $ex_status = 'N';
        } else {
            $ex_status = 'Y';
            $package_details = $this->user_model->get_package_details($expired['package_id']);
        }
        $orderList = $this->user_model->getAllOders($arr['member_id']);
        if ($orderList[0]['order_id'] != '') {
            $past_order = 'Y';
        } else {
            $past_order = 'N';
        }
        if ($get_data['profile_image'] != '') {
            $get_data['profile_image'] = base_url() . "uploads/members/" . $get_data['member_id'] . "_thumb." . $get_data['profile_image'] . '?' . date("his");
        }
        if ($get_data['status'] == 'Y') {
            $status = 'Y';
        } else {
            $status = 'N';
        }
        $result = array('status' => $status, 'data' => $get_data, 'expired' => $ex_status, 'past_order' => $past_order, 'package_details' => $package_details);
        echo json_encode($result);
    }

    function get_restaurant_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->get_restaurant_detail($arr);
        $get_data_config = $this->user_model->config_filter();
        foreach ($get_data as $key => $cuisine) {
            $get_data[$key]['cuisines'] = $this->user_model->get_restaurant_cuisine_detail($cuisine['restaurant_id']);
        }
        $result = array('status' => 'true', 'data' => $get_data, 'filter' => $get_data_config);
        echo json_encode($result);
        //echo json_encode($get_data);
    }

    function get_user_restaurant_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->get_user_restaurant_detail($arr['user_id']);
        foreach ($get_data as $key => $cuisine) {
            $get_data[$key]['cuisines'] = $this->user_model->get_restaurant_cuisine_detail($cuisine['restaurant_id']);
        }
        echo json_encode($get_data);
    }

    function edit_profile_user() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->edit_profile_user($arr);
        $get_data_user = $this->user_model->get_userdetails($arr['member_id']);
        if ($get_data_user['profile_image'] != '') {
            $get_data_user['profile_image'] = base_url() . "uploads/members/" . $get_data_user['member_id'] . "_thumb." . $get_data_user['profile_image'] . '?' . date("his");
        }
        $result = array('status' => 'true', 'user_data' => $get_data_user);
        echo json_encode($result);
    }

    ##########################

    public function subscription_pay() {

        //print_r($_POST);
        $arr = json_decode(file_get_contents('php://input'), true);

        //$arr =$_POST;

        /* active: "Y"
          add_date: "2015-12-01"
          count_restaurants: 2
          discount: "22.00"
          email: "asd@asd.asd"
          from_date: "2015-12-01"
          number_end: "3"
          number_start: "1"
          password: "asdasd"
          pay_token: "tok_17DaOJE6RvePU8B89mlOXgo7"
          reg_method: "general"
          rest_details: "ChIJi9BURJFZwokRDcqvIgxdXds,ChIJP_DysOT1wokRbj86nKeyM2M"
          sub_package_id: "21"
          to_date: "2015-12-01"
          user_fee: "50.00" */

        $this->load->model('client_model');
        $this->load->library('stripe_lib');
        $stripe = $this->client_model->getAdmin_strip();
        $data = $arr;
        $array['payment_data'] = $data['order_data'];
        $restaurant = $data['order_data']['rest_details'];



        if ($data != '') {

            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            //print_r($stripe['stripe_private_key']);


            if ($array['payment_data']['lat'] != '') {


                $deal_lat = $array['payment_data']['lat'];
                $deal_long = $array['payment_data']['long'];
                $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $deal_lat . ',' . $deal_long . '&sensor=false');

                $output = json_decode($geocode);

                for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
                    $cn = array($output->results[0]->address_components[$j]->types[0]);
                    if (in_array("locality", $cn)) {
                        $city = $output->results[0]->address_components[$j]->long_name;
                    }
                    if (in_array("country", $cn)) {
                        $country = $output->results[0]->address_components[$j]->long_name;
                    }
                    if (in_array("postal_code", $cn)) {
                        $zipcode = $output->results[0]->address_components[$j]->long_name;
                    }
                    if (in_array("administrative_area_level_1", $cn)) {
                        $state = $output->results[0]->address_components[$j]->long_name;
                    }
                }
            }

            $arr_user = array("email" => $array['payment_data']['email'],
                "password" => md5($array['payment_data']['password']),
                "created_date" => date('Y-m-d H:i'),
                "auth_type" => $array['payment_data']['auth_type'],
                "first_name" => $array['payment_data']['name_on_card'],
                "billing_status" => 'N',
                "profile_image" => $array['payment_data']['profile_image'],
                "city" => $city,
                "state" => $state,
                "zipcode" => $zipcode,
                "device_id" => $array['payment_data']['device_id'],
                "lat" => $array['payment_data']['lat'],
                "long" => $array['payment_data']['long'],
            );
            $new_userid = $this->client_model->insert_user($arr_user);

            $admin_args = array(
                'description' => "Customer for " . $array['payment_data']['email'],
                'email' => $array['payment_data']['email'],
                'source' => $array['payment_data']['pay_token']
            );

            $customer = $this->stripe_lib->createCustomer($admin_args);
            $customer_strip_id = $customer->id;

            $last_4digit = $array['payment_data']['last4'];
            $brand = $array['payment_data']['brand'];
            $owner_payment = array(
                'amount' => bcmul($array['payment_data']['user_fee'], 100),
                'currency' => 'usd',
                'customer' => $customer_strip_id
            );
            $retData = $this->stripe_lib->chargeCustomer($owner_payment);
            $response = mysql_real_escape_string(serialize($retData));
            $total = bcdiv($retData->amount, 100, 3);

            $no = rand(000001, 999999);

            //$array['payment_data']['brand']="VISA";
            //$array['payment_data']['last4']="5544";

            $arr12 = array("member_id" => $new_userid,
                "strip_customer_id" => $customer_strip_id,
                "created_time" => date('Y-m-d H:i'),
                "last_4digit" => $array['payment_data']['last4'],
                "brand" => $array['payment_data']['brand'],
                "easy_name" => $array['payment_data']['easy_name'],
                "name" => $array['payment_data']['name_on_card'],
                "strip_order_id" => $retData->id,
                "amount" => $array['payment_data']['user_fee']
            );
            $subscription_id = $this->client_model->insert_member_stripid_map($arr12);

            $User_subscription_master = array("member_id" => $new_userid,
                "strip_id" => $subscription_id,
                "package_id" => $array['payment_data']['sub_package_id']);
            $user_sub_id = $this->user_model->insert_1('User_subscription_master', $User_subscription_master);

            //get user details in subscription_log 
            //$user_details_log=$this->user_model->user_details_log($new_userid);
            //get package details
            $get_package_details = $this->user_model->get_package_details($array['payment_data']['sub_package_id']);
            $User_subscription_log = array("member_id" => $new_userid,
                "start_date" => date('Y-m-d'),
                "End_date" => Date('Y-m-d', strtotime("+" . $get_package_details['durn'])),
                "subscription_id" => $user_sub_id);
            $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);


            foreach ($restaurant as $row) {
                $arr = array("member_id" => $new_userid,
                    "strip_id" => $subscription_id,
                    "subscription_id" => $user_sub_id,
                    "updated_on" => date('Y-m-d H:i'),
                    "restaurant_id" => $row);
                $this->db->insert('current_restaurant', $arr);
            }

            #send email
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;
            $this->load->library('email');
            $this->email->initialize($config);

            $this->email->from($stripe['email']);
            $this->email->reply_to($stripe['email']);
            $this->email->to($array['payment_data']['email']);
            //$this->email->to("bibinv@newagesmb.com");
            $this->email->subject("Welcome to Replimatic");
            $message = "<p>Thank you for registering with  Replimatic </p>
<p>Email address : '" . $array['payment_data']['email'] . "' <br></p>
<p>&nbsp;</p>
<p>Thanks,</p>
<p>Replimatic </p>";
            $this->email->message($message);
            $this->email->send();

            $result = array('status' => 'true', 'message' => $this->config->item('saveOrder'), 'user_id' => $new_userid);
        } else {
            $result = array('status' => 'error', 'message' => $this->config->item('invalidRequest'));
        }
        echo json_encode($result);
        //echo '<pre>';print_r($sides);echo'<pre>';exit;		 
    }

    function save_billing_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $this->user_model->update_phone($arr['bill_data']['phone'], $arr['bill_data']['user_id']);
        $arr_1 = array("member_id" => $arr['bill_data']['user_id'],
            "address1" => $arr['bill_data']['address1'],
            "city" => $arr['bill_data']['city'],
            "state" => $arr['bill_data']['state'],
            "zipcode" => $arr['bill_data']['zip'],
            "active" => 'Y',
            "billing_date" => date('Y-m-d H:i'),
            "address2" => $arr['bill_data']['address2'],
            "easy_name" => $arr['bill_data']['addrname'],
            "phone" => $arr['bill_data']['phone']
        );
        $this->db->insert('billing_master', $arr_1);
        $get_strip = $this->user_model->getUser_strip_details($arr['bill_data']['user_id']);
        $address = array("member_id" => $arr['bill_data']['user_id'],
            "stripeid" => $get_strip['id'],
            "address" => $arr['bill_data']['address1'] . ", " . $arr['bill_data']['address2'],
            "pincode" => $arr['bill_data']['zip'],
            "state" => $arr['bill_data']['state'],
            "city" => $arr['bill_data']['city'],
            "save_as" => $arr['bill_data']['addrname'],
            "phone" => $arr['bill_data']['phone']
        );
        $this->db->insert('address', $address);
        $billing_satus = $this->user_model->updateBilling_status($arr_1);
        $result_1 = $this->user_model->get_userdetails($arr['bill_data']['user_id']);
        $result = array('status' => 'true', 'message' => 'billing data inserted successfully', 'data' => $result_1);
        echo json_encode($result);
    }

    function get_full_cuisine_detail() {
        $this->load->model('user_model');
        $get_data = $this->user_model->get_full_cuisine_detail();
        foreach ($get_data as $cuisine) {
            $cuisine_data[] = $cuisine['cuisine_name'];
        }
        echo json_encode($cuisine_data);
    }

    function randomPassword() {
        $alphabet = "0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function generate_randomCode() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->checkEmailExist($arr['email']);
        if ($get_data) {
            //send email
            $new_password = $this->randomPassword(); # generate random password
            ## send new email address
            $dtls['new_password'] = $new_password;
            $admin_email = $this->user_model->getAdminEmail();
            $this->user_model->update_randomPassword_code(md5($new_password), $get_data['member_id']);
            //$site = $this->users_model->getWebsite();
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;
            $this->load->library('email');
            $this->email->initialize($config);

            $this->email->from($admin_email['email']);
            $this->email->reply_to($admin_email['email']);
            $this->email->to($arr['email']);
            //$this->email->to("bibinv@newagesmb.com");
            $this->email->subject("Forgot Password");
            $message = "<p>Your account details are </p>
<p>Email address : '" . $arr['email'] . "' <br>
  Random Code : '" . $new_password . "'</p>
<p>&nbsp;</p>
<p>Thanks,</p>
<p>Replimatic Team </p>";
            $this->email->message($message);
            $this->email->send();
            ## send new email address

            $result = array('status' => 'true', 'message' => 'Your new password has been sent to the email address');
            echo json_encode($result);
        } else {
            $result = array('status' => 'false', 'message' => 'Email not exist');
            echo json_encode($result);
        }
    }

    function check_Psdcode() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->check_Psdcode($arr['code']);
        if ($get_data) {
            $result = array('status' => 'true', 'data' => $get_data);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

    function update_new_password() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->update_new_password($arr['member_id'], $arr['password']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function get_restaurant_food_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');

        $get_data = $this->user_model->get_restaurant_food_detail($arr['restaurant_id']);

        $get_menu = $this->user_model->get_menu_detail($get_data['restaurant_id']);

        foreach ($get_menu as $key => $menu_detail) {
            $get_menu[$key]['dish'] = $this->user_model->get_dish_detail($menu_detail['id'], $get_data['restaurant_id']);
        }


        /* 	$id=$get_data['locu_id'];

          //$id="47bcb07e6e1629065984";
          error_reporting(E_ALL);
          $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
          $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
          \"api_key\" : \"$api_key\",
          \"fields\" : [\"name\", \"menus\" ],
          \"venue_queries\" : [
          { \"locu_id\":\"$id\" }

          ]
          }'";
          //        echo $uri;
          $c_pid = exec($uri);
          // echo "<pre>"; print_r(json_decode($c_pid)->venues[0]->menus[0]);
          if(isset(json_decode($c_pid)->venues[0]->menus[0]))
          {
          $status='true';
          }
          else
          {
          $status='false';
          }

          //echo $status;

          $result = array('status' => $status,'details'=>json_decode($c_pid)); */
        if (sizeof($get_menu)) {
            $status = 'true';
        } else {
            $status = 'false';
        }
        $result = array('status' => $status, 'details' => $get_menu);
        echo json_encode($result);
    }

    public function getRestaurantMenuDetail($array = '') {

        //$check_array='{"function":"getRestaurantMenuDetail", "parameters": {"item_id": "80"}}';
        $array = json_decode(file_get_contents('php://input'), true);


        //$array 	=	 json_decode($check_array, true);
        //$array=$array['parameters'];
        //echo '<pre>';print_r($array);exit;
        if ($array != '') {
            //$data['id'] = 12;
            $result_array = $this->user_model->getRestaurantMenuDetail($array['item_id']);
            //echo '<pre>';print_r($result_array);
            //if($array['member_id'])
            //	$check_fav  = $this->user_model->isFavItem($array['member_id'],$array['item_id']);

            $sizeresult = $this->user_model->getRestaurantMenuSizeDetail($array['item_id']);
            //echo '<pre>';print_r($result_array);
            //echo '<pre>';print_r($sizeresult);exit;


            $category = array();

            if (!empty($result_array)) {
                $i = 0;
                $sort_order =0;
                foreach ($result_array as $val) {
                    $menu['category_id'] = $val['menu'];
                    $menu['item_name'] = $val['dish_name'];
                    $menu['item_id'] = $val['id'];
                    $menu['item_description'] = nl2br($val['description']);

                    //$menu['is_fav'] = $check_fav;


                    if ($val['option_id'] != '') {

                        
                        $menu['option_id'][$sort_order] = $val['option_id'];
                        $menu['option_name'][$val['option_id']] = $val['option_name'];
                        $menu['is_mandatory'][$val['option_id']] = $val['mandatory'];
                        $menu['is_multiple'][$val['option_id']] = $val['multiple'];
                        $menu['multiple_limit'][$val['option_id']] = $val['limit'];
                        $menu['sortorder'][$val['option_id']] = $val['sortorder'];

                        if ($val['side_id'] != '') {

                            $side_sort_order = $val['sidesortorder'];
                            $menu['side_id'][$val['option_id']][$side_sort_order] = $val['side_id'];
                            $menu['side_item'][$val['option_id']][$val['side_id']] = $val['side_item'];
                            $menu['side_price'][$val['option_id']][$val['side_id']] = $val['side_price'];
                        } else {
                            $menu['side_id'][$val['option_id']] = array();
                            $menu['side_item'][$val['option_id']] = array();
                            $menu['side_price'][$val['option_id']] = array();
                        }
                        $i++;
                    } else {
                        $menu['option_id'] = array();
                        $menu['option_name'] = array();
                        $menu['is_mandatory'] = array();
                        $menu['side_id'] = array();
                        $menu['side_item'] = array();
                        $menu['side_price'] = array();
                    }
                    $sort_order++;
                }
                $menu['sizes'] = $sizeresult;
                $result = array('status' => 'success', 'result' => $menu);
            } else {
                $result = array('status' => 'error', 'message' => $this->config->item('invalidLocation'));
            }
        } else {
            $result = array('status' => 'error', 'message' => $this->config->item('invalidRequest'));
        }

        echo json_encode($result);
    }

    function get_discount_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);

        $this->load->model('user_model');
        $get_data = $this->user_model->get_discount_detail($arr['member_id']);
        $get_data['address'] = $this->user_model->get_address_user($arr['member_id']);
        $get_data['card'] = $this->user_model->get_user_carddetails($arr['member_id']);
        $get_data['delivery_time'] = '45';
        $get_data['tax'] = '8';
        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    ###############for web services################

    function get_subscription_detail_web($count) {

        $this->load->model('user_model');
        $get_data = $this->user_model->get_subscription_detail($count);
        //$get_data['count_restaurants']=$arr['count'];
        if (sizeof($get_data) > 1) {
            $status = 'false';
        } else {
            if ($get_data[0]['number_of_restaurants'] != $count) {
                $status = 'false';
            } else {
                $status = 'true';
            }
        }
        $result = array('status' => $status, 'data' => $get_data);
        echo json_encode($result);
    }

    #################################

    function get_alladdress_user() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->get_alladdress_user($arr['member_id']);
        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    function get_user_carddetails_all() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->get_user_carddetails_all($arr['member_id'], $all_addr = 'all');

        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    function add_user_carddetails() {

        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->library('stripe_lib');
        $stripe = $this->client_model->getAdmin_strip();
        $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
        $admin_args = array(
            'description' => "Customer for " . $arr['email'],
            'email' => $arr['email'],
            'source' => $arr['card_token']
        );


        $customer = $this->stripe_lib->createCustomer($admin_args);
        $customer_strip_id = $customer->id;

        $arr12 = array("member_id" => $arr['member_id'],
            "strip_customer_id" => $customer_strip_id,
            "created_time" => date('Y-m-d H:i'),
            "last_4digit" => $arr['last4'],
            "brand" => $arr['brand'],
            "easy_name" => $arr['easy_name'],
            "name" => $arr['name_on_card'],
            "strip_order_id" => '0',
            "amount" => '0',
            "card_status" => 'N'
        );
        $subscription_id = $this->client_model->insert_member_stripid_map($arr12);
        $result = array('status' => 'true', 'member_strip_id' => $subscription_id);
        echo json_encode($result);
    }

    function add_user_cardaddress() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $address = array("member_id" => $arr['member_id'],
            "stripeid" => $arr['card_id'],
            "address" => $arr['address'],
            "pincode" => $arr['pincode'],
            "state" => $arr['state'],
            "city" => $arr['city'],
            "save_as" => $arr['save_as'],
            "phone" => $arr['phone']
        );
        $this->db->insert('address', $address);
        $data = array(
            "card_status" => 'Y'
        );
        $this->user_model->delete_user_cardaddress($data, $arr['card_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function edit_user_cardaddress() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $address = array("member_id" => $arr['member_id'],
            "stripeid" => $arr['card_id'],
            "address" => $arr['address'],
            "pincode" => $arr['pincode'],
            "state" => $arr['state'],
            "city" => $arr['city'],
            "save_as" => $arr['save_as'],
            "phone" => $arr['phone']
        );
        if ($arr['ad_id']) {
            $this->user_model->edit_user_cardaddress($address, $arr['ad_id']);
        } else {
            $this->db->insert('address', $address);
        }

        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function delete_user_cardaddress() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $data = array(
            "delete_status" => 'Y'
        );
        $this->user_model->delete_user_cardaddress($data, $arr['id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function get_cms_datas() {
        $arr = json_decode(file_get_contents('php://input'), true);
        if ($arr['key'] == 'privacy') {
            $data_privacy = $this->user_model->get_cms_datas_privacy();
            $result = array('status' => 'true', "privacy" => $data_privacy['Answer']);
            echo json_encode($result);
        }
        if ($arr['key'] == 'terms') {
            $data_terms = $this->user_model->get_cms_datas_terms();
            $result = array('status' => 'true', 'terms' => $data_terms['Answer']);
            echo json_encode($result);
        }
        if ($arr['key'] == 'faq') {
            $data_faq = $this->user_model->get_cms_datas_faq();
            $result = array('status' => 'true', 'faq' => $data_faq['Answer']);
            echo json_encode($result);
        }
        if ($arr['key'] == 'help') {
            $data_help = $this->user_model->get_cms_datas_help();
//            print_r($data_help);
            $result = array('status' => 'true', 'help' => $data_help['Answer']);
            echo json_encode($result);
        }
    }

    function contact_us() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $data_1 = $this->member_model->admin_contratct();
        $name = $arr['name'];
        $message = $arr['message'];
        $email = $arr['email'];
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['crlf'] = PHP_EOL;
        $config['newline'] = PHP_EOL;
        $message1 = "Name " . $name . "<br/>Email " . $email . "<br/> Message: " . $message;
        $this->load->library('email');
        $this->email->initialize($config);

        $this->email->from($data_1['email']);
        $this->email->to($data_1['contact_email']);
//            $this->email->to($this->config->item('email_from'));
        $this->email->subject("Message From Contact Us");
        $this->email->message($message1);
        $this->email->send();
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function insert_address() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $adds_id = $this->user_model->insert_1('address', $arr);
        $result = array('status' => 'true', 'id' => $adds_id);
        echo json_encode($result);
    }

    function edit_address() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $adds_id = $this->user_model->edit_address($arr, $arr['ad_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function delete_address() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $adds_id = $this->user_model->delete_address($arr['ad_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function save_order_data() {

        $test_array = json_decode(file_get_contents('php://input'), true);
        foreach ($test_array as $data) {

            $this->load->library('stripe_lib');
            $stripe = $this->client_model->getAdmin_strip();
            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            $get_data = $this->user_model->get_strip_detail_fromstrip_id($data['payment_card']['id']);
            $owner_payment = array(
                'amount' => bcmul($data['totalDue'], 100),
                'currency' => 'usd',
                'customer' => $get_data['strip_customer_id']
                    //'customer' =>'cus_7nn51keAcwIolj'
            );
            //print_r($owner_payment);
            $retData = $this->stripe_lib->chargeCustomer($owner_payment);
            $response = mysql_real_escape_string(serialize($retData));
            $total = bcdiv($retData->amount, 100, 3);

            $no = rand(000001, 999999);
            $ref_num = "#RP" . $data['user_id'] . $no;
            $data['referance_number'] = $ref_num;
            if ($retData->id) {
                $data['name'] = $this->user_model->get_user_name($data['user_id']);
                $get_package_id = $this->user_model->get_package_id($data['user_id']);
                $arr12 = array("member_id" => $data['user_id'],
                    "strip_customer_id" => $get_data['strip_customer_id'],
                    "restaurant_id" => $data['rest_id'],
                    "subscription_id" => $get_package_id['subscription_id'],
                    "created_time" => date('Y-m-d H:i'),
                    "last_4digit" => $get_data['last_4digit'],
                    "brand" => $get_data['brand'],
                    "easy_name" => $get_data['easy_name'],
                    "name" => $get_data['name'],
                    "strip_order_id" => $retData->id,
                    "amount" => $data['totalDue']
                );

                $owner_email = $this->member_model->get_owner_email($data['user_id']);
                $subscription_id = $this->client_model->insert_member_stripid_map($arr12);
                $ordermaster = array("member_id" => $data['user_id'],
                    "order_ref_id" => $ref_num,
                    "restaurant_id" => $data['rest_id'],
                    "location_id" => $data['rest_id'],
                    "subscription_id" => $get_package_id['subscription_id'],
                    "created_time" => date("Y-m-d H:i:s"),
                    "order_status" => "New",
                    "sub_total" => $data['subtotal'],
                    "total_amount" => $data['totalDue'],
                    "discount_amount" => $data['discount'],
                    "tax_amount" => $data['tax'],
                    "delivery_service_amount" => $data['delivery_fee'],
                    "payment_status" => 'Y',
                    "order_type" => $data['delivery_method'],
                    "is_later" => $data['is_later'],
                    "tip" => $data['tip'],
                    "stripe_id" => $subscription_id
                        //"delivery_time"=>date('Y-m-d H:i:s',strtotime($array['delivery_time']))
                );
                //echo '<pre>';print_r($ordermaster);      
                $order_id = $this->user_model->insert_1('order_master', $ordermaster);



                foreach ($data['cart_data'] as $cart) {
                    //get item data
                    //echo "<pre>";print_r($cart);
                    $orderitems = array("order_id" => $order_id,
                        "item_id" => $cart['item_id'],
                        "unit_price" => $cart['size']['price'],
                        "quantity" => $cart['quantity'],
                        "price" => $cart['size']['price'],
                        "instructions" => $cart['specialInst'],
                        "size" => $cart['size']['size']
                    );

                    $ord_item_id = $this->user_model->insert_1('order_items', $orderitems);

                    $data['ord_item_id']=$order_id;
                    foreach ($cart['options'] as $option) {
                        foreach ($option['sides'] as $sides) {
                            //get side details
                            $ar1[] = $sides['side_name'];
                            $ar2[] = $sides['side_price'];
                            $ar3[] = $sides['side_id'];
                        }
                        $sides = array("sides" => $ar1, "price" => $ar2, "side_id" => $ar3);
                        $sides = serialize($sides);
                        $orderoptionmap = array("order_id" => $order_id,
                            "ord_item_id" => $ord_item_id,
                            "options" => $option['option_name'],
                            "sides" => $sides,
                            "option_id" => $option['option_id']
                        );
                        $mapid = $this->user_model->insert_1('order_option_map', $orderoptionmap);
                    }
                }
                $discount_amount = $this->user_model->get_discount_user_amount($data['user_id']);

                if ($data['delivery_method'] == 'delivery') {
                    $new_array = array(
                        "zipcode" => $data['delivery_address']['pincode'],
                        "address_id" => $data['delivery_address']['ad_id'],
                        "member_id" => $data['delivery_address']['member_id'],
                        "city" => $data['delivery_address']['city'],
                        "state" => $data['delivery_address']['state'],
                        "notes" => $data['delivery_address']['delivery_notes'],
                        "address" => $data['delivery_address']['address'],
                        "order_id" => $order_id,
                        "phone" => $data['delivery_address']['phone'],
                        "created_date" => date('Y-m-d H:i:s')
                    );
                    $address_id = $this->user_model->insert_1('delivery_address', $new_array);
                }
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['crlf'] = PHP_EOL;
                $config['newline'] = PHP_EOL;
                $admin_mail = $this->member_model->admin_contratct();
                $admn_email = $admin_mail['email'];
                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->reply_to($admn_email, 'Fulspoon');
                $this->email->from($admn_email, 'Fulspoon');
                $this->email->bcc('aneeshg@newagesmb.com');
                $this->email->bcc('anton@newagesmb.com');
                $this->email->to($owner_email);
                $this->email->subject("New Order in Fulspoon!!-(" . $ref_num . ")");
                $mail_contents = $this->load->view('email/email.php', $data, TRUE);
                $this->email->message($mail_contents);
                $this->email->send();
            }
        }


        $result = array('status' => 'true', 'data' => $discount_amount);
        echo json_encode($result);
    }

    function current_rest() {
        //$arr =json_decode('{"old_password":"123456","new_password":"qwerty","confirm":"qwerty","member_id":"401"}', TRUE);
        $arr = json_decode(file_get_contents('php://input'), true);
        $user_id = $arr['member_id'];
        $current_rest = $this->client_model->get_rest($user_id);
        $result = array('status' => 'true', 'restaurants' => $current_rest);
        echo json_encode($result);
    }

    function change_password() {
        //$arr =json_decode('{"old_password":"123456","new_password":"qwerty","confirm":"qwerty","member_id":"401"}', TRUE);
        $arr = json_decode(file_get_contents('php://input'), true);
        $change_password = $this->user_model->check_old_psd(md5($arr['old_password']), $arr['member_id']);
        if ($change_password) {
            $update_newPassword = $this->user_model->update_newPassword(md5($arr['new_password']), $arr['member_id']);
            $result = array('status' => 'true');
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

    function test() {
        echo md5(123456);
        exit;
        //$lat='40.4574055';
        //$lng='-106.2088298';



        $deal_lat = 40.4574055;
        $deal_long = -106.2088298;
        $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $deal_lat . ',' . $deal_long . '&sensor=false');

        $output = json_decode($geocode);

        for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
            $cn = array($output->results[0]->address_components[$j]->types[0]);
            if (in_array("locality", $cn)) {
                $city = $output->results[0]->address_components[$j]->long_name;
            }
            if (in_array("country", $cn)) {
                $country = $output->results[0]->address_components[$j]->long_name;
            }
            if (in_array("postal_code", $cn)) {
                $zipcode = $output->results[0]->address_components[$j]->long_name;
            }
            if (in_array("administrative_area_level_1", $cn)) {
                $state = $output->results[0]->address_components[$j]->long_name;
            }
        }
        echo $city;
        echo $country;
        echo $zipcode;
        echo $state;




        exit;
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
        $json = @file_get_contents($url);
        $data = json_decode($json);
//echo $data->results[0]->formatted_address;
        echo "<pre>";
        print_r($data);
        exit;



        $address = 'mulanthuruthy,ernakulam';
        echo "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&sensor=false";
        exit;
// We get the JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
// We convert the JSON to an array
        $geo = json_decode($geo, true);
// If everything is cool
        if ($geo['status'] = 'OK') {
            echo $latitude = $geo['results'][0]['geometry']['location']['lat'];
            echo $longitude = $geo['results'][0]['geometry']['location']['lng'];
        }
    }

    function get_all_oder_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $orderList = $this->user_model->getAllOders($arr['member_id']);

        if (count($orderList) != 0) {
            $total_amount = 0;
            $discount_amount = 0;

            for ($i = 0; $i < count($orderList); $i++) {
                $total_amount = $total_amount + $orderList[$i]['total_amount'];
                $discount_amount = $discount_amount + $orderList[$i]['discount_amount'];
                //$orderList[$i]['items'] = $this->user_model->getAllOderItemDetails($orderList[$i]['order_id']);
                //$orderList[$i]['side_order'] = $this->user_model->getAllOder_side($orderList[$i]['order_id']);
            }

            $result = array('status' => 'true', 'orderList' => $orderList, 'total_amount' => $total_amount, 'discount_amount' => $discount_amount);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

    function Twilo() {
        require('twilio-php-master/Services/Twilio.php');

        $account_sid = 'ACc006359abbec78df3e8fc0488c68cede';
        $auth_token = 'e4367679af00b2529a9f6bf369e8c88a';
        $client = new Services_Twilio($account_sid, $auth_token);

        $client->account->calls->create('+12014318323', '+919496803469', 'AP6f2aed3d5119b29fa265d2c7314778ed', array(
            'Method' => 'GET',
            'FallbackMethod' => 'GET',
            'StatusCallbackMethod' => 'GET',
            'Record' => 'false',
        ));
    }

    function get_all_oder_restaurant_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $orderList = $this->user_model->getAllOders($arr['member_id'], $arr['restaurant']);
        if (count($orderList) != 0) {
            $total_amount = 0;
            $discount_amount = 0;

            for ($i = 0; $i < count($orderList); $i++) {
                $total_amount = $total_amount + $orderList[$i]['total_amount'];
                $discount_amount = $discount_amount + $orderList[$i]['discount_amount'];
                $orderList[$i]['items'] = $this->user_model->getAllOderItemDetails($orderList[$i]['order_id']);
            }
            $result = array('status' => 'true', 'orderList' => $orderList);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

    function update_push_notif() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->user_model->update_push_notif($arr['push'], $arr['member_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function geoLocation() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->user_model->geoLocation($arr['geo_location'], $arr['member_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function change_subscription_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $User_subscription_master = array("member_id" => $arr['member_id'],
            "package_id" => $arr['sub_package_id']);
        $user_sub_id = $this->user_model->insert_1('User_subscription_master', $User_subscription_master);
        foreach ($restaurant as $row) {
            $arr = array("member_id" => $arr['member_id'],
                "subscription_id" => $user_sub_id,
                "updated_on" => date('Y-m-d H:i'),
                "restaurant_id" => $row);
            $this->db->insert('current_restaurant', $arr);
        }
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    function updateMemberImage($user_id) {

        $user_id = $_GET['member_id'];
        // $user_id = $_POST['member_id'];


        if ($_FILES['image']['name'] <> "") {
            #################  Upload ############

            $imagename = strtotime(date("Y-m-d H:i:s"));
            $tempFile = $_FILES['image']['tmp_name'];
            $fileParts = pathinfo($_FILES['image']['name']);
            $image_name = $user_id . '.' . $fileParts['extension'];
            $foldername = "uploads/members/$image_name";
            $targetFolder = 'uploads/members';
            $targetFile = realpath($targetFolder) . '/' . $image_name;
            move_uploaded_file($tempFile, $targetFile);
            $imagethumb = "uploads/members/" . $user_id . "_thumb." . $fileParts['extension'];
            //$imagethumb1 = "upload/members" . $user_id . "_thumb1." . $fileParts['extension'];
            list($width, $height, $type, $attr) = getimagesize($targetFile);
            $configThumb = array();
            $configThumb['image_library'] = 'gd2';
            $configThumb['source_image'] = $targetFile;
            $configThumb['new_image'] = $imagethumb;
            $configThumb['create_thumb'] = false;
            $configThumb['maintain_ratio'] = TRUE;
            $configThumb['width'] = 320;
            $configThumb['height'] = 320;

            $this->load->library('image_lib');
            $this->image_lib->initialize($configThumb);
            $this->image_lib->resize();
            chmod('uploads/members/' . $user_id . "_thumb." . $extension, 757);
            $this->user_model->updateMemberImage($fileParts['extension'], $user_id);
            $result = array('status' => 'true', 'image' => base_url() . "uploads/members/" . $user_id . "_thumb." . $fileParts['extension']);
        } else {
            $result = array('status' => 'false');
        }

        echo json_encode($result);
    }

    function getUser_subscription_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $data = $this->user_model->get_subscription_all_details($arr['member_id']);
        $result = array('status' => 'true', 'data' => $data);
        echo json_encode($result);
    }

    function get_past_order_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $member_id = $arr['member_id'];
        $order_id = $arr['order_id'];
        $order_details = $this->user_model->get_order_details($order_id);
        $data = $this->user_model->get_past_order_details($member_id, $order_id);
        $address = $this->user_model->get_order_address($order_id);
        $subscription_data = $this->user_model->get_ord_rest_subscr_details($member_id, $order_id);

        $result = array('status' => 'true', 'data' => $data, 'subscription_data' => $subscription_data, "address" => $address, 'order_details' => $order_details);
        echo json_encode($result);
    }

    function get_all_paymentHistory() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $payment_history = $this->user_model->payment_history($arr['member_id']);
        $orderList = $this->user_model->getAllOders($arr['member_id']);

        if (count($orderList) != 0) {
            $total_amount = 0;
            $discount_amount = 0;

            for ($i = 0; $i < count($orderList); $i++) {
                $total_amount = $total_amount + $orderList[$i]['total_amount'];
                $discount_amount = $discount_amount + $orderList[$i]['discount_amount'];
                //$orderList[$i]['items'] = $this->user_model->getAllOderItemDetails($orderList[$i]['order_id']);
                //$orderList[$i]['side_order'] = $this->user_model->getAllOder_side($orderList[$i]['order_id']);
            }

            $result = array('status' => 'true', 'orderList' => $orderList, 'total_amount' => $total_amount, 'discount_amount' => $discount_amount, 'payment_history' => $payment_history);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

}

?>