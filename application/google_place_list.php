<?php
//echo "<pre>";
//print_r($response[0]['locu_data']->locu_extend);
//exit;
?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Fulspoon | Home">
        <meta name="author" content="NewageSMB">
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style-api.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />

        <link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <!--<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">-->
<!--        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>-->

        <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap-select.css">
        <script src="<?= base_url() ?>js/jquery.min.js"></script>
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>js/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery_009.js"></script>





        <!-- Margo JS  -->

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <style>
            .placeholder::-webkit-input-placeholder { color: red !important;}
            .placeholder:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                color:    red !important;
                opacity:  1;
            }
            .placeholder::-moz-placeholder { /* Mozilla Firefox 19+ */
                color:    red !important;
                opacity:  1;
            }
            .placeholder:-ms-input-placeholder { /* Internet Explorer 10+ */
                color:   red !important;
            }

            .dropdown-menu li{ float:inherit !important;}
            .bootstrap-select .btn{ height:50px;}
            .bootstrap-select.form-control{ height:inherit !important;}
            .bootstrap-select .btn .caret{ color:#6FBE44 !important; border-left: 8px solid transparent !important;
                                           border-right: 8px solid transparent !important;
                                           border-top: 8px dashed !important;}
            .bs-donebutton{ display:none;}
        </style>

    </head>

    <body style="background:#6fbe44 !important;">
        <div class="navbar navbar-default  navbar-static-top" style="background-color:#fff; border-bottom:2px solid #6fbe44;">
            <div class="container">
                <div class="navbar-header">
                    <!-- Stat Toggle Nav Link For Mobiles -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End Toggle Nav Link For Mobiles -->
                    <a class="navbar-brand" href="<?= base_url() ?>owners">     <?php echo '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="170px" height="46px" viewBox="0 0 792 214" style="enable-background:new 0 0 792 214;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#73BF4C;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{opacity:0.52;}
	.st3{clip-path:url(#SVGID_6_);fill:#6EBD44;}
	.st4{fill:url(#SVGID_7_);}
</style>
<g>
	<g>
		<g>
			<g>
				<path class="st0" d="M253.3,163.1v-61.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
					c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H253.3z"/>
				<path class="st0" d="M262.9,166.1h-12.6v-61.5h-13.9V92.6h13.9v-3.1c0-16.9,8.3-26.3,23.4-26.3c5.5,0,10.6,1.8,15.3,5.3l2.3,1.7
					l-6.5,9.7l-2.4-1.5c-3.6-2.1-5.6-3-8.9-3c-4.9,0-10.5,1.6-10.5,14.1v3.1h22.4v11.9h-22.4V166.1z M256.3,160.1h0.6V89.6
					c0-17.5,10.3-20.1,16.5-20.1c3.9,0,6.7,1,9.5,2.5l0.1-0.1c-2.9-1.7-6.1-2.5-9.3-2.5c-11.7,0-17.4,6.6-17.4,20.3V160.1z"/>
			</g>
			<g>
				<path class="st0" d="M353.1,163.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V94.9h6.7V134
					c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V94.9h6.7v68.2H353.1z"/>
				<path class="st0" d="M328.9,167.2L328.9,167.2c-20.5,0-33.7-13-33.7-33V91.9h12.7V134c0,13.3,7.4,20.5,20.9,20.5l0.3,0
					c12.3-0.1,20.8-9.1,20.8-21.8V91.9h12.7v74.2h-12.6v-6.7C344.7,164.3,337.3,167.1,328.9,167.2L328.9,167.2z M301.2,97.9v36.2
					c0,16.7,10.6,27,27.7,27c4.9-0.1,13.2-1.3,19-8c-4.7,4.6-11.2,7.3-18.6,7.4l-0.4,0c-16.6,0-26.9-10.2-26.9-26.5V97.9H301.2z
					 M356.1,160.1h0.6V97.9H356v34.9c0,3.3-0.5,6.4-1.4,9.2l1.5-2.7V160.1z"/>
			</g>
			<g>
				<rect x="377.5" y="66.7" class="st0" width="6.7" height="96.4"/>
				<path class="st0" d="M387.2,166.1h-12.7V63.7h12.7V166.1z M380.5,160.1h0.7V69.7h-0.7V160.1z"/>
			</g>
			<g>
				<path class="st0" d="M450.8,107.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
					c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
					c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
					c9.9,0,18.6,2.8,25.3,9.1L450.8,107.5z"/>
				<path class="st0" d="M429.8,167.2c-14.1,0-26.1-5.6-32.3-15l-1.4-2.2l9.4-8.4l1.9,2.8c4.5,6.4,12.9,10.3,22.5,10.3
					c5.4,0,17.9-1,18.2-10.1c0.2-6.7-7.7-8.5-19.6-10.4c-12.8-2.1-28.8-4.8-29-21.2c-0.1-5.4,1.8-10.1,5.5-13.8
					c5.2-5.2,13.9-8.2,24-8.2l0.5,0c11.1,0,20.4,3.3,27.4,9.9l2.2,2.1l-8.2,8.7l-2.2-1.9c-6.1-5.3-11.9-6.4-19.2-6.4l-0.5,0
					c-6.7,0-12.3,1.7-15.1,4.6c-1.3,1.4-2,3-1.9,4.9c0.2,5.9,8,7.7,17.3,9.4l2.4,0.4c11.9,2,29.8,5,29,22.8
					C460.3,161.4,444.5,167.2,429.8,167.2z M404.1,151c5.4,6.4,14.9,10.2,25.7,10.2c5.8,0,24.5-1.2,25-16.1
					c0.5-11.6-10.1-14.3-24-16.7l-2.4-0.4c-8.7-1.5-22-3.9-22.3-15.1c-0.1-3.5,1.1-6.7,3.6-9.2c4-4.1,11-6.4,19.4-6.4l0.5,0
					c6.6,0,13.7,0.8,20.9,6.1l0.1-0.1c-5.5-4.2-12.6-6.3-21-6.3l-0.5,0c-8.5,0-15.7,2.3-19.8,6.4c-2.5,2.5-3.8,5.8-3.7,9.5
					c0.1,10.6,9.5,13,23.9,15.4c11.1,1.8,25,4,24.7,16.5c-0.4,9.9-9.4,15.9-24.2,15.9c-10.2,0-19.4-3.8-25.3-10.2L404.1,151z"/>
			</g>
			<g>
				<path class="st0" d="M472.8,191.4V95.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35
					c0,22.9-15.4,35.3-34.6,35.3c-11.6,0-22.3-5.6-28.1-16.9v44.1H472.8z M535.2,129.1c0-19.1-12.4-28.5-27.8-28.5
					c-15.8,0-27.5,12-27.5,28.6c0,16.7,12,28.6,27.5,28.6C522.8,157.8,535.2,148.2,535.2,129.1"/>
				<path class="st0" d="M482.3,194.4h-12.5V92.1h12.6v9.8c6.6-6.9,16-10.8,26.5-10.8l0.1,0c21.5,0.8,35.9,16.1,35.9,38
					c0,22.9-15.1,38.3-37.6,38.3c-9.9,0-18.7-3.8-25.1-10.5V194.4z M475.8,188.4h0.5v-53.5l1.7,3.4c-0.8-2.9-1.2-5.9-1.2-9.1
					c0-2.9,0.3-5.8,1-8.4l-1.4,2.6V98.1h-0.6V188.4z M487.8,153.9c5.1,4.8,11.9,7.4,19.6,7.4c19.2,0,31.6-12.7,31.6-32.3
					c0-18.7-11.8-31.3-30.1-32c-4,0-7.8,0.7-11.2,1.9c3-1,6.3-1.5,9.8-1.5c18.7,0,30.8,12.4,30.8,31.5c0,19.3-12.1,31.8-30.8,31.8
					C499.8,160.8,493,158.3,487.8,153.9z M507.3,103.6c-14.2,0-24.5,10.8-24.5,25.6c0,14.9,10.3,25.6,24.5,25.6
					c12,0,24.8-6.8,24.8-25.8C532.2,113.1,522.9,103.6,507.3,103.6z"/>
			</g>
			<g>
				<path class="st0" d="M555.3,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
					C570.7,164,555.3,150.8,555.3,129.2 M617.8,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
					c0,17.6,12.5,28.2,27.9,28.2C605.2,157.4,617.8,146.8,617.8,129.2"/>
				<path class="st0" d="M589.8,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
					C627.4,151.5,611.9,167,589.8,167z M589.8,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8
					c18.6,0,31.6-13.1,31.6-31.8C621.4,110.1,608.4,96.8,589.8,96.8z M589.8,160.4c-18.2,0-30.9-12.8-30.9-31.2
					c0-18.6,13-32.1,30.9-32.1c17.9,0,31,13.5,31,32.1C620.8,147.6,608,160.4,589.8,160.4z M589.8,103.2c-14.5,0-24.9,11-24.9,26.1
					c0,14.9,10.3,25.2,24.9,25.2c14.7,0,25-10.4,25-25.2C614.8,114.1,604.3,103.2,589.8,103.2z"/>
			</g>
			<g>
				<path class="st0" d="M636.8,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
					C652.2,164,636.8,150.8,636.8,129.2 M699.3,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
					C686.7,157.4,699.3,146.8,699.3,129.2"/>
				<path class="st0" d="M671.3,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
					C708.9,151.5,693.4,167,671.3,167z M671.3,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8
					c18.6,0,31.6-13.1,31.6-31.8C702.9,110.1,689.9,96.8,671.3,96.8z M671.3,160.4c-18.2,0-31-12.8-31-31.2c0-18.6,13-32.1,31-32.1
					c17.9,0,31,13.5,31,32.1C702.3,147.6,689.5,160.4,671.3,160.4z M671.3,103.2c-14.5,0-25,11-25,26.1c0,14.9,10.3,25.2,25,25.2
					c14.7,0,25-10.4,25-25.2C696.3,114.1,685.8,103.2,671.3,103.2z"/>
			</g>
			<g>
				<path class="st0" d="M728,95.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V124
					c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H728z"/>
				<path class="st0" d="M786,166.1h-12.7V124c0-13.1-7.6-20.5-20.8-20.5l-0.4,0c-12.3,0.1-20.9,9.1-20.9,21.8v40.9h-12.7v-74H731
					v6.6c5.4-4.9,12.8-7.7,21.2-7.8c20.5,0,33.8,13,33.8,33V166.1z M779.2,160.1h0.7v-36.2c0-16.7-10.6-27-27.7-27
					c-4.8,0.1-13.1,1.3-18.9,8c4.7-4.6,11.2-7.3,18.6-7.3l0.4,0c16.6,0,26.8,10.2,26.8,26.5V160.1z M724.4,160.1h0.7v-34.9
					c0-3.2,0.5-6.3,1.3-9.1l-1.5,2.6V98.1h-0.6V160.1z"/>
			</g>
			<path class="st0" d="M94.8,161c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
				c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
				c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
				C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
				c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
				C83.9,152.9,88.3,158.9,94.8,161"/>
			<g>
				<g>
					<defs>
						<rect id="SVGID_1_" x="3.9" y="6.8" width="214.9" height="203.2"/>
					</defs>
					<clipPath id="SVGID_2_">
						<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st1">
						<defs>
							<path id="SVGID_3_" d="M94.8,161c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
								c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
								c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
								C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
								c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
								C83.9,152.9,88.3,158.9,94.8,161"/>
						</defs>
						<clipPath id="SVGID_4_">
							<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
						</clipPath>
					</g>
				</g>
			</g>
			<g class="st2">
				<g>
					<defs>
						<rect id="SVGID_5_" x="3.9" y="12.2" width="214.9" height="197.8"/>
					</defs>
					<clipPath id="SVGID_6_">
						<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
					</clipPath>
					<path class="st3" d="M215.6,72.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
						c2.5,54.9-48.4,101.6-101.6,101.6C49.9,198.2,8.4,154,4,100.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
						C218.5,130,222.2,100.6,215.6,72.9"/>
				</g>
			</g>
		</g>
	</g>
	<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="107.8113" y1="62.1905" x2="84.9779" y2="90.1905">
		<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0"/>
		<stop  offset="0.4058" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
		<stop  offset="0.9913" style="stop-color:#787878;stop-opacity:0.6"/>
	</linearGradient>
	<path class="st4" d="M89.9,52.7c0,0-7.5,13.8-6,53.3c0,0,1.6-25.4,31.6-21.8c0,0-2-20.4,8.6-33.5L89.9,52.7z"/>
</g>
</svg>'; ?>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <!-- Stat Search -->
                    <!-- Stat Search -->
                    <div class="buttons-side">
                        <!--<a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>-->
                        <a class="btn btn-default btnlogin" href="<?= base_url() ?>owners/signup" role="button">SIGN UP</a>
                    </div>
                    <!-- End Search -->
                    <!-- End Search -->
                    <!-- Start Navigation List -->
                    <ul class="nav navbar-nav navbar-right">
                        <li> <a >Call Us: (917) 960-2520</a></li>             
                    </ul>
                    <!-- End Navigation List -->
                </div>
            </div>

            <!-- Mobile Menu Start -->
            <ul class="wpb-mobile-menu">
                <li><a>Call Us: (917) 960-2520</a></li>
                <!--<li><a href="index-01.html">LOGIN</a></li>-->
                <li><a href="#">SIGN UP</a></li></ul>
            <!-- Mobile Menu End -->

        </div>
        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <!-- End Header -->

            <!-- Start Content -->
            <div id="content">
                <div class="container bg_white">

                    <div class="col-lg-6 maininfo">

                        <h3 class="hdlabel"><?= $response[0]['google_place_result']->name ?></h3>
                        <div class="form-group itemlst">
                            <div class="col-sm-3 listitem">
                                <?php
                                $rating = round($response[0]['google_place_result']->rating);
                                if ($rating != '') {
                                    $non_rate = 5 - $rating;
                                    for ($i = 1; $i <= $rating; $i++) {
                                        ?>
                                        <li><img src="<?= base_url() ?>images/star.png"></li>
                                        <?php
                                    }
                                    for ($i = 1; $i <= $non_rate; $i++) {
                                        ?>
                                        <li><img src="<?= base_url() ?>images/star00.png"></li>
                                        <?php
                                    }
                                }
                                ?>



                            </div>
                            <label class="col-sm-9">
                                <?php if ($response[0]['google_place_result']->user_ratings_total != '') { ?>
                                    (<span><?= $response[0]['google_place_result']->user_ratings_total; ?></span>)
                                <?php } ?>
                            </label>
                        </div>
                        <div class="form-group itemlst">
                            <label class="col-sm-12 pad0">
                                <?= $response[0]['google_place_result']->formatted_address ?>
                            </label>
                        </div>
                        <div class="form-group itemlst">
                            <label class="col-sm-12 pad0">
                                <img src="<?= base_url() ?>images/phone.png"> <span>  <?= $response[0]['google_place_result']->international_phone_number ?></span>
                            </label>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="form-group itemlst">
                            <label class="col-sm-3 pad0 list_tp">Price Rating</label>
                            <div class="col-sm-4 listitem">
                                <?php
                                $price_rating = round($response[0]['google_place_result']->price_level);
                                $p_rate = 4 - $price_rating;
                                if ($price_rating != '') {
                                    for ($i = 1; $i <= $price_rating; $i++) {
                                        ?>
                                        <li><img src="<?= base_url() ?>images/dolor11.png"></li>
                                        <?php
                                    }
                                    for ($i = 1; $i <= $p_rate; $i++) {
                                        ?>
                                        <li><img src="<?= base_url() ?>images/dolor00.png"></li>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php if ($response[0]['google_place_result']->website) { ?>
                                <div class="col-sm-4 listitem">
                                    <a href=" <?= $response[0]['google_place_result']->website ?>" target="_blank">   <button class="btn btngray"><i><img src="<?= base_url() ?>images/globe.png"></i> VIEW WEBSITE</button></a>
                                </div><?php } ?>
                        </div>

                    </div>



                    <div class="col-lg-6 maininfo">
                        <div class="panel panel-default" style="border:none;">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title hoursbtn">
                                    HOURS
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body hourswp">
                                    <div class="">

                                        <?php
                                        foreach ($response[0]['google_place_result']->opening_hours->weekday_text as $weekday) {
                                            $week_array = explode(":", $weekday);
                                            $si_arr = sizeof($week_array);
                                            for ($i = 1; $i < $si_arr; $i++) {
                                                $time.=$week_array[$i];
                                                if ($si_arr - $i > 1) {
                                                    $time.=":";
                                                }
                                            }
                                            ?>  
                                            <div class="col-lg-4 col-sm-4 hourslst">
                                                <?= $week_array[0]; ?> : <br><span class="color_green"><?= $time; ?></span> 
                                            </div><?php
                                            unset($time);
                                        }
                                        ?>

                                        <div class="clearfix"></div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>




                    <div class="clearfix"></div>

                    <div class="col-lg-6 pading10">
                        <div class="slider">


                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top:0 !important;">
                                <!-- Indicators -->
                                <?php if (sizeof($response[0]['google_place_result']->photos) > 0) { ?>
                                    <ol class="carousel-indicators">
                                        <?php
                                        $i = 0;
                                        foreach ($response[0]['google_place_result']->photos as $img) {
                                            ?>
                                            <li data-target="#carousel-example-generic" data-slide-to="<?= $i; ?>" <?php if ($i == 0) { ?> class="active" <?php
                                            }$i++;
                                            ?>></li>
                                            <?php } ?>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <?php
                                        $i = 0;
                                        foreach ($response[0]['google_place_result']->photos as $img) {
                                            $image = $img->photo_reference;
                                            ?>
                                            <div class="item <?php if ($i == 0) { ?> active <?php
                                                $i++;
                                            }
                                            ?>">
                                                <img style="max-height: 300px; min-height: 300px; width: 100%" id='image_item' src='https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&maxheight=500&photoreference=<?= $image ?>&key=<?= $key1 ?>'  >  


                                            </div><?php } ?>

                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                <?php } else { ?>



                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top:0 !important;">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators"></ol>
                                        <div class="carousel-inner text-center" role="listbox">
                                            <img style="max-height: 300px; min-height: 300px; width: auto" id='image_item' src='<?= base_url() ?>images/no-image-slider.png'  >
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>


                        </div>
                    </div>

                    <div class="col-lg-6 pading10">
                        <div id="map_canvas"  style="max-height: 300px; min-height: 300px; width: 100%" class="col-lg-6">
                            <script>
                                function initialize() {
                                    var map_canvas = document.getElementById('map_canvas');
                                    var map_options = {
                                        center: new google.maps.LatLng(<?= $response[0]['google_place_result']->geometry->location->lat ?>, <?= $response[0]['google_place_result']->geometry->location->lng ?>),
                                        zoom: 18,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    var mapPin = " http://www.google.com/mapfiles/marker.png";
                                    var map = new google.maps.Map(map_canvas, map_options)
                                    var Marker = new google.maps.Marker({
                                        map: map,
                                        position: map.getCenter()
                                    });
                                }
                                google.maps.event.addDomListener(window, 'load', initialize);
                            </script>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <form action="<?= base_url() ?>owners/save_owner_hotel" id="form_sub" method="post">
                        <input type="hidden" name="google_id" value="<?= $response[0]['google_place_result']->place_id ?>">
                        <input type="hidden" name="locu" value="<?= $response[0]['locu_data']->locu_extend->venues[0]->locu_id ?>">
                        <input type="hidden" name="postal_code" value="<?= $response[0]['postal_code'] ?>">
                        <input type="hidden" name="formatted_address" value="<?= $response[0]['google_place_result']->formatted_address ?>">
                        <input type="hidden" name="rating" value="<?= $rating ?>">
                        <input type="hidden" name="res_name" value="<?= $response[0]['google_place_result']->name ?>">
                        <input type="hidden" name="lat" value="<?= $response[0]['google_place_result']->geometry->location->lat ?>">
                        <input type="hidden" name="lng" value="<?= $response[0]['google_place_result']->geometry->location->lng ?>">
                        <input type="hidden" name="pricing" value="<?= $price_rating ?>">

                        <div class="listwp">
                            <h4 class="labelhead"><i><img src="<?= base_url() ?>images/contact-ic.png" style="vertical-align:top;"> </i> CONTACT</h4>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">EMAIL :</label>
                                <div class="col-sm-12 listitem">
                                    <input type="email" onChange="check_email()" required placeholder="email" name="email" id="exampleInputNname" class="form-control email_owner inputsignup">
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">FAX :</label>
                                <div class="col-sm-12 listitem">
                                    <input type="text" name="FAX" placeholder="FAX" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                            $ph11 = str_replace("-", "", $response[0]['google_place_result']->international_phone_number);
                            $ph111 = str_replace(" ", "", $ph11);
                            ?>
                            <div class="form-group col-lg-6">
                                <label  class="labelitem">Contact Number :</label>
                                <div class="col-sm-12 listitem">
                                    <input type="text" name="contact_number" placeholder="Contact Number" id="exampleInputNname" value=" <?php
                                    echo $ph111;
                                    ?>" class="form-control ContactNumber inputsignup">
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">business owner :</label>
                                <div class="col-sm-12 listitem">
                                    <input type="text" required name="business owner"  placeholder="business owner" id="exampleInputNname" class="form-control business_owner inputsignup">
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">CUISINES :</label>
                                <div class="col-sm-12 listitem">
                                    <fieldset class="ui-field-contain">
                                        <select required name="cuisine[]" id="done" class="selectpicker" multiple data-done-button="true">
                                            <?php foreach ($cuisines as $cuis) { ?>
                                                <option value="<?= $cuis['cusine_id'] ?>"> <?= $cuis['cusine_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>
                                    <span class="erroe_msg cusine_error"></span>
                                </div>
                            </div>   


                            <div class="form-group col-lg-6">
                                <label  class="labelitem">Sales Tax Rate:</label>
                                <div class="col-sm-12 listitem">
                                    <input type="text" required name="tax"  placeholder="Sales Tax Rate" id="tax" class="form-control business_owner inputsignup percentage">
                                    <span class="erroe_msg cusine_error"></span>
                                </div>
                            </div>   
                            <script>
                                jQuery(document).ready(function ($) {
                                    $('.percentage').maskMoney({
                                        suffix: '%',
                                        precision: 0
                                    });
                                });
                            </script>

                            <div class="clearfix"></div>
                            <div class="col-lg-12" style="margin-top:15px;">
                                <div class="col-sm-12 listitem">
                                    <div class="radio">
                                        <input type="hidden" value="1" id="check_flag_bank">
                                        <div class="col-xs-4 pad0">

                                            <input name="account_type"  type="radio" checked="checked" style="font-size: 14px !important" id="ach" value="ACH" class="check_bank inputsignup">
                                            <label for="ach" class="labelitem">ACH</label>
                                        </div>


                                        <div class="col-xs-7 pad0">

                                            <input type="radio" name="account_type" id="check" value="check" class="check_bank inputsignup">
                                            <label for="check"  class="labelitem">Cheque Book</label>
                                        </div>
                                    </div>
                                </div></div>
                            <div class="clearfix"></div>
                            <script>
                                $(document).ready(function ()
                                {
                                    $('#ach').click(function ()
                                    {
                                        var $this = $(this);
                                        if ($this.is(':checked'))
                                        {
                                            $("#check_flag_bank").val('1');
                                            $("#bname").val('');
                                            $("#rnum").val('');
                                            $("#accnum").val('');
                                            $('#bname').attr('required', true);
                                            $('#rnum').attr('required', true);
                                            $('#accnum').attr('required', true);
                                            $('#chekkk').show();
                                            $('#check').attr('checked', false);
                                            $('#ach').attr('checked', true);
                                        } else {
                                            $("#check_flag_bank").val('0');
                                            $('#chekkk').hide();
                                            $('#bname').removeAttr('required');
                                            $('#rnum').removeAttr('required');
                                            $('#accnum').removeAttr('required');
                                            $('#check').attr('checked', true);
                                            $('#ach').attr('checked', false);
                                        }
                                    });



                                    $('#check').click(function ()
                                    {
                                        var $this = $(this);
                                        if ($this.is(':checked'))
                                        {
                                            $("#check_flag_bank").val('0');
                                            // alert("hi");
                                            $('#chekkk').hide();
                                            $('#bname').removeAttr('required');
                                            $('#rnum').removeAttr('required');
                                            $('#accnum').removeAttr('required');
                                            //  document.getElementById("bname").required = false;
                                            $('#ach').attr('checked', false);
                                            $('#check').attr('checked', true);
                                        } else {
//                                            alert("hi");
                                            //$('#chekkk').hide();
                                            $('#ach').attr('checked', true);
                                            $('#check').attr('checked', false);
                                            $("#bname").val('');
                                            $("#rnum").val('');
                                            $("#accnum").val('');
                                            $("#check_flag_bank").val('1');
//                                            document.getElementById("bname").required = true;
                                            $('#bname').attr('required', true);
                                            $('#rnum').attr('required', true);
                                            $('#accnum').attr('required', true);
//                                            $('#chekkk').show();
                                            $('#chekkk').show();
                                        }
                                    });
                                });
                            </script>
                            <div id="chekkk">
                                <div class="clearfix"></div>
                                
                                <!--6/24/2016-->
                                  <div class="form-group col-lg-6" >
                                    <label  class="labelitem">First name :</label>
                                    <div class="col-sm-12 listitem">
                                        <input type="text"  required name="first_name" placeholder="First Name" id="first_name" class="form-control  inputsignup">
                                    </div>
                                </div>



  <div class="form-group col-lg-6" >
                                    <label  class="labelitem">Last name :</label>
                                    <div class="col-sm-12 listitem">
                                        <input type="text"  required name="last_name" placeholder="Last Name" id="last_name" class="form-control  inputsignup">
                                    </div>
                                </div>


  <div class="form-group col-lg-6" >
                                    <label  class="labelitem">ssn :</label>
                                    <div class="col-sm-12 listitem">
                                        <input type="text"  required name="ssn" placeholder="ssn" id="ssn" class="form-control  inputsignup">
                                    </div>
                                </div>

                                <!--6/24/2016-->
                                <div class="form-group col-lg-6" >
                                    <label  class="labelitem">Bank name :</label>
                                    <div class="col-sm-12 listitem">
                                        <input type="text"  required name="bank_name" placeholder="bank name" id="bname" class="form-control bname inputsignup">
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">Routing Number :</label>
                                    <div class="col-sm-12 listitem">
                                        <input type="text" required name="RoutingNumber" placeholder="Routing Number" id="rnum" class="form-control rnum inputsignup">
                                    </div>
                                </div>
                               

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">Account Number :</label>
                                    <div class="col-sm-12 listitem">
                                        <input type="text" required name="AccountNumber" placeholder="Account Number" id="accnum" class="form-control accnum inputsignup">
                                    </div>
                                </div></div>

                            <div class="clearfix"></div>
                        </div>


                        <div class="listwp">
                            <h4 class="labelhead"><i><img src="<?= base_url() ?>images/delivery-ic.png"> </i> DELIVERY</h4>
                            <?php
                            $locu = $response[0]['locu_data'];
                            $extend = $response[0]['locu_data']->locu_extend->venues[0];
                            ?>

                            <div class="form-group col-lg-6" style="margin-bottom:10px;">
                                <label  class="labelitem col-sm-5">DELIVERY :</label>
                                <div class="col-sm-7 listitem">
                                    <div class="radio">
                                        <div class="col-xs-4 pad0">
                                            <input id="scttn" type="radio" <?php if ($extend->delivery->will_deliver == 1) { ?>  checked="checked"<?php } ?> name="delivery" value="yes">
                                            <label for="scttn">YES</label>
                                        </div>
                                        <div class="col-xs-4 pad0">
                                            <input id="scttn1" type="radio"  name="delivery" value="no">
                                            <label for="scttn1">NO</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem col-sm-5" style="padding-top:12px;">Minimum Order :</label>
                                <div class="col-sm-7 listitem">
                                    <input type="text" name="minimum_order" placeholder="$"  id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem col-xs-5" style="padding-top:12px;">areas :</label>
                                <div class="col-xs-4 listitem">
                                    <input type="number" name="areas" placeholder="miles" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                                <label  class="labelitem col-xs-3" style="padding-top:12px; padding-left:10px;">RADIUS</label>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem col-sm-5" style="padding-top:12px;">Delivery Fee :</label>
                                <div class="col-sm-7 listitem">
                                    <input type="text" name="delivery_fee" placeholder="$" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <label  class="labelitem col-sm-5" style="padding-top:12px;">Minimum Pickup Time :</label>
                                <div class="col-sm-7 listitem">
                                    <input type="text" name="min_pickUp_time" placeholder="Minimum Pickup Time" id="exampleInputNname" class="form-control min_pickUp_time inputsignup">
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <label  class="labelitem col-sm-5" style="padding-top:12px;">Minimum Delivery Time :</label>
                                <div class="col-sm-7 listitem">
                                    <input type="text" name="min_Delivery_time" placeholder="Minimum Delivery Time" id="exampleInputNname" class="form-control min_Delivery_time inputsignup">
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="listwp">
                            <h4 class="labelhead"><i><img src="<?= base_url() ?>images/contact-ic.png" style="vertical-align:top;"> </i> SALES PERSON</h4>
                            <div class="form-group col-lg-6">

                                <div class="form-group col-lg-10">
                                    <label  class="labelitem col-sm-5" style="padding-top:12px;">Sales Person:</label>
                                    <div class="col-sm-7 listitem">
                                        <input type="text" required name="sales_person" placeholder="Sales Person" id="exampleInputNname" class="form-control sales_prsn inputsignup">
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                        if ($feature_config['value'] == "Y") {
                            ?>
                            <div class="listwp">
                                <h4 class="labelhead"><i><img src="<?= base_url() ?>images/features-ic.png" style="vertical-align:top;"> </i> FEATURES</h4>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">alcohol  :</label>
                                    <div class="col-sm-12 listitem">
                                        <select name="Alcohol" class="form-control selectpicker">
                                            <option value="">Select one</option>
                                            <option <?php if ($extend->extended->alcohol == "beer_and_wine") { ?> selected <?php } ?>>beer and wine</option>
                                            <option <?php if ($extend->extended->alcohol == "full_bar") { ?> selected <?php } ?>>full bar</option>
                                            <option <?php if ($extend->extended->alcohol == "no_alcohol") { ?> selected <?php } ?>>no alcohol</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">parking :</label>
                                    <div class="col-sm-12 listitem">
                                        <select name="parking[]"  class="selectpicker" multiple data-done-button="true">

                                            <option value="">Select one</option>
                                            <?php foreach ($extend->extended->parking as $parking) { ?>
                                                <option <?php if ($parking == "garage") { ?> selected <?php } ?>>garage</option>
                                                <option <?php if ($parking == "lot") { ?> selected <?php } ?>>lot</option>
                                                <option <?php if ($parking == "street") { ?>  selected <?php } ?> >street</option>
                                                <option <?php if ($parking == "valet") { ?>  selected <?php } ?>>valet</option>
                                                <option <?php if ($parking == "validated") { ?>  selected <?php } ?>>validated</option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">WIFI  :</label>
                                    <div class="col-sm-12 listitem">
                                        <select name="wifi" class="form-control selectpicker">
                                            <option value="">Select one</option>
                                            <?php if ($extend->extended->wifi == "free") { ?>
                                                <option selected>free</option>
                                                <option>paid</option>
                                                <option>no</option>
                                            <?php } else if ($extend->extended->wifi == "paid") { ?>
                                                <option>free</option>
                                                <option selected>paid</option>
                                                <option>no</option>
                                            <?php } elseif ($extend->extended->wifi == "no") { ?>
                                                <option>free</option>
                                                <option>paid</option>
                                                <option selected>no</option>
                                            <?php } else { ?> <option>free</option>
                                                <option>paid</option>
                                                <option >no</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">corkage  :</label>
                                    <div class="col-sm-12 listitem">
                                        <select name="corkage" class="form-control selectpicker">
                                            <option value="">Select one</option>
                                            <?php if ($extend->extended->corkage == "free") { ?>
                                                <option selected>free</option>
                                                <option>paid</option>
                                                <option>no</option>
                                            <?php } elseif ($extend->extended->corkage == "paid") { ?>
                                                <option>free</option>
                                                <option selected>paid</option>
                                                <option>no</option>
                                            <?php } elseif ($extend->extended->corkage == "no") { ?>
                                                <option>free</option>
                                                <option>paid</option>
                                                <option selected>no</option>
                                            <?php } else { ?>
                                                <option>free</option>
                                                <option>paid</option>
                                                <option >no</option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">dietary restrictions  :</label>
                                    <div class="col-sm-12 listitem">
                                        <div class="styled" style="max-width:100%;">
                                            <select name="dietary_restrictions" class="form-control selectpicker">
                                                <option value="">Select one</option>
                                                <option <?php if ($extend->extended->dietary_restrictions == "vegetarian") {
                                                ?> selected <?php } ?>>vegetarian</option>
                                                <option <?php if ($extend->extended->dietary_restrictions == "vegan") {
                                                ?> selected <?php } ?>>vegan</option>
                                                <option  <?php if ($extend->extended->dietary_restrictions == "kosher") {
                                                ?> selected <?php } ?>>kosher</option>
                                                <option  <?php if ($extend->extended->dietary_restrictions == "halal") {
                                                ?> selected <?php } ?>>halal</option>
                                                <option   <?php if ($extend->extended->dietary_restrictions == "dairy-free") {
                                                ?> selected <?php } ?>>dairy free</option>
                                                <option  <?php if ($extend->extended->dietary_restrictions == "gluten-free") {
                                                ?> selected <?php } ?>>gluten free</option>
                                                <option <?php if ($extend->extended->dietary_restrictions == "soy-free") {
                                                ?> selected <?php } ?>>soy free</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">MUSIC  :</label>
                                    <div class="col-sm-12 listitem">
                                        <select name="music" class="form-control selectpicker">
                                            <option value="">Select one</option>
                                            <?php if ($extend->extended->music == "dj") { ?>
                                                <option selected>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background music</option>
                                                <option>karaoke</option>
                                                <option>no music</option>
                                            <?php } else if ($extend->extended->music == "live") { ?>
                                                <option>dj</option>
                                                <option selected>live</option>
                                                <option>jukebox</option>
                                                <option>background music</option>
                                                <option>karaoke</option>
                                                <option>no music</option>
                                            <?php } else if ($extend->extended->music == "jukebox") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option selected>jukebox</option>
                                                <option>background music</option>
                                                <option>karaoke</option>
                                                <option>no music</option>
                                            <?php } else if ($extend->extended->music == "background_music") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option selected>background music</option>
                                                <option>karaoke</option>
                                                <option>no music</option>
                                            <?php } else if ($extend->extended->music == "karaoke") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background music</option>
                                                <option selected>karaoke</option>
                                                <option>no music</option>
                                            <?php } elseif ($extend->extended->music == "no_music") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background music</option>
                                                <option>karaoke</option>
                                                <option selected>no music</option>
                                            <?php } else { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background music</option>
                                                <option>karaoke</option>
                                                <option >no music</option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">SPORTS  :</label>
                                    <div class="col-sm-12 listitem">
                                        <select name="sports" class="form-control selectpicker">
                                            <option value="">Select one</option>
                                            <option <?php
                                            if ($extend->extended->sports == "soccer") {
                                                $sp = 1;
                                                ?> selected <?php } ?>>soccer</option>
                                            <option <?php
                                            if ($extend->extended->sports == "basketball") {
                                                $sp = 1;
                                                ?> selected <?php } ?>>basketball</option>
                                            <option <?php
                                            if ($extend->extended->sports == "hockey") {
                                                $sp = 1;
                                                ?> selected <?php } ?>>hockey</option>
                                            <option <?php
                                            if ($extend->extended->sports == "football") {
                                                $sp = 1;
                                                ?> selected <?php } ?>>football</option>
                                            <option <?php
                                            if ($extend->extended->sports == "baseball") {
                                                $sp = 1;
                                                ?> selected <?php } ?>>baseball</option>
                                            <option <?php
                                            if ($extend->extended->sports == "mma") {
                                                ?> selected <?php } ?>>mma</option>

                                            <option    <?php if ($extend->extended->sports == "other") { ?> selected  <?php } ?>>other</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">wheelchair accessible :</label>
                                    <div class="col-sm-12 listitem">
                                        <select class="form-control selectpicker">
                                            <option value="">Select one</option>
                                            <option <?php if ($extend->extended->wheelchair_accessible == 1) { ?> selected <?php } ?>>YES</option>
                                            <option <?php if ($extend->extended->wheelchair_accessible == 0) { ?> selected <?php } ?>>NO</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">ambience :</label>
                                    <div class="col-sm-12 listitem">
                                        <?php
//                                                                        
                                        $ambience = '';
                                        foreach ($extend->extended->ambience as $amb) {
                                            if ($ambience == '') {
                                                $ambience.=$amb;
                                            } else {
                                                $ambience.="," . $amb;
                                            }
                                        }
                                        ?>
                                        <input type="text" name="ambience" placeholder="ambience" value="<?= $ambience ?>" id="exampleInputNname" class="form-control inputsignup">
                                    </div>
                                </div>

                                <div class="form-group col-lg-3">
                                    <label  class="labelitem">outdoor seating :</label>
                                    <div class="col-sm-12 listitem" style="padding-top:10px;">
                                        <div class="radio">
                                            <div class="col-xs-6 pad0">
                                                <input id="scttn2" type="radio" name="outdoor_seating"  <?php if ($extend->extended->outdoor_seating == 1) { ?>checked="checked" <?php } ?> value="yes">
                                                <label for="scttn2">YES</label>
                                            </div>
                                            <div class="col-xs-6 pad0">
                                                <input id="scttn3" type="radio" name="outdoor_seating"   value="no">
                                                <label for="scttn3">NO</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-lg-3">
                                    <label  class="labelitem">waiter service  :</label>
                                    <div class="col-sm-12 listitem" style="padding-top:10px;">
                                        <div class="radio">
                                            <div class="col-xs-6 pad0">
                                                <input id="scttn4" type="radio" <?php if ($extend->extended->waiter_service == 1) { ?> checked="checked" <?php } ?> name="waiter_service" value="yes">
                                                <label for="scttn4">YES</label>
                                            </div>
                                            <div class="col-xs-6 pad0">
                                                <input id="scttn5" type="radio" name="waiter_service" value="no">
                                                <label for="scttn5">NO</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">SMOKING  :</label>
                                    <div class="col-sm-12 listitem" style="padding-top:10px;">
                                        <div class="radio">
                                            <div class="col-xs-3 col-sm-3 pad0">
                                                <input id="scttn6" type="radio"  <?php if ($extend->extended->smoking == "yes") { ?> checked="checked" <?php } ?> name="smoking" value="yes">
                                                <label for="scttn6">YES</label>
                                            </div>
                                            <div class="col-xs-3 col-sm-3 pad0">
                                                <input id="scttn7" type="radio" name="smoking" value="no"  <?php if ($extend->extended->smoking == "no") { ?> checked="checked" <?php } ?> >
                                                <label for="scttn7">NO</label>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 pad0">
                                                <input id="scttn8" type="radio" <?php if ($extend->extended->smoking == "outdoor") { ?> checked="checked" <?php } ?> name="smoking" value="outdoor">
                                                <label for="scttn8">OUTDOOR</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label  class="labelitem">television  :</label>
                                    <div class="col-sm-12 listitem" style="padding-top:10px;">
                                        <div class="radio">
                                            <div class="col-xs-3 pad0">
                                                <input id="scttn9" type="radio" <?php if ($extend->extended->television == 1) { ?>  checked="checked" <?php } ?> name="television" value="yes">
                                                <label for="scttn9">YES</label>
                                            </div>
                                            <div class="col-xs-3 pad0">
                                                <input id="scttn10" type="radio" name="television" value="no">
                                                <label for="scttn10">NO</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="clearfix"></div>
                            </div>
                        <?php } ?>
                        <div class="col-lg-6 ">

                            <button type="button" id="sub_fn" class="btn btn-block btncontinue">CONFIRM</button>
                        </div>

                    </form>


                </div>
            </div>
            <!-- End content -->


            <!-- Start Footer -->

            <!-- End Footer -->


            <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>

            <script>
                                function validateEmail(email) {
                                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                    return emailReg.test(email);
                                }

                                function check_email() {
                                    var email = $(".email_owner").val();
//        alert(email);
                                    $.ajax({
                                        type: "post",
                                        url: "<?php echo base_url(); ?>owners/email_exist",
                                        data: {email: email},
                                        success: function (data) {
                                            if (data.indexOf('error') >= 0)
                                            {
                                                $(".email_owner").addClass('placeholder');
                                                $(".email_owner").val('');
                                                $('.email_owner').attr('placeholder', 'Your email id is already exist.');
                                                $(".email_owner").focus();
                                                return false;
                                                // alert("haiii");
                                            } else {
                                                return true;
                                            }


//                                                                alert(data);
                                        }
                                    });
                                }

                                $("#sub_fn").on('click', function () {
                                    var cusine = $("#done").val();
//                                    alert(cusine);
                                    var email = $(".email_owner").val();
                                    var tax = $("#tax").val();
                                    tax = tax.replace('%', '');
                                    tax = tax.replace(',', '');
                                    tax = parseFloat(tax);
//                                    alert(tax);
                                    var owner_name = $(".business_owner").val();
                                    var check = $("#check_flag_bank").val();
                                    var bank_name = $("#bname").val();
                                    var routnumber = $("#rnum").val();
									 var first_name = $("#first_name").val();
									 var last_name = $("#last_name").val();
									 var ssn = $("#ssn").val();
                                    var sales_person = $(".sales_prsn").val();
//                                    alert(sales_person);
//                                    var tax = $("#tax").val();
                                    var account_number = $("#accnum").val();
                                    var phone = $(".ContactNumber").val();
//                                    check_email(email);
                                    if (phone == '') {
                                        $(".ContactNumber").addClass('placeholder');
                                        $(".ContactNumber").val('');
                                        $('.ContactNumber').attr('placeholder', 'Please enter a valid contact number.');
                                        $(".ContactNumber").focus();
                                    }
                                    else if (email == '')
                                    {
                                        $(".email_owner").addClass('placeholder');
                                        $(".email_owner").val('');
                                        $('.email_owner').attr('placeholder', 'Please enter a valid email.');
                                        $(".email_owner").focus();
                                    }
                                    else if (!validateEmail(email))
                                    {
                                        $(".email_owner").addClass('placeholder');
                                        $(".email_owner").val('');
                                        $('.email_owner').attr('placeholder', 'Please enter a valid email.');
                                        $(".email_owner").focus();
                                    }
                                    else if (owner_name == '')
                                    {
                                        $(".business_owner").addClass('placeholder');
                                        $(".business_owner").val('');
                                        $('.business_owner').attr('placeholder', 'Please enter a valid owner name.');
                                        $(".business_owner").focus();
                                    } else if (tax > 100 || isNaN(tax)) {
//                                        alert("sds");
                                        $("#tax").addClass('placeholder');
                                        $("#tax").val('');
                                        $('#tax').attr('placeholder', 'tax value shold be less than 100.');
//                                        $("#tax").focus();

                                    }
                                    else if (cusine == '' || cusine == null)
                                    {
                                        $(".cusine_error").addClass('placeholder');
                                        $(".cusine_error").empty();
                                        $('.cusine_error').html("<p style='color:red'>Please select cuisine.</p>");
                                        $(".cusine_error").focus();
                                    } else if (sales_person == '') {
//                                        alert("sd");
                                        $(".sales_prsn").addClass('placeholder');
                                        $(".sales_prsn").val('');
                                        $('.sales_prsn').attr('placeholder', "Please enter a name.");
                                        $(".sales_prsn").focus();
                                    }
									 else if (check == 1 || check == '1') {
                                        $(".cusine_error").empty();
										
										if (first_name == '') {
                                            $("#first_name").addClass('placeholder');
                                            $("#first_name").val('');
                                            $('#first_name').attr('placeholder', 'Please enter a valid first name.');
                                            $("#first_name").focus();
                                        } 
										else if (last_name == '')
                                        {
                                            $("#last_name").addClass('placeholder');
                                            $("#last_name").val('');
                                            $('#last_name').attr('placeholder', 'Please enter a valid last name.');
                                            $("#last_name").focus();
                                        }
										else if (ssn == '')
                                        {
                                            $("#ssn").addClass('placeholder');
                                            $("#ssn").val('');
                                            $('#ssn').attr('placeholder', 'Please enter a valid ssn.');
                                            $("#ssn").focus();
                                        }
										
                                        else if (bank_name == '')
                                        {
                                            $("#bname").addClass('placeholder');
                                            $("#bname").val('');
                                            $('#bname').attr('placeholder', 'Please enter a valid bank name.');
                                            $("#bname").focus();
                                        }
                                        else if (routnumber == '') {
                                            $("#rnum").addClass('placeholder');
                                            $("#rnum").val('');
                                            $('#rnum').attr('placeholder', 'Please enter a valid routing number.');
                                            $("#rnum").focus();
                                        } else if (account_number == '') {
                                            $("#accnum").addClass('placeholder');
                                            $("#accnum").val('');
                                            $('#accnum').attr('placeholder', 'Please enter a valid account number.');
                                            $("#accnum").focus();
                                        } else {
                                            $("#form_sub").submit();
                                        }
                                    }
                                    else {
                                        $(".cusine_error").empty();
                                        $("#form_sub").submit();
                                    }
                                });
                                $(document).ready(function () {
                                    var mySelect = $('#first-disabled2');

                                    $('#special').on('click', function () {
                                        mySelect.find('option:selected').prop('disabled', true);
                                        mySelect.selectpicker('refresh');
                                    });

                                    $('#special2').on('click', function () {
                                        mySelect.find('option:disabled').prop('disabled', false);
                                        mySelect.selectpicker('refresh');
                                    });

                                    $('#basic2').selectpicker({
                                        liveSearch: true,
                                        maxOptions: 1
                                    });

                                });
            </script>
