<?php

session_start();

error_reporting(1);

class Member extends MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('Member_model', 'member_model');
        $this->load->model('client_model');
        $this->load->model('user_model');

        date_default_timezone_set('GMT');
    }

    function index() {
//        redirect("../index.php");
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->helper('form');
        redirect("member/signup");
//        $data['header'] = $this->load->view('admin/header', '', true);
//        $data['footer'] = $this->load->view('admin/footer', '', true);
    }

//    function faq() {
//        $data['faq'] = $this->member_model->faq();
//        $this->load->view("faq", $data);
//        $this->load->view("member_footer");
//    }

    function terms_conditions111() {
        $data['terms'] = $this->member_model->terms_of_use();
        $this->load->view("terms_condition", $data);
        $this->load->view("member_footer");
    }

    public function changepassword() {
        $data = $this->input->post();
        if ($data['password'] == $data['confirm_password']) {

            $check = array('password' => md5($data['password']));
            $admin_data = $this->owner_model->getOwnerDetails(array('email' => $data['email']));
            $result = $this->owner_model->updateOwner($check, $admin_data['restaurant_id']);
            if ($result != 'error') {
                delete_cookie($data['key']);
                echo 'success';
            } else {
                echo 'Unknown error occur';
            }
        } else {
            echo 'Password Mismatch';
        }
    }

    function change_subscription_details() {
        if (!$this->check()) {
            redirect('/member/login', 'refresh');
        }
        $this->load->model('member_model');
        $google_detail = $this->member_model->get_google_id();
        $key = $this->member_model->get_key();
        foreach ($google_detail as $sql_key => $res_data) {
            $google_data = $res_data['google_id'];
            $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_data&key=$key";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $uri);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response13 = json_decode($sData);
            $google_detail[$sql_key]['hotel_data'] = $response13->result;
            $google_detail[$sql_key]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
        }
        $data['states'] = $this->member_model->select_states();
        $data['key'] = $key;
        $data['rest_count'] = count($google_detail);
        $data['response_datas'] = $google_detail;
        $data['cuisines'] = $this->member_model->select_cuisine();
        $user_data = $this->session->userdata('member');
        $member_id = $user_data->member_id;
        $data['select_restaurents'] = $this->member_model->get_rest($member_id);
        $this->load->view("member/change_the_subscription", $data);
    }

    function signup($email = '', $zip = '') {

        $this->load->model('member_model');
        $google_detail = $this->member_model->get_google_id();
        $key = $this->member_model->get_key();
        foreach ($google_detail as $sql_key => $res_data) {
            $google_data = $res_data['google_id'];
            $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_data&key=$key";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $uri);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response13 = json_decode($sData);
            $google_detail[$sql_key]['hotel_data'] = $response13->result;
            $google_detail[$sql_key]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
        }
        $data['stripe_public_key'] = $this->member_model->get_stripe_public_config();
        $data['cusine_config'] = $this->member_model->get_cusine_config();
        $data['rating_config'] = $this->member_model->get_rating_config();
        $data['price_config'] = $this->member_model->get_price_config();
        $data['feature_config'] = $this->member_model->get_feature_config();
        $data['states'] = $this->member_model->select_states();
        $data['key'] = $key;
        $data['rest_count'] = count($google_detail);
        $data['response_datas'] = $google_detail;
        $data['cuisines'] = $this->member_model->select_cuisine();
        $data['email'] = $_POST['fild_email'];
        $data['zipcode'] = $_POST['zip_fild'];
//echo '<pre>';print_r($_POST);exit;
        $this->load->view("member_header");
        $this->load->view('signup', $data, true);
        $this->load->view("signup");
        $this->load->view("member_footer");
    }

    public function check_login() {

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->member_model->getMemberDetails(array('email' => $email));
        //echo md5($user['password']);exit;
        if (count($user) > 0) {
            if ($user['password'] === md5($password)) {

                $this->session->set_userdata('member', (object) $user);
                $result = array('status' => 'success', 'result' => 'valid');
                echo json_encode($result);
                exit;
            } else {
                $result = array('status' => 'error', 'result' => 'invalid');
                echo json_encode($result);
                //echo 'invalid';
                exit;
            }
        } else {
            $result = array('status' => 'error', 'result' => 'invalid');
            echo json_encode($result);
            //echo 'invalid';
            exit;
        }
    }

    public function check() {
        $user_data = $this->session->userdata('member');
        if ($user_data->member_id)
            return 1;
        else
            return 0;
    }

    function dashboard() {
        $user = $this->session->userdata('user');
        $data['restaurant_id'] = $user->member_id;
        if ($this->check()) {
            $data['site_name'] = 'Replimatic';
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('member/header', $data);
            $this->load->view('member/home', $data);
            $this->load->view('member/footer', $data);
        } else {
            redirect('/member/login', 'refresh');
        }
    }

    function subscription_details() {
        
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('welcome', 'refresh');
    }

    function login() {
        $this->load->view("member/header");
        $this->load->view("member/login");
        $this->load->view("member/footer");
    }

    function package() {
        $count = $_POST['count'];
//        $count = 2;
        $this->load->model('user_model');
        $get_data = $this->user_model->get_subscription_detail($count);
//        print_r($get_data);
//        exit;
        //$get_data['count_restaurants']=$arr['count'];
        if (sizeof($get_data) > 1) {
            $status = 'false';
        } else {
            if ($get_data[0]['number_of_restaurants'] != $arr['count']) {
                $status = 'false';
            } else {
                $status = 'true';
            }
        }
//        if($status=='true'){
        echo'    <div class="col-md-12 packagelabel">packages <span>$' . $get_data[0]['user_fee'] . '</span></div>
                                    <div class="cartwrap">
                                        <h6>Restaurants</h6>
                                        <ul>
                                            <li>Bella Pizza II & Restaurant</li>
                                            <li>Cafe De Novo</li>
                                        </ul>
                                    </div>
                                    <div class="carttotal">GRAND TOTAL  <span>$' . $get_data[0]['user_fee'] . '</span></div>';
//        }
    }

    function signup_test() {
        $this->load->view("signup_test");
    }

    function save_package_user() {
        $user_data = $this->session->userdata('member');
        $ins_id = $user_data->member_id;
        $package_arr = array("member_id" => $ins_id, "package_id" => $_POST['subscip_id']);
        $subscr_id = $this->member_model->save_package($package_arr);
        $res_arr = explode(",", $_POST['res_string_pc_c']);
        foreach ($res_arr as $rest) {
            $array_rest = array("member_id" => $ins_id, "restaurant_id" => $rest, "subscription_id" => $subscr_id, "create_date" => date("Y-m-d"));
            $this->member_model->save_res_user($array_rest);
        }
        redirect("member/update_success");
    }

    function update_success() {
        $this->load->view("member/update_success");
    }

    function save_user() {
        if ($_POST['fullname'] != '') {
            $first_name = $_POST['fullname'];
        } else {
            $first_name = $_POST['fname'];
        }

        $arr = array("first_name" => $first_name, "billing_status" => "Y", "gender" => $_POST['gender'], "facebook_id" => $_POST['fb_id'], "last_name" => $_POST['lname'], "zipcode" => $_POST['zip_code'], "email" => $_POST['email'], "status" => "Y", 'password' => md5($_POST['psd']));
        $ins_id = $this->member_model->ins_mem($arr);
        $_SESSION['reg_user_id'] = $ins_id;
        $this->db->insert("member_stripid_map", array("member_id" => $ins_id, "save_card" => $_POST['save_card'], "amount" => $_POST['price'], "brand" => $_POST['brand'], "created_time" => date("Y-m-d H:i:s"), "strip_order_id" => $_POST['tocken'], "last_4digit" => $_POST['last_4'], "strip_customer_id" => $_POST['card_resp']));
        $strip_id = $this->db->insert_id();
        $email = $this->member_model->get_email("User_Registration");
        $user_id = $ins_id;
        $user = $this->member_model->get_user($user_id);
        $package_arr = array("member_id" => $ins_id, "package_id" => $_POST['subscip_id'], "strip_id" => $strip_id);
        $billing_addr = array("name" => $first_name . $user['last_name'], "save_as" => $_POST['save_addr'], "member_id" => $user_id, "stripeid" => $strip_id, "address" => $_POST['biill_addr1'] . $_POST['bill_addr2'], "pincode" => $_POST['zipcode'], "state" => $_POST['state'], 'city' => $_POST['bill_city']);
        $del_arr = array("name" => $first_name . $user['last_name'], "save_as" => $_POST['rsave_addr'], "member_id" => $user_id, "stripeid" => $strip_id, "address" => $_POST['del_addr1'] . $_POST['del_addr2'], "pincode" => $_POST['del_zip'], "state" => $_POST['del_state'], 'city' => $_POST['del_city']);
        $res_arr = explode(",", $_POST['res_string_pc_c']);
        $subscr_id = $this->member_model->save_package($package_arr);
        foreach ($res_arr as $rest) {
            $array_rest = array("member_id" => $ins_id, "strip_id" => $strip_id, "restaurant_id" => $rest, "subscription_id" => $subscr_id, "create_date" => date("Y-m-d"));
            $this->member_model->save_res_user($array_rest);
        }

        $name = $first_name . " " . $user['last_name'];

        $get_package_details = $this->user_model->get_package_details($_POST['subscip_id']);
        $User_subscription_log = array("member_id" => $ins_id,
            "start_date" => date('Y-m-d'),
            "End_date" => Date('Y-m-d', strtotime("+" . $get_package_details['durn'])),
            "subscription_id" => $subscr_id,
            "strip_id" => $strip_id,
        );
        $this->user_model->insert_1('subscription_log', $User_subscription_log);
        $this->member_model->save_addr($billing_addr);
        if ($_POST['rsave_addr'] != $_POST['save_addr']) {
            $this->member_model->save_addr($del_arr);
        }
        unset($_SESSION['reg_user_id']);
        $data_1 = $this->member_model->admin_contratct();
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['crlf'] = PHP_EOL;
        $config['newline'] = PHP_EOL;
        $message1 = $email['email_template'];
        $subject = $email['email_subject'];
        $message1 = str_replace('#baseurl#', base_url(), $message1);
        $message1 = str_replace('#name#', $name, $message1);
        $message1 = str_replace('#YEAR#', date('Y'), $message1);

//            echo $message1;
//            exit;
//            $message1 = "Hi " . $name . ",<br/>Yor Login Credentials are following : <br/>User Name : " . $email . "<br/> Password: " . $password;
        $this->load->library('email');
        $this->email->initialize($config);
        $pjt_name = $data_1['project_name'];
        $this->email->from($data_1['email'], $pjt_name);
        $this->email->to($_POST['email']);
//            $this->email->to($this->config->item('email_from'));
        $this->email->subject($subject);
        $this->email->message($message1);
        $this->email->send();


        redirect("member/member_success");

//        echo '<pre>';
//        print_r($_POST);
    }

    function member_success() {
        $this->load->view("confirm");
    }

    function get_subscription_detail_web() {
        $count = $_POST['count'];
//        $count = 4;
        $uri = base_url() . "client/get_subscription_detail_web/" . $count;
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $uri);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response13 = json_decode($sData);
//        echo '<pre>';
//        print_r($response13);
//        exit;
        if ($response13->status == 'true') {
            $res_name = '';
            $key = $this->member_model->get_key();
            $res_string = $_POST['res_string'];
            $res_string_arr = explode(",", $res_string);
            foreach ($res_string_arr as $res_id) {
                $google_id = $this->member_model->get_restaurent($res_id);
                $google_data = $google_id['google_id'];
                $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_data&key=$key";
//                
                $ch1 = curl_init();
                curl_setopt($ch1, CURLOPT_URL, $uri);
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
                $sData = curl_exec($ch1);
                $response1 = json_decode($sData);
                $res_name = $res_name . "<li>" . $response1->result->name . "</li>";
            }
            $datas = $response13->data[0];
//            print_r($datas);
            echo $response13->status . '$$$$#####&&&***    <div class="col-md-12 packagelabel">' . $datas->package_name . ' <span>$' . $datas->user_fee . '</span></div>
                                    <div class="cartwrap">
                                        <h6>Restaurants</h6>
                                        <ul>
                                          ' . $res_name . '
                                        </ul>
                                    </div>
                                    <div class="carttotal">GRAND TOTAL  <span>$' . $datas->user_fee . '</span></div>$$$$#####&&&***' . $datas->sub_package_id;
        } elseif ($response13->status == 'false') {
            $datas = $response13->data;
            $package = '';
            foreach ($datas as $pack_data) {
                $package = $package . '<li style="text-align:center; font-size:18px; margin-bottom:10px;">' . $pack_data->package_name . "  $" . $pack_data->user_fee . "</li>";
            }
            echo $response13->status . '$$$$#####&&&*** <p style="text-align:center; margin-bottom:20px;">There is no packages for your selection ! . So kindly choose another one
            Suggestion</p>
                <h6  style="font-size:20px; border-bottom:1px solid #f2f2f2; padding-bottom:5px;text-align:center; padding-bottom:15px; margin-bottom:10px;">Packages</h6>
                <ul >
                    ' . $package . '
                </ul>';
        }
    }

    function cus_rest_check() {
        $cus_array = $_POST['cus_select'];
        $fea_array = $_POST['fea_array'];
        $search_key = $_POST['search_key'];
        $sort_type = $_POST['sort_type'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $key = $this->member_model->get_key();
        $google_detail = $this->member_model->get_google_id_filter($cus_array, $fea_array, $search_key, $sort_type, $latitude, $longitude);
        $rate_max = $_POST['change_rate'];
        $price_max = $_POST['change_price'];
        $filter_resp_array = array();
        $ii = 0;
        foreach ($google_detail as $res_data) {
            $google_data = $res_data['google_id'];
            $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_data&key=$key";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $uri);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response13 = json_decode($sData);
            $res_rate = $response13->result->rating;
            $res_price = $response13->result->price_level;
            if ($res_rate == 0 && $res_price == 0) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            } elseif ($res_rate >= $rate_max && $res_price >= $price_max) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            } elseif ($res_rate >= $rate_max && $res_price == 0) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            } elseif ($res_rate == 0 && $res_price >= $price_max) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            } elseif ($rate_max == 0 && $price_max == 0) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            } elseif ($res_rate >= $rate_max && $price_max == 0) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            } elseif ($rate_max == 0 && $res_price >= $price_max) {
                $filter_resp_array[$ii]['delivery'] = $res_data['delivery'];
                $filter_resp_array[$ii]['delivery_fee'] = $res_data['delivery_fee'];
                $filter_resp_array[$ii]['restaurant_id'] = $res_data['restaurant_id'];
                $filter_resp_array[$ii]['hotel_data'] = $response13->result;
                $filter_resp_array[$ii]['cuisines'] = $this->member_model->get_cuisine_res($res_data['restaurant_id']);
                $ii++;
            }
        }
        $sel_id_arr = explode(",", $_POST['selected_hotel']);
        $res_sel_count = 0;
        $sel_res_id_str = '';
        foreach ($filter_resp_array as $hotel) {
            $rating = round($hotel['hotel_data']->rating);
            $rate_differ = 5 - $rating;
            $image = $hotel['hotel_data']->photos[0]->photo_reference;
            $price_rating = round($hotel['hotel_data']->price_level);
            $p_rate = 4 - $price_rating;
            echo '  <div style="cursor: auto" class="result-wrapper assign_wrapper_pc"  id="rest_id' . $hotel['hotel_data']->place_id . '" >';
            if (in_array($hotel['restaurant_id'], $sel_id_arr)) {
                echo '<div class="select-wrapperover sel_pc_flag"></div>
                   <input type="hidden" value="select" class="sel_val">
                         ';
                $res_sel_count++;
                if ($sel_res_id_str == 0) {
                    $sel_res_id_str = $hotel['restaurant_id'];
                } else {
                    $sel_res_id_str = $sel_res_id_str . ',' . $hotel['restaurant_id'];
                }
            } else {
                echo ' <input type="hidden" value="unselect" class="sel_val">';
            }
            echo '                  <input type="hidden" value="' . $hotel['hotel_data']->place_id . '" name="res_id" id="res_id_pc" class="res_id_pc">
                                   
                                          <input type="hidden" class="sel_res_this" value="' . $hotel['restaurant_id'] . '">
  <div class="col-md-2 text-center"><img src="https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&photoreference=' . $image . '&key=' . $key . '" width="150" height="150" alt="" class="thumb100"/></div>                                  

                                    <div class="col-md-5 restaurantinfo">
                                        <h5>' . $hotel['hotel_data']->name . '</h5>';
            $ccccc123 = '';
            foreach ($hotel['cuisines'] as $cusine) {
                if (strlen($ccccc123) <= 25) {
                    if ($ccccc123 == '') {
                        echo $cusine['cusine_name'];
                    } else {
                        echo ',' . $cusine['cusine_name'];
                    }
                    $ccccc123.=$cusine['cusine_name'];
                } else {
                    echo '.....';
                    break;
                }
            }

            echo '  <p style="font-weight:400"><i class="fa fa-map-marker"></i> ' . $hotel['hotel_data']->formatted_address . '</p>                 
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-8">
                       
                   <p class="middle">
                         ';
            if ($rating != '') {
                $non_rate = 5 - $rating;
                for ($i = 1; $i <= $rating; $i++) {
                    echo '         <img src="' . base_url() . 'images/star.png">';
                }
                for ($i = 1; $i <= $non_rate; $i++) {
                    echo '     <img src="' . base_url() . 'images/star00.png">';
                }
            }
            echo' <br>' . $hotel['hotel_data']->user_ratings_total . ' ratings
                        </p>
                        

                     <p class="middle">';

            if ($price_rating != '') {
                for ($i = 1; $i <= $price_rating; $i++) {

                    echo'        <img src="' . base_url() . 'images/dollar1.png">';
                }
                for ($i = 1; $i <= $p_rate; $i++) {

                    echo '<img src="' . base_url() . 'images/dollar0.png">';
                }
            }

            echo ' </p>


</div>
                    <div class="col-md-2"><p class="middle deleverystat">';
            if ($hotel['delivery'] == 'yes' && ($hotel['delivery_fee'] == 0 || $hotel['delivery_fee'] == '' || $hotel['delivery_fee'] == '0')) {

                echo 'FREE<br><span>Delivery</span>';
            } elseif ($hotel['delivery'] == 'yes') {

                echo '$' . $hotel['delivery_fee'] . '<br><span> Delivery Fee</span>';
            } else {
                echo '      NO   <br><span>Delivery </span>';
            }


            echo '</p></div>
                    </div>
                    <div class="clearfix"></div></div>';
        }
        echo '  <input type="hidden" id="sel_pc_id_res"  value="' . $sel_res_id_str . '">
                  <input type="hidden" id="sel_pc_val_res" value="' . $res_sel_count . '">
				  <input type="hidden" id="count_ajax" value="' . count($filter_resp_array) . '">
        ';
    }

    function checkUserexist_with_fb() {
        $this->load->model('Member_model');
        $member_id = $this->Member_model->validateFacebookId($_POST['facebook_id']);
        $userdet1 = $this->Member_model->checkEmailExist($_POST['email_address']);
#check whether the user already registerd
        if ($member_id) { # if the user already connected to facebook
            echo "user exist";
        } else if ($userdet1) {
            $member_id = $this->Member_model->updateFacebookId($userdet1['member_id'], $_POST['facebook_id']);
            echo "user exist";
        } else {
            echo "user not exist";
// redirect("home/registration_thanks");
        }
    }

    public function reset_password($new_password) {
        $cookie = get_cookie($new_password);

        if ($cookie) {
            $data['email'] = $cookie;
            $data['key'] = $new_password;
            //delete_cookie($new_password);
            //$output['site_name']	= getConfigValue('site_name_front');
            $data['site_name'] = 'Fulspoon';
            $data['title'] = 'Password Reset';
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('owner/static_header', $data);
            $this->load->view('owner/resetPassword', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect(base_url() . 'owner/home', 'refresh');
        }
    }

    public function forgotpwd() {
        $this->load->model('owner_model');
        $this->load->model('email_model');
        $email_id = $_POST['email'];
        $data = $this->member_model->checkEmailExist($email_id);
        $new_password = $this->owner_model->randomPassword();
//        echo $new_password;
//        exit;
        $this->load->model('email_model');
        $this->load->helper('cookie');
        $cookie = array(
            'name' => $new_password,
            'value' => $email_id,
            'expire' => time() + 3600
        );

        if (count($data) > 0) {
            $email = $this->email_model->get_email_template('forgot_password_admins');

            $this->load->library('encrypt');
            $full_name = $data['full_name'];
            $target_name = $this->config->item('site_name');
            $link = base_url() . 'member/reset_password/' . $new_password;
                $data_1 = $this->member_model->admin_contratct();
            $subject = $email['email_subject'];
            $message = $email['email_template'];
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html; charset=utf-8" . "\r\n";
            $headers .= 'From: ' . $data_1['project_name'].'<'.$data_1['email'] . ">\r\n";

            $message = $email['email_template'];
            $message = str_replace('#MemberName#', $full_name, $message);
            $message = str_replace('#LINK#', $link, $message);
            $message = str_replace('#subject#', $subject, $message);
            $message = str_replace('#baseurl#', base_url(), $message);

            if (@mail($email_id, $subject, $message, $headers)) {
                echo "success";
                set_cookie($cookie);
            } else
                echo "There is error in sending mail!";
        }
        else {
            echo "Please enter valid email id!";
        }
    }

    function privacy_policy() {
        $data['terms'] = $this->member_model->privacy_policy();
        $this->load->view("privacy_policy", $data);
        $this->load->view("member_footer");
    }

    function terms_conditions() {
        $data['terms'] = $this->member_model->terms_of_use();
        $this->load->view("terms_condition", $data);
        $this->load->view("member_footer");
    }

    function checkUserexist() {
        $this->load->model('Member_model');
        $userdet1 = $this->Member_model->checkEmailExist($_POST['email_address']);
        if ($userdet1) {
            echo "user exist";
        } else {
            echo "user not exist";
        }
    }

    function confirm_signup($str_id) {
        if ($str_id != '') {
            $this->db->insert("member_stripid_map", array("member_id" => $_SESSION['reg_user_id'], "strip_customer_id" => $str_id));
            $this->db->update("member_master", array("status" => "Y"), array("member_id" => $_SESSION['reg_user_id']));
            $this->load->view("confirm");
        } else {
            
        }
    }

    function get_restaurantDetails() {
        $this->load->model('member_model');
        $google_detail = $this->member_model->get_google_id();
        $key = $this->member_model->get_key();
        foreach ($google_detail as $sql_key => $res_data) {
            $google_data = $res_data['google_id'];
            $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_data&key=$key";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $uri);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response13 = json_decode($sData);
            $google_detail[$sql_key]['hotel_data'] = $response13->result;
        }
        $data['response_datas'] = $google_detail;
        $this->load->view('signup', $data, true);
    }

    function contact_us() {
        $data_1 = $this->member_model->admin_contratct();
//        print_r($data_1);
        $address = urlencode($data_1['address']);
//        echo $address;
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response_a = json_decode($sData);
//        print_r($response_a);
//        exit;
        $data['contact'] = $data_1;
        $data['lat'] = $response_a->results[0]->geometry->location->lat;
        $data['long'] = $response_a->results[0]->geometry->location->lng;
        $this->load->view("contact_us", $data);
        $this->load->view("footer_owner");
    }

    function contact() {
        $data_1 = $this->member_model->admin_contratct();
//        print_r($data_1);
        $address = urlencode($data_1['address']);
//        echo $address;
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response_a = json_decode($sData);
        $data['contact'] = $data_1;
        $data['lat'] = $response_a->results[0]->geometry->location->lat;
        $data['long'] = $response_a->results[0]->geometry->location->lng;
        $this->load->view("heder_sub", $data);
        $this->load->view("footermember");
    }

    function faq() {
        $data['faq'] = $this->member_model->faq();
//        print_r($data);
        $this->load->view("faq_1", $data);
        $this->load->view("footermember");
    }

    function terms_condition() {
        $data['terms'] = $this->member_model->terms_of_use();
        $this->load->view("terms_condition_1", $data);
        $this->load->view("footermember");
    }

}

?>