<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class payment_model extends MY_Model {

    public $_table = 'preferences';

    public function setTable($table) {
        $this->_table = $table;
    }

    function get_existing_customers($res_id) {


        $sql = "SELECT *,COUNT(pre_rest) AS existing_sub,COUNT(pre_subscription) FROM (SELECT *,(SELECT T3.restaurant_id 
								  FROM current_restaurant T3 
								  WHERE T3.restaurant_id='$res_id'
								  AND T3.subscription_id=pre_subscription) AS pre_rest  FROM 
				  (SELECT * FROM (SELECT T1.*,(SELECT T2.subscription_id 
					  FROM User_subscription_master T2 
					  WHERE T1.subscription_id>T2.subscription_id
						   AND T2.member_id=T1.member_id
						   LIMIT 1) AS pre_subscription FROM 
				  current_restaurant T1 
				  WHERE T1.restaurant_id='$res_id' ORDER BY T1.current_id DESC) as tewst GROUP BY member_id DESC  )AS pre_sub) as ytew
				";


        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function get_new_customers($res_id) {


        $sql = "SELECT COUNT(log_id) as new_sub FROM (SELECT log_id FROM (SELECT * FROM
					(SELECT * FROM 
					 current_restaurant
					 WHERE restaurant_id='$res_id'
					 ORDER BY current_id DESC) AS res 
				  GROUP BY member_id ) T1 LEFT JOIN subscription_log T2
				  ON T2.subscription_id=T1.subscription_id
				  WHERE End_date>=CURDATE() AND start_date <=CURDATE() GROUP BY T2.member_id) AS tab

				";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function order_report($res_id) {
        //'2016-04-16' to now()

        $sql = "SELECT  COUNT(T1.created_time) as count_order,SUM(T1.total_amount) as total_amount,
		          DATE_ADD(FIRST_DAY_OF_WEEK(now()), INTERVAL 7 DAY) AS due_on  
				  FROM order_master T1 
				  LEFT JOIN member_stripid_map T2 ON T1.order_id=T2.order_id 
				  WHERE T1.restaurant_id='$res_id'  AND T1.created_time>=FIRST_DAY_OF_WEEK(now()) AND T1.order_status!='Declined'
	
				";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function merchants_details($date, $pay_method = '', $status = '', $res_id) {


        $sql = "select T1.recipient_id,T1.amount,T1.recipient_response_id,T1.create_date,T1.restaurant_id,T1.payment_type as account_type,T2.restaurant_name,(T1.new_subscribers + T1.existing_subscribers) AS restaurant_count
				  from merchant_payment T1
				  LEFT JOIN  restaurant_master T2 ON T2.restaurant_id=T1.restaurant_id 
				  where T1.type='subscription' AND T1.create_date>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1') AND T1.create_date<=SUBDATE(CURDATE(),INTERVAL 1 DAY)
						   ";
        if ($pay_method != '') {
            $sql.="  and T1.payment_type='$pay_method'";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function merchants_details_ajx($date, $pay_method = '', $status = '', $res_id) {


        $sql = "select T1.recipient_id,T1.amount,T1.recipient_response_id,T1.create_date,T1.restaurant_id,T1.payment_type as account_type,T2.restaurant_name,(T1.new_subscribers + T1.existing_subscribers) AS restaurant_count
				  from merchant_payment T1
				  LEFT JOIN  restaurant_master T2 ON T2.restaurant_id=T1.restaurant_id 
				  where T1.type='subscription' AND T1.create_date>=DATE_FORMAT(LAST_DAY('$date' - INTERVAL 1 MONTH), '%Y-%m-1') AND T1.create_date<=SUBDATE('$date',INTERVAL 1 DAY)
						   ";
        if ($pay_method != '') {
            $sql.="  and T1.payment_type='$pay_method'";
        }
        $sql.=" GROUP BY T1.restaurant_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function merchants_details_crone() {


        $sql = "SELECT DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1') as first,SUBDATE(CURDATE(),INTERVAL 1 DAY) as last,T3.account_type,T3.restaurant_id,T3.restaurant_name,T1.*,COUNT(T2.restaurant_id) As count_restaurant 
					  FROM subscription_log T1 
					  LEFT JOIN current_restaurant T2 ON T1.subscription_id=T2.subscription_id
					  LEFT JOIN restaurant_master T3 ON T2.restaurant_id=T3.restaurant_id
					  WHERE  T1.created_on>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1') 
					  AND T1.created_on<=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND T3.restaurant_name!='' GROUP BY T2.restaurant_id
						   ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function merchants_details_copy($date, $pay_method = '', $status = '', $res_id) {


        $sql = "select T1.recipient_id,T1.amount,T1.recipient_response_id,T1.create_date,T1.restaurant_id,T1.payment_type as account_type,T2.restaurant_name,(T1.new_subscribers + T1.existing_subscribers) AS restaurant_count
				  from merchant_payment T1
				  LEFT JOIN  restaurant_master T2 ON T2.restaurant_id=T1.restaurant_id 
				  where T1.type='subscription' AND T1.create_date>='$date' AND T1.create_date<=LAST_DAY('$date')
						   ";
        if ($pay_method != '') {
            $sql.="  and T1.payment_type='$pay_method'";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getFirstDayCount() {

        $sql = "SELECT count(*) as count,T3.member_id,T3.start_date,T1.restaurant_id,T2.subscription_id,T3.subscription_id FROM 
					  restaurant_master T1 
					  LEFT JOIN current_restaurant T2 ON T1.restaurant_id=T2.restaurant_id
					  LEFT JOIN subscription_log T3 ON T2.subscription_id=T3.subscription_id
					  WHERE T2.subscription_id!=''  AND
					  T3.start_date>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1')
					  AND T3.End_date>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1')   GROUP BY T1.restaurant_id ORDER BY restaurant_id desc
				";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getCurrentMonth_Count($rest_id) {



        $sql = "SELECT COUNT(T2.restaurant_id) As count_restaurant 
					  FROM subscription_log T1 
					  LEFT JOIN current_restaurant T2 ON T1.subscription_id=T2.subscription_id
					  LEFT JOIN restaurant_master T3 ON T2.restaurant_id=T3.restaurant_id
					  WHERE  T1.created_on>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1') 
					  AND T1.created_on<=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND T2.restaurant_id='$rest_id' GROUP BY T2.restaurant_id
				";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getCurrentMonth_Amount($rest_id) {



        $sql = "SELECT COUNT(T1.restaurant_id) AS count_order,SUM(T1.total_amount) as restaurant_amount
					  FROM `order_master` T1 
					  LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id 
					  WHERE T1.created_time>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1') 
					  AND T1.created_time<=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND T1.order_status!='Declined' AND  T1.order_status!='Canceled'
					  AND    T1.restaurant_id='$rest_id'
					  GROUP BY T1.restaurant_id
				";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getLastDayCount($restaurant_id) {

        $sql = "SELECT count(*) as count,T3.member_id,T3.start_date,T1.restaurant_id,T2.subscription_id,T3.subscription_id FROM 
						restaurant_master T1 
						LEFT JOIN current_restaurant T2 ON T1.restaurant_id=T2.restaurant_id
						LEFT JOIN subscription_log T3 ON T2.subscription_id=T3.subscription_id
						WHERE T2.subscription_id!=''  AND
						T3.start_date<=SUBDATE(CURDATE(),INTERVAL 1 DAY) 
						AND T3.End_date>=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND  T1.restaurant_id ='$restaurant_id' GROUP BY T1.restaurant_id 
				";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function merchant_payment() {


        /* 	  $sql = "SELECT FIRST_DAY_OF_WEEK(SUBDATE(CURDATE(),INTERVAL 1 DAY)) as mon,SUBDATE(CURDATE(),INTERVAL 1 DAY) as sat,COUNT(T1.restaurant_id) AS count_order,T2.restaurant_name,T1.restaurant_id,SUM(T1.total_amount) as restaurant_amount
          FROM `order_master` T1
          LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id
          WHERE T1.created_time>=FIRST_DAY_OF_WEEK(SUBDATE(CURDATE(),INTERVAL 1 DAY))
          AND T1.created_time<=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND T1.order_status!='Declined' AND  T1.order_status!='Canceled'
          AND T1.merchant_payment='N'
          GROUP BY T1.restaurant_id
          "; */

        // 	  $sql = "SELECT FIRST_DAY_OF_WEEK(SUBDATE(CURDATE(),INTERVAL 1 DAY)) as mon,SUBDATE(CURDATE(),INTERVAL 1 DAY) as sat,COUNT(T1.restaurant_id) AS count_order,T2.restaurant_name,T1.restaurant_id,SUM(T1.total_amount) as restaurant_amount
        // 	  FROM `order_master` T1 
        // 	  LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id 
        // 	  WHERE T1.created_time>=FIRST_DAY_OF_WEEK(SUBDATE(CURDATE(),INTERVAL 1 DAY)) 
        // 	  AND T1.created_time<=CURDATE() AND T1.order_status!='Declined' AND  T1.order_status!='Canceled'
        // 	  AND T1.merchant_payment='N'
        // 	  GROUP BY T1.restaurant_id
        // "; 

        $sql = "SELECT FIRST_DAY_OF_WEEK(SUBDATE(CURDATE(),INTERVAL 1 DAY)) as mon,SUBDATE(CURDATE(),INTERVAL 1 DAY) as sat,COUNT(T1.restaurant_id) AS count_order,T2.restaurant_name,T1.restaurant_id,SUM(T1.total_amount -(((T1.total_amount * 3)/100) )) as tot,SUM(((T1.total_amount * 3)/100)) as proce,SUM(T1.total_amount) as restaurant_amount
					  FROM `order_master` T1 
					  LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id 
					  WHERE T1.created_time>=SUBDATE(CONCAT(CURDATE(),' 00:00:00') ,INTERVAL 9 DAY)
					  AND T1.created_time<=SUBDATE(CONCAT(CURDATE(),' 23:59:59'),INTERVAL 3 DAY) AND T1.order_status!='Declined' AND  T1.order_status!='Canceled'
					  AND T1.merchant_payment='N'
					  GROUP BY T1.restaurant_id";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function merchant_payment_check() {


        $sql = "SELECT COUNT(T1.restaurant_id) AS count_order,T2.restaurant_name,T1.restaurant_id,SUM(T1.total_amount) as restaurant_amount
					  FROM `order_master` T1 
					  LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id 
					  WHERE T1.created_time>=FIRST_DAY_OF_WEEK(SUBDATE(CURDATE(),INTERVAL 1 DAY)) 
					  AND T1.created_time<=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND T1.order_status!='Declined' AND  T1.order_status!='Canceled'
					  AND T1.merchant_payment='N'
					  GROUP BY T1.restaurant_id
				";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function merchant_payment_copy() {


        $sql = "SELECT T2.restaurant_name,T1.restaurant_id,SUM(T1.total_amount) as restaurant_amount
					  FROM `order_master` T1 
					  LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id 
					  WHERE T1.created_time>=FIRST_DAY_OF_WEEK(NOW()) 
					  AND T1.created_time<=NOW() AND T1.order_status!='Declined' AND  T1.order_status!='Canceled'
					  AND T1.merchant_payment!='N'
					  GROUP BY T1.restaurant_id
				";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function restaurent_details_recipient($res_id) {
        $sql = "SELECT * from restaurant_master where restaurant_id='$res_id'";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function order_master_update_payment_status($res_id) {
        $arr = array('merchant_payment' => 'Y');
        $cond = array('restaurant_id' => $res_id);
        $this->db->update("order_master", $arr, $cond);
    }

    function insert_payment_details($arr) {
        return $this->db->insert('merchant_payment', $arr);
    }

    function merchants_order($res_id, $date = '') {
        $sql = "select 
					  T1.payment_type as account_type,T1.create_date,T1.order_count,T1.processing_fee,T1.restaurant_id,T1.payment_type,T2.restaurant_name,T1.amount
					  from merchant_payment T1
					  LEFT JOIN  restaurant_master T2 ON T2.restaurant_id=T1.restaurant_id 
					  where T1.type='order'  ";
        if ($date == '') {
            $sql.=" AND T1.create_date<=FIRST_DAY_OF_WEEK(now())";
        }


        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function merchants_prev_month_report($resid){


   $sql = " SELECT T1.member_id 
					  FROM subscription_log T1 
					  LEFT JOIN current_restaurant T2 ON T1.subscription_id=T2.subscription_id
					  LEFT JOIN restaurant_master T3 ON T2.restaurant_id=T3.restaurant_id
					  WHERE  T1.created_on>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 2 MONTH), '%Y-%m-1') 
					  AND T1.created_on<=SUBDATE(SUBDATE(CURDATE(),INTERVAL 1 MONTH),INTERVAL 1 DAY) AND T3.restaurant_name!='' and T3.restaurant_id ='$resid' GROUP BY T1.member_id
				";

        $query = $this->db->query($sql);
        return $query->result_array();

       

    }
    function merchants_current_month_report($resid){
          $sql = " SELECT T1.member_id 
					  FROM subscription_log T1 
					  LEFT JOIN current_restaurant T2 ON T1.subscription_id=T2.subscription_id
					  LEFT JOIN restaurant_master T3 ON T2.restaurant_id=T3.restaurant_id
					  WHERE  T1.created_on>=DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-1') 
					  AND T1.created_on<=SUBDATE(CURDATE(),INTERVAL 1 DAY) AND T3.restaurant_name!='' and T3.restaurant_id ='$resid' GROUP BY T1.member_id
				";

        $query = $this->db->query($sql);
        return $query->result_array();

      
    }

}

?>
	