<?php

error_reporting(1);
ini_set("display_errors", "on");

class User_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        date_default_timezone_set('GMT');
    }

    # function to check whether the email already created by the user

    function checkEmailExist($email) {
        $sql = "select * from member_master where email = '$email'   ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function update_twilio_order($arr, $cond) {
        return $this->db->update("order_master", $arr, $cond);
    }

    function get_dec_twilio_data() {
        $data = $this->db->query("select order_id from order_master where twilioflag='Y'");
        return $data->result_array();
    }

    function getGa() {
        $data = $this->db->query("select value from general_config where field='GA_code'");
        return $data->row_array();
    }

    function get_userdetails($id) {

        $sql = "select * from member_master where member_id = '$id'";

        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function update_randomPassword_code($rand, $member_id) {
        $data = array(
            'rand_password_code' => $rand
        );

        $this->db->where('member_id', $member_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function get_time_rest($id) {
        $data = $this->db->query("select pickup_time,delivery_time,minimum_order from restaurant_master where restaurant_id='$id'");
        return $data->row_array();
    }

    function getAdminEmail($id) {
        $sql = "SELECT email FROM `admin_master`";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function check_ref_code($pass) {
        $data = $this->db->query("select count(*) as count from order_master where order_ref_id='$pass'");
        $count = $data->row_array();
        return $count['count'];
    }

    function get_order_details($order_id) {
        $items = $this->db->query("select T3.contact_number,T1.*,T2.* from  order_master T1 
		                           left join member_stripid_map T2 on T1.stripe_id=T2.id 
								   LEFT JOIN restaurant_master T3 ON T1.restaurant_id=T3.restaurant_id
								   where T1.order_id='$order_id'");
        return $items->row_array();
    }

    function update_ref($array, $condition) {
        return $this->db->update("order_master", $array, $condition);
    }

    function get_past_order_details($user_id, $order_id) {
        $items = $this->db->query("select T1.*,T2.dish_name,T2.price as current_price,T2.menu as category_id, T3.menu_name as category_name  from  order_items T1 left join dish_master T2 on T1.item_id=T2.id left join menu_rest_master T3 on T2.menu=T3.id  where T1.order_id='$order_id'");
        $order_items = $items->result_array();
//        echo $this->db->last_query();
        foreach ($order_items as $key => $ord_item) {
            $side_data = $this->db->query("select T1.*,T3.mandatory,T3.option_name,T3.price from order_option_map T1  left join dish_options T3 on T3.option_id=T1.option_id where  T1.ord_item_id='" . $ord_item['ord_item_id'] . "'");
            $side_array = $side_data->result_array();
            foreach ($side_array as $key1 => $side) {
                $side_array[$key1]['sides'] = unserialize($side['sides']);
            }
            $order_items[$key]['side_array'] = $side_array;

            $size_data = $this->db->query("SELECT price,size,map_id
				FROM dish_item_size_map WHERE item_id = '" . $ord_item['item_id'] . "'
				ORDER BY price ASC ");
//            echo $this->db->last_query();
            $order_items[$key]['size_array'] = $size_data->result_array();
        }
        return $order_items;
    }

    function get_Admin_detail() {
        $sql = "SELECT stripe_public_key,google_id FROM `admin_master`";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function facebookExist($facebookExist) {

        $sql = "select * from member_master where facebook_id = '$facebookExist'   ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    # function to create user

    function createUser($arr) {
        $data = array(
            //'email_address' => $arr['email'],
            //'password' => $arr['password'],
            'DOB' => $arr['DOB'],
            'Fname' => $arr['Fname'],
            'Lname' => $arr['Lname'],
            'parents_name' => $arr['Pname'],
            'member_type' => $arr['member_type'],
            'phone' => $arr['phone'],
            'contact_info' => $arr['address'],
                //'device_token' => $arr['device_token'],
        );

        $this->db->insert('member_master', $data);
        $user_id = $this->db->insert_id();
        return $user_id;
    }

    function updateMemberImage($img_extension, $user_id) {
        $data = array(
            'profile_image' => $img_extension
        );

        $this->db->where('member_id', $user_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function updateBilling_status($detail) {
        $sql1 = "select * from member_master where member_id = '" . $detail['member_id'] . "' ";
        $query1 = $this->db->query($sql1);
        $result = $query1->row_array();
        if ($result['state'] != '') {
            $data = array(
                'billing_status' => 'Y',
                //'zipcode' => $detail['zipcode'],
//			'state' => $detail['state'],iiii
//			'city' => $detail['city'],
                'phone' => $detail['phone'],
            );
        } else {
            $data = array(
                'billing_status' => 'Y',
                'zipcode' => $detail['zipcode'],
                'state' => $detail['state'],
                'city' => $detail['city'],
                'phone' => $detail['phone'],
            );
        }


        $this->db->where('member_id', $detail['member_id']);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function edit_profile_user($detail) {
        $data = array(
            'zipcode' => $detail['zipcode'],
            'state' => $detail['state'],
            'city' => $detail['city'],
            'phone' => $detail['phone'],
            'first_name' => $detail['first_name'],
            'email' => $detail['email'],
        );

        $this->db->where('member_id', $detail['member_id']);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    #Login function

    function userLogin($arr) {
        $email = $arr['email'];
        $password = md5($arr['password']);
        $sql1 = "select * from member_master where email = '$email' and password = '$password' and status='Y' ";
        $query1 = $this->db->query($sql1);
        $result = $query1->row_array();
        return $result;
    }

    function facebookExist_email($data) {
        $sql1 = "select * from member_master where email = '" . $data['email'] . "'";
        $query1 = $this->db->query($sql1);
        $result = $query1->row_array();

        if ($result['member_id']) {
            $this->updatefb_id($result['member_id'], $data['id']);
        }
        return $result;
    }

    function updatefb_id($user_id, $id) {
        $data = array(
            'facebook_id' => $id
        );
        $this->db->where('member_id', $user_id);
        $id = $this->db->update('member_master', $data);
        return;
    }

    function get_subscription_detail($count) {
        $sql1 = "select * from subscription_package where number_of_restaurants='$count' and active='Y' GROUP BY number_of_restaurants LIMIT 1";
        $query1 = $this->db->query($sql1);
        $result = $query1->result_array();
        if ($result) {
            return $result;
            exit;
        } else {
            $sql2 = " SELECT * FROM subscription_package WHERE number_of_restaurants=(select number_of_restaurants from subscription_package 
				where number_of_restaurants>$count and active='Y' GROUP BY number_of_restaurants 
				ORDER BY number_of_restaurants ASC LIMIT 1) OR number_of_restaurants =(select number_of_restaurants from subscription_package 
				where number_of_restaurants<$count and active='Y' GROUP BY number_of_restaurants 
				ORDER BY number_of_restaurants DESC LIMIT 1) GROUP BY number_of_restaurants";
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            return $result2;
            exit;
        }
    }

#######################	

    public function getOwnerStripDetails($data) {
        if (getLocationConfigValue('19', $data['location_id']) != '')
            $stripe_public_key = getLocationConfigValue('19', $data['location_id']);
        else
            $stripe_public_key = $this->config->item('stripe_public_key');

        if (getLocationConfigValue('20', $data['location_id']) != '')
            $stripe_private_key = getLocationConfigValue('20', $data['location_id']);
        else
            $stripe_private_key = $this->config->item('stripe_private_key');

        return array('stripe_public_key' => $stripe_public_key, 'stripe_private_key' => $stripe_private_key);
    }

    public function get_where($table, $data, $order_by = "id ASC", $type = "row") {

        $query = $this->db->order_by($order_by)->get_where($table, $data);


        if ($type == "row") {
            return $query->row_array();
        } else {

            return $query->result_array();
        }
    }

    public function getCustomerStripId($data) {
        $this->db->order_by("created_time", "desc");
        $this->db->where(array('member_id' => $data['member_id'], 'restaurant_id' => $data['restaurant_id'], 'location_id' => $data['location_id']));
        $query = $this->db->get('order_stripid_map');
        return $query->result_array();
    }

    public function insert_1($table, $data) {
        //print_r($data);exit;
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function get_rest1($id) {
        $query = $this->db->query("select changed_method from order_master where order_id='$id'");
        $data = $query->row_array();
        return $data['changed_method'];
    }

    function get_rest($id) {
        $query = $this->db->query("select restaurant_name from restaurant_master where restaurant_id='$id'");
        $data = $query->row_array();
        return $data['restaurant_name'];
    }

    function get_rest_all($id) {
        $query = $this->db->query("select * from restaurant_master where restaurant_id='$id'");
        $data = $query->row_array();
        return $data;
    }

    function get_discount_user_amount($member_id) {
        $sql = "SELECT SUM(discount_amount) as total_amount,(SELECT SUM(discount_amount) FROM `order_master` WHERE member_id='$member_id' AND order_status!='Cancelled' AND order_status!='Declined' and  created_time >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) as month_amount FROM `order_master` WHERE member_id='$member_id' AND order_status!='Cancelled' AND order_status!='Declined'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function get_discountAndTotal_amount($member_id) {
        $sql = "SELECT SUM(discount_amount) as discount_amount,(SUM(total_amount) +(SELECT SUM(amount) FROM member_stripid_map WHERE member_id='$member_id' AND restaurant_id=0)) as total_amount FROM `order_master` WHERE member_id='$member_id' AND    order_status!='Cancelled' AND order_status!='Declined'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    public function updatePromoCount($id, $promocode, $restaurant_id) {
        $this->db->where(array('promocode' => $promocode, 'restaurant_id' => $restaurant_id));
        $result = $this->db->get('promo_order_map');
        $count = $result->num_rows();

        $this->db->where('promo_id', $id);
        $this->db->update('promocodes', array('total_uses' => $count));
    }

    function get_restaurant_nonChain_withUser($arr) {
        $miles = $arr['admin_miles'];
        $latitude = $arr['lat'];
        $longitude = $arr['long'];
        $user_id = $arr['uid'];
        $dd = $this->db->query("SELECT T1.*,( 3959 * acos( cos( radians($latitude) ) * cos( radians(lat) ) * cos( radians(`long`) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) ) AS distance from restaurant_master T1 WHERE ( SELECT count(T2.id) from chain_rest_child T2 WHERE T2.rest_id= T1.restaurant_id )=0 and ( 3959 * acos( cos( radians($latitude) ) * cos( radians(lat) ) * cos( radians(`long`) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) )<=$miles and T1.status ='Y' AND T1.signed_flag='Y' AND T1.chain_flag='N' GROUP BY T1.google_id order by distance asc, T1.restaurant_id desc");
        return $dd->result_array();
    }

    function get_restaurant_nonChain($arr) {
        $miles = $arr['admin_miles'];
        $latitude = $arr['lat'];
        $longitude = $arr['long'];
        $dd = $this->db->query("SELECT T1.*,( 3959 * acos( cos( radians($latitude) ) * cos( radians(lat) ) * cos( radians(`long`) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) ) AS distance from restaurant_master T1 WHERE ( SELECT count(T2.id) from chain_rest_child T2 WHERE T2.rest_id= T1.restaurant_id )=0 and ( 3959 * acos( cos( radians($latitude) ) * cos( radians(lat) ) * cos( radians(`long`) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) )<=$miles and T1.status ='Y' AND T1.signed_flag='Y' AND T1.chain_flag='N' GROUP BY T1.google_id order by distance asc, T1.restaurant_id desc");
        return $dd->result_array();
    }

    function get_restaurant_nonChain_nonLoc($arr) {
        $dd = $this->db->query("SELECT T1.* from restaurant_master T1 WHERE   T1.status ='Y' AND T1.signed_flag='Y' AND T1.chain_flag='N' GROUP BY T1.google_id order by  T1.restaurant_id desc");
        return $dd->result_array();
    }

    function get_restChainMaster() {
        $dd = $this->db->query("select T1.* from chain_master T2 LEFT JOIN restaurant_master T1  on T2.managed_restaurent=T1.restaurant_id where T1.status ='Y' AND T1.signed_flag='Y'");
        return $dd->result_array();
    }

    function get_restaurant_detail($data) {

//	$miles=$this->config->item('stripe_private_key');
        $miles = $data['admin_miles'];
        $latitude = $data['lat'];
        $longitude = $data['long'];
//        $latitude="10.0718";
//        $longitude="76.5488";
        if ($latitude != '') {
            $sql = "select *,( 3959 * acos( cos( radians($latitude) ) * cos( radians(lat) ) * cos( radians(`long`) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) ) AS distance from restaurant_master where status ='Y' AND signed_flag='Y' and ( 3959 * acos( cos( radians($latitude) ) * cos( radians(lat) ) * cos( radians(`long`) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) )<=$miles  GROUP BY google_id order by distance asc, restaurant_id desc";
        } else {
            $sql = "select * from restaurant_master where status ='Y' AND signed_flag='Y' GROUP BY google_id order by restaurant_id desc";
        }
        $query = $this->db->query($sql);
//        echo $this->db->last_query();
        $result = $query->result_array();
        return $result;
    }

    function config_filter() {
        $sql = "SELECT `value`,field  FROM general_config WHERE field='cuisine_filter' or field='rating_filter' OR field='Price_filter' OR field='feature_filter'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_restaurant_cuisine_detail($id) {
        $sql = "SELECT T2.cusine_name as cuisine_name FROM `restauarnt_cusine_map` T1 LEFT JOIN cusine_master T2 ON T1.cusine_id=T2.cusine_id WHERE T1.rest_id='$id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_user_restaurant_detail($user_id) {
        $sql = "SELECT  SP.user_fee,SL.End_date,SP.number_of_restaurants,RM.* FROM subscription_log SL 
					   LEFT JOIN User_subscription_master US ON SL.subscription_id=US.subscription_id
					   LEFT JOIN current_restaurant CR ON CR.subscription_id=US.subscription_id  ";
        // AND CR.strip_id=SL.strip_id
        $sql.= " LEFT JOIN restaurant_master RM ON RM.restaurant_id=CR.restaurant_id 
				        
                       LEFT JOIN subscription_package SP ON SP.sub_package_id=US.package_id
					   WHERE SL.End_date>=DATE(NOW()) AND SL.member_id='$user_id' AND RM.status='Y' and RM.expire_flag='N' ORDER BY SL.log_id,CR.current_id DESC ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        for ($i = 0; $i < $result[0]['number_of_restaurants']; $i++) {
            $result_1[$i] = $result[$i];
        }

        return $result_1;
    }

    function find_chain_id($res_id) {
        $chain = $this->db->query("select chain_id,count(id) as count from chain_rest_child where rest_id='$res_id'");
        $result = $chain->row_array();
//        echo $this->db->last_query();
        if ($result['count'] == 0) {
            return 'N';
        } else {
            return $result['chain_id'];
        }
    }

    function checkContactNumber($user_id) {
        $data = $this->db->query("select phone,divice_type,push_notification,device_tocken,device_id from member_master where member_id='$user_id'");
        return $data->row_array();
    }

    function find_nearest_id($chain_id, $latitude, $longitude, $adminMiles) {
        $data = $this->db->query("select T2.rest_id,T1.restaurant_id,( 3959 * acos( cos( radians($latitude) ) * cos( radians( T1.lat ) ) * cos( radians(T1.long) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( T1.lat ) ) ) ) AS distance  from chain_rest_child T2 left join restaurant_master T1 on T2.rest_id=T1.restaurant_id where T2.chain_id='$chain_id' and T1.status='Y' and T1.signed_flag='Y' order by distance asc limit 1");
        $return = $data->row_array();
//        echo $this->db->last_query()."<br/>";
        return $return['restaurant_id'];
    }

    function getUserOfflineLoc($user_id) {
        $dat = $this->db->query("select zipcode,city,state,lat,`long` from member master where member_id='$id'");
        return $dat->row_array();
    }

    function get_user_restaurant_detail_ChainFeature($user_id, $lat, $long, $admin_miles) {
//        $sub = $this->db->query("select T1.number_of_restaurants from subscription_package T1 left join User_subscription_master T2 on T1.sub_package_id=T2.package_id where T2.member_id='$user_id' order by T2.subscription_id desc limit 1");

        $return = $this->db->query("SELECT  SP.user_fee,SL.End_date,SP.number_of_restaurants FROM subscription_log SL LEFT JOIN User_subscription_master US ON SL.subscription_id=US.subscription_id
     LEFT JOIN subscription_package SP ON SP.sub_package_id=US.package_id    WHERE SL.End_date>=DATE(NOW()) AND SL.member_id='" . $user_id . "'  ORDER BY SL.log_id DESC ");
        $restDetails = $return->row_array();
//        echo $this->db->last_query() . "<br/>";
//        print_r($restDetails);
        $number_of_rest = $restDetails['number_of_restaurants'];
        if (!$number_of_rest) {
            $number_of_rest = 0;
        }
        $sub = $this->db->query("select T1.restaurant_id from current_restaurant T1 left join User_subscription_master T2 on T1.subscription_id=T2.subscription_id where T2.member_id='$user_id' order by T2.subscription_id desc limit $number_of_rest");
        $curr_rest = $sub->result_array();
//        echo $this->db->last_query() . "<br/>";
//        exit;
        $rww = 0;
        foreach ($curr_rest as $key => $restau) {
            $rest_id = $restau['restaurant_id'];
            $chain_id = $this->find_chain_id($rest_id);
//            echo $chain_id."<br/>";
//            exit;
            $restArray["subDetails"] = $restDetails;
            if ($chain_id == 'N') {
                $rte = $this->db->query("select * from restaurant_master where restaurant_id='$rest_id'");
                $restArray['restDet'] = $rte->row_array();
            } else {
                $nearest_chain_res_id = $this->find_nearest_id($chain_id, $lat, $long, $admin_miles);
                $rte = $this->db->query("select * from restaurant_master where restaurant_id='$nearest_chain_res_id'");
                $restArray['restDet'] = $rte->row_array();
//                print_r($restArray);
//                exit;
//                echo $this->db->last_query();
            }
//            $curr_rest[$key]['restaurant_details'] = $restArray;
            $result[$rww] = $restArray;
            $rww++;
//            echo'<pre>';
//            print_r($result);
//            echo "value " . $number_of_rest . " cou " . $rww;
        }
//        exit;
        for ($i = 0; $i < $number_of_rest; $i++) {
            $merge = array_merge($result[$i]['subDetails'], $result[$i]['restDet']);
//            print_r($merge);
            $result_1[$i] = $merge;
        }
        return $result_1;
    }

    function get_full_cuisine_detail($id) {
        $sql = "SELECT cusine_name as cuisine_name FROM cusine_master WHERE active='Y'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function check_Psdcode($code) {
        $sql = "SELECT * FROM `member_master` WHERE rand_password_code='$code'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function update_new_password($member_id, $password) {
        $data = array(
            'password' => md5($password)
        );
        $this->db->where('member_id', $member_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function update_phone($phone, $user_id) {
        $data = array(
            'phone' => $phone
        );
        $this->db->where('member_id', $user_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function profile_image($img, $user_id) {
        $data = array(
            'profile_image' => $img
        );
        $this->db->where('member_id', $user_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function edit_address($address, $id) {
        $this->db->where('ad_id', $id);
        $id = $this->db->update('address', $address);
        return true;
    }

    function delete_address($id) {
        $this->db->where('ad_id', $id);
        $this->db->delete('address');
        return true;
    }

    function get_restaurant_food_detail($restaurant_id) {
        $sql = "SELECT * FROM  restaurant_master WHERE restaurant_id='$restaurant_id'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function get_menu_detail($restaurant_id) {
        $sql = "SELECT * FROM `menu_rest_master` WHERE res_id='$restaurant_id' and status='Y'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_menu_detail_new($restaurant_id) {
        $sql = "SELECT * FROM `menu_rest_master` WHERE res_id='$restaurant_id' and status='Y'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_package_details($package_id) {
        $sql = "SELECT *,CONCAT(duration,' ',frequency) as durn FROM `subscription_package` WHERE sub_package_id='$package_id'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function user_details_log($member_id) {
        $sql = "SELECT count(*) as count FROM `subscription_log` WHERE member_id='member_id' AND End_date>=DATE(NOW())";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function get_dish_detail($menu_id, $restaurant_id) {




        $sql = " SELECT * FROM (SELECT dc.id as cate_id,dc.menu_name,dc.res_id,
di.id,di.description,di.dish_name,di.sortorder as dish_sort,
(SELECT MIN(price) as price from dish_item_size_map where item_id=di.id) as price
FROM dish_master di
INNER JOIN menu_rest_master dc  
ON(dc.id = di.menu)
WHERE dc.res_id='$restaurant_id'
AND dc.id='$menu_id'
AND dc.status = 'Y' 
AND di.status='Y' 

ORDER BY dish_sort) as test WHERE price<>''";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function getRestaurantMenuDetail($id) {
        $res = array();

        if ($id) {

            $sql = "SELECT dish_master.*,dish_options.option_id,dish_options.option_name,dish_options.limit,
				dish_options.sortorder,dish_options.mandatory,dish_options.multiple,option_sides.side_id,
				option_sides.side_item,option_sides.sortorder as sidesortorder,option_sides.price as side_price		
				FROM dish_master
				LEFT JOIN  dish_options
				ON dish_master.id=dish_options.dish_item_id and dish_options.status!='N'
				LEFT JOIN  option_sides
				ON option_sides.option_id = dish_options.option_id
				INNER JOIN menu_rest_master
				ON dish_master.menu = menu_rest_master.id

				WHERE dish_master.id=$id and menu_rest_master.`status`='Y'
				and dish_master.`status`='Y'
				ORDER BY dish_options.sortorder,sidesortorder
			";

            $query = $this->db->query($sql);
            $res = $query->result_array();


            return $res;
        }
    }

    function get_config1() {
        $data = $this->db->query("select * from general_config where field='tiwlo_account_sid'");
        $return['sid'] = $data->row_array();
        $data1 = $this->db->query("select * from general_config where field='tiwlo_auth_token' ");
        $return['authtocken'] = $data1->row_array();
        $data1 = $this->db->query("select * from general_config where field='Twilo_phone_number' ");
        $return['phonenumber'] = $data1->row_array();
        return $return;
    }

    function get_owner_phone($id) {
        $data = $this->db->query("select T1.item_id,T2.contact_number from order_items T1 left join dish_master T3 on T1.item_id=T3.id left join restaurant_master T2 on T3.rid=T2.restaurant_id where T1.ord_item_id='$id' ");
        $phone = $data->row_array();
        return $phone['contact_number'];
    }

    function get_order_twilo_data($id) {
        $data = $this->db->query("select T1.order_type,T1.delivery_time,T1.order_ref_id, T2.first_name,T1.restaurant_id from order_master T1 left join member_master T2 on T1.member_id=T2.member_id where T1.order_id='" . $id . "'");
        $return = $data->row_array();
        $ord_data = $this->db->query("select T1.quantity,T2.dish_name from order_items T1 left join dish_master T2 on T1.item_id=T2.id where order_id=$id");
        $return['order_data'] = $ord_data->result_array();
        $res_id = $return['restaurant_id'];
        $time = $this->db->query("select timezone from restaurant_master where restaurant_id='$res_id'");
        $ti = $time->row_array();
        $timezone = $ti['timezone'];
        $return['timezone'] = $timezone;
        return $return;
    }

    public function getRestaurantMenuSizeDetail($id) {
        $sql = "SELECT price,size FROM dish_item_size_map WHERE item_id = $id ORDER BY price ASC";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function getUser_strip_details($id) {
        $sql = "SELECT id FROM `member_stripid_map` WHERE member_id='$id' ORDER BY id DESC LIMIT 1";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    public function get_discount_detail($id) {
        $sql = "  SELECT T3.discount,T2.* FROM subscription_log T1 LEFT JOIN User_subscription_master T2 ON T1.subscription_id=T2.subscription_id 
        LEFT JOIN subscription_package T3 ON T2.package_id=T3.sub_package_id WHERE T1.member_id='$id' ORDER BY log_id DESC LIMIT 1";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    public function get_address_user($member_id) {
        $sql = " SELECT * FROM address WHERE member_id='$member_id' LIMIT 1 ";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    function get_order_address($id) {
        $qry = $this->db->query("select T2.* from delivery_address T1 left join address T2 on T1.address_id=T2.ad_id where T1.order_id='$id' ");
//        echo $this->db->last_query();
        return $qry->row_array();
    }

    function get_ord_rest_subscr_details($user_id, $order_id) {
        $subscr_data = $this->db->query("select T2.package_id,T2.subscription_id from subscription_log T1 LEFT JOIN User_subscription_master T2 ON T1.subscription_id=T2.subscription_id where T1.member_id='$user_id' order by T1.log_id desc limit 1");
        $subscr = $subscr_data->row_array();
//        $package_id = $subscr['package_id'];
        $sub_id = $subscr['subscription_id'];
        $rest_id = $this->get_rest_from_ord($order_id);
        foreach ($rest_id as $key => $res) {
            $fla = $this->db->query("select count(*) as count from current_restaurant where member_id='$user_id' and restaurant_id='" . $res['rid'] . "' and subscription_id='" . $sub_id . "'");
            $count = $fla->row_array();
            if ($count > 0) {
                $rest_id[$key]['subscr_flag'] = "Y";
            } else {
                $rest_id[$key]['subscr_flag'] = "N";
            }
        }
        $return['restaurant_details'] = $rest_id;
        $return['subscription'] = $subscr;
        return $return;
    }

    function get_rest_from_ord($order_id) {
        $data = $this->db->query("select T2.rid,T3.* from dish_master T2 left join order_items T1 on T1.item_id=T2.id left join restaurant_master T3 on T3.restaurant_id=T2.rid where T1.order_id='$order_id'");
        return $data->result_array();
    }

    public function get_alladdress_user($member_id) {
        $sql = " SELECT * FROM address WHERE member_id='$member_id'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function get_user_carddetails($member_id) {
        $sql = "SELECT (T1.`name`) AS name_on_card,(T2.ad_id) as adds_id,T2.*,T1.last_4digit,T1.brand,T1.id,T1.easy_name FROM `member_stripid_map` T1 
								 LEFT JOIN address T2 
								 ON  T1.id=T2.stripeid
								 WHERE T1.member_id='$member_id' AND T1.delete_status='N'  ORDER BY T1.id DESC LIMIT 1";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    public function get_user_carddetails_all($member_id) {
        $sql = "SELECT *,(T1.`name`) AS name_on_card,(T2.ad_id) as adds_id,T2.*,T1.last_4digit,T1.brand,T1.id,T1.easy_name FROM `member_stripid_map` T1 
								 LEFT JOIN address T2 
								 ON  T1.id=T2.stripeid
								 WHERE T1.member_id='$member_id' AND T1.delete_status='N'  AND T1.restaurant_id='0'";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function get_strip_detail_fromstrip_id($id) {
        $sql = "SELECT * FROM `member_stripid_map` WHERE id='$id'";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    function get_user_name($uid) {
        $data = $this->db->query("select first_name,last_name from member_master where member_id='$uid'");
        $user = $data->row_array();
        return $user['first_name'] . " " . $user['last_name'];
    }

    public function get_package_id($id) {
        $sql = "SELECT T1.subscription_id FROM `subscription_log` T1 WHERE End_date>=CURRENT_DATE() AND T1.member_id='$id'";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    function edit_user_cardaddress($data, $id) {
        $this->db->where('ad_id', $id);
        $id = $this->db->update('address', $data);
        return true;
    }

    function delete_user_cardaddress($data, $id) {
        $this->db->where('id', $id);
        $id = $this->db->update('member_stripid_map', $data);
        return true;
    }

    public function get_cms_datas_privacy() {
        $sql = "SELECT Answer FROM cms_master WHERE `status`='Y' AND type='privacy_policy'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_cms_datas_terms() {
        $sql = "SELECT Answer FROM cms_master WHERE `status`='Y' AND type='terms_of_use' ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_cms_datas_faq() {
        $sql = "SELECT Answer FROM cms_master WHERE `status`='Y' AND type='faq' ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_cms_datas_help() {
        $sql = "SELECT Answer FROM cms_master WHERE `status`='Y' AND type='help' ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function update_newPassword($password, $member_id) {
        $data = array(
            'password' => $password
        );

        $this->db->where('member_id', $member_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    public function check_old_psd($password, $member_id) {
        $sql = "SELECT * FROM member_master WHERE password='$password' AND member_id='$member_id'";
        $result = $this->db->query($sql);
        $result_i = $result->row();
        return $result_i;
    }

    public function getAllOders($member_id, $restaurant_id = '', $limit = '') {
        $sql = "SELECT DISTINCT
		
					(CONVERT_TZ(OM.created_time,'+00:00',rl.time_zone)) as  created_time,
				    (CONVERT_TZ(OM.delivery_time,'+00:00',rl.time_zone)) as  delivery_time,   OM.order_id,
						   OM.order_ref_id,
						   OM.restaurant_id,
						   OM.location_id,
						   rl.restaurant_id,
                                                   rl.restaurant_name,
						   OM.member_id,
						   OM.total_amount,
						   OM.sub_total,
						   OM.discount_amount,
						   OM.tax_amount,
						   OM.refund_amount,
						   OM.delivery_service_amount,
						   OM.order_status,
						   OM.payment_status,
						   OM.order_type,
						   OM.is_later,
						   OM.delivery_time,
						   OM.tip,
						   da.address,
						   da.zipcode,
						   da.city,
						   da.phone,
						   da.state,
						   da.notes,
						   sm.last_4digit,
						   mm.phone as contact_phone
                                                   FROM order_master OM
						   LEFT JOIN delivery_address da ON da.order_id =  OM.order_id
						   LEFT JOIN order_stripid_map sm ON sm.order_id =  OM.order_id
						   LEFT JOIN order_items i1 on i1.order_id=OM.order_id 
						   LEFT JOIN dish_master dish on dish.id=i1.item_id 
						   LEFT JOIN restaurant_master rl ON rl.restaurant_id = OM.restaurant_id
						   LEFT JOIN member_master mm ON mm.member_id =  OM.member_id
                                                   LEFT JOIN chain_rest_child crc ON rl.restaurant_id = crc.rest_id
                                            	   WHERE rl.restaurant_id!='' and rl.`status`='Y' AND  OM.member_id = '$member_id'  and OM.order_status!='Cancelled' and OM.order_status!='Declined' ";
        if ($restaurant_id != '') {
            $sql.=" AND OM.restaurant_id='$restaurant_id'";
        }
        $sql.=" and OM.status='Y' AND IF(rl.chain_flag ='Y', crc.rest_id!='',1) ORDER BY OM.created_time DESC  ";

        if ($limit == '') {
            $sql.=" LIMIT 0,15";
        } else {
            $sql.=" LIMIT " . $limit . ",15";
        }
        $query = $this->db->query($sql);
//        echo $this->db->last_query();
//        exit;
        $res = $query->result_array();
        return $res;
    }

    function get_restaurant_chain($arr) {
        $miles = $arr['admin_miles'];
        $latitude = $arr['lat'];
        $longitude = $arr['long'];
        $dd = $this->db->query("SELECT * FROM (SELECT T1.id,T3.*,( 3959 * acos( cos( radians($latitude) ) * cos( radians(T3.lat) ) * cos( radians(T3.`long`) - 
radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) ) AS distance from chain_master T1 LEFT JOIN chain_rest_child T2 on T2.chain_id=T1.id  LEFT JOIN restaurant_master T3 on T2.rest_id=T3.restaurant_id where T3.status ='Y' AND T3.signed_flag='Y' ORDER BY distance ASC) as tq GROUP BY id ");
        $result = $dd->result_array();
        $return = array();
        foreach ($result as $res) {
            if ($res['distance'] <= $miles) {
                $return[] = $res;
            }
        }
        return $return;
    }

    function generlConfig($field) {
        $data = $this->db->query("select `value` from general_config where field='$field'");
        $return = $data->row_array();
        return $return['value'];
    }

    function getRest_chain_userId_loc($arr) {
        $uid = $arr['uid'];
        $miles = $arr['admin_miles'];
        $latitude = $arr['lat'];
        $longitude = $arr['long'];
        $return = $this->db->query("SELECT  SP.user_fee,SL.End_date,SP.number_of_restaurants FROM subscription_log SL LEFT JOIN User_subscription_master US ON SL.subscription_id=US.subscription_id
     LEFT JOIN subscription_package SP ON SP.sub_package_id=US.package_id    WHERE SL.End_date>=DATE(NOW()) AND SL.member_id='" . $uid . "'  ORDER BY SL.log_id DESC ");
        $restDetails = $return->row_array();
        $number_of_rest = $restDetails['number_of_restaurants'];
        if (!$number_of_rest) {
            $number_of_rest = 0;
        }
        $sub = $this->db->query("select T1.restaurant_id,T3.chain_flag from current_restaurant T1 left join User_subscription_master T2 on T1.subscription_id=T2.subscription_id left join restaurant_master T3 on T1.restaurant_id=T3.restaurant_id where T2.member_id='$uid' order by T2.subscription_id desc limit $number_of_rest");
        $curr_rest = $sub->result_array();
        $result = array();
        $rs = array();
        $query = "SELECT * FROM (SELECT T1.id,T3.*,( 3959 * acos( cos( radians($latitude) ) * cos( radians(T3.lat) ) * cos( radians(T3.`long`) - 
radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) ) AS distance from chain_master T1 LEFT JOIN chain_rest_child T2 on T2.chain_id=T1.id  LEFT JOIN restaurant_master T3 on T2.rest_id=T3.restaurant_id where T3.status ='Y' AND T3.signed_flag='Y' and ( 3959 * acos( cos( radians($latitude) ) * cos( radians(T3.lat) ) * cos( radians(T3.`long`) - 
radians($longitude) ) + sin( radians($latitude) ) * sin( radians(`lat`) ) ) )<=" . $miles;
        foreach ($curr_rest as $curRest) {
            $selR = $this->db->query("select * from restaurant_master where restaurant_id=" . $curRest['restaurant_id']);
//            echo $this->db->last_query() . "<br/>";
            $rs[] = $selR->row_array();
            if ($curRest['chain_flag'] == 'Y') {
                $mas_child_res = $this->find_child($curRest['restaurant_id']);
                foreach ($mas_child_res as $mcr) {
                    $query.=" and T3.restaurant_id!=" . $mcr['rest_id'];
                }
            }
        }
        $query.=" ORDER BY distance ASC) as tq GROUP BY id ";
        $mas = $this->db->query($query);
//        echo $this->db->last_query();
//        exit;
        $mass = $mas->result_array();
        return array_merge($mass, $rs);
    }

    function find_child($id) {
        $dt = $this->db->query("select chain_id from chain_rest_child where rest_id=" . $id);
        $rs = $dt->row_array();
        $dat = $this->db->query("select rest_id from chain_rest_child where chain_id=" . $rs['chain_id']);
        return $dat->result_array();
    }

    function get_restChainMaster_userID($uid) {
        $return = $this->db->query("SELECT  SP.user_fee,SL.End_date,SP.number_of_restaurants FROM subscription_log SL LEFT JOIN User_subscription_master US ON SL.subscription_id=US.subscription_id
     LEFT JOIN subscription_package SP ON SP.sub_package_id=US.package_id    WHERE SL.End_date>=DATE(NOW()) AND SL.member_id='" . $uid . "'  ORDER BY SL.log_id DESC ");
        $restDetails = $return->row_array();
        $number_of_rest = $restDetails['number_of_restaurants'];
        if (!$number_of_rest) {
            $number_of_rest = 0;
        }
        $sub = $this->db->query("select T1.restaurant_id from current_restaurant T1 left join User_subscription_master T2 on T1.subscription_id=T2.subscription_id left join restaurant_master T3 on T1.restaurant_id=T3.restaurant_id where T2.member_id='$uid' and T3.chain_flag='Y' order by T2.subscription_id desc limit $number_of_rest");
        $curr_rest = $sub->result_array();
        $result = array();
        $rs = array();
        foreach ($curr_rest as $curRest) {
            $qry = $this->db->query("SELECT T1.managed_restaurent from chain_master T1 LEFT JOIN chain_rest_child T2 on T1.id =T2.chain_id WHERE T2.rest_id=" . $curRest['restaurant_id']);
            $result[] = $qry->row_array();
//            $result = array_merge($result, $res);
            $selR = $this->db->query("select * from restaurant_master where restaurant_id=" . $curRest['restaurant_id']);
            $rs[] = $selR->row_array();
        }

        $query = "select T1.* from chain_master T2 LEFT JOIN restaurant_master T1  on T2.managed_restaurent=T1.restaurant_id where T1.status ='Y' AND T1.signed_flag='Y' AND T1.chain_flag='Y'";
        foreach ($result as $master) {
            $query.=" and T2.managed_restaurent!=" . $master['managed_restaurent'];
        }
        $mas = $this->db->query($query);
        $mass = $mas->result_array();
//        echo $this->db->last_query();
//        echo '<pre>';
//        print_r($rs);
//        print_r($mass);
//        exit;
        return array_merge($mass, $rs);
    }

    public function getAllOderItemDetails($order_id) {
        $sql = "SELECT DM.*,OI.*,DIM.* FROM order_items OI
					LEFT JOIN dish_items_master DIM on DIM.item_id=OI.item_id 
                    LEFT JOIN dish_master DM ON OI.item_id=DM.id
					WHERE OI.order_id = '$order_id'
					";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function getAllOder_side($order_id) {
        $sql = "SELECT * FROM order_option_map WHERE order_id='$order_id'";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    function update_push_notif($push, $member_id) {
        $data = array(
            'push_notification' => $push
        );

        $this->db->where('member_id', $member_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    function geoLocation($geo_location, $member_id) {
        $data = array(
            'geo_location' => $geo_location
        );

        $this->db->where('member_id', $member_id);
        $id = $this->db->update('member_master', $data);
        return true;
    }

    public function get_subscription_all_details($member_id) {

        /* $sql = "SELECT A3.number_of_restaurants,A2.* FROM subscription_log A1 
          LEFT JOIN current_restaurant A2 ON A1.subscription_id=A2.subscription_id
          LEFT JOIN User_subscription_master A4 ON A1.subscription_id=A4.subscription_id
          LEFT JOIN subscription_package A3 ON A4.package_id=A3.sub_package_id
          WHERE A1.member_id='$member_id' AND A1.End_date>=DATE(NOW())  GROUP BY A2.current_id ORDER BY A2.current_id DESC"; */
        $sql = "SELECT A3.number_of_restaurants,A2.* FROM subscription_log A1 
         LEFT JOIN current_restaurant A2 ON A1.subscription_id=A2.subscription_id
         LEFT JOIN User_subscription_master A4 ON A1.subscription_id=A4.subscription_id
         LEFT JOIN subscription_package A3 ON A4.package_id=A3.sub_package_id
		 LEFT JOIN restaurant_master A5 ON A5.restaurant_id=A2.restaurant_id
         WHERE A1.member_id='$member_id' AND A1.End_date>=DATE(NOW()) AND A5.restaurant_id!=''  GROUP BY A2.current_id ORDER BY A2.current_id DESC";
        /*
          $sql = "SELECT * FROM current_restaurant
          WHERE subscription_id=(SELECT T1.subscription_id FROM `subscription_log` T1
          WHERE T1.member_id='$member_id' ORDER BY T1.log_id DESC LIMIT 1)"; */
        $query = $this->db->query($sql);
        //select * from subscription_package where sub_package_id='28' and active='Y' 
        $data_11 = $query->result_array();
        $cont = sizeof($data_11);
        for ($i = 0; $i < $cont; $i++) {

            $data[$i] = $data_11[$i];
            $data[$i]['number_of_restaurants'] = $cont;
        }
        $tok = 0;
        foreach ($data as $key => $datas) {
            $sql1 = "SELECT * FROM restaurant_master WHERE  restaurant_id='" . $datas['restaurant_id'] . "'";
            $query1 = $this->db->query($sql1);
            $data[$key]['restaurant_detais'] = $query1->row_array();
        }

        return $data;
    }

    public function payment_history($member_id) {
        $sql = "SELECT T1.*,T3.* FROM `member_stripid_map` T1 LEFT JOIN User_subscription_master T2 ON T1.id=T2.strip_id
   LEFT JOIN subscription_log T3 ON T2.subscription_id=T3.subscription_id  WHERE T1.member_id='$member_id' AND T1.amount!='0' AND T1.restaurant_id='0'";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function payment_history_1_new($member_id, $limit = '') {
        $sql = "SELECT T3.End_date,T3.start_date,A1.*,A1.amount,A2.order_ref_id,A2.total_amount,A2.sub_total,A2.discount_amount,A2.tax_amount
            FROM `member_stripid_map` A1 
            LEFT JOIN order_master A2 ON A1.id=A2.stripe_id 
            LEFT JOIN User_subscription_master T2 ON A1.id=T2.strip_id
            LEFT JOIN subscription_log T3 ON T2.subscription_id=T3.subscription_id 
            WHERE A1.member_id='" . $member_id . "'  AND A1.amount<>'' 
            AND ((A2.order_status!='Cancelled' AND A2.order_status!='Declined') OR A2.order_status IS NULL) 
            AND  (A2.`status`!='N' OR A2.`status` IS NULL)  GROUP BY A1.id ORDER BY A1.id DESC";
        if ($limit == '') {
            $sql.=" LIMIT 0,15";
        } else {
            $sql.=" LIMIT " . $limit . ",15";
        }
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function historySBscription($member_id) {
        $sql = "SELECT * FROM User_subscription_master 
		WHERE member_id='" . $member_id . "' 
		AND subscription_id>(SELECT subscription_id FROM User_subscription_master 
		WHERE member_id='" . $member_id . "' AND  strip_id<>'' ORDER BY subscription_id DESC LIMIT 1)
		AND ISNULL(strip_id) ORDER BY subscription_id DESC  LIMIT 1";
        $query = $this->db->query($sql);
        $res = $query->row_array();
        return $res;
    }

    public function getOrderStripMap($order_id) {
        $sql = "SELECT T1.*,
          last_4digit,
					brand,
					strip_order_id,
					strip_customer_id,
					T1.order_id,
					payment_mode
					FROM order_master T1
			LEFT JOIN member_stripid_map T2
			ON T2.id = T1.stripe_id
		    WHERE  T1.order_id='" . $order_id . "'";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function delivery_time($restaurant_id) {
        $sql = "SELECT *,delivery_fee,
		              IFNULL(areas_delivery,(SELECT `value` 
                      FROM general_config WHERE field='default_radius' )) AS areas_delivery ,
					   IF(tax='',(SELECT `value` 
                       FROM general_config 
					   WHERE field='sales_tax_percentage' ),tax) AS tax 
				  FROM `restaurant_master` WHERE restaurant_id='" . $restaurant_id . "'";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function delivery_time_new($restaurant_id) {
        $sql = "SELECT *,delivery_fee,
		              IFNULL(areas_delivery,(SELECT `value` 
                      FROM general_config WHERE field='default_radius' )) AS areas_delivery ,
					   IF(tax='',(SELECT `value` 
                       FROM general_config 
					   WHERE field='sales_tax_percentage' ),tax) AS tax_1 
				  FROM `restaurant_master` WHERE restaurant_id='" . $restaurant_id . "'";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function update_new_UserDatas($arr) {
        $data = array(
            'device_id' => $arr['device_id'],
            'divice_type' => $arr['device_platform'],
            'device_tocken' => $arr['device_token']
        );
//        $this->db->where('member_id', $arr['member_id']);
        $condition = array('member_id' => $arr['member_id']);
        $id = $this->db->update('member_master', $data, $condition);
//        echo $this->db->last_query();
//        exit;
        return true;
    }

    function get_user_order($id) {
        
    }

    public function next_restaurant_status($member_id) {
        $sql = "SELECT * FROM `subscription_next` WHERE member_id='" . $member_id . "' AND `status` ='Y'";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function update_next_restaurant_status($next_sub_id) {
        $data = array(
            'status' => 'N'
        );

        $this->db->where('subscription_id', $next_sub_id);
        $id = $this->db->update('subscription_next', $data);
        return true;
    }

}

?>
	  