<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class User_model extends MY_Model { 	
public $_table = 'member_master';
	
public function setTable($table){
			$this->_table = $table;
}
	
public function getCustomerDetails($restaurant_id,$key,$num,$offset){
		$sql     = "SELECT member_master.*, restaurant_master.name
		  		    FROM member_master INNER JOIN restaurant_master 
					ON (member_master.restaurant_id = restaurant_master.restaurant_id)
				    WHERE member_master.restaurant_id = $restaurant_id";
		if($key != ''){
		$sql  .= "  AND (member_master.first_name LIKE '%$key%' OR 
				    member_master.last_name LIKE '%$key%' OR 
				    member_master.email LIKE '%$key%')";	
		}
		if($offset)
			$sql.=" limit $offset,$num";
		else
			$sql.=" limit $num";		
	
		$query 	= $this->db->query($sql);
		return $query->result_array();	
}
public function getCustomerDetails1($key,$num,$offset){
		$sql     = "SELECT member_master.*
		  		    FROM member_master where 1=1 ";
					
		if($key != ''){
		$sql  .= "  AND (first_name LIKE '%$key%' OR 
				    last_name LIKE '%$key%' OR 
				   email LIKE '%$key%')";	
		}
		$sql.=" order by member_id desc ";
		if($offset)
			$sql.=" limit $offset,$num";
		else
			$sql.=" limit $num";
			
				
		$query 	= $this->db->query($sql);
		return $query->result_array();	
}
	
public function countCustomers($restaurant_id,$key){
			$sql	 = "SELECT count(*) as num
						FROM member_master where 1=1
						";
			if($key != '' ){
		    $sql .= " AND (first_name LIKE '%$key%' OR 
					 last_name LIKE '%$key%' OR 
					 email LIKE '%$key%')";	
		    }			
			$query 	= $this->db->query($sql);
			$result	= $query->row_array();	
			return  $result['num'];
}
public function countCustomers1($key,$id=''){
			$sql	 = "SELECT count(*) as num
						FROM member_master WHERE 1=1";
			if($id != '' && $id != 0){
			$sql	 .=" AND restaurant_id = $id";
			}
			if($key != ''){
		    $sql .= " AND (member_master.first_name LIKE '%$key%' OR 
					  member_master.last_name LIKE '%$key%' OR 
					  member_master.email LIKE '%$key%')";	
		    }	
			
			$query 	= $this->db->query($sql);
			$result	= $query->row_array();	
			return  $result['num'];
}
public function UpdateDetails($table,$data,$where){

			$this->db->where($where);
			$this->db->update($table, $data);
			return true;
			
}
public function getCustomerInfo($member_id)
{
			$sql 	= "SELECT member_master.*
					   FROM member_master
					   WHERE  member_master.member_id = $member_id";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->row_array();	
            return $query;
}


public function getCustomeraddress($member_id)
{
			$sql 	= "SELECT *
					   FROM address T1 LEFT JOIN member_stripid_map T2 ON T1.stripeid=T2.id
					   WHERE  T1.member_id = '$member_id'";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
            return $query;
}

public function getStripInfo($member_id)
{
			$sql 	= "SELECT * FROM `member_stripid_map` WHERE member_id='$member_id'";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
            return $query;
}
public function bulkDelete($member_id){
		
		$sql = "DELETE FROM member_master 
				WHERE member_master.member_id ='$member_id'";
				
		$query = $this->db->query($sql);

}
public function payment_history($member_id)
{
			$sql 	= "SELECT (SELECT SUM(total_amount) FROM order_master T1 
     LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id  
     WHERE member_id='$member_id' AND  T1.subscription_id=T2.subscription_id ) as total_amount, T2.subscription_id as subscription_id_new,(SELECT start_date from subscription_log WHERE member_id ='$member_id' AND subscription_id=T1.subscription_id  ORDER BY start_date ASC LIMIT 1) as start_date,
((SELECT End_date from subscription_log WHERE member_id ='$member_id' AND subscription_id=T1.subscription_id  ORDER BY End_date DESC LIMIT 1)) as End_date,T3.*,T4.package_name,(CONCAT(T3.amount,' * ',(SELECT COUNT(*) from subscription_log WHERE subscription_id =T1.subscription_id ))) as amount FROM `subscription_log` T1 
							   LEFT JOIN User_subscription_master T2 ON T1.subscription_id=T2.subscription_id
							   LEFT JOIN member_stripid_map T3 ON T2.strip_id=T3.id
							   LEFT JOIN subscription_package T4 ON T2.package_id=T4.sub_package_id
							   WHERE  T1.member_id='$member_id' AND T1.`status`='Y' group by subscription_id_new ";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
			foreach($query as $key=>$data)
			{
			$sql1="SELECT T2.restaurant_name FROM current_restaurant T1 LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id
 WHERE T1.subscription_id='".$data['subscription_id_new']."'";
			$test	= $this->db->query($sql1);
			$query[$key]['restaurant_list']  = $test->result_array();	
			}
			
            return $query;
}


 

public function get_restaurantdetails_subscription($member_id,$subscription_id)
{
			$sql 	= "SELECT T2.restaurant_name,sum(total_amount) as total_amount,(SELECT SUM(total_amount) FROM order_master T1 
     LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id  
     WHERE member_id='$member_id' AND  T1.subscription_id='$subscription_id' AND (T1.order_status ='Completed' OR T1.order_status ='New') ) as sub_total_amount  FROM order_master T1 LEFT JOIN restaurant_master T2 ON T1.restaurant_id=T2.restaurant_id  
	 WHERE member_id='$member_id' AND  T1.subscription_id='$subscription_id' AND (T1.order_status ='Completed' OR T1.order_status ='New') GROUP BY T1.restaurant_id";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
            return $query;
}




}
?>