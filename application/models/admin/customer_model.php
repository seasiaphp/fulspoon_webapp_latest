<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Customer_model extends MY_Model { 	
public $_table = 'member_master';
	
public function setTable($table){
			$this->_table = $table;
}
	
public function getCustomerDetails($restaurant_id,$key,$num,$offset){
		$sql     = "SELECT member_master.*, restaurant_master.name
		  		    FROM member_master INNER JOIN restaurant_master 
					ON (member_master.restaurant_id = restaurant_master.restaurant_id)
				    WHERE member_master.restaurant_id = $restaurant_id";
		if($key != ''){
		$sql  .= "  AND (member_master.first_name LIKE '%$key%' OR 
				    member_master.last_name LIKE '%$key%' OR 
				    member_master.email LIKE '%$key%')";	
		}
		if($offset)
			$sql.=" limit $offset,$num";
		else
			$sql.=" limit $num";		
	
		$query 	= $this->db->query($sql);
		return $query->result_array();	
}
public function getCustomerDetails1($key,$num,$offset,$id=''){
		$sql     = "SELECT member_master.*, restaurant_master.name
		  		    FROM member_master INNER JOIN restaurant_master 
					ON (member_master.restaurant_id = restaurant_master.restaurant_id)
		  		    WHERE  1=1 ";
					
		if($id != ''&& $id != 0 ){
			$sql	 .=" AND member_master.restaurant_id=$id";
			}			
		if($key != ''){
		$sql  .= "  AND (member_master.first_name LIKE '%$key%' OR 
				    member_master.last_name LIKE '%$key%' OR 
				    member_master.email LIKE '%$key%' OR
					restaurant_master.name LIKE '%$key%')";	
		}
		if($offset)
			$sql.=" limit $offset,$num";
		else
			$sql.=" limit $num";		

		$query 	= $this->db->query($sql);
		return $query->result_array();	
}
	
public function countCustomers($restaurant_id,$key){
			$sql	 = "SELECT count(*) as num
						FROM member_master
						WHERE restaurant_id=$restaurant_id";
			if($key != '' ){
		    $sql .= " AND (member_master.first_name LIKE '%$key%' OR 
					  member_master.last_name LIKE '%$key%' OR 
					  member_master.email LIKE '%$key%')";	
		    }			
			$query 	= $this->db->query($sql);
			$result	= $query->row_array();	
			return  $result['num'];
}
public function countCustomers1($key,$id=''){
			$sql	 = "SELECT count(*) as num
						FROM member_master WHERE 1=1";
			if($id != '' && $id != 0){
			$sql	 .=" AND restaurant_id = $id";
			}
			if($key != ''){
		    $sql .= " AND (member_master.first_name LIKE '%$key%' OR 
					  member_master.last_name LIKE '%$key%' OR 
					  member_master.email LIKE '%$key%')";	
		    }	
			
			$query 	= $this->db->query($sql);
			$result	= $query->row_array();	
			return  $result['num'];
}
public function UpdateDetails($table,$data,$where){

			$this->db->where($where);
			$this->db->update($table, $data);
			return true;
			
}
public function getCustomerInfo($member_id,$restaurant_id)
{
	
			/*$sql 	= "SELECT member_master.*, restaurant_master.name 
					   FROM member_master
					   INNER JOIN restaurant_master 
					   ON (member_master.restaurant_id = restaurant_master.restaurant_id AND member_master.member_id = $member_id)
					   WHERE member_master.restaurant_id='$restaurant_id'";*/
			if($restaurant_id != ''){		   
			$sql 	= "SELECT member_master.*, restaurant_master.name 
					   FROM member_master
					   INNER JOIN restaurant_master 
					   ON (member_master.restaurant_id = restaurant_master.restaurant_id AND member_master.member_id = $member_id)
					   WHERE member_master.restaurant_id='$restaurant_id'";	
					   }
			$sql 	= "SELECT member_master.*
					   FROM member_master
					   WHERE  member_master.member_id = $member_id";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->row_array();	
            return $query;
}
public function bulkDelete($member_id){
		
		$sql = "DELETE FROM member_master 
				WHERE member_master.member_id =$member_id";
				
		$query = $this->db->query($sql);

}

}
?>