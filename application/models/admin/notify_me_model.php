<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class notify_me_model extends MY_Model { 	
public $_table = 'member_master';
	
public function setTable($table){
			$this->_table = $table;
}
	
public function getCustomerDetails($restaurant_id,$key,$num,$offset){
		$sql     = "SELECT member_master.*, restaurant_master.name
		  		    FROM member_master INNER JOIN restaurant_master 
					ON (member_master.restaurant_id = restaurant_master.restaurant_id)
				    WHERE member_master.restaurant_id = $restaurant_id";
		if($key != ''){
		$sql  .= "  AND (member_master.first_name LIKE '%$key%' OR 
				    member_master.last_name LIKE '%$key%' OR 
				    member_master.email LIKE '%$key%')";	
		}
		if($offset)
			$sql.=" limit $offset,$num";
		else
			$sql.=" limit $num";		
	
		$query 	= $this->db->query($sql);
		return $query->result_array();	
}
public function getCustomerDetails1($key,$num,$offset){
		$sql     = "SELECT * FROM notify_mail where 1=1 ";
					
		if($key != ''){
		$sql  .= "  AND (email LIKE '%$key%')";	
		}
		$sql.=" ORDER BY e_id DESC";
		if($offset)
			$sql.=" limit $offset,$num";
		else
			$sql.=" limit $num";
			
				
		$query 	= $this->db->query($sql);
		return $query->result_array();	
}
	
public function countCustomers($restaurant_id,$key){
			$sql	 = "SELECT count(*) as num
						FROM notify_mail where 1=1
						";
			if($key != '' ){
		    $sql .= " AND (email LIKE '%$key%')";	
		    }			
			$query 	= $this->db->query($sql);
			$result	= $query->row_array();	
			return  $result['num'];
}
public function countCustomers1($key,$id=''){
			$sql	 = "SELECT count(*) as num
						FROM notify_mail where 1=1
						";
			if($key != '' ){
		    $sql .= " AND (email LIKE '%$key%')";	
		    }			
			$query 	= $this->db->query($sql);
			$result	= $query->row_array();	
			return  $result['num'];
}
public function UpdateDetails($table,$data,$where){

			$this->db->where($where);
			$this->db->update($table, $data);
			return true;
			
}
public function getCustomerInfo($member_id)
{
			$sql 	= "SELECT member_master.*
					   FROM member_master
					   WHERE  member_master.member_id = $member_id";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->row_array();	
            return $query;
}


public function getCustomeraddress($member_id)
{
			$sql 	= "SELECT *
					   FROM address T1 LEFT JOIN member_stripid_map T2 ON T1.stripeid=T2.strip_customer_id
					   WHERE  T1.member_id = '$member_id'";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
            return $query;
}

public function getStripInfo($member_id)
{
			$sql 	= "SELECT * FROM `member_stripid_map` WHERE member_id='$member_id'";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
            return $query;
}
public function bulkDelete($e_id){
		
		$sql = "DELETE FROM notify_mail 
				WHERE e_id ='$e_id'";
		$query = $this->db->query($sql);

}
public function payment_history($member_id)
{
			$sql 	= "SELECT T1.start_date,T1.End_date,T3.*,T4.package_name FROM `subscription_log` T1 
							   LEFT JOIN User_subscription_master T2 ON T1.subscription_id=T2.subscription_id
							   LEFT JOIN member_stripid_map T3 ON T2.strip_id=T3.id
							   LEFT JOIN subscription_package T4 ON T2.package_id=T4.sub_package_id
							   WHERE  T1.member_id='$member_id' AND T1.`status`='Y'";	   	   
			$query 	= $this->db->query($sql);
			$query  = $query->result_array();	
            return $query;
}
public function file_xls_test($member_id)
{
			$query 	= "SELECT email,zipcode,date_time FROM notify_mail where needed='Y'";	   	   
            return $query;
}



}
?>