<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Avg_order_model extends MY_Model {

    public $_table = 'promocodes';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function adddetails($table, $data) {

        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function getPromoCount($key = '') {
        $sql = "SELECT COUNT(*) as num FROM average_order_master WHERE 1=1";
        if ($key != '') {
            $sql .= " AND (average_order LIKE '%$key%')";
        }

        $query = $this->db->query($sql);

        $result = $query->row_array();
        return $result['num'];
    }

    public function getAllPromocodes($status, $key, $num, $offset) {

        $sql = "SELECT * FROM  average_order_master WHERE 1=1 ";

        if ($status)
            $sql .= " AND status = '$status' ";

        if ($key != '') {
		$sql .= " AND (average_order LIKE '%$key%')";
        }

        $sql.=" ORDER BY ao_id DESC ";
        if ($offset)
            $sql.=" limit $offset,$num ";
        else
            $sql.=" limit $num ";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function checkExist($promocode, $restaurant_id) {
        $sql = "SELECT COUNT(promocode) as num FROM promocodes WHERE restaurant_id='$restaurant_id' AND promocode= '$promocode'";
        //echo $sql;exit;
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;
        return $num;
    }

}

?>