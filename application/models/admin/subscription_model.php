<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscription_model extends MY_Model {

    public $_table = 'subscription_package';

    public function setTable($table) {
        $this->_table = $table;
    }

  
	 public function subscription_insert($data) {

        $this->db->insert(subscription_package,$data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }



    public function sub_detail($sub_package_id) {
        $sql = "SELECT * FROM subscription_package where sub_package_id='".$sub_package_id."'";
        //echo $sql;exit;
        
        $query = $this->db->query($sql);
        return $query->row();
	
    }
	   public function get_package_full_detail() {
        $sql = "	SELECT * FROM subscription_package ORDER BY number_of_restaurants asc";
        $query = $this->db->query($sql);
        return $query->result();
	
    }

	
	 public function subscription_update($data,$sub_id) {

       $this->db->where('sub_package_id', $sub_id);
       $this->db->update(subscription_package,$data);
       return true;

    }
	
	 public function get_sub_details($sub_package_id) {
       /* $sql = "SELECT * FROM `subscription_log` WHERE subscription_id='".$sub_package_id."'";*/
        $sql="SELECT * FROM User_subscription_master T1 
					  LEFT JOIN subscription_log T2 ON T1.subscription_id=T2.subscription_id
					  WHERE T1.package_id='$sub_package_id' AND T2.End_date>=DATE(NOW())";
        $query = $this->db->query($sql);
        return $query->result_array();
	
    }
	public function bulkDelete($id){
		
		$sql = "DELETE FROM subscription_package 
				WHERE sub_package_id ='".$id."'";
				
		$query = $this->db->query($sql);

}
 public function check_exist($arr) {
        $sql = "SELECT * FROM `subscription_package` WHERE frequency='".$arr['frequency']."' AND duration='".$arr['duration']."' and number_of_restaurants='".$arr['number_of_restaurants']."'";
		if($arr['sub_id'])
		{
		 $sql .=" AND sub_package_id!='".$arr['sub_id']."'";
		}
	
		
        $query = $this->db->query($sql);
        return $query->row_array();
	
    }


	 
}

?>