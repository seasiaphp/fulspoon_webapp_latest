<?php

class Admin_model extends MY_Model {

    public $_table = 'member_admins';

    public function __construct() {
        $this->_database = $this->db;
        $this->load->database();
    }

    public function setTable($table) {
        $this->_table = $table;
    }

    public function getUser() {

        $this->db->select("*");
        $this->db->from("member_admins");
        $query = $this->db->get();
        return $query->row();
    }

    public function getadmindetail($username) {
        $sql = "SELECT admin_id,full_name,username,password,email,created_time,status,role,root FROM admin_master WHERE  username='$username'";
        $query = $this->db->query($sql);
        $result = $query->result();
        //print_r($result);
        return $result[0];
    }

    public function getMemberDetails($id) {
        $sql = "SELECT * FROM admin_master WHERE  username='$username'";

        $query = $this->db->query($sql);
        $result = $query->result();
        return $result[0];
    }

    public function getpassword($email) {
        $sql = "SELECT password
				FROM admin_master
				WHERE  email ='$email'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getCount($location_id) {

        $sql = "SELECT COUNT(order_id) as num FROM order_master";
        if ($location_id != '')
            $sql .=" WHERE  location_id ='$location_id '";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getCount2($restaurant_id) {

        $sql = "SELECT COUNT(order_id) as num FROM order_master";
        if ($restaurant_id != '')
            $sql .=" WHERE  restaurant_id ='$restaurant_id '";
        //print_r($sql);exit;
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getMenu($location_id) {

        $sql = "SELECT COUNT(category_id) as num FROM  dish_category_master WHERE  location_id ='$location_id '";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getPreference($location_id) {

        $sql = "SELECT COUNT(field) as num FROM   preferences WHERE  location_id ='$location_id'";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;
        if ($num == 0) {
            $sql = "SELECT COUNT(field) as num FROM   general_config WHERE  editable_to_restaurant ='Y'";
            $result = $this->db->query($sql);
            $results = $result->result();
            $num = $results[0]->num ? $results[0]->num : 0;
            return $num;
        }
        return $num;
    }

    public function getCustomer($restaurant_id) {

        $sql = "SELECT COUNT(member_id) as num FROM  member_master";
        if ($restaurant_id != '')
            $sql .=" WHERE  restaurant_id ='$restaurant_id '";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getRestaurant() {

        $sql = "SELECT COUNT(restaurant_id) as num FROM  restaurant_master ";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getSettings() {

        $sql = "SELECT COUNT(field) as num FROM  general_config	 ";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getReport($restaurant_id) {

        $sql = "SELECT COUNT(order_id) as num FROM order_master	 WHERE restaurant_id='$restaurant_id' ";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getLocation($restaurant_id) {

        $sql = "SELECT COUNT(location_id) as num FROM restaurant_locations WHERE restaurant_id='$restaurant_id' ";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

    public function getPromocode() {

        $sql = "SELECT COUNT(promo_id) as num FROM promocodes ";
        $result = $this->db->query($sql);
        $results = $result->result();
        $num = $results[0]->num ? $results[0]->num : 0;

        return $num;
    }

}
