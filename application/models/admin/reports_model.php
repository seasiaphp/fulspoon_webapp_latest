<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends MY_Model {

    public $_table = 'order_master';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function getAllSalesReport($restaurant_id, $num, $offset, $startDate, $endDate) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name,C.type
				FROM order_master A
				LEFT JOIN member_master B on A.member_id=B.member_id 
				LEFT JOIN restaurant_locations C on A.location_id=C.location_id 
				WHERE A.restaurant_id=$restaurant_id ";
        if ($startDate != '' && $endDate != '')
            $sql.= " AND created_time >= '$startDate' AND created_time <= '$endDate' ";

        $sql.= " ORDER BY payment_status  DESC ";
        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";
        //echo $sql;		
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function countSalesReport($restaurant_id, $startDate, $endDate) {
        $sql = "SELECT count(*) as num
					FROM order_master A
					WHERE 1=1 
					AND restaurant_id=$restaurant_id
					";
        if ($startDate != '' && $endDate != '')
            $sql.= " AND created_time >= '$startDate' AND created_time <= '$endDate' ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function getAllSalesReportcsv($fields, $limit = '', $per_page = '', $start = '', $end = '') {

        //SELECT * from table Where comp_id IN (".implode(',',$arr).");

        if (count($fields) > 0) {
            $comma = '';
            for ($i = 0; $i < count($fields); $i++) {
                switch ($fields[$i]) {
                    case 'order_ref_id':
                        $flds .=$comma . "A.order_ref_id";
                        break;
                    case 'restaurant_name':
                        $flds .=$comma . "C.restaurant_name  ";
                        break;
                    case 'first_name':
                        $flds .=$comma . "B.first_name,B.last_name";
                        break;
                    case 'order_type ':
                        $flds .=$comma . "A.order_type";
                        break;
                    case 'total_amount':
                        $flds .=$comma . "A.total_amount";
                        break;
                    case 'created_time ':
                        $flds .=$comma . "A.created_time";
                        break;

                    default:
                        $flds .=$comma . $fields[$i];
                }
                $comma = ' , ';
            }
        }


        $sql = "SELECT ";
        $sql.= " $flds FROM order_master A
				LEFT JOIN member_master B on A.member_id=B.member_id 
				LEFT JOIN restaurant_locations C on A.location_id=C.location_id 
				WHERE 1=1 ";
        if ($start != '' && $end != '')
            $sql.= " AND created_time >= '$start' AND created_time <= '$end' ";
        if ($limit)
            $sql.="limit $limit,$per_page";
        //echo $sql;exit;		
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getQuery($fields, $limit = '', $per_page = '', $start, $end) {

        if (count($fields) > 0) {
            $comma = '';
            for ($i = 0; $i < count($fields); $i++) {
                switch ($fields[$i]) {
                    case 'order_ref_id':
                        $fld .=$comma . "A.order_ref_id";
                        break;
                    case 'restaurant_name':
                        $fld .=$comma . "C.restaurant_name  ";
                        break;
                    case 'first_name':
                        $fld .=$comma . "B.first_name,B.last_name";
                        break;
                    case 'order_type ':
                        $fld .=$comma . "A.order_type";
                        break;
                    case 'total_amount':
                        $fld .=$comma . "A.total_amount";
                        break;
                    case 'created_time ':
                        $fld .=$comma . "A.created_time";
                        break;

                    default:
                        $fld .=$comma . $fields[$i];
                }
                $comma = ' , ';
            }
        }


        $sql = "SELECT ";
        $sql.= " $fld FROM order_master A
							LEFT JOIN member_master B on A.member_id=B.member_id 
							LEFT JOIN restaurant_locations C on A.location_id=C.location_id 
							WHERE 1=1 ";
        if ($start != '' && $end != '')
            $sql.= " AND created_time >= '$start' AND created_time <= '$end' ";
        if ($limit)
            $sql.="limit $limit,$per_page";
        $query = $this->db->query($sql);
        //print_r( $query);exit;
        return $query;
    }

    public function getAllCustomerReport($restaurant_id, $num, $offset, $startDate, $endDate) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name,C.type
				FROM order_master A
				LEFT JOIN member_master B on A.member_id=B.member_id 
				LEFT JOIN restaurant_locations C on A.location_id=C.location_id 
				WHERE A.restaurant_id=$restaurant_id ";
        if ($startDate != '' && $endDate != '')
            $sql.= " AND created_time >= '$startDate' AND created_time <= '$endDate' ";

        $sql.= " ORDER BY payment_status  DESC ";
        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";
        //echo $sql;		
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}

?>