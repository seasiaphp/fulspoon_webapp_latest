<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of chainModel
 *
 * @author aneesh
 */
class chain_model extends CI_Model {

    public function __construct() {
        $this->load->database();
//        date_default_timezone_set('GMT');
    }

    function getChainCount($keyword, $status) {
        $qry = "select count(id) as count from chain_master where 1 ";
        if ($keyword != '') {
            $qry.=" and name like '%$keyword%' ";
        }if ($status != '') {
            $qry = " and status='$status'";
        }
        $data = $this->db->query($qry);
        return $data->result_array();
    }

    function getChainData($id) {
        $data = $this->db->query("select T1.*,T2.restaurant_name from chain_master T1 left join restaurant_master T2 on T1.managed_restaurent=T2.restaurant_id where T1.id='$id' AND T2.status ='Y'");
        return $data->row_array();
    }

	function getChainData_1($id) {
 

       $data = $this->db->query("select T1.* from chain_rest_child T1 where T1.rest_id='$id'");
        $data1 = $data->row_array();
     $rest_id =$data1['chain_id']; 
	   $data = $this->db->query("SELECT T1.chain_id, T2 . *
FROM chain_rest_child T1
LEFT JOIN restaurant_master T2 ON T1.rest_id = T2.restaurant_id
WHERE T1.chain_id = '$rest_id' AND T2.status ='Y'");

        return $data->result_array();
    }
	
	
	
	
    function update_chain($array, $cond, $id, $child) {
        $data = $this->db->query("select * from chain_rest_child where chain_id='$id'");
        $re = $data->result_array();
        foreach ($re as $res) {
            $this->db->update("restaurant_master", array("chain_flag" => "N"), array("restaurant_id" => $res['rest_id']));
        }

        $this->db->delete("chain_rest_child", array("chain_id" => $id));
        foreach ($child as $rest) {
            $this->db->insert("chain_rest_child", array("rest_id" => $rest, "chain_id" => $id));
            $this->db->update("restaurant_master", array("chain_flag" => "Y"), array("restaurant_id" => $rest));
        }
        return $this->db->update("chain_master", $array, $cond);
    }

    function insert_chain($array, $child) {
        $this->db->insert("chain_master", $array);
        $id = $this->db->insert_id();
        foreach ($child as $rest) {
            $this->db->insert("chain_rest_child", array("rest_id" => $rest, "chain_id" => $id));
            $this->db->update("restaurant_master", array("chain_flag" => "Y"), array("restaurant_id" => $rest));
        }
        return TRUE;
    }

    function getSelectRest($id) {
        $data = $this->db->query("select T2.restaurant_id,T2.restaurant_name,T2.formatted_address  from chain_rest_child T1 left join restaurant_master T2 on T2.restaurant_id=T1.rest_id where T1.chain_id=$id");
        return $data->result_array();
    }

    function getNonSelectRest($id) {
        $data = $this->db->query("SELECT T1.restaurant_id,T1.restaurant_name,T1.formatted_address from restaurant_master T1 WHERE ( SELECT count(T2.id) from chain_rest_child T2 WHERE T2.rest_id= T1.restaurant_id )=0");
        return $data->result_array();
    }

    function deleteC($id) {
        $dat = $this->db->query("select rest_id from chain_rest_child where chain_id=$id");
        $data = $dat->result_array();
        foreach ($data as $res) {
            $this->db->update("restaurant_master", array("chain_flag" => "N"), array("restaurant_id" => $res['rest_id']));
        }
        $this->db->delete("chain_rest_child", array("chain_id" => $id));
        return $this->db->delete("chain_master", array("id" => $id));
    }

    function getRestList() {
        $data = $this->db->query("select T1.restaurant_id,T1.restaurant_name,T1.formatted_address from restaurant_master T1 order by T1.restaurant_id desc");
        return $data->result_array();
    }

    function getChainLists($keyword, $status) {
        $qry = "select T1.*,(SELECT count(T3.id) from chain_rest_child T3 WHERE T3.chain_id = T1.id ) as count  ,T2.restaurant_name,T2.formatted_address from chain_master T1  LEFT JOIN restaurant_master T2 on T1.managed_restaurent=T2.restaurant_id where 1 ";
        if ($keyword != '') {
            $qry.=" and name like '%$keyword%' ";
        }if ($status != '') {
            $qry = " and status='$status'";
        }
        $data = $this->db->query($qry);
        return $data->result_array();
    }

    //put your code here
}
