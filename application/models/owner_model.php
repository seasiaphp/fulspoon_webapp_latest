<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Owner_model extends MY_Model {

    public $_table = 'restaurant_master';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function getOwnerDetails($check_data) {
        $qry = "select T1.*,T3.name as chain_name from restaurant_master T1 left join chain_rest_child T2 on T1.restaurant_id=T2.rest_id left join chain_master T3 on T2.chain_id=T3.id where T1.email='" . $check_data['email'] . "'";
//        $this->db->where($check_data);
//        $this->db->from('restaurant_master');
        $query = $this->db->query($qry);
        return $query->row_array();
    }

    #function to generate random Password

    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function updateOwner($data, $id) {
        if ($id) {
            $this->db->where('restaurant_id', $id);
            $result = $this->db->update('restaurant_master', $data);
            return;
        } else
            return 'error';
    }

}

?>