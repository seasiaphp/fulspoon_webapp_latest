<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class cron_model extends MY_Model {

    public function __construct() {
        $this->load->database();
//        date_default_timezone_set('GMT');
    }

    function get_curr_dish($rid, $dish_id) {
        $data = $this->db->query("select * from dish_options where dish_item_id='$dish_id' and restaurant_id='$rid'");
        return $data->result_array();
    }

    function get_opt_sides($id) {
        $data = $this->db->query("select * from option_sides where option_id='$id'");
        return $data->result_array();
    }

    function login_push() {
        $data = $this->db->query("select member_id from member_master where phone='' and(device_id!='' or device_tocken!='') and push_ph_flag='N' ");
        return $data->result_array();
    }

    function update1($db, $arr, $cond) {
        return $this->db->update($db, $arr, $cond);
    }

    function update_add_side($ar, $cn) {
        return $this->db->update("option_sides", $ar, $cn);
    }

    function get_config_details_decline() {
        $data = $this->db->query("select `value` from general_config where field='Order_decline_max_Time'");
        $dats = $data->row_array();
        return $dats['value'];
    }

    function update_size_locu($arr, $id) {
        $this->db->update("dish_item_size_map", $arr, array("map_id" => $id));
//        echo $this->db->last_query();
        return TRUE;
    }

    function update_regular_size($array, $cond, $map_id, $dish_id) {
        $this->db->update("dish_item_size_map", $array, $cond);
        $this->db->query("DELETE FROM dish_item_size_map WHERE item_id='$dish_id' and map_id!='$map_id'");
    }

    function get_regular_size($id) {
        $data = $this->db->query("select *, count(*) as count from dish_item_size_map where item_id='$id'");
        return $data->row_array();
    }

    function get_size_loc($id) {
        $data = $this->db->query("select * from dish_item_size_map where item_id='$id'");
        return $data->result_array();
    }

    function get_dish_ofMenu($id) {
        $this->db->update("menu_rest_master", array("updated_on" => date("Y-m-d H:i:s"), "created_on" => date("Y-m-d H:i:s")), array("id" => $id));
        $data = $this->db->query("select * from dish_master where menu='$id'");
        return $data->result_array();
    }

    function update_decline($id) {
        $this->db->update("order_master", array("order_status" => "Declined"), array("order_id" => $id));
//        echo $this->db->last_query()."<br/>";
        return true;
    }

    function get_rest_menu($id) {
        $data = $this->db->query("select T1.* from menu_rest_master T1 where T1.res_id='$id'");
        return $data->result_array();
    }

    function subscribtion_cron() {
        $sql = "SELECT IFNULL(package,package_log) AS package_recd,member_id,sub_recd,IFNULL(sub_recd,subscription_id) as up_sub_recrd FROM (SELECT 
			(SELECT subscription_id 
			FROM User_subscription_master 
			WHERE subscription_id>log.subscription_id AND member_id=log.member_id ORDER BY subscription_id DESC LIMIT 1) AS sub_recd,log.subscription_id,log.member_id,
			(SELECT package_id 
			FROM User_subscription_master 
			WHERE subscription_id>log.subscription_id AND member_id=log.member_id ORDER BY subscription_id DESC LIMIT 1) AS package,
			(SELECT package_id 
			FROM User_subscription_master 
			WHERE subscription_id=log.subscription_id AND member_id=log.member_id ORDER BY subscription_id DESC LIMIT 1 ) AS package_log
			FROM (SELECT * FROM subscription_log WHERE End_date=date(NOW())) as log ) as sub_recd GROUP BY member_id";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_last_stripid($member_id) {
        $sql = "SELECT * FROM member_stripid_map WHERE member_id='$member_id' ORDER BY id DESC limit 1";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function get_last_stripid_new($strip_id) {
        $sql = "SELECT * FROM member_stripid_map WHERE id='" . $strip_id . "'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function get_price($package_id) {
        $sql = "SELECT *,CONCAT(duration,' ',frequency) as durn FROM subscription_package WHERE sub_package_id='$package_id' AND active='Y'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function get_new_orders() {
        $data = $this->db->query("select T1.*,T2.timezone,T2.restaurant_id,T2.restaurant_name,T2.contact_number,T2.pickup_time,T2.delivery_time,T3.* from order_master T1 left join restaurant_master T2 on T1.restaurant_id=T2.restaurant_id left join member_master T3 on T1.member_id=T3.member_id where T1.order_status='New'");
        return $data->result_array();
    }

    function get_config_details() {
        $data = $this->db->query("select `value` from general_config where field='Twilo_call_time'");
        $return = $data->row_array();
        return $return['value'];
    }

    function get_accepted_orders() {
        $data = $this->db->query("select T1.*,T2.restaurant_name,T2.pickup_time,T2.delivery_time,T3.* from order_master T1 left join restaurant_master T2 on T1.restaurant_id=T2.restaurant_id left join member_master T3 on T1.member_id=T3.member_id where T1.order_status='Accepted'");
        return $data->result_array();
    }

    function update_complete($order_id) {
        $sql = "UPDATE order_master SET order_status='Completed', order_completed_time='" . date("Y-m-d H:i:s") . "',changed_method='crone' WHERE order_id =$order_id";
        $query = $this->db->query($sql);
        return true;
    }

    function get_restaurant($subscription_id) {
        $sql = "SELECT * FROM current_restaurant WHERE subscription_id='$subscription_id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_restaurant_new($subscription_id) {
        $sql = "SELECT * FROM restaurants_next WHERE next_subscription_id='$subscription_id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function insert_1($table, $data) {
        //print_r($data);exit;
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function upddata($current_id, $data) {
        extract($data);
        $this->db->where('current_id', $current_id);
        $this->db->update('current_restaurant', $data);
        return true;
    }

    public function upddata_new($sub_id, $data) {
        extract($data);
        $this->db->where('subscription_id', $sub_id);
        $this->db->update('subscription_next', $data);
        return true;
    }

    public function upddata_user_sub_master($subscription_id, $data) {
        extract($data);
        $this->db->where('subscription_id', $subscription_id);
        $this->db->update('User_subscription_master', $data);
        return true;
    }

    function get_user_email($member_id) {
        $sql = "SELECT * FROM member_master WHERE member_id='$member_id'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function subscribtion_cron_new() {
        $sql = "SELECT A1.strip_id as prev_strip,IFNULL(A2.package_id,A3.package_id) AS package_recd ,A2.subscription_id AS new_subscription_id ,A2.strip_id,A2.package_id AS  new_package_id,
	 A1.subscription_id,A1.member_id,A3.package_id
     FROM subscription_log A1 
     LEFT JOIN subscription_next A2 
     ON A1.member_id=A2.member_id  AND A2.`status`='Y'
     LEFT JOIN User_subscription_master A3 ON A3.subscription_id=A1.subscription_id
     WHERE A1.End_date=date(NOW()) ";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

}

?>