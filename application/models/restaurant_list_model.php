<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Restaurant_list_model extends MY_Model {

    public $_table = 'restaurant_master';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function adddetails($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function checkChainMain($id) {
        $data = $this->db->query("select count(*) as count from chain_master where managed_restaurent=" . $id);
        $result = $data->row_array();
        return $result['count'];
    }

    public function usernameExist($username) {

        $this->db->where('username', $username);
        $this->db->from('member_admins');
        return $this->db->count_all_results();
    }

    public function getRestaurantCount($key = '', $status) {


        //return $this->db->count_all_results('restaurant_master');
        $sql = "SELECT COUNT(*) as num FROM restaurant_master WHERE 1=1";
        if ($key != '') {
            $sql .= " AND (restaurant_master.restaurant_name LIKE '%$key%' )";
        }
        if ($status)
            $sql .= " AND restaurant_master.status = '$status' and signed_flag='Y' ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function getRestaurantLists($status, $key, $num, $offset) {



        $sql = "SELECT * FROM restaurant_master WHERE 1=1 and signed_flag='Y' ";

        if ($status)
            $sql .= " AND restaurant_master.status = '$status' ";

        if ($key != '') {
            $sql .= " AND (restaurant_master.restaurant_name LIKE '%$key%' )";
        }

        $sql .= " GROUP BY restaurant_id";
        $sql .= " order by status desc,restaurant_id desc";

        if ($offset)
            $sql.=" limit $offset,$num";
        else
            $sql.=" limit $num";


        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAllDetails($res_id) {

        $sql = "SELECT T1.cusine_id FROM `restauarnt_cusine_map` T1 LEFT JOIN cusine_master T2 ON T1.cusine_id=T2.cusine_id WHERE rest_id='$res_id' AND T2.active='Y'";

        $query = $this->db->query($sql);
        $query = $query->result_array();
        return $query;
    }

    public function getAllDetails_Rest($res_id) {

        $sql = "SELECT * FROM `restaurant_master` WHERE restaurant_id='$res_id'";
        $query = $this->db->query($sql);
        $query = $query->row_array();
        return $query;
    }

    public function UpdateDetails($table, $data, $where) {

        $this->db->where($where);
        $this->db->update($table, $data);
        return true;
    }

    public function bulkDelete($res_id) {

        foreach ($res_id as $res) {
            $sql = "DELETE p, pa
      FROM restaurant_master p
      LEFT JOIN restauarnt_cusine_map pa ON pa.rest_id = p.restaurant_id
     WHERE  p.restaurant_id ='$res'";
            $query = $this->db->query($sql);
        }

        return;
    }

    public function bulkUpdate($res_id, $status) {

        $res_id = implode(",", $res_id['restaurant_id']);
        $status = ($status['status']);
        $sql = "UPDATE restaurant_master SET `status`='$status' WHERE restaurant_id IN ($res_id)";
        $query = $this->db->query($sql);
        echo $this->db->last_query();
    }

    public function UpdateDetails_subscription_package($block, $id) {
        $sql = "UPDATE subscription_package SET `active`='$block' WHERE sub_package_id ='$id'";
        $query = $this->db->query($sql);
        return;
    }

    public function getRestaurant() {
        $sql = "SELECT restaurant_id,name FROM restaurant_master";

        $result = $this->db->query($sql);
        $results = $result->result_array();
        return $results;
    }

    public function UpdateDetails_rest_data($block, $id) {
        $sql = "UPDATE subscription_package SET `active`='$block' WHERE sub_package_id ='$id'";
        $query = $this->db->query($sql);
        return;
    }

    public function getAllDetails_cuisine($res_id) {

        $sql = "SELECT * FROM `cusine_master` WHERE active='Y'";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        return $query;
    }

    public function UpdateDetails_cuisine_data($cu_id, $res_id) {
        $sql = "UPDATE restauarnt_cusine_map SET `cusine_id`='$res_id' WHERE res_cusine_id ='$cu_id'";

        $query = $this->db->query($sql);
        return;
    }

    public function Delete_cuisine($res_id) {

        $sql = "DELETE FROM restauarnt_cusine_map WHERE rest_id = '" . $res_id . "'";
        $query = $this->db->query($sql);
        return;
    }

    public function insert_cuisine($res_id, $cusine_id) {

        $sql = "INSERT INTO restauarnt_cusine_map (rest_id,cusine_id) VALUES ('" . $res_id . "','" . $cusine_id . "')";

        $query = $this->db->query($sql);

        return;
    }

    public function genaral_config() {

        $sql = "SELECT `value` FROM `general_config` WHERE field='feature_filter'";
        $query = $this->db->query($sql);
        $query1 = $query->row_array();
        return $query1;
    }

}

?>