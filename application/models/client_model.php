<?php

class Client_model extends CI_Model {

    public function __construct() {
        $this->load->database();
//        date_default_timezone_set('GMT');
    }

    public function insert($table, $data) {
        //print_r($data);exit;
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function update($table, $data, $cond) {

        return $this->db->update($table, $data, $cond);
    }

    public function delete_records($table, $data) {

        return $this->db->delete($table, $data);
    }

    function delete_old_locu($rid) {
        $this->db->query("delete from menu_rest_master where created_on < DATE_SUB(NOW(), INTERVAL 300 MINUTE) and res_id='$rid' and menu_name_original!=''");
        $this->db->query("delete from menu_rest_master where created_on < DATE_SUB(NOW(), INTERVAL 300 MINUTE) and res_id='$rid' and menu_name_original!=''");
//        $this->db->query("delete from dish_master where created_on < DATE_SUB(NOW(), INTERVAL 150 MINUTE) and rid='$rid'");
        return TRUE;
    }

    function get_calcValue() {
        $data = $this->db->query("select * from transactional_comparison order by tc_id desc limit 1");
        return $data->row_array();
    }

    function update_cat_dish_rest($dish_name, $option_grp, $desc, $price, $true_menu_name, $rest_id) {
        $menu_data = $this->db->query("select *,count(*) as count from menu_rest_master where menu_name_original='" . mysql_real_escape_string($true_menu_name) . "' and res_id='$rest_id'");
        $menu = $menu_data->row_array();
        if ($menu['count'] > 0) {
            $menu_id = $menu['id'];
            $this->db->update("menu_rest_master", array("created_on" => date("Y-m-d H:i:s")), array("id" => $menu_id));
            $dish_data = $this->db->query("select * from dish_master where rid='$rest_id' and menu='$menu_id'");
            $dish_array = $dish_data->result_array();
            $flag = 0;
            foreach ($dish_array as $dish) {
                if ($dish['dish_name'] == $dish_name) {
                    $flag = 1;
                    $dish_id = $dish['id'];
                    $this->db->update("dish_master", array("created_on" => date("Y-m-d H:i:s")), array("id" => $dish_id));
                    if ($dish_name != $dish['dish_name_orginal']) {
                        $this->db->update("dish_master", array("dish_name" => $dish_name, "dish_name_orginal" => $dish_name), array("id" => $dish_id));
                    }
                    if ($desc != $dish['description_original']) {
                        $this->db->update("dish_master", array("description_original" => $desc, "description" => $desc), array("id" => $dish_id));
                    }
                    if ($price != $dish['price_original']) {
                        $this->db->update("dish_master", array("price_original" => $desc, "price" => $desc), array("id" => $dish_id));
                    }
                    foreach ($option_grp as $option_g) {
                        foreach ($option_g as $options) {
                            $name_op = $options->name;
                            $dish_qry = $this->db->query("select * from dish_master where restaurant_id='$rest_id' and dish_item_id='$dish_id' and option_name_original='$name_op'");
                            $dish_op = $dish_qry->row_array();
                            $price_op = $options->price;
                            $dish_array = array("restaurant_id" => $rest_id, "dish_item_id" => $dish_id, "option_name" => $name, "price" => $price_op, "price_original" => $price, "option_name_original" => $name);
                            $this->member_model->ins_dish($dish_array);
                        }
                    }
                }
            }
            if ($flag == 0) {
                $arr = array("dish_name" => $dish_name, "description" => $desc, "price" => $price, "dish_name_orginal" => $dish_name, "description_original" => $desc, "price_original" => $price, "menu" => $menu_id, "rid" => $rest_id);
                $this->db->insert("dish_master", $arr);
            }
        } else {
            $this->db->insert("menu_rest_master", array("menu_name" => $true_menu_name, "menu_name_original" => $true_menu_name, "res_id" => $rest_id));
            $menu_id = $this->db->insert_id();
            $arr = array("dish_name" => $dish_name, "description" => $desc, "price" => $price, "dish_name_orginal" => $dish_name, "description_original" => $desc, "price_original" => $price, "menu" => $menu_id, "rid" => $rest_id);
            $this->db->insert("dish_master", $arr);
        }
        return TRUE;
    }

    public function get_where($table, $data, $order_by = "id ASC", $type = "row") {

        $query = $this->db->order_by($order_by)->get_where($table, $data);

        if ($type == "row") {
            return $query->row_array();
        } else {

            return $query->result_array();
        }
    }

    public function updateUser($data) {

        $user_id = $data['member_id'];

        $this->db->update('member_master', array('password' => $data['password']), array('member_id' => $user_id));

        return $this->getUserDetails($user_id);
    }

    public function getUserDetails($id) {
        $this->db->select('*');
        $this->db->from('member_master');
        //	$this->db->join("member_details","member_details.member_id=member_master.member_id","left");
        $this->db->where('member_master.member_id', $id);
        $result = $this->db->get();
        //echo $str = $this->db->last_query();
        $resu = $result->result_object();
        return $resu[0];
    }

    #function to generate random Password

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function cancelOrder($order_id) {
        $sql = "UPDATE order_master SET order_status='Cancelled' WHERE order_id=$order_id";
        $query = $this->db->query($sql);
        return true;
    }

    public function favouriteLists($member_id, $location_id) {
        $sql = "SELECT member_fav_items.fav_id,dc.category_id,dc.category_name,dc.location_id,dc.restaurant_id,
					di.item_id,di.item_description,di.item_name,
					(SELECT MIN(price) as price from dish_item_size_map where item_id=di.item_id) as price
					FROM member_fav_items 
					INNER JOIN dish_items_master di
					ON member_fav_items.item_id = di.item_id
					INNER JOIN dish_category_master dc  
					ON(dc.category_id = di.category_id)
					WHERE member_fav_items.member_id=$member_id
					AND dc.location_id=$location_id
					AND dc.status = 'Y' 
					AND di.status='Y' 
					ORDER BY member_fav_items.created_time DESC
					";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function removeFavourite($member_id, $item_id) {
        $sql = "DELETE FROM member_fav_items  WHERE member_fav_items.member_id=$member_id and member_fav_items.item_id=$item_id";
        $query = $this->db->query($sql);
        return;
    }

    public function isFavItem($member_id, $item_id) {
        $sql = "SELECT fav_id FROM member_fav_items  WHERE member_fav_items.member_id=$member_id and member_fav_items.item_id=$item_id";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function get_rest($user_id) {
        $data = $this->db->query("SELECT T1.subscription_id FROM `subscription_log` T1 WHERE End_date>=CURRENT_DATE() AND T1.member_id='$user_id'");
        $data_log = $data->row_array();

        $subscr_id = $data_log['subscription_id'];
        $data_pur = $this->db->query("select restaurant_id from current_restaurant where subscription_id='$subscr_id' and member_id='$user_id'");
//         echo $this->db->last_query();
//        exit;
        return $data_pur->result_array();
    }

	function get_rest_user($rest_id) {
     

     
         $data_pur = $this->db->query("select * from current_restaurant where restaurant_id='".$rest_id."'");
         $data_log = $data_pur->result_array();
      
        return $data_log;
    }
	
	
	
    public function getAllOders($member_id, $offset, $order_id = '', $limit) {

        $server_timezone = date_default_timezone_get();
        if ($server_timezone == 'GMT') {
            $server_offset = '+0:00';
        } else {
            $dateTimeZone = new DateTimeZone($server_timezone);
            $date = new DateTime("now", $dateTimeZone);
            $server_offset = $dateTimeZone->getOffset($date);
        }

        $sql = "SELECT OM.order_id,
						   OM.order_ref_id,
						   OM.restaurant_id,
						   OM.location_id,
						   rl.restaurant_name,
						   OM.member_id,
						   OM.total_amount,
						   OM.order_status,
						   OM.payment_status,
						   OM.order_type,
						   OM.is_later,
						   OM.delivery_time,
						   OM.tip,
						   DATE_FORMAT(CONVERT_TZ(OM.created_time,'" . $server_offset . "','" . $offset . "'),'%Y-%m-%d %H:%i:%s') as created_time
						   FROM order_master OM
						   LEFT JOIN restaurant_locations rl
						   ON rl.location_id =  OM.location_id
						   
					WHERE  OM.member_id = '$member_id' and status='Y'";

        if ($order_id != '')
            $sql.=" and OM.order_id=" . $order_id;

        $sql.=" ORDER BY created_time DESC 
					";

        if ($limit)
            $sql.= " LIMIT 0,$limit";

        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function getAllOderItemDetails($order_id) {
        $sql = "SELECT OI.*,DIM.* FROM order_items OI
					LEFT JOIN dish_items_master DIM on DIM.item_id=OI.item_id 
					WHERE OI.order_id = '$order_id'
					";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function getOderItemDetails($order_item_id) {
        $sql = "SELECT OI.*,DIM.* FROM order_items OI
					LEFT JOIN dish_items_master DIM on DIM.item_id=OI.item_id 
					WHERE OI.ord_item_id = '$order_item_id'
					";
        $query = $this->db->query($sql);
        $res = $query->row_array();
        return $res;
    }

    public function getAllOderSidesDetails($ord_item_id) {
        $sql = "SELECT * FROM order_option_map 
					WHERE ord_item_id = '$ord_item_id'
					";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    function get_categary_list() {
        $sql = "select *  from category_master WHERE  status ='Y' ";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    }

    public function getRestaurant($data, $radius) {

        if ($data['lat'] && $data['long']) {
            $sql = "SELECT * FROM (SELECT r.location_id,r.restaurant_id,r.restaurant_name,r.description,
					 (case when (r.type ='Both' ) THEN 'Delivery - Pickup' ELSE r.type  END) as type ,
					r.address,r.phone,r.latitude,r.longitude,r.city,r.state,r.zip_code,
			(
				  3959 * acos (
				  cos ( radians(" . $data['lat'] . ") )
				  * cos( radians( latitude ) )
				  * cos( radians( longitude ) - radians(" . $data['long'] . ") )
				  + sin ( radians(" . $data['lat'] . ") )
				  * sin( radians( latitude ) )
				)
			) AS distance FROM restaurant_locations  r
			  INNER JOIN restaurant_master rm 
			  ON r.restaurant_id = rm.restaurant_id
			  LEFT JOIN dish_category_master  dc
			  ON r.location_id = dc.location_id
			  WHERE  r.latitude!=0 and r.longitude!=0 and rm.status='Y' and  r.restaurant_id =" . $data['restaurant_id'] . "  GROUP BY r.location_id ) temp ";

            if ($radius != 0)
                $sql .= " WHERE temp.distance<=" . $radius;

            $sql.=" ORDER BY temp.distance";
        }
        else {
            $sql = "SELECT r.location_id,r.restaurant_id,r.restaurant_name,r.description,
			 (case when (r.type ='Both' ) THEN 'Delivery - Pickup' ELSE r.type  END) as type ,
			  r.address,r.city,r.state,r.zip_code,r.phone,r.latitude,r.longitude
			  FROM restaurant_locations  r
			  INNER JOIN restaurant_master rm 
			  ON r.restaurant_id = rm.restaurant_id
			  LEFT JOIN dish_category_master  dc
			  ON r.location_id = dc.location_id
			  WHERE  r.latitude!=0 and r.longitude!=0 and rm.status='Y' and  r.restaurant_id =" . $data['restaurant_id'] . "  GROUP BY r.location_id ";
        }
        $query = $this->db->query($sql);
        $res = $query->result_array();

        return $res;
    }

    public function getRestaurantDetails($location_id) {
        $sql = "SELECT r.location_id,r.restaurant_id,r.restaurant_name,r.description,
			 (case when (r.type ='Both' ) THEN 'Delivery - Pickup' ELSE r.type  END) as type ,
			  r.address,r.city,r.state,r.zip_code,r.timezone,r.phone,r.latitude,r.longitude,GROUP_CONCAT(DISTINCT dc.category_name) as category_name 
			  FROM restaurant_locations  r
			  INNER JOIN restaurant_master rm 
			  ON r.restaurant_id = rm.restaurant_id
			  LEFT JOIN dish_category_master  dc
			  ON r.location_id = dc.location_id
			  WHERE  rm.status='Y' and  r.location_id =" . $location_id . "  GROUP BY r.location_id ";
        //echo $sql;exit;	  
        $query = $this->db->query($sql);
        $res = $query->row_array();

        return $res;
    }

    public function getRestaurantMenu($data) {
        $res = array();
        if ($data['location_id']) {

            $sql = "		
				SELECT dc.category_id,dc.category_name,dc.location_id,dc.restaurant_id,dc.sortorder as category_sort,
				dc.subtitle,di.item_id,di.item_description,di.item_name,di.sortorder as dish_sort,
				(SELECT MIN(price) as price from dish_item_size_map where item_id=di.item_id) as price
				FROM dish_items_master di
				INNER JOIN dish_category_master dc  
				ON(dc.category_id = di.category_id)
				WHERE dc.location_id = '" . $data['location_id'] . "' 
				AND dc.restaurant_id='" . $data['restaurant_id'] . "' 
				AND dc.status = 'Y' 
				AND di.status='Y' 
				ORDER BY category_sort,dish_sort
			";

            $query = $this->db->query($sql);
            $res = $query->result_array();

            return $res;
        }
    }

    public function getRestaurantMenuDetail($id) {
        $res = array();

        if ($id) {

            $sql = "SELECT dish_items_master.*,restaurant_locations.location_id,dish_options.option_id,dish_options.option_name,
				dish_options.sortorder,dish_options.mandatory,dish_options.multiple,dish_options.limit,option_sides.side_id,
				option_sides.side_item,option_sides.sortorder as sidesortorder,option_sides.price as side_price		
				FROM dish_items_master
				LEFT JOIN  dish_options
				ON dish_items_master.item_id=dish_options.dish_item_id and dish_options.status!='N'
				LEFT JOIN  option_sides
				ON option_sides.option_id = dish_options.option_id
				INNER JOIN dish_category_master
				ON dish_items_master.category_id = dish_category_master.category_id
				INNER JOIN restaurant_locations
				ON restaurant_locations.location_id = dish_category_master.location_id
				WHERE dish_items_master.item_id=$id and dish_category_master.`status`='Y'
				and dish_items_master.`status`='Y'
				ORDER BY dish_options.sortorder,sidesortorder
			";

            $query = $this->db->query($sql);
            $res = $query->result_array();


            return $res;
        }
    }

    public function getRestaurantMenuSizeDetail($id) {
        $sql = "SELECT price,size
				FROM dish_item_size_map
				WHERE item_id = $id
				ORDER BY price ASC
				";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function checkCartResId($id) {
        $sql = "SELECT restaurant_id 
				FROM order_cart_items
				WHERE order_cart_items.member_id = $id
				GROUP BY restaurant_id";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    public function checkCartDetail($check_array) {

        $result = $this->db->get_where('order_cart_items', $check_array);
        if ($result->num_rows() != 0) {
            return $result->row_array();
        } else {
            return 0;
        }
    }

    public function getCartQty($check_array) {

        $result = $this->getCartDetails($check_array);
        return count($result);
    }

    public function addCartDetail($data) {

        $this->db->insert('order_cart_items', $data);
        return $this->db->insert_id();
    }

    public function updateCartDetail($data, $check_array) {
        $this->db->where($check_array);
        $this->db->update('order_cart_items', $data);
    }

    public function removeCartDetail($check_array) {
        $this->db->where($check_array);
        $this->db->delete('order_cart_items');
    }

    public function getCartDetails($check_array) {
        $sql = "SELECT order_cart_items.cart_id,
						dish_items_master.item_id,
						dish_items_master.item_name,
						dish_item_size_map.size,
						dish_item_size_map.price,
						order_cart_items.quantity,
						order_cart_items.item_size,
						order_cart_items.restaurant_id,
						order_cart_items.quantity * dish_item_size_map.price as item_total
			FROM order_cart_items
			INNER JOIN dish_items_master
			ON dish_items_master.item_id = order_cart_items.item_id
			INNER JOIN dish_item_size_map
			ON dish_item_size_map.map_id = order_cart_items.item_size
			WHERE dish_items_master.`status`='Y' and dish_item_size_map.`status`='Y' and order_cart_items.member_id = " . $check_array['member_id'];

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function deleteInactiveItems($check_array) {
        $sql = "DELETE order_cart_items FROM order_cart_items
				INNER JOIN dish_items_master
				ON dish_items_master.item_id = order_cart_items.item_id
				INNER JOIN dish_category_master
				ON dish_category_master.category_id = dish_items_master.category_id
				WHERE  ( dish_category_master.`status`='N' || dish_items_master.`status`='N' )  and order_cart_items.member_id = " . $check_array['member_id'];

        $result = $this->db->query($sql);
    }

    public function getCartTotal($check_array) {
        $sql = "SELECT SUM(order_cart_items.quantity * dish_item_size_map.price) as total
			FROM order_cart_items
			INNER JOIN dish_items_master
			ON dish_items_master.item_id = order_cart_items.item_id
			INNER JOIN dish_item_size_map
			ON dish_item_size_map.map_id = order_cart_items.item_size
			WHERE dish_items_master.`status`='Y' and dish_item_size_map.`status`='Y' and order_cart_items.member_id = " . $check_array['member_id'] . " GROUP BY order_cart_items.member_id";

        $result = $this->db->query($sql);

        return $result->row_array();
    }

    public function getOrderRefId() {
        $this->db->select_max('order_ref_id');
        $result = $this->db->get('order_master')->row();
        $ref_id = $result->order_ref_id;
        if ($ref_id < 10000) {
            $ref_id = 10001;
        } else {
            $ref_id = $ref_id + 1;
        }
        return $ref_id;
    }

    public function insertOrder($data) {
        $this->db->insert('order_master', $data);
        return $this->db->insert_id();
    }

    public function insertOrderItem($data) {
        $this->db->insert('order_items', $data);
        return $this->db->insert_id();
    }

    public function getPendingOrder($user_id) {
        if ($user_id) {
            $result = $this->db->get_where('order_master', array('member_id' => $user_id, 'order_status' => 'Pending'));
            if ($result->num_rows() != 0) {
                return $result->row()->order_id;
            }
        } else {
            return 0;
        }
    }

    public function deletePendingOrder($order_id) {
        if ($order_id) {
            $this->db->where('order_id', $order_id);
            $this->db->delete('order_master');

            $this->db->where('order_id', $order_id);
            $this->db->delete('order_items');
        }
    }

    public function getOrderItems($order_id) {
        if ($order_id) {
            $sql = "SELECT  order_items.order_id,
							dish_items_master.item_id,
							dish_items_master.item_name,
							dish_item_size_map.size,
							dish_item_size_map.price,
							order_items.quantity,
							order_items.item_size,
							order_items.price
				FROM order_items
				INNER JOIN dish_items_master
				ON dish_items_master.item_id = order_items.item_id
				INNER JOIN dish_item_size_map
				ON dish_item_size_map.map_id = order_items.item_size
				WHERE order_items.order_id = " . $order_id;

            $result = $this->db->query($sql);

            return $result->result_array();
        }
    }

    public function getFinalOrder($user_id, $order_ref_id) {
        if ($user_id) {
            $result = $this->db->get_where('order_master', array('member_id' => $user_id, 'order_ref_id' => $order_ref_id, 'order_status' => 'Pending'));
            if ($result->num_rows() != 0) {
                return $result->row()->order_id;
            }
        } else {
            return 0;
        }
    }

    public function setOrderSucess($order_id) {
        if ($order_id) {

            $this->db->where('order_id', $order_id);
            $this->db->update('order_master', array('order_status' => 'New'));

            $this->db->where('order_id', $order_id);
            $this->db->delete('order_cart_items');
        }
    }

    public function getOrder($order_ref_id) {
        $this->db->where('order_ref_id', $order_ref_id);
        $result = $this->db->get('order_master');
        return $result->row_array();
    }

    public function getDetailOrder($order_id) {
        $sql = "SELECT MM.first_name,MM.last_name,MM.auth_type,RL.restaurant_name,RL.type,OM.order_status,OM.order_id,OM.order_ref_id,OM.created_time
				FROM order_master OM 
				LEFT JOIN member_master MM ON OM.member_id = MM.member_id
				LEFT JOIN restaurant_locations RL ON OM.location_id = RL.location_id
				WHERE OM.order_id =$order_id";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getDetailOption($ord_item_id) {
        $sql = "SELECT * 
				FROM order_option_map
				WHERE ord_item_id  =$ord_item_id";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getDetailItem($order_id) {
        $sql = "SELECT OI.*,DIM.*
				FROM order_items OI
				LEFT JOIN dish_items_master DIM ON OI.item_id = DIM.item_id
				WHERE OI.order_id =$order_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getDeliveryAddress($id) {
        $sql = "SELECT da.address,da.city,da.zipcode,da.notes,da.created_date,da.status,da.state FROM delivery_address da 
				LEFT JOIN order_master om ON da.order_id = om.order_id
				WHERE om.member_id = $id
				ORDER BY created_date DESC
				LIMIT 0,1";
        $result = $this->db->query($sql);

        return $result->row_array();
    }

    public function getRestaurantTimeDetails($location_id, $offset, $rest_offset) {

        $sql = "SELECT day,start_at,end_at FROM  (
				SELECT day,start_at,end_at  	FROM restaurant_opening_times
				WHERE location_id  = $location_id
				UNION 
				
				SELECT day_name,start_at,end_at 
								FROM days_name
				WHERE
				days_name.day_name NOT IN(SELECT day
				FROM restaurant_opening_times
				WHERE location_id  =$location_id)) tmp ORDER BY FIELD(day, 'sunday', 'monday', 'tuesday','wednesday','thursday','friday','saturday')";

        /* $sql = "SELECT day,start_at,end_at,start_at_convert ,end_at_convert FROM  (
          SELECT day,start_at as start_at,end_at as end_at,DATE_FORMAT(CONVERT_TZ(CONCAT(CURDATE(),' ',start_at),'".$rest_offset."','".$offset."'),'%H:%i:%s') as start_at_convert,DATE_FORMAT(CONVERT_TZ(CONCAT(CURDATE(),' ',end_at),'".$rest_offset."','".$offset."'),'%H:%i:%s') as end_at_convert  FROM restaurant_opening_times
          WHERE location_id  = $location_id
          UNION

          SELECT day_name,start_at,end_at,start_at,end_at
          FROM days_name
          WHERE
          days_name.day_name NOT IN(SELECT day
          FROM restaurant_opening_times
          WHERE location_id  =$location_id)) tmp ORDER BY FIELD(day, 'sunday', 'monday', 'tuesday','wednesday','thursday','friday','saturday')"; */

        //echo '<pre>';echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getPromocode($data) {
        $this->db->where($data);
        $result = $this->db->get('promocodes');
        return $result->row_array();
    }

    public function updatePromoCount($id, $promocode, $restaurant_id) {
        $this->db->where(array('promocode' => $promocode, 'restaurant_id' => $restaurant_id));
        $result = $this->db->get('promo_order_map');
        $count = $result->num_rows();

        $this->db->where('promo_id', $id);
        $this->db->update('promocodes', array('total_uses' => $count));
    }

    public function getOwnerStripDetails($data) {
        return array('stripe_public_key' => $this->config->item('stripe_public_key'), 'stripe_private_key' => $this->config->item('stripe_private_key'));
    }

    public function getCustomerStripId($data) {
        $this->db->order_by("created_time", "desc");
        $this->db->where(array('member_id' => $data['member_id'], 'restaurant_id' => $data['restaurant_id'], 'location_id' => $data['location_id']));
        $query = $this->db->get('member_stripid_map');
        return $query->result_array();
    }

    public function getAdmin_strip() {
        $sql = "SELECT * FROM admin_master WHERE admin_id='1'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insert_user($data) {
        $this->db->insert('member_master', $data);
        return $this->db->insert_id();
    }

    public function insert_member_stripid_map($data) {
        $this->db->insert('member_stripid_map', $data);
        return $this->db->insert_id();
    }

}
