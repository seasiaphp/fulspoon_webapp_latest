<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Preference_model extends MY_Model {

    public $_table = 'preferences';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function get_all_conf_field_n_values() {
        $result = $this->get_many_by('display', 'Y');
        return $result;
    }

    function update_res($arr, $cond) {
        return $this->db->update("restaurant_master", $arr, $cond);
    }

    public function get_config_value($fieldname) {
        $result = $this->get_by(array('field' => $fieldname));
        return $result->value;
    }

    function get_restaurent($id) {
        $data = $this->db->query("select * from restaurant_master where restaurant_id='$id' ");
        return $data->row_array();
    }

    public function get_restConfig($location_id) {
        $sql = "SELECT p.value as test,gc.id,gc.units,gc.field,IFNULL(p.value, gc.value) AS value, 
				gc.title, gc.description FROM general_config gc 
				LEFT JOIN 
				preferences p
				ON p.config_id = gc.id and location_id='$location_id'
				WHERE gc.editable_to_restaurant='Y' AND gc.editable='Y'
				";

        $query = $this->db->query($sql);
        return $query->result();
    }
	
	public function get_where($table, $data, $order_by = "id ASC", $type = "row"){	
	
		$query   = $this->db->order_by($order_by)->get_where($table, $data);
		
		if($type == "row")
		{
			return  $query->row_array();
		}else{
		
			return $query->result_array();
		}
		
	}	

}
