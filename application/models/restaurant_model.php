<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Restaurant_model extends MY_Model {

    public $_table = 'restaurant_master';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function adddetails($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function usernameExist($username) {

        $this->db->where('username', $username);
        $this->db->from('member_admins');
        return $this->db->count_all_results();
    }

    public function getRestaurantCount($key = '') {

        //return $this->db->count_all_results('restaurant_master');
        $sql = "SELECT COUNT(*) as num FROM restaurant_master WHERE status='Y'";
        if ($key != '') {
            $sql .= " AND (restaurant_master.name LIKE '%$key%' OR 
					  restaurant_master.phone LIKE '%$key%' OR 
					  restaurant_master.address LIKE '%$key%')";
        }

        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function getRestaurantLists($status, $key, $num, $offset) {
        $manager_role = $this->config->item('manager_role_id') ? $this->config->item('manager_role_id') : 2;
        $start = $start ? $start : 0;
        $sql = "SELECT member_admins.admin_id, member_admins.full_name, 
					member_admins.username, restaurant_master.restaurant_id, 
					restaurant_master.name,restaurant_master.phone,restaurant_master.status
		  		FROM member_admins
				INNER JOIN restaurant_master 
					ON (member_admins.restaurant_id=restaurant_master.restaurant_id AND member_admins.role = $manager_role)
				WHERE 1=1 ";

        if ($status)
            $sql .= " AND restaurant_master.status = '$status' ";

        if ($key != '') {
            $sql .= " AND (restaurant_master.name LIKE '%$key%' OR 
					  restaurant_master.address LIKE '%$key%' OR 
					  member_admins.username LIKE '%$key%'OR 
					  member_admins.full_name LIKE '%$key%')";
        }

        $sql .= " GROUP BY restaurant_master.restaurant_id";

        if ($offset)
            $sql.=" limit $offset,$num";
        else
            $sql.=" limit $num";


        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAllDetails($res_id) {

        $sql="SELECT * FROM admin_master WHERE  username='$username'";
        $query = $this->db->query($sql);
        $query = $query->row_array();
        return $query;
    }

    public function UpdateDetails($table, $data, $where) {

        $this->db->where($where);
        $this->db->update($table, $data);
        return true;
    }

    public function bulkDelete($res_id) {

        if (is_array($res_id)) {
            $res_id = implode(",", $res_id);
        }

        $sql = "DELETE FROM member_admins,restaurant_master USING 
				member_admins INNER JOIN restaurant_master on 
				(member_admins.restaurant_id = restaurant_master.restaurant_id)
				WHERE restaurant_master.restaurant_id IN($res_id)";
        $query = $this->db->query($sql);
    }

    public function bulkUpdate($res_id, $status) {

        $res_id = implode(",", $res_id['restaurant_id']);
        $status = ($status['status']);
        $sql = "UPDATE restaurant_master SET `status`='$status' WHERE restaurant_id IN ($res_id)";
        $query = $this->db->query($sql);
        echo $this->db->last_query();
    }
 public function UpdateDetails_subscription_package($block, $id) {
        $sql = "UPDATE subscription_package SET `active`='$block' WHERE sub_package_id ='$id'";
	
        $query = $this->db->query($sql);
		return;
    }
    public function getRestaurant() {
        $sql = "SELECT restaurant_id,name FROM restaurant_master";

        $result = $this->db->query($sql);
        $results = $result->result_array();
        return $results;
    }
	
	

}

?>