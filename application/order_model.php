<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends MY_Model {

//    public function __construct() {
//        $this->load->database();
//        date_default_timezone_set('GMT');
//    }

    public $_table = 'order_master';

    public function setTable($table) {
        $this->_table = $table;
    }

    public function getOrderDetails($restaurant_id, $location_id, $num, $offset, $role) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name 
				FROM order_master A
				LEFT JOIN member_master B on A.member_id=B.member_id 
				LEFT JOIN restaurant_locations C on A.location_id=C.location_id
				WHERE order_status='New' 
				";
        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";
        $sql.=" AND A.payment_status='Y' ";
        $sql.=" ORDER BY A.order_id DESC ";

        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function countNewOrders($restaurant_id, $location_id, $role) {
        $sql = "SELECT count(*) as num
					FROM order_master A
					WHERE order_status='New' AND A.payment_status='Y'";
        if ($role == '3')
            $sql.= " AND  location_id=$location_id	";
        else
            $sql.= " AND  restaurant_id=$restaurant_id	";

        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function getActiveOrders($restaurant_id, $location_id, $searchOrder, $num, $offset, $role) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name 
					FROM order_master A
					LEFT JOIN member_master B on A.member_id=B.member_id 
					LEFT JOIN restaurant_locations C on A.location_id=C.location_id
					WHERE order_status='" . $searchOrder . "'

					";

        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";

        $sql.=" AND A.payment_status='Y' ";

        $sql.= " ORDER BY order_id  DESC ";


        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";

        $query = $this->db->query($sql);
        //echo  $this->db->last_query(); exit;	
        return $query->result_array();
    }

    public function countActiveOrders($restaurant_id, $location_id, $searchOrder, $role) {
        $sql = "SELECT count(*) as num
					FROM order_master A
					WHERE order_status='" . $searchOrder . "'
					
					";
        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";

        $sql.=" AND A.payment_status='Y' ";

        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function getCancelledOrders($restaurant_id, $location_id, $num, $offset, $role) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name 
					FROM order_master A
					LEFT JOIN member_master B on A.member_id=B.member_id
					LEFT JOIN restaurant_locations C on A.location_id=C.location_id
					WHERE order_status='Declined'
					";

        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";

        $sql.=" AND A.payment_status='Y' ";
        $sql.=" ORDER BY order_id  DESC ";
        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getLateOrders($restaurant_id, $location_id, $num, $offset, $role) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name 
					FROM order_master A
					LEFT JOIN member_master B on A.member_id=B.member_id
					LEFT JOIN restaurant_locations C on A.location_id=C.location_id
					 WHERE 1=1 
					";

        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";

        $sql.=" AND A.payment_status='Y' AND is_later='Y' AND order_status ='New' ";

        $sql.=" ORDER BY delivery_time ASC ";
        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function countCancelledOrders($restaurant_id, $location_id, $role) {
        $sql = "SELECT count(*) as num
					FROM order_master A
					WHERE order_status='Declined'
					";

        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";
        $sql.=" AND A.payment_status='Y' ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function countLateOrders($restaurant_id, $location_id, $role) {
        $sql = "SELECT count(*) as num
					FROM order_master A
					WHERE order_status='New'
					";

        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";
        $sql.=" AND A.payment_status='Y' AND is_later='Y' ";
        //echo $sql;			
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function getAllOrders($restaurant_id, $location_id, $num, $offset, $role) {
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,B.phone as user_phone,C.restaurant_name 
					FROM order_master A
					LEFT JOIN member_master B on A.member_id=B.member_id
					LEFT JOIN restaurant_locations C on A.location_id=C.location_id
					 WHERE  
					";

        if ($role == '3')
            $sql.= " A.location_id=$location_id	";
        else
            $sql.= "   A.restaurant_id=$restaurant_id	";
        $sql.=" AND A.payment_status='Y'";
        $sql.=" ORDER BY order_id  DESC ";
        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";


        $query = $this->db->query($sql);
//        echo $this->db->last_query();
//        exit;
        return $query->result_array();
    }

    public function countAllOrders($restaurant_id, $location_id, $role) {
        $sql = "SELECT count(*) as num
					FROM order_master A
					WHERE order_status!='Completed'
					";

        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";
        $sql.=" AND A.payment_status='Y' ";

        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['num'];
    }

    public function AllOrdersCount($restaurant_id, $location_id, $role) {

        if ($role == '3')
            $query = " AND  order_master.location_id=$location_id AND order_master.payment_status='Y'";
        else
            $query = " AND  order_master.restaurant_id=$restaurant_id	AND order_master.payment_status='Y'";


        $sql = "SELECT 
			  (SELECT count(*)	FROM order_master WHERE order_status='Declined' " . $query . " ) as cancelled ,
			  (SELECT count(*)  FROM order_master WHERE order_status='New'  " . $query . " ) as newcount,
			  (SELECT count(*) 	FROM order_master  WHERE order_status='Accepted' " . $query . " ) as accepted,
			  (SELECT count(*) 	FROM order_master  WHERE order_status!='Completed' ) as allcount,
			  (SELECT count(*) 	FROM order_master  WHERE is_later='Y'  " . $query . " AND order_status ='New'  ) as late
			  
			  ";
        //echo $sql;
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    public function acceptOrder($order_id) {
        $sql = "UPDATE order_master SET order_status='Accepted', order_accepted_time='" . date("Y-m-d H:i:s") . "' , changed_method='Portal'
				WHERE order_id =$order_id";
        $query = $this->db->query($sql);
        return true;
    }

    function get_order_details($order_id) {
        $data = $this->db->query("select T1.*,T2.restaurant_name,T2.pickup_time,T2.delivery_time,T3.* from order_master T1 left join restaurant_master T2 on T1.restaurant_id=T2.restaurant_id left join member_master T3 on T1.member_id=T3.member_id where T1.order_id='$order_id'");
        return $data->row_array();
    }

    public function completeOrder($order_id) {
        $sql = "UPDATE order_master SET order_status='Completed', order_completed_time='" . date("Y-m-d H:i:s") . "', changed_method='Portal' WHERE order_id =$order_id";
        $query = $this->db->query($sql);
        return true;
    }

    function get_rest_id($order_id) {
        $sql = $this->db->query("select restaurant_id from order_master where order_id='$order_id'");
        $ref = $sql->row_array();
        return $ref['restaurant_id'];
    }

    function get_Order_ref($order_id) {
        $sql = $this->db->query("select order_ref_id from order_master where order_id='$order_id'");
        $ref = $sql->row_array();
        return $ref['order_ref_id'];
    }

    public function cancelOrder($order_id, $refund_amount) {
        $sql = "UPDATE order_master SET order_status='Declined',refund_amount=$refund_amount,order_declined_time='" . date("Y-m-d H:i:s") . "', changed_method='Portal' WHERE order_id=$order_id";
        $query = $this->db->query($sql);
        return true;
    }

    public function refundOrder($order_id, $refund_amount) {
        $sql = "UPDATE order_master SET refund_amount=$refund_amount WHERE order_id=$order_id";
        $query = $this->db->query($sql);
        return true;
    }

    public function getOrderInfo($order_id, $location_id) {
        $sql = "SELECT OM.order_id,OM.order_ref_id,OM.member_id,OM.created_time,OM.total_amount,
				DIM.item_name,DIM.price,DIM.category_id,
				OI.unit_price,OI.quantity,OI.price,
				MM.first_name,MM.lasr_name
				FROM order_master OM 
				LEFT JOIN order_items OI ON OM.order_id = OI.order_id
				LEFT JOIN dish_items_master DIM ON OI.item_id = DIM.item_id
				LEFT JOIN member_master MM ON OM.member_id = MM.member_id
				WHERE OM.order_id =$order_id AND location_id='$location_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /* public function getDetailOrder($order_id){
      $sql = "SELECT MM.first_name,MM.last_name,MM.auth_type,OM.order_status,
      OM.created_time,RL.restaurant_name,RL.type,OI.*,DIM.*,DO.*
      FROM order_master OM
      LEFT JOIN member_master MM ON OM.member_id = MM.member_id
      LEFT JOIN restaurant_locations RL ON OM.location_id = RL.location_id
      LEFT JOIN order_items OI ON OM.order_id = OI.order_id
      LEFT JOIN dish_items_master DIM ON OI.item_id =DIM .item_id
      LEFT JOIN fk_dish_options DO ON OI.option_id = DO.option_id
      WHERE OM.order_id =$order_id";
      //echo $sql;exit;
      $query = $this->db->query($sql);
      return $query->result_array();

      } */

    public function getDetailOrder($order_id) {
        $sql = "SELECT MM.first_name,MM.last_name,MM.auth_type,MM.email,MM.phone as user_phone ,RL.restaurant_name,RL.type,OM.order_type,DA.*, 
				OM.order_status,OM.order_id,OM.order_ref_id,OM.created_time,OM.delivery_time,OM.tip,OM.discount_amount,OM.tax_amount,OM.sub_total,OM.refund_amount,OM.total_amount
				FROM order_master OM 
				LEFT JOIN delivery_address DA ON OM.order_id = DA.order_id 
				LEFT JOIN member_master MM ON OM.member_id = MM.member_id
				LEFT JOIN restaurant_locations RL ON OM.location_id = RL.location_id
				WHERE OM.order_id =$order_id";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getDetailOption($ord_item_id) {
        $sql = "SELECT * 
				FROM order_option_map
				WHERE ord_item_id  =$ord_item_id";
        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getDetailItem($order_id) {
        $sql = "SELECT OI.*,DIM.*
				FROM order_items OI
				LEFT JOIN dish_master DIM ON OI.item_id = DIM.id
				WHERE OI.order_id =$order_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function searchOrder($restaurant_id, $location_id, $searchOrder, $num, $offset, $role) {
        if ($searchOrder == '')
            $searchOrder = "Accepted";
        $sql = "SELECT A.*,B.first_name,B.last_name,B.auth_type,C.restaurant_name 
				FROM order_master A
				LEFT JOIN member_master B on A.member_id=B.member_id 
				LEFT JOIN restaurant_locations C on A.location_id=C.location_id
				WHERE order_status='" . $searchOrder . "'
				";
        if ($role == '3')
            $sql.= " AND  A.location_id=$location_id	";
        else
            $sql.= " AND  A.restaurant_id=$restaurant_id	";

        $sql.=" ORDER BY order_id  DESC ";
        if ($offset)
            $sql.="limit $offset,$num";
        else
            $sql.="limit $num";

        $query = $this->db->query($sql);



        //echo  $this->db->last_query(); exit;	
        return $query->result_array();
    }

    public function getOrderDetailsAdmin($key, $id) {

        /* $sql     = "SELECT order_master.*,restaurant_locations.restaurant_name, member_master.first_name,member_master.last_name,member_master.auth_type,restaurant_master.name
          FROM member_master INNER JOIN order_master
          INNER JOIN restaurant_master on restaurant_master.restaurant_id =order_master.restaurant_id
          INNER JOIN restaurant_locations
          ON (member_master.member_id =  order_master.member_id) AND
          AND restaurant_master.restaurant_id =restaurant_locations.restaurant_id
          WHERE  1=1 AND order_master.payment_status='Y' ";
          if($id != ''&& $id != 0 ){
          $sql	 .=" AND member_master.restaurant_id=$id";
          }
          if($key != ''){
          $sql  .= "  AND (member_master.first_name LIKE '%$key%' OR
          member_master.last_name LIKE '%$key%' OR
          restaurant_master.name LIKE '%$key%') ";

          }
          $sql  .= " GROUP BY member_master.member_id  "	; */

        $sql = "SELECT OM.*,RM.*,RL.*,MM.* FROM order_master OM
			  LEFT JOIN restaurant_master RM on RM.restaurant_id=OM.restaurant_id 
			  LEFT JOIN restaurant_locations RL on RL.location_id=OM.location_id 
			  LEFT JOIN member_master MM on MM.member_id=OM.member_id
			  WHERE OM.payment_status='Y' ";
        if ($id != '' && $id != 0) {
            $sql .=" AND OM.restaurant_id=$id";
        }
        if ($key != '') {
            $sql .= "  AND (MM.first_name LIKE '%$key%' OR 
				    MM.last_name LIKE '%$key%' OR 
				    RM.name LIKE '%$key%' OR
					RL.restaurant_name LIKE '%$key%' OR
					OM.order_status LIKE '%$key%' OR
					OM.order_type LIKE '%$key%') ";
        }


        //echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getOwnerStripDetails($data) {
        return array('stripe_public_key' => $this->config->item('stripe_public_key'), 'stripe_private_key' => $this->config->item('stripe_private_key'));
    }

    public function getOrderStripMap($order_id, $payment_mode = '') {
        $sql = "SELECT order_master.*,
					order_stripid_map.last_4digit,
					order_stripid_map.brand,
					order_stripid_map.strip_order_id,
					order_stripid_map.strip_customer_id,
					order_stripid_map.order_id,
					order_stripid_map.payment_mode
					FROM order_master 
			LEFT JOIN order_stripid_map
			ON order_stripid_map.order_id = order_master.order_id
			 WHERE  order_master.order_id='$order_id' ";

        if ($payment_mode)
            $sql.="and order_stripid_map.payment_mode='$payment_mode'";

        $sql.=" ORDER BY order_stripid_map.created_time DESC";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}

?>