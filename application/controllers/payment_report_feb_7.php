<?php

session_start();

error_reporting(1);

class Payment_report extends MY_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('Member_model', 'member_model');
        $this->load->model('member_model');
        $this->load->model('client_model');
        $this->load->model('payment_model');
        $this->load->library('session');
        $this->load->helper('url');
        date_default_timezone_set('GMT');
    }

    function index() {
//        redirect("../index.php");
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->view("payment_report");
//        $data['header'] = $this->load->view('admin/header', '', true);
//        $data['footer'] = $this->load->view('admin/footer', '', true);
    }

    function payment_details() {
        $user = $this->session->userdata('user');
        $res_id = $user->restaurant_id;

        $data['existing'] = $this->payment_model->get_existing_customers($res_id);
        $data['admin_val'] = $this->member_model->admin_val();
        $data['new'] = $this->payment_model->get_new_customers($res_id);

        $data12['restaurant_id'] = $res_id;
        $data12['active'] = 'payment_details';
        $this->load->view('owner/header', $data12);
        $this->load->view("payment_report", $data);
        $this->load->view('owner/footer');
    }

    function order_report() {
        $this->load->model('order_model');
        $user = $this->session->userdata('user');
        $res_id = $user->restaurant_id;

        $data['order_report'] = $this->payment_model->order_report($res_id);
        $data1['admin_val'] = $this->member_model->admin_val();
        $processing_fee = number_format(((($data['order_report']['total_amount'] * 2.9) / 100) ) + .3 * $data['order_report']['count_order'], 2, '.', '');
        $data['order_report']['processing_fee'] = $processing_fee;
        $data['order_report']['total_amount_proc'] = $data['order_report']['total_amount'] - $processing_fee;
        $data12['restaurant_id'] = $res_id;
        $data12['last_order_number'] = $this->order_model->getLastOrderNumber($user->restaurant_id);
        $data12['active'] = 'payment_report';
        $this->load->view('owner/header', $data12);
        $this->load->view("order_report", $data);
        $this->load->view('owner/footer');
    }

    function merchants_details() {


        /*     $_REQUEST['month'] = (!$_POST['month'] ? ($_GET['month'] ? $_GET['month'] : '') : $_POST['month']);
          $_REQUEST['year'] = (!$_POST['year'] ? ($_GET['year'] ? $_GET['year'] : '') : $_POST['year']);
          $_REQUEST['pay_method'] = (!$_POST['pay_method'] ? ($_GET['pay_method'] ? $_GET['pay_method'] : '') : $_POST['pay_method']);
          $_REQUEST['status'] = (!$_POST['status'] ? ($_GET['status'] ? $_GET['status'] : '') : $_POST['status']);


          if ($_REQUEST['year'] == '' || $_REQUEST['month'] == '') {

          $date = date('Y-m') . '-1';
          $data['merchants_details1']['year'] = date('Y');
          $data['merchants_details1']['month'] = date('m');
          } else {
          $date = rtrim($_REQUEST['year']) . '-0' . $_REQUEST['month'] . '-01';
          $data['merchants_details1']['year'] = $_REQUEST['year'];
          $data['merchants_details1']['month'] = $_REQUEST['month'];
          }

          $data['admin_val'] = $this->member_model->admin_val();
          $data['merchants_details'] = $this->payment_model->merchants_details($date, $_REQUEST['pay_method'], $_REQUEST['status']);
         */

        //$this->load->view("merchants_reports",$data);	

        $ouput['output'] = $this->load->view('merchants_reports', $data, true);
        $this->_render_output($ouput);
    }

    function merchants_order($res_id) {


        // $data['merchants_odr']= $this->payment_model->merchants_order();


        $ouput['output'] = $this->load->view('merchants_order', $data, true);
        $this->_render_output($ouput);
    }

    function crone_order_payment() {
        $this->load->library('stripe_lib');
        $data = $this->payment_model->merchant_payment();

        $stripe_res = $this->client_model->getAdmin_strip();
        foreach ($data as $result) {


          $amount = number_format((float) $amount, 2, '.', '');
            // $processing_fee = ((($result['restaurant_amount'] * 2.9) / 100) ) - .3 ;
            //$amount = $result['restaurant_amount'] - .25;
            $amount = ($result['restaurant_amount'] - $result['proce'] );
            // $processing_fee = .25 + $result['proce'];
            $processing_fee =  $result['proce'];
            $amount = number_format((float) $amount, 2, '.', '');
            $restaurent_details = $this->payment_model->restaurent_details_recipient($result['restaurant_id']);

            if ($restaurent_details['account_type'] == 'ACH') {


                // Set your secret key: remember to change this to your live secret key in production
                // See your keys here https://dashboard.stripe.com/account/apikeys
                $stripe = $this->client_model->getAdmin_strip();
                try {
                    \Stripe\Stripe::setApiKey($stripe['stripe_private_key']);
                    $response = \Stripe\Balance::retrieve();
                    foreach ($response->available as $pendingBalance) {
                        $avaialbleBal = $pendingBalance->amount;
                    }
                } catch (Exception $e) {
                    $body = $e->getJsonBody();
                    echo json_encode(array('error' => $body['error']['message']));
                }
                if ($avaialbleBal >= $amount) {
                    try {
                        var_dump($stripe['stripe_private_key']);
                        \Stripe\Stripe::setApiKey($stripe['stripe_private_key']);
                    } catch (Exception $E) {

                        var_dump($E);
                    }
                    try {
                        echo "sdsad";
                        $transfer = \Stripe\Transfer::create(array(
                                    "amount" => str_replace(".", "", $amount), // amount in cents
                                    "currency" => "usd",
                                    "recipient" => $restaurent_details['recipient'],
                                    "description" =>" Order payment by fulspoon ",
                                        )
                        );
                        if (!$transfer) {
                            throw new Exception('Problem with tableBresults');
                        }
                    } catch (Exception $err) {
                        var_dump($err);
                    }


                    $order_data = array('restaurant_id' => $result['restaurant_id'],
                        'type' => 'order',
                        'amount' => $result['restaurant_amount'],
                        "order_count" => $result['count_order'],
                        "processing_fee" => $processing_fee,
                        "payment_type" => "ACH",
                        'recipient_response_id' => $transfer->id
                    );

                    $this->payment_model->insert_payment_details($order_data);

                    $restaurent_details = $this->payment_model->order_master_update_payment_status($result['restaurant_id']);
                    $recipient_response_id = '';
                    if ($transfer->id != '') {
                        $New_Subscriber_for_owner = $this->member_model->get_email("order_payment_success");
                        $message2 = $New_Subscriber_for_owner['email_template'];
                        $subject2 = $New_Subscriber_for_owner['email_subject'];
                        //$message2 = str_replace('#baseurl#', base_url(), $message2);
                        $message2 = str_replace('#start_date#', $result['mon'], $message2);
                        $message2 = str_replace('#end_date#', $result['sat'], $message2);
                        $message2 = str_replace('#amount#', $amount, $message2);
                        $message2 = str_replace('#Bank_Name#', $result['restaurant_name'], $message2);
                        $message2 = str_replace('#last_4_digits_of_account_number#', substr($restaurent_details['AccountNumber'], -4), $message2);
                        //$message2 = str_replace('#SubscriberName#', $array['payment_data']['name_on_card'], $message2);
                        $this->load->library('email');
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'utf-8';
                        $config['crlf'] = PHP_EOL;
                        $config['newline'] = PHP_EOL;
                        $this->email->initialize($config);
                        $pjt_name = $stripe_res['project_name'];
                        $category = "Payment Report";
                        $to_mail = $restaurent_details['email'];
                        $from = $stripe_res['email'];

                        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                    } else {
                        //fail
                        $New_Subscriber_for_owner = $this->member_model->get_email("order_payment_failed");
                        $message2 = $New_Subscriber_for_owner['email_template'];
                        $subject2 = $New_Subscriber_for_owner['email_subject'];


                        $message2 = str_replace('#start_date#', $result['mon'], $message2);
                        $message2 = str_replace('#end_date#', $result['sat'], $message2);
                        $message2 = str_replace('#amount#', $amount, $message2);
                        $message2 = str_replace('#Bank_Name#', $result['restaurant_name'], $message2);
                        $message2 = str_replace('#last_4_digits_of_account_number#', substr($restaurent_details['AccountNumber'], -4), $message2);
                        $message2 = str_replace('#client_address#', $restaurent_details['formatted_address'], $message2);

                        //$message2 = str_replace('#SubscriberName#', $array['payment_data']['name_on_card'], $message2);
                        $pjt_name = $stripe_res['project_name'];
                        $category = "Payment Report";
                        $to_mail = $restaurent_details['email'];
                        $from = $stripe_res['email'];

                        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                    }
                } else {
                    $New_Subscriber_for_owner = $this->member_model->get_email("order_payment_failed_amount");
                    $message2 = $New_Subscriber_for_owner['email_template'];
                    $subject2 = $New_Subscriber_for_owner['email_subject'];
                    $message2 = str_replace('#start_date#', $result['mon'], $message2);
                    $message2 = str_replace('#end_date#', $result['sat'], $message2);
                    $message2 = str_replace('#amount#', $amount, $message2);
                    $message2 = str_replace('#Bank_Name#', $result['restaurant_name'], $message2);
                    $message2 = str_replace('#last_4_digits_of_account_number#', substr($restaurent_details['AccountNumber'], -4), $message2);
                    $message2 = str_replace('#client_address#', $restaurent_details['formatted_address'], $message2);
                    $pjt_name = $stripe_res['project_name'];
                    $category = "Payment Report";
                    $to_mail = $restaurent_details['email'];
                    $from = $stripe_res['email'];

                    $this->member_model->mail_sendgrid($from, $subject2, $message2, $category, $from, $pjt_name);
                }
            } else {
                $order_data1 = array('restaurant_id' => $result['restaurant_id'],
                    'type' => 'order',
                    'amount' => $result['restaurant_amount'],
                    "order_count" => $result['count_order'],
                    "processing_fee" => $processing_fee,
                    "payment_type" => "check"
                );
                $this->payment_model->insert_payment_details($order_data1);

                $restaurent_details = $this->payment_model->order_master_update_payment_status($result['restaurant_id']);
            }
        }

        $new_array = array("0");
        foreach ($data as $value) {

            array_push($new_array, $value['restaurant_id']);
        }

        $sql123 = "SELECT restaurant_id FROM restaurant_master WHERE `status` = 'Y'";
        $query = $this->db->query($sql123);
        $restaurant_id_new = $query->result_array();
        foreach ($restaurant_id_new as $value) {
            if (in_array($value['restaurant_id'], $new_array)) {
                
            } else {

                $order_data2 = array('restaurant_id' => $value['restaurant_id'],
                    'type' => 'order',
                    "payment_type" => "check"
                );
                $this->payment_model->insert_payment_details($order_data2);
            }
        }
    }

    function check_bal() {
        $this->load->library('stripe_lib');

        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey("sk_live_LOKtTD7UUR7BgHc5bxA9fpbA");

        try {
            $response = \Stripe\Balance::retrieve();
            foreach ($response->pending as $pendingBalance) {
                echo "Pending balance of " . $pendingBalance->amount . " in " . $pendingBalance->currency . "<br>";
            }

            foreach ($response->available as $pendingBalance) {
//                  echo'hiii<pre>';
//                  print_r($pendingBalance);
//                  exit;
                echo "Available balance of " . $pendingBalance->amount . " in " . $pendingBalance->currency . "<br>";
            }
            echo '<pre>';
            print_r($response);
        } catch (Exception $e) {
            $body = $e->getJsonBody();
            echo json_encode(array('error' => $body['error']['message']));
            ////exit;
        }
    }

    function crone_subscription_payment() {

        $this->load->library('stripe_lib');
        $date = date('Y-m') . "-01";
        $pay_method = 'ACH';
        $stripe_res = $this->client_model->getAdmin_strip();
        $data = $this->payment_model->merchants_details_crone($date, $pay_method);

        $data1 = $this->member_model->admin_val();
//        echo $this->db->last_query();
//        echo '<pre>';
//        print_r($data);
        $stripe = $this->client_model->getAdmin_strip();

//                    echo $stripe['stripe_private_key'];


        foreach ($data as $result) {
            $amount = ($result['count_restaurant'] * $data1['contract_price']);
            $amount = number_format((float) $amount, 2, '.', '');
            $amount = str_replace(".", "", $amount);
            if ($result['account_type'] == 'ACH') {

                $restaurent_details = $this->payment_model->restaurent_details_recipient($result['restaurant_id']);
                if ($restaurent_details['recipient'] != '') {
                    \Stripe\Stripe::setApiKey($stripe['stripe_private_key']);
                    try {
                        $transfer = \Stripe\Transfer::create(array(
                                    "amount" => $amount,
                                    "currency" => "usd",
                                    "recipient" => $restaurent_details['recipient'],
                                    "description" => "Subscription payment done by fulspoon",
                                        )
                        );
                    } catch (Exception $E) {
                        $transfer->id = '';
                        echo 'Exception';
                    }
                    $amount = $amount / 100;
                    $order_data = array('restaurant_id' => $result['restaurant_id'],
                        'type' => 'subscription',
                        'amount' => $amount,
                        'payment_type' => 'ACH',
                        'new_subscribers' => $result['count_restaurant'],
                        'existing_subscribers' => '0',
                        'recipient_response_id' => $transfer->id
                    );
                    $this->payment_model->insert_payment_details($order_data);
                    if ($transfer->id != '') {
                        $New_Subscriber_for_owner = $this->member_model->get_email("subscription_payment_success");
                        $message2 = $New_Subscriber_for_owner['email_template'];
                        $subject2 = $New_Subscriber_for_owner['email_subject'];
                        $message2 = str_replace('#start_date#', date("m-d-Y", strtotime($result['first'])), $message2);
                        $message2 = str_replace('#end_date#', date("m-d-Y", strtotime($result['last'])), $message2);
                        $message2 = str_replace('#YEAR#', date('Y'), $message2);
                        $message2 = str_replace('#amount#', $amount, $message2);
                        $message2 = str_replace('#Bank_Name#', $result['restaurant_name'], $message2);
                        $message2 = str_replace('#OwnerName#', $result['restaurant_name'], $message2);
                        $message2 = str_replace('#last_4_digits_of_account_number#', substr($restaurent_details['AccountNumber'], -4), $message2);
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'utf-8';
                        $config['crlf'] = PHP_EOL;
                        $config['newline'] = PHP_EOL;
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $pjt_name = $stripe_res['project_name'];
                        $category = "Payment Report success";
                        $to_mail = $restaurent_details['email'];
                        $from = $stripe_res['email'];
                        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                    } else {
                        $pjt_name = 'Fulspoon LLC';
                        $New_Subscriber_for_owner = $this->member_model->get_email("subscription_payment_failed");
                        $message2 = $New_Subscriber_for_owner['email_template'];
                        $subject2 = $New_Subscriber_for_owner['email_subject'];
                        $message2 = str_replace('#OwnerName#', $result['restaurant_name'], $message2);
                        $message2 = str_replace('#start_date#', date("m-d-Y", strtotime($result['first'])), $message2);
                        $message2 = str_replace('#end_date#', date("m-d-Y", strtotime($result['last'])), $message2);
                        $message2 = str_replace('#amount#', $amount, $message2);
                        $message2 = str_replace('#YEAR#', date('Y'), $message2);
                        $message2 = str_replace('#Bank_Name#', $result['restaurant_name'], $message2);
                        $message2 = str_replace('#last_4_digits_of_account_number#', substr($restaurent_details['AccountNumber'], -4), $message2);
                        $message2 = str_replace('#client_address#', $restaurent_details['formatted_address'], $message2);
                        $category = "Payment Report fail";
                        $to_mail = $restaurent_details['email'];
                        $from = $stripe_res['email'];
                        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                    }
                } else {
                    $pjt_name = 'Fulspoon LLC';
                    $New_Subscriber_for_owner = $this->member_model->get_email("subscription_payment_failed");
                    $message2 = $New_Subscriber_for_owner['email_template'];
                    $subject2 = $New_Subscriber_for_owner['email_subject'];
                    $message2 = str_replace('#start_date#', $result['first'], $message2);
                    $message2 = str_replace('#end_date#', $result['last'], $message2);
                    $message2 = str_replace('#amount#', $amount, $message2);
                    $message2 = str_replace('#OwnerName#', $result['restaurant_name'], $message2);
                    $message2 = str_replace('#YEAR#', date('Y'), $message2);
                    $message2 = str_replace('#Bank_Name#', $result['restaurant_name'], $message2);
                    $message2 = str_replace('#last_4_digits_of_account_number#', substr($restaurent_details['AccountNumber'], -4), $message2);
                    $message2 = str_replace('#client_address#', $restaurent_details['formatted_address'], $message2);
                    $category = "Payment Report fail";
                    $to_mail = $restaurent_details['email'];
                    $from = $stripe_res['email'];
//            $pjt_name = $data_1['project_name'];
                    $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                }
            } else {
                $order_data = array('restaurant_id' => $result['restaurant_id'],
                    'type' => 'subscription',
                    'amount' => ($amount / 100),
                    'payment_type' => 'check',
                    'new_subscribers' => $result['count_restaurant'],
                    'existing_subscribers' => '0'
                );
                $this->payment_model->insert_payment_details($order_data);
                $restaurent_details = $this->payment_model->order_master_update_payment_status($result['restaurant_id']);
            }
        }
        /* 6/24 */
        $dataF = $this->payment_model->getFirstDayCount();

        echo $this->db->last_query();
        echo 'before <pre>';
        print_r($dataF);
        foreach ($dataF as $Fdata) {
            $restaurent_details = $this->payment_model->restaurent_details_recipient($Fdata['restaurant_id']);
            echo'loop';
            echo '<pre>';
            print_r($restaurent_details);
            $dataL = $this->payment_model->getLastDayCount($Fdata['restaurant_id']);
            $rest_count = $this->payment_model->getCurrentMonth_Count($Fdata['restaurant_id']);
            $amount = ($rest_count['count_restaurant'] * $data1['contract_price']);
            $rest_amount = $this->payment_model->getCurrentMonth_Amount($Fdata['restaurant_id']);
            $processing_fee = number_format((float) (((($rest_amount['restaurant_amount'] * 2.9) / 100) ) + .3 * $rest_amount['count_order']), 2, '.', '');
            $Monthly_email_report_to_owner = $this->member_model->get_email("Monthly_email_report_to_owner");
            $message2 = $Monthly_email_report_to_owner['email_template'];
            $subject2 = $Monthly_email_report_to_owner['email_subject'];
            $datestring = date("Y-m-d") . ' first day of last month';
            $dt = date_create($datestring);
            $year = $dt->format('Y');
            $month = $dt->format('M');
            $NewAdd = $dataL['count'] - $Fdata['count'];
            $subject2 = str_replace('#StartDate#', $month, $subject2);
            $subject2 = str_replace('#EndDate#', $year, $subject2);
            $message2 = str_replace('#OwnerName#', $restaurent_details['restaurant_name'], $message2);
            $message2 = str_replace('#Num_subscribers_day_ month#', $Fdata['count'], $message2);
            $message2 = str_replace('#Number_subscribers_last_day_of_month#', $dataL['count'], $message2);
            if ($NewAdd > 0) {
                $message2 = str_replace('#Num_subscribers_who_added_this_restaurant#', $NewAdd, $message2);
            } else {
                $message2 = str_replace('#Num_subscribers_who_added_this_restaurant#', '0', $message2);
            }
            $message2 = str_replace('#total_subscription_fees_paid_to_restaurant_for_the_month#', $amount, $message2);
            $message2 = str_replace('#Sum_of_invoice#', $processing_fee, $message2);
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;
            $this->load->library('email');
            $this->email->initialize($config);
            $pjt_name = $stripe_res['project_name'];


            $category = "Payment Report";
            $to_mail = $restaurent_details['email'];
            $from = $stripe_res['email'];
//            $pjt_name = $data_1['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
        }

        /* 6/24 */
    }

    function test3() {
        $datestring = date("Y-m-d") . ' first day of last month';
        $dt = date_create($datestring);
        $year = $dt->format('Y');
        $month = $dt->format('M');
    }

    function ajaxblock() {



        if ($_POST['month'] == 0) {
            $month = 'Jan';
        } else if ($_POST['month'] == 1) {
            $month = 'Feb';
        } else if ($_POST['month'] == 2) {
            $month = 'Mar';
        } else if ($_POST['month'] == 3) {
            $month = 'Apr';
        } else if ($_POST['month'] == 4) {
            $month = 'May';
        } else if ($_POST['month'] == 5) {
            $month = 'Jun';
        } else if ($_POST['month'] == 6) {
            $month = 'Jul';
        } else if ($_POST['month'] == 7) {
            $month = 'Aug';
        } else if ($_POST['month'] == 8) {
            $month = 'Sep';
        } else if ($_POST['month'] == 9) {
            $month = 'Oct';
        } else if ($_POST['month'] == 10) {
            $month = 'Nov';
        } else if ($_POST['month'] == 11) {
            $month = 'Dec';
        }

        if ($_POST['month1'] == 0) {
            $month1 = 'Jan';
        } else if ($_POST['month1'] == 1) {
            $month1 = 'Feb';
        } else if ($_POST['month1'] == 2) {
            $month1 = 'Mar';
        } else if ($_POST['month1'] == 3) {
            $month1 = 'Apr';
        } else if ($_POST['month1'] == 4) {
            $month1 = 'May';
        } else if ($_POST['month1'] == 5) {
            $month1 = 'Jun';
        } else if ($_POST['month1'] == 6) {
            $month1 = 'Jul';
        } else if ($_POST['month1'] == 7) {
            $month1 = 'Aug';
        } else if ($_POST['month1'] == 8) {
            $month1 = 'Sep';
        } else if ($_POST['month1'] == 9) {
            $month1 = 'Oct';
        } else if ($_POST['day1'] == 10) {
            $month1 = 'Nov';
        } else if ($_POST['month'] == 11) {
            $month1 = 'Dec';
        }


        if ($_POST['day'] == 1) {
            $day = 'first';
        } else if ($_POST['day'] == 2) {
            $day = 'second';
        } else if ($_POST['day'] == 3) {
            $day = 'third';
        } else if ($_POST['day'] == 4) {
            $day = 'fourth';
        } else if ($_POST['day'] == 5) {
            $day = 'fifth';
        }



        if ($_POST['day1'] == 1) {
            $day1 = 'first';
        } else if ($_POST['day1'] == 2) {
            $day1 = 'second';
        } else if ($_POST['day1'] == 3) {
            $day1 = 'third';
        } else if ($_POST['day1'] == 4) {
            $day1 = 'fourth';
        } else if ($_POST['day1'] == 5) {
            $day1 = 'fifth';
        }





        $third_monday = new DateTime($day . ' monday of' . $month . ' ' . $_POST['year']);

        // if date has passed, get next month's third monday
        if ($third_monday < new DateTime()) {
            $third_monday->modify($day . ' monday of ' . $month . ' ' . $_POST['year']);
        }

        $or_date = $third_monday->format('Y-m-d');



        $third_monday = new DateTime($day1 . ' monday of' . $month1 . ' ' . $_POST['year1']);

        // if date has passed, get next month's third monday
        if ($third_monday < new DateTime()) {
            $third_monday->modify($day1 . ' monday of ' . $month1 . ' ' . $_POST['year1']);
        }

        $or_date12 = $third_monday->format('Y-m-d');

        $user = $this->session->userdata('user');
        $res_id = $user->restaurant_id;

        $sql = "select * from merchant_payment where type='order' AND create_date>='$or_date' AND create_date<=DATE_ADD('$or_date12', INTERVAL 7 DAY)
		 and restaurant_id='$res_id'";
        //echo $sql;

        $query = $this->db->query($sql);
        $data = $query->result_array();

        $tot = 0;
        foreach ($data as $result) {
            $originalDate = $result['create_date'];
            $newDate = date("m-d-Y", strtotime($originalDate));
            $status = '';
            if ($result['payment_type'] == 'ACH') {
                if ($result['recipient_response_id'] != '') {
                    $status = 'Success';
                } else {
                    $status = 'Fail';
                }
            }
            echo '<tr><td style="text-align:center">' . $newDate . '</td><td style="text-align:center">' . $result['order_count'] . '</td><td style="text-align:center">$' . $result['amount'] . '</td><td style="text-align:center">$' . $result['processing_fee'] . '</td><td style="text-align:center">' . $status . '</td><td style="text-align:center">$' . ($result['amount'] - $result['processing_fee']) . '</td></tr>';
            $tot = ($result['amount'] - $result['processing_fee']) + $tot;
        }

        echo '<td  class="numeric" colspan="4">Total Paid</td>
        <td colspan="2" style="text-align:center">$' . $tot . '</td>';
    }

    function ajaxsub() {
        ob_start();
        session_start();
        $st_date = date("Y-m", strtotime($_POST['start_date']));
        $st_date = $st_date . '-01';
        $en_date = date("Y-m", strtotime($_POST['end_date']));
        $en_date = $en_date . '-01';
        $user = $this->session->userdata('user');
        if ($user->restaurant_id == '') {
            $user->restaurant_id = $_SESSION['res_id'];
        }
        $res_id = $user->restaurant_id;
        $sql = "select * from merchant_payment where type='subscription' AND create_date>='$st_date' AND create_date<=LAST_DAY('$en_date')  and restaurant_id='$res_id'";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $tot = 0;
        foreach ($data as $result) {
            $originalDate = $result['create_date'];
            $newDate = date("m-d-Y", strtotime($originalDate));
            echo '<tr><td class="callabel" data-title="Payment Date" >' . $newDate . '</td><td  data-title="New Subscribers of the Month" class="numeric">' . $result['new_subscribers'] . '</td><td  data-title="Number of Renewing Subscribers" class="numeric">' . $result['existing_subscribers'] . '</td><td data-title="Number of Non-renewing Subscribers" class="numeric">' . $result['subscripers_left'] . '</td><td data-title="Total Subscribers" class="numeric">' . ($result['new_subscribers'] + $result['existing_subscribers']) . '</td><td data-title="Amount Paid" class="numeric">$' . ($result['amount']) . '</td></tr>';
            $tot = ($result['amount']) + $tot;
        }

        echo '<td  class="numeric" colspan="5">Total Paid</td>
        <td colspan="2" style="text-align:center">$' . $tot . '</td>';
    }

    function saveCredit($arr) {

        $this->load->library('stripe_lib');

        //$arr['member_id'] =79;//167 79
        //$arr['account_type'] = "card";
        $arr['user_type'] = "provider"; //provider;normal
        $arr['account_type'] = "bank";
        //for card
        //$arr['card_number'] = "4242424242424242";//4111111111111111
        //$arr['expiry_month'] = "1";
        //$arr['expiry_year'] = "2018";
        //$arr['cvv'] = "475";
        //$arr['email']="aji@newagesmb.com";
        //$arr['name_on_card']="aji";
        //$arr['name']="aji";
        //    //for bank
        $arr['country'] = "US";
        $arr['currency'] = "usd";
        $arr['routing_number'] = "110000000";
        $arr['account_holder_type'] = "individual";
        $arr['ac_number'] = "000123456789";
        $arr['email'] = "bibinv@newagesmb.com";
        $arr['name'] = "bibin";
        $arr['ssn'] = "123";
        //$arr['ssn'] = "111111111";


        $stripe['stripe_private_key'] = "sk_test_9dGC7diQpthDKdwIsuSMISgr";



        $this->stripe_lib->setApiKey($stripe['stripe_private_key']);

        ///////////////////////////////////////$Details = $this->client_model->user_details($arr['member_id']);
        //if($Details['stripe_customer_id']!='')
        //{
        //	$result 	= array('status' => 'true', 'message' =>"Sucess");
        //}
        //else{


        if ($arr['account_type'] == "card") {
            if ($arr['user_type'] == "normal") {
                $Token = $this->stripe_lib->createCardToken($arr['account_type'], $arr['card_number'], $arr['expiry_month'], $arr['expiry_year'], $arr['cvv']);
                $Customer = $this->stripe_lib->createCustomer(array("description" => "Customer for " . $arr['email'],
                    "email" => $arr['email'], "source" => $Token->id, "metadata" => array("user_id" => $arr['member_id'],
                        "email" => $arr['email'], "name" => $arr['name_on_card'])));
                $arr['cus_id'] = $Customer->id;
                $arr['last_4_digits'] = $Token->card->last4;
                $this->client_model->saveCredit($arr);
                $result = array('status' => 'true', 'message' => "Sucess");
            } else if ($arr['user_type'] == "provider") {
                $Token = $this->stripe_lib->createCardToken($arr['account_type'], $arr['card_number'], $arr['expiry_month'], $arr['expiry_year'], $arr['cvv']);
                //print_r($Token['id']);exit;	
                $tkn = $Token['id'];

                if (count((explode(" ", trim($arr['name'])))) == 1) {
                    $arr['name'] = $arr['name'] . " " . $arr['name'];
                }
                //print_r($arr['name']);exit;	
                $recipient = \Stripe\Recipient::create(array(
                            "name" => $arr['name'],
                            "type" => "individual"
                ));
                $arr['cus_id'] = $recipient->id;
                $arr['last_4_digits'] = $Token->card->last4;
                $this->client_model->saveCredit($arr);
                $result = array('status' => 'true', 'message' => "Sucess");
            }
        } else if ($arr['account_type'] == "bank") {


            if ($arr['user_type'] == "provider") {

                //$arr['account_type'] = "bank_account";
                //print_r($arr['routing_number']);	print_r($arr['ac_number']);	
                $Token = $this->stripe_lib->createBankAccount("bank_account", "US", "usd", $arr['routing_number'], $arr['ac_number']);

                $recipient = $this->stripe_lib->createRecipient($Token['id']);
                $arr['cus_id'] = $recipient->id;
                if ($recipient->active_account['last4'] != '') {

                    $arr['last_4_digits'] = $recipient->active_account['last4'];
                } else {

                    $arr['last_4_digits'] = "";
                }
                /////////////////////////////////$this->client_model->saveCredit($arr);
                /////////////////////////////////$result 	= array('status' => 'true', 'message' =>"Sucess");
            }
            echo $arr['cus_id'];
            echo "success";
        }

        //}

        echo json_encode($result);
    }

    function ajaxadmS() {

        $user = $this->session->userdata('user');
        $res_email = $user->email;


        if ($_POST['is_check'] != '') {

            $res_id = $_POST['resci_id'];
            $res_dat = $_POST['is_check'];

            $sql_1 = "UPDATE merchant_payment SET recipient_response_id='$res_dat' WHERE recipient_id='$res_id'";

            $query = $this->db->query($sql_1);

            $sql_q = "select *,DATE_FORMAT(LAST_DAY(T1.create_date - INTERVAL 1 MONTH), '%Y-%m-1') as first,SUBDATE(T1.create_date,INTERVAL 1 DAY) as last from merchant_payment T1 left join restaurant_master T2 on T1.restaurant_id=T2.restaurant_id where 
			T1.recipient_id='$res_id'";
            $query = $this->db->query($sql_q);
            $reat_details = $query->row_array();

            //check no: send to hotel owners:-subscr
            $stripe_res = $this->client_model->getAdmin_strip();
            $New_Subscriber_for_owner = $this->member_model->get_email("cheque_mailed_subscription");
            $message2 = $New_Subscriber_for_owner['email_template'];
            $subject2 = $New_Subscriber_for_owner['email_subject'];


            $originalDate = $reat_details['last'];
            $newDate = date("Y-m-d", strtotime($originalDate));

            $message2 = str_replace('#start_date#', $reat_details['start'], $message2);
            $message2 = str_replace('#end_date#', $newDate, $message2);
            $message2 = str_replace('#amount#', $reat_details['amount'], $message2);
            $message2 = str_replace('#Bank_Name#', $reat_details['restaurant_name'], $message2);
            $message2 = str_replace('#last_4_digits_of_account_number#', substr($reat_details['AccountNumber'], -4), $message2);
            $category = "Payment Report";
            $to_mail = $reat_details['email'];
            $from = $stripe_res['email'];
            $pjt_name = $stripe['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
        }

        $month = $_POST['month'];
        $year = $_POST['year'];

        if ($month == '' || $year == '') {

            $date = date('Y-m') . '-1';
            $data['merchants_details1']['year'] = date('Y');
            $data['merchants_details1']['month'] = date('m');
        } else {
            if ($month > 9) {
                $date = rtrim($year) . '-' . $month . '-01';
            } else {
                $date = rtrim($year) . '-0' . $month . '-01';
            }

            $data['merchants_details1']['year'] = $year;
            $data['merchants_details1']['month'] = $month;
        }

        $data1 = $this->member_model->admin_val();
        $result = $this->payment_model->merchants_details_ajx($date, $_POST['p_type'], $_POST['s_status']);



        $tot = 0;
        foreach ($result as $data) {

            $stat = '';

            if ($data['account_type'] == 'ACH') {
                if ($data['recipient_response_id'] == '') {
                    $stat = 'Failed';
                    $qweer = $data['recipient_id'] . '_id';
                    $trans_id = "<input type='text' class='form-control' id=" . $qweer . " value=" . $data['recipient_response_id'] . "><input id='check_box" . $data['recipient_id'] . "' type='checkbox' class='check_test123456'    value='" . $data['recipient_id'] . "'>";
                } else {
                    $trans_id = $data['recipient_response_id'];
                    $stat = 'Paid';
                }
            } else {

                if ($data['recipient_response_id'] != '') {

                    $stat = 'Mailed';
                }
            }


            if ($data['account_type'] == 'check') {

                $qweer = $data['recipient_id'] . '_id';

                $trans_id = "<input type='text' class='form-control' id=" . $qweer . " value=" . $data['recipient_response_id'] . "><input id='check_box" . $data['recipient_id'] . "' type='checkbox' class='check_test123456'    value='" . $data['recipient_id'] . "'>";
            }






            echo '<tr><td style="text-align:center">' . $data['restaurant_name'] . '</td><td style="text-align:center">' . date("m-d-Y", strtotime($data['create_date'])) . '</td><td style="text-align:center">' . $data['account_type'] . '</td><td style="text-align:center">' . $trans_id . '</td><td style="text-align:center">' . $stat . '</td><td style="text-align:center">$' . number_format((float) ($data['amount'] - $data['processing_fee']), 2, '.', '') . '</td></tr>';
            $tot = ($data['amount'] - $data['processing_fee']) + $tot;
        }

        echo '<td  class="numeric" colspan="4">Total Paid</td>
        <td colspan="2" style="text-align:center">$' . number_format((float) $tot, 2, '.', '') . '</td> <script></script>';
    }

    function ajaxadm() {

        $user = $this->session->userdata('user');
        $res_email = $user->email;

        if ($_POST['is_check'] != '') {


            $res_id = $_POST['resci_id'];
            $res_dat = $_POST['is_check'];

            $sql_1 = "UPDATE merchant_payment SET recipient_response_id='$res_dat' WHERE recipient_id='$res_id'";

            $query = $this->db->query($sql_1);

            //check no: send to hotel owners:-order
            $sql_q = "select *,DATE_FORMAT(LAST_DAY(T1.create_date - INTERVAL 1 MONTH), '%Y-%m-1') as first,SUBDATE(T1.create_date,INTERVAL 1 DAY) as last from merchant_payment T1 left join restaurant_master T2 on T1.restaurant_id=T2.restaurant_id where 
			T1.recipient_id='$res_id'";
            $query = $this->db->query($sql_q);
            $reat_details = $query->row_array();

            //check no: send to hotel owners:-subscr
            $stripe_res = $this->client_model->getAdmin_strip();
            $New_Subscriber_for_owner = $this->member_model->get_email("cheque_mailed_subscription");
            $message2 = $New_Subscriber_for_owner['email_template'];
            $subject2 = $New_Subscriber_for_owner['email_subject'];


            $originalDate = $reat_details['last'];
            $newDate = date("Y-m-d", strtotime($originalDate));

            $message2 = str_replace('#start_date#', $reat_details['start'], $message2);
            $message2 = str_replace('#end_date#', $newDate, $message2);
            $message2 = str_replace('#amount#', $reat_details['amount'], $message2);
            $message2 = str_replace('#Bank_Name#', $reat_details['restaurant_name'], $message2);
            $message2 = str_replace('#last_4_digits_of_account_number#', substr($reat_details['AccountNumber'], -4), $message2);

            $category = "Payment Report";
            $to_mail = $reat_details['email'];
            $from = $stripe_res['email'];
            $pjt_name = $stripe['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
        }

        if ($_POST['month'] == 0) {
            $month = 'Jan';
        } else if ($_POST['month'] == 1) {
            $month = 'Feb';
        } else if ($_POST['month'] == 2) {
            $month = 'Mar';
        } else if ($_POST['month'] == 3) {
            $month = 'Apr';
        } else if ($_POST['month'] == 4) {
            $month = 'May';
        } else if ($_POST['month'] == 5) {
            $month = 'Jun';
        } else if ($_POST['month'] == 6) {
            $month = 'Jul';
        } else if ($_POST['month'] == 7) {
            $month = 'Aug';
        } else if ($_POST['month'] == 8) {
            $month = 'Sep';
        } else if ($_POST['month'] == 9) {
            $month = 'Oct';
        } else if ($_POST['month'] == 10) {
            $month = 'Nov';
        } else if ($_POST['month'] == 11) {
            $month = 'Dec';
        }




        if ($_POST['day'] == 1) {
            $day = 'first';
        } else if ($_POST['day'] == 2) {
            $day = 'second';
        } else if ($_POST['day'] == 3) {
            $day = 'third';
        } else if ($_POST['day'] == 4) {
            $day = 'fourth';
        } else if ($_POST['day'] == 5) {
            $day = 'fifth';
        }









        $third_monday = new DateTime($day . ' monday of' . $month . ' ' . $_POST['year']);

        // if date has passed, get next month's third monday
        if ($third_monday < new DateTime()) {
            $third_monday->modify($day . ' monday of ' . $month . ' ' . $_POST['year']);
        }

        $or_date = $third_monday->format('Y-m-d');
        $or_date1 = $or_date . ' 00:00:00';
        $or_date2 = $or_date . ' 59:59:59';
        $p_type = $_POST['p_type'];
        $s_status = $_POST['s_status'];





        $sql = "select 
					 T1.recipient_response_id, T1.recipient_id,T1.payment_type as account_type,T1.create_date,T1.order_count,T1.processing_fee,T1.restaurant_id,T1.payment_type,T2.restaurant_name,T1.amount
					  from merchant_payment T1
					  LEFT JOIN  restaurant_master T2 ON T2.restaurant_id=T1.restaurant_id 
					  where T1.type='order' AND T1.create_date>='$or_date1' AND T1.create_date<='$or_date2'";
        if ($p_type != '') {
            $sql.="  and T1.payment_type='$p_type'";
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();

        $tot = 0;
        foreach ($result as $data) {

            $stat = '';

            if ($data['account_type'] == 'ACH') {
                if ($data['recipient_response_id'] == '') {
                    $stat = 'Failed';
                    $qweer = $data['recipient_id'] . '_id';
                    $trans_id = "<input type='text' class='form-control' id=" . $qweer . " value=" . $data['recipient_response_id'] . "><input id='check_box" . $data['recipient_id'] . "' type='checkbox'    value=" . $data['recipient_id'] . " class='check_test' >";
                } else {
                    $trans_id = $data['recipient_response_id'];
                    $stat = 'Paid';
                }
            } else {

                if ($data['recipient_response_id'] != '') {

                    $stat = 'Mailed';
                }
            }


            if ($data['account_type'] == 'ACH') {
                
            } else {
                $qweer = $data['recipient_id'] . '_id';

                $trans_id = "<input type='text' class='form-control' id=" . $qweer . " value=" . $data['recipient_response_id'] . "><input id='check_box" . $data['recipient_id'] . "' type='checkbox'    value=" . $data['recipient_id'] . " class='check_test' >";
            }






            echo '<tr><td style="text-align:center">' . $data['restaurant_name'] . '</td><td style="text-align:center">' . date("m-d-Y", strtotime($data['create_date'])) . '</td><td style="text-align:center">' . $data['account_type'] . '</td><td>' . $trans_id . '</td><td style="text-align:center">' . $stat . '</td><td style="text-align:center">$' . number_format((float) ($data['amount'] - $data['processing_fee']), 2, '.', '') . '</td></tr>';
            $tot = ($data['amount'] - $data['processing_fee']) + $tot;
        }

        echo '<td  class="numeric" colspan="4">Total Paid</td>
        <td colspan="2" style="text-align:center">$' . number_format((float) $tot, 2, '.', '') . '</td>';
    }

    ##########################

    function test() {

        echo "test1";
        $this->load->library('stripe_lib');
//        $this->stripe_lib->setApiKey("sk_test_9dGC7diQpthDKdwIsuSMISgr");
        \Stripe\Stripe::setApiKey("sk_live_LOKtTD7UUR7BgHc5bxA9fpbA");
//        \Stripe\Stripe::setApiKey("sk_test_kI1BmSYqEgX51xHSPG0lsnEb");
        // Create a transfer to the specified recipient
        try {
            $transfer = \Stripe\Transfer::create(array(
                        "amount" => '100', // amount in cents
                        "currency" => "usd",
                        "recipient" => 'rp_18vnLgJFzfx3xbbxo0yLbsRK',
                            )
            );
        } catch (Exception $e) {
            $body = $e->getJsonBody();
            echo json_encode(array('error' => $body['error']['message']));
            ////exit;
        }

        echo $transfer->id;

        echo "<pre>";
        echo "test";
        print_r($transfer);
    }

    function test_1() {

        $this->load->library('stripe_lib');


        $this->stripe_lib->setApiKey("sk_test_9dGC7diQpthDKdwIsuSMISgr");
        $Token = $this->stripe_lib->createBankAccount("bank_account", "US", "usd", '110000000', '000123456789');

        echo $Token['id'];
        /* $params = array(
          "name" => $company['first_name'] . " " . $company['last_name'],
          "type" => "individual",
          "email" => $company['email'],
          "tax_id" => $company['ssn'],
          "bank_account"=>$Token['id']
          );

          $recipient = $this->stripe_lib->createRecipient($params); */



        $params = array(
            "name" => "testfirst testlast",
            "type" => "individual",
            "email" => "test@test.com",
            "tax_id" => "111111111",
            "bank_account" => $Token['id']
        );

        echo '<pre>';
        print_r($params);

        $recipient = $this->stripe_lib->createRecipient($params);

        echo $array['recipient'] = $recipient->id;
    }

    function test2() {
        $New_Subscriber_for_owner = $this->member_model->get_email("order_payment_success");
        $message2 = $New_Subscriber_for_owner['email_template'];
        $subject2 = $New_Subscriber_for_owner['email_subject'];
        //$message2 = str_replace('#baseurl#', base_url(), $message2);
        // $message2 = str_replace('#OwnerName#', $owner['restaurant_name'], $message2);
        //$message2 = str_replace('#YEAR#', date("Y"), $message2);
        //$message2 = str_replace('#SubscriberID#', $new_userid, $message2);
        //$message2 = str_replace('#SubscriberName#', $array['payment_data']['name_on_card'], $message2);
        $this->load->library('email');
        $this->email->initialize($config);
        $pjt_name = $stripe['project_name'];

        $category = "Payment Report";
        $to_mail = $owner['email'];
        $from = $stripe['email'];
        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
    }

    function test_recipient() {
//        echo "1";

        $this->load->library('stripe_lib');
//        echo "2";
//        $name = "Harry";
//        $last_name = "potter";
//        $email = "harryp2105@gmail.com";
//        $ssn = "111111111";
//        $RoutingNumber = "110000000";
//        $AccountNumber = "000123456789";



        $name = "Gaurav";
        $last_name = "Bazaz";
        $email = "gauravbazaz@gmail.com";
        $ssn = "229672436";
        $RoutingNumber = "021200025";
        $AccountNumber = "1967469642";
//        echo "3";
        $stripe = $this->client_model->getAdmin_strip();
//        echo "4";
//        $this->stripe_lib->setApiKey("sk_test_kI1BmSYqEgX51xHSPG0lsnEb ");
        $this->stripe_lib->setApiKey("sk_live_LOKtTD7UUR7BgHc5bxA9fpbA");
        $Token = $this->stripe_lib->createBankAccount("bank_account", "US", "usd", $RoutingNumber, $AccountNumber);
//        echo "6";
        $params = array(
            "name" => $name . " " . $last_name,
            "type" => "individual",
            "email" => $email,
            "tax_id" => $ssn,
            "bank_account" => $Token['id']
        );
        $recipient = $this->stripe_lib->createRecipient($params);
//        echo "7";
        $array['recipient'] = $recipient->id;
        echo $array['recipient'];
    }

}

?>