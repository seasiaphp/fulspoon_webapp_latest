<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//error_reporting(E_ALL);

class Restaurant_list extends MY_Controller {

    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     * 		http://example.com/index.php/welcome

     * 	- or -  

     * 		http://example.com/index.php/welcome/index

     * 	- or -

     * Since this controller is set as the default controller in 

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see http://codeigniter.com/user_guide/general/urls.html



     * $this->load->library('crud');

     * load crud library if u want to perform basic listing/add/edit and

     * other similar stuffs

     * $this->load->library('preferences');

     * for showing a configuration table where users can only update fields

     */
    public function __construct() {



        parent::__construct();

        $this->_setAsAdmin();

        $this->load->model('restaurant_list_model');
        $this->load->model('user_model');
        $this->load->model('member_model');

        $this->load->helper(array('url', 'cookie'));

        $this->user = $this->session->userdata('user');

        $permission = menuPermissions($this->user->role);

        //echo $permission;



        if ($permission == 0)
            redirect($this->user->root);

        if ($this->user == '')
            redirect('admin');
    }

    public function index() {

        redirect($this->user->root . '/restaurant_list/lists');
    }

    public function check() {

        $username = $_POST['username'];

        $usernameexist = $this->restaurant_list_model->usernameExist($username);

        echo $usernameexist;
    }

    public function add() {



        if ($_SERVER['REQUEST_METHOD'] == 'POST') {





            $usernameexist = $this->restaurant_list_model->usernameExist($this->input->post('username'));

            if (($usernameexist) > 0) {

                $msg = ' UserName Already Exist.!';

                $this->session->set_flashdata('message', $msg, 'SUCCESS');

                redirect('admin/restaurant/add');
            }



            $data = array('restaurant_id' => '',
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'header_color' => "#" . $this->input->post('header_color'),
                'body_color' => "#" . $this->input->post('body_color')
            );

            $id = $this->restaurant_list_model->adddetails('restaurant_master', $data);







            $data = array('admin_id' => '',
                'full_name' => $this->input->post('managername'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'email' => $this->input->post('email'),
                'created_time' => date('Y-m-d H:i:s'),
                'role' => 2,
                'restaurant_id' => $id);

            $this->restaurant_list_model->adddetails('member_admins', $data);

            $msg = 'Restaurant Details Added successfully .!';

            $this->session->set_flashdata('message', $msg, 'SUCCESS');

            redirect('admin/restaurant/lists');
        }

        $output['SUB_TITLE'] = 'Add Restaurant';

        $output['output'] = $this->load->view('admin/restaurant/add', '', true); //loading success view

        $this->_render_output($output);
    }

    public function lists() {



        //echo '<pre>';print_r($_POST);	
        //for pagination

        $config = array();

        $config = $this->pagination();

        $this->load->library('pagination');

        $data['total_rows'] = getConfigValue('default_pagination');

        //$data['total_rows'] 	= 2;

        $_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] : $data['total_rows']) : $_POST['limit']);

        $_REQUEST['key'] = (!$_POST['key'] ? ($_GET['key'] ? $_GET['key'] : '') : $_POST['key']);

        $params = '?t=1';

        if ($_REQUEST['limit'])
            $params .= '&limit=' . $_REQUEST['limit'];

        if ($_REQUEST['key'])
            $params .= '&key=' . $_REQUEST['key'];

        if ($_REQUEST['status'])
            $params .= '&status=' . $_REQUEST['status'];

        $config['base_url'] = site_url("admin/restaurant_list/lists") . "/" . $params;

        $config['total_rows'] = $this->restaurant_list_model->getRestaurantCount($_REQUEST['key'], $_REQUEST['status']);
//exit;
        $config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows'] : $_REQUEST['limit'];

        $data['page'] = $_REQUEST['per_page'];

        $data['limit'] = $_REQUEST['limit'];

        $data['key'] = $_REQUEST['key'];

        $data['status'] = $_REQUEST['status'];





        $this->pagination->initialize($config);

        //----------------------------------------------------------

        $data_gol = $this->restaurant_list_model->getRestaurantLists($_REQUEST['status'], $_REQUEST['key'], $config['per_page'], $_REQUEST['per_page']);
//exit;
        //$key_1 = "AIzaSyAzMWcy2s8X167yaf1NxGYne0n1RSNE4gM";

        $key_1 = $this->member_model->get_key();

        $data['key_1']['value'] = $key_1;

        foreach ($data_gol as $sql_key => $res_data) {

            
            $google_data = $res_data['google_id'];


            //print_r($key_1);
            //print_r($google_data);

//die;

            $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_data&key=$key_1";



            $ch1 = curl_init();

            curl_setopt($ch1, CURLOPT_URL, $uri);

            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');

            $sData = curl_exec($ch1);

            $response13 = json_decode($sData);

            $data_gol[$sql_key]['hotel_data'] = $response13->result;
        }

        $data['userlist'] = $data_gol;









        $output['SUB_TITLE'] = 'Restaurant List';

        $output['output'] = $this->load->view('admin/restaurant/lists', $data, true);

        $this->_render_output($output);
    }

  public function edit_1($restaurant_id) {





      if ($_SERVER['REQUEST_METHOD'] == 'POST') {

      if($_POST['cuisine_id'])

      {



      $cuisi=$_POST['cuisine_id'];

      $cuisi_name=$_POST['cuisine_name'];

      $test=count($_POST['cuisine_id']);

      for($i=0;$i<$test;$i++)

      {

      $this->restaurant_list_model->UpdateDetails_cuisine_data($cuisi[$i],$cuisi_name[$i]);

      }

      }

      $rest=array('alcohol'=>$_POST['alcohol'],'parking'=>$_POST['parking'],'wifi'=>$_POST['wifi'],'corkage'=>$_POST['corkage'],'dietary_restrictions'=>$_POST['dietary_restrictions'],'music'=>$_POST['music'],'sports'=>$_POST['sports'],'wheelchair_accessible'=>$_POST['wheelchair_accessible'],'outdoor_seating'=>$_POST['outdoor_seating'],'smoking'=>$_POST['smoking'],'waiter_service'=>$_POST['waiter_service'],'television'=>$_POST['television'],'ambience'=>$_POST['ambience'],'delivery'=>$_POST['delivery'],'pickup'=>$_POST['pickup']);



      $this->restaurant_list_model->UpdateDetails('restaurant_master',$rest,array('restaurant_id'=>$_POST['restaurant_id']));

      redirect('admin/restaurant_list/lists');



      } else {

      $output['SUB_TITLE'] = 'Edit Restaurant';

      $data['reataurantdetails'] = $this->restaurant_list_model->getAllDetails($restaurant_id);

      $data['reataurantdetails_res'] = $this->restaurant_list_model->getAllDetails_Rest($restaurant_id);

      $data['get_cuisine'] = $this->restaurant_list_model->getAllDetails_cuisine();



      $output['output'] = $this->load->view('admin/restaurant/edit', $data, true); //loading success view

      $this->_render_output($output);

      }

      }



    

    function string_compare($str_a, $str_b) {

        $length = strlen($str_a);

        $length_b = strlen($str_b);



        $i = 0;

        $segmentcount = 0;

        $segmentsinfo = array();

        $segment = '';

        while ($i < $length) {

            $char = substr($str_a, $i, 1);

            if (strpos($str_b, $char) !== FALSE) {

                $segment = $segment . $char;

                if (strpos($str_b, $segment) !== FALSE) {

                    $segmentpos_a = $i - strlen($segment) + 1;

                    $segmentpos_b = strpos($str_b, $segment);

                    $positiondiff = abs($segmentpos_a - $segmentpos_b);

                    $posfactor = ($length - $positiondiff) / $length_b; // <-- ?

                    $lengthfactor = strlen($segment) / $length;

                    $segmentsinfo[$segmentcount] = array('segment' => $segment, 'score' => ($posfactor * $lengthfactor));
                } else {

                    $segment = '';

                    $i--;

                    $segmentcount++;
                }
            } else {

                $segment = '';

                $segmentcount++;
            }

            $i++;
        }



        // PHP 5.3 lambda in array_map      

        $totalscore = array_sum(array_map(function($v) {

                    return $v['score'];
                }, $segmentsinfo));



        if ($totalscore <= 0.5) {

            if (stripos($str_a, $str_b) !== false) {

                $totalscore = 0.7;
            } elseif (stripos($str_b, $str_a) !== false) {

                $totalscore = 0.7;
            }
        }

        return $totalscore;
    }

    public function edit($restaurant_id, $google_id) {

//echo"jas";


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//
            $this->restaurant_list_model->Delete_cuisine($_POST['restaurant_id']);

            foreach ($_POST['cuisine'] as $cuzi) {

                $this->restaurant_list_model->insert_cuisine($_POST['restaurant_id'], $cuzi);
            }

            unset($_POST['cuisine']);



            $loc_id = $_POST['locu_id'];



            if ($_POST['locu_id'] == '' && $_POST['google_id'] != '') {





                $key1 = $this->member_model->get_key();

                $key = $this->member_model->get_key();

                $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" . $_POST['google_id'] . "&key=$key1";

                //       echo $uri_g;
                //    exit;

                $ch1 = curl_init();

                curl_setopt($ch1, CURLOPT_URL, $uri_g);

                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);

                curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');

                $sData = curl_exec($ch1);

                $response13 = json_decode($sData);

                $response_datas1 = $response13->result;

                $lat1 = $response_datas1->geometry->location->lat;

                $long1 = $response_datas1->geometry->location->lng;

                $google_rest_name = $response_datas1->name;

                $res_data = array();

                $res_data['google_place_result'] = $response_datas1;

                $website_url = $res_data['google_place_result']->website;

                $location = str_replace("+", "", urlencode(end(explode(",", $res_data->vicinity))));

                if ($location == '') {

                    $location = $response_datas1->address_components[2]->long_name;
                }



                $address_array = $response_datas1->address_components;

                foreach ($address_array as $address) {

                    if (in_array("postal_code", $address->types)) {

                        $postal_code = $address->long_name;

                        $res_data['postal_code'] = $postal_code;
                    }
                }



                $name3 = str_replace(" ", "%20", $res_data['google_place_result']->name);

                          echo $name3;
                        echo $lat1;
                        echo $long1;
                        echo $location;


                die('hi');

                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&has_menu=TRUE&radius=100&locality=$location&category=restaurant&api_key=3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";

    //                   echo $url_locu."<br/>";
  //                     exit;

//die;
                $ch_locu = curl_init();

                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                $sData3 = curl_exec($ch_locu);

                $response_locu = json_decode($sData3);
                $api_key = $this->user_model->generlConfig('locu_key');
                if (sizeof($response_locu->objects) == 0) {

                    unset($response_locu);

                    $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&locality=$location&category=restaurant&api_key=$api_key";

                    //            echo $url_locu;

                    $ch_locu = curl_init();

                    curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                    curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                    curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                    $sData3 = curl_exec($ch_locu);

                    $response_locu = json_decode($sData3);
                }

                if (sizeof($response_locu->objects) == 0) {

                    unset($response_locu);

                    $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&category=restaurant&api_key=$api_key";

                    $ch_locu = curl_init();

                    curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                    curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                    curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                    $sData3 = curl_exec($ch_locu);

                    $response_locu = json_decode($sData3);
                }

                if (sizeof($response_locu->objects) == 0) {

                    unset($response_locu);

                    $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&locality=$location&radius=20&category=restaurant&api_key=$api_key";

                    $ch_locu = curl_init();

                    curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                    curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                    curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                    $sData3 = curl_exec($ch_locu);

                    $response_locu = json_decode($sData3);
                }

                if (sizeof($response_locu->objects) == 0) {

                    unset($response_locu);

                    $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&radius=100&category=restaurant&api_key=$api_key";

                    $ch_locu = curl_init();

                    curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                    curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                    curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                    $sData3 = curl_exec($ch_locu);

                    $response_locu = json_decode($sData3);
                }

                if (sizeof($response_locu->objects) == 0) {

                    unset($response_locu);

                    $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&category=restaurant&api_key=$api_key";

                    $ch_locu = curl_init();

                    curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                    curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                    curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                    $sData3 = curl_exec($ch_locu);

                    $response_locu = json_decode($sData3);
                }

                if (sizeof($response_locu->objects) == 0) {

                    unset($response_locu);

                    $url_locu = "https://api.locu.com/v1_0/venue/search/?website_url=$website_url&category=restaurant&api_key=$api_key";

                    $ch_locu = curl_init();

                    curl_setopt($ch_locu, CURLOPT_URL, $url_locu);

                    curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);

                    curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');

                    $sData3 = curl_exec($ch_locu);

                    $response_locu = json_decode($sData3);
                }

                //echo "<pre>"; 
                // print_r($response_locu->objects);
                // print_r($response_locu);

                if (sizeof($response_locu->objects) > 1) {

                    unset($near);



                    foreach ($response_locu->objects as $key_locu13 => $locu_152) {

                        $latit = $locu_152->lat;

                        $longit = $locu_152->long;

                        $theta = $long1 - $longit;

                        $dist = sin(deg2rad($lat1)) * sin(deg2rad($latit)) + cos(deg2rad($lat1)) * cos(deg2rad($latit)) * cos(deg2rad($theta));

                        $dist = acos($dist);

                        $dist = rad2deg($dist);

                        $miles = $dist * 60 * 1.1515;

                        $unit = strtoupper($unit);

                        if ($unit == "K") {

                            $return1234 = ($miles * 1.609344);
                        } else if ($unit == "N") {

                            $return1234 = ($miles * 0.8684);
                        } else {

                            $return1234 = $miles;
                        }

                        $return1234 = abs($return1234);

                        //                    echo "<br/>";

                        if (!isset($near)) {

                            $near = $return1234;
                        }

                        if ($return1234 <= $near) {

                            $near = $return1234;

                            unset($response_locu_original);

                            $response_locu_original = $locu_152;

                            $response_locu_original->distanceBetweenLatLong = $return1234;

                            //                        echo "<pre>";
                            //                        print_r($response_locu_original);
                        }
                    }
                } else {

                    //             

                    $response_locu_original = $response_locu->objects[0];

                    //                echo "<pre>";
                    //                print_r($response_locu);
                }

                //        echo'<pre>';
                //        print_r($response_locu_original);
                //        exit;

                $locu_name = $response_locu_original->name;

                $match_val = $this->string_compare($google_rest_name, $locu_name);

                $score = levenshtein($google_rest_name, $locu_name);

                //        echo $match_val;
                //        exit;

                if ($match_val > 1) {

                    $match_val = $this->string_compare($locu_name, $google_rest_name);
                }

                if ($match_val >= 0.5 && $score <= 10) {

                    $locu_id = $response_locu_original->id;

//                    $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
                    $api_key = $this->user_model->generlConfig('locu_key');
                    $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{

									  \"api_key\" : \"$api_key\",

									  \"fields\" : [\"name\"],

									  \"venue_queries\" : [

									  { \"locu_id\":\"$locu_id\" }

									  

									  ]

									  }'";

                    //        echo $uri;

                    $c_pid = exec($uri);

                    $c_pid = json_decode($c_pid);

                    $response_locu_original->locu_menu = $c_pid->venues;

                    $uri4 = "curl -X POST https://api.locu.com/v2/venue/search -d '{

									  \"api_key\" : \"$api_key\",

									  \"fields\" : [\"name\", \"extended\",\"delivery\"],

									  \"venue_queries\" : [

									  { \"locu_id\":\"$locu_id\" }

									  

									  ]

									  }'";

                    $c_pid3 = exec($uri4);

                    $c_pid3 = json_decode($c_pid3);

                    $response_locu_original->locu_extend = $c_pid3;
                } else {

                    unset($response_locu_original);

                    $response_locu_original = array();
                }

                //            } 











                $_POST['restaurant_name'] = $google_rest_name;

                $_POST['formatted_address'] = $response_datas1->vicinity;

                if ($response_locu_original->phone != '') {

                    $_POST['contact_number'] = $response_locu_original->phone;
                } else {

//                    $_POST['contact_number'] = '';
                }

                $_POST['locu_id'] = $response_locu_original->id;
            }

            $sub_val = $_POST['sub_val'];

            unset($_POST['sub_val']);

            $this->restaurant_list_model->UpdateDetails('restaurant_master', $_POST, array('restaurant_id' => $_POST['restaurant_id']));



            if ($loc_id == '' && $_POST['google_id'] != '') {

                $this->menu_list($_POST['restaurant_id']);

                if ($sub_val == 'Y') {

                    redirect('admin/restaurant_list/edit/' . $_POST['restaurant_id'] . '/' . $_POST['google_id']);
                }
            }

            redirect('admin/restaurant_list/lists');
        }

        $this->load->model('member_model');

        $key1 = $this->member_model->get_key();



        $key = $this->member_model->get_key();



        $data['cuisines'] = $this->member_model->select_cuisine();



        $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$key1";

//        echo $uri_g;
//        exit;

        $ch1 = curl_init();

        curl_setopt($ch1, CURLOPT_URL, $uri_g);

        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');

        $sData = curl_exec($ch1);

        $response13 = json_decode($sData);

        $response_datas1 = $response13->result;

        $lat1 = $response_datas1->geometry->location->lat;

        $long1 = $response_datas1->geometry->location->lng;

        $res_data = array();

        $res_data['google_place_result'] = $response_datas1;

        $website_url = $res_data['google_place_result']->website;

        $location = str_replace("+", "", urlencode(end(explode(",", $res_data->vicinity))));

        $name3 = str_replace(" ", "%20", $res_data->name);

//      

        $c_pid3 = exec($uri4);

        $c_pid3 = json_decode($c_pid3);

        $response_locu_original->locu_extend = $c_pid3;

//print_r($response_locu_original->locu_extend);
//die("testet");

        $res_data['locu_data'] = $response_locu_original;

        $a++;

        $google_resp_data[] = $res_data;



        $data['response'] = $google_resp_data;

        $data['google'] = $res_data;

        $data['key'] = $key;

        $data['key1'] = $key1;

        $data['reataurantdetails'] = $this->restaurant_list_model->getAllDetails($restaurant_id);

        foreach ($data['reataurantdetails'] as $cuz) {

            $test[] = $cuz['cusine_id'];
        }



        $data['reataurantdetails'] = $test;

        $data['genaral_config'] = $this->restaurant_list_model->genaral_config();

        $data['reataurantdetails_res'] = $this->restaurant_list_model->getAllDetails_Rest($restaurant_id);
  $ouput['output'] = $this->load->view("admin/restaurant/edit", $data, true);

		
		
		
        $this->_render_output($ouput);
    }

    function menu_list($rid) {

        $this->load->model('user_model');

        $locu_id = $this->member_model->get_locu_id($rid);

//        $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
        $api_key = $this->user_model->generlConfig('locu_key');

        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{

         \"api_key\" : \"$api_key\",

 \"fields\" : [\"name\", \"menus\", \"extended\",\"delivery\" ],

  \"venue_queries\" : [

   { \"locu_id\":\"$locu_id\" }

    

  ]

}'";

        // echo $uri;

        $c_pid = exec($uri);

        $c_pid = json_decode($c_pid);

//        echo'<pre>';
//        print_r($c_pid);
//        exit;

        foreach ($c_pid->venues[0]->menus as $menues) {

            $menu_name = $menues->menu_name;

            $true_menu_name = $menu_name;



//            echo sizeof($menues->sections);

            foreach ($menues->sections as $sec_key => $section) {

                $true_menu_name = $menu_name;

                $sec_menu_name = $menu_name;

                if ($section->section_name != '') {

                    $sec_menu_name = $section->section_name;

                    $true_menu_name = $sec_menu_name;
                }

                foreach ($section->subsections as $sub_key => $subsection) {

                    $true_menu_name = $sec_menu_name;

                    $sub_menu_name = $sec_menu_name;

                    if ($subsection->subsection_name != '') {

                        $sub_menu_name = $subsection->subsection_name;

                        $true_menu_name = $sub_menu_name;
                    }

                    foreach ($subsection->contents as $cont_key => $contents) {

//                     echo '<pre>';   print_r($contents);

                        $dish_name = $contents->name;

                        $desc = $contents->description;

                        $price = $contents->price;

                        $dish_id = $this->member_model->save_cat_dish_rest($dish_name, $desc, $price, $true_menu_name, $rid);

                        $option_grp = $contents->option_groups;

                        $chk_flag = 0;

                        foreach ($option_grp as $option_g) {

                            $chk_flag = 1;

                            if ($option_g->type == "OPTION_CHOOSE") {

                                $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);

//                            

                                $flaggg = 0;

                                foreach ($option_g->options as $options) {

//                                    echo '<pre>';

                                    $flaggg = 1;

                                    $name = $options->name;

                                    $price_op = $options->price;

                                    $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);

                                    $this->member_model->ins_size($size_array);
                                }

                                if ($flaggg != 1) {

                                    $this->member_model->ins_size($size_array);
                                }
                            } elseif ($option_g->type == "OPTION_ADD") {

                                $option_name_title = $option_g->text;

                                if ($option_name_title == '') {

                                    $option_name_title = 'options and sides';
                                }

                                $dish_array = array("restaurant_id" => $rid, "dish_item_id" => $dish_id, "option_name" => $option_name_title, "option_name_original" => $option_name_title);

                                $dish_id3 = $this->member_model->ins_dish($dish_array);

                                foreach ($option_g->options as $options) {

                                    $name = $options->name;

                                    $price_op1 = explode("+", $options->price);

                                    $price_op = $price_op1[1];

                                    $dish_array = array("option_id" => $dish_id3, "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);

                                    $this->member_model->ins_side_options($dish_array);
                                }

//                                foreach ($option_g->options as $options) {
//                                    echo '<pre>';
//
//                                    $name = $options->name;
//                                    $price_op = $options->price;
//                                    $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
//                                    $this->member_model->ins_size($size_array);
//                                }
                            }
                        }

                        if ($chk_flag == 0) {

                            $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);

                            $this->member_model->ins_size($size_array);
                        }
                    }

//                    exit;
                }
            }
        }

//        redirect("owners/category/$rid");
    }

    public function bulkAction($bulkaction_list = '', $res_id) {



        $bulkaction = $this->input->post('bulkaction');



        $id = $this->uri->segment(5);

        $res_id = $this->input->post('sel') ? $this->input->post('sel') : $id;



        if ($bulkaction == '')
            $bulkaction = 'delete';



        if ($bulkaction) {

            if ($res_id) {

                switch ($bulkaction) {

                    case 'delete':



                        $delete_id = $this->restaurant_list_model->bulkDelete($res_id);

                        $this->session->set_flashdata('message', 'Restaurant(s) Successfully Deleted ');

                        break;
                }
            } else {

                $this->session->set_flashdata('message', 'Please select at least one member.! ', 'ERROR');
            }
        }

        redirect('admin/restaurant_list/lists');
    }

    public function ajaxblock() {



        $id = $this->input->post('id');

        $block = $this->input->post('is_block') == 'Y' ? 'N' : 'Y';
        $chkchain = $this->restaurant_list_model->checkChainMain($id);
//        echo trim($chkchain);
//        exit;


        if ($chkchain == 0) {
            $this->restaurant_list_model->UpdateDetails('restaurant_master', array('status' => $block), array('restaurant_id' => $id));

            if ($block == 'Y') {

                $this->member_model->update_expire_flag($id, "N");

                $this->session->set_flashdata('message', "  Restaurant Unblocked ");
            } else {

                $this->member_model->update_expire_flag($id, "Y");

                $this->change_subscription_auto($id);



                $this->session->set_flashdata('message', "  Restaurant Blocked ");
            }

            $this->session->set_flashdata('class', "success");
        } else {
            echo 'Fail';
        }
    }

	public function ajaxmenuentrystatus() {



        echo $id = $this->input->post('id');

        $block = $this->input->post('status_id') == 'Y' ? 'N' : 'Y';
		echo $block;
		
        $chkchain = $this->restaurant_list_model->checkChainMain($id);
//        echo trim($chkchain);
//        exit;


        if ($chkchain == 0) {
            $this->restaurant_list_model->UpdateDetails('restaurant_master', array('menu_api_status' => $block), array('restaurant_id' => $id));

            if ($block == 'Y') {

               // $this->member_model->update_expire_flag($id, "N");

               // $this->session->set_flashdata('message', "  Restaurant Unblocked ");
            } else {

              //  $this->member_model->update_expire_flag($id, "Y");

                //$this->change_subscription_auto($id);



                $this->session->set_flashdata('message', "  Restaurant Blocked ");
            }

            $this->session->set_flashdata('class', "success");
        } else {
            echo 'Fail';
        }
    }

	
	
	
    public function change() {



        $data['admin_id'] = $_POST['admin_id'];

        echo $this->load->view('admin/restaurant/changepassword', $data);
    }

    public function changepwd() {



        $new_pwd = $_POST['new_pwd'];

        $admin_id = $_POST['admin_id'];

        $data = array('password' => $new_pwd);



        if ($this->restaurant_list_model->updateDetails('member_admins', $data, array('admin_id' => $admin_id)) == true) {

            echo "success";

            exit;
        } else {

            echo "error";

            exit;
        }
    }

    public function pagination() {



        $config['page_query_string'] = TRUE;

        $config['first_link'] = 'First';

        $config['last_link'] = 'Last';



        $config['full_tag_open'] = "<ul class='pagination'>";

        $config['full_tag_close'] = "</ul>";

        $config['num_tag_open'] = '<li>';

        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";

        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";

        $config['next_tag_open'] = "<li>";

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";

        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";

        $config['first_tagl_close'] = "</li>";

        $config['last_tag_open'] = "<li>";

        $config['last_tagl_close'] = "</li>";



        return $config;
    }

    function test1() {

        $sql = "SELECT SUM(A4.total_amount) AS total_amount,A3.first_name,A3.email ,A1.*,A2.*,A3.first_name 

				   FROM subscription_log A1  

				   LEFT JOIN current_restaurant A2 ON  A1.subscription_id =A2.subscription_id

				   LEFT JOIN member_master A3 ON A3.member_id=A1.member_id

				   LEFT JOIN order_master A4 ON A4.subscription_id=A1.subscription_id  

				   WHERE  start_date<='2016-03-18' AND A1.End_date>='2016-03-18'  AND A2.restaurant_id='165' AND A3.member_id IS NOT NULL GROUP BY A1.log_id";

        $query = $this->db->query($sql);

        $result = $query->result_array();



        echo "

       <tr><td>Total Amount</td><td>Name</td><td>Email</td><td></td></tr>

	  

     ";

        // echo "haiii";
        //echo json_encode($result[0]);
    }

    public function approve_disable() {
	
	
	//die('test');
	
	$id = $this->input->post('id');
	  $this->load->model('email_model');
	  
	   $block = $this->input->post('is_block') == 'Y' ? 'N' : 'Y';
	 
	   
	    
	   $this->load->model('restaurant_list_model');
        $res_id = $id;
        $data = $this->restaurant_list_model->getAllDetails_Rest($res_id);
		$full_name = $data['restaurant_name'];
	   if($block == "Y"){
	   
	  
		
	 $email = $this->email_model->get_email_template('Approve_Restaurant');

           $this->load->library('encrypt');
            $full_name = $data['restaurant_name'];
            $target_name = $this->config->item('site_name');
           
            $data_1 = $this->member_model->admin_contratct();
            $subject = $email['email_subject'];
            $message = $email['email_template'];
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html; charset=utf-8" . "\r\n";
            $headers .= 'From: ' . $data_1['project_name'] . '<' . $data_1['email'] . ">\r\n";

          $message = str_replace("#name#",$full_name.",",$message);
		  $message = str_replace("#YEAR#","2017",$message);

	 $this->load->library('email');
	  $this->email->from($data_1['email']); //change it
   $this->email->to($data['email']); //change it
   $this->email->subject($subject);
   $this->email->message($message);
   $this->email->from( $data_1['email'], $data_1['project_name']);

   
   
   if ($this->email->send())
   {
      $data['success'] = 'Yes';
   }
   else
   {
      $data['success'] = 'No';
      $data['error'] = $this->email->print_debugger(array(
         'headers'
      ));
   }

 }
   if($block == "N"){
    
    // for owner
           $email = $this->email_model->get_email_template('Disable_Restaurant');

           $this->load->library('encrypt');
          
            $target_name = $this->config->item('site_name');
           
            $data_1 = $this->member_model->admin_contratct();
            $subject = "Restaurant". $full_name." ". $email['email_subject'];
            $message = $email['email_template'];
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html; charset=utf-8" . "\r\n";
            $headers .= 'From: ' . $data_1['project_name'] . '<' . $data_1['email'] . ">\r\n";

          $message = str_replace("#restaurant#",$full_name.",",$message);
          $message = str_replace("#name#",$member_data['first_name'].",",$message);
          $message = str_replace("#YEAR#","2017",$message);

     $this->load->library('email');
      $this->email->from($data_1['email']); //change it
   $this->email->to($data['email']); //change it
   $this->email->subject($subject);
   $this->email->message($message);
   $this->email->from( $data_1['email'], $data_1['project_name']);

   
   
   if ($this->email->send())
   {
      $data['success'] = 'Yes';
   }
   else
   {
      $data['success'] = 'No';
      $data['error'] = $this->email->print_debugger(array(
         'headers'
      ));
   }


    // end
   
    $this->load->model('restaurant_list_model');
	 $this->load->model('client_model');

	 $data = $this->client_model->get_rest_user($id);
     //echo"<pre>";print_r($data);die('test');
	 $this->load->model('member_model');
   foreach($data as $value){
   
    
    
   $member_data = $this->member_model->get_user($value['member_id']);
     
	 //echo $member_data['email']."<br>";
    
   
		
	 $email = $this->email_model->get_email_template('Disable_Restaurant');

           $this->load->library('encrypt');
          
            $target_name = $this->config->item('site_name');
           
            $data_1 = $this->member_model->admin_contratct();
            $subject = $email['email_subject'];
            $message = $email['email_template'];
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html; charset=utf-8" . "\r\n";
            $headers .= 'From: ' . $data_1['project_name'] . '<' . $data_1['email'] . ">\r\n";

          $message = str_replace("#restaurant#",$full_name.",",$message);
		  $message = str_replace("#name#",$member_data['first_name'].",",$message);
		  $message = str_replace("#YEAR#","2017",$message);

	 $this->load->library('email');
	  $this->email->from($data_1['email']); //change it
   $this->email->to($member_data['email']); //change it
   $this->email->subject($subject);
   $this->email->message($message);
   $this->email->from( $data_1['email'], $data_1['project_name']);

   
 
   if ($this->email->send())
   {
      $data['success'] = 'Yes';
   }
   else
   {
      $data['success'] = 'No';
      $data['error'] = $this->email->print_debugger(array(
         'headers'
      ));
   }
   
   
   }
 
   
   
   
   }
 
 
 
 
        
        $block = $this->input->post('is_block') == 'Y' ? 'N' : 'Y';
        $this->restaurant_list_model->UpdateDetails('restaurant_master', array('status' => $block), array('restaurant_id' => $id));
        return true;
    }

    public function del_goog() {





        $this->db->where('restaurant_id', $_POST['id']);

        $this->db->update('restaurant_master', array('google_id' => ''));

        return true;
    }

    function change_subscription_auto($res_id) {

        $this->load->model('member_model');

        $this->load->model('user_model');

        $current_subscription = $this->member_model->find_sub($res_id);

//        echo '<pre>';
//        print_r($current_subscription);

        foreach ($current_subscription as $curr_sub) {

            $number_of_rest = $curr_sub['number_of_restaurants'] - 1;



            if ($number_of_rest > 0) {

                $sub_id = $curr_sub['subscription_id'];

                $member_id = $curr_sub['member_id'];

                $mynext_sub = $this->member_model->find_next_sub($number_of_rest);

                $rest_list = $this->member_model->get_rest_list($sub_id, $member_id, $res_id);

//                $card_id = $this->member_model->select_card($member_id);

                $strip_id = $curr_sub['strip_id'];

                $User_subscription_master = array("member_id" => $member_id,
                    "package_id" => $mynext_sub['sub_package_id'],
                    "strip_id" => $strip_id);



                $new_sub_prize = $this->user_model->get_package_details($mynext_sub['sub_package_id']);

                $next_restaurant_status = $this->user_model->next_restaurant_status($member_id);

                if (isset($next_restaurant_status['member_id'])) {

                    $next_restaurant_status = $this->user_model->update_next_restaurant_status($next_restaurant_status['subscription_id']);
                }



                $user_sub_id = $this->user_model->insert_1('subscription_next', $User_subscription_master);

                foreach ($rest_list as $rest) {

                    $array_rest = array("member_id" => $member_id, "restaurant_id" => $rest['restaurant_id'], "subscription_id" => $user_sub_id, "create_date" => date("Y-m-d"));

                    $this->member_model->save_res_user1($array_rest);
                }
            }
        }
    }

}
?>

