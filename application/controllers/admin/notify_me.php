<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
class notify_me extends My_Controller {


public function __construct() {                        
		parent::__construct();
		$this->_setAsAdmin();
		 
		$this->load->helper(array('url','cookie'));	
		$this->load->model('admin/notify_me_model');	
		$this->load->model('restaurant_model');		
}

public function index(){
	redirect($this->user->root.'/notify_me/lists');
}

public function lists($res_id){
  
        $role=$this->session->userdata('user')->role;
		if($this->session->userdata('user')!=''){
			$user				 	= $this->session->userdata('user');
			$restaurant_id			= $user->restaurant_id;
            //for pagination
			$config					= array();
			$config=$this->pagination();
			$this->load->library('pagination');
			$data['total_rows']= getConfigValue('default_pagination');
			//$data['total_rows']		= 2;
			$_REQUEST['limit'] 		= (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);
			$params					= '?t=1';
				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];
			$config['base_url']		= site_url($this->user->root."/notify_me/lists")."/".$params;
			if($role==1){
			$restaurant_id=$_POST['rest'];
			//print_r( $restaurant_id);
			$config['total_rows'] 	= $this->notify_me_model->countCustomers1($_REQUEST['key'],$restaurant_id);
			}
			
			else{
			$config['total_rows'] 	= $this->notify_me_model->countCustomers($restaurant_id,$_REQUEST['key']);
			}
			$config['per_page'] 	= $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
			$data['page'] 			= $_REQUEST['per_page'];
			$data['limit'] 			= $_REQUEST['limit'];
			$data['key']  			= $_REQUEST['key'];
			$this->pagination->initialize($config);		
			 $restaurant_id=$_POST['rest'];
			//print_r( $restaurant_id);
			 $data['customerdetails'] = $this->notify_me_model->getCustomerDetails1($_REQUEST['key'],$config['per_page'],$_REQUEST['per_page']);	
			// print_r($data['customerdetails']);
			$ouput['output']		 = $this->load->view('admin/notify_me/lists',$data,true);
			$this->_render_output($ouput);
		
		}else{
			redirect('admin');
		}
	}
	
	
public function details($member_id){
	if($this->session->userdata('user')!=''){
	if($_SERVER['REQUEST_METHOD']=='POST'){	
		   $member_id 		 = $this->input->post('member_id');
			$data			 = array('first_name'=>$this->input->post('first_name'),
									 'last_name'=>$this->input->post('last_name'),
									 'email'=>$this->input->post('email'),
									 'phone'=>$this->input->post('phone'));	
			$this->notify_me_model->UpdateDetails('member_master',$data,array('member_id'=>$member_id));
		    $this->session->set_flashdata('message', 'Customer Details successfully updated.!' ,'SUCCESS');	
	         redirect($this->user->root.'/notify_me/lists');
	
	}
			$user				 	= $this->session->userdata('user');
			$restaurant_id			= $user->restaurant_id;
			$member_id	    = $member_id?$member_id:($this->input->get('member_id')?$this->input->get('member_id'):0);
			if($member_id!=''){
			$data['address'] 			= $this->notify_me_model->getCustomeraddress($member_id);
			$data['result'] 			= $this->notify_me_model->getCustomerInfo($member_id);
			$data['strip'] 			    = $this->notify_me_model->getStripInfo($member_id);
			$data['payment_history']    = $this->notify_me_model->payment_history($member_id);
			
			if(count($data['result'])==0){
					$data['error']="No user found";
				}else{
					$data['error']='';
				}
		    }
			else{
				$data['error']='';
				$data['result'] = $_POST;
			}
			$ouput['output']			= $this->load->view('admin/notify_me/details',$data,true);
			$this->_render_output($ouput);
	}else{
		redirect('admin');
	}
		
}
public function ajaxblock(){


  
			$id 		=	$this->input->post('id');
			$block		=	$this->input->post('is_block') == 'Y' ? 'N':'Y';
			$this->notify_me_model->UpdateDetails('member_master',array('status'=>$block),array('member_id'=>$id));					
				if($block=='Y')
				{						
					$this->session->set_flashdata('message', "  User Unblocked ");
				}
				else{
					$this->session->set_flashdata('message', "  User Blocked ");
				}
			$this->session->set_flashdata('class', "success");
			
			
}
public function edit($member_id){	
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$member_id 		 = $member_id?$member_id:$this->input->post('member_id');
			$data			 = array('first_name'=>trim($this->input->post('first_name')),
									 'last_name'=>trim($this->input->post('last_name')));	
			$this->notify_me_model->UpdateDetails('fk_member_master',$data,array('member_id'=>$member_id));
		    $this->session->set_flashdata('message', 'Customer Details successfully updated.!' ,'SUCCESS');	
		    redirect($this->user->root.'/customers/lists');
		}
	    

} 	
public function pagination(){
		
			$config['page_query_string'] = TRUE;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";			
			
			return $config;		
		
		
}
public function delete($e_id){
	     //$member_id	    = $member_id?$member_id:($this->input->get('member_id')?$this->input->get('member_id'):0);	
           $this->notify_me_model->bulkDelete($e_id);
		  $this->session->set_flashdata('message', 'User Details successfully deleted.!' ,'SUCCESS');	
		   redirect($this->user->root.'/notify_me/lists?limit='.$_REQUEST['limit'].'& per_page='.$_REQUEST['per_page'].'& key='.$_REQUEST['key']);
} 











####################excel
	#Function to Export data As Excel 	
	function file_xls()
	{
	
		//load our new PHPExcel library
		$this->load->library('excel');
		

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
	
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Report');
	
		//$query 		=		 $this->db->get('child_info');
		$query1=$this->notify_me_model->file_xls_test();
		
		 $query=$this->db->query($query1);
	
		 if(!$query)
            return false;
 
	   $fields 		=		 $query->list_fields();
	//var_dump($fields);
	  
	 	  
	   $col 		=		 0;
       foreach ($fields as $field)
       {
	   		if($field=='email')		$field1='email';
			if($field=='zipcode')		$field1='zipcode';
			if($field=='date_time')		$field1='date_time';
			
			
			/*if($field=='nickelodeon_signup')	$field1='Nickelodeon Signup';
			if($field=='nick_jr_signup')		$field1='Junior Signup';
			if($field=='email_address')			$field1='Email';*/
			//if($field=='email_subscription')	$field1='Email Subscription';
			
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field1);
            $col++;
       }
 		
        // Fetching the table data
		
        $row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {	
				if($field=='email')
				{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					/*if($data->$field=='Y')
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "YES");
					}
					else if($data->$field=='')
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "YES");
					}
					else
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "NO");
					}*/
				}
				
				
				
				
				
				else if($field=='zipcode')
				{
					if($data->$field=='' or $data->$field=='NULL')
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, " ");
					}
					
					else
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					}
				}
				
				else if($field=='date_time')
				{ 
					if($data->$field=='' or $data->$field=='NULL')
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, " NA");
					}
					
					else
					{
						$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					}
				}
				
				
				
				
				
			  else
				{
				  $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
				}
                $col++;
            }
 
            $row++;
        }
	
		
		$filename='Report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	
    }
  






		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */