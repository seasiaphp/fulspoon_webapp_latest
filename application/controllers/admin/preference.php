<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preference extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 * $this->load->library('crud');
	 * load crud library if u want to perform basic listing/add/edit and
	 * other similar stuffs
	 * $this->load->library('preferences');
	 * for showing a configuration table where users can only update fields
	 */	
	public function __construct()
	{
		parent::__construct();
		$this->_setAsAdmin();
		$this->load->model('preference_model');
		$this->load->model('admin/subscription_model');
		
		$this->user 	= $this->session->userdata('user');
		if($this->user=='')
			redirect('admin');		
			
	}
    public function index(){
	
		redirect($this->user->root.'/preference/lists_package');
	
	}
	public function lists($location_id=''){
		$user = $this->session->userdata('user');
		$restaurant_id=$user->restaurant_id;
		if($location_id=='')
			$location_id=$user->location_id;	
				
		
		$data['restconfiglist']=$this->preference_model->get_restConfig($location_id);
		//$data['restconfiglist']=$this->preference_model->get_many_by(array('location_id'=>$location_id));
		//echo '<pre>';print_r($data['restconfiglist']);exit;
		$this->load->model('config_model');
		$data['generalconfiglist']=$this->config_model->get_many_by(array('editable_to_restaurant'=>'Y'));
		
		if(count($data['restconfiglist'])!=0){
			$data['configlist']=$data['restconfiglist'];
		}else{
			$data['configlist']=$data['generalconfiglist'];
		}
		//exit;
		
		if($_SERVER['REQUEST_METHOD']=='POST')
		{
			$post_title	=	$_POST['title'];
			$post_aar	=	$_POST;
			//echo '<pre>';print_r($_POST);
			if(count($data['restconfiglist'])!=0){
				foreach ($post_aar as $key => $value) 
				{
					if($key!='title'){
						$update_id = $this->preference_model->update_by(array('config_id'=>$key,'location_id'=>$location_id), array('value'=>$value));
						//echo $this->db->last_query();
					}
				}
			}else{
				
				$i=0;
				foreach ($post_aar as $key => $value) 
				{
				
					if($key!='title'){
						$dd=array('config_id'=>$key,
								  'field'=>$key,
								  'value'=>$value,
								  'location_id'=>$location_id,
								  'description'=>$post_title[$i]);
						$update_id = $this->preference_model->insert($dd);
						$i++;
					}
					
				}
				
			}
			
			
			$this->session->set_flashdata('message', 'Preferences  Updated Successfully. ','SUCCESS');				
			redirect($this->user->root.'/preference/lists/'.$location_id);
		} 
		$output['output']=$this->load->view('admin/preference/preference', $data, true);
		$this->_render_output($output);
	}
	 public function lists_package(){
	 	$data1			 = array('active'=>'Y');
	 $data['subscription']=$this->subscription_model->get_package_full_detail();
	  $data['limit']='10';
	 $output['output']=$this->load->view('admin/subscription/lists',$data,true);
	 $this->_render_output($output);
	 }
	 public function add($sub_id)
	 {
	
	 $this->load->model('admin/subscription_model');
	
	 	if($_SERVER['REQUEST_METHOD']=='POST'){	
		
					 
					   			$data			 = array('frequency'=>$this->input->post('frequency'),
								                 'duration'=>$this->input->post('duration'),
												 'number_of_restaurants'=>$this->input->post('number_of_restaurants'),
												 'package_name'=>$this->input->post('package_name'),
												 'number_end'=>$this->input->post('uses'),
												 'discount '=>$this->input->post('discount'),
												 'user_fee'=>$this->input->post('user_fee'),
												 'add_date '=>date("Y-m-d") ,
												 'show_web'=>$this->input->post('active')
												 );
						if($this->input->post('sub_id'))
						{
						$this->subscription_model->subscription_update($data,$this->input->post('sub_id'));
						}	
						else
						{					 	
						$this->subscription_model->subscription_insert($data);
						}
						$this->session->set_flashdata('message', 'Details Added Successfully','SUCCESS');
					    redirect('admin/preference/lists_package');
	 }

	if($sub_id)
	{
	$data['sub_detail']= $this->subscription_model->sub_detail($sub_id);
	}
	 
	 $output['output']                 = $this->load->view('admin/subscription/add',$data,true);//loading success view
	$this->_render_output($output);
	 }
	 
	 
	 public function ajaxblock(){
	        $this->load->model('restaurant_model');
			$id 	=	$this->input->post('id');
			$block		=	$this->input->post('is_block') == 'Y' ? 'N':'Y';
			$this->restaurant_model->UpdateDetails_subscription_package($block,$id);					
			if($block=='Y')
			{						
					$this->session->set_flashdata('message', "  Restaurant Unblocked ");
			}
			else
			{
					$this->session->set_flashdata('message', "  Restaurant Blocked ");
			}
			$this->session->set_flashdata('class', "success");
			
			
}
public function delete($id){

 $data=$this->subscription_model->get_sub_details($id);

 if(sizeof($data)>0)
 {
 $this->session->set_flashdata('message', 'Sorry,You cant deleted.!' ,'SUCCESS');	
 }
 else
 {
     $this->subscription_model->bulkDelete($id);
    $this->session->set_flashdata('message', 'Details successfully deleted.!' ,'SUCCESS');	
}			
		   redirect($this->user->root.'/preference/lists_package?limit='.$_REQUEST['limit'].'& per_page='.$_REQUEST['per_page']);
}

public function check_exist()
{
 $data=$this->subscription_model->check_exist($_POST);
 if($data)
 {
 echo "sry";
 }
 else
 {
 echo "success";
 }
exit;
}
}
