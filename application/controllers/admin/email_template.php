<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

error_reporting(0);
ini_set("display_errors","off");
class Email_template extends MY_Controller {

    public function __construct() 
    {
        parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('admin/email_model');
		$session_id = $this->session->userdata('admin_id');
   }

	//loadning login area
	public function index()
	{
		
		 $this->load->library('session');
		$this->load->library('pagination');
		$config['base_url']   = base_url().'admin/email_template/index';
		$config['total_rows'] =  $this->email_model->get_email_count();
		$config['per_page']   = 20;//$this->config->item('page_num');
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
        $config['next_link']						= 'Next';
        $config['prev_link']						= 'Prev';
        $config['cur_tag_open'] 					= '<a class="hover"><b>';
        $config['cur_tag_close']					= '</b></a>';
		$config['cur_page'] = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['title'] = 'Welcome to Administrator Area - List Email templates';
	    //fetching email list
		$config['per_page'] = "100";
		$data['cms'] = $this->email_model->get_email_list( $config['per_page'],$this->uri->segment(3));
		$output['output']=$this->load->view('admin/email/email_list',$data,true);
	    $this->_render_output($output);
		
	}
	//edit email templates
	function edit_template($id='')
	{
	 
	    $cmsid = $id;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email_subject', 'Email Subject', 'required');
		$this->form_validation->set_rules('email_template', 'Email Content', 'required');
		   			
	    if(isset($_POST['id']) && $_POST['id'] > 0 ){
	     	 if ($this->form_validation->run() == FALSE)
			 {
				 $data['cms'] = $this->email_model->get_email_id($_POST['id']);
			  	 $output['output']= $this->load->view('admin/email/edit_email_template',$data,true);
				 $this->_render_output($output);
		     }else
			 {
                 $cmsid = $_POST['id'];
				 unset($_POST['id']);
				 unset($_POST['button']);
				 //updating cms
				 $this->session->set_flashdata('message', 'Email Template Updated Successfully');
				 $this->email_model->update_email($_POST,$cmsid);
				 redirect('admin/email_template');
				
			 }
	   
	   }else{
		   if($cmsid > 0 ){
		      $data['cms'] = $this->email_model->get_email_id($cmsid);	 
			  if( count( $data['cms'] ) < 1 ){
			     redirect('email_template');
			  }
			 $output['output']= $this->load->view('admin/email/edit_email_template',$data,true);
			 $this->_render_output($output);
			  }else{
			 redirect('email_template');
		   }
	}
	}//end of email edit
    //email view
	function view_email($id){
	   if( $id > 0 ){
	       $cms_data['cms'] = $this->email_model->get_email_id($id);
		     $this->load->view('admin/email_pop',$cms_data);
	   
	   }else{
	     redirect('email/index');
	   }				
	}
}