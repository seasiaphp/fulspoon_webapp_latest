<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 * $this->load->library('crud');
	 * load crud library if u want to perform basic listing/add/edit and
	 * other similar stuffs
	 * $this->load->library('preferences');
	 * for showing a configuration table where users can only update fields
	 */	
	public function __construct()
	{
		parent::__construct();
		$this->_setAsAdmin();
		$this->load->model('config_model');
		$this->user 	= $this->session->userdata('user');
		if($this->user=='')
			redirect('admin');		
	}
    public function index(){
	
		redirect('admin/configuration/settings');
	
	}
	public function settings(){
	
	
	
		$data['pref']=$this->config_model->get_all();
	
		if($_SERVER['REQUEST_METHOD']=='POST')
		
		{
	
	
		$admin_array=array(
		'username'=>$_POST['username'],
		'password'=>$_POST['password'],
		'email'=>$_POST['email'],
		'stripe_private_key'=>$_POST['stripe_private_key'],
		'stripe_public_key'=>$_POST['stripe_public_key'],
		'month_subscriber'=>$_POST['month_subscriber'],
		'average_order'=>$_POST['average_order'],
		'google_id'=>$_POST['google_id'],
		
		);
			 
		    $update_id = $this->config_model->update_admin($_POST['admin_id'],$admin_array);
			 $post_aar	=	$_POST;
			 unset($post_aar['admin_id'],$post_aar['username'],$post_aar['password'],$post_aar['email'],$post_aar['stripe_private_key'],$post_aar['stripe_public_key'],$post_aar['month_subscriber'],$post_aar['average_order'],$post_aar['google_id']);
		 	foreach ($post_aar as $key => $value) 
			{
				$update_id = $this->config_model->update_general_config(array('field'=>$key), array('value'=>$value));
			}
			$this->session->set_flashdata('message', 'Settings  Updated Successfully. ','SUCCESS');				
			redirect('admin/configuration/settings');
		} 
		$data['pref_general_config']=$this->config_model->get_all_from_general_config();
		
		$output['output']=$this->load->view('admin/settings/config', $data, true);
		$this->_render_output($output);
	}
	
	public function changePassowrd(){
		$output['output']=$this->load->view('admin/settings/changePassword','',true);
		$this->_render_output($output);
	}
	public function change(){
		$this->load->model('config_model');		
		if($_SERVER['REQUEST_METHOD']=='POST')
		{	
			$old_passord=$this->input->post('old_passord');
			$user=$this->session->userdata('user');
			
				$new_password=$this->input->post('new_password');
				$confirm_password=$this->input->post('confirm_password');
				$result=$this->config_model->change_password($user->username,$old_passord,$new_password);
				if($result!=0)
				{ 
					$this->session->set_flashdata('success_pwd', 'Password changed successfully');
					redirect('admin/configuration/changePassowrd');
				}
				else
				{
					$this->session->set_flashdata('error_pwd', 'Old Password was incorrect');
					redirect('admin/configuration/changePassowrd');
				}
				
		}
		else{
			redirect('admin/home');	
		}
	}
	
	public function emailconfig(){
		$data['templatelist']=$this->config_model->getAllEmailtemplates();
		//echo "<pre>"; print_r($data['templatelist']); echo "</pre>"; exit;
		$output['output']=$this->load->view('admin/settings/emailconfig',$data,true);
		$this->_render_output($output);
	}
	public function addemail($id=''){
	$this->load->library('form_validation');
		if($_SERVER['REQUEST_METHOD']=='POST')
		{	
		$this->form_validation->set_rules('email_title', 'Email Title', 'required');
		$this->form_validation->set_rules('email_subject', 'Email Subject', 'required');
		if ($this->form_validation->run() === TRUE){
		
			$data=array(
				'email_title'=>$this->input->post('email_title'),
				'email_template'=>$this->input->post('content'),
				'email_subject'=>$this->input->post('email_subject')
			);
			$email_id=$this->input->post('email_id');
			$id = $this->config_model->updateEmailTemplate($email_id, $data);		
			$msg = ' Email Template successfully updated.!' ;
			$this->session->set_flashdata('message', $msg ,'SUCCESS');					
			redirect('admin/configuration/emailconfig');
		}
		else
		{
		$data['details']=array(
				'email_title'=>$this->input->post('email_title'),
				'email_template'=>$this->input->post('content'),
				'email_subject'=>$this->input->post('email_subject')
			);
			$output['output']=$this->load->view('admin/settings/addemailconfig',$data, true);
			$this->_render_output($output);
			
		}
		}
		
		else
		{
			$data['details'] = $this->config_model->getEmilDetails($id);
			//print_r($data['details']);exit;
			//print_r($data['details']);exit;
			$output['output']=$this->load->view('admin/settings/addemailconfig',$data, true);
			$this->_render_output($output);	
		}

	}		
	
}
