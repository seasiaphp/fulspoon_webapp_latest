<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
class User_list extends My_Controller {


public function __construct() {                        
		parent::__construct();
		$this->_setAsAdmin();
		$this->load->helper(array('url','cookie'));	
		$this->load->model('admin/user_model');	
		$this->load->model('restaurant_model');		
}

public function index(){
	redirect($this->user->root.'/user_list/lists');
}

public function lists($res_id){
  
        $role=$this->session->userdata('user')->role;
		if($this->session->userdata('user')!=''){
			$user				 	= $this->session->userdata('user');
			$restaurant_id			= $user->restaurant_id;
            //for pagination
			$config					= array();
			$config=$this->pagination();
			$this->load->library('pagination');
			$data['total_rows']= getConfigValue('default_pagination');
			//$data['total_rows']		= 2;
			$_REQUEST['limit'] 		= (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);
			$params					= '?t=1';
				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];
			$config['base_url']		= site_url($this->user->root."/user_list/lists")."/".$params;
			if($role==1){
			$restaurant_id=$_POST['rest'];
			//print_r( $restaurant_id);
			$config['total_rows'] 	= $this->user_model->countCustomers1($_REQUEST['key'],$restaurant_id);
			}
			
			else{
			$config['total_rows'] 	= $this->user_model->countCustomers($restaurant_id,$_REQUEST['key']);
			}
			$config['per_page'] 	= $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
			$data['page'] 			= $_REQUEST['per_page'];
			$data['limit'] 			= $_REQUEST['limit'];
			$data['key']  			= $_REQUEST['key'];
			$this->pagination->initialize($config);		
			 $restaurant_id=$_POST['rest'];
			//print_r( $restaurant_id);
			 $data['customerdetails'] = $this->user_model->getCustomerDetails1($_REQUEST['key'],$config['per_page'],$_REQUEST['per_page']);	
			// print_r($data['customerdetails']);
			$ouput['output']		 = $this->load->view('admin/users/lists',$data,true);
			$this->_render_output($ouput);
		
		}else{
			redirect('admin');
		}
	}
	
	
public function details($member_id){
	if($this->session->userdata('user')!=''){
	if($_SERVER['REQUEST_METHOD']=='POST'){
		   $member_id 		 = $this->input->post('member_id');
			$data			 = array('first_name'=>$this->input->post('first_name'),
									 'last_name'=>$this->input->post('last_name'),
									 'email'=>$this->input->post('email'),
									 'phone'=>$this->input->post('phone'));	
			$this->user_model->UpdateDetails('member_master',$data,array('member_id'=>$member_id));
		    $this->session->set_flashdata('message', 'Customer Details successfully updated.!' ,'SUCCESS');	
	         redirect($this->user->root.'/user_list/lists');
	
	}
			$user				 	= $this->session->userdata('user');
			$restaurant_id			= $user->restaurant_id;
			$member_id	    = $member_id?$member_id:($this->input->get('member_id')?$this->input->get('member_id'):0);
			if($member_id!=''){
			$data['address'] 			= $this->user_model->getCustomeraddress($member_id);
			$data['result'] 			= $this->user_model->getCustomerInfo($member_id);
			$data['strip'] 			    = $this->user_model->getStripInfo($member_id);
			$data['payment_history']    = $this->user_model->payment_history($member_id);
			
			foreach($data['payment_history'] as $key=>$restaurant_list)
			{
			$data['payment_history'][$key]['restaurant']=$this->user_model->get_restaurantdetails_subscription($member_id,$restaurant_list['subscription_id_new']);
			}
			
			if(count($data['result'])==0){
					$data['error']="No user found";
				}else{
					$data['error']='';
				}
		    }
			else{
				$data['error']='';
				$data['result'] = $_POST;
			}
			$ouput['output']			= $this->load->view('admin/users/details',$data,true);
			$this->_render_output($ouput);
	}else{
		redirect('admin');
	}
		
}
public function ajaxblock(){
  
			$id 		=	$this->input->post('id');
			$block		=	$this->input->post('is_block') == 'Y' ? 'N':'Y';
			$this->user_model->UpdateDetails('member_master',array('status'=>$block),array('member_id'=>$id));					
				if($block=='Y')
				{						
					$this->session->set_flashdata('message', "  User Unblocked ");
				}
				else{
					$this->session->set_flashdata('message', "  User Blocked ");
				}
			$this->session->set_flashdata('class', "success");
			
			
}
public function ajaxsuperuserblock(){
  
			$id 		=	$this->input->post('id');
			$block		=	$this->input->post('is_block') == '1' ? '0':'1';
			$this->user_model->UpdateDetails('member_master',array('supertestuser'=>$block),array('member_id'=>$id));					
				if($block=='Y')
				{						
					$this->session->set_flashdata('message', "  User has move to Super Test User");
				}
				else{
					$this->session->set_flashdata('message', "  Super Test user has move to User");
				}
			$this->session->set_flashdata('class', "success");
			
			
}



public function edit($member_id){	
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$member_id 		 = $member_id?$member_id:$this->input->post('member_id');
			$data			 = array('first_name'=>trim($this->input->post('first_name')),
									 'last_name'=>trim($this->input->post('last_name')));	
			$this->user_model->UpdateDetails('fk_member_master',$data,array('member_id'=>$member_id));
		    $this->session->set_flashdata('message', 'Customer Details successfully updated.!' ,'SUCCESS');	
		    redirect($this->user->root.'/customers/lists');
		}
	    

} 	
public function pagination(){
		
			$config['page_query_string'] = TRUE;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";			
			
			return $config;		
		
		
}
public function delete($member_id){
	     //$member_id	    = $member_id?$member_id:($this->input->get('member_id')?$this->input->get('member_id'):0);	
           $this->user_model->bulkDelete($member_id);
		  $this->session->set_flashdata('message', 'User Details successfully deleted.!' ,'SUCCESS');	
		   redirect($this->user->root.'/user_list/lists?limit='.$_REQUEST['limit'].'& per_page='.$_REQUEST['per_page'].'& key='.$_REQUEST['key']);
} 		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */