<?php

//echo "<pre>";
//print_r($response[0]['locu_data']->locu_extend);
//exit;
?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Replimatic | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Replimatic | Home">
        <meta name="author" content="NewageSMB">
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style-api.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />

        <link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>


        <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap-select.css">
        <script src="<?= base_url() ?>js/jquery.min.js"></script>
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>js/bootstrap-select.js"></script>





        <!-- Margo JS  -->

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


        <style>
            .dropdown-menu li{ float:inherit !important;}
            .bootstrap-select .btn{ height:50px;}
            .bootstrap-select.form-control{ height:inherit !important;}
            .bootstrap-select .btn .caret{ color:#6FBE44 !important; border-left: 8px solid transparent !important;
                                           border-right: 8px solid transparent !important;
                                           border-top: 8px dashed !important;}
            .bs-donebutton{ display:none;}
        </style>

    </head>

    <body style="background:#f4f4f4 !important;">

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <!-- End Header ( Logo & Naviagtion ) -->
            </header>
            <!-- End Header -->

            <!-- Start Content -->

            <div class="containe">
            
            
            
            
            <script>
			del_goog
			</script>
            
            
            
            
            
            <?php if($reataurantdetails_res['google_id']!=''){?>
            
            
            
            
            
            

                <div class="col-lg-6 maininfo">
                
               <?php if($reataurantdetails_res['google_id']!=''){ ?>
                      <div class="form-group col-lg-12 ">
                         <div class="form-group col-lg-3 ">
                            
                            <button type="button" onClick="del_goog(<?php echo $reataurantdetails_res['restaurant_id'] ?>)"  class="btn btn-block btncontinue ">Back</button>
                        </div>
                        </div>
                        <?php } ?>
                        
                    <h3 class="hdlabel"><?= $reataurantdetails_res['restaurant_name'] ?></h3>
                    <div class="form-group itemlst">
                        <div class="col-sm-3 listitem">
                            <?php
                            $rating = round($response[0]['google_place_result']->rating);
                            if ($rating != '') {
                                $non_rate = 5 - $rating;
                                for ($i = 1; $i <= $rating; $i++) {
                                    ?>
                                    <li><img src="<?= base_url() ?>images/star.png"></li>
                                    <?php
                                }
                                for ($i = 1; $i <= $non_rate; $i++) {
                                    ?>
                                    <li><img src="<?= base_url() ?>images/star00.png"></li>
                                    <?php
                                }
                            }
                            ?>



                        </div>
                        <label class="col-sm-9">
                            (<span><?= $response[0]['google_place_result']->user_ratings_total ?></span>)
                        </label>
                    </div>
                    <div class="form-group itemlst">
                        <label class="col-sm-12 pad0">
                            <?= $reataurantdetails_res['formatted_address'] ?>
                        </label>
                    </div>
                    <div class="form-group itemlst">
                        <label class="col-sm-12 pad0">
                            <img src="<?= base_url() ?>images/phone.png"> <span>  <?= $reataurantdetails_res['contact_number'] ?></span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="form-group itemlst">
                        <label class="col-sm-3 pad0 list_tp">Price Rating</label>
                        <div class="col-sm-4 listitem">
                            <?php
                            $price_rating = round($response[0]['google_place_result']->price_level);
                            $p_rate = 4 - $price_rating;
                            if ($price_rating != '') {
                                for ($i = 1; $i <= $price_rating; $i++) {
                                    ?>
                                    <li><img src="<?= base_url() ?>images/dolor11.png"></li>
                                    <?php
                                }
                                for ($i = 1; $i <= $p_rate; $i++) {
                                    ?>
                                    <li><img src="<?= base_url() ?>images/dolor00.png"></li>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($response[0]['google_place_result']->website) { ?>
                            <div class="col-sm-4 listitem">
                                <a href=" <?= $response[0]['google_place_result']->website ?>" target="_blank">   <button class="btn btngray"><i><img src="<?= base_url() ?>images/globe.png"></i> VIEW WEBSITE</button></a>
                            </div><?php } ?>
                    </div>

                </div>



                <div class="col-lg-6 maininfo">
                    <div class="panel panel-default" style="border:none;">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title hoursbtn">
                                HOURS
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body hourswp">
                                <div class="">

                                    <?php
                                    foreach ($response[0]['google_place_result']->opening_hours->weekday_text as $weekday) {
//                                        echo $weekday;
                                        $week_array = explode(":", $weekday);
//                                        print_r($week_array);
                                        $si_arr = sizeof($week_array);
                                        for ($i = 1; $i < $si_arr; $i++) {
                                            $time.=$week_array[$i];
                                            if ($si_arr - $i > 1) {
                                                $time.=":";
                                            }
                                        }
                                        ?>  
                                        <div class="col-lg-4 col-sm-4 hourslst">
                                            <?= $week_array[0]; ?> : <br><span class="color_green"><?= $time; ?></span> 
                                        </div><?php
                                        unset($time);
                                    }
                                    ?>

                                    <div class="clearfix"></div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>




                <div class="clearfix"></div>

                <div class="col-lg-6 pading10">
                    <div class="slider">


                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top:0 !important;">
                            <!-- Indicators -->
                            <?php if (sizeof($response[0]['google_place_result']->photos) > 0) { ?>
                                <ol class="carousel-indicators">
                                    <?php
                                    $i = 0;
                                    foreach ($response[0]['google_place_result']->photos as $img) {
                                        ?>
                                        <li data-target="#carousel-example-generic" data-slide-to="<?= $i; ?>" <?php if ($i == 0) { ?> class="active" <?php
                                        }$i++;
                                        ?>></li>
                                        <?php } ?>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <?php
                                    $i = 0;
                                    foreach ($response[0]['google_place_result']->photos as $img) {
                                        $image = $img->photo_reference;
                                        ?>
                                        <div class="item <?php if ($i == 0) { ?> active <?php
                                            $i++;
                                        }
                                        ?>">
                                            <img style="max-height: 300px; min-height: 300px; width: 100%" id='image_item' src='https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&maxheight=500&photoreference=<?= $image ?>&key=<?= $key1 ?>'  >  


                                        </div><?php } ?>

                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            <?php } else { ?>



                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators"></ol>
                                    <div class="carousel-inner" role="listbox">
                                        <img style="max-height: 300px; min-height: 300px; width: 100%" id='image_item' src='<?= base_url() ?>images/no-image-slider.png'  >
                                    </div>
                                </div>

                            <?php } ?>
                        </div>


                    </div>
                </div>

                <div class="col-lg-6 pading10">
                    <div id="map_canvas"  style="max-height: 300px; min-height: 300px; width: 100%" class="col-lg-6">
                        <script>
                            function initialize() {
                                var map_canvas = document.getElementById('map_canvas');
                                var map_options = {
                                    center: new google.maps.LatLng(<?= $response[0]['google_place_result']->geometry->location->lat ?>, <?= $response[0]['google_place_result']->geometry->location->lng ?>),
                                    zoom: 18,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                                var mapPin = " http://www.google.com/mapfiles/marker.png";
                                var map = new google.maps.Map(map_canvas, map_options)
                                var Marker = new google.maps.Marker({
                                    map: map,
                                    position: map.getCenter()
                                });
                            }
                            google.maps.event.addDomListener(window, 'load', initialize);
                        </script>
                    </div>
                </div>
                
                
                <?php } else {?>
                
                
                
                
                  <form action="" method="post">
                
                 <div class="col-lg-12 maininfo">

                    <h3 class="hdlabel"><?= $reataurantdetails_res['restaurant_name'] ?></h3>
                    <div class="form-group itemlst">
                        
                        
                    </div>
                    <div class="form-group itemlst">
                        <label class="col-sm-12 pad0">
                            <?=$reataurantdetails_res['formatted_address'] ?>
                        </label>
                    </div>
                    <div class="form-group itemlst">
                        <label class="col-sm-12 pad0">
                            <img src="<?= base_url() ?>images/phone.png"> <span>  <?=$reataurantdetails_res['contact_number'] ?></span>
                        </label>
                    </div>
                    <div class="form-group  col-lg-6">
                            <label  class="labelitem">RESTAURANT NAME :</label>
                                <input type="text" name="restaurant_name" placeholder="restaurant name" id="restaurant_name" value="<?= $reataurantdetails_res['restaurant_name'] ?>" class="form-control inputsignup" >
                        </div>
                        
                        <div class="form-group  col-lg-6">
                            <label  class="labelitem">ADDRESS :</label>
                                <input type="text" name="formatted_address" placeholder="formatted address" value="<?=$reataurantdetails_res['formatted_address'] ?>" id="formatted_address" class="form-control inputsignup" >
                        </div>
                        <div class="form-group  col-lg-6">
                            <label  class="labelitem">CONTACT NUMBER :</label>
                                <input type="text" name="contact_number" placeholder="contact number" id="contact_number" value="<?=$reataurantdetails_res['contact_number'] ?>" class="form-control inputsignup" >
                        </div>
                        
                        
                        
                        
                        
                        
                    	
                    <div class="form-group  col-lg-6">
                            <label  class="labelitem">GOOGLE ID :</label>
                                <input type="text" name="google_id" placeholder="google id" id="google_id" value="<?php echo $reataurantdetails_res['google_id'] ?>" class="form-control inputsignup" >
                        </div>
                        <input type="hidden" name="sub_val" value="Y" >
                          <div class="form-group col-lg-12 ">
                         <div class="form-group col-lg-3 pull-right pad0 ">
                            
                            <button type="submit"  class="btn btn-block btncontinue ">Find</button>
                        </div>
                        </div>
                        <div class="clearfix"></div>

                </div>
                 

                </div>
                
                
                
                 
                
            
                
                
                <?php } ?>
                
                
                
                
                
                
                
                
                
                
                <div class="clearfix"></div>
 <?php if($reataurantdetails_res['google_id']!=''){?>
                <form action="" method="post">
                <?php } ?>
                    <div class="listwp">
                        <h4 class="labelhead"><i><img src="<?= base_url() ?>images/contact-ic.png" style="vertical-align:top;"> </i> CONTACT</h4>

                        
                         <div class="form-group col-lg-6">
                            <label  class="labelitem">EMAIL :</label>
                            <div class="col-sm-12 listitem">
                             <?php if($reataurantdetails_res['google_id']!=''){?>
                                <input type="hidden" name="sub_val" value="N" >
                                <input type="hidden" name="google_id" value="<?= $reataurantdetails_res['google_id']; ?>">
                                <?php  } ?>
                                <input type="hidden" name="locu_id" value="<?= $reataurantdetails_res['locu_id']; ?>">
                                <input type="hidden" name="restaurant_id" value="<?= $reataurantdetails_res['restaurant_id']; ?>">

                                <input type="email" placeholder="email" name="email" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['email']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem">FAX :</label>
                            <div class="col-sm-12 listitem">
                                <input type="text" name="FAX" placeholder="FAX" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['FAX']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem">business owner :</label>
                            <div class="col-sm-12 listitem">
                                <input type="text" name="business owner"  placeholder="business owner" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['business_owner']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem">CUISINES :</label>
                            <div class="col-sm-12 listitem">
                                <select name="cuisine[]" class="form-control" multiple>

                                    <?php foreach ($cuisines as $cuis) { ?>
                                        <option value="<?= $cuis['cusine_id'] ?>"   
                                                <?php if (in_array($cuis['cusine_id'], $reataurantdetails)) { ?> selected
                                                <?php } ?>  > <?= $cuis['cusine_name'] ?></option>
                                            <?php } ?>
                                </select>

                            </div>
                        </div>    <div class="clearfix"></div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem">Bank name :</label>
                            <div class="col-sm-12 listitem">
                                <input type="text" name="bank_name" placeholder="bank name" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['bank_name']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem">Routing Number :</label>
                            <div class="col-sm-12 listitem">
                                <input type="text" name="RoutingNumber" placeholder="Routing Number" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['RoutingNumber']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem">Account Number :</label>
                            <div class="col-sm-12 listitem">
                                <input type="text" name="AccountNumber" placeholder="Account Number" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['AccountNumber']; ?>">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>


                    <div class="listwp">
                        <h4 class="labelhead"><i><img src="<?= base_url() ?>images/delivery-ic.png"> </i> DELIVERY</h4>
                        <?php
                        $locu = $response[0]['locu_data'];
                        $extend = $response[0]['locu_data']->locu_extend->venues[0];
                        ?>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem col-sm-5">DELIVERY :</label>
                            <div class="col-sm-7 listitem">
                                <div class="radio">
                                    <div class="col-xs-4 pad0">
                                        <input id="scttn" type="radio" <?php if ($reataurantdetails_res['delivery'] == 'yes') { ?>  checked="checked"<?php } ?> name="delivery" value="yes">
                                        <label for="scttn">YES</label>
                                    </div>
                                    <div class="col-xs-4 pad0">
                                        <input id="scttn1" type="radio" <?php if ($reataurantdetails_res['delivery'] == 'no') { ?>  checked="checked"<?php } ?>  name="delivery" value="no">
                                        <label for="scttn1">NO</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem col-sm-5" style="padding-top:12px;">Minimum Order :</label>
                            <div class="col-sm-7 listitem">
                                <input type="text" name="minimum_order" placeholder="$"  id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['minimum_order']; ?>">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem col-xs-5" style="padding-top:12px;">areas :</label>
                            <div class="col-xs-4 listitem">
                                <input type="text" name="areas_delivery" placeholder="miles" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['areas_delivery']; ?>">
                            </div>
                            <label  class="labelitem col-xs-3" style="padding-top:12px; padding-left:10px;">RADIUS</label>
                        </div>

                        <div class="form-group col-lg-6">
                            <label  class="labelitem col-sm-5" style="padding-top:12px;">Delivery Fee :</label>
                            <div class="col-sm-7 listitem">
                                <input type="text" name="delivery_fee" placeholder="$" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['delivery_fee']; ?>">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <?php if ($genaral_config['value'] != 'N') { ?>
                        <div class="listwp">
                            <h4 class="labelhead"><i><img src="<?= base_url() ?>images/features-ic.png" style="vertical-align:top;"> </i> FEATURES</h4>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">alcohol  :</label>
                                <div class="col-sm-12 listitem">
                                    <select name="Alcohol" class="form-control">
                                        <option <?php if ($reataurantdetails_res['alcohol'] == "") { ?> selected <?php } ?>> Select one</option>
                                        <option <?php if ($reataurantdetails_res['alcohol'] == "beer_and_wine") { ?> selected <?php } ?>>beer and wine</option>
                                        <option <?php if ($reataurantdetails_res['alcohol'] == "full_bar") { ?> selected <?php } ?>>full bar</option>
                                        <option <?php if ($reataurantdetails_res['alcohol'] == "no_alcohol") { ?> selected <?php } ?>>no alcohol</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">parking :</label>
                                <div class="col-sm-12 listitem">
                                    <select name="parking" class="form-control">
                                        <option value="">Select one</option>
                                        <?php if ($reataurantdetails_res['parking'] == "garage") { ?>  
                                            <option selected>garage</option>
                                            <option>lot</option>
                                            <option>street</option>
                                            <option>valet</option>
                                            <option>validated</option>
                                        <?php } elseif ($reataurantdetails_res['parking'] == "lot") { ?>  
                                            <option>garage</option>
                                            <option selected>lot</option>
                                            <option>street</option>
                                            <option>valet</option>
                                            <option>validated</option>
                                        <?php } elseif ($reataurantdetails_res['parking'] == "valet") { ?>  
                                            <option>garage</option>
                                            <option>lot</option>
                                            <option>street</option>
                                            <option selected>valet</option>
                                            <option>validated</option>
                                        <?php } elseif ($reataurantdetails_res['parking'] == "validated") { ?>  
                                            <option>garage</option>
                                            <option>lot</option>
                                            <option>street</option>
                                            <option>valet</option>
                                            <option selected>validated</option>
                                        <?php } elseif ($reataurantdetails_res['parking'] == "lot") { ?>  
                                            <option>garage</option>
                                            <option selected>lot</option>
                                            <option>street</option>
                                            <option>valet</option>
                                            <option>validated</option>
                                        <?php } else { ?><option>garage</option>
                                            <option >lot</option>
                                            <option>street</option>
                                            <option>valet</option>
                                            <option>validated</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">WIFI  :</label>
                                <div class="col-sm-12 listitem">
                                    <select name="wifi" class="form-control">
                                        <option value="">Select one</option>
                                        <?php if ($reataurantdetails_res['wifi'] == "free") { ?>
                                            <option selected>free</option>
                                            <option>paid</option>
                                            <option>no</option>
                                        <?php } else if ($reataurantdetails_res['wifi'] == "paid") { ?>
                                            <option>free</option>
                                            <option selected>paid</option>
                                            <option>no</option>
                                        <?php } elseif ($reataurantdetails_res['wifi'] == "no") { ?>
                                            <option>free</option>
                                            <option>paid</option>
                                            <option selected>no</option>
                                        <?php } else { ?> <option>free</option>
                                            <option>paid</option>
                                            <option >no</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">corkage  :</label>
                                <div class="col-sm-12 listitem">
                                    <select name="corkage" class="form-control ">
                                        <option value="">Select one</option>
                                        <?php if ($reataurantdetails_res['corkage'] == "free") { ?>
                                            <option selected>free</option>
                                            <option>paid</option>
                                            <option>no</option>
                                        <?php } elseif ($reataurantdetails_res['corkage'] == "paid") { ?>
                                            <option>free</option>
                                            <option selected>paid</option>
                                            <option>no</option>
                                        <?php } elseif ($reataurantdetails_res['corkage'] == "no") { ?>
                                            <option>free</option>
                                            <option>paid</option>
                                            <option selected>no</option>
                                        <?php } else { ?>
                                            <option>free</option>
                                            <option>paid</option>
                                            <option >no</option> 
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">dietary restrictions  :</label>
                                <div class="col-sm-12 listitem">
                                    <div class="styled" style="max-width:100%;">
                                        <select name="dietary_restrictions" class="form-control">
                                            <option value="">Select one</option>
                                            <option <?php if ($reataurantdetails_res['dietary_restrictions'] == "vegetarian") {
                                            ?> selected <?php } ?>>vegetarian</option>
                                            <option <?php if ($reataurantdetails_res['dietary_restrictions'] == "vegan") {
                                            ?> selected <?php } ?>>vegan</option>
                                            <option  <?php if ($reataurantdetails_res['dietary_restrictions'] == "kosher") {
                                            ?> selected <?php } ?>>kosher</option>
                                            <option  <?php if ($reataurantdetails_res['dietary_restrictions'] == "halal") {
                                            ?> selected <?php } ?>>halal</option>
                                            <option   <?php if ($reataurantdetails_res['dietary_restrictions'] == "dairy-free") {
                                            ?> selected <?php } ?>>dairy free</option>
                                            <option  <?php if ($reataurantdetails_res['dietary_restrictions'] == "gluten-free") {
                                            ?> selected <?php } ?>>gluten free</option>
                                            <option <?php if ($reataurantdetails_res['dietary_restrictions'] == "soy-free") {
                                            ?> selected <?php } ?>>soy free</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">MUSIC  :</label>
                                <div class="col-sm-12 listitem">
                                    <select name="music" class="form-control">
                                        <option value="">Select one</option>
                                        <?php if ($reataurantdetails_res['music'] == "dj") { ?>
                                            <option selected>dj</option>
                                            <option>live</option>
                                            <option>jukebox</option>
                                            <option>background music</option>
                                            <option>karaoke</option>
                                            <option>no music</option>
                                        <?php } else if ($reataurantdetails_res['music'] == "live") { ?>
                                            <option>dj</option>
                                            <option selected>live</option>
                                            <option>jukebox</option>
                                            <option>background music</option>
                                            <option>karaoke</option>
                                            <option>no music</option>
                                        <?php } else if ($reataurantdetails_res['music'] == "jukebox") { ?>
                                            <option>dj</option>
                                            <option>live</option>
                                            <option selected>jukebox</option>
                                            <option>background music</option>
                                            <option>karaoke</option>
                                            <option>no music</option>
                                        <?php } else if ($reataurantdetails_res['music'] == "background_music") { ?>
                                            <option>dj</option>
                                            <option>live</option>
                                            <option>jukebox</option>
                                            <option selected>background music</option>
                                            <option>karaoke</option>
                                            <option>no music</option>
                                        <?php } else if ($reataurantdetails_res['music'] == "karaoke") { ?>
                                            <option>dj</option>
                                            <option>live</option>
                                            <option>jukebox</option>
                                            <option>background music</option>
                                            <option selected>karaoke</option>
                                            <option>no music</option>
                                        <?php } elseif ($reataurantdetails_res['music'] == "no_music") { ?>
                                            <option>dj</option>
                                            <option>live</option>
                                            <option>jukebox</option>
                                            <option>background music</option>
                                            <option>karaoke</option>
                                            <option selected>no music</option>
                                        <?php } else { ?>
                                            <option>dj</option>
                                            <option>live</option>
                                            <option>jukebox</option>
                                            <option>background music</option>
                                            <option>karaoke</option>
                                            <option >no music</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">SPORTS  :</label>
                                <div class="col-sm-12 listitem">
                                    <select name="sports" class="form-control">
                                        <option value="">Select one</option>
                                        <option <?php
                                        if ($reataurantdetails_res['sports'] == "soccer") {
                                            $sp = 1;
                                            ?> selected <?php } ?>>soccer</option>
                                        <option <?php
                                    if ($reataurantdetails_res['sports'] == "basketball") {
                                        $sp = 1;
                                            ?> selected <?php } ?>>basketball</option>
                                        <option <?php
                                    if ($reataurantdetails_res['sports'] == "hockey") {
                                        $sp = 1;
                                            ?> selected <?php } ?>>hockey</option>
                                        <option <?php
                                    if ($reataurantdetails_res['sports'] == "football") {
                                        $sp = 1;
                                            ?> selected <?php } ?>>football</option>
                                        <option <?php
                                    if ($reataurantdetails_res['sports'] == "baseball") {
                                        $sp = 1;
                                            ?> selected <?php } ?>>baseball</option>
                                        <option <?php
                                    if ($reataurantdetails_res['sports'] == "mma") {
                                            ?> selected <?php } ?>>mma</option>

                                        <option    <?php if ($reataurantdetails_res['sports'] == "other") { ?> selected  <?php } ?>>other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">wheelchair accessible :</label>
                                <div class="col-sm-12 listitem">
                                    <select class="form-control">
                                        <option value="">Select one</option>
                                        <option <?php if ($reataurantdetails_res['wheelchair_accessible'] == 'yes') { ?> selected <?php } ?>>YES</option>
                                        <option <?php if ($reataurantdetails_res['wheelchair_accessible'] == 'no') { ?> selected <?php } ?>>NO</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">ambience :</label>
                                <div class="col-sm-12 listitem">
                                    <input type="text" name="ambience" placeholder="ambience" id="exampleInputNname" class="form-control inputsignup" value="<?php echo $reataurantdetails_res['ambience']; ?>">
                                </div>
                            </div>

                            <div class="form-group col-lg-3">
                                <label  class="labelitem">outdoor seating :</label>
                                <div class="col-sm-12 listitem" style="padding-top:20px;">
                                    <div class="radio">
                                        <div class="col-xs-6 pad0">
                                            <input id="scttn2" type="radio" name="outdoor_seating"  <?php if ($reataurantdetails_res['outdoor_seating'] == 'yes') { ?>checked="checked" <?php } ?> value="yes">
                                            <label for="scttn2">YES</label>
                                        </div>
                                        <div class="col-xs-6 pad0">
                                            <input id="scttn3" type="radio" name="outdoor_seating" <?php if ($reataurantdetails_res['outdoor_seating'] == 'no') { ?>checked="checked" <?php } ?>  value="no">
                                            <label for="scttn3">NO</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-3">
                                <label  class="labelitem">waiter service  :</label>
                                <div class="col-sm-12 listitem" style="padding-top:20px;">
                                    <div class="radio">
                                        <div class="col-xs-6 pad0">
                                            <input id="scttn4" type="radio" <?php if ($reataurantdetails_res['waiter_service'] == 'yes') { ?> checked="checked" <?php } ?> name="waiter_service" value="yes">
                                            <label for="scttn4">YES</label>
                                        </div>
                                        <div class="col-xs-6 pad0">
                                            <input id="scttn5" type="radio" <?php if ($reataurantdetails_res['waiter_service'] == 'no') { ?> checked="checked" <?php } ?> name="waiter_service" value="no">
                                            <label for="scttn5">NO</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">SMOKING  :</label>
                                <div class="col-sm-12 listitem" style="padding-top:20px;">
                                    <div class="radio">
                                        <div class="col-xs-3 pad0">
                                            <input id="scttn6" type="radio"  <?php if ($reataurantdetails_res['smoking'] == 'yes') { ?> checked="checked" <?php } ?> name="smoking" value="yes">
                                            <label for="scttn6">YES</label>
                                        </div>
                                        <div class="col-xs-3 pad0">
                                            <input id="scttn7" type="radio" <?php if ($reataurantdetails_res['smoking'] == 'no') { ?> checked="checked" <?php } ?>  name="smoking" value="no">
                                            <label for="scttn7">NO</label>
                                        </div>
                                        <div class="col-xs-6 pad0">
                                            <input id="scttn8" type="radio" <?php if ($reataurantdetails_res['smoking'] == 'outdoor') { ?> checked="checked" <?php } ?> name="smoking" value="outdoor">
                                            <label for="scttn8">OUTDOOR</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-6">
                                <label  class="labelitem">television  :</label>
                                <div class="col-sm-12 listitem" style="padding-top:20px;">
                                    <div class="radio">
                                        <div class="col-xs-3 pad0">
                                            <input id="scttn9" type="radio" <?php if ($reataurantdetails_res['television'] == 'yes') { ?>  checked="checked" <?php } ?> name="television" value="yes">
                                            <label for="scttn9">YES</label>
                                        </div>
                                        <div class="col-xs-3 pad0">
                                            <input id="scttn10" type="radio" <?php if ($reataurantdetails_res['television'] == 'no') { ?>  checked="checked" <?php } ?>  name="television" value="no">
                                            <label for="scttn10">NO</label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="clearfix"></div>
                        </div>
                    <?php } ?>


                    <div class="listwp">
                        <h4 class="labelhead"><i><img style="vertical-align:top;" src="<?php echo base_url(); ?>images/contact-ic.png"> </i> SALES PERSON</h4>
                        <div class="form-group col-lg-6">

                            <div class="form-group col-lg-10">
                                <label style="padding-top:12px;" class="labelitem col-sm-5">Sales Person:</label>
                                <div class="col-sm-7 listitem">
                                    <input type="text" class="form-control inputsignup" id="exampleInputNname" placeholder="Sales Person" name="sales_person" value="<?php echo $reataurantdetails_res['sales_person']; ?>">
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                 <!--   <div class="col-lg-12 ">

                        <div class="col-lg-4 "> <input type="text" class="form-control inputsignup" id=""></div>
                        <div class="col-lg-4 "> <input type="text" class="form-control inputsignup" id=""></div>
                        <div class="col-lg-3 "><input type="button" onClick="test();" value="test"></div>
                    </div>
-->
                    <div class="col-lg-6 ">

                        <button type="submit" id="lst_bt" class="btn btn-block btncontinue">SAVE</button>
                    </div>
                     <div class="col-lg-6 ">
                   <?php if($reataurantdetails_res['status']=='N'){?>
                        <button type="button" id="lst_bt" class="btn btn-block btncontinue" onClick="approve_disable2(<?php echo  $reataurantdetails_res['restaurant_id'];?>)">Approve</button>
                        <?php } else{ ?>
                        <button type="button" id="lst_bt" class="btn btn-block btncontinue" style="background-color:#ef6650"  onClick="approve_disable1(<?php echo  $reataurantdetails_res['restaurant_id'];?>)">Disable</button>
                        <?php } ?>
                    </div>

                </form>


            </div>
        </div>
        <!-- End content -->


        <!-- Start Footer -->

        <!-- End Footer -->

    </div>
    <!-- End Container -->

    <!-- Go To Top Link -->


    <div class="modal fade bs-example-modal-sm col-lg-12" id="exceeds" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-sm">

            <div class="modal-content">

                <div class="modal-header">

                    <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>

                    <center> <h4 id="myModalLabel" class="modal-title"><span id="ex_head"></span></h4></center>

                </div>

                <div class="modal-body lsting">



                    <table id="ex_data"><td>sdufyds</td><td>hdffrf</td><td>dysftudf</td><td>djyfiuydi</td></table>





                    <div class="clearfix"></div>

                </div>



            </div>

        </div>

    </div>

    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
    <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
    <script>
	

	            function approve_disable1(restaurant_id)
                            {
								
								$.ajax({
										type : "POST",
										url  : "<?php echo site_url('admin/restaurant_list/approve_disable'); ?>",
										data : {id:restaurant_id,is_block: 'Y'}, 
										cache : false,
										success : function(res) {
											 window.location.reload();
											}
									 });  
							}
	           function approve_disable2(restaurant_id)
                            {
								
								
								$.ajax({
										type : "POST",
										url  : "<?php echo site_url('admin/restaurant_list/approve_disable'); ?>",
										data : {id:restaurant_id,is_block: 'N'}, 
										cache : false,
										success : function(res) {
											 window.location.reload();
											}
									 });  
							}
							 function del_goog(restaurant_id)
                            {
								alert('haiii');
								
								$.ajax({
										type : "POST",
										url  : "<?php echo site_url('admin/restaurant_list/del_goog'); ?>",
										data : {id:restaurant_id}, 
										cache : false,
										success : function(res) {
											 window.location.reload();
											}
									 });  
							}
							
							
						
	
                            function test()
                            {

//alert("tst");
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo base_url(); ?>admin/restaurant_list/test1',
                                    dataType: 'html',
                                    data: {"dfgkf": 'dsmhf',
                                    },
                                    success: function (data) {

                                        alert(data)

                                        console.log(data);
                                        //$('#dialogDiv').html(data)	
                                        $("#ex_data").append($('<table>').html(data));


                                        $('#exceeds').modal('show');
                                        // return false;


                                    },
                                });


                            }
                            $(document).ready(function () {
                                var mySelect = $('#first-disabled2');

                                $('#special').on('click', function () {
                                    mySelect.find('option:selected').prop('disabled', true);
                                    mySelect.selectpicker('refresh');
                                });

                                $('#special2').on('click', function () {
                                    mySelect.find('option:disabled').prop('disabled', false);
                                    mySelect.selectpicker('refresh');
                                });

                                $('#basic2').selectpicker({
                                    liveSearch: true,
                                    maxOptions: 1
                                });
                            });
    </script>

</body>

</html>