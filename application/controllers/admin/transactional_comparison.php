<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
class Transactional_comparison extends MY_Controller {

	/**
	 * Location Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 * $this->load->library('crud');
	 * load crud library if u want to perform basic listing/add/edit and
	 * other similar stuffs
	 * $this->load->library('preferences');
	 * for showing a configuration table where users can only update fields
	 */	
public function __construct(){
	
				parent::__construct();
				$this->_setAsAdmin();
				$this->load->model('admin/transactional_comparison_model');
				$this->user 	= $this->session->userdata('user');
				if($this->user=='')
					redirect('admin');		
}
public function index(){
	redirect('admin/transactional_comparison/lists');		
	
}
public function add($tid){


			if($_SERVER['REQUEST_METHOD']=='POST'){
	if($_POST['tc_id'])
	{
	$tc=array(tc_id=>$_POST['tc_id']);
	      $update_id = $this->transactional_comparison_model->update_by($tc,$_POST);
		  $this->session->set_flashdata('message', 'Details Updated Successfully','SUCCESS');
	}
    else{
		$update_id = $this->transactional_comparison_model->adddetails('transactional_comparison',$_POST);
		$this->session->set_flashdata('message', 'Details Added Successfully','SUCCESS');
	}
					    redirect('admin/transactional_comparison/lists');
				
			}else{
			
				$page						      =	($_REQUEST['pageno'])?$_REQUEST['pageno']:'';
				if($tid)
				{
				
				$data['get_transactional_comparison']   = $this->transactional_comparison_model->get_transactional_comparison($tid);
				}
				
				$output['output']                 = $this->load->view('admin/transactional_comparison/add',$data,true);//loading success view
				$this->_render_output($output);
			}
			
}

public function lists(){

             //for pagination
			$config					= array();
			$config					= $this->pagination();
			$this->load->library('pagination');
			$data['total_rows']= getConfigValue('default_pagination');
			//$data['total_rows'] 	= 2;

			$_REQUEST['limit'] 		= (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);
			$_REQUEST['key'] 		= (!$_POST['key'] ? ($_GET['key'] ? $_GET['key'] :''):$_POST['key']);
			$_REQUEST['status'] 		= (!$_POST['status'] ? ($_GET['status'] ? $_GET['status'] :''):$_POST['status']);
			$params = '?t=1';
				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];
				if($_REQUEST['key']) $params .= '&key='.$_REQUEST['key'];
				if($_REQUEST['status']) $params .= '&status='.$_REQUEST['status'];
			$config['base_url'] 	= site_url($this->user->root."/transactional_comparison/lists")."/".$params;
            $config['total_rows']	= $this->transactional_comparison_model->getPromoCount($_REQUEST['key']);
		    $config['per_page']   	= $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
		    $data['page'] 			= $_REQUEST['per_page'];
		    $data['limit'] 			= $_REQUEST['limit'];
			$data['key'] 			= $_REQUEST['key'];
			$data['status'] 		= $_REQUEST['status'];
			
		    $this->pagination->initialize($config);	

		    $data['promolist']   = $this->transactional_comparison_model->getAllPromocodes($_REQUEST['status'],$_REQUEST['key'],$config['per_page'],$_REQUEST['per_page']);
			
		//----------------------------------------------------------
	   //echo "here";exit;
       $output['SUB_TITLE'] = 'promocodes List';               
       $output['output']=$this->load->view('admin/transactional_comparison/lists',$data, true);
	   $this->_render_output($output);
}	
	

public function bulkAction($bulkaction_list='',$location_id){	
	
			$bulkaction =  $this->input->post('bulkaction');
			$location_id=$this->uri->segment(5);
			$location_id = $this->input->post('sel')?$this->input->post('sel'):$location_id;
			
		if($bulkaction=='')
			$bulkaction	=	'delete';
			
		if($bulkaction){
			if($location_id){
				switch($bulkaction){
					case 'delete':
					
						$delete_id = $this->location_model->bulkDelete($location_id);					
						$this->session->set_flashdata('message', 'Restaurant(s) Successfully Deleted ');
						
						break;
					case 'inactive':
						$update_id = $this->restaurant_model->bulkUpdate(array('restaurant_id'=>$res_id), array('status'=>'N'));	
							//echo "<pre>";  print_r(COUNT($owner_id)); echo "</pre>"; exit;
							if((COUNT($location_id)) == 1)
								$msg = 'User details updated successfully' ;
							else
								$msg = COUNT($location_id).' Restaurant details Successfully Updated.!' ;
							$this->session->set_flashdata('message', $msg ,'SUCCESS');	
						break;
					case 'active':
						$update_id = $this->restaurant_model->bulkUpdate(array('restaurant_id'=>$res_id), array('status'=>'Y'));						
						
							if(COUNT($location_id) == 1)
								$msg = 'User details updated successfully' ;
							else
								$msg = COUNT($location_id).' Restaurant details Successfully Updated.!' ;
							$this->session->set_flashdata('message', $msg ,'SUCCESS');	
						
						break;
				}
			  }  
			 else{
				$this->session->set_flashdata('message', 'Please select at least one member.! ','ERROR');	
			  }
		}
		redirect('admin/location/lists');
}



public function pagination(){
		
			$config['page_query_string'] = TRUE;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";			
			
			return $config;		
		
		
}

public function generatePromo(){
	
		   $this->load->helper('string');
		   $promo= random_string('alpha',8);              
		   echo $promo;
	          
}
public function check_exist(){
	            
       	 $promocode=$_POST['promocode'];
		 $restaurant_id=$_POST['restaurant_id'];
		 echo $promo_code = $this->transactional_comparison_model->checkExist($promocode,$restaurant_id);			
	          
}
}

	
?>
