<?php


 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//error_reporting(E_ALL);


class Orders extends My_Controller {





	public function __construct() 

	{                        

		parent::__construct();

		$this->_setAsAdmin();

		$this->load->helper(array('url','cookie'));	

		$this->load->model('admin/order_model');

		$this->user 	= $this->session->userdata('user');

		if($this->user=='')

			redirect('admin');					

	}



	public function index()

	{

		redirect($this->user->root.'/orders/lists');

	}

	public function lists($category_id=''){

	

		$this->load->model('restaurant_model');		

        $role=$this->session->userdata('user')->role;

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

            $role=$user->role;

			$data['allcounts']=$this->order_model->AllOrdersCount($restaurant_id,$location_id,$role);

			$config['total_rows'] = $this->order_model->countNewOrders($restaurant_id,$location_id,$role);

			$config['per_page'] = $config['total_rows'];

			 $data['key']  			= $_REQUEST['key'];

			 if( $role==1){

				 $restaurant_id=$_POST['rest'];

				 $data['restaurantlist']=$this->restaurant_model->getRestaurant();

				 $data['orderdetails']	=$this->order_model->getOrderDetailsAdmin($_REQUEST['key'],$restaurant_id);

				 //echo '<pre>';print_r($data['orderdetails']);exit;		

				 $ouput['output']			= $this->load->view('admin/orders/lists',$data,true);

				 $this->_render_output($ouput);

			 }

			else{

				$data['orderdetails']	=$this->order_model->getOrderDetails($restaurant_id,$location_id,$config['per_page'],'',$role);

				//echo '<pre>';print_r($data['orderdetails']);exit;

				$this->order_model->setTable('fk_restaurant_locations');	

				$row = $this->order_model->get_by('location_id',$location_id);	

				//echo '<pre>';print_r($row);exit;

				if(count($data['orderdetails'])!=0){

					$i=0;

					foreach($data['orderdetails'] as $val){

						//echo $row->timezone;

						$created_time=$this->ConvertToViewTimezone($row->timezone,$val['created_time']);

						$data['orderdetails'][$i]['created_time']=$created_time;

						$i++;					

					}

				}

				//echo '<pre>';print_r($data['orderdetails']);exit;	

				//echo $startdate =  $this->ConvertToViewTimezone($row->timezone,$newdate);

				//exit;

				$ouput['output']			= $this->load->view('admin/orders/order-new',$data,true);

				$this->_render_output($ouput);

		    }

		}else{

			redirect('admin');

		}

	}
	public function admin_lists() {

       // $user = $this->session->userdata('user');
//        $data['restaurant_id'] = $user->restaurant_id;
//        $this->load->model('restaurant_model');
//        $role = $this->session->userdata('user')->role;
//        if ($this->session->userdata('user') != '') {
//            $user = $this->session->userdata('user');
//            $restaurant_id = 1;
//            $location_id = $user->restaurant_id;
//
            $role = 3;

            $data['allcounts'] = $this->order_model->admin_AllOrdersCount();
			
		
            $config['total_rows'] = $this->order_model->admin_countNewOrders();
			
			
		
			
            $config['per_page'] = $config['total_rows'];
            $data['key'] = $_REQUEST['key'];


            $data['orderdetails'] = $this->order_model->admin_getOrderDetails( $config['per_page']);
            //echo '<pre>';print_r($data['orderdetails']);exit;
            $this->order_model->setTable('restaurant_locations');
            $row = $this->order_model->get_by('location_id', $location_id);
            //echo '<pre>';print_r($row);exit;
            if (count($data['orderdetails']) != 0) {
                $i = 0;
                foreach ($data['orderdetails'] as $val) {
                    //echo $row->timezone;
                    $created_time = $this->ConvertToViewTimezone($row->timezone, $val['created_time']);
                    $data['orderdetails'][$i]['created_time'] = $created_time;
                    $i++;
                }
            }
            //echo '<pre>';print_r($data['orderdetails']);exit;	
            //echo $startdate =  $this->ConvertToViewTimezone($row->timezone,$newdate);
            //exit;

     $ouput['output']			= $this->load->view('admin/orders/admin_order-new',$data,true);

				$this->_render_output($ouput);


           
    }         
	public function accepted()

	{

		

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			

			$order_status=$_REQUEST['search_sel'];

			if($order_status=='')

				$order_status="Accepted";

				

			$data['allcounts']=$this->order_model->AllOrdersCount($restaurant_id,$location_id,$role);

			//for pagination

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			//$data['total_rows']= getConfigValue('default_pagination');

			$data['total_rows']= 2;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

				if($_REQUEST['search_sel']) $params .= '&search_sel='.$_REQUEST['search_sel'];

				

			$config['base_url'] = site_url($this->user->root."/orders/accepted")."/".$params;

			

			$config['total_rows'] = $this->order_model->countActiveOrders($restaurant_id,$location_id,$order_status,$role);

			

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

		   $data['limit'] 			= $_REQUEST['limit'];

			$this->pagination->initialize($config);		

			

			

			

			if($_SERVER['REQUEST_METHOD']=='POST'){

				

				$data['orderstatus']=$order_status;

				$data['orderdetails']	=	$this->order_model->searchOrder($restaurant_id,$location_id,$order_status,$config['per_page'],$_REQUEST['per_page'],$role);

				$ouput['output']			= $this->load->view('admin/orders/order-accepted',$data,true);

				$this->_render_output($ouput);

			}else{

				$data['orderdetails']	=$this->order_model->getActiveOrders($restaurant_id,$location_id,$order_status,$config['per_page'],$_REQUEST['per_page'],$role);

				//echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-accepted',$data,true);

				$this->_render_output($ouput);

			}

		}else{

			redirect('admin');

		}



	}

public function admin_accepted()

	{

		

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			

			$order_status=$_REQUEST['search_sel'];

			if($order_status=='')

				$order_status="Accepted";

				

			$data['allcounts']=$this->order_model->admin_AllOrdersCount();

			//for pagination

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			//$data['total_rows']= getConfigValue('default_pagination');

			$data['total_rows']= 2;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

				if($_REQUEST['search_sel']) $params .= '&search_sel='.$_REQUEST['search_sel'];

				

			$config['base_url'] = site_url($this->user->root."/orders/accepted")."/".$params;

			

			$config['total_rows'] = $this->order_model->admin_countActiveOrders();

			

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

		   $data['limit'] 			= $_REQUEST['limit'];

			$this->pagination->initialize($config);		

			

			

			

			if($_SERVER['REQUEST_METHOD']=='POST'){

				

				$data['orderstatus']=$order_status;

				$data['orderdetails']	=	$this->order_model->admin_searchOrder($restaurant_id,$location_id,$order_status,$config['per_page'],$_REQUEST['per_page'],$role);

				$ouput['output']			= $this->load->view('admin/orders/order-accepted',$data,true);

				$this->_render_output($ouput);

			}else{

				$data['orderdetails']	=$this->order_model->admin_getActiveOrders('Accepted',$config['per_page'],$_REQUEST['per_page'],$role);

		
		
		

			//	echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-accepted',$data,true);

				$this->_render_output($ouput);

			}

		}else{

			redirect('admin');

		}



	}












public function cancelled()

	{

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			$data['allcounts']=$this->order_model->AllOrdersCount($restaurant_id,$location_id,$role);

			

			//for pagination

			

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			$data['total_rows']= getConfigValue('default_pagination');

			//$data['total_rows']= 2;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

			$config['base_url'] = site_url($this->user->root."/orders/cancelled")."/".$params;

			$config['total_rows'] = $this->order_model->countCancelledOrders($restaurant_id,$location_id,$role);

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

			$data['limit'] 			= $_REQUEST['limit'];

			$this->pagination->initialize($config);	

			

				$data['orderdetails']	=$this->order_model->getCancelledOrders($restaurant_id,$location_id,$config['per_page'],$_REQUEST['per_page'],$role);

				//echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-cancelled',$data,true);

				$this->_render_output($ouput);

			

		}else{

			redirect('admin');

		}



	}	
	public function admin_cancelled()

	{

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			$data['allcounts']=$this->order_model->admin_AllOrdersCount();

			

			//for pagination

			

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			$data['total_rows']= getConfigValue('default_pagination');

			//$data['total_rows']= 2;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

			$config['base_url'] = site_url($this->user->root."/orders/cancelled")."/".$params;

			$config['total_rows'] = $this->order_model->admin_countCancelledOrders();

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

			$data['limit'] 			= $_REQUEST['limit'];

			$this->pagination->initialize($config);	

			

				$data['orderdetails']	=$this->order_model->admin_getCancelledOrders($restaurant_id,$location_id,$config['per_page'],$_REQUEST['per_page'],$role);

				//echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-cancelled',$data,true);

				$this->_render_output($ouput);

			

		}else{

			redirect('admin');

		}



	}	
//need to change the code.....

	public function late()

	{

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			$data['allcounts']=$this->order_model->AllOrdersCount($restaurant_id,$location_id,$role);

			

			//for pagination

			

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			$data['total_rows']= getConfigValue('default_pagination');

			//$data['total_rows']= 2;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

			$config['base_url'] = site_url($this->user->root."/orders/cancelled")."/".$params;

			$config['total_rows'] = $this->order_model->countLateOrders($restaurant_id,$location_id,$role);

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

			$this->pagination->initialize($config);	

			

				$data['orderdetails']	=$this->order_model->getLateOrders($restaurant_id,$location_id,$config['per_page'],$_REQUEST['per_page'],$role);

				//echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-late',$data,true);

				$this->_render_output($ouput);

			

		}else{

			redirect('admin');

		}



	}	


 public function admin_loadNewOrder() {
    
        $role = 3;
        $total_rows = $this->order_model->admin_countNewOrders($config['per_page']);
        $datetimeformat = getConfigValue('time_format') . ' ' . getConfigValue('date_format');
        $data['orderdetails'] = $this->order_model->admin_getOrderDetails();
        $url = base_url() . "owner/orders/details/";
        $fb_imgurl = base_url() . "assets/images/icon-fb.png";
        $general_imgurl = base_url() . "assets/images/icon-frkouse.png";
       header("Content-Type: text/event-stream");
       header("Cache-Control: no-cache");
       header("Connection: keep-alive");
        $newdata = "";
        $newtabdata = "";
        $newmobdata = "";
        if (count($data['orderdetails']) != 0) {
            foreach ($data['orderdetails'] as $order) {
                $newdata.="<tr id=" . $order['order_id'] . " class='trlink'>";
                $this->order_model->setTable('restaurant_locations');
                $row = $this->order_model->get_by('location_id', $location_id);
                $created_time = $order['created_time'];
                $delivery_time = $order['delivery_time'];
                $order['created_time'] = date('g:i a m/j/Y', strtotime($created_time));
                $order['delivery_time'] = date('g:i a m/j/Y', strtotime($delivery_time));
                $newdata.="<td style='cursor:pointer;' class='tdlink'>" . $order['first_name'] . ' ' . $order['last_name'] . "</td><td href='" . $url . $order['order_id'] . "' style='cursor:pointer;' class='tdlink hide-tab hide-mob'>" . $order['created_time'] . "</td><td href='" . $url . $order['order_id'] . "' style='cursor:pointer;' class='tdlink hide-tab hide-mob'>" . date($datetimeformat, strtotime($order['delivery_time'])) . "</td><td href='" . $url . $order['order_id'] . "' style='cursor:pointer;' class='tdlink hide-mob'>" . $order['order_ref_id'] . "</td><td href='" . $url . $order['order_id'] . "' style='cursor:pointer;' class='tdlink'>" . $order['order_type'] . "</td><td href='" . $url . $order['order_id'] . "' style='cursor:pointer;' class='tdlink hide-mob hide-tab '>$" . $order['total_amount'] . "</td>";
                $newdata.="<td  class='tdlink hide-desk hide'><a class='more-btn' href='javascript:void(0)'></a></td></tr>";
            }
        } else {
            $newdata.="<tr><td colspan='9' class='tbl_row'>No New Orders</td></tr>";
            $newtabdata.="<tr><td colspan='5' class='tbl_row'>No New Orders</td></tr>";
            $newmobdata.="<tr><td colspan='4' class='tbl_row'>No New Orders</td></tr>";
        }

        echo "data: $total_rows###$newdata\n\n";

        ob_flush();
        flush();
        //echo $this->load->view('admin/orders/stocks','',true);
    }



public function all()

	{

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			$data['allcounts']=$this->order_model->AllOrdersCount($restaurant_id,$location_id,$role);

			//for pagination

			

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			$data['total_rows']= getConfigValue('default_pagination');

			//$data['total_rows']= 10;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

			$config['base_url'] = site_url($this->user->root."/orders/all")."/".$params;

			$config['total_rows'] = $this->order_model->countAllOrders($restaurant_id,$location_id,$role);

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

			$data['limit'] 			= $_REQUEST['limit'];

			$this->pagination->initialize($config);	

			

				$data['orderdetails']	=$this->order_model->getAllOrders($restaurant_id,$location_id,$config['per_page'],$_REQUEST['per_page'],$role);

				//print_r($data['orderdetails']);exit;

				//echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-all',$data,true);

				$this->_render_output($ouput);

			

		}else{

			redirect('admin');

		}



	}	
public function admin_all()

	{

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			

			$data['allcounts']=$this->order_model->admin_AllOrdersCount();

			//for pagination

			

			$config	=array();

			$config=$this->pagination();

			$this->load->library('pagination');

			$data['total_rows']= getConfigValue('default_pagination');

			//$data['total_rows']= 10;

			

			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);

			$params = '?t=1';

				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];

			$config['base_url'] = site_url($this->user->root."/orders/admin_all")."/".$params;

			$config['total_rows'] = $this->order_model->countAllAdminOrders();

			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];

			$data['page'] = $_REQUEST['per_page'];

			$data['limit'] 			= $_REQUEST['limit'];

			$this->pagination->initialize($config);	

			

				$data['orderdetails']	=$this->order_model->getAllAdminOrders($config['per_page'],$_REQUEST['per_page']);

				//print_r($data['orderdetails']);exit;

				//echo '<pre>';print_r($data['orderdetails']);exit;		

				$ouput['output']			= $this->load->view('admin/orders/order-admin-all',$data,true);

				$this->_render_output($ouput);

			

		}else{

			redirect('admin');

		}



	}	


public function details($order_id){

		if($this->session->userdata('user')!=''){

			$user = $this->session->userdata('user');

			$restaurant_id=$user->restaurant_id;

			$location_id=$user->location_id;

			$role=$user->role;

			$data['usertype']=$user->root;

			$data['allcounts']=$this->order_model->admin_AllOrdersCount($restaurant_id,$location_id,$role);

			//echo '<pre>';print_r($data['allcounts']);exit;

			$data['orderdetails']=$this->order_model->getDetailOrder($order_id);

			$data['itemdetails']=$this->order_model->getDetailItem($order_id);
		
			//$data['optiondetails']=$this->order_model->getDetailOption($order_id);

			if(count($data['itemdetails']!='')){

					foreach($data['itemdetails'] as $var){

						$sidesdetails[$var['ord_item_id']]   =$this->order_model->getDetailOption($var['ord_item_id']);

						//echo '<pre>';print_r($sidesdetails);

					}

				}

			//echo '<pre>';print_r($data['itemdetails']);exit;	

			$data['sidesdetails']=$sidesdetails;

			$data['orderid']=$order_id;

			$ouput['output']	=	$this->load->view('admin/orders/order-details',$data,true);

			$this->_render_output($ouput);

		}else{

			redirect('admin');

		}

		

	}
	public function acceptOrder(){

		$order_id=$_POST['order_id'];

		$order	=	$this->order_model->acceptOrder($order_id);

		echo "success";

		exit;

	}
    public function cancelOrder(){

		$order_id=$_POST['order_id'];

		$resp = $this->refundOrderAmount($order_id);

		if($resp['status']=='success')

			$order	=	$this->order_model->cancelOrder($order_id);

		else

			echo "error";

		exit;

	}
	public function completeOrder(){

		$order_id=$_POST['order_id'];

		$order	=	$this->order_model->completeOrder($order_id);

	

		exit;

	}
    public function sublitcompleteOrder(){

		//echo '<pre>';print_r($_POST);exit;	

		$order_id=$_POST['order_id'];

		$order	=	$this->order_model->completeOrder($order_id);

		$this->session->set_flashdata('success_message', 'Order completed successfully');

		redirect($this->user->root.'/orders/lists');

	}
public function pagination(){

		

			$config['page_query_string'] = TRUE;

			$config['first_link'] = 'First';

			$config['last_link'] = 'Last';



			$config['full_tag_open'] = "<ul class='pagination'>";

			$config['full_tag_close'] ="</ul>";

			$config['num_tag_open'] = '<li>';

			$config['num_tag_close'] = '</li>';

			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";

			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";

			$config['next_tag_open'] = "<li>";

			$config['next_tagl_close'] = "</li>";

			$config['prev_tag_open'] = "<li>";

			$config['prev_tagl_close'] = "</li>";

			$config['first_tag_open'] = "<li>";

			$config['first_tagl_close'] = "</li>";

			$config['last_tag_open'] = "<li>";

			$config['last_tagl_close'] = "</li>";			

			

			return $config;		

		

		

	}
	public function loadNewOrder(){

		$user = $this->session->userdata('user');

		$restaurant_id=$user->restaurant_id;

		$location_id=$user->location_id;

        $role=$user->role;

		$total_rows = $this->order_model->countNewOrders($restaurant_id,$location_id,$role);

		$datetimeformat = getConfigValue('time_format').' '.getConfigValue('date_format');

		//$timeformat = getConfigValue('time_format');

		$data['orderdetails']	=$this->order_model->getOrderDetails($restaurant_id,$location_id,$total_rows,'',$role);

		//echo '<pre>';print_r($data['orderdetails']);exit;		

		$url=base_url().$this->user->root."/orders/details/";

		$fb_imgurl=base_url()."assets/images/icon-fb.png";

		$general_imgurl=base_url()."assets/images/icon-frkouse.png";

		header("Content-Type: text/event-stream");

		header("Cache-Control: no-cache");

		header("Connection: keep-alive");

		

		$newdata="";

		if(count($data['orderdetails'])!=0){

           	foreach($data['orderdetails'] as $order){

            $newdata.="<tr id='row_'".$order['order_id']."'>";

			

			if($this->user->role!='3'){	

			$newdata.="<td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>".$order['restaurant_name']."</td>";

			}

				

				$this->order_model->setTable('fk_restaurant_locations');	

				$row = $this->order_model->get_by('location_id',$location_id);	



				$created_time=$this->ConvertToViewTimezone($row->timezone,$order['created_time']);

				$order['created_time']=date('g:i a m/j/Y',strtotime($created_time));

						

				

				

			$newdata.="<td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>".$order['first_name'].' '.$order['last_name']."</td><td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink fontorange'>".$order['order_status']."</td><td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>".$order['created_time']."</td><td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>".date($datetimeformat,strtotime($order['delivery_time']))."</td><td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>".$order['order_ref_id']."</td><td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>".$order['order_type']."</td><td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>$".$order['total_amount']."</td>";

			if($this->user->role=='3'){	

			//$newdata.="<td><input type='button' class='btn btn-info accept' data-attr='".$order['order_id']."' value='ACCEPT'></td><td><input type='button' class='btn btn_gray cancel' data-attr='".$order['order_id']."' value='CANCEL'></td>";

			}

			$newdata.="<td href='".$url.$order['order_id']."' style='cursor:pointer;' class='tdlink'>";

			if($order['auth_type']=="both" || $order['auth_type']=="facebook"){

				$newdata.="<img src='".$fb_imgurl."'>";

			}else if($order['auth_type']=="general"){

				$newdata.="<img src='".$general_imgurl."'>";

			}

			

			$newdata.="</td></tr>";

		

			}

		}else {

      		 $newdata.="<tr><td colspan='9' class='tbl_row'>No new orders</td></tr>";

        } 

            

           echo "data: $total_rows###$newdata\n\n"; 

		 

		ob_flush();

		flush();

	//echo $this->load->view('admin/orders/stocks','',true);

			

	}

	public function loadAutoTimeUpdateDetail($order_id){

		$user = $this->session->userdata('user');

		$restaurant_id=$user->restaurant_id;

		$location_id=$user->location_id;

        $role=$user->role;

		

		

		$orderdetails=$this->order_model->getDetailOrder($order_id);

		



		header("Content-Type: text/event-stream");

		header("Cache-Control: no-cache");

	    header("Connection: keep-alive");

		$cur_date =strtotime(date('Y-m-d')); 

	    $deldate=explode(' ',$orderdetails[0]['delivery_time']);

		

		

				$newdata="";

				

				$retval ='';

                $granularity=1;

                $date = strtotime($orderdetails[0]['delivery_time']);

				///echo time();

				//$test=  strtotime(date('2015-09-17 12:59:00'));

                $difference = $date-  time();

                $periods = array('decade' => 315360000,

                'year' => 31536000,

                'month' => 2628000,

                'week' => 604800,

                'day' => 86400,

                'hour' => 3600,

                'minute' => 60,

                'second' => 1);

                

                if ($difference < 5) { // less than 5 seconds ago, let's say "just now"

                // $retval = "posted just now";

                $retval_time = "just now";

                

                } else {

					foreach ($periods as $key => $value_date) {

					if ($difference >= $value_date) {

					$time = floor($difference/$value_date);

					$difference %= $value_date;

					$retval .= ($retval ? ' ' : '').$time.' ';

					//$retval .= (($time > 1) ? $key.'s' : $key);

					$granularity--;

					}

					if ($granularity == '0') { break; }

                }

                // $retval= ' posted '.$retval.' ago';

				//echo $time;

                $retval_time=$retval;

				

                

                }

				

				

				if($key=='day'){

					

					if(date('H:i A',strtotime($deldate[1]))=='00:00 AM'){

						$newdata.= '12:00 AM '.date('m-d-Y',strtotime($orderdetails[0]['delivery_time']));

						//$newdata.=date('H:i A m-d-Y',strtotime($orderdetails[0]['delivery_time']));

					}else{

						$newdata.=date('g:i A m-d-Y',strtotime($orderdetails[0]['delivery_time']));

					}

						

				}else if($key=='hour'){

					if(strtotime($deldate[0])==strtotime(date('Y-m-d'))){

						$newdata.= date('g:i A',strtotime($deldate[1]));

					}else{

						//echo $deldate[1];

						$newdtar=explode(':',$deldate[1]); 

						//print_r($newdta);

						//if(date('H:i A',strtotime($deldate[1]))=='00:00 AM'){

						//if($newdtar[0]=='00'){

						//	$newdata.= '12:'.$newdtar[1].' AM Tomorrow';

						//}else{

							$newdata.= date('g:i A',strtotime($deldate[1]))." Tommorow";

						//}

						

					}

				}else if($key=='minute'){

					$newdata.= $retval.' Mins';

				}else if($key=='second'){

					$newdata.= 'Now';

				}else{

					$newdata.= 'Late';

				} 

				

			

            

        echo "data: $newdata\n\n"; 

		

		//ob_flush();

		//flush();

	//echo $this->load->view('admin/orders/stocks','',true);

			

	}

   public function ConvertToViewTimezone($user_time_zone,$newdate){

		   	//$user_time_zone= $this->session->userdata('timezone');

			

			$to_time_zone	 = 'Greenwich';

			//$to_time_zone	 = date_default_timezone_get();

			//$user_time_zone	= 'America/Mexico_City';

			$to_time_zone 	= ($to_time_zone)?$to_time_zone:date_default_timezone_get();

			$user_time_zone = ($user_time_zone)?$user_time_zone:date_default_timezone_get();

			if($user_time_zone){

				$schedule_date = new DateTime($newdate, new DateTimeZone($to_time_zone) );

				$schedule_date->setTimeZone(new DateTimeZone($user_time_zone));

				$returndate =  $schedule_date->format('Y-m-d H:i:s');

				

			}

			return $returndate;	

	   }	
  
	

	}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */