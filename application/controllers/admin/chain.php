<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of chain
 *
 * @author aneesh
 */
class chain extends MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->_setAsAdmin();
        $this->load->model('chain_model');
        $this->load->model('member_model');
        $this->load->helper(array('url', 'cookie'));
        $this->user = $this->session->userdata('user');
        $permission = menuPermissions($this->user->role);
        //echo $permission;

        if ($permission == 0)
            redirect($this->user->root);
        if ($this->user == '')
            redirect('admin');
    }

    function lists() {
        $this->load->library('pagination');
        $data['total_rows'] = getConfigValue('default_pagination');
        $_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] : $data['total_rows']) : $_POST['limit']);
        $_REQUEST['key'] = (!$_POST['key'] ? ($_GET['key'] ? $_GET['key'] : '') : $_POST['key']);
        $params = '?t=1';
        if ($_REQUEST['limit'])
            $params .= '&limit=' . $_REQUEST['limit'];
        if ($_REQUEST['key'])
            $params .= '&key=' . $_REQUEST['key'];
        if ($_REQUEST['status'])
            $params .= '&status=' . $_REQUEST['status'];
        $config['base_url'] = site_url("admin/chain/lists") . "/" . $params;
        $config['total_rows'] = $this->chain_model->getChainCount($_REQUEST['key'], $_REQUEST['status']);
        $config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows'] : $_REQUEST['limit'];
        $data['page'] = $_REQUEST['per_page'];
        $data['limit'] = $_REQUEST['limit'];
        $data['key'] = $_REQUEST['key'];
        $data['status'] = $_REQUEST['status'];
        $this->pagination->initialize($config);
        //----------------------------------------------------------
        $data_gol = $this->chain_model->getChainLists($_REQUEST['status'], $_REQUEST['key'], $config['per_page'], $_REQUEST['per_page']);
        $data['chainList'] = $data_gol;
        $key_1 = $this->member_model->get_key();
        $ouput['SUB_TITLE'] = 'Chain List';
        $ouput['output'] = $this->load->view("admin/chain/chainList", $data, true);
        $this->_render_output($ouput);
//     );
    }

    function delete($id) {
        $this->chain_model->deleteC($id);
        redirect("admin/chain/lists");
    }

    function add() {
        $data['restlist'] = $this->chain_model->getRestList();
        $data['rest_nonSelect'] = $this->chain_model->getRestList();
        $ouput['output'] = $this->load->view("admin/chain/add", $data, true);
        $this->_render_output($ouput);
    }

    function edit($id) {
        $data['data'] = $this->chain_model->getChainData($id);
        $data['restlist'] = $this->chain_model->getRestList();
        $data['rest_nonSelect'] = $this->chain_model->getNonSelectRest($id);
        $data['rest_Select'] = $this->chain_model->getSelectRest($id);
        $ouput['output'] = $this->load->view("admin/chain/edit", $data, true);
        $this->_render_output($ouput);
    }

    function addVal() {
        $array = array("name" => $_POST['chainname'], "desc" => $_POST['desc'], "managed_restaurent" => $_POST['master']);
        $this->chain_model->insert_chain($array, $_POST['SelectedRest']);
        redirect("admin/chain/lists");
    }

    function editVAl() {
        $array = array("name" => $_POST['chainname'], "desc" => $_POST['desc'], "managed_restaurent" => $_POST['master']);
        $condition = array('id' => $_POST['id']);
        $this->chain_model->update_chain($array, $condition, $_POST['id'], $_POST['SelectedRest']);
        redirect("admin/chain/lists");
    }

    public function pagination() {
        $config['page_query_string'] = TRUE;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        return $config;
    }

}
