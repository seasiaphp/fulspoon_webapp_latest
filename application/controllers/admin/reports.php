<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
class Reports extends My_Controller {


	public function __construct()
	{                        
		parent::__construct();
		$this->_setAsAdmin();
		$this->load->helper(array('url','cookie'));	
		$this->load->model('admin/reports_model');		
	}

	public function index()
	{
		redirect('admin/reports/sales');
	}
	public function sales($category_id=''){
		
		if($this->session->userdata('user')!=''){
			
			$user = $this->session->userdata('user');
			$restaurant_id=$user->restaurant_id;
			$location_id=$user->location_id;
			
			//for pagination
			$config	=array();
			$config=$this->pagination();
			$this->load->library('pagination');
			$data['total_rows']= getConfigValue('default_pagination');
			//$data['total_rows']= 2;
			
			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);
			$_REQUEST['daterange'] = (!$_POST['daterange'] ? ($_GET['daterange'] ? $_GET['daterange'] :''):$_POST['daterange']);
			$params = '?t=1';
				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];
				if($_REQUEST['daterange']) $params .= '&daterange='.$_REQUEST['daterange'];
				
				
			if($_REQUEST['daterange']!='')
			{
				$daterange	=	$_REQUEST['daterange'];
				$dates	=	explode('-',$daterange);
				$startDate	=	date('Y-m-d',strtotime($dates[0]));
				$endDate	=	date('Y-m-d',strtotime($dates[1]));
				$data['startDate']   =  $startDate;
				$data['endDate']   =  $endDate;
			}
			
			
			if($_SERVER['REQUEST_METHOD']=='POST'){
				$config['total_rows'] = $this->reports_model->countSalesReport($restaurant_id,$startDate,$endDate);
				 $config['per_page'] =$data['limit']= $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
				$data['orderdetails']	=$this->reports_model->getAllSalesReport($restaurant_id,$config['per_page'],$_REQUEST['per_page'],$startDate,$endDate);
				
			}else{
				$config['total_rows'] = $this->reports_model->countSalesReport($restaurant_id);
				$config['per_page'] = $data['limit']=$_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
				$data['orderdetails']	=$this->reports_model->getAllSalesReport($restaurant_id,$config['per_page'],$_REQUEST['per_page']);
			}
			$data['per_page']=$config['per_page'] ;
			$config['base_url'] = site_url("admin/reports/sales")."/".$params;	
			$this->pagination->initialize($config);		
			//echo '<pre>';print_r($data['orderdetails']);exit;		
			$ouput['output']			= $this->load->view('admin/reports/reports-sales',$data,true);
			$this->_render_output($ouput);
		
		}else{
			redirect('admin');
		}
	}
	public function customers()
	{
		
		
		if($this->session->userdata('user')!=''){
			$user = $this->session->userdata('user');
			$restaurant_id=$user->restaurant_id;
			$location_id=$user->location_id;
				
							
			//for pagination
			$config	=array();
			$config=$this->pagination();
			$this->load->library('pagination');
			$data['total_rows']= getConfigValue('default_pagination');
			
			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);
			$params = '?t=1';
				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];
				if($_REQUEST['search_sel']) $params .= '&search_sel='.$_REQUEST['search_sel'];
				
			$config['base_url'] = site_url("admin/reports/reports-customers")."/".$params;
			//$config['total_rows'] = $this->reports_model->countCustomersReports($location_id);
			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
			$data['page'] = $_REQUEST['per_page'];
			$this->pagination->initialize($config);		
			
			
			
			if($_SERVER['REQUEST_METHOD']=='POST'){
				
				$data['orderstatus']=$order_status;
				//$data['orderdetails']	=	$this->reports_model->countCustomersReports($location_id,$order_status,$config['per_page'],$_REQUEST['per_page']);
				$ouput['output']			= $this->load->view('admin/reports/reports-customers',$data,true);
				$this->_render_output($ouput);
			}else{
			##
				//$data['orderdetails']	=$this->reports_model->getAllCustomerReport($location_id,$order_status,$config['per_page'],$_REQUEST['per_page']);
				$data['orderdetails']	=$this->reports_model->getAllCustomerReport($restaurant,$config['per_page'],$_REQUEST['per_page']);
			
				//echo '<pre>';print_r($data['orderdetails']);exit;		
				$ouput['output']			= $this->load->view('admin/reports/reports-customers',$data,true);
				$this->_render_output($ouput);
			}
		}else{
			redirect('admin');
		}

	}


	public function items()
	{
		if($this->session->userdata('user')!=''){
			$user = $this->session->userdata('user');
			$restaurant_id=$user->restaurant_id;
			$location_id=$user->location_id;
						//for pagination
			$config	=array();
			$config=$this->pagination();
			$this->load->library('pagination');
			$data['total_rows']= getConfigValue('default_pagination');
			//$data['total_rows']= 2;
			
			$_REQUEST['limit'] = (!$_POST['limit'] ? ($_GET['limit'] ? $_GET['limit'] :$data['total_rows']):$_POST['limit']);
			$params = '?t=1';
				if($_REQUEST['limit']) $params .= '&limit='.$_REQUEST['limit'];
			$config['base_url'] = site_url("admin/reports/reports-items")."/".$params;
			//$config['total_rows'] = $this->reports_model->countItemsReport($location_id);
			$config['per_page'] = $_REQUEST['limit'] == 'all' ? $config['total_rows']:$_REQUEST['limit'];
			$data['page'] = $_REQUEST['per_page'];
			$this->pagination->initialize($config);	
			
				//$data['orderdetails']	=$this->reports_model->getAllItemsReport($location_id,$config['per_page'],$_REQUEST['per_page']);
				//echo '<pre>';print_r($data['orderdetails']);exit;		
				$ouput['output']			= $this->load->view('admin/reports/reports-items',$data,true);
				$this->_render_output($ouput);
			
		}else{
			redirect('admin');
		}

	}	


	public function pagination(){
		
			$config['page_query_string'] = TRUE;
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';

			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#' class='btn-info btn'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";			
			
			return $config;		
		
		
	}
	public function csv()
	{    
	    
	    $this->load->helper('custom_helper');
	     $page=$_POST['page'];
		 $fields=$_POST['feilds'];
		 $limit=$_POST['limit'];
		 $per_page=$_POST['per_page'];
		 $start=$_POST['startdate'];
		 $end=$_POST['enddate'];
		 $line=implode (", ", $fields);
	  // echo $limit;exit;
		//$restaurant_id=$user->restaurant_id;
		 if($page=='single'){
		 $data['orderdetails']	=$this->reports_model->getAllSalesReportcsv($fields,$limit,$per_page,$start,$end);
		 }else{
		 $data['orderdetails']	=$this->reports_model->getAllSalesReportcsv($fields,'','',$start,$end);
		 }
		 
	    $file_name="uploads/csv/".$restaurant_id.'_'.date('d-m-Y').".csv";
		$fp = fopen($file_name, 'w');
		$line .= "\n";
        fputs($fp, $line);
		$order	=	explode(',', $data['orderdetails']);
		//print_r($data['orderdetails']);exit;
		foreach($data['orderdetails'] as $order){
			
		$line = "";
		$line = str_replace('"', '""', $order['order_ref_id']) . ','.str_replace('"', '""', $order['restaurant_name']) . ','.
				str_replace('"', '""', $order['first_name'].''.$order['last_name']).
		        ','.str_replace('"', '""', $order['order_type']). ','.str_replace('"', '""', $order['total_amount']). ','.
				str_replace('"', '""', date('d-m-Y',strtotime($order['created_time'])));
		$line .= "\n";
		fputs($fp, $line); 		
		}
		
//$phones	=	explode(',', $phones);

fclose($fp);
            
          
// this function is written in custom_helper
	$date = date("Y-m-d");
    $result=$this->reports_model->getQuery($fields,$limit,$per_page,$start,$end); 
   query_to_csv($result,TRUE,'uploads/csv/_'.strtotime($date).'.csv');
   $ouput['output']			= $this->load->view('admin/reports/reports-sales',$data,true);
   $this->_render_output($ouput);
          
	}
	
	public function generatecsv()
	{
		 $data['start']=$_POST['start'];
		 $data['end']=$_POST['end'];
		//$data['generatecsv']=array("OrderID","Name");
		echo $this->load->view('admin/reports/generate-csv',$data,true);	
	}
	
	}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */