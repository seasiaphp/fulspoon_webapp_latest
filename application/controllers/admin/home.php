<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
	
        parent::__construct();
        $this->_setAsAdmin();
        $this->user = $this->session->userdata('user');
        $this->load->model('restaurant_model');
        $this->load->model('admin/admin_model');
		//print_r( $this->user);
		//echo 'fsds';exit;
        if ($this->user == '')
            redirect('admin');
    }

    public function index() {
        //print_r($this->session->userdata('user')->restaurant_id);exit;
        $data['template_url'] = $this->template_url;
        $member_details = $this->admin_model->getMemberDetails($this->user->admin_id); //print_r($member_details);
        $data['menus'] = getMenus($this->user->role);
        $data['user'] = $this->session->userdata('user');
        $data['countorder'] = $this->admin_model->getCount();
        $data['countcustomer'] = $this->admin_model->getCustomer();
        $data['countrestaurant'] = $this->admin_model->getRestaurant();
        $data['countsettings'] = $this->admin_model->getSettings();
        
        $ouput['output'] = $this->load->view('admin/' . $this->admin_theme . '/home', $data, true);
        $this->_render_output($ouput);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */