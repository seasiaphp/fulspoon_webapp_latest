<?php

class Member_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    // user this for get data from any tables with conditions
    public function get_where($table, $cond, $order_by = "id ASC", $type = "row") {
        $query = $this->db->order_by($order_by)->get_where($table, $cond);
        if ($type == "row") {
            return $query->row_array();
        } else {

            return $query->result_array();
        }
    }

    function logout($arr, $cond) {
        return $this->db->update("member_master", $arr, $cond);
    }

    function get_user_order($id) {
        $data = $this->db->query("select T2.device_tocken,T2.divice_type,T2.device_id,T2.push_notification from order_master T1 left join member_master T2 on T1.member_id = T2.member_id where T1.order_id='" . $id . "'");
        return $data->row_array();
    }

    function get_owner_email($id) {
        $query = $this->db->query("select `email` from restaurant_master where restaurant_id='$id'");
        $data = $query->row_array();
        return $data['email'];
    }

    function get_user($id) {
        $data = $this->db->query("select * from member_master where member_id='$id'");
        return $data->row_array();
    }

    function get_state1($id) {
        $data = $this->db->query("SELECT abbrev FROM `states` WHERE id='$id'");
        return $data->row_array();
    }

    function save_addr($arr) {
        return $this->db->insert("address", $arr);
    }

    function get_package_price($sub_id) {
        $sql = "SELECT user_fee FROM subscription_package WHERE sub_package_id  = '$sub_id' ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['user_fee'];
    }

    function get_email($name) {
        $qry = $this->db->query("select * from email_config where name='$name'");
        return $qry->row_array();
    }

    function get_all_package() {
        $qry = $this->db->query("SELECT * FROM subscription_package WHERE active='Y'");
        return $qry->result_array();
    }

    function get_user_subscription_lastDate($member_id) {
        $qry = $this->db->query("SELECT end_date FROM subscription_log WHERE member_id='$member_id' ORDER BY log_id DESC LIMIT 1");
        return $qry->row_array();
    }

    function getMemberDetails($check_data) {
        $this->db->where($check_data);
        $this->db->from('member_master');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_rest($user_id) {
        $data = $this->db->query("SELECT T1.subscription_id FROM `subscription_log` T1 WHERE End_date>=CURRENT_DATE() AND T1.member_id='$user_id'");
        $data_log = $data->row_array();

        $subscr_id = $data_log['subscription_id'];
        $data_pur = $this->db->query("select restaurant_id from current_restaurant where subscription_id='$subscr_id' and member_id='$user_id'");
//         echo $this->db->last_query();
//        exit;
        return $data_pur->result_array();
    }

    function getplaystore() {
        $data = $this->db->query("select value from general_config where field='app-id'");
        return $data->row_array();
    }

    function getappstore() {
        $data = $this->db->query("select value from general_config where field='appstore-id'");
        return $data->row_array();
    }

    function find_sub($res_id) {
//        $data = $this->db->query("select T1.*,T2.*,T3.number_of_restaurants,T4.package_id from current_restaurant T1 left join subscription_log T2 on T1.subscription_id=T2.subscription_id left join User_subscription_master T4 on T1.subscription_id=T4.subscription_id left join subscription_package T3 on T4.package_id=T3.sub_package_id where T1.restaurant_id='$res_id' and ((T2.start_date>='" . date("Y-m-d") . "' and T2.End_date<='" . date("Y-m-d") . "' ) or (T2.start_date<='" . date("Y-m-d") . "' and T2.End_date<='" . date("Y-m-d") . "' )) group by T1.member_id");

        $data = $this->db->query("select T1.*,T2.*,T3.number_of_restaurants,T4.package_id from current_restaurant T1 left join subscription_log T2 on T1.subscription_id=T2.subscription_id left join User_subscription_master T4 on T1.subscription_id=T4.subscription_id
 left join subscription_package T3 on T4.package_id=T3.sub_package_id where T1.restaurant_id='$res_id' and
 ((T2.start_date >= '" . date("Y-m-d") . "' AND T2.End_date <= '" . date("Y-m-d") . "') or (T2.start_date <= '" . date("Y-m-d") . "' AND T2.End_date >= '" . date("Y-m-d") . "') ) group by T1.member_id
");

        $result = $data->result_array();
//        echo $this->db->last_query();
        return $result;
    }

    function get_my_stripe_id($id) {
        $data = $this->db->query("select strip_id from subscription_log where member_id=$id order by log_id desc limit 1");
        $return = $data->row_array();
        return $return['strip_id'];
    }

    function select_card($id) {
        $data = $this->db->query("select id from member_stripid_map where ");
    }

    function get_rest_list($sub_id, $user_id, $res_id) {
        $data = $this->db->query("select T1.restaurant_id from current_restaurant T1 left join restaurant_master T2 on T1.restaurant_id=T2.restaurant_id where T1.member_id='$user_id' and T1.subscription_id='$sub_id' and T1.restaurant_id!='$res_id' and T2.expire_flag='N'");
        return $data->result_array();
    }

    function find_next_sub($number) {
        return $this->db->query("select * from subscription_package where number_of_restaurants=$number")->row_array();
    }

    function update_contract($arr, $cond) {
        return $this->db->update("restaurant_master", $arr, $cond);
    }

    function validateFacebookId($fb_id) {
        $sql = "SELECT * FROM member_master WHERE facebook_id  = '$fb_id' ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function update_expire_flag($id, $status) {
        $this->db->update("restaurant_master", array("expire_flag" => $status), array("restaurant_id" => $id));
    }

    function rest_master_select() {
        $data = $this->db->query("select * from restaurant_master where status='Y'");
        return $data->result_array();
    }

    function save_cuisine($arr) {
        $this->db->insert("cusine_master", $arr);
        return $this->db->insert_id();
    }

    function member_model_sub() {
        $data = $this->db->query("select * from proportion_subscription order by proportion asc");
        return $data->result_array();
    }

    function get_avg_model() {
        $data = $this->db->query("select * from average_order_master order by average_order asc");
        return $data->result_array();
    }

    function admin_val() {
        $data = $this->db->query("select * from admin_master");
        return $data->row_array();
    }

    function get_exptd_inc_mod() {
        $data = $this->db->query("select * from expected_incremental_orders order by expected_incremental_orders asc");
        return $data->result_array();
    }

    function check_new_cuisine($arr) {
        $data = $this->db->query("select count(*) as count from cusine_master where cusine_name='$arr'");
        $count = $data->row_array();
        $cou = $count['count'];
        return $cou;
    }

    function select_cuisine() {
        $data = $this->db->query("select * from cusine_master order by cusine_id");
        return $data->result_array();
    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKabxcmmm0987654321LMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function Registration($arr) {
        $password = md5($arr['password']);
        $data = array(
            'user_type' => 2,
            'first_name' => ucfirst($arr['name']),
            'user_email' => $arr['user_email'],
            'password' => $password,
            'created_date' => date("Y-m-d H:i:s"),
            'facebook_id' => $arr['fb_unique_id'],
            'device_id' => $arr['devicetoken'],
            'status' => 'Y',
        );
        $this->db->insert('member_master', $data);
        return $this->db->insert_id();
    }

    function ins_mem($arr) {
        $this->db->insert("member_master", $arr);
        return $this->db->insert_id();
    }

    function updateMember($arr, $cond) {
        return $this->db->update("member_master", $arr, $cond);
    }

    function checkEmailExist($email) {
        $sql = "select * from member_master where email = '$email'   ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function checkEmailExist_res($email) {
        $sql = "select * from restaurant_master where email = '$email'   ";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

    function updateFacebookId($member_id, $facebook_id) {
        $this->db->query("update member_master set facebook_id ='" . $facebook_id . "' where member_id='" . $member_id . "' ");
        return;
    }

    function save_hotel($hotel) {
        $this->db->insert("restaurant_master", $hotel);
        return $this->db->insert_id();
    }

    function update_hotel($hotel) {
        $this->db->where('google_id', $hotel['google_id']);
        $this->db->update("restaurant_master", $hotel);
    }

    function save_restCuisine_map($arr) {
        $this->db->insert("restauarnt_cusine_map", $arr);
    }

    function get_restaurant_detail() {
        $sql = "select *,google_id as place_id from restaurant_master where status ='Y' and signed_flag='Y' order by restaurant_id desc";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_google_id() {
        $data = $this->db->query("select * from restaurant_master where status ='Y' and signed_flag='Y' group by google_id order by restaurant_id desc");
        return $data->result_array();
    }

    function get_locu_id($rid) {
        $data = $this->db->query("select locu_id from restaurant_master where restaurant_id='$rid' ");
        $res = $data->row_array();
        return $res['locu_id'];
    }

    function faq() {
        $data = $this->db->query("select * from cms_master where type='faq' order by faq_id desc");
        return $data->row_array();
    }

    function terms_of_use() {
        $data = $this->db->query("select * from cms_master where type='terms_of_use' order by faq_id desc");
        return $data->row_array();
    }

    function privacy_policy() {
        $data = $this->db->query("select * from cms_master where type='privacy_policy' order by faq_id desc");
        return $data->row_array();
    }

    function save_package($arr) {
        $this->db->insert("User_subscription_master", $arr);
        return $this->db->insert_id();
    }

    function save_res_user($arr) {
        return $this->db->insert("current_restaurant", $arr);
    }

    function save_res_user1($arr) {
        return $this->db->insert("restaurants_next", $arr);
    }

    function get_cusine_config() {
        $data = $this->db->query("select value from general_config where field='cuisine_filter' ");
        return $data->row_array();
    }

    function get_rating_config() {
        $data = $this->db->query("select value from general_config where  field='rating_filter'");
        return $data->row_array();
    }

    function get_stripe_public_config() {
        $data = $this->db->query("select stripe_public_key from admin_master ");
        $return = $data->row_array();
        return $return['stripe_public_key'];
    }

    function get_price_config() {
        $data = $this->db->query("select value from general_config where  field='Price_filter'");
        return $data->row_array();
    }

    function get_feature_config() {
        $data = $this->db->query("select value from general_config where field='feature_filter'");
        return $data->row_array();
    }

    function select_states() {
        $data = $this->db->query("select * from states");
        return $data->result_array();
    }

    function get_restaurent($id) {
        $data = $this->db->query("select * from restaurant_master where restaurant_id='$id' ");
        return $data->row_array();
    }

    function check_order($id) {
        $data = $this->db->query("select order_status from order_master where order_id='$id'");
        $row = $data->row_array();
        if ($row['order_status'] == "New") {
            return "Y";
        } else {
            return "N";
        }
    }

    function update_order($arr1, $arr2) {
        return $this->db->update("order_master", $arr1, $arr2);
    }

    function admin_contratct() {
        $data = $this->db->query("select * from admin_master order by admin_id desc limit 1");
        return $data->row_array();
    }

    function get_google_id_con($rid) {
        $data = $this->db->query("select google_id from restaurant_master where restaurant_id='$rid' ");
        $res = $data->row_array();
        return $res['google_id'];
    }

    function gelAllCategories($rid) {
        $data = $this->db->query("select * from menu_rest_master where res_id='$rid'");
        return $data->result_array();
    }

    function ins_size($arr) {
        $this->db->insert("dish_item_size_map", $arr);
        return $this->db->insert_id();
    }

    function get_key() {
        $data = $this->db->query("select google_id from admin_master order by admin_id desc limit 1");
        $key = $data->row_array();
        return $key['google_id'];
    }

    function ins_side_options($arr) {
        $this->db->insert("option_sides", $arr);
        //echo $this->db->last_query();
        return $this->db->insert_id();
    }

    function ins_dish($arr) {
        $this->db->insert("dish_options", $arr);
        return $this->db->insert_id();
    }

    function save_cat_dish_rest($dish_name, $desc, $price, $menu_name, $rid) {
        $menu_id = $this->find_menu_id($menu_name, $rid);
        $arr = array("dish_name" => $dish_name, "description" => $desc, "price" => $price, "dish_name_orginal" => $dish_name, "description_original" => $desc, "price_original" => $price, "menu" => $menu_id, "rid" => $rid);
        $this->db->insert("dish_master", $arr);
        return $this->db->insert_id();
    }

    function find_menu_id($menu_name, $rid) {
        $data = $this->db->query("select count(*) as count,id from menu_rest_master where menu_name_original='" . mysql_real_escape_string($menu_name) . "' and res_id='$rid'");
        $count = $data->row_array();
        if ($count['count'] > 0) {
            return $count['id'];
        } else {
            $this->db->insert("menu_rest_master", array("menu_name" => $menu_name, "menu_name_original" => $menu_name, "res_id" => $rid));
            return $this->db->insert_id();
        }
    }

    function get_fb_app() {
        $data = $this->db->query("select `value` from general_config where field='facebook_app_id'");
        $datas = $data->row_array();
        return $datas['value'];
    }

    function check_res_exist($id) {
        $data = $this->db->query("select count(*) as count from restaurant_master where google_id='$id'");
        $cou = $data->row_array();
        return $cou['count'];
    }

    function delete_reg($id) {
        $this->db->delete("restauarnt_cusine_map", array("rest_id" => $id));
        return $this->db->delete("restaurant_master", array("restaurant_id" => $id));
    }

    function get_google_id_filter($cus_arr, $fea_array, $search_key = '', $sort_type = 'default', $latitude, $longitude) {

        if ($latitude != '') {
            $qry = "select T1.*,T2.*,( 3959 * acos( cos( radians($latitude) ) * cos( radians( T1.lat ) ) * cos( radians(T1.long) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( T1.lat ) ) ) ) AS distance  from restaurant_master T1 left join restauarnt_cusine_map T2 on T1.restaurant_id= T2.rest_id where T1.status='Y'  and T1.signed_flag='Y' ";

            if ($search_key != '')
                $qry.=" and (T1.postal_code = '" . $search_key . "' || T1.formatted_address LIKE '%" . $search_key . "%' || T1.restaurant_name LIKE '%" . $search_key . "%' || T1.restaurant_name LIKE '%" . $search_key . "%' ) ";

            if (sizeof($cus_arr) > 0) {
                $qry.=" and ( ";
                $i = 0;
                foreach ($cus_arr as $cus) {
                    if ($i > 0) {
                        $qry.=" or (";
                    }
                    $qry.=" T2.cusine_id='" . $cus . "'";
                    if ($i > 0) {
                        $qry.=" ) ";
                    }
                    $i++;
                }
                $qry.=" )";
            }

            foreach ($fea_array as $feature) {

                switch ($feature) {
                    case 'alcohol': $qry.= " and T1.alcohol!='' && T1.alcohol!='no_alcohol' ";
                        break;
                    case 'parking': $qry.= " and T1.parking!='' ";
                        break;
                    case 'wifi': $qry.= " and T1.wifi!='' && T1.wifi!='no' ";
                        break;
                    case 'corkage': $qry.= " and  T1.corkage!='' ";
                        break;
                    case 'dietary_restrictions': $qry.= " and  T1.dietary_restrictions!='' ";
                        break;
                    case 'music': $qry.= " and  T1.music!='' ";
                        break;
                    case 'sports':$qry.= " and T1.sports!='' ";
                        break;
                    case 'wheelchair_accessible':$qry.= " and T1.wheelchair_accessible!='' && T1.wheelchair_accessible!='no' ";
                        break;
                    case 'outdoor_seating':$qry.= " and ( T1.outdoor_seating!='' && T1.outdoor_seating!='no' ";
                        break;
                    case 'smoking':$qry.= " and  T1.smoking!='' && T1.smoking!='no' ";
                        break;
                    case 'waiter_service':$qry.= " and  T1.waiter_service!='' && T1.waiter_service!='no' ";
                        break;
                    case 'television':$qry.= " and T1.television!='' ";
                        break;
                    case 'ambience':$qry.= " and T1.ambience!='' ";
                        break;
                }
            }
            $qry.= "  group by T2.rest_id ";
            $lat_qry = " distance asc ";
            switch ($sort_type) {
                case 'default' : $qry.= " order by " . $lat_qry . " , T1.restaurant_id desc";
                    break;
                case 'price_asc' : $qry.= "  order by  T1.pricing asc ,$lat_qry ";
                    break;
                case 'price_desc' : $qry.= "  order by  T1.pricing desc ,$lat_qry ";
                    break;
                case 'rating' : $qry.= "  order by  T1.rating desc ,$lat_qry ,";
                    break;
                default : $qry.= " order by " . $lat_qry . " , T1.restaurant_id desc";
                    break;
            }
        } else {
            $qry = "select T1.*,T2.* from restaurant_master T1 left join restauarnt_cusine_map T2 on T1.restaurant_id= T2.rest_id where T1.status='Y'  and T1.signed_flag='Y' ";


            if ($search_key != '') {
                $qry.=" and (T1.postal_code = '" . $search_key . "' || T1.formatted_address LIKE '%" . $search_key . "%' || T1.restaurant_name LIKE '%" . $search_key . "%' || T1.restaurant_name LIKE '%" . $search_key . "%' ) ";
            }


            if (sizeof($cus_arr) > 0) {
                $qry.=" and ( ";
                $i = 0;
                foreach ($cus_arr as $cus) {
                    if ($i > 0) {
                        $qry.=" or (";
                    }
                    $qry.=" T2.cusine_id='" . $cus . "'";
                    if ($i > 0) {
                        $qry.=" ) ";
                    }
                    $i++;
                }
                $qry.=" )";
            }

            foreach ($fea_array as $feature) {

                switch ($feature) {
                    case 'alcohol': $qry.= " and T1.alcohol!='' && T1.alcohol!='no_alcohol' ";
                        break;
                    case 'parking': $qry.= " and T1.parking!='' ";
                        break;
                    case 'wifi': $qry.= " and T1.wifi!='' && T1.wifi!='no' ";
                        break;
                    case 'corkage': $qry.= " and  T1.corkage!='' ";
                        break;
                    case 'dietary_restrictions': $qry.= " and  T1.dietary_restrictions!='' ";
                        break;
                    case 'music': $qry.= " and  T1.music!='' ";
                        break;
                    case 'sports':$qry.= " and T1.sports!='' ";
                        break;
                    case 'wheelchair_accessible':$qry.= " and T1.wheelchair_accessible!='' && T1.wheelchair_accessible!='no' ";
                        break;
                    case 'outdoor_seating':$qry.= " and ( T1.outdoor_seating!='' && T1.outdoor_seating!='no' ";
                        break;
                    case 'smoking':$qry.= " and  T1.smoking!='' && T1.smoking!='no' ";
                        break;
                    case 'waiter_service':$qry.= " and  T1.waiter_service!='' && T1.waiter_service!='no' ";
                        break;
                    case 'television':$qry.= " and T1.television!='' ";
                        break;
                    case 'ambience':$qry.= " and T1.ambience!='' ";
                        break;
                }
            }
            $qry.= "  group by T2.rest_id ";

            switch ($sort_type) {
                case 'default' : $qry.= "  order by T1.restaurant_id desc";
                    break;
                case 'price_asc' : $qry.= "  order by T1.pricing asc";
                    break;
                case 'price_desc' : $qry.= "  order by T1.pricing desc";
                    break;
                case 'rating' : $qry.= "  order by T1.rating desc";
                    break;
                default : $qry.= "  order by T1.restaurant_id desc";
                    break;
            }
        }
        $data = $this->db->query($qry);
        return $data->result_array();
    }

    function get_cuisine_res($id) {
        $sql = "select T1.*,T2.cusine_name from restauarnt_cusine_map T1 left join cusine_master T2 on T1.cusine_id=T2.cusine_id where T1.rest_id='$id' order by T1.res_cusine_id desc";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    //new code umesh
    public function gelAllDishItems($restaurant_id, $cat_id) {
        $sql = "SELECT A.*,B.menu_name 
					FROM dish_master A
					LEFT JOIN menu_rest_master B on A.menu=B.id ";

        $sql.="	WHERE  A.menu in (
					SELECT menu 
					FROM  dish_master 
					WHERE B.res_id='$restaurant_id'
					)";
        if ($cat_id != '') {
            $sql.=" AND A.menu='$cat_id'";
        }
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getFeatures() {
        
    }

    public function pendingSubscription_now($member_id) {
        $sql = "SELECT T3.* FROM (SELECT subscription_id FROM User_subscription_master 
	 WHERE member_id='" . $member_id . "' AND  strip_id<>'' ORDER BY subscription_id DESC LIMIT 1) AS T1 
	 LEFT JOIN current_restaurant T2 ON T1.subscription_id=T2.subscription_id 
	 LEFT JOIN restaurant_master T3 ON T2.restaurant_id=T3.restaurant_id";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function pendingSubscription_later($member_id) {
        $sql = "SELECT A3.* FROM subscription_next A1 
					LEFT JOIN restaurants_next A2 ON A1.subscription_id=A2.next_subscription_id
					LEFT JOIN restaurant_master A3 ON A2.restaurant_id=A3.restaurant_id 
					WHERE A1.member_id='" . $member_id . "' AND A1.status='Y'";
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_all_restaurant_detail($rid) {
        $data = $this->db->query("select * from restaurant_master where restaurant_id='$rid' ");
        $res = $data->row_array();
        return $res;
    }

}

?>