<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of owners
 *
 * @author aneesh
 */
// error_reporting(E_ALL);
class Home extends MY_Controller {

    public function __construct() {

        parent::__construct();
//        date_default_timezone_set('GMT');
        $this->load->model('owner_model');
        $this->load->model('/order_model');
        $this->load->model('member_model');
        $this->load->library('session');

        ob_start();
        session_start();
        //$this->clear_cache();
    }

    //function clear_cache() {
    //    $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
    //    $this->output->set_header("Pragma: no-cache");
    //}

    public function index() {
        $user = $this->session->userdata('user');
        //print_r($user);exit;
        $restaurant_id = $user->restaurant_id;
        if ($this->check()) {
            redirect('owner/home/dashboard', 'refresh');
        } else {
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('owner/static_header', $data);
            $this->load->view('owner/login');
            $this->load->view('owner/footer');
        }
    }

    public function new_pass() {
        $user = $this->session->userdata('user');

        $res_id = $user->restaurant_id;
        $_POST['new_owner'] = 'N';
        $_POST['password'] = md5($_POST['password']);


        $this->db->where('restaurant_id', $res_id);
        $this->db->update('restaurant_master', $_POST);


        redirect('owner/home/dashboard', 'refresh');
    }

    public function check_login() {
        $email = $this->input->post('email');

        $password = $this->input->post('password');

        $user = $this->owner_model->getOwnerDetails(array('email' => $email));




        //echo md5($user['password']);exit;
        if (count($user) > 0) {
            if ($user['password'] === md5($password)) {
                $_SESSION['res_id'] = $user['restaurant_id'];
                $this->session->set_userdata('user', (object) $user);
                $result = array('status' => 'success', 'result' => 'valid', 'new_owner' => $user['new_owner']);
                echo json_encode($result);
                exit;
            } else {
                $result = array('status' => 'error', 'result' => 'invalid');
                echo json_encode($result);
                //echo 'invalid';
                exit;
            }
        } else {
            $result = array('status' => 'error', 'result' => 'invalid');
            echo json_encode($result);
            //echo 'invalid';
            exit;
        }
    }

//    function forgot_password(){
//        
//    }

    public function forgotpwd() {

        $email_id = $_POST['email'];
        $data = $this->owner_model->getOwnerDetails(array('email' => $email_id));
        $new_password = $this->owner_model->randomPassword();
//        echo $new_password;
//        exit;
        $this->load->model('email_model');
        $this->load->helper('cookie');
        $cookie = array(
            'name' => $new_password,
            'value' => $email_id,
            'expire' => time() + 3600
        );

        if (count($data) > 0) {
            $email = $this->email_model->get_email_template('forgot_password_admins');
            $category = "Forgot Password";
            $this->load->library('encrypt');
            $full_name = $data['full_name'];
            $target_name = $this->config->item('site_name');
            $link = base_url() . 'owner/home/reset_password/' . $new_password;
            $data_1 = $this->member_model->admin_contratct();
            $subject = $email['email_subject'];
            $message = $email['email_template'];
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html; charset=utf-8" . "\r\n";
            $headers .= 'From: ' . $data_1['project_name'] . '<' . $data_1['email'] . ">\r\n";
            $message = $email['email_template'];
            $message = str_replace('#MemberName#', $full_name, $message);
            $message = str_replace('#LINK#', $link, $message);
            $message = str_replace('#subject#', $subject, $message);
            $message = str_replace('#baseurl#', base_url(), $message);
            $this->member_model->mail_sendgrid($email_id, $subject, $message, $category, $data_1['email'], $data_1['project_name']);
//            if (@mail($email_id, $subject, $message, $headers)) {
            echo "success";
            set_cookie($cookie);
//            } else
//                echo "There is error in sending mail!";
        } else {
            echo "Please enter valid email id!";
        }
    }

//    function changepassword(){
//        
//    }

    public function reset_password($new_password) {
        $cookie = get_cookie($new_password);

        if ($cookie) {
            $data['email'] = $cookie;
            $data['key'] = $new_password;
            //delete_cookie($new_password);
            //$output['site_name']	= getConfigValue('site_name_front');
            $data['site_name'] = 'Fulspoon';
            $data['title'] = 'Password Reset';
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('owner/static_header', $data);
            $this->load->view('owner/resetPassword', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect(base_url() . 'owner/home', 'refresh');
        }
    }

    public function changepassword() {
        $data = $this->input->post();
        if ($data['password'] == $data['confirm_password']) {

            $check = array('password' => md5($data['password']));
            $admin_data = $this->owner_model->getOwnerDetails(array('email' => $data['email']));
            $result = $this->owner_model->updateOwner($check, $admin_data['restaurant_id']);
            if ($result != 'error') {
                delete_cookie($data['key']);
                echo 'success';
            } else {
                echo 'Unknown error occur';
            }
        } else {
            echo 'Password Mismatch';
        }
    }

    public function dashboard() {

        $user = $this->session->userdata('user');
        //print_r($user);exit;
        $data['restaurant_id'] = $user->restaurant_id;
        if ($this->check()) {
            $data['last_order_number'] = $this->order_model->getLastOrderNumber($user->restaurant_id);
            $data['site_name'] = 'Fulspoon';
            $data['template_url'] = base_url() . 'assets/dashboard';
            $this->load->view('owner/header', $data);
            $this->load->view('owner/home', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect('owner/home', 'refresh');
        }
    }

    public function new_owner() {

        $user = $this->session->userdata('user');
        $this->load->view('owner/new_owner', $data);
        $this->load->view('owner/footer', $data);
    }

    public function check() {
        $user_data = $this->session->userdata('user');
        if ($user_data->restaurant_id)
            return 1;
        else
            return 0;
    }

    public function logout() {
        $this->session->sess_destroy();
        unset($_SESSION);
        //print_r($this->session->all_userdata()); 
        //exit;
        redirect('owner', 'refresh');
    }

    public function preference($location_id = '') {

        $this->load->model('preference_model');
        $user = $this->session->userdata('user');


        $location_id = $user->restaurant_id;


        $data['restconfiglist'] = $this->preference_model->get_restConfig($location_id);
        $data['restaurent'] = $this->preference_model->get_restaurent($location_id);
//        $data['restaurent_delivery_time'] = $this->preference_model->get_delivery_time($location_id);
//echo '<pre>';print_r($user);exit;
        //exit;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post_title = $_POST['title'];
            $post_aar = $_POST;
            //echo '<pre>';print_r($_POST);

            $array_update = array("delivery_time" => $_POST['Minimum_delivery_time'], "pickup_time" => $_POST['Minimum_pickup_time'], "tax" => $_POST['tax']);
            $condition_arr = array("restaurant_id" => $location_id);
            $this->preference_model->update_res($array_update, $condition_arr);
            $this->session->set_flashdata('success_message', 'Preferences  Updated Successfully. ', 'SUCCESS');
            redirect('owner/home/preference');
        }
        $data['last_order_number'] = $this->order_model->getLastOrderNumber($user->restaurant_id);
        $data['active'] = 'preference';
        $this->load->view('owner/header', $data);
        $this->load->view('owner/preference', $data);
        $this->load->view('owner/footer', $data);
    }

    public function profile($location_id = '') {
        if ($this->check()) {
            $this->load->model('preference_model');
            $user = $this->session->userdata('user');
            $location_id = $user->restaurant_id;
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->db->where('restaurant_id', $_POST['restaurant_id']);
                $this->db->update('restaurant_master', $_POST);
                $i = 0;
//                foreach ($post_aar as $key => $value) {
//                    if ($key != 'title') {
//                        $dd = array('config_id' => $key,
//                            'field' => $key,
//                            'value' => $value,
//                            'location_id' => $location_id,
//                            'description' => $post_title[$i]);
//                        $update_id = $this->preference_model->insert($dd);
//                        $i++;
//                    }
//                }
//            }
                $this->session->set_flashdata('success_message', 'Profile  Updated Successfully. ', 'SUCCESS');
                redirect('owner/profile');
            }
            $data['last_order_number'] = $this->order_model->getLastOrderNumber($user->restaurant_id);
            $data['profile'] = $this->preference_model->get_restaurent($location_id);
            $data['cusine_config'] = $this->member_model->get_cusine_config();
            $data['rating_config'] = $this->member_model->get_rating_config();
            $data['price_config'] = $this->member_model->get_price_config();
            $data['feature_config'] = $this->member_model->get_feature_config();
            //echo '<pre>';print_r($data);exit;
            $data['active'] = 'profile';
            $this->load->view('owner/header', $data);
            $this->load->view('owner/profile', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect("owner");
        }
    }

}
