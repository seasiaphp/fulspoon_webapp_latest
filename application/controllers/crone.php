<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of crone
 *
 * @author aneesh
 */
error_reporting(1);
ini_set("display_errors", "on");
ini_set("memory_limit", "256M");
ini_set('max_execution_time', 0);

class crone extends MY_Controller {

    public function __construct() {

        error_reporting(1);
        parent::__construct();
        $this->load->model('Member_model', 'member_model');
        $this->load->model('client_model');
        $this->load->model('user_model');
        $this->load->model('cron_model');
//        date_default_timezone_set('GMT');
    }

    function crone_locu() {
        $resdata = $this->member_model->rest_master_select();
        foreach ($resdata as $res) {
            $locu_id = $res['locu_id'];
            $rest_id = $res['restaurant_id'];
//            $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $api_key = $this->user_model->generlConfig('locu_key');
            $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\", \"extended\",\"delivery\",\"contact\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
      ]
}'";
            echo $uri;
            $c_pid = exec($uri);
            $c_pid = json_decode($c_pid);
            $current_menu_details = $this->cron_model->get_rest_menu($rest_id);
            foreach ($c_pid->venues[0]->menus as $menues) {
                $menu_flag = 0;
                $menu_name = $menues->menu_name;
                foreach ($current_menu_details as $curr_menu_dtls) {
                    $curr_mn_name = $curr_menu_dtls["menu_name_original"];
                    $true_menu_name = $menu_name;
                    foreach ($menues->sections as $sec_key => $section) {
                        $true_menu_name = $menu_name;
                        $sec_menu_name = $menu_name;
                        if ($section->section_name != '') {
                            $sec_menu_name = $section->section_name;
                            $true_menu_name = $sec_menu_name;
                        }
                        foreach ($section->subsections as $sub_key => $subsection) {
                            $true_menu_name = $sec_menu_name;
                            $sub_menu_name = $sec_menu_name;
                            if ($subsection->subsection_name != '') {
                                $sub_menu_name = $subsection->subsection_name;
                                $true_menu_name = $sub_menu_name;
                            }
                            if ($true_menu_name == $curr_mn_name) {
                                $menu_flag = 1;
                                $current_dish_all = $this->cron_model->get_dish_ofMenu($curr_menu_dtls['id']);
                                foreach ($subsection->contents as $cont_key => $contents) {
                                    $dish_flag = 0;
                                    foreach ($current_dish_all as $curr_dish) {
                                        $current_dishName = $curr_dish['dish_name_orginal'];
                                        $dish_name = $contents->name;
                                        if ($current_dishName == $dish_name) {
                                            $rid = $rest_id;
                                            $dish_flag = 1;
                                            $desc = $contents->description;
                                            $price = $contents->price;
                                            $dish_id = $curr_dish['id'];
                                            $option_grp = $contents->option_groups;
                                            $chk_crone_flag = 0;
                                            $chk_flag = 0;
                                            $curr_size = $this->cron_model->get_size_loc($dish_id);
                                            foreach ($option_grp as $option_g) {
                                                $chk_flag = 1;
                                                if ($option_g->type == "OPTION_CHOOSE") {

                                                    echo 'OPTION_CHOOSE<br/>current_size<br/>';
                                                    $flaggg = 0;

                                                    foreach ($option_g->options as $options) {
                                                        $check_flag_size = 0;
                                                        foreach ($curr_size as $cur_size) {
                                                            $flaggg = 1;
                                                            if ($cur_size['size'] == $options->name) {
                                                                $check_flag_size = 10;
                                                                if ($cur_size['size_original'] == $options->name) {
                                                                    $name = $cur_size['size'];
                                                                    $size_original_op = $cur_size['size_original'];
                                                                } else {
                                                                    $name = $options->name;
                                                                    $size_original_op = $name;
                                                                }if ($cur_size['price_original'] == $options->price) {
                                                                    if ($cur_size['price'] == '') {
                                                                        $cur_size['price'] = 0;
                                                                    }if ($cur_size['price_original'] == '') {
                                                                        $cur_size['price_original'] = 0;
                                                                    }
                                                                    $price_op = $cur_size['price'];
                                                                    $price_op_original = $cur_size['price_original'];
                                                                } else {
                                                                    if ($options->price == '') {
                                                                        $price_op = 0;
                                                                    } else {
                                                                        $price_op = $options->price;
                                                                    }
                                                                    $price_op_original = $price_op;
                                                                }
                                                                $size_array = array("item_id" => $dish_id, "update_on" => date("Y-m-d H:i:s"), "size" => $name, "price" => $price_op_original, "size_original" => $size_original_op, "price_original" => $price_op);
                                                                $this->cron_model->update_size_locu($size_array, $cur_size['map_id']);
                                                                $this->db->query("delete from dish_item_size_map where item_id = '$dish_id' and size = '$name'  and map_id!='" . $cur_size['map_id'] . "' ");
                                                            }
                                                        }if ($check_flag_size == 0) {
                                                            $flaggg = 1;
                                                            $name = $options->name;
                                                            $price_op = $options->price;
                                                            $size_array = array("item_id" => $dish_id, "update_on" => date("Y-m-d H:i:s"), "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
                                                            $this->member_model->ins_size($size_array);
                                                        }
                                                    }
                                                    if ($flaggg != 1) {
                                                        $chk_regular_size = $this->cron_model->get_regular_size($dish_id);
                                                        if ($chk_regular_size['count'] > 0) {
                                                            $map_id = $chk_regular_size['map_id'];
                                                            if ($chk_regular_size['size_original'] != "Regular") {
                                                                $size = $chk_regular_size['size'];
                                                                $size_original = $chk_regular_size['size_original'];
                                                            } else {
                                                                $size = 'Regular';
                                                                $size_original = $size;
                                                            }
                                                            if ($chk_regular_size['price_original'] == $price) {
                                                                $price = $chk_regular_size['price'];
                                                                $price_original = $chk_regular_size['price_original'];
                                                                if ($price == '') {
                                                                    $price = 0;
                                                                }
                                                                if ($price_original == '') {
                                                                    $price_original = 0;
                                                                }
                                                            } else {
                                                                if ($price == '') {
                                                                    $price = 0;
                                                                }
                                                                $price_original = $price;
                                                            }
                                                            $size_array = array("item_id" => $dish_id, "size" => $size, "size_original" => $size_original, "update_on" => date("Y-m-d H:i:s"), "price" => $price, "price_original" => $price_original);
                                                            $this->cron_model->update_size_locu($size_array, $chk_regular_size['map_id']);
                                                        } else {
                                                            $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
                                                            $this->member_model->ins_size($size_array);
                                                        }
//                                                     
                                                    }
                                                } elseif ($option_g->type == "OPTION_ADD") {
                                                    $curr_dish_size = $this->cron_model->get_curr_dish($rid, $dish_id);
                                                    foreach ($curr_dish_size as $current_dishes) {
                                                        $opt_sides = $this->cron_model->get_opt_sides($current_dishes['option_id']);
                                                        $option_name_title = $option_g->text;
                                                        $dish_id3 = $current_dishes['option_id'];
                                                        if ($option_name_title == '') {
                                                            $option_name_title = 'options and sides';
                                                            foreach ($option_g->options as $options3) {
                                                                $op_flag1 = 0;
                                                                foreach ($opt_sides as $option_1) {
                                                                    $name = $options3->name;
                                                                    if ($option_1['side_item_original'] == $name) {
                                                                        $op_flag1 = 1;
                                                                        $price_op1 = explode("+", $options3->price);
                                                                        $price_op = $price_op1[1];
                                                                        if ($price_op == '') {
                                                                            $price_op = 0;
                                                                        }
                                                                        if ($option_1['price_original'] == $price_op) {
                                                                            $price_original = $price_op;
                                                                            $price_op = $option_1['price'];
                                                                            if ($price_op == '') {
                                                                                $price_op = 0;
                                                                            }
                                                                        } else {
                                                                            $price_original = $price_op;
                                                                        }
                                                                        $dish_arrayss = array("restaurant_id" => $rid, "updated_on" => date("Y-m-d H:i:s"), "dish_item_id" => $dish_id, "option_name" => $current_dishes['option_name'], "option_name_original" => $option_name_title);
                                                                        $cond_Array = array("option_id" => $current_dishes['option_id']);
                                                                        $this->cron_model->update1("dish_options", $dish_arrayss, $cond_Array);
                                                                        $name_side = $option_1['side_item'];
                                                                        $dish_array = array("option_id" => $dish_id3, "updated_on" => date("Y-m-d H:i:s"), "side_item" => $name_side, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                                                        $condition_array = array("side_id" => $option_1['side_id']);
                                                                        $this->cron_model->update_add_side($dish_array, $condition_array);
                                                                    }
                                                                }
                                                                if ($op_flag1 == 0) {
                                                                    $name = $options3->name;
                                                                    $price_op1 = explode("+", $options3->price);
                                                                    $price_op = $price_op1[1];
                                                                    if ($price_op == '') {
                                                                        $price_op = 0;
                                                                    }
                                                                    $dish_array = array("option_id" => $dish_id3, "updated_on" => date("Y-m-d H:i:s"), "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                                                    $this->member_model->ins_side_options($dish_array);
                                                                }
                                                            }
                                                        } else {
                                                            if ($current_dishes['option_name_original'] == $option_name_title) {
                                                                $dish_array = array("restaurant_id" => $rid, "updated_on" => date("Y-m-d H:i:s"), "dish_item_id" => $dish_id, "option_name" => $current_dishes['option_name'], "option_name_original" => $option_name_title);
                                                                $cond_Array = array("option_id" => $current_dishes['option_id']);
                                                                $this->cron_model->update1("dish_options", $dish_array, $cond_Array);
                                                                foreach ($option_g->options as $options3) {
                                                                    $op_flag1 = 0;
                                                                    foreach ($opt_sides as $option_1) {
                                                                        $name = $options3->name;
                                                                        if ($option_1['side_item_original'] == $name) {
                                                                            $op_flag1 = 1;
                                                                            $price_op1 = explode("+", $options3->price);
                                                                            $price_op = $price_op1[1];
                                                                            if ($price_op == '') {
                                                                                $price_op = 0;
                                                                            }
                                                                            if ($option_1['price_original'] == $price_op) {
                                                                                $price_original = $price_op;
                                                                                $price_op = $option_1['price'];
                                                                                if ($price_op == '') {
                                                                                    $price_op = 0;
                                                                                }
                                                                            } else {
                                                                                $price_original = $price_op;
                                                                            }
                                                                            $name_side = $option_1['side_item'];
                                                                            $dish_array = array("option_id" => $dish_id3, "updated_on" => date("Y-m-d H:i:s"), "side_item" => $name_side, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                                                            $condition_array = array("side_id" => $option_1['side_id']);
                                                                            $this->cron_model->update_add_side($dish_array, $condition_array);
                                                                        }
                                                                    }
                                                                    if ($op_flag1 == 0) {
                                                                        $name = $options3->name;
                                                                        $price_op1 = explode("+", $options3->price);
                                                                        $price_op = $price_op1[1];
                                                                        if ($price_op == '') {
                                                                            $price_op = 0;
                                                                        }
                                                                        $dish_array = array("option_id" => $dish_id3, "updated_on" => date("Y-m-d H:i:s"), "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                                                        $this->member_model->ins_side_options($dish_array);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if ($chk_flag == 0) {
                                                $chk_regular_size = $this->cron_model->get_regular_size($dish_id);
                                                $map_id = $chk_regular_size['map_id'];
                                                $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
                                                $cond_array = array("map_id" => $map_id);
                                                $this->cron_model->update_regular_size($size_array, $cond_array, $map_id, $dish_id);
                                            }
                                        }
                                    }

                                    if ($dish_flag == 0) {
                                        $dish_name = $contents->name;
                                        $desc = $contents->description;
                                        $price = $contents->price;
                                        $dish_id = $this->member_model->save_cat_dish_rest($dish_name, $desc, $price, $true_menu_name, $rest_id);
                                        $option_grp = $contents->option_groups;
                                        $chk_flag = 0;
                                        foreach ($option_grp as $option_g) {
                                            $chk_flag = 1;
                                            if ($option_g->type == "OPTION_CHOOSE") {
                                                $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
                                                $flaggg = 0;
                                                foreach ($option_g->options as $options) {
                                                    $flaggg = 1;
                                                    $name = $options->name;
                                                    $price_op = $options->price;
                                                    $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
                                                    $this->member_model->ins_size($size_array);
                                                }
                                                if ($flaggg != 1) {
                                                    $this->member_model->ins_size($size_array);
                                                }
                                            } elseif ($option_g->type == "OPTION_ADD") {
                                                $option_name_title = $option_g->text;
                                                if ($option_name_title == '') {
                                                    $option_name_title = 'options and sides';
                                                }
                                                $dish_array = array("restaurant_id" => $rid, "dish_item_id" => $dish_id, "option_name" => $option_name_title, "option_name_original" => $option_name_title);
                                                $dish_id3 = $this->member_model->ins_dish($dish_array);
                                                foreach ($option_g->options as $options) {
                                                    $name = $options->name;
                                                    $price_op1 = explode("+", $options->price);
                                                    $price_op = $price_op1[1];
                                                    $dish_array = array("option_id" => $dish_id3, "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                                    $this->member_model->ins_side_options($dish_array);
                                                }
                                            }
                                        }
                                        if ($chk_flag == 0) {
                                            $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
                                            $this->member_model->ins_size($size_array);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if ($menu_flag == 0) {
                    foreach ($menues->sections as $sec_key => $section) {
                        $true_menu_name = $menu_name;
                        $sec_menu_name = $menu_name;
                        if ($section->section_name != '') {
                            $sec_menu_name = $section->section_name;
                            $true_menu_name = $sec_menu_name;
                        }
                        foreach ($section->subsections as $sub_key => $subsection) {
                            $true_menu_name = $sec_menu_name;
                            $sub_menu_name = $sec_menu_name;
                            if ($subsection->subsection_name != '') {
                                $sub_menu_name = $subsection->subsection_name;
                                $true_menu_name = $sub_menu_name;
                            }
                            foreach ($subsection->contents as $cont_key => $contents) {
//                     echo '<pre>';   print_r($contents);
                                $dish_name = $contents->name;
                                $desc = $contents->description;
                                $price = $contents->price;
                                $dish_id = $this->member_model->save_cat_dish_rest($dish_name, $desc, $price, $true_menu_name, $rid);
                                $option_grp = $contents->option_groups;
                                $chk_flag = 0;
                                foreach ($option_grp as $option_g) {
                                    $chk_flag = 1;
                                    if ($option_g->type == "OPTION_CHOOSE") {
                                        $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
//                            
                                        $flaggg = 0;
                                        foreach ($option_g->options as $options) {
//                                    echo '<pre>';
                                            $flaggg = 1;
                                            $name = $options->name;
                                            $price_op = $options->price;
                                            $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
                                            $this->member_model->ins_size($size_array);
                                        }
                                        if ($flaggg != 1) {
                                            $this->member_model->ins_size($size_array);
                                        }
                                    } elseif ($option_g->type == "OPTION_ADD") {
                                        $option_name_title = $option_g->text;
                                        if ($option_name_title == '') {
                                            $option_name_title = 'options and sides';
                                        }
                                        $dish_array = array("restaurant_id" => $rid, "dish_item_id" => $dish_id, "option_name" => $option_name_title, "option_name_original" => $option_name_title);
                                        $dish_id3 = $this->member_model->ins_dish($dish_array);
                                        foreach ($option_g->options as $options) {
                                            $name = $options->name;
                                            $price_op1 = explode("+", $options->price);
                                            $price_op = $price_op1[1];
                                            $dish_array = array("option_id" => $dish_id3, "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                            $this->member_model->ins_side_options($dish_array);
                                        }
                                    }
                                }
                                if ($chk_flag == 0) {
                                    $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
                                    $this->member_model->ins_size($size_array);
                                }
                            }
//                    exit;
                        }
                    }
                }
            }
        }
        exit;
        foreach ($resdata as $res) {
            $rest_id = $res['restaurant_id'];
            $this->client_model->delete_old_locu($rest_id);
        }
    }

    function subscribtion_cron() {
        $this->load->model('cron_model');
        $this->load->model('user_model');
        $this->load->model('menu_model');

        $prev_recd = $this->cron_model->subscribtion_cron();
        if (sizeof($prev_recd) > 0) {
            $admin_mail = $this->menu_model->get_admin_mail();
            foreach ($prev_recd as $recd) {
                $get_user_email = $this->cron_model->get_user_email($recd['member_id']);
                $last_strip = $this->cron_model->get_last_stripid($recd['member_id']);
                $get_price = $this->cron_model->get_price($recd['package_recd']);
                $this->load->library('stripe_lib');
                $stripe = $this->client_model->getAdmin_strip();
                $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
                $customer_strip_id = $last_strip['strip_customer_id'];
                $owner_payment = array(
                    'amount' => bcmul($get_price['user_fee'], 100),
                    'currency' => 'usd',
                    'customer' => $customer_strip_id
                );
                $retData = $this->stripe_lib->chargeCustomer($owner_payment);
                $response = mysql_real_escape_string(serialize($retData));
                $total = bcdiv($retData->amount, 100, 3);

                $no = rand(000001, 999999);

                if (sizeof($retData->id) > 0) {

                    $arr12 = array("member_id" => $recd['member_id'],
                        "strip_customer_id" => $customer_strip_id,
                        "created_time" => date('Y-m-d H:i'),
                        "last_4digit" => $last_strip['last_4digit'],
                        "brand" => $last_strip['brand'],
                        //"order_id"=>$order_id,
                        "strip_order_id" => $retData->id,
                        "amount" => $get_price['user_fee']
                    );

                    $subscription_id = $this->client_model->insert_member_stripid_map($arr12);
                    if ($recd['sub_recd'] == '') {

                        $User_subscription_master = array("member_id" => $recd['member_id'],
                            "strip_id" => $subscription_id,
                            "package_id" => $recd['package_recd']);

                        $user_sub_id = $this->user_model->insert_1('User_subscription_master', $User_subscription_master);

                        $User_subscription_log = array("member_id" => $recd['member_id'],
                            "start_date" => date('Y-m-d'),
                            "End_date" => Date('Y-m-d', strtotime("+" . $get_price['durn'])),
                            "subscription_id" => $user_sub_id,
                            "strip_id" => $subscription_id
                        );
                        $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);
                        $get_restaurant = $this->cron_model->get_restaurant($recd['up_sub_recrd']);
                        foreach ($get_restaurant as $restaurant_list) {
                            //activated previous package/restaurants in current_restaurant table
                            $arr = array("member_id" => $recd['member_id'],
                                "strip_id" => $subscription_id,
                                "subscription_id" => $user_sub_id,
                                "updated_on" => date('Y-m-d H:i'),
                                "restaurant_id" => $restaurant_list['restaurant_id'],
                                "create_date" => date('Y-m-d H:i')
                            );
                            $this->cron_model->insert_1('current_restaurant', $arr);
                        }
                    } else {
                        $User_subscription_master = array(
                            "strip_id" => $subscription_id,
                        );
                        $this->cron_model->upddata_user_sub_master($recd['up_sub_recrd'], $User_subscription_master);


                        $User_subscription_log = array("member_id" => $recd['member_id'],
                            "start_date" => date('Y-m-d'),
                            "End_date" => Date('Y-m-d', strtotime("+" . $get_price['durn'])),
                            "subscription_id" => $recd['sub_recd'],
                            "strip_id" => $subscription_id
                        );
                        $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);


                        $get_restaurant = $this->cron_model->get_restaurant($recd['up_sub_recrd']);
                        foreach ($get_restaurant as $restaurant_list) {
                            //activated previous package/restaurants in current_restaurant table
                            $arr = array(
                                "create_date" => date('Y-m-d H:i'),
                                "strip_id" => $subscription_id,
                            );
                            $this->cron_model->upddata($restaurant_list['current_id'], $arr);
                        }
                    }
                    $user = $this->member_model->get_user($recd['member_id']);
                    ###########mail for subscription success##########
                    $emails = $this->member_model->get_email("Fulspoon_Subscription_Renewed");
                    $mail_contents = $emails['email_template'];
                    $mail_contents = str_replace("#baseurl#", base_url(), $mail_contents);
                    $mail_contents = str_replace("#NAME#", $user['first_name'] . " " . $user['last_name'], $mail_contents);
                    $mail_contents = str_replace("#SubscriberID#", $user_sub_id, $mail_contents);
                    $mail_contents = str_replace("#Date#", date("m-d-Y"), $mail_contents);
                    $mail_contents = str_replace("#BilledAmt#", $get_price['user_fee'], $mail_contents);
                    $mail_contents = str_replace("#PaymentMethod#", "Stripe Payment", $mail_contents);
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['crlf'] = PHP_EOL;
                    $config['newline'] = PHP_EOL;
                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->reply_to($admin_mail['email']);
                    $this->email->from($admin_mail['email']);
                    $this->email->subject($emails['email_subject']);
                    $this->email->to($user['email']);
                    $this->email->message($mail_contents);
                    $pjt_name = $admin_mail['project_name'];
                    $category = "Subscription";
                    $to_mail = $admin_mail['email'];
                    $from = $admin_mail['email'];
                    $subject2 = $emails['email_subject'];
                    $this->member_model->mail_sendgrid($to_mail, $subject2, $mail_contents, $category, $from, $pjt_name);


                    $this->menu_model->insert('notify_mail', array('email' => $_POST['fild_email'], 'zipcode' => $_POST['zip_fild']));
                } else {
                    //payment rejected
                    ###########mail for subscription failure##########

                    $emails = $this->member_model->get_email("Fulspoon_Subscription_Renewed");
                    $mail_contents = $emails['email_template'];
                    $mail_contents = str_replace("#baseurl#", base_url(), $mail_contents);
                    $mail_contents = str_replace("#NAME#", $user['first_name'] . " " . $user['last_name'], $mail_contents);
                    $mail_contents = str_replace("#SubscriberID#", $user_sub_id, $mail_contents);
                    $mail_contents = str_replace("#Date#", date("m-d-Y"), $mail_contents);
                    $mail_contents = str_replace("#BilledAmt#", $get_price['user_fee'], $mail_contents);
                    $mail_contents = str_replace("#PaymentMethod#", "Stripe Payment", $mail_contents);
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['crlf'] = PHP_EOL;
                    $config['newline'] = PHP_EOL;
                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->reply_to($admin_mail['email']);
                    $this->email->from($admin_mail['email']);
                    $this->email->subject($emails['email_subject']);
                    $this->email->to($user['email']);
                    $this->email->message($mail_contents);
                    $pjt_name = $admin_mail['project_name'];
                    $category = "Subscription";
                    $to_mail = $admin_mail['email'];
                    $from = $admin_mail['email'];
                    $subject2 = $emails['email_subject'];
                    $this->member_model->mail_sendgrid($to_mail, $subject2, $mail_contents, $category, $from, $pjt_name);
                }
            }
        } else {
            #no result found
        }
    }

    function subscribtion_cron_new() {

        $this->load->model('cron_model');
        $this->load->model('user_model');
        $this->load->model('menu_model');

        $prev_recd = $this->cron_model->subscribtion_cron_new();

        if (sizeof($prev_recd) > 0) {

            $admin_mail = $this->menu_model->get_admin_mail();

            foreach ($prev_recd as $recd) {

                $get_user_email = $this->cron_model->get_user_email($recd['member_id']);
                if ($recd['strip_id'] == '') {
                    $last_strip = $this->cron_model->get_last_stripid_new($recd['prev_strip']);
                } else {
                    $last_strip = $this->cron_model->get_last_stripid_new($recd['strip_id']);
                }

                //get price from package id
                $get_price = $this->cron_model->get_price($recd['package_recd']);
                $this->load->library('stripe_lib');
                $stripe = $this->client_model->getAdmin_strip();
                $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
                $customer_strip_id = $last_strip['strip_customer_id'];

//                echo $customer_strip_id;exit;

                $owner_payment = array(
                    'amount' => bcmul($get_price['user_fee'], 100),
                    'currency' => 'usd',
                    'customer' => $customer_strip_id
                );

                $retData = $this->stripe_lib->chargeCustomer_cron($owner_payment);
                $response = mysql_real_escape_string(serialize($retData));
                $total = bcdiv($retData->amount, 100, 3);

                $no = rand(000001, 999999);
                $user_sub_id = $recd['member_id'];
                if (sizeof($retData->id) > 0) {

                    $arr12 = array("member_id" => $recd['member_id'],
                        "strip_customer_id" => $customer_strip_id,
                        "created_time" => date('Y-m-d H:i'),
                        "last_4digit" => $last_strip['last_4digit'],
                        "brand" => $last_strip['brand'],
                        "strip_order_id" => $retData->id,
                        "amount" => $get_price['user_fee']
                    );

                    $subscription_id = $this->client_model->insert_member_stripid_map($arr12);

                    if ($recd['new_subscription_id'] == '') {
                        $User_subscription_log = array("member_id" => $recd['member_id'],
                            "start_date" => date('Y-m-d'),
                            "End_date" => Date('Y-m-d', strtotime("+" . $get_price['durn'])),
                            "subscription_id" => $recd['subscription_id'],
                            "strip_id" => $subscription_id
                        );
                        $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);




                        ################add new package####################
                        $message = "Your subscription to Fulspoon has been renewed on " . date("m-d-Y") . ". Billed amount : $" . $get_price['user_fee'] . " ";


                        $user = $this->member_model->get_user($recd['member_id']);
//                          die($message);
//                          
                        ###########mail for subscription success##########

                        $type = "Subscription";
                        $this->push_Notification("", $message, $type, "", $recd['member_id']);
                        $user_sub_id = $user['member_id'];
                        $emails = $this->member_model->get_email("Fulspoon_Subscription_Renewed");
                        $mail_contents = $emails['email_template'];
                        $mail_contents = str_replace("#baseurl#", base_url(), $mail_contents);
                        $mail_contents = str_replace("#NAME#", $user['first_name'] . " " . $user['last_name'], $mail_contents);
                        $mail_contents = str_replace("#SubscriberID#", $user_sub_id, $mail_contents);
                        $mail_contents = str_replace("#Date#", date("m-d-Y"), $mail_contents);
                        $mail_contents = str_replace("#BilledAmt#", $get_price['user_fee'], $mail_contents);
                        $mail_contents = str_replace("#PaymentMethod#", "Stripe Payment", $mail_contents);
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'utf-8';
                        $config['crlf'] = PHP_EOL;
                        $config['newline'] = PHP_EOL;
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->reply_to($admin_mail['email']);
                        $pjt_name = $admin_mail['project_name'];
                        //                    $mail_contents = " Your Next Subscription is success.";
                        $this->email->message($mail_contents);
                        $category = "Subscription";
                        $to_mail = $user['email'];
                        $from = $admin_mail['email'];
                        $subject2 = $emails['email_subject'];
                        $this->member_model->mail_sendgrid($to_mail, $subject2, $mail_contents, $category, $from, $pjt_name);


                        $this->menu_model->insert('notify_mail', array('email' => $_POST['fild_email'], 'zipcode' => $_POST['zip_fild']));

                        $arr = array(
                            "status" => 'N',
                        );
                        $this->cron_model->upddata_new($recd['new_subscription_id'], $arr);
                        ################end new package####################
                    } else {

                        if ($recd['new_package_id'] == $recd['package_id']) {
                            $User_subscription_log = array("member_id" => $recd['member_id'],
                                "start_date" => date('Y-m-d'),
                                "End_date" => Date('Y-m-d', strtotime("+" . $get_price['durn'])),
                                "subscription_id" => $recd['subscription_id'],
                                "strip_id" => $subscription_id
                            );
                            $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);

                            $get_restaurant = $this->cron_model->get_restaurant_new($recd['new_subscription_id']);

                            foreach ($get_restaurant as $restaurant_list) {
                                //activated previous package/restaurants in current_restaurant table
                                $arr = array("member_id" => $recd['member_id'],
                                    "strip_id" => $subscription_id,
                                    "subscription_id" => $recd['subscription_id'],
                                    "updated_on" => date('Y-m-d H:i'),
                                    "restaurant_id" => $restaurant_list['restaurant_id'],
                                    "create_date" => date('Y-m-d H:i')
                                );
                                $this->cron_model->insert_1('current_restaurant', $arr);
                            }
                        } else {
                            $User_subscription_master = array("member_id" => $recd['member_id'],
                                "strip_id" => $subscription_id,
                                "package_id" => $recd['package_recd']);

                            $user_sub_id = $this->user_model->insert_1('User_subscription_master', $User_subscription_master);

                            $User_subscription_log = array("member_id" => $recd['member_id'],
                                "start_date" => date('Y-m-d'),
                                "End_date" => Date('Y-m-d', strtotime("+" . $get_price['durn'])),
                                "subscription_id" => $user_sub_id,
                                "strip_id" => $subscription_id
                            );
                            $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);

                            $get_restaurant = $this->cron_model->get_restaurant_new($recd['new_subscription_id']);

                            foreach ($get_restaurant as $restaurant_list) {
                                //activated previous package/restaurants in current_restaurant table
                                $arr = array("member_id" => $recd['member_id'],
                                    "strip_id" => $subscription_id,
                                    "subscription_id" => $user_sub_id,
                                    "updated_on" => date('Y-m-d H:i'),
                                    "restaurant_id" => $restaurant_list['restaurant_id'],
                                    "create_date" => date('Y-m-d H:i')
                                );
                                $this->cron_model->insert_1('current_restaurant', $arr);
                            }
                        } $message = "Your subscription to Fulspoon has been renewed on " . date("m-d-Y") . ". Billed amount : " . $get_price['user_fee'] . " ";


                        $user = $this->member_model->get_user($recd['member_id']);
//                          die($message);
//                          
                        ###########mail for subscription success##########

                        $type = "Subscription";
                        $this->push_Notification("", $message, $type, "", $recd['member_id']);

                        $emails = $this->member_model->get_email("Fulspoon_Subscription_Renewed");
                        $mail_contents = $emails['email_template'];
                        $mail_contents = str_replace("#baseurl#", base_url(), $mail_contents);
                        $mail_contents = str_replace("#NAME#", $user['first_name'] . " " . $user['last_name'], $mail_contents);
                        $mail_contents = str_replace("#SubscriberID#", $user_sub_id, $mail_contents);
                        $mail_contents = str_replace("#Date#", date("m-d-Y"), $mail_contents);
                        $mail_contents = str_replace("#BilledAmt#", $get_price['user_fee'], $mail_contents);
                        $mail_contents = str_replace("#PaymentMethod#", "Stripe Payment", $mail_contents);
//                        $config['mailtype'] = 'html';
//                        $config['charset'] = 'utf-8';
//                        $config['crlf'] = PHP_EOL;
//                        $config['newline'] = PHP_EOL;
//                        $this->load->library('email');
//                        $this->email->initialize($config);
//                        $this->email->reply_to($admin_mail['email']);
                        $pjt_name = $admin_mail['project_name'];
//                        $this->email->from($admin_mail['email'], $pjt_name);
//                        $this->email->subject($emails['email_subject']);
//                        $this->email->to($user['email']);
//                        $this->email->bcc("aneeshg@newagesmb.com");
//                        //$data['prodect'] = $this->users_model->get_child_order($order_num);
//                        //$mail_contents = $this->load->view('email_temp.php',true, TRUE);
//                        #
//												//                    $mail_contents = " Your Next Subscription is success.";
//                        $this->email->message($mail_contents);
//                        $this->email->send();
//                          $pjt_name = $data_1['project_name'];
                        $category = "Subscription";
                        $to_mail = $user['email'];
                        $from = $admin_mail['email'];
                        $subject2 = $emails['email_subject'];
                        $this->member_model->mail_sendgrid($to_mail, $subject2, $mail_contents, $category, $from, $pjt_name);

                        $this->menu_model->insert('notify_mail', array('email' => $_POST['fild_email'], 'zipcode' => $_POST['zip_fild']));

                        $arr = array(
                            "status" => 'N',
                        );
                        $this->cron_model->upddata_new($recd['new_subscription_id'], $arr);
                    }
                } else {
//                    echo "haiii2";
                    //payment rejected
                    ###########mail for subscription failure##########
                    $emails = $this->member_model->get_email("Unable_to_Renew_Fulspoon_Subscription");
                    $message = "We were unable to renew your subscription due to problems with your payment method. Please correct or add a new payment method so we can renew your subscription. If you are unable to add a working payment method, the subscription will be suspended and you won't be able to place orders through Fulspoon until the issues is resolved.";
                    $type = "Subscription";
                    $this->push_Notification("", $message, $type, "", $recd['member_id']);
                    $mail_contents = $emails['email_template'];
                    $mail_contents = str_replace("#baseurl#", base_url(), $mail_contents);
                    $mail_contents = str_replace("#NAME#", $user['first_name'] . " " . $user['last_name'], $mail_contents);
                    $mail_contents = str_replace("#SubscriberID#", $user_sub_id, $mail_contents);
                    $mail_contents = str_replace("#Date#", date("m-d-Y"), $mail_contents);
                    $mail_contents = str_replace("#BilledAmt#", $get_price['user_fee'], $mail_contents);
                    $mail_contents = str_replace("#PaymentMethod#", "Stripe Payment", $mail_contents);

                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->reply_to($admin_mail['email']);
                    $pjt_name = $admin_mail['project_name'];
                    $this->email->from($admin_mail['email'], $pjt_name);
                    $this->email->subject($emails['email_subject']);
                    $this->email->to($user['email']);
                    //$data['prodect'] = $this->users_model->get_child_order($order_num);
                    //$mail_contents = $this->load->view('email_temp.php',true, TRUE);
                    #
					//                    $mail_contents = " Your Next Subscription is success.";
                    $this->email->message($mail_contents);
//                    $pjt_name = $data_1['project_name'];
                    $category = "Subscription";
                    $to_mail = $user['email'];
                    $from = $admin_mail['email'];
                    $subject2 = $emails['email_subject'];
                    $this->member_model->mail_sendgrid($to_mail, $subject2, $mail_contents, $category, $from, $pjt_name);
//                   
//                    $this->email->send();
                }
            }
        } else {

            #no result found
        }
    }

    function google_expire() {
        $this->load->model('member_model');
        $restaurent = $this->member_model->rest_master_select();
        $key1 = $this->member_model->get_key();
        $key = $this->member_model->get_key();
        foreach ($restaurent as $rest) {
            $g_key = $rest['google_id'];
            if ($g_key != '') {
                //$data['cuisines'] = $this->member_model->select_cuisine();
                $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$g_key&key=$key1";
                $ch1 = curl_init();
                curl_setopt($ch1, CURLOPT_URL, $uri_g);
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
                $sData = curl_exec($ch1);
                $response13 = json_decode($sData);
                $response_datas1 = $response13->result;
//                echo'<br/>sizeof :' . sizeof($response_datas1);
                $res_id = $rest['restaurant_id'];
                if (sizeof($response_datas1) == 0) {
                    if ($rest['expire_flag'] != 'Y') {
                        $this->member_model->update_expire_flag($res_id, "Y");
                        $this->change_subscription_auto($res_id);
                    }
                } else {
//                    echo '<pre>';
//                    print_r($response_datas1);
                    $lat1 = $response_datas1->geometry->location->lat;
                    $long1 = $response_datas1->geometry->location->lng;
                    $google_rest_name = $response_datas1->name;
//                    echo $response_datas1->formatted_address;
//                    ;
                    $address1 = $response_datas1->formatted_address;
                    if ($response_datas1->international_phone_number != '') {
                        $ph11 = str_replace("-", "", $response_datas1->international_phone_number);
                        $ph111 = str_replace(" ", "", $ph11);
                    } else {
                        $ph111 = '';
                    }
                    $address_array = $response_datas1->address_components;
                    foreach ($address_array as $address) {
                        if (in_array("postal_code", $address->types)) {
                            $postal_code = $address->long_name;
                        }
                    }
                    $rating = round($response_datas1->rating);
                    $price_level = $response_datas1->price_level;
//                    $pincode = $response_datas1->
                    $status = 'N';
                    $array = array("expire_flag" => $status, "formatted_address" => $address1, "restaurant_name" => $google_rest_name, "rating" => $rating, "pricing" => $price_level, "postal_code" => $postal_code, "contact_number" => $ph111);
                    $condition = array("restaurant_id" => $res_id);
                    $this->member_model->update_contract($array, $condition);
//                    $res_data = array();
                }
            }
        }
    }

    function change_subscription_auto($res_id) {
        $this->load->model('member_model');
        $this->load->model('user_model');
        $current_subscription = $this->member_model->find_sub($res_id);
//        echo '<pre>';
//        print_r($current_subscription);
        foreach ($current_subscription as $curr_sub) {
            $number_of_rest = $curr_sub['number_of_restaurants'] - 1;
            if ($number_of_rest > 0) {
                $sub_id = $curr_sub['subscription_id'];
                $member_id = $curr_sub['member_id'];
                $mynext_sub = $this->member_model->find_next_sub($number_of_rest);
                $rest_list = $this->member_model->get_rest_list($sub_id, $member_id, $res_id);
                $strip_id = $curr_sub['strip_id'];
                $User_subscription_master = array("member_id" => $member_id,
                    "package_id" => $mynext_sub['sub_package_id'],
                    "strip_id" => $strip_id);
                $new_sub_prize = $this->user_model->get_package_details($mynext_sub['sub_package_id']);
                $next_restaurant_status = $this->user_model->next_restaurant_status($member_id);
                if (isset($next_restaurant_status['member_id'])) {
                    $next_restaurant_status = $this->user_model->update_next_restaurant_status($next_restaurant_status['subscription_id']);
                }
                $user_sub_id = $this->user_model->insert_1('subscription_next', $User_subscription_master);
                foreach ($rest_list as $rest) {
                    $array_rest = array("member_id" => $member_id, "restaurant_id" => $rest['restaurant_id'], "next_subscription_id" => $user_sub_id, "updated_on" => date("Y-m-d"));
                    $this->member_model->save_res_user1($array_rest);
                }
            }
        }
    }

    function android() {
//        $key='AIzaSyDkVqzHrvhpdo_wk6E4jShb5_mnBpJZHTU';
        $key = $this->member_model->get_key();
//        echo $key;
//        exit;
//                echo'<script>alert('.$key.')</script>';
        $message = array('anchor' => '12',
            'message' => 's',
            'title' => 's',
            'type' => 's'
        );
        $url = 'https://android.googleapis.com/gcm/send';
        $registrationIDs = array('APA91bE6VkYa7wKhUi0wfk1IZ8g5MGhjcDpy4_h4Xfb0jSwZWdMnktfTwlcyJ2YbgcmtITcPEE9dDA-M5hqPTtZ_VN6ksF26oPa8-IOZe8YGJMttB3JMSN8aqrAWTxkg7mUFUgPJwLwW');
        $fields = array(
            'registration_ids' => $registrationIDs,
            'data' => $message,
        );

        define('GOOGLE_API_KEY', $key);

        $headers = array(
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
//            echo json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false)
            die('Curl failed ' . curl_error());

        curl_close($ch);
        var_dump($result);
    }

    function sendPushNotification_test($message = 'asdd') {
        $deviceToken = 'a07f714b59bd1ca9daf17459940276647acb023ad497708086067e4e0722e874';
        $development_mode = 'Y';
        $passphrase = 'newage';
        $activity_id = '122';
        $type = 'Order';
        ////////////////////////////////////////////////////////////////////////////////
        $ctx = stream_context_create();
//        var_dump($ctx);
        if ($development_mode == 'Y') {
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
        } else {
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-prod.pem');
        }
        if ($development_mode == 'Y') {
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        } else {
            $fp = stream_socket_client(
                    'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $badge_count = 0;
        $body['aps'] = array(
            'alert' => $message,
            'badge' => intval($badge_count),
            'id' => $activity_id,
            'type' => $type,
            'sound' => 'default'
        );
        $payload = json_encode($body);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        var_dump($result);
        fclose($fp);
    }

    function test_notif() {
        $development_mode = 'N';
        $passphrase = 'newage';
        $deviceToken = 'd432fdcb0b2aa956f8b8b8469ce565d200e993b7783d2b2bdc7a2a0aee395b11';
        ////////////////////////////////////////////////////////////////////////////////
        $ctx = stream_context_create();
//        var_dump($ctx);
        if ($development_mode == 'Y') {
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
        } else {
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-prod.pem');
        }
        if ($development_mode == 'Y') {
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        } else {
            $fp = stream_socket_client(
                    'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' => 'dknjhf',
            'badge' => intval(0),
            'id' => '325665',
            'type' => 'jdhsfuy',
            'sound' => 'default'
        );
        $payload = json_encode($body);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        print_r($result);
        fclose($fp);
    }

    function push_Notification($order_id, $message, $type, $title = '', $user_id = "") {
        $this->load->model('member_model');
        if ($order_id != "") {
            $user_details = $this->member_model->get_user_order($order_id);
        } else {
            $user_details = $this->member_model->get_user($user_id);
        }
        if ($user_details['push_notification'] == "Y") {
            if ($user_details['divice_type'] == 'Android') {
                $key = $this->member_model->get_key();
                $message = array('anchor' => $order_id,
                    'message' => $message,
                    'title' => $title,
                    'type' => $type,
                    'user_id' => $user_id
                );
                $url = 'https://android.googleapis.com/gcm/send';
                $registrationIDs = array($user_details['device_tocken']);
                $fields = array(
                    'registration_ids' => $registrationIDs,
                    'data' => $message,
                );

                define('GOOGLE_API_KEY', $key);

                $headers = array(
                    'Authorization:key=' . GOOGLE_API_KEY,
                    'Content-Type: application/json'
                );
//            echo json_encode($fields);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === false)
                    die('Curl failed ' . curl_error());

                curl_close($ch);
                return $result;
            }else {
                $development_mode = 'N';
                $passphrase = 'newage';
                ////////////////////////////////////////////////////////////////////////////////
                $ctx = stream_context_create();
                if ($development_mode == 'Y') {
                    stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
                } else {
                    stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-prod.pem');
                }
                if ($development_mode == 'Y') {
                    $fp = stream_socket_client(
                            'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                } else {
                    $fp = stream_socket_client(
                            'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                }
                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);
                $badge_count = 0;
                $body['aps'] = array(
                    'alert' => $message,
                    'badge' => intval($badge_count),
                    'id' => $order_id,
                    'type' => $type,
                    'sound' => 'default'
                );
                $payload = json_encode($body);
                $msg = chr(0) . pack('n', 32) . pack('H*', $user_details['device_tocken']) . pack('n', strlen($payload)) . $payload;
                $result = fwrite($fp, $msg, strlen($msg));
                fclose($fp);
            }
        }else {
            return FALSE;
        }
    }

    function check_login_push() {
        $data = $this->cron_model->login_push();
        foreach ($data as $ccc) {
            $this->checkContactNumber($ccc['member_id']);
        }
    }

    function checkContactNumber($user_id) {
        $this->load->model('user_model');
        $data = $this->user_model->checkContactNumber($user_id);
        if ($data['phone'] == '') {
            $message = 'Please add your phone number to your profile so that restaurants can contact you regarding your orders.';
            $this->cron_model->update1("member_master", array("push_ph_flag" => "Y"), array("member_id" => $user_id));
            $this->push_Notification("", $message, "Declined", "Declined", $user_id);
        }
    }

    function checkContactNumber_test($user_id = '783') {
        $this->load->model('user_model');
//        $data = $this->user_model->checkContactNumber($user_id);
//        echo '<pre>';
//        print_r($data);
//        exit;
//        if ($data['phone'] == '') {
        $message = 'Please add your phone number to your profile so that restaurants can contact you regarding your orders.';
        $this->push_Notification("2017", $message, "Order_is_declined", "Order");
//        }
    }

    function twilio_send_push(){
        $this->load->model('user_model');
        $this->load->model('order_model');
        $data = $this->user_model->get_dec_twilio_data();
        foreach ($data as $datas) {
            $rest_id = $this->order_model->get_rest_id($datas['order_id']);
            $restaurent_name = $this->user_model->get_rest($rest_id);
            $message = 'Your Order ' . $datas['order_ref_id'] . " with " . $restaurent_name . " has been  declined by restaurent admin.  Please contact restaurant for details. You have not been charged for this order.";
            $this->push_Notification($datas['order_id'], $message, "Declined", "Declined");
            $arr = array("twilioflag" => "N");
            $con = array("order_id" => $datas['order_id']);
            $this->user_model->update_twilio_order($arr, $con);
        }
    }

    function payment_refund($order_id, $amount = 0) {
        $this->load->model('user_model');
        $this->load->model('client_model');
        $this->load->library('stripe_lib');
        $data = $this->user_model->getOrderStripMap($order_id);
        if (sizeof($data) != 0) {
            $stripe = $this->client_model->getAdmin_strip();
            if (!$stripe) {
                return array('status' => 'error');
                exit;
            }
            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            $strip_order_id = $data[0]['strip_order_id'];
            $refund_amount = $data[0]['total_amount'];
            $charge_details = $this->stripe_lib->getChargeDetails($strip_order_id);

            if ($amount > 0 && $amount <= $data[0]['total_amount'])
                $refund_amount = $amount;

            $amount = bcmul($refund_amount, 100);

            if ($amount > $charge_details->amount && $charge_details->amount)
                $amount = $charge_details->amount;

            $reason = 'requested_by_customer';
            $refund_array = array("charge" => $strip_order_id, "amount" => $amount, "reason" => $reason);
            $resp = $this->stripe_lib->refundeCustomer($refund_array);
            $response = serialize($resp);

            if ($resp->id) {

                $arr = array("member_id" => $data[0]['member_id'],
                    "restaurant_id" => $data[0]['restaurant_id'],
                    "location_id" => $data[0]['location_id'],
                    "strip_customer_id" => $data[0]['strip_customer_id'],
                    "created_time" => date('Y-m-d H:i'),
                    "last_4digit" => $data[0]['last_4digit'],
                    "brand" => $data[0]['brand'],
                    "order_id" => $order_id,
                    "strip_order_id" => $resp->id,
                    "strip_response" => $response,
                    "payment_mode" => "refund");

                $this->db->insert('member_stripid_map', $arr);

                return array('status' => 'success', 'refund_amount' => $refund_amount);
            } else {
                return array('status' => 'error');
            }
        } else {
            return array('status' => 'error');
        }
    }

    function Twilo_call() {
        $this->load->model('user_model');
        require('twilio-php-master/Services/Twilio.php');
        $order_data = $this->cron_model->get_new_orders();
        $twilo_call_time = ($this->cron_model->get_config_details()) * 60;
        foreach ($order_data as $order) {
            $current_time = strtotime(date("Y-m-d H:i:s"));
             $order_created_time = strtotime($order['created_time']);
//            echo "current time".$current_time.", Order created time <br/>";
//          echo '<pre>';  print_r($order);
//            exit;
           
            
            if ($current_time - $order_created_time >= $twilo_call_time) {
                $twilo = $this->user_model->get_config1();
                $account_sid = $twilo['sid']['value'];
                $auth_token = $twilo['authtocken']['value'];
                $client = new Services_Twilio($account_sid, $auth_token);
//        print_r($client->account->calls);
//        exit;
                $owner_phone = $order['contact_number'];
                $phonenumber = $twilo['phonenumber']['value'];
                try {
                    // Initiate a new outbound call
                    $call = $client->account->calls->create(
                            $phonenumber, // The number of the phone initiating the call
                            $owner_phone, // The number of the phone receiving call
                            base_url() . 'client/message/' . $order['order_id'] // The URL Twilio will request when the call is answered
                    );
            echo 'Started call: ' . $call->sid;
//        print_r($call);
                } catch (Exception $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        }
    }

    function auto_decline() {
        $this->load->model('order_model');
        $this->load->model('user_model');
        $order_data = $this->cron_model->get_new_orders();
        $admin_time = ($this->cron_model->get_config_details_decline()) * 60;
        foreach ($order_data as $order) {
            $rest_id = $this->order_model->get_rest_id($order['order_id']);
            $restaurent_name = $this->user_model->get_rest($rest_id);
            $order_created_time = strtotime($order['created_time']);
            $current_time = strtotime(date("Y-m-d H:i:s"));
            if ($current_time - $order_created_time >= $admin_time) {
                $this->cron_model->update_decline($order['order_id']);
                $this->payment_refund($order['order_id']);
                $message = 'Your Order ' . $order['order_ref_id'] . " with " . $restaurent_name . " has been automatically declined due to no response from restaurant.  Please contact restaurant for details. You have not been charged for this order.";
                $type = "Order";
                $title = "Order_is_declined";
                $this->push_Notification($order['order_id'], $message, $type, $title);
            }
        }
    }

    function order_completion() {
        $this->load->model('order_model');
        $this->load->model('user_model');
        $order_data = $this->cron_model->get_accepted_orders();
        $data_1 = $this->member_model->admin_contratct();
        $msg = $this->member_model->get_email("Order_Ready_user");
        foreach ($order_data as $order) {
            $order_id = $order['order_id'];
            $rest_id = $this->order_model->get_rest_id($order_id);
            $restaurent_name = $this->user_model->get_rest($rest_id);
            if ($order['order_type'] == 'Delivery') {
                $delivery_time = $order['delivery_time'] * 60;
                if ($order['order_accepted_time'] != '') {
                    $order_accepted_time = strtotime($order['order_accepted_time']);
                    $current_time = strtotime(date("Y-m-d H:i:s"));
//                    echo "order accepted time : " . $order['order_accepted_time'] . "<br/>";
//                       echo "current time : ".date("Y-m-d H:i:s")."<br/>";
//                       $dif=$current_time - $order_accepted_time;
//                    echo $current_time."-".$order_accepted_time." =".$dif." >".$delivery_time."<br/>";
//                    exit;
                    if ($current_time - $order_accepted_time >= $delivery_time) {
                        $this->cron_model->update_complete($order_id);
                        $message = "Your order " . $order['order_ref_id'] . " with " . $restaurent_name . " is ready for " . $order['order_type'];
                        $type = "Order";
                        $title = "Order_is_ready";
                        $this->push_Notification($order_id, $message, $type, $title);
                        $name = $order['first_name'] . " " . $order['last_name'];
                        $order_type = $order['order_type'];
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'utf-8';
                        $config['crlf'] = PHP_EOL;
                        $config['newline'] = PHP_EOL;
                        $message2 = $msg['email_template'];
                        $subject2 = $msg['email_subject'];
                        $message2 = str_replace('#YEAR#', date('Y'), $message2);
                        $message2 = str_replace('#baseurl#', base_url(), $message2);
                        $message2 = str_replace('#name#', $name, $message2);
                        $message2 = str_replace('#order_num#', $order['order_ref_id'], $message2);
                        $message2 = str_replace('#order_time#', date("h:i:A", strtotime($order['created_time'])), $message2);
                        $message2 = str_replace('#restaurant_name#', $restaurent_name, $message2);
                        $message2 = str_replace('#order_num#', $order['order_ref_id'], $message2);
                        $message2 = str_replace('#order_type#', $order_type, $message2);
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $pjt_name = $data_1['project_name'];
                        $category = "Order";
                        $to_mail = $order['email'];
                        $from = $data_1['email'];
                        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                    }
                }
            } else {
                $delivery_time = $order['pickup_time'] * 60;
                if ($order['order_accepted_time'] != '') {
                    $order_accepted_time = strtotime($order['order_accepted_time']);
                    $current_time = strtotime(date("Y-m-d H:i:s"));
                    if ($current_time - $order_accepted_time >= $delivery_time) {
                        $this->cron_model->update_complete($order_id);
                        $message = "Your order " . $order['order_ref_id'] . " with " . $restaurent_name . " is ready for " . $order['order_type'];
                        echo $message;
                        $type = "Order_completed";
                        $title = "Order_completed";
                        $this->push_Notification($order_id, $message, $type, $title);
                        $name = $order['first_name'] . " " . $order['last_name'];
                        $order_type = $order['order_type'];
                        $message2 = $msg['email_template'];
                        $subject2 = $msg['email_subject'];
                        $message2 = str_replace('#baseurl#', base_url(), $message2);
                        $message2 = str_replace('#YEAR#', date('Y'), $message2);
                        $message2 = str_replace('#name#', $name, $message2);
                        $message2 = str_replace('#order_num#', $order['order_ref_id'], $message2);
                        $message2 = str_replace('#order_type#', $order_type, $message2);
                        $message2 = str_replace('#order_time#', date("h:i:A", strtotime($order['created_time'])), $message2);
                        $message2 = str_replace('#restaurant_name#', $restaurent_name, $message2);
                        $pjt_name = $data_1['project_name'];
                        $category = "Order";
                        $to_mail = $order['email'];
                        $from = $data_1['email'];
                        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                    }
                }
            }
        }
    }

}
