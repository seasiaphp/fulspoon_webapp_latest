<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of owners
 *
 * @author aneesh
 */
// error_reporting(E_ALL);
class owners extends MY_Controller {

    public function __construct() {

        parent::__construct();
//
//        if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'replimatic' || $_SERVER['PHP_AUTH_PW'] != 'replimatic!!123') {
//            header('WWW-Authenticate: Basic realm="MyProject"');
//            header('HTTP/1.0 401 Unauthorized');
//            die('Acces s Denied');
//        }
//


        $this->load->model('user_model');
        $this->load->model('order_model');
        $this->load->model('Member_model', 'member_model');
        $this->load->model('client_model');
        $this->load->model('menu_model');
//        date_default_timezone_set('GMT');
    }

//    static public
    function string_compare($str_a, $str_b) {
        $length = strlen($str_a);
        $length_b = strlen($str_b);

        $i = 0;
        $segmentcount = 0;
        $segmentsinfo = array();
        $segment = '';
        while ($i < $length) {
            $char = substr($str_a, $i, 1);
            if (strpos($str_b, $char) !== FALSE) {
                $segment = $segment . $char;
                if (strpos($str_b, $segment) !== FALSE) {
                    $segmentpos_a = $i - strlen($segment) + 1;
                    $segmentpos_b = strpos($str_b, $segment);
                    $positiondiff = abs($segmentpos_a - $segmentpos_b);
                    $posfactor = ($length - $positiondiff) / $length_b; // <-- ?
                    $lengthfactor = strlen($segment) / $length;
                    $segmentsinfo[$segmentcount] = array('segment' => $segment, 'score' => ($posfactor * $lengthfactor));
                } else {
                    $segment = '';
                    $i--;
                    $segmentcount++;
                }
            } else {
                $segment = '';
                $segmentcount++;
            }
            $i++;
        }

// PHP 5.3 lambda in array_map      
        $totalscore = array_sum(array_map(function($v) {
                    return $v['score'];
                }, $segmentsinfo));

        if ($totalscore <= 0.5) {
            if (stripos($str_a, $str_b) !== false) {
                $totalscore = 0.7;
            } elseif (stripos($str_b, $str_a) !== false) {
                $totalscore = 0.7;
            }
        }
        return $totalscore;
    }

    function index() {
        $data['rep_sub'] = $this->member_model->member_model_sub();
        $data['avg_order'] = $this->member_model->get_avg_model();
        $data['get_exptd_inc_mod'] = $this->member_model->get_exptd_inc_mod();
        $data['transactional_calc'] = $this->client_model->get_calcValue();
        $data['admin_val'] = $this->member_model->admin_val();
//        $this->load->view("owners_header");
        $this->load->view("oweners_index", $data);
        $this->load->view("footer_owner");
    }

    function signup() {
        $this->load->view("oweners_signup");
        $this->load->view("footer_owner");
    }

    function output_json($data) {
        echo "<pre>";
        print_r($data);
    }

    function contact_us() {
        $data_1 = $this->member_model->admin_contratct();
//        print_r($data_1);
        $address = urlencode($data_1['address']);
//        echo $address;
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response_a = json_decode($sData);
//        print_r($response_a);
//        exit;
        $data['contact'] = $data_1;
        $data['lat'] = $response_a->results[0]->geometry->location->lat;
        $data['long'] = $response_a->results[0]->geometry->location->lng;
        $this->load->view("contact_us", $data);
        $this->load->view("footer_owner");
    }

    function list_all($page_token = '') {
        session_start();
        if (is_numeric($_POST['address'])) {
            $_POST['address'] = $_POST['address'] . " us";
        }
        $_SESSION['hot_name'] = $_POST['name'];
        $_SESSION['add_name'] = $_POST['address'];
        ini_set('max_execution_time', 1820);
        set_time_limit(1000);
        $key1 = $this->member_model->get_key();
        $key = $this->member_model->get_key();
        if (strlen($page_token) == 0) {
            $address = urlencode($_POST['address']);
            $name = urlencode(str_replace("-", " ", $_POST['name']));
            $url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=US";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response);
//        print_r($response_a);
            $lat = $response_a->results[0]->geometry->location->lat;
            $long = $response_a->results[0]->geometry->location->lng;
            if ($name != '') {

                $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=500&types=restaurant&name=$name&sensor=false&key=$key";
//              echo $url1;
//              exit;
                $ch1 = curl_init();
                curl_setopt($ch1, CURLOPT_URL, $url1);
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
                $sData = curl_exec($ch1);
                $google_resp_data = array();
                $response1 = json_decode($sData);
                $response_data = $response1->results;
                if ($response1->status == "ZERO_RESULTS") {
                    $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&types=restaurant&name=$name&sensor=false&key=$key";
                    $ch1 = curl_init();
                    curl_setopt($ch1, CURLOPT_URL, $url1);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
                    $sData = curl_exec($ch1);
                    $google_resp_data = array();
                    $response1 = json_decode($sData);
                    $response_data = $response1->results;
//                     echo '<pre>';
//                    print_r($response_data);
//                    exit;
                }
            }
            if ($name == '' || $response1->status == "ZERO_RESULTS") {

                $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&rankby=distance&keyword=restaurant&sensor=false&key=$key";
//            &pagetoken=CpQCDQEAALpbxCN-feoKhyqFpTlf9QANXGTQ1O10Wcrl4nrVGYmKYvV0CSQBTqJxaDoiznZuiB7feYmIrRAXO5nI-VyCypfm0rGI2bygprgz46uuwptxffUM2_gStZJ88QGo-O-ymf_m8QeSQV9aDbzQ9lDxOtDeyyTLWbJKI0z3-q3W6QSgJN8Mkv6ZnS-RNeJ4gGCQ-xGzrxQaIbQ4iLdDE1057phHSwzWtdayz_k3IxLtlzdeIjlIF1LVlk5LlcKWF8H05uljVoddb0bPL0yXVegSv_JBmuyPIVDTJmLmaH7Y4Kr3IQColWGwkNj-j70Sv49y9-xugj4eczYo-r_HKlzHHF9sOI9b6BENRJuJQ8WHUhd_EhA01N_EjSl9_dhj7ULcU0VoGhQH9aXoJifuKXN_2sdEY5lQoJlX2A
//                     https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=33.8648303,-118.0800782&rankby=distance&keyword=sushi&sensor=false&key=AIzaSyAzMWcy2s8X167yaf1NxGYne0n1RSNE4gM
//            echo $url1;
                $ch1 = curl_init();
                curl_setopt($ch1, CURLOPT_URL, $url1);
                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
                $sData = curl_exec($ch1);
                $response1 = json_decode($sData);
                $page_token = $response1->next_page_token;
                $response_data = $response1->results;
            }
        } else {
//            echo'sd';
//            exit;
            $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=" . $page_token . "&sensor=false&key=" . $key;
//               $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&rankby=distance&keyword=restaurant&sensor=false&key=$key";
//            &pagetoken=CpQCDQEAALpbxCN-feoKhyqFpTlf9QANXGTQ1O10Wcrl4nrVGYmKYvV0CSQBTqJxaDoiznZuiB7feYmIrRAXO5nI-VyCypfm0rGI2bygprgz46uuwptxffUM2_gStZJ88QGo-O-ymf_m8QeSQV9aDbzQ9lDxOtDeyyTLWbJKI0z3-q3W6QSgJN8Mkv6ZnS-RNeJ4gGCQ-xGzrxQaIbQ4iLdDE1057phHSwzWtdayz_k3IxLtlzdeIjlIF1LVlk5LlcKWF8H05uljVoddb0bPL0yXVegSv_JBmuyPIVDTJmLmaH7Y4Kr3IQColWGwkNj-j70Sv49y9-xugj4eczYo-r_HKlzHHF9sOI9b6BENRJuJQ8WHUhd_EhA01N_EjSl9_dhj7ULcU0VoGhQH9aXoJifuKXN_2sdEY5lQoJlX2A
//                     https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=33.8648303,-118.0800782&rankby=distance&keyword=sushi&sensor=false&key=AIzaSyAzMWcy2s8X167yaf1NxGYne0n1RSNE4gM
//            echo $url1;
//            exit;
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $url1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response1 = json_decode($sData);
            $page_token = $response1->next_page_token;
            $response_data = $response1->results;
        }
        if (sizeof($response_data) > 1) {
//            echo $page_token;
//            exit;
//            $data['prvs_page_token']=$page_token;
            $data['page_token'] = $page_token;
            $data['response_data'] = $response_data;
            $data['key'] = $key;
            $data['key1'] = $key1;
            $this->load->view("restaurent_list", $data);
            $this->load->view("footer_owner");
        } elseif (sizeof($response_data) == 1) {
            $google_id = $response_data[0]->place_id;
            redirect("owners/restaurant_check/" . $google_id);
        } else {
            redirect("owners/no_result/");
        }
    }

    function no_result() {
        $this->load->view("no_result", $data);
        $this->load->view("footer_owner");
    }

    function test233() {
        $key = $this->member_model->get_key();
        $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=9.9700,76.2800&radius=500&types=atm&&key=$key";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        echo "<pre>";
        print_r($response_a);
        exit;
    }

    function test() {
        ini_set('max_execution_time', 1820);
        set_time_limit(1000);
        $_POST['address'] = "NY";
        $_POST['name'] = "Blue hill";
        $key1 = "AIzaSyDD5HCo8sdJmNrHi7zQCzYPJJCFFVPW-Mo";
        $key = "AIzaSyDH__WsFRkuVTMadzdrFz_hSPtKQWNKHvM";
        $address = str_replace(" ", "+", $_POST['address']);
        $name = urlencode($_POST['name']);
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=US";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
//        print_r($response_a);
        $lat = $response_a->results[0]->geometry->location->lat;
        $long = $response_a->results[0]->geometry->location->lng;
        $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=500&types=restaurant,food,hotel&name=$name&key=$key";
//       echo $url1;
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $url1);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $google_resp_data = array();
        $response1 = json_decode($sData);
        $response_data = $response1->results;
        if ($response1->status == "ZERO_RESULTS") {
            $url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=5000&types=restaurant&name=$name&key=$key";
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $url1);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response1 = json_decode($sData);
            $response_data = $response1->results;
        }
        $data['response_data'] = $response_data;
        $data['key'] = $key;
        $data['key1'] = $key1;
        $this->load->view("restaurent_list", $data);
//        $a = 0;
//
//        foreach ($response_data as $res_data) {
//            $lat1 = $res_data->geometry->location->lat;
//            $long1 = $res_data->geometry->location->lng;
//            $google_id = $res_data->place_id;
//            $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$key";
//            $ch1 = curl_init();
//            curl_setopt($ch1, CURLOPT_URL, $uri);
//            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
//            $sData = curl_exec($ch1);
//            $response13 = json_decode($sData);
//            $response_datas1 = $response13->result;
//            $res_data->google_place_result = $response_datas1;
//            $website_url = $res_data->google_place_result->website;
//            $location = str_replace("+", "", urlencode(end(explode(",", $res_data->vicinity))));
//            $name3 = str_replace(" ", "%20", $res_data->name);
////            echo $name3;
//            $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&has_menu=TRUE&radius=100&locality=$location&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//            $ch_locu = curl_init();
//            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//            $sData3 = curl_exec($ch_locu);
//            $response_locu = json_decode($sData3);
//            if (sizeof($response_locu->objects) == 0) {
//                unset($response_locu);
//                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&locality=$location&has_menu=TRUE&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//                $ch_locu = curl_init();
//                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//                $sData3 = curl_exec($ch_locu);
//                $response_locu = json_decode($sData3);
//            }
//            if (sizeof($response_locu->objects) == 0) {
//                unset($response_locu);
//                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&has_menu=TRUE&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//                $ch_locu = curl_init();
//                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//                $sData3 = curl_exec($ch_locu);
//                $response_locu = json_decode($sData3);
//            }
//            if (sizeof($response_locu->objects) == 0) {
//                unset($response_locu);
//                $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&locality=$location&has_menu=TRUE&radius=20&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//                $ch_locu = curl_init();
//                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//                $sData3 = curl_exec($ch_locu);
//                $response_locu = json_decode($sData3);
//            }
//            if (sizeof($response_locu->objects) == 0) {
//                unset($response_locu);
//                $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&radius=100&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//                $ch_locu = curl_init();
//                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//                $sData3 = curl_exec($ch_locu);
//                $response_locu = json_decode($sData3);
//            }
//            if (sizeof($response_locu->objects) == 0) {
//                unset($response_locu);
//                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//                $ch_locu = curl_init();
//                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//                $sData3 = curl_exec($ch_locu);
//                $response_locu = json_decode($sData3);
//            }
//            if (sizeof($response_locu->objects) == 0) {
//                unset($response_locu);
//                $url_locu = "https://api.locu.com/v1_0/venue/search/?website_url=$website_url&category=restaurant&api_key=9cc4991662b881c0bc4b89d63d151430f725dd5a";
//                $ch_locu = curl_init();
//                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
//                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
//                $sData3 = curl_exec($ch_locu);
//                $response_locu = json_decode($sData3);
//            }
//            if (sizeof($response_locu->objects) > 1) {
//                unset($near);
//                foreach ($response_locu->objects as $key_locu13 => $locu_152) {
////                    echo "<pre>";
////                    print_r($locu_152);
//                    $latit = $locu_152->lat;
//                    $longit = $locu_152->long;
//                    $theta = $long1 - $longit;
//                    $dist = sin(deg2rad($lat1)) * sin(deg2rad($latit)) + cos(deg2rad($lat1)) * cos(deg2rad($latit)) * cos(deg2rad($theta));
//                    $dist = acos($dist);
//                    $dist = rad2deg($dist);
//                    $miles = $dist * 60 * 1.1515;
//                    $unit = strtoupper($unit);
//
//                    if ($unit == "K") {
//                        $return1234 = ($miles * 1.609344);
//                    } else if ($unit == "N") {
//                        $return1234 = ($miles * 0.8684);
//                    } else {
//                        $return1234 = $miles;
//                    }
//                    $return1234 = abs($return1234);
////                    echo "<br/>";
//                    if (!isset($near)) {
//                        $near = $return1234;
//                    }
////echo "distance :".$return1234." near: ". $near."<br/>";
//                    if ($return1234 <= $near) {
//                        $near = $return1234;
//                        unset($response_locu_original);
//                        $response_locu_original = $locu_152;
//                        $response_locu_original->distanceBetweenLatLong = $return1234;
////                        echo "<pre>";
////                        print_r($response_locu_original);
//                    }
//                }
//            } else {
////             
//                $response_locu_original = $response_locu->objects[0];
////                echo "<pre>";
////                print_r($response_locu);
//            }
//
//            $locu_id = $response_locu_original->id;
//
//            $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
////            $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
////         \"api_key\" : \"$api_key\",
//// \"fields\" : [\"name\", \"menus\" ],
////  \"venue_queries\" : [
////   { \"locu_id\":\"$locu_id\" }
////    
////  ]
////}'";
//////        echo $uri;
////            $c_pid = exec($uri);
////            $c_pid = json_decode($c_pid);
////            $response_locu_original->locu_menu = $c_pid->venues;
//            $uri4 = "curl -X POST https://api.locu.com/v2/venue/search -d '{
//         \"api_key\" : \"$api_key\",
// \"fields\" : [\"name\", \"extended\",\"delivery\"],
//  \"venue_queries\" : [
//   { \"locu_id\":\"$locu_id\" }
//    
//  ]
//}'";
//            $c_pid3 = exec($uri4);
//            $c_pid3 = json_decode($c_pid3);
//            $response_locu_original->locu_extend = $c_pid3;
////            } 
//            $res_data->locu_data = $response_locu_original;
//            $a++;
//            $google_resp_data[] = $res_data;
//        }
//        $data['response'] = $google_resp_data;
//        $data['google'] = $response_data;
//        $data['key'] = $key;
//        $data['key1'] = $key1;
////        print_r($data);
////        exit;
//        $this->load->view("oweners_api-details", $data);
    }

    function check_list() {
        $this->load->view("restaurent_list");
    }

    function rest_test() {
        $key1 = $this->member_model->get_key();
        $key = "AIzaSyDH__WsFRkuVTMadzdrFz_hSPtKQWNKHvM";
//        $data['cuisines'] = $this->member_model->select_cuisine();
        $google_id = 'ChIJ68J3tfpYwokRz2cAXs-5aC4';
        $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$key1";
    }

    function oweners_contract($id) {
        $key1 = $this->member_model->get_key();
//        $key = $this->member_model->get_key();
        $google_id = $this->member_model->get_google_id_con($id);
        $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$key1";
//        echo $uri_g;
//        exit;
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $uri_g);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response13 = json_decode($sData);
        $response_datas1 = $response13->result;
//        echo $response_datas1->name;
//         echo $response_datas1->formatted_address;
//        print_r($response_datas1);
//        exit;
        $get_all_restaurant_detail = $this->member_model->get_all_restaurant_detail($id);

        $data['data'] = $this->member_model->admin_contratct();
        $data['client_name'] = $get_all_restaurant_detail['restaurant_name'];
        $data['client_address'] = $get_all_restaurant_detail['formatted_address'];
        $data['id'] = $id;
        $this->load->view("oweners_contract", $data);
        $this->load->view("footer_owner");
    }

    function faq() {
        $data['faq'] = $this->member_model->faq();
        $this->load->view("faq", $data);
        $this->load->view("footer_owner");
    }

    function terms_condition() {
        $data['terms'] = $this->member_model->terms_of_use();
        $this->load->view("terms_condition", $data);
        $this->load->view("footer_owner");
    }

    function google_test() {
        $this->load->view("go_google");
    }

    function test_mail_sendgrid() {


        $url = 'https://api.sendgrid.com/';
        $user = 'fulspoon';
        $pass = 'sf$88&&SG';

        $json_string = array(
            'to' => array(
                'aneeshg@newagesmb.com', 'bibinv@newagesmb.com', 'nitu@newagesmb.com', 'hermioneg2105@gmail.com'
            ),
            'category' => 'test_category'
        );


        $params = array(
            'api_user' => $user,
            'api_key' => $pass,
            'x-smtpapi' => json_encode($json_string),
            'to' => 'aneeshg@newagesmb.com',
            'subject' => 'testing from curl',
            'html' => 'Fulspoon',
//            'text' => 'bets',
            'from' => 'info@fulspoon.com',
        );


        $request = $url . 'api/mail.send.json';
        $sendgrid_apikey = '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';
// Generate curl request
        $session = curl_init($request);
// Tell curl to use HTTP POST
        curl_setopt($session, CURLOPT_POST, true);
// Tell curl that this is the body of the POST
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
// Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
// Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// obtain response
        $response = curl_exec($session);
        curl_close($session);

// print everything out
        print_r($response);
    }

    function goole_test() {
        $key1 = $this->member_model->get_key();
        $key = $this->member_model->get_key();
//        $g_key = $_POST['google_key'];
        $g_key = "ChIJvV_FuU0VkFQRCPtOJX1_2OY";
//$data['cuisines'] = $this->member_model->select_cuisine();
//        $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$g_key&key=$key1";
        $uri_g = "https://maps.googleapis.com/maps/api/timezone/json?location=47.6134818,-122.3461115&key=$key1";
//        echo $uri_g;
//        exit;
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $uri_g);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response13 = json_decode($sData);
        $response_datas1 = $response13->result;
//        $lat1 = $response_datas1->geometry->location->lat;
//        $long1 = $response_datas1->geometry->location->lng;
//        $google_rest_name = $response_datas1->name;
        $res_data = array();
        echo '<pre>';
        print_r($response_datas1);
        exit;
        $res_data['google_place_result'] = $response_datas1;
        $website_url = $res_data['google_place_result']->website;
        $location = str_replace("+", "", urlencode(end(explode(",", $res_data->vicinity))));
        if ($location == '') {
            $location = $response_datas1->address_components[2]->long_name;
        }
        $address_array = $response_datas1->address_components;
        foreach ($address_array as $address) {
            if (in_array("postal_code", $address->types)) {
                $postal_code = $address->long_name;
            }
        }
        $name3 = str_replace(" ", "%20", $res_data['google_place_result']->name);
//            echo $name3;
        $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&radius=100&locality=$location&category=restaurant&api_key=3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
//       echo $url_locu."<br/>";
//       exit;
        $ch_locu = curl_init();
        curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
        curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
        $sData3 = curl_exec($ch_locu);
        $response_locu = json_decode($sData3);
        if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&locality=$location&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
//            echo $url_locu;
//            exit;
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        }
        if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&has_menu=TRUE&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        } if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        }
        if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&locality=$location&has_menu=TRUE&radius=20&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        }
        if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&radius=100&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        }
        if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        }
        if (sizeof($response_locu->objects) == 0) {
            unset($response_locu);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?website_url=$website_url&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
        }
        if (sizeof($response_locu->objects) > 1) {
            unset($near);
            foreach ($response_locu->objects as $key_locu13 => $locu_152) {
                $latit = $locu_152->lat;
                $longit = $locu_152->long;
                $theta = $long1 - $longit;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($latit)) + cos(deg2rad($lat1)) * cos(deg2rad($latit)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);
                if ($unit == "K") {
                    $return1234 = ($miles * 1.609344);
                } else if ($unit == "N") {
                    $return1234 = ($miles * 0.8684);
                } else {
                    $return1234 = $miles;
                }
                $return1234 = abs($return1234);
//                    echo "<br/>";
                if (!isset($near)) {
                    $near = $return1234;
                }
                if ($return1234 <= $near) {
                    $near = $return1234;
                    unset($response_locu_original);
                    $response_locu_original = $locu_152;
                    $response_locu_original->distanceBetweenLatLong = $return1234;
//                        echo "<pre>";
//                        print_r($response_locu_original);
                }
            }
        } else {
//             
            $response_locu_original = $response_locu->objects[0];
//                echo "<pre>";
//                print_r($response_locu);
        }
//        echo'<pre>';
//        print_r($response_locu_original);
//        exit;
        $locu_name = $response_locu_original->name;
        $match_val = $this->string_compare($google_rest_name, $locu_name);
        if ($match_val > 1) {
            $match_val = $this->string_compare($locu_name, $google_rest_name);
        }
        echo $locu_name . " Google name" . $google_rest_name . "<br/>";
        echo $match_val . "<br/>";
        $score = levenshtein($locu_name, $google_rest_name);
//        exit;
        $locu_id = $response_locu_original->id;
        $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        $c_pid = json_decode($c_pid);
        $response_locu_original->locu_menu = $c_pid->venues;
        $uri4 = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"extended\",\"delivery\"],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
    
  ]
}'";
        $c_pid3 = exec($uri4);
        $c_pid3 = json_decode($c_pid3);
        $response_locu_original->locu_extend = $c_pid3;
        echo '<pre>';
        print_r($response_locu_original->locu_menu);
//            } 
//        $res_data['locu_data'] = $response_locu_original;
//        $a++;
//        $google_resp_data[] = $res_data;
//
//        $data['response'] = $google_resp_data;
//        $data['google'] = $res_data;
//        $data['key'] = $key;
//        $data['key1'] = $key1;
//
//        $this->load->view("google_place_list", $data);
//        $this->load->view("footer_owner");
    }

    function update_db() {
        $key1 = $this->member_model->get_key();

        $data = $this->db->query("select * from restaurant_master");
        $result = $data->result_array();
        foreach ($result as $res) {
            $google_id = $res['google_id'];
            $res_id = $res['restaurant_id'];
            $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$key1";
            echo $uri_g;
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $uri_g);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response13 = json_decode($sData);
            $response_datas1 = $response13->result;
            $lat1 = $response_datas1->geometry->location->lat;
            $long1 = $response_datas1->geometry->location->lng;
            $google_rest_name = $response_datas1->name;
            $this->db->update("restaurant_master", array("restaurant_name" => $google_rest_name, "lat" => $lat1, "long" => $long1), array("restaurant_id" => $res_id));
//            echo $this->db->last_query() . "<br/>";
        }
    }

    function check_res_exist($id) {
        $data = $this->member_model->check_res_exist($id);
        if ($data == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function restaurant_exist() {
        $this->load->view('already_exist');
    }

    function restaurant_check($google_id) {
        if ($this->check_res_exist($google_id) == FALSE) {
            redirect('owners/restaurant_exist');
        } else {
            $key1 = $this->member_model->get_key();
            $key = $this->member_model->get_key();
            $data['cuisines'] = $this->member_model->select_cuisine();
            $uri_g = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$key1";
//        echo $uri_g;
//        exit;
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $uri_g);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
            $sData = curl_exec($ch1);
            $response13 = json_decode($sData);
            $response_datas1 = $response13->result;
            $lat1 = $response_datas1->geometry->location->lat;
            $long1 = $response_datas1->geometry->location->lng;
            $google_rest_name = $response_datas1->name;
            $res_data = array();
            $res_data['google_place_result'] = $response_datas1;
            $website_url = $res_data['google_place_result']->website;
            $location = str_replace("+", "", urlencode(end(explode(",", $res_data->vicinity))));
            if ($location == '') {
                $location = $response_datas1->address_components[2]->long_name;
            }

            $address_array = $response_datas1->address_components;
            foreach ($address_array as $address) {
                if (in_array("postal_code", $address->types)) {
                    $postal_code = $address->long_name;
                    $res_data['postal_code'] = $postal_code;
                }
            }

            $name3 = str_replace(" ", "%20", $res_data['google_place_result']->name);
            $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&has_menu=TRUE&radius=100&locality=$location&category=restaurant&api_key=3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
            $ch_locu = curl_init();
            curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
            curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
            $sData3 = curl_exec($ch_locu);
            $response_locu = json_decode($sData3);
            if (sizeof($response_locu->objects) == 0) {
                unset($response_locu);
                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&locality=$location&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
//            echo $url_locu;
                $ch_locu = curl_init();
                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
                $sData3 = curl_exec($ch_locu);
                $response_locu = json_decode($sData3);
            }
            if (sizeof($response_locu->objects) == 0) {
                unset($response_locu);
                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&location=$lat1%2C$long1&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
                $ch_locu = curl_init();
                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
                $sData3 = curl_exec($ch_locu);
                $response_locu = json_decode($sData3);
            }
            if (sizeof($response_locu->objects) == 0) {
                unset($response_locu);
                $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&locality=$location&radius=20&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
                $ch_locu = curl_init();
                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
                $sData3 = curl_exec($ch_locu);
                $response_locu = json_decode($sData3);
            }
            if (sizeof($response_locu->objects) == 0) {
                unset($response_locu);
                $url_locu = "https://api.locu.com/v1_0/venue/search/?location=$lat1%2C$long1&radius=100&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
                $ch_locu = curl_init();
                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
                $sData3 = curl_exec($ch_locu);
                $response_locu = json_decode($sData3);
            }
            if (sizeof($response_locu->objects) == 0) {
                unset($response_locu);
                $url_locu = "https://api.locu.com/v1_0/venue/search/?name=$name3&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
                $ch_locu = curl_init();
                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
                $sData3 = curl_exec($ch_locu);
                $response_locu = json_decode($sData3);
            }
            if (sizeof($response_locu->objects) == 0) {
                unset($response_locu);
                $url_locu = "https://api.locu.com/v1_0/venue/search/?website_url=$website_url&category=restaurant&api_key=de1d60cb51b6054b1021a211b272d39dbd256eb8";
                $ch_locu = curl_init();
                curl_setopt($ch_locu, CURLOPT_URL, $url_locu);
                curl_setopt($ch_locu, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_locu, CURLOPT_ENCODING, 'gzip');
                $sData3 = curl_exec($ch_locu);
                $response_locu = json_decode($sData3);
            }
            if (sizeof($response_locu->objects) > 1) {
                unset($near);
                foreach ($response_locu->objects as $key_locu13 => $locu_152) {
                    $latit = $locu_152->lat;
                    $longit = $locu_152->long;
                    $theta = $long1 - $longit;
                    $dist = sin(deg2rad($lat1)) * sin(deg2rad($latit)) + cos(deg2rad($lat1)) * cos(deg2rad($latit)) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $unit = strtoupper($unit);
                    if ($unit == "K") {
                        $return1234 = ($miles * 1.609344);
                    } else if ($unit == "N") {
                        $return1234 = ($miles * 0.8684);
                    } else {
                        $return1234 = $miles;
                    }
                    $return1234 = abs($return1234);
//                    echo "<br/>";
                    if (!isset($near)) {
                        $near = $return1234;
                    }
                    if ($return1234 <= $near) {
                        $near = $return1234;
                        unset($response_locu_original);
                        $response_locu_original = $locu_152;
                        $response_locu_original->distanceBetweenLatLong = $return1234;
//                        echo "<pre>";
//                        print_r($response_locu_original);
                    }
                }
            } else {
//             
                $response_locu_original = $response_locu->objects[0];
//                echo "<pre>";
//                print_r($response_locu);
            }
//        echo'<pre>';
//        print_r($response_locu_original);
//        exit;
            $locu_name = $response_locu_original->name;
            $match_val = $this->string_compare($google_rest_name, $locu_name);
            $score = levenshtein($google_rest_name, $locu_name);
//        echo $match_val;
//        exit;
            if ($match_val > 1) {
                $match_val = $this->string_compare($locu_name, $google_rest_name);
            }
            if ($match_val >= 0.5 && $score <= 10) {
                $locu_id = $response_locu_original->id;
                $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
                $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
    
  ]
}'";
//        echo $uri;
                $c_pid = exec($uri);
                $c_pid = json_decode($c_pid);
                $response_locu_original->locu_menu = $c_pid->venues;
                $uri4 = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"extended\",\"delivery\"],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
    
  ]
}'";
                $c_pid3 = exec($uri4);
                $c_pid3 = json_decode($c_pid3);
                $response_locu_original->locu_extend = $c_pid3;
            } else {
                unset($response_locu_original);
                $response_locu_original = array();
            }
//            } 
            $res_data['locu_data'] = $response_locu_original;
            $a++;
            $google_resp_data[] = $res_data;
            $data['cusine_config'] = $this->member_model->get_cusine_config();
            $data['rating_config'] = $this->member_model->get_rating_config();
            $data['price_config'] = $this->member_model->get_price_config();
            $data['feature_config'] = $this->member_model->get_feature_config();
            $data['response'] = $google_resp_data;
            $data['google'] = $res_data;
            $data['key'] = $key;
            $data['key1'] = $key1;

            $this->load->view("google_place_list", $data);
            $this->load->view("footer_owner");
        }
    }

    function test_locu_api54() {
//        $id = "12b7a12c8be41b2714d6";  //Zylo
        $id = "a9accaf89e46eeec7215";
        error_reporting(E_ALL);
        $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$id\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        echo '<pre>';
        print_r(json_decode($c_pid));
    }

    function menu_list_test() {
        $locu_id = "a9accaf89e46eeec7215";
        $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\", \"extended\",\"delivery\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
    
  ]
}'";
// echo $uri;
        $c_pid = exec($uri);
        $c_pid = json_decode($c_pid);
//        echo'<pre>';
//        print_r($c_pid);
//        exit;
        foreach ($c_pid->venues[0]->menus as $menues) {
            $menu_name = $menues->menu_name;
            $true_menu_name = $menu_name;

//            echo sizeof($menues->sections);
            foreach ($menues->sections as $sec_key => $section) {
                $true_menu_name = $menu_name;
                $sec_menu_name = $menu_name;
                if ($section->section_name != '') {
                    $sec_menu_name = $section->section_name;
                    $true_menu_name = $sec_menu_name;
                }
//                echo $true_menu_name."<br/>";
                foreach ($section->subsections as $sub_key => $subsection) {
                    $true_menu_name = $sec_menu_name;
                    $sub_menu_name = $sec_menu_name;
                    if ($subsection->subsection_name != '') {
                        $sub_menu_name = $subsection->subsection_name;
                        $true_menu_name = $sub_menu_name;
                    }
                    echo $true_menu_name . "<br/>";
                    foreach ($subsection->contents as $cont_key => $contents) {
//                     echo '<pre>';   print_r($contents);
                        $dish_name = $contents->name;
                        $desc = $contents->description;
                        $price = $contents->price;
//                        $dish_id = $this->member_model->save_cat_dish_rest($dish_name, $desc, $price, $true_menu_name, $rid);
                        $option_grp = $contents->option_groups;
                        $chk_flag = 0;
                        foreach ($option_grp as $option_g) {
                            $chk_flag = 1;
                            if ($option_g->type == "OPTION_CHOOSE") {
                                $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
//                                $this->member_model->ins_size($size_array);
                                $option_name_title = $option_g->text;
                                if ($option_name_title == '') {
                                    $option_name_title = 'options and sides';
                                }
//                                $dish_array = array("restaurant_id" => $rid, "dish_item_id" => $dish_id, "option_name" => $option_name_title, "option_name_original" => $option_name_title);
//                                $dish_id3 = $this->member_model->ins_dish($dish_array);
                                foreach ($option_g->options as $options) {
                                    $name = $options->name;
                                    $price_op1 = explode("+", $options->price);
                                    $price_op = $price_op1[1];
                                    $dish_array = array("option_id" => $dish_id3, "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                }
                            } elseif ($option_g->type == "OPTION_ADD") {
                                foreach ($option_g->options as $options) {
                                    echo '<pre>';

                                    $name = $options->name;
                                    $price_op = $options->price;
                                    $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
//                                    $this->member_model->ins_size($size_array);
                                }
                            }
                        }
                        if ($chk_flag == 0) {
                            $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
//                            $this->member_model->ins_size($size_array);
                        }
                    }
//                    exit;
                }
            }
        }
//        redirect("owners/category/$rid");
    }

    function menu_list($rid) {
        $locu_id = $this->member_model->get_locu_id($rid);
        $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\", \"extended\",\"delivery\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$locu_id\" }
    
  ]
}'";
// echo $uri;
        $c_pid = exec($uri);
        $c_pid = json_decode($c_pid);
//        echo'<pre>';
//        print_r($c_pid);
//        exit;
        foreach ($c_pid->venues[0]->menus as $menues) {
            $menu_name = $menues->menu_name;
            $true_menu_name = $menu_name;

//            echo sizeof($menues->sections);
            foreach ($menues->sections as $sec_key => $section) {
                $true_menu_name = $menu_name;
                $sec_menu_name = $menu_name;
                if ($section->section_name != '') {
                    $sec_menu_name = $section->section_name;
                    $true_menu_name = $sec_menu_name;
                }
                foreach ($section->subsections as $sub_key => $subsection) {
                    $true_menu_name = $sec_menu_name;
                    $sub_menu_name = $sec_menu_name;
                    if ($subsection->subsection_name != '') {
                        $sub_menu_name = $subsection->subsection_name;
                        $true_menu_name = $sub_menu_name;
                    }
                    foreach ($subsection->contents as $cont_key => $contents) {
//                     echo '<pre>';   print_r($contents);
                        $dish_name = $contents->name;
                        $desc = $contents->description;
                        $price = $contents->price;
                        $dish_id = $this->member_model->save_cat_dish_rest($dish_name, $desc, $price, $true_menu_name, $rid);
                        $option_grp = $contents->option_groups;
                        $chk_flag = 0;
                        foreach ($option_grp as $option_g) {
                            $chk_flag = 1;
                            if ($option_g->type == "OPTION_CHOOSE") {
                                $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
//                            
                                $flaggg = 0;
                                foreach ($option_g->options as $options) {
//                                    echo '<pre>';
                                    $flaggg = 1;
                                    $name = $options->name;
                                    $price_op = $options->price;
                                    $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
                                    $this->member_model->ins_size($size_array);
                                }
                                if ($flaggg != 1) {
                                    $this->member_model->ins_size($size_array);
                                }
                            } elseif ($option_g->type == "OPTION_ADD") {
                                $option_name_title = $option_g->text;
                                if ($option_name_title == '') {
                                    $option_name_title = 'options and sides';
                                }
                                $dish_array = array("restaurant_id" => $rid, "dish_item_id" => $dish_id, "option_name" => $option_name_title, "option_name_original" => $option_name_title);
                                $dish_id3 = $this->member_model->ins_dish($dish_array);
                                foreach ($option_g->options as $options) {
                                    $name = $options->name;
                                    $price_op1 = explode("+", $options->price);
                                    $price_op = $price_op1[1];
                                    $dish_array = array("option_id" => $dish_id3, "side_item" => $name, "price" => $price_op, "price_original" => $price_op, "side_item_original" => $name);
                                    $this->member_model->ins_side_options($dish_array);
                                }
//                                foreach ($option_g->options as $options) {
//                                    echo '<pre>';
//
//                                    $name = $options->name;
//                                    $price_op = $options->price;
//                                    $size_array = array("item_id" => $dish_id, "size" => $name, "price" => $price_op, "size_original" => $name, "price_original" => $price_op);
//                                    $this->member_model->ins_size($size_array);
//                                }
                            }
                        }
                        if ($chk_flag == 0) {
                            $size_array = array("item_id" => $dish_id, "size" => "Regular", "size_original" => "Regular", "price" => $price, "price_original" => $price);
                            $this->member_model->ins_size($size_array);
                        }
                    }
//                    exit;
                }
            }
        }
//        redirect("owners/category/$rid");
    }

    public function category($restaurant_id) {

        $user = $this->session->userdata('user');
//$restaurant_id = $user->restaurant_id;
        $location_id = $user->location_id;


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $restaurant_id = $this->input->post('restaurant_id');
            $category_id = $this->input->post('cate_id');
            $array = array(
                'menu_name' => $this->input->post('category_name'),
                'res_id' => $restaurant_id,
                'status' => 'Y',
                'created_on' => date("Y:m:d H:i:s"),
            );

//print_r($_POST);exit;
//$number; //=$this->menu_model->maxSortOrderCategory($location_id);

            $result = $this->menu_model->checkCategoryExist($restaurant_id, $array['category_name'], $category_id);
//print_r($_POST);exit;
            if (count($result) != 0) {
                $this->session->set_flashdata('error_message', 'Category already exist');
                redirect('owners/category/' . $restaurant_id);
            } else {
                $resultsitem['id'] = $this->menu_model->insertCategory($array, $category_id);
                if ($category_id != '')
                    $this->session->set_flashdata('success_message', 'Category updated successfully');
                else
                    $this->session->set_flashdata('success_message', 'Category added successfully');
                redirect('owners/category/' . $restaurant_id);
            }
        }else {
            $data['categorylist'] = $this->member_model->gelAllCategories($restaurant_id);
            foreach ($data['categorylist'] as $val) {
                
            }
            $data['restaurant_id'] = $restaurant_id;
//echo '<pre>';print_r($data['categorylist']);exit;		
            $ouput['output'] = $this->load->view('admin/menu/menu-tab', $data, true);
            $this->_render_output($ouput);
        }
    }

    public function dish($restaurant_id, $cat_id) {
        $data['restaurant_id'] = $restaurant_id;
        $data['cat_id'] = $cat_id;

        $user = $this->session->userdata('user');
//$restaurant_id = $user->restaurant_id;
        $location_id = $user->location_id;
//$data['dishlist'] =$this->member_model->get_where('dish_master',array('rid'=>$restaurant_id),'id','');
        $data['category'] = $this->member_model->gelAllCategories($restaurant_id);


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data['cat_id'] = $this->input->post('category');
            $data['dishlist'] = $this->member_model->gelAllDishItems($restaurant_id, $cat_id);
        } else {
            $data['dishlist'] = $this->member_model->gelAllDishItems($restaurant_id, $cat_id);
        }

        $ouput['output'] = $this->load->view('admin/menu/dish-tab', $data, true);
        $this->_render_output($ouput);
    }

    function save_owner_hotel() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//            print_r($_POST);
//            exit;
//            var_dump($_POST['google_id']);
//            $GOO_ID = trim($_POST['google_id']);
            $_POST['tax'] = str_replace(' ', '%', $_POST['tax']);
            $array = array(
                "google_id" => $_POST['google_id'],
                "locu_id" => $_POST['locu'],
                "alcohol" => $_POST['Alcohol'],
                "wifi" => $_POST['wifi'],
                "corkage" => $_POST['corkage'],
                "dietary_restrictions" => $_POST['dietary_restrictions'],
                "music" => $_POST['music'],
                "sports" => $_POST['sports'],
                "outdoor_seating" => $_POST['outdoor_seating'],
                "smoking" => $_POST['smoking'],
                "television" => $_POST['television'],
                "ambience" => $_POST['ambience'],
                "delivery" => $_POST['delivery'],
                "AccountNumber" => $_POST['AccountNumber'],
                "RoutingNumber" => $_POST['RoutingNumber'],
                "bank_name" => $_POST['bank_name'],
                "business_owner" => $_POST['business_owner'],
                "FAX" => $_POST['FAX'],
                "email" => $_POST['email'],
                "postal_code" => $_POST['postal_code'],
                "formatted_address" => $_POST['formatted_address'],
                "pricing" => $_POST['pricing'],
                "rating" => $_POST['rating'],
                "restaurant_name" => $_POST['res_name'],
                "lat" => $_POST['lat'],
//                "google_id" => $_POST['gid'],
                "long" => $_POST['lng'],
                "areas_delivery" => $_POST['areas'],
                "minimum_order" => $_POST['minimum_order'],
                "delivery_fee" => $_POST['delivery_fee'],
                "pickup_time" => $_POST['min_pickUp_time'],
                "delivery_time" => $_POST['min_Delivery_time'],
                "account_type" => $_POST['account_type'],
                "sales_person" => $_POST['sales_person'],
                "contact_number" => $_POST['contact_number'],
                "tax" => $_POST['tax'],
                "signed_flag" => 'Y',
                "first_name_strip" => $_POST['first_name'],
                "last_name_strip" => $_POST['last_name'],
                "ssn" => $_POST['ssn']
            );
//            print_r($array);
            if ($_POST['account_type'] == 'ACH') {

                $this->load->library('stripe_lib');

                $stripe = $this->client_model->getAdmin_strip();

                $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
                $Token = $this->stripe_lib->createBankAccount("bank_account", "US", "usd", $_POST['RoutingNumber'], $_POST['AccountNumber']);

                $params = array(
                    "name" => $_POST['first_name'] . " " . $_POST['last_name'],
                    "type" => "individual",
                    "email" => $_POST['email'],
                    "tax_id" => $_POST['ssn'],
                    "bank_account" => $Token['id']
                );


                $recipient = $this->stripe_lib->createRecipient($params);


                $array['recipient'] = $recipient->id;
            }
//            echo '<pre>';
//            print_r($array);
            $id = $this->member_model->save_hotel($array);
//            echo $this->db->last_query();
//            exit;
            foreach ($_POST['cuisine'] as $cus) {
                $this->member_model->save_restCuisine_map(array("cusine_id" => $cus, "rest_id" => $id));
            }
            if ($_POST['locu'] != '') {
                $this->menu_list($id);
            }
//            redirect("owners/oweners_success/$id");
            redirect("owners/oweners_contract/$id");
        } else {
            redirect("owners");
        }
    }

    function oweners_decline($id) {
        $this->member_model->delete_reg($id);
        redirect("owners");
    }

    function select_hotel() {
        $google_place_id = $this->uri->segment(3, 0);
        $locu_id = $this->uri->segment(4, 0);
        $data['google_place_id'] = $google_place_id;
        $data['locu_id'] = $locu_id;
        $data['cuisines'] = $this->Member_model->select_cuisine();
        $this->load->view("select_hotel", $data);
    }

    function checkNewCuisine() {
        $newCuisine = $_POST['newCuisine'];
        $count = $this->Member_model->check_new_cuisine($newCuisine);
        if ($count > 0) {
            echo "error";
        } else {
            echo "success";
        }
    }

    function newCuisine() {
        $newCuisine = $_POST['newCuisine'];
        $arr = array("cusine_name" => $newCuisine, "active" => "Y");
        $id = $this->Member_model->save_cuisine($arr);
        echo '    <div class="col-lg-12" style="margin-top: .71%" > <input type="checkbox" checked name="cus[]" value="' . $id . '" />' . $newCuisine . '</div>';
    }

    function save_hotel() {
        $ow_id = 0;
        $google_place_id = $_POST['google_id'];
        $locu_id = $_POST['locu'];
        $desc = $_POST['desc'];
        $array = array("google_id" => $google_place_id, "locu_id" => $locu_id, "owner_id" => $ow_id, "desc" => $desc);
        $rest_id = $this->Member_model->save_hotel($array);
        foreach ($_POST['cus'] as $cuisine) {
            $save_array = array("cusine_id" => $cuisine, "rest_id" => $rest_id);
            $this->Member_model->save_restCuisine_map($save_array);
        }
        $this->load->view("oweners_success", true);
    }

    function accept_owner_order($encrypted, $key) {
        $status = $this->member_model->check_order($encrypted);
//        $encrypted = rawurldecode(($encrypted));
//
//        $decrypted_id = (rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
        if ($status == "Y") {
            $rest_id = $this->order_model->get_rest_id($encrypted);
            $order_num = $this->order_model->get_Order_ref($encrypted);
            $restaurent_name = $this->user_model->get_rest($rest_id);
            $message = "Your order " . $order_num . " with " . $restaurent_name . " has been accepted'";
            $this->push_Notification($encrypted, $message, "Order");
            $array = array("order_status" => "Accepted", "order_accepted_time" => date("Y-m-d H:i:s"), "changed_method" => "mail");
            $condition = array("order_id" => $encrypted);
            $this->member_model->update_order($array, $condition);
            $this->load->view("confirm_order");
        } else {
            
        }
    }

    function accept_owner_order_web($encrypted, $key) {
        $status = $this->member_model->check_order($encrypted);
//        $encrypted = rawurldecode(($encrypted));
//
//        $decrypted_id = (rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
        if ($status == "Y") {
            $array = array("order_status" => "Accepted", "order_accepted_time" => date("Y-m-d H:i:s"), "changed_method" => "Twilo_call");
            $condition = array("order_id" => $encrypted);
            $this->member_model->update_order($array, $condition);
            $order_id = $encrypted;
            $msg = $this->member_model->get_email("Order_fails_user");
            $order = $this->order_model->get_order_details($order_id);
            $order_type = $order['order_type'];
            $rest_id = $this->order_model->get_rest_id($order_id);
            $restaurent_name = $this->user_model->get_rest($rest_id);
            $name = $order['first_name'] . " " . $order['last_name'];
            $message2 = $msg['email_template'];
            $subject2 = $msg['email_subject'] . "(" . $order['order_ref_id'] . ")";
            $message2 = str_replace('#baseurl#', base_url(), $message2);
            $message2 = str_replace('#name#', $name, $message2);
            $message2 = str_replace('#order_num#', $order['order_ref_id'], $message2);
            $message2 = str_replace('#order_time#', date("h:i:A", strtotime($order['created_time'])), $message2);
            $message2 = str_replace('#restaurant_name#', $restaurent_name, $message2);
            $message2 = str_replace('#order_type#', $order_type, $message2);
            $message2 = str_replace('#YEAR#', date('Y'), $message2);

            $data_1 = $this->member_model->admin_contratct();
            $pjt_name = $data_1['project_name'];
            $category = "Order";
            $to_mail = $order['email'];
            $from = $data_1['email'];
//            $pjt_name = $data_1['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);




            $this->load->view("twiml/twiml/1.0/order_accept");
        } else {
            
        }
    }

    function payment_refund($order_id, $amount = 0) {
        $this->load->model('user_model');
        $this->load->model('client_model');
        $this->load->library('stripe_lib');
        $data = $this->user_model->getOrderStripMap($order_id);
        if (sizeof($data) != 0) {
            $stripe = $this->client_model->getAdmin_strip();
            if (!$stripe) {
                return array('status' => 'error');
                exit;
            }
            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            $strip_order_id = $data[0]['strip_order_id'];
            $refund_amount = $data[0]['total_amount'];
            $charge_details = $this->stripe_lib->getChargeDetails($strip_order_id);

            if ($amount > 0 && $amount <= $data[0]['total_amount'])
                $refund_amount = $amount;

            $amount = bcmul($refund_amount, 100);

            if ($amount > $charge_details->amount && $charge_details->amount)
                $amount = $charge_details->amount;

            $reason = 'requested_by_customer';
            $refund_array = array("charge" => $strip_order_id, "amount" => $amount, "reason" => $reason);
            $resp = $this->stripe_lib->refundeCustomer($refund_array);
            $response = serialize($resp);

            if ($resp->id) {

                $arr = array("member_id" => $data[0]['member_id'],
                    "restaurant_id" => $data[0]['restaurant_id'],
                    "location_id" => $data[0]['location_id'],
                    "strip_customer_id" => $data[0]['strip_customer_id'],
                    "created_time" => date('Y-m-d H:i'),
                    "last_4digit" => $data[0]['last_4digit'],
                    "brand" => $data[0]['brand'],
                    "order_id" => $order_id,
                    "strip_order_id" => $resp->id,
                    "strip_response" => $response,
                    "payment_mode" => "refund");

                $this->db->insert('member_stripid_map', $arr);

                return array('status' => 'success', 'refund_amount' => $refund_amount);
            } else {
                return array('status' => 'error');
            }
        } else {
            return array('status' => 'error');
        }
    }

    function decline_owner_order_web($encrypted, $key) {
        $this->load->model('order_model');
//        $encrypted = rawurldecode(($encrypted));
//        $decrypted_id = (rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
        $status = $this->member_model->check_order($encrypted);
        if ($status == "Y") {
            $order_id = $encrypted;
            $resp = $this->payment_refund($order_id);
            if ($resp['status'] == 'success') {
                $array = array("order_status" => "Declined", "order_declined_time" => date("Y-m-d H:i:s"),"twilioflag"=>"Y", "changed_method" => "Twilo_call");
                $condition = array("order_id" => $encrypted);
//        $data_1 = $this->member_model->admin_contratct();
                $this->member_model->update_order($array, $condition);
                $this->payment_refund($encrypted);
//                $data_1 = $this->member_model->admin_contratct();
//                $order = $this->order_model->get_order_details($encrypted);

                $msg = $this->member_model->get_email("Order_fails_user");
                $order = $this->order_model->get_order_details($order_id);
                $order_type = $order['order_type'];
                $rest_id = $this->order_model->get_rest_id($order_id);
                $restaurent_name = $this->user_model->get_rest($rest_id);
                $name = $order['first_name'] . " " . $order['last_name'];
                $message2 = $msg['email_template'];
                $subject2 = $msg['email_subject'] . "(" . $order['order_ref_id'] . ")";
                $message2 = str_replace('#baseurl#', base_url(), $message2);
                $message2 = str_replace('#name#', $name, $message2);
                $message2 = str_replace('#order_num#', $order['order_ref_id'], $message2);
                $message2 = str_replace('#order_time#', date("h:i:A", strtotime($order['created_time'])), $message2);
                $message2 = str_replace('#restaurant_name#', $restaurent_name, $message2);
                $message2 = str_replace('#order_type#', $order_type, $message2);
                $message2 = str_replace('#YEAR#', date('Y'), $message2);

                $data_1 = $this->member_model->admin_contratct();
                $pjt_name = $data_1['project_name'];
                $category = "Order";
                $to_mail = $order['email'];
                $from = $data_1['email'];
                $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
                $this->load->view("twiml/twiml/1.0/order_cancel");
            }
        } else {
            
        }
//        $this->push_Notification($encrypted, "Order Declined", "Declined", "Declined");
    }

    function Poratal($id, $ksj) {
        $this->load->view("twiml/twiml/1.0/order_portal");
    }

    function push_Notification($order_id, $message, $type, $title = '') {
        $user_details = $this->member_model->get_user_order($order_id);
//        print_r($user_details);
//        echo $this->db->last_query();
        if ($user_details['push_notification'] == "Y") {
            if ($user_details['divice_type'] == 'Android') {
//        $key = $this->member_model->get_key();
                $message = array('anchor' => $order_id,
                    'message' => $message,
                    'title' => $title,
                    'type' => $type
                );
                $url = 'https://android.googleapis.com/gcm/send';
                $registrationIDs = array($user_details['device_tocken']);
                $fields = array(
                    'registration_ids' => $registrationIDs,
                    'data' => $message,
                );

                define('GOOGLE_API_KEY', 'AIzaSyDD5HCo8sdJmNrHi7zQCzYPJJCFFVPW-Mo');

                $headers = array(
                    'Authorization:key=' . GOOGLE_API_KEY,
                    'Content-Type: application/json'
                );
//            echo json_encode($fields);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === false)
                    die('Curl failed ' . curl_error());

                curl_close($ch);
                return $result;
            }else {
                $development_mode = 'N';
                $passphrase = 'newage';
////////////////////////////////////////////////////////////////////////////////
                $ctx = stream_context_create();
//        var_dump($ctx);
                if ($development_mode == 'Y') {
                    stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
                } else {
                    stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-prod.pem');
                }
                if ($development_mode == 'Y') {
                    $fp = stream_socket_client(
                            'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                } else {
                    $fp = stream_socket_client(
                            'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                }
                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);
//echo 'Connected to APNS' . PHP_EOL;
// Create the payload body
                $badge_count = 0;
                $body['aps'] = array(
                    'alert' => $message,
                    'badge' => intval($badge_count),
                    'id' => $order_id,
                    'type' => $type,
                    'sound' => 'default'
                );
                $payload = json_encode($body);
// Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $user_details['device_tocken']) . pack('n', strlen($payload)) . $payload;
                $result = fwrite($fp, $msg, strlen($msg));
                fclose($fp);
            }
        }else {
            return FALSE;
        }
    }

    function decline_owner_order($encrypted, $key) {
        $this->load->model("order_model");
//        $encrypted = rawurldecode(($encrypted));
//        $decrypted_id = (rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
        $this->load->model('order_model');
//        $encrypted = rawurldecode(($encrypted));
//        $decrypted_id = (rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
        $status = $this->member_model->check_order($encrypted);
        if ($status == "Y") {
            $resp = $this->payment_refund($encrypted);
            if ($resp['status'] == 'success') {
                $array = array("order_status" => "Declined", "order_declined_time" => date("Y-m-d H:i:s"), "changed_method" => "mail");
                $condition = array("order_id" => $encrypted);
                $this->member_model->update_order($array, $condition);
                $ref = $this->order_model->get_Order_ref($encrypted);
                $rest_id = $this->order_model->get_rest_id($encrypted);
                $restaurent_name = $this->user_model->get_rest($rest_id);
                $message = "Your order " . $ref . " with " . $restaurent_name . "  has been declined. Please contact restaurant for details.";
                $this->payment_refund($encrypted);
                $this->push_Notification($encrypted, $message, "Declined");
                $data_1 = $this->member_model->admin_contratct();
                $order = $this->order_model->get_order_details($encrypted);
                $msg = $this->member_model->get_email("Order_fails_user");
                $order_type = $order['order_type'];
                $name = $order['first_name'] . " " . $order['last_name'];
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['crlf'] = PHP_EOL;
                $config['newline'] = PHP_EOL;
                $message2 = $msg['email_template'];
                $subject2 = $msg['email_subject'];
                $message2 = str_replace('#baseurl#', base_url(), $message2);
                $message2 = str_replace('#name#', $name, $message2);
                $message2 = str_replace('#order_num#', $order['order_ref_id'], $message2);
                $message2 = str_replace('#order_type#', $order_type, $message2);
                $message2 = str_replace('#YEAR#', date('Y'), $message2);
                $category = "Order";
                $to_mail = $order['email'];
                $from = $data_1['email'];
                $pjt_name = $data_1['project_name'];
                $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
//            $this->load->library('email');
//            $this->email->initialize($config);
//           
//            $this->email->from($data_1['email'], $pjt_name);
//            $this->email->to($order['email']);
//            $this->email->bcc("gauravbazaz@gmail.com");
//            $this->email->subject($subject2);
//            $this->email->message($message2);
//            $this->email->send();
                redirect("owners/decline_success");
            }
        } else {
            
        }
    }

    function decline_success() {
        $this->load->view("confirm_order");
    }

//    function payment_refund($order_id, $amount = 0) {
//
//        $this->load->library('stripe_lib');
////$order_id=728;
//        $data = $this->user_model->getOrderStripMap($order_id);
//
//
//        if (sizeof($data) != 0) {
//
//            $stripe = $this->client_model->getAdmin_strip();
//            if (!$stripe) {
//                return array('status' => 'error');
//                exit;
//            }
//            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
//            $strip_order_id = $data[0]['strip_order_id'];
//            $refund_amount = $data[0]['total_amount'];
//            $charge_details = $this->stripe_lib->getChargeDetails($strip_order_id);
//            if ($amount > 0 && $amount <= $data[0]['total_amount'])
//                $refund_amount = $amount;
//            $amount = bcmul($refund_amount, 100);
//            if ($amount > $charge_details->amount && $charge_details->amount)
//                $amount = $charge_details->amount;
//            $reason = 'requested_by_customer';
//            $refund_array = array("charge" => $strip_order_id, "amount" => $amount, "reason" => $reason);
//            $resp = $this->stripe_lib->refundeCustomer($refund_array);
//            $response = serialize($resp);
//
//            if ($resp->id) {
//
//                $arr = array("member_id" => $data[0]['member_id'],
//                    "restaurant_id" => $data[0]['restaurant_id'],
//                    "location_id" => $data[0]['location_id'],
//                    "strip_customer_id" => $data[0]['strip_customer_id'],
//                    "created_time" => date('Y-m-d H:i'),
//                    "last_4digit" => $data[0]['last_4digit'],
//                    "brand" => $data[0]['brand'],
//                    "order_id" => $order_id,
//                    "strip_order_id" => $resp->id,
//                    "strip_response" => $response,
//                    "payment_mode" => "refund");
//
//                $this->db->insert('member_stripid_map', $arr);
//
//                return array('status' => 'success', 'refund_amount' => $refund_amount);
//            } else {
//                return array('status' => 'error');
//            }
//        } else {
//            return array('status' => 'error');
//        }
//    }

    function send_mail() {

        $data_1 = $this->member_model->admin_contratct();


//            $name = $_POST['name'];

        $name = $_POST['name'];
        $message3 = $_POST['message'];
        $email = $_POST['email'];

//$email = $rest['email'];
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['crlf'] = PHP_EOL;
        $config['newline'] = PHP_EOL;
        $message = $this->member_model->get_email("Contact_Us");

        $message1 = $message['email_template'];
        $subject = $message['email_subject'];
        $message1 = str_replace('#baseurl#', base_url(), $message1);
        $message1 = str_replace('#Name#', $name, $message1);
        $message1 = str_replace('#TOPIC#', '', $message1);
        $message1 = str_replace('#email#', $email, $message1);
        $message1 = str_replace('#Message#', $message3, $message1);
        $message1 = str_replace('#subject#', $subject, $message1);

//            exit;
//            $message1 = "Hi " . $name . ",<br/>Yor Login Credentials are following : <br/>User Name : " . $email . "<br/> Password: " . $password;
//        $this->load->library('email');
//        $this->email->initialize($config);
////$list_bcc = array($data_1['email'], 'bibinv@newagesmb.com', 'gauravbazaz@gmail.com');
//        
//        $this->email->from($email);
//        $this->email->bcc("gauravbazaz@gmail.com");
//        $this->email->to($data_1['email']);
////            $this->email->to($this->config->item('email_from'));
//        $this->email->subject($subject);
//        $this->email->message($message1);
//        $this->email->send();
        $category = "Contact";
        $pjt_name = $data_1['project_name'];
//        $to_mail = $email;
//        $from = $data_1['email'];
        $this->member_model->mail_sendgrid($data_1['email'], $subject, $message1, $category, $email, $pjt_name);
        $msg = $this->member_model->get_email("contact_us_form_email_to_user");
        $msg1 = $msg['email_template'];
        $sub = $msg['email_subject'];
        $msg1 = str_replace("#baseurl#", base_url(), $msg1);
        $msg1 = str_replace("#Name#", $name, $msg1);
//        $this->load->library('email');
//        $this->email->initialize($config);
////$list_bcc = array($data_1['email'], 'bibinv@newagesmb.com', 'gauravbazaz@gmail.com');
//        $this->email->from($data_1['contact_email']);
//        $this->email->bcc("gauravbazaz@gmail.com");
//        $this->email->to($email);
//        $this->email->subject($sub);
//        $this->email->message($msg1);
//        $this->email->send();
//        $category = "Contact";
//        $to_mail = $email;
//        $from = $data_1['email'];
        $this->member_model->mail_sendgrid($email, $sub, $msg1, $category, $data_1['contact_email'], $pjt_name);
//$data_1 = $this->member_model->admin_contratct();
//        $name = $_POST['name'];
//        $message = $_POST['message'];
//        $email = $_POST['email'];
//        $config['mailtype'] = 'html';
//        $config['charset'] = 'utf-8';
//        $config['crlf'] = PHP_EOL;
//        $config['newline'] = PHP_EOL;
//        $message1 = "Name " . $name . "<br/>Email " . $email . "<br/> Message: " . $message;
//        $this->load->library('email');
//        $this->email->initialize($config);
//
//        $this->email->from($data_1['email']);
//        $this->email->to($data_1['email']);
////            $this->email->to($this->config->item('email_from'));
//        $this->email->subject("Message From Contact Us");
//        $this->email->message($message1);
//        $this->email->send();
    }

    function email_exist() {
        $this->load->model('member_model');
        $exist_detail = $this->member_model->checkEmailExist_res($_POST['email']);

        if (!$exist_detail) {

            echo("success");
            exit;
        } else {
            echo("error");
            exit;
        }
    }

    function oweners_success() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $name = $_POST['client_name'];
            $password = rand(11111, 99999) . uniqid() . rand(111, 999);
            $array = array("signed_by" => $name, "signed_date" => date("Y-m-d H:i:s"), "signed_flag" => "Y", "password" => md5($password));
            $cond = array("restaurant_id" => $id);
            $this->member_model->update_contract($array, $cond);
            $data_1 = $this->member_model->admin_contratct();


//            $name = $_POST['name'];
            $rest = $this->member_model->get_restaurent($id);
//            print_r($rest);
//            exit;
//            $message = $_POST['message'];
            $email = $rest['email'];
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;
            $message = $this->member_model->get_email("Owner_Registration");
            $message1 = $message['email_template'];
            $subject = $message['email_subject'];
            $message1 = str_replace('#baseurl#', base_url(), $message1);
            $message1 = str_replace('#BASEURL#', base_url(), $message1);
            $message1 = str_replace('#name#', $name, $message1);
            $message1 = str_replace('#email#', $email, $message1);
            $message1 = str_replace('#password#', $password, $message1);
            $message1 = str_replace('#YEAR#', date('Y'), $message1);
//            echo $message1;
//            exit;
//            $message1 = "Hi " . $name . ",<br/>Yor Login Credentials are following : <br/>User Name : " . $email . "<br/> Password: " . $password;
            $this->load->library('email');
            $this->email->initialize($config);
            $pjt_name = $data_1['project_name'];
            $this->email->from($data_1['email'], $pjt_name);
            $this->email->to($email);
            $data['name'] = $name;
            $data['password'] = $password;
            $data['email'] = $email;
//            $this->email->to($this->config->item('email_from'));
//                $message1 = $this->load->view('email/owners_registration.php', $data, TRUE);
//            $this->email->subject($subject);
//            $this->email->message($message1);
//            $this->email->bcc("gauravbazaz@gmail.com");
//            $this->email->send();
            $category = "Owner";
            $to_mail = $email;
            $from = $data_1['email'];
            $this->member_model->mail_sendgrid($to_mail, $subject, $message1, $category, $from, $pjt_name);
            $data['id'] = $id;
            $this->load->view("oweners_success", $data);
            $this->load->view("footer_owner");
        } else {
            redirect("owners/signup");
        }
    }

    function test_api() {
        $id = $_POST['id'];
        if ($id != '') {
            error_reporting(E_ALL);
            $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
            $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"menus\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$id\" }
    
  ]
}'";
//        echo $uri;
            $c_pid = exec($uri);
            echo "<pre>";
            print_r(json_decode($c_pid));
            echo '</pre>';
            exit;
        } else {
            $this->load->view("locu_list");
        }
    }

    function test_api5($id) {

        error_reporting(E_ALL);
        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
\"fields\" : [\"name\",  \"extended\",\"delivery\",\"menus\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$id\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        echo "<pre>";
        print_r(json_decode($c_pid));
        echo '</pre>';
        exit;
    }

    function test_api52() {
//$id = $_POST['id'];
//        $id = "ded1e1816755d369d59d";
//        error_reporting(E_ALL);
//        $api_key = "de1d60cb51b6054b1021a211b272d39dbd256eb8";
//        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
//         \"api_key\" : \"$api_key\",
// \"fields\" : [\"name\", \"menus\" ],
//  \"venue_queries\" : [
//   { \"locu_id\":\"$id\" }
//    
//  ]
//}'";
////        echo $uri;
//        $c_pid = exec($uri);
//        echo '<pre>';
//        print_r(json_decode($c_pid));
//
        $id = "6304fef7dd9503194c2b";
        error_reporting(E_ALL);
        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
\"fields\" : [\"name\",  \"extended\",\"delivery\",\"menus\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$id\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        echo '<pre>';
        print_r(json_decode($c_pid));
        $id = "4eef2bb2d2b2ffa0f7d9";
        error_reporting(E_ALL);
        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
\"fields\" : [\"name\",  \"extended\",\"delivery\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$id\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        echo '<pre>';
        print_r(json_decode($c_pid));
        $id = "64c42d6f571a677cdf76";
        error_reporting(E_ALL);
        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
\"fields\" : [\"name\",  \"extended\",\"delivery\" ],
  \"venue_queries\" : [
   { \"locu_id\":\"$id\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        echo '<pre>';
        print_r(json_decode($c_pid));
//        $id = "7ae02d400d4959b95783";
//        error_reporting(E_ALL);
//        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
//        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
//         \"api_key\" : \"$api_key\",
// \"fields\" : [\"name\", \"menus\" ],
//  \"venue_queries\" : [
//   { \"locu_id\":\"$id\" }
//    
//  ]
//}'";
////        echo $uri;
//        $c_pid = exec($uri);
//        echo "<pre>";
//        print_r(json_decode($c_pid));
//        $id = "9d206f08ee6d291affa4";
//        error_reporting(E_ALL);
//        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
//        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
//         \"api_key\" : \"$api_key\",
// \"fields\" : [\"name\", \"menus\"],
//  \"venue_queries\" : [
//   { \"locu_id\":\"$id\" }
//    
//  ]
//}'";
////        echo $uri;
//        $c_pid = exec($uri);
//
//        print_r(json_decode($c_pid));
        echo '</pre>';
        exit;
    }

    function test_api1() {
// $id = $_POST['id'];
        error_reporting(E_ALL);

        $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
        $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
         \"api_key\" : \"$api_key\",
 \"fields\" : [\"name\", \"extended\",\"delivery\",\"contact\"],
  \"venue_queries\" : [
   { \"locu_id\":\"14fcdd09767e702a8e9f\" }
    
  ]
}'";
//        echo $uri;
        $c_pid = exec($uri);
        echo "<pre>";
        print_r(json_decode($c_pid));
        echo '</pre>';
        exit;
    }

    function locu_list() {
        $google_id = $_POST['json'];
        $google_key = $this->member_model->get_key();
        $uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$google_id&key=$google_key";
        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_URL, $uri);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch1, CURLOPT_ENCODING, 'gzip');
        $sData = curl_exec($ch1);
        $response1 = json_decode($sData);
        $response_data = $response1->result;
        echo '<pre>';
        print_r($response_data);
        echo '</pre>';
    }

    function insert_average_order() {
        $average_order = $_POST['average_order'];
        $arr = array("average_order" => $average_order,
            "add_date" => date('Y-m-d H:i'),
            "average_order_with_sign" => '$' . $average_order,
        );
        $this->db->insert('average_order_master', $arr);
    }

    public function add_dish($restaurant_id, $cat_id, $item_id) {
        $this->load->model('menu_model');
        $data['restaurant_id'] = $restaurant_id;
        $data['categorylist'] = $this->member_model->gelAllCategories($restaurant_id);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $item_id = $this->input->post('item_id');
            $number = $this->menu_model->maxSortOrderDishItem($location_id, $this->input->post('category'));

            $data['restaurant_id'] = $this->input->post('restaurant_id');
            $array = array(
                'menu' => $this->input->post('category'),
                'dish_name' => stripslashes(mysql_real_escape_string($this->input->post('menu_item'))),
                'description' => $this->input->post('description'),
                'sortorder' => $number + 1,
            );
            $result = $this->menu_model->checkItemExist($array['category_id'], $array['item_name'], $item_id);

            if (count($result) != 0) {
                $this->session->set_flashdata('error_message', 'Item already exist');
                redirect($this->user->root . '/menu/add_dish/' . $item_id);
            } else {
                $resultsitem['id'] = $this->menu_model->insertDishItems($array, $item_id);
                $mulsize = $this->input->post('mulsize');
                $mulprice = $this->input->post('mulprice');
                $mulsizeid = $this->input->post('mulsizeid');
                if ($this->input->post('nosize') == 'no') {
                    for ($i = 0; $i < count($mulsize); $i++) {
                        if ($mulsize[$i] != '' and $mulprice[$i] != '') {
                            $newarr = array("item_id" => $resultsitem['id'],
                                "size" => $mulsize[$i],
                                "price" => $mulprice[$i]
                            );
                            $resu = $this->menu_model->UpdateOrInsertDishItemSize($newarr, $mulsizeid[$i]);
                        }
                    }
                } else {

                    $newarr = array("item_id" => $resultsitem['id'],
                        "size" => "Regular",
                        "price" => $this->input->post('price_dish')
                    );
                    $resu = $this->menu_model->insertDishItemSize($newarr, $resultsitem['id']);
                }
//$delid	=$this->menu_model->deleteOptionAndSides($resultsitem['id']);
//echo '<pre>';print_r($_POST);exit;
                $option_item = $this->input->post('option_item');
                $rev_option_item = array_reverse($option_item);
                $j = 1;
//echo $rev_option_item['id'];

                for ($i = 0; $i < count($rev_option_item); $i++) {
                    $ar = array('option_item' => $rev_option_item[$i],
                        'sides' => $this->input->post('sides_' . $j),
                        'price' => $this->input->post('price_' . $j)
                    );
                    $sidearr = $this->input->post('sides_' . $j);
                    $mandatoryAr = $this->input->post('mandatory_' . $j);
                    $mul_limAr = $this->input->post('mul_lim_' . $j);
                    $multipleAr = $this->input->post('multiple_' . $j);
                    if ($multipleAr == 'on') {
                        $multiple = 'Y';
                        $mul_lim = $mul_limAr;
                    } else {
                        $multiple = 'N';
                        $mul_lim = 1;
                    }
                    if ($mandatoryAr == 'on') {
                        $mandatory = 'Y';
                    } else {
                        $mandatory = 'N';
                    }


                    $maxnum = $this->menu_model->maxSortOrder($location_id, $resultsitem['id']);
                    $maxnum = $maxnum + 1;
                    if ($ar['option_item'] != '' and $sidearr[0] != '') {

                        $dishoption = array("location_id" => $location_id,
                            "restaurant_id" => $data['restaurant_id'],
                            "dish_item_id" => $resultsitem['id'],
                            "option_name" => $ar['option_item'],
                            "mandatory" => $mandatory,
                            "multiple" => $multiple,
                            "limit" => $mul_lim,
                            "sortorder" => $maxnum,
                        );
//insert into dish_options
                        $optid = $this->menu_model->insertDishOptions($dishoption);
//echo '<pre>';$ar['option_item'];
                        if (count($ar['sides']) != 0 and count($ar['price']) != 0) {
                            for ($k = 0; $k < count($ar['sides']); $k++) {
                                if ($ar['sides'][$k] != '') {
//insert into option_sides
                                    $sidenumber = $this->menu_model->maxSortOrderDish($optid);
                                    $sidenumber = $sidenumber + 1;
                                    $sidesarray = array("option_id" => $optid,
                                        "side_item" => $ar['sides'][$k],
                                        "price" => $ar['price'][$k],
                                        "sortorder" => $sidenumber,
                                    );
                                    $dishid = $this->menu_model->insertOptionsSides($sidesarray);

//echo '<pre>';echo $ar['sides'][$k];
//echo '<pre>';echo $ar['price'][$k];
                                }
                            }
                        }
                    }


                    $j++;
                }

//-------------for options and dishes-----close

                if ($item_id != '')
                    $this->session->set_flashdata('success_message', 'Dish item updated successfully');
                else
                    $this->session->set_flashdata('success_message', 'Dish item added successfully');
                redirect($this->user->root . '/owners/add_dish/' . $data['restaurant_id'] . '/' . $array['menu'] . '/' . $resultsitem['id']);
//echo $this->load->view('admin/menu/add_dish');
                exit;
            }
        }else {


            if ($item_id != '') {
                $result = $this->menu_model->checkItem($item_id);

                if (count($result) != 0) {
                    $data['itemdetails'] = $this->menu_model->getItemDetails($item_id);
//echo '<pre>';print_r($data['itemdetails']);exit;
//echo '<pre>';print_r($data['itemdetails']);
//echo $item_id;
//$data['options_details']	=$this->menu_model->getDishOptions($item_id);
//echo '<pre>';print_r($data['options_details']);exit;
                    $data['options_details'] = $this->menu_model->getDishOptions($item_id);
                    $data['sizes_details'] = $this->menu_model->getDishsizes($item_id);
//echo '<pre>';print_r($data['sizes_details']);exit;
                    if (count($data['options_details'] != '')) {
                        foreach ($data['options_details'] as $var) {
                            $sidesdetails[$var['option_id']] = $this->menu_model->getOptionSides($var['option_id']);
                        }
                    }
                    $data['sidesdetails'] = $sidesdetails;
                    $ouput['output'] = $this->load->view('admin/menu/add-dish_new', $data, true);
                    $this->_render_output($ouput);
                } else {
                    redirect($this->user->root . '/menu/dish');
                }
            } else {
                $data['categorylist'] = $this->member_model->gelAllCategories($restaurant_id, $location_id);
//echo '<pre>';print_r($data['categorylist']);exit;
                $data['itemdetails']['category_id'] = $cat_id;
                $ouput['output'] = $this->load->view('admin/menu/add-dish', $data, true);
                $this->_render_output($ouput);
            }
        }
    }

    public function categoryStatus() {

        $category_id = $_POST['category_id'];
        if ($_POST['status'] == 'Y')
            $status = 'N';
        else
            $status = 'Y';
        if ($category_id != '')
            $res = $this->menu_model->categoryStatus($category_id, $status);
    }

    public function ajaxUpdateDIshItem() {
        $category = $_POST['category'];
        $menu_item = stripslashes(mysql_real_escape_string($_POST['menu_item']));
        $description = stripslashes(mysql_real_escape_string($_POST['description']));
        $item_id = $_POST['item_id'];

        $newarray = array("menu" => $category,
            "dish_name" => $menu_item,
            "description" => $description,
        );
        $resu = $this->menu_model->insertDishItems($newarray, $item_id);


        $result = $this->menu_model->getItemDetails($resu);
        $result['item_name'] = stripslashes($result['dish_name']);
//echo '<pre>';print_r($result);	exit;
        echo json_encode($result);
//echo $this->load->view('admin/menu/ajaxAddSidePrice',$data);
    }

    public function ajaxAddSidePrice() {
        $data['newid'] = $_POST['newid'];
        $data['newcnt'] = $_POST['newcnt'];
        $data['item_id'] = $_POST['item_id'];

        $mulsize = $_POST['size_array'];
        $mulprice = $_POST['price_array'];
        $mulsizeid = $_POST['map_id'];

        for ($i = 0; $i < count($mulsize); $i++) {
            if ($mulsize[$i] != '' and $mulprice[$i] != '') {

                if ($data['item_id'] != '') {
                    $newarr = array("item_id" => $data['item_id'],
                        "size" => $mulsize[$i],
                        "price" => $mulprice[$i]
                    );
                    $resu = $this->menu_model->UpdateOrInsertDishItemSize($newarr, $mulsizeid[$i]);
                }
            }
        }



        echo $this->load->view('admin/menu/ajaxAddSidePrice', $data);
    }

    public function deleteSize() {
        $sizeid = isset($_POST['sizeid']) ? $_POST['sizeid'] : '';
        if ($sizeid != '') {
            $res = $this->menu_model->delDishItemSize($sizeid);
        } else {
            $item_id = $_POST['item_id'];
            $size = $_POST['size'];
            $res = $this->menu_model->delDishItemSize('', $item_id, $size);
        }
    }

    public function checkItemExist() {
        $category_id = $this->input->post('category');
        $item_name = stripslashes(mysql_real_escape_string($this->input->post('menu_item')));
        $item_id = $this->input->post('item_id');
        $result = $this->menu_model->checkItemExist($category_id, $item_name, $item_id);
//print_r($result);
        if (count($result) != 0) {
            echo "0"; //exist
        } else {
            echo "1"; //not exist
        }
        exit;
    }

    public function saveOptionAjax() {
        $option_id = $_POST['option_id'];
        $option_name = $_POST['name'];
        $this->menu_model->setTable('dish_options');
        $res = $this->menu_model->update_by(array('option_id' => $option_id), array('option_name' => $option_name));
        return true;
    }

    public function saveOptionSideAjax() {
        $option_id = $_POST['option_id'];
        $side_id = $_POST['side_id'];
        $side_item = $_POST['value'];
        $this->menu_model->setTable('option_sides');
        if ($side_id != '') {
            $res = $this->menu_model->update_by(array('side_id' => $side_id), array('side_item' => $side_item));
        } else {
            if ($side_item != '') {
                $res = $this->menu_model->insert(array('option_id' => $option_id, 'side_item' => $side_item));
            }
        }
//echo $this->db->insert_id();
//print_r($res);
        $data['sidesdetails'] = $this->menu_model->getOptionSides($option_id, 'ASC');
        $data['option_id'] = $option_id;
        echo $this->load->view('admin/menu/ajaxSidePrice', $data);

        return true;
    }

    public function saveOptionPriceAjax() {
        $option_id = $_POST['option_id'];
        $side_id = $_POST['side_id'];
        $price = $_POST['value'];
        $this->menu_model->setTable('option_sides');
        $res = $this->menu_model->update_by(array('side_id' => $side_id), array('price' => $price));
        return true;
    }

    public function ajaxUpdateItem() {
        $optitle = $_POST['option_item'];
        $options = $_POST['options'];
        $prices = $_POST['price'];
        $option_id = $_POST['option_id'];
        $sidesid = $_POST['sidesid'];
        $mandatory = $_POST['mandatory'];
        $multiple = $_POST['multiple'];


        $data['categorylist'] = $this->menu_model->updateDishOptions($option_id, $optitle, $mandatory, $multiple);

        if (count($options) != 0) {
            for ($i = 0; $i < count($options); $i++) {
                if ($options[$i] != '') {

                    $number = $this->menu_model->maxSortOrderDish($option_id);

                    $arr = array(
                        'side_item' => $options[$i],
                        'price' => $prices[$i],
                        'option_id' => $option_id
                    );

                    $data['categorylist'] = $this->menu_model->UpdateOptionsSides($arr, $sidesid[$i], $options[$i], $option_id, $number);
// echo '<pre>';print_r($option_id);exit;
//echo $this->load->view('admin/menu/add',$option_id);
                }
            }
        }
        $data['sidesdetails'] = $this->menu_model->getOptionSides($option_id, 'ASC');
        $data['option_id'] = $option_id;
        echo $this->load->view('admin/menu/ajaxSidePrice', $data);
//print_r($option);exit;
    }

    public function OptionStatus() {

        $option_id = $_POST['option_id'];
        if ($_POST['status'] == 'Y')
            $status = 'N';
        else
            $status = 'Y';
        if ($option_id != '') {
            $this->menu_model->setTable('dish_options');
            $res = $this->menu_model->update_by(array('option_id' => $option_id), array('status' => $status));
        }
    }

    public function addSidesDiv() {
        $data['optid'] = $_POST['optid'];
        $data['count'] = $_POST['count'] + 1;
        echo $this->load->view('admin/menu/sidesDiv', $data);
    }

    public function ajaxOptionAndSides() {
        $user = $this->session->userdata('user');
        $location_id = $user->location_id;
//$restaurant_id=$user->restaurant_id;

        $restaurant_id = $_POST['restaurant_id'];
        $option_item = $_POST['option_item'];
        $optside = $_POST['optside'];
        $optprice = $_POST['optprice'];
        $item_id = $_POST['item_id'];
        $mandatory = $_POST['mandatory'];
        $multiple = $_POST['multiple'];
        $limit = $_POST['mul_lim'];


        $number = $this->menu_model->maxSortOrder($location_id, $item_id);

        $dishoption = array("location_id" => $location_id,
            "restaurant_id" => $restaurant_id,
            "dish_item_id" => $item_id,
            "mandatory" => $mandatory,
            "multiple" => $multiple,
            "limit" => $limit,
            "option_name" => $option_item,
            "sortorder" => $number + 1,
        );
//insert into dish_options
        $optid = $this->menu_model->insertDishOptions($dishoption);

        if (count($optside) != 0 and count($optprice) != 0) {
            for ($k = 0; $k < count($optside); $k++) {
                if ($optside[$k] != '' and $optprice[$k] != '') {
//insert into option_sides
                    $sidenumber = $this->menu_model->maxSortOrderDish($optid);
                    $maxnumber = $sidenumber + 1;

                    $sidesarray = array("option_id" => $optid,
                        "side_item" => $optside[$k],
                        "price" => $optprice[$k],
                        "sortorder" => $maxnumber,
                    );
                    $dishid = $this->menu_model->insertOptionsSides($sidesarray);
                }
            }
        }


        if ($item_id != '') {
            $data['itemdetails'] = $this->menu_model->getItemDetails($item_id);
            $data['options_details'] = $this->menu_model->getDishOptions($item_id);
            $data['sizes_details'] = $this->menu_model->getDishsizes($item_id);

            if (count($data['options_details'] != '')) {
                foreach ($data['options_details'] as $var) {
                    $sidesdetails[$var['option_id']] = $this->menu_model->getOptionSides($var['option_id']);
                }
            }
            $data['sidesdetails'] = $sidesdetails;

//echo '<pre>';print_r($data['sizes_details']);exit;
//echo '<pre>';print_r($data);
//echo $item_id;
        }
        $data['page'] = "plusBtn";
        $data['categorylist'] = $this->menu_model->gelAllCategories($restaurant_id, $location_id);
        echo $this->load->view('admin/menu/ajaxoptionsides', $data);
    }

    public function sortorder() {
//$sidesort	=	$_POST['sideslist'];
        $sidesort = $_POST['sideslist'];
//echo '<pre>';print_r($sidesort);
        $i = 1;
        foreach ($sidesort as $val) {
//echo $val;
            $this->menu_model->setTable('option_sides');
            $this->menu_model->update_by(array('side_id' => $val), array('sortorder' => $i));
            $i++;
        }
    }

    public function optionsortorder() {
        $optionlist = $_POST['optionlist'];
//echo '<pre>';print_r($_POST['optionlist']);
        $i = 1;
        foreach ($optionlist as $val) {
            $this->menu_model->setTable('dish_options');
            $this->menu_model->update_by(array('option_id' => $val), array('sortorder' => $i));
            $i++;
        }
    }

    public function deleteSides() {
        $sidesid = isset($_POST['sidesid']) ? $_POST['sidesid'] : '';
        $optionsid = $_POST['optionsid'];
        $res = $this->menu_model->delDishItemSides($sidesid, $optionsid);
        echo $res;
        exit;
    }

    public function ajaxdelShowsides() {
        $option_id = $_POST['option_id'];
        $data['sidesdetails'] = $this->menu_model->getOptionSides($option_id);
        $data['option_id'] = $option_id;
        echo $this->load->view('admin/menu/ajaxSidePriceDel', $data);
    }

    public function deleteall() {
        $data['option_id'] = $_POST['option_id'];
        $data['deletealloptions'] = $this->menu_model->delDishOption($data['option_id']);
        $data['deleteallsides'] = $this->menu_model->delDishSide($data['option_id']);
        exit;
    }

    public function SaveDishItem() {
        $user = $this->session->userdata('user');
//$restaurant_id=$user->restaurant_id;
        $restaurant_id = $this->input->post('restaurant_id');
        $location_id = $user->location_id;
        $item_id = $this->input->post('item_id');

        $array = array(
            'menu' => $this->input->post('category'),
            'dish_name' => stripslashes(mysql_real_escape_string($this->input->post('menu_item'))),
            'description' => mysql_real_escape_string($this->input->post('description')),
        );
        $resultsitem['id'] = $this->menu_model->insertDishItems($array, $item_id);

        $mulsize = $this->input->post('size_array');
        $mulprice = $this->input->post('price_array');
        $mulsizeid = $this->input->post('map_id');
//        echo print_r($array);exit;
//echo '<pre>';print_r($_POST);exit;
        if ($this->input->post('nosize') != 'true') {
            $newarr = array("item_id" => $resultsitem['id'],
                "size" => "Regular",
                "price" => $this->input->post('price_array')
            );

            $resu = $this->menu_model->insertDishItemSize($newarr, $resultsitem['id']);
        } else {
            $resu = $this->menu_model->deleteDishItemSize($resultsitem['id']);
            for ($i = 0; $i < count($mulsize); $i++) {
                if ($mulsize[$i] != '' and $mulprice[$i] != '') {
                    $newarr = array("item_id" => $resultsitem['id'],
                        "size" => $mulsize[$i],
                        "price" => $mulprice[$i]
                    );
//$resu	=$this->menu_model->UpdateOrInsertDishItemSize($newarr,$mulsizeid[$i]);			  
                    $resu = $this->menu_model->UpdateOrInsertDishItemSize($newarr);
                }
            }
        }
        $optionsides = $this->input->post('optionsides');
        $optionprice = $this->input->post('optionprice');
        $option_item = $this->input->post('option_item');
        $mandatory = $this->input->post('mandatory');
        $multiple = $this->input->post('multiple');
        $mul_lim = $this->input->post('mul_lim');

        $number = $this->menu_model->maxSortOrder($location_id, $item_id);

        $dishoption = array("location_id" => $location_id,
            "restaurant_id" => $restaurant_id,
            "dish_item_id" => $item_id,
            "option_name" => $option_item,
            "mandatory" => $mandatory,
            "multiple" => $multiple,
            "limit" => $mul_lim,
            "sortorder" => $number + 1,
        );
        if ($option_item != '' and $optionsides[0] != '') {
//insert into dish_options
            $optid = $this->menu_model->insertDishOptions($dishoption);
            if (count($optionsides) != 0) {
                for ($k = 0; $k < count($optionsides); $k++) {

                    $sidenumber = $this->menu_model->maxSortOrderDish($optid);
                    $maxnumber = $sidenumber + 1;


                    if ($optionsides[$k] != '') {
//insert into option_sides
                        $sidesarray = array("option_id" => $optid,
                            "side_item" => $optionsides[$k],
                            "price" => $optionprice[$k],
                            "sortorder" => $maxnumber
                        );
                        $dishid = $this->menu_model->insertOptionsSides($sidesarray);
                    }
                }
            }
        }
        $arrOptid = $this->input->post('arrOptid');
        $arrMan = $this->input->post('arrMan');
        $arrMul = $this->input->post('arrMul');
        $arrLimit = $this->input->post('arrLimit');
        if (count($arrOptid) != 0) {
            for ($j = 0; $j < count($arrOptid); $j++) {
                $arraydata = array("mandatory" => $arrMan[$j],
                    "multiple" => $arrMul[$j],
                    "limit" => $arrLimit[$j]
                );

                $dishid = $this->menu_model->updateDishitem($arraydata, $arrOptid[$j]);
            }
        }
//echo '<pre>';print_r($arrLimit);exit;		
        $newsideArrayID = $this->input->post('newsideArrayID');
        $newsideArray = $this->input->post('newsideArray');
        $newPriceArray = $this->input->post('newPriceArray');
        if (count($newsideArrayID) != 0) {
            for ($l = 0; $l < count($newsideArrayID); $l++) {
                if ($newsideArray[$l] != '') {
                    $sidenumber = $this->menu_model->maxSortOrderDish($newsideArrayID[$l]);
                    $arraydata2 = array("side_item" => $newsideArray[$l],
                        "price" => $newPriceArray[$l],
                        "option_id" => $newsideArrayID[$l],
                        "sortorder" => $sidenumber + 1,
                    );
                    $dishid = $this->menu_model->insertOptionsSides($arraydata2);
                }
            }
        }

        if ($item_id != '') {
            $data['itemdetails'] = $this->menu_model->getItemDetails($item_id);
            $data['options_details'] = $this->menu_model->getDishOptions($item_id);
            $data['sizes_details'] = $this->menu_model->getDishsizes($item_id);

            if (count($data['options_details'] != '')) {
                foreach ($data['options_details'] as $var) {
                    $sidesdetails[$var['option_id']] = $this->menu_model->getOptionSides($var['option_id']);
                }
            }
            $data['sidesdetails'] = $sidesdetails;

//echo '<pre>';print_r($data);exit;
//echo '<pre>';print_r($data);
//echo $item_id;
        }
        $data['categorylist'] = $this->menu_model->gelAllCategories($restaurant_id, $location_id);
        echo $this->load->view('admin/menu/ajaxoptionsides', $data);
    }

    public function checkCategoryExist() {
        $user = $this->session->userdata('user');
//$restaurant_id=$user->restaurant_id;


        $restaurant_id = $this->input->post('restaurant_id');
        $category_name = $this->input->post('category');
        $category_id = $this->input->post('category_id');
        $result = $this->menu_model->checkCategoryExist($restaurant_id, $category_name, $category_id);
        if (count($result) != 0) {
            echo "0"; //exist
        } else {
            echo "1"; //not exist
        }
        exit;
    }

    public function getCategoryDetail() {
        $cat_id = $_POST['cat_id'];
        $res = $this->menu_model->getCategoryDetails($cat_id);
        $data['category_id'] = $res['id'];
        $data['category_name'] = stripslashes($res['menu_name']);
        echo json_encode($data);
        exit;
    }

    public function checkCat($cat_id) {
        $category_id = $_POST['category_id'];
        $restaurant_id = $_POST['restaurant_id'];
        $user = $this->session->userdata('user');
        $data['catid'] = $cat_id;
        $data['dishlist'] = $this->menu_model->gelSelectedDishItems($restaurant_id, $category_id);
        echo count($data['dishlist']);
    }

    public function deleteCategory($category_id) {
        $category_id = $this->input->post('category_id');
        if ($category_id != '')
            $res = $this->menu_model->delCategory($category_id);
        $this->session->set_flashdata('success_message', 'Category deleted successfully');
//redirect('admin/menu/dish/');
    }

    public function deleteDish($item_id) {

        $item_id = $this->input->post('item_id');
        if ($item_id != '')
            $res = $this->menu_model->delRestaurantItems($item_id);
//$this->session->set_flashdata('success_message', 'Dish item deleted successfully');
//redirect('admin/menu/dish/');
    }

    public function itemStatus() {

        $item_id = $_POST['item_id'];
        if ($_POST['status'] == 'Y')
            $status = 'N';
        else
            $status = 'Y';
        if ($item_id != '')
            $res = $this->menu_model->dishItemStatus($item_id, $status);
    }

    public function addOptionDiv() {
        $data['count'] = $_POST['count'] + 1;
        echo $this->load->view('admin/menu/optionDiv', $data);
    }

    /* 	public function getCat($cat_id)
      {
      $cat_id	=	$_POST['cat_id'];
      $user = $this->session->userdata('user');
      $restaurant_id=$user->restaurant_id;
      $location_id=$user->location_id;
      //$data['category']	=	$this->menu_model->gelAllCategories($restaurant_id,$location_id);
      $data['catid']	=	$cat_id;
      $data['category']	=	$this->menu_model->gelAllCategories($restaurant_id,$location_id);
      $data['dishlist']	=	$this->menu_model->gelSelectedDishItems($location_id,$cat_id);
      //print_r($data['category']);exit;
      echo $this->load->view('admin/menu/dish-tab',$data);

      } */

    function test_rep() {
        $data['get_package_details'] = $this->menu_model->get_package_details();
        $this->load->view('index_new1', $data);
        $this->load->view("footer_owner");
    }

    function add_restaurent() {
        $data['cuisines'] = $this->member_model->select_cuisine();
        $data['cusine_config'] = $this->member_model->get_cusine_config();
        $data['rating_config'] = $this->member_model->get_rating_config();
        $data['price_config'] = $this->member_model->get_price_config();
        $data['feature_config'] = $this->member_model->get_feature_config();
        $this->load->view('google_place_list_new', $data);
        $this->load->view("footer_owner");
    }

}
