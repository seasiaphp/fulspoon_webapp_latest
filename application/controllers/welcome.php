<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->model('menu_model');
        $this->load->model('member_model');




        if ($_SERVER['REQUEST_METHOD'] == 'POST') {




            $data_1 = $this->member_model->admin_contratct();



            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;

            $message = $this->member_model->get_email("Notify_email");

            $message1 = $message['email_template'];
            $subject = $message['email_subject'];
            $message1 = str_replace("#baseurl#", base_url(), $message1);
            $this->load->library('email');
            $this->email->initialize($config);

            $this->email->from($data_1['email']);
            $list_123 = array($_POST['fild_email']);
//            $this->email->to($list_123);
////            $this->email->to($this->config->item('email_from'));
//            $this->email->subject($subject);
//            $this->email->message($message1);
//            $this->email->send();

            $category = "Member";
            $to_mail = $list_123;
            $from = $data_1['email'];
            $pjt_name = $data_1['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject, $message1, $category, $from, $pjt_name);
            $this->menu_model->insert('notify_mail', array('email' => $_POST['fild_email'], 'zipcode' => $_POST['zip_fild']));
            echo ('success');
            //redirect("welcome");
        }
        $this->load->helper('url');
        $data['get_package_details'] = $this->menu_model->get_package_details();

        $data['get_package_details'][0]['app-id'] = $this->config->item('app-id');

        $data['get_package_details'][0]['appstore-id'] = $this->config->item('appstore-id');

        //$this->load->view('welcome_message');
        //$this->load->view('welcome_message', $data);
        //print_r($data);
        $this->load->view('welcome_message', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */