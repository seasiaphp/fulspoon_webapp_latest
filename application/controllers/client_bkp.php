<?php

error_reporting(1);
ini_set("display_errors", "on");
ini_set("memory_limit", "256M");

class Client extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('client_model');
        $this->load->model('member_model');
    }

    public function index() {
        if (isset($HTTP_RAW_POST_DATA)) {
            $json = $HTTP_RAW_POST_DATA;
        } else {
            $json = implode("\r\n", file('php://input'));
        }


        /*  $json='{"function":"subscription_pay", "parameters":{"order_data": {"user_id": 9,
          "add_date": "2015-12-01",
          "count_restaurants": 2,
          "discount": "22.00",
          "email": "asd@as123d.asd",
          "from_date": "2015-12-01",
          "number_end": "3",
          "number_start": "1",
          "password": "asdasd",
          "pay_token": "tok_17EM5dE6RvePU8B8dmP6uYaO",
          "reg_method": "general",
          "rest_details": {"0":"ChIJi9BURJFZwokRDcqvIgxdXds","1":"ChIJP_DysOT1wokRbj86nKeyM2M"},
          "sub_package_id": "21",
          "to_date": "2015-12-01",
          "user_fee": "50.00"
          }}}'; */
        /* $json='{"function":"get_discount_detail", "parameters":{member_id: "341"}}'; */




        $json = preg_replace("{\\\}", "", $json);
        $array = json_decode($json, TRUE);
        //$token = $array['token'];
        $function_name = $array['function'];
        $this->$function_name($array['parameters']);
    }

    #Authentication function to check by v(version of API),apv - version of application for statistical purposes,authentication key,session key
    #Format example: A-1.0 (A for Android, I fro iOS, W for web))

    function auth_url($arr) {
        $this->load->model('user_model');
        if ($arr['parameters']['v'] == '') {
            $ar = array('result' => 'null', 'errorMessage' => 'missing API version', 'errorCode' => '1790');
            echo(json_encode($ar));
            exit;
        } else if ($arr['parameters']['apv'] == '') {
            $ar = array('result' => 'null', 'errorMessage' => 'missing API version', 'errorCode' => '1790');
            echo(json_encode($ar));
            exit;
        } else if ($arr['parameters']['authKey'] == '') {
            $ar = array('result' => 'null', 'errorMessage' => ' unauthorized to use API (wrong authKey)', 'errorCode' => '1810 ');
            echo(json_encode($ar));
            exit;
        } else if ($arr['parameters']['function'] != 'openSession' && $arr['parameters']['sessionKey'] == "") {
            $ar = array('result' => 'null', 'errorMessage' => 'Session has expired!', 'errorCode' => '1080');
            echo(json_encode($ar));
            exit;
        }
        return;
    }

    #Function to get session key

    function openSession($arr) {
        $this->load->model('user_model');

        #delete entry with the device token
        $this->user_model->deleteSessionEntry($arr['device_token']);
        $preferences = $this->user_model->getPreferences();

        if ($preferences['version'] == '1')
            $preferences['version'] = '1.0';
        else
            $preferences['version'] = '1';
        $preferences['session_id'] = $this->randomSessionId();

        #update session ID
        $this->user_model->updateSessionID($arr['device_token'], $preferences['session_id']);
        echo json_encode($preferences);
    }

    function GAcode() {
        $this->load->model('user_model');
        $data = $this->user_model->getGa();
        $result = array('status' => 'true', 'message' => ' Successful', 'GAcode' => $data['value']);
        echo json_encode($result);
    }

    #function for facebook login using member id which is used to get the profile information

    function facebookLogin() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $data = $this->user_model->facebookExist($arr['id']);
        if (!$data['facebook_id']) {
            $data_mail = $this->user_model->facebookExist_email($arr);
            if (!$data_mail['email']) {
                $result = array('status' => 'false', 'message' => 'facebook id not exist ');
                echo json_encode($result);
            } else {
                if ($data_mail['profile_image'] != '') {
                    $data_mail['profile_image'] = base_url() . "uploads/members/" . $data_mail['member_id'] . "_thumb." . $data_mail['profile_image'] . '?' . date("his");
                }
                $result = array('status' => 'true', 'message' => 'Fb Login Successful', 'data' => $data_mail);
                echo json_encode($result);
            }
        } else {
            $data_mail['profile_image'] = base_url() . "uploads/members/" . $data_mail['member_id'] . "_thumb." . $data_mail['profile_image'] . '?' . date("his");
            $result = array('status' => 'true', 'message' => 'Fb Login Successful', 'data' => $data);
            echo json_encode($result);
        }
    }

    # APP Registration Function using post method and arguements used DOB,Fname,Pname,Lname,member_type,phone,Email

    function registration() {
        $this->load->model('user_model');
        $arr = array(
            'DOB' => $_POST['DOB'],
            'Fname' => $_POST['Fname'],
            'Pname' => $_POST['Pname'],
            'Lname' => $_POST['Lname'],
            'member_type' => $_POST['member_type'],
            'phone' => $_POST['phone'],
        );

        $get_data = $this->user_model->checkEmailExist($arr['Email']);

        if (!$get_data['Fname']) {

            $user_id = $this->user_model->createUser($arr);


            if ($_FILES['image']['name'] <> "") {
                #################  Upload ############

                $imagename = strtotime(date("Y-m-d H:i:s"));
                $tempFile = $_FILES['image']['tmp_name'];
                $fileParts = pathinfo($_FILES['image']['name']);
                $image_name = $user_id . '.' . $fileParts['extension'];
                $foldername = "upload/members/$image_name";
                $targetFolder = 'upload/members';
                $targetFile = realpath($targetFolder) . '/' . $image_name;
                move_uploaded_file($tempFile, $targetFile);
                //chmod('upload/members/' . $user_id . $extension, 757);
                $imagethumb = "upload/members/" . $user_id . "_thumb." . $fileParts['extension'];
                //$imagethumb1 = "upload/members" . $user_id . "_thumb1." . $fileParts['extension'];
                list($width, $height, $type, $attr) = getimagesize($targetFile);
                $configThumb = array();
                $configThumb['image_library'] = 'gd2';
                $configThumb['source_image'] = $targetFile;
                $configThumb['new_image'] = $imagethumb;
                $configThumb['create_thumb'] = false;
                $configThumb['maintain_ratio'] = TRUE;
                $configThumb['width'] = 320;
                $configThumb['height'] = 320;

                $this->load->library('image_lib');
                $this->image_lib->initialize($configThumb);
                $this->image_lib->resize();
                chmod('upload/members/' . $user_id . "_thumb." . $extension, 757);
                $this->user_model->updateMemberImage($fileParts['extension'], $user_id);
            }

            $result = array('status' => 'true', 'message' => 'User Account created successfully', 'user_id' => $user_id, 'username' => $arr['Fname'], 'phone' => $arr['phone']);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false', 'message' => 'Email Address/Phone no: already exist ');
            echo json_encode($result);
        }
    }

    #App login function for login purpose and arguements used email and password

    function Login($arr) {
        if (!isset($arr['email'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        #Normal Login
        $result = $this->user_model->userLogin($arr);
        if ($result) {
            if ($result['profile_image'] != '') {
                if ($result['profile_image']) {
                    $result['profile_image'] = base_url() . "uploads/members/" . $result['member_id'] . "_thumb." . $result['profile_image'] . '?' . date("his");
                }
            }
            $result = array('status' => 'true', 'email' => $arr['email'], 'message' => 'Login Successful', 'data' => $result);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false', 'message' => "Incorrect Username or Password");
            echo json_encode($result);
        }
    }

    #function to check whether email does exist and arguement used is email

    function registration_Exist($arr) {
        if (!isset($arr['email'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        $get_data = $this->user_model->checkEmailExist($arr['email']);
        if (sizeof($get_data)) {
            $result = array('status' => 'false', 'message' => "Email Exist");
            echo json_encode($result);
        } else {
            $result = array('status' => 'true', 'message' => "Email Not Exist");
            echo json_encode($result);
        }
    }

    # function to check how much packages provided by each restuarant selected by customer and the only arguement used is count

    function get_subscription_detail($arr) {
        if (!isset($arr['count'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        $get_data = $this->user_model->get_subscription_detail($arr['count']);
        //$get_data['count_restaurants']=$arr['count'];
        if (sizeof($get_data) > 1) {
            $status = 'false';
        } else {
            if ($get_data[0]['number_of_restaurants'] != $arr['count']) {
                $status = 'false';
            } else {
                $status = 'true';
            }
        }
        $result = array('status' => $status, 'data' => $get_data);
        echo json_encode($result);
    }

    # function to fetch admin details 

    function get_admin_detail() {
        $this->load->model('user_model');
        $get_data = $this->user_model->get_Admin_detail();
        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    # function to fetch whether a customers subscription has been expired and fetch the past order details. Arguements used member_id

    function get_user_status_detail($arr) {
        if (!isset($arr['member_id'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');

        $this->user_model->update_new_UserDatas($arr);
        $cond = array(member_id => $arr['member_id']);
        $get_data = $this->user_model->get_where('member_master', $cond, 'member_id');
        $historySBscription = $this->user_model->historySBscription($arr['member_id']);
        if ($historySBscription['subscription_id'] == '') {
            $pending_sub = 'N';
        } else {
            $pending_sub = 'Y';
        }


        $expired = $this->user_model->get_discount_detail($arr['member_id']);
        if ($expired['subscription_id'] != '') {
            $ex_status = 'N';
        } else {
            $ex_status = 'Y';
            $package_details = $this->user_model->get_package_details($expired['package_id']);
        }
        $orderList = $this->user_model->getAllOders($arr['member_id']);
        if ($orderList[0]['order_id'] != '') {
            $past_order = 'Y';
        } else {
            $past_order = 'N';
        }
        if ($get_data['profile_image'] != '') {
            $get_data['profile_image'] = base_url() . "uploads/members/" . $get_data['member_id'] . "_thumb." . $get_data['profile_image'] . '?' . date("his");
        }
        if ($get_data['status'] == 'Y') {
            $status = 'Y';
        } else {
            $status = 'N';
        }
        $result = array('status' => $status, 'data' => $get_data, 'expired' => $ex_status, 'past_order' => $past_order, 'package_details' => $package_details, 'pending_sub' => $pending_sub);
        echo json_encode($result);
    }

    # function to fetch restuarant details such as cuisines details and arguement used is long

    function get_restaurant_detail($arr) {
        if (!isset($arr['long'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        $get_data = $this->user_model->get_restaurant_detail($arr);
        $get_data_config = $this->user_model->config_filter();
        foreach ($get_data as $key => $cuisine) {
            $get_data[$key]['cuisines'] = $this->user_model->get_restaurant_cuisine_detail($cuisine['restaurant_id']);
        }
        $result = array('status' => 'true', 'data' => $get_data, 'filter' => $get_data_config);
        echo json_encode($result);
        //echo json_encode($get_data);
    }

    #function to fetch users restuarant details with cuisine details using arguement user_id

    function get_user_restaurant_detail($arr) {
        if (!isset($arr['user_id'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        $get_data = $this->user_model->get_user_restaurant_detail($arr['user_id']);
        foreach ($get_data as $key => $cuisine) {
            $get_data[$key]['cuisines'] = $this->user_model->get_restaurant_cuisine_detail($cuisine['restaurant_id']);
        }
        if (sizeof($get_data) == 0) {
            $get_data = 'no_rests';
        }
        echo json_encode($get_data);
    }

    #function to fetch users restuarant details using arguement user_id

    function get_user_restaurant_details($arr) {

        $this->load->model('user_model');
        $get_data = $this->user_model->get_user_restaurant_detail($arr['user_id']);

//        foreach ($get_data as $key => $cuisine) {
//            $get_data[$key]['cuisines'] = $this->user_model->get_restaurant_cuisine_detail($cuisine['restaurant_id']);
//        }
//        echo json_encode($get_data);
        return $get_data;
    }

    # function for send push notifications to android devices

    public function sendPushNotification_ANDROID($message = '') {
        // include config
        // Set POST variables
        $message['notId'] = crc32(mt_rand());
        $url = 'https://android.googleapis.com/gcm/send';
        $registrationIDs = array("APA91bGwxM2BouyqoV8_62A3jQb1f5t00nJPtv8HJWimxQy1ZiDGjN65nW1zOfoXRwU2-wb1CPFuEUtvJh-vfL4LIj01gTKBa9fkqkqxvPKhrYrxdJv3bo2G49jQKDMqN0uzB7Bjxkf3");
        $fields = array(
            'registration_ids' => $registrationIDs,
            'data' => $message,
        );

        /*        $headers = array(
          'Authorization: key=AIzaSyAJk9yuOe9E9Babuv2rUs6isF5x8b5tm2A',
          'Content-Type: application/json'
          ); */
        $headers = array(
            'Authorization: key=AIzaSyDD5HCo8sdJmNrHi7zQCzYPJJCFFVPW-Mo',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
//        return $result;
//        //print_r($fields);
        echo $result;
    }

    #Function for editing user profile arguements used member_id

    function edit_profile_user($arr) {
        if (!isset($arr['member_id'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        $get_data = $this->user_model->edit_profile_user($arr);
        $get_data_user = $this->user_model->get_userdetails($arr['member_id']);
        if ($get_data_user['profile_image'] != '') {
            $get_data_user['profile_image'] = base_url() . "uploads/members/" . $get_data_user['member_id'] . "_thumb." . $get_data_user['profile_image'] . '?' . date("his");
        }
        $result = array('status' => 'true', 'user_data' => $get_data_user);
        echo json_encode($result);
    }

    ##########################function for stripe payment and arguements used rest_details,lat,long,order_data,password,auth_type,name_on_card,email,profile_image,device_id,user_fee

    public function subscription_pay() {

        //print_r($_POST);
        $arr = json_decode(file_get_contents('php://input'), true);

        //$arr =$_POST;

        /* active: "Y"
          add_date: "2015-12-01"
          count_restaurants: 2
          discount: "22.00"
          email: "asd@asd.asd"
          from_date: "2015-12-01"
          number_end: "3"
          number_start: "1"
          password: "asdasd"
          pay_token: "tok_17DaOJE6RvePU8B89mlOXgo7"
          reg_method: "general"
          rest_details: "ChIJi9BURJFZwokRDcqvIgxdXds,ChIJP_DysOT1wokRbj86nKeyM2M"
          sub_package_id: "21"
          to_date: "2015-12-01"
          user_fee: "50.00" */

        $this->load->model('client_model');
        $this->load->library('stripe_lib');
        $stripe = $this->client_model->getAdmin_strip();
        $data = $arr;
        $array['payment_data'] = $data['order_data'];
        $restaurant = $data['order_data']['rest_details'];


        $pjt_name = $stripe['project_name'];
        if ($data != '') {

            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            //print_r($stripe['stripe_private_key']);


            if ($array['payment_data']['lat'] != '') {


                $deal_lat = $array['payment_data']['lat'];
                $deal_long = $array['payment_data']['long'];
                $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $deal_lat . ',' . $deal_long . '&sensor=false');

                $output = json_decode($geocode);

                for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
                    $cn = array($output->results[0]->address_components[$j]->types[0]);
                    if (in_array("locality", $cn)) {
                        $city = $output->results[0]->address_components[$j]->long_name;
                    }
                    if (in_array("country", $cn)) {
                        $country = $output->results[0]->address_components[$j]->long_name;
                    }
                    if (in_array("postal_code", $cn)) {
                        $zipcode = $output->results[0]->address_components[$j]->long_name;
                    }
                    if (in_array("administrative_area_level_1", $cn)) {
                        $state = $output->results[0]->address_components[$j]->long_name;
                    }
                }
            }


            $arr_user = array("email" => $array['payment_data']['email'],
                "password" => md5($array['payment_data']['password']),
                "created_date" => date('Y-m-d H:i'),
                "auth_type" => $array['payment_data']['auth_type'],
                "first_name" => $array['payment_data']['name_on_card'],
                "billing_status" => 'N',
                "profile_image" => $array['payment_data']['profile_image'],
                "city" => $city,
                "state" => $state,
                "zipcode" => $zipcode,
                "device_id" => $array['payment_data']['device_id'],
                "lat" => $array['payment_data']['lat'],
                "long" => $array['payment_data']['long'],
            );
            $new_userid = $this->client_model->insert_user($arr_user);


            if ($array['payment_data']['profile_image']) {
                $image_url = $array['payment_data']['profile_image'];
                $extension = "jpg";
                $imagename = $new_userid . '_thumb.' . $extension;
                list($width, $height) = getimagesize($image_url);
                $image = file_get_contents($image_url); // sets $image to the contents of the url
                file_put_contents('uploads/members/' . $imagename, $image);
                $targetFile = realpath('uploads/members') . "/" . $imagename;
                chmod(base_url() . 'uploads/members/' . $imagename, 757);
                $up_arr = array("img_extension" => $extension);
                $img_1 = $extension;
                $this->user_model->profile_image($img_1, $new_userid);
            }


            $admin_args = array(
                'description' => "Customer for " . $array['payment_data']['email'],
                'email' => $array['payment_data']['email'],
                'source' => $array['payment_data']['pay_token']
            );

            $customer = $this->stripe_lib->createCustomer($admin_args);
            $customer_strip_id = $customer->id;

            $last_4digit = $array['payment_data']['last4'];
            $brand = $array['payment_data']['brand'];
            $owner_payment = array(
                'amount' => bcmul($array['payment_data']['user_fee'], 100),
                'currency' => 'usd',
                'customer' => $customer_strip_id
            );
            $retData = $this->stripe_lib->chargeCustomer($owner_payment);
            $response = mysql_real_escape_string(serialize($retData));
            $total = bcdiv($retData->amount, 100, 3);

            $no = rand(000001, 999999);

            //$array['payment_data']['brand']="VISA";
            //$array['payment_data']['last4']="5544";

            $arr12 = array("member_id" => $new_userid,
                "strip_customer_id" => $customer_strip_id,
                "created_time" => date('Y-m-d H:i'),
                "last_4digit" => $array['payment_data']['last4'],
                "brand" => $array['payment_data']['brand'],
                "easy_name" => $array['payment_data']['easy_name'],
                "name" => $array['payment_data']['name_on_card'],
                "strip_order_id" => $retData->id,
                "amount" => $array['payment_data']['user_fee']
            );
            $subscription_id = $this->client_model->insert_member_stripid_map($arr12);

            $User_subscription_master = array("member_id" => $new_userid,
                "strip_id" => $subscription_id,
                "package_id" => $array['payment_data']['sub_package_id']);
            $user_sub_id = $this->user_model->insert_1('User_subscription_master', $User_subscription_master);

            //get user details in subscription_log 
            //$user_details_log=$this->user_model->user_details_log($new_userid);
            //get package details
            $get_package_details = $this->user_model->get_package_details($array['payment_data']['sub_package_id']);
            $User_subscription_log = array("member_id" => $new_userid,
                "start_date" => date('Y-m-d'),
                "End_date" => Date('Y-m-d', strtotime("+" . $get_package_details['durn'])),
                "subscription_id" => $user_sub_id,
                "strip_id" => $subscription_id);
            $user_log_id = $this->user_model->insert_1('subscription_log', $User_subscription_log);




            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;

            foreach ($restaurant as $row) {
                $arr = array("member_id" => $new_userid,
                    "strip_id" => $subscription_id,
                    "subscription_id" => $user_sub_id,
                    "updated_on" => date('Y-m-d H:i'),
                    "restaurant_id" => $row
                );
                $this->user_model->insert_1('current_restaurant', $arr);

                $owner = $this->user_model->get_restaurant_food_detail($row);
                $New_Subscriber_for_owner = $this->member_model->get_email("New_Subscriber_for_owner");
                $message2 = $New_Subscriber_for_owner['email_template'];
                $subject2 = $New_Subscriber_for_owner['email_subject'];
                $message2 = str_replace('#baseurl#', base_url(), $message2);
                $message2 = str_replace('#OwnerName#', $owner['restaurant_name'], $message2);
                $message2 = str_replace('#YEAR#', date("Y"), $message2);
                $message2 = str_replace('#SubscriberID#', $new_userid, $message2);
                $message2 = str_replace('#SubscriberName#', $array['payment_data']['name_on_card'], $message2);
                $this->load->library('email');
                $this->email->initialize($config);

                $this->email->from($stripe['email'], $pjt_name);
                $this->email->to($owner['email']);
                $this->email->bcc("gauravbazaz@gmail.com");
                $this->email->subject($subject2);
                $this->email->message($message2);
//                $this->email->send();
                $category = "Subscription";
                $to_mail = $array['payment_data']['email'];
                $from = $stripe['email'];
//            $pjt_name = $data_1['project_name'];
                $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
            }

            #send email


            $email = $this->member_model->get_email("User_Registration");
            $data_1 = $this->member_model->admin_contratct();
            $message1 = $email['email_template'];
            $subject = $email['email_subject'];
            $message1 = str_replace('#baseurl#', base_url(), $message1);
            $message1 = str_replace('#name#', $array['payment_data']['name_on_card'], $message1);
            $message1 = str_replace('#subject#', $subject, $message1);
            $message1 = str_replace('#YEAR#', date("Y"), $message1);

//            echo $message1;
//            exit;
//            $message1 = "Hi " . $name . ",<br/>Yor Login Credentials are following : <br/>User Name : " . $email . "<br/> Password: " . $password;
            $this->load->library('email');
            $this->email->initialize($config);
            $pjt_name = $stripe['project_name'];
            $this->email->from($stripe['email'], $pjt_name);
            $this->email->to($array['payment_data']['email']);
//            $this->email->to($this->config->item('email_from'));
            $this->email->bcc("gauravbazaz@gmail.com");
            $this->email->subject($subject);
            $this->email->message($message1);
//            $this->email->send();
            $category = "Subscription";
            $to_mail = $array['payment_data']['email'];
            $from = $stripe['email'];
//            $pjt_name = $data_1['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject, $message1, $category, $from, $pjt_name);


            ##############
            /*  $config['mailtype'] = 'html';
              $config['charset'] = 'utf-8';
              $config['crlf'] = PHP_EOL;
              $config['newline'] = PHP_EOL;
              $this->load->library('email');
              $this->email->initialize($config);

              $this->email->from($stripe['email']);
              $this->email->reply_to($stripe['email']);
              $this->email->to($array['payment_data']['email']);
              //$this->email->to("bibinv@newagesmb.com");
              $this->email->subject("Welcome to Fulspoon");
              $message = "<p>Thank you for registering with  Fulspoon </p>
              <p>Email address : '" . $array['payment_data']['email'] . "' <br></p>
              <p>&nbsp;</p>
              <p>Thanks,</p>
              <p>Fulspoon </p>";
              $this->email->message($message);
              $this->email->send(); */

            $result = array('status' => 'true', 'message' => $this->config->item('saveOrder'), 'user_id' => $new_userid);
        } else {
            $result = array('status' => 'error', 'message' => $this->config->item('invalidRequest'));
        }
        echo json_encode($result);
        //echo '<pre>';print_r($sides);echo'<pre>';exit;		 
    }

    #function for saving billing details arguements used phone,user_id,address1,city,state,zip,address2,addrname,phone

    function save_billing_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $this->user_model->update_phone($arr['bill_data']['phone'], $arr['bill_data']['user_id']);
        $arr_1 = array("member_id" => $arr['bill_data']['user_id'],
            "address1" => $arr['bill_data']['address1'],
            "city" => $arr['bill_data']['city'],
            "state" => $arr['bill_data']['state'],
            "zipcode" => $arr['bill_data']['zip'],
            "active" => 'Y',
            "billing_date" => date('Y-m-d H:i'),
            "address2" => $arr['bill_data']['address2'],
            "easy_name" => $arr['bill_data']['addrname'],
            "phone" => $arr['bill_data']['phone']
        );
        $this->db->insert('billing_master', $arr_1);
        $get_strip = $this->user_model->getUser_strip_details($arr['bill_data']['user_id']);
        $address = array("member_id" => $arr['bill_data']['user_id'],
            "stripeid" => $get_strip['id'],
            "address" => $arr['bill_data']['address1'] . ", " . $arr['bill_data']['address2'],
            "pincode" => $arr['bill_data']['zip'],
            "state" => $arr['bill_data']['state'],
            "city" => $arr['bill_data']['city'],
            "save_as" => $arr['bill_data']['addrname'],
            "phone" => $arr['bill_data']['phone']
        );
        $this->db->insert('address', $address);
        $billing_satus = $this->user_model->updateBilling_status($arr_1);
        $result_1 = $this->user_model->get_userdetails($arr['bill_data']['user_id']);
        if ($result_1 ['profile_image'] != '') {
            $result_1 ['profile_image'] = base_url() . "uploads/members/" . $arr['bill_data']['user_id'] . "_thumb." . $result_1 ['profile_image'];
        }
        $result = array('status' => 'true', 'message' => 'billing data inserted successfully', 'data' => $result_1);
        echo json_encode($result);
    }

#function to fetch  details of all cuisines 

    function get_full_cuisine_detail() {
        $this->load->model('user_model');
        $get_data = $this->user_model->get_full_cuisine_detail();
        foreach ($get_data as $cuisine) {
            $cuisine_data[] = $cuisine['cuisine_name'];
        }
        echo json_encode($cuisine_data);
    }

#function to generate random password, sub function of generate_randomCode

    function randomPassword() {
        $alphabet = "0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

#function to generate random password using arguement email 

    function generate_randomCode() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->checkEmailExist($arr['email']);
        if ($get_data) {
            //send email
            $new_password = $this->randomPassword();
            # generate random password
            ## send new email address
            $dtls['new_password'] = $new_password;
            $admin_email = $this->user_model->getAdminEmail();
            $this->user_model->update_randomPassword_code(md5($new_password), $get_data['member_id']);
            //$site = $this->users_model->getWebsite();
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['crlf'] = PHP_EOL;
            $config['newline'] = PHP_EOL;
//            $this->load->library('email');
//            $this->email->initialize($config);
//
//            $this->email->from($admin_email['email']);
//            $this->email->reply_to($admin_email['email']);
//            $this->email->to($arr['email']);
            //$this->email->to("bibinv@newagesmb.com");
//            $this->email->bcc("gauravbazaz@gmail.com");
//            $this->email->subject("Forgot Password");
            $message = "<p>Your account details are </p>
<p>Email address : '" . $arr['email'] . "' <br>
  Random Code : '" . $new_password . "'</p>
<p>&nbsp;</p>
<p>Thanks,</p>
<p>Fulspooon Team </p>";
            $this->email->message($message);
//            $this->email->send();
//            
//            
            ## send new email address
            $subject = 'Forgot Password';
            $category = "Member";
            $to_mail = $arr['email'];
            $from = $admin_email['email'];
            $pjt_name = $data_1['project_name'];
            $this->member_model->mail_sendgrid($to_mail, $subject, $message, $category, $from, $pjt_name);
            $result = array('status' => 'true', 'message' => 'Your new password has been sent to the email address');
            echo json_encode($result);
        } else {
            $result = array('status' => 'false', 'message' => 'Email not exist');
            echo json_encode($result);
        }
    }

    #function to check passwordcode using arguement code

    function check_Psdcode() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->check_Psdcode(md5($arr['code']));
        if ($get_data) {
            $result = array('status' => 'true', 'data' => $get_data);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

    #function to update new password using arguement member_id and password

    function update_new_password() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->update_new_password($arr['member_id'], $arr['password']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    #function to fetch food details of a particular restuarant using arguement restuarant_id

    function get_restaurant_food_detail($arr) {
        if (!isset($arr['restaurant_id'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        //161 162 163 165 158 157 155 154 153 151
        //$arr['restaurant_id']=151;
        $this->load->model('user_model');

        //$get_data = $this->user_model->get_restaurant_food_detail($arr['restaurant_id']);

        $get_menu = $this->user_model->get_menu_detail($arr['restaurant_id']);

        foreach ($get_menu as $key => $menu_detail) {
            $get_menu[$key]['dish'] = $this->user_model->get_dish_detail($menu_detail['id'], $arr['restaurant_id']);
        }


        /* 	$id=$get_data['locu_id'];

          //$id="47bcb07e6e1629065984";
          error_reporting(E_ALL);
          $api_key = "3cb4cf1278ee93a6807eb2c7e30627ee97f60f08";
          $uri = "curl -X POST https://api.locu.com/v2/venue/search -d '{
          \"api_key\" : \"$api_key\",
          \"fields\" : [\"name\", \"menus\" ],
          \"venue_queries\" : [
          { \"locu_id\":\"$id\" }

          ]
          }'";
          //        echo $uri;
          $c_pid = exec($uri);
          // echo "<pre>"; print_r(json_decode($c_pid)->venues[0]->menus[0]);
          if(isset(json_decode($c_pid)->venues[0]->menus[0]))
          {
          $status='true';
          }
          else
          {
          $status='false';
          }

          //echo $status;

          $result = array('status' => $status,'details'=>json_decode($c_pid)); */
        if (sizeof($get_menu)) {
            $status = 'true';
        } else {
            $status = 'false';
        }
        //echo "<pre>"; print_r($get_menu);
        //exit;
        $result = array('status' => $status, 'details' => $get_menu);
        echo json_encode($result);
    }

    #function to fetch the details of an item using arguement item_id

    public function getRestaurantMenuDetail1($array = '') {

        //$check_array='{"function":"getRestaurantMenuDetail", "parameters": {"item_id": "80"}}';
        $array = json_decode(file_get_contents('php://input'), true);


        //$array 	=	 json_decode($check_array, true);
        //$array=$array['parameters'];
        //echo '<pre>';print_r($array);exit;
        if ($array != '') {
            //$data['id'] = 12;
            $result_array = $this->user_model->getRestaurantMenuDetail($array['item_id']);
            //echo '<pre>';print_r($result_array);
            //if($array['member_id'])
            //	$check_fav  = $this->user_model->isFavItem($array['member_id'],$array['item_id']);

            $sizeresult = $this->user_model->getRestaurantMenuSizeDetail($array['item_id']);
            //echo '<pre>';print_r($result_array);
            //echo '<pre>';print_r($sizeresult);exit;


            $category = array();

            if (!empty($result_array)) {
                $i = 1;
                $sort_order = 1;
                foreach ($result_array as $val) {
                    $menu['category_id'] = $val['menu'];
                    $menu['item_name'] = $val['dish_name'];
                    $menu['item_id'] = $val['id'];
                    $menu['item_description'] = nl2br($val['description']);

                    //$menu['is_fav'] = $check_fav;


                    if ($val['option_id'] != '') {


                        $menu['option_id'][$sort_order] = $val['option_id'];
                        $menu['option_name'][$val['option_id']] = $val['option_name'];
                        $menu['is_mandatory'][$val['option_id']] = $val['mandatory'];
                        $menu['is_multiple'][$val['option_id']] = $val['multiple'];
                        $menu['multiple_limit'][$val['option_id']] = $val['limit'];
                        $menu['sortorder'][$val['option_id']] = $val['sortorder'];

                        if ($val['side_id'] != '') {

                            $side_sort_order = $val['sidesortorder'];
                            $menu['side_id'][$val['option_id']][$side_sort_order] = $val['side_id'];
                            $menu['side_item'][$val['option_id']][$val['side_id']] = $val['side_item'];
                            $menu['side_price'][$val['option_id']][$val['side_id']] = $val['side_price'];
                        } else {
                            $menu['side_id'][$val['option_id']] = array();
                            $menu['side_item'][$val['option_id']] = array();
                            $menu['side_price'][$val['option_id']] = array();
                        }
                        $i++;
                        $sort_order++;
                    } else {
                        $menu['option_id'] = array();
                        $menu['option_name'] = array();
                        $menu['is_mandatory'] = array();
                        $menu['side_id'] = array();
                        $menu['side_item'] = array();
                        $menu['side_price'] = array();
                    }
                }
                $menu['sizes'] = $sizeresult;
                $result = array('status' => 'success', 'result' => $menu);
            } else {
                $result = array('status' => 'error', 'message' => $this->config->item('invalidLocation'));
            }
        } else {
            $result = array('status' => 'error', 'message' => $this->config->item('invalidRequest'));
        }

        echo json_encode($result);
    }

    # test function

    public function getRestaurantMenuDetail($array = '') {

        //$check_array='{"function":"getRestaurantMenuDetail", "parameters": {"item_id": "80"}}';
        $array = json_decode(file_get_contents('php://input'), true);
//$array['item_id']=1629;
// $array=json_decode('{"item_id":"4395","category_id":"545","rest_id":"154"}', true); 
        //$array 	=	 json_decode($check_array, true);
        //$array=$array['parameters'];
        //echo '<pre>';print_r($array);exit;
        if ($array != '') {
            //$data['id'] = 12;
            $result_array = $this->user_model->getRestaurantMenuDetail($array['item_id']);
            //echo '<pre>';print_r($result_array);
            //if($array['member_id'])
            //	$check_fav  = $this->user_model->isFavItem($array['member_id'],$array['item_id']);

            $sizeresult = $this->user_model->getRestaurantMenuSizeDetail($array['item_id']);
            //  echo '<pre>';print_r($result_array);
            //echo '<pre>';print_r($sizeresult);exit;


            $category = array();

            if (!empty($result_array)) {
                $i = 1;
                $sort_order = 1;
                foreach ($result_array as $val) {
                    $menu['category_id'] = $val['menu'];
                    $menu['item_name'] = $val['dish_name'];
                    $menu['item_id'] = $val['id'];
                    $menu['item_description'] = nl2br($val['description']);

                    //$menu['is_fav'] = $check_fav;


                    if ($val['option_id'] != '') {


                        $menu['option_id'][$val['option_id']] = $val['option_id'];
                        $menu['option_name'][$val['option_id']] = $val['option_name'];
                        $menu['is_mandatory'][$val['option_id']] = $val['mandatory'];
                        $menu['is_multiple'][$val['option_id']] = $val['multiple'];
                        $menu['multiple_limit'][$val['option_id']] = $val['limit'];
                        $menu['sortorder'][$val['option_id']] = $val['sortorder'];

                        if ($val['side_id'] != '') {

                            $side_sort_order = $val['sidesortorder'];
                            $menu['side_id'][$val['option_id']][$side_sort_order] = $val['side_id'];
                            $menu['side_item'][$val['option_id']][$val['side_id']] = $val['side_item'];
                            $menu['side_price'][$val['option_id']][$val['side_id']] = $val['side_price'];
                        } else {
                            $menu['side_id'][$val['option_id']] = array();
                            $menu['side_item'][$val['option_id']] = array();
                            $menu['side_price'][$val['option_id']] = array();
                        }
                        $i++;
                        $sort_order++;
                    } else {
                        $menu['option_id'] = array();
                        $menu['option_name'] = array();
                        $menu['is_mandatory'] = array();
                        $menu['side_id'] = array();
                        $menu['side_item'] = array();
                        $menu['side_price'] = array();
                    }
                }
                $menu['sizes'] = $sizeresult;
                //echo "<pre>";print_r($menu);
                $result = array('status' => 'success', 'result' => $menu);
            } else {
                $result = array('status' => 'error', 'message' => $this->config->item('invalidLocation'));
            }
        } else {
            $result = array('status' => 'error', 'message' => $this->config->item('invalidRequest'));
        }

        echo json_encode($result);
    }

    #function to get details of discount given for a particular customer in a restuarant and arguements used are member_id and restuarant_id 

    function get_discount_detail() {
        $arr = json_decode(file_get_contents('php://input'), true);

        $this->load->model('user_model');
        $get_data = $this->user_model->get_discount_detail($arr['member_id']);
        $get_data['address'] = $this->user_model->get_address_user($arr['member_id']);
        $get_data['card'] = $this->user_model->get_user_carddetails($arr['member_id']);
        $order_detail = $this->user_model->delivery_time($arr['restaurant_id']);
        $get_data['delivery_fee'] = $order_detail['delivery_fee'];
        $get_data['tax'] = $order_detail['tax'];
        $get_data['delivery_radius'] = $order_detail['areas_delivery'];
        $get_data['restaurant_details'] = $order_detail;
        //$get_data['tax'] = 8;
        //$get_data['delivery_radius'] = 10;
        $get_data['delivery_time'] = $order_detail['delivery_time'];
        $get_data['pickup_time'] = $order_detail['pickup_time'];
        $get_data['minimum_order'] = $order_detail['minimum_order'];
        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    ###############for web services################
    # function to check how much packages provided by each restuarant selected by customer and the only arguement used is count for website

    function get_subscription_detail_web($count) {

        $this->load->model('user_model');
        $get_data = $this->user_model->get_subscription_detail($count);
        //$get_data['count_restaurants']=$arr['count'];
        if (sizeof($get_data) > 1) {
            $status = 'false';
        } else {
            if ($get_data[0]['number_of_restaurants'] != $count) {
                $status = 'false';
            } else {
                $status = 'true';
            }
        }
        $result = array('status' => $status, 'data' => $get_data);
        echo json_encode($result);
    }

    #################################
    #Function for fetching complete address of the user arguement used member_id

    function get_alladdress_user($arr) {
        if (!isset($arr['member_id'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model('user_model');
        $get_data = $this->user_model->get_alladdress_user($arr['member_id']);
        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

    #function to fetch the details of all card used by the user and arguement used is member_id

    function get_user_carddetails_all() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->model('user_model');
        $get_data = $this->user_model->get_user_carddetails_all($arr['member_id'], $all_addr = 'all');

        $result = array('status' => 'true', 'data' => $get_data);
        echo json_encode($result);
    }

#function to add the details of  used by the user and arguement used are email, card_token, member_id,brand,easy_name,name_on_card

    function add_user_carddetails() {

        $arr = json_decode(file_get_contents('php://input'), true);
        $this->load->library('stripe_lib');
        $stripe = $this->client_model->getAdmin_strip();
        $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
        $admin_args = array(
            'description' => "Customer for " . $arr['email'],
            'email' => $arr['email'],
            'source' => $arr['card_token']
        );


        $customer = $this->stripe_lib->createCustomer($admin_args);
        $customer_strip_id = $customer->id;

        $arr12 = array("member_id" => $arr['member_id'],
            "strip_customer_id" => $customer_strip_id,
            "created_time" => date('Y-m-d H:i'),
            "last_4digit" => $arr['last4'],
            "brand" => $arr['brand'],
            "easy_name" => $arr['easy_name'],
            "name" => $arr['name_on_card'],
            "strip_order_id" => '0',
            "amount" => '0',
            "card_status" => 'N'
        );
        $subscription_id = $this->client_model->insert_member_stripid_map($arr12);
        $result = array('status' => 'true', 'member_strip_id' => $subscription_id);
        echo json_encode($result);
    }

#function to add the address of card used by the user and arguement used are card_id, address, member_id,pincode,state,city,save_as,phone

    function add_user_cardaddress() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $address = array("member_id" => $arr['member_id'],
            "stripeid" => $arr['card_id'],
            "address" => $arr['address'],
            "pincode" => $arr['pincode'],
            "state" => $arr['state'],
            "city" => $arr['city'],
            "save_as" => $arr['save_as'],
            "phone" => $arr['phone']
        );
        $this->db->insert('address', $address);
        $data = array(
            "card_status" => 'Y'
        );
        $this->user_model->delete_user_cardaddress($data, $arr['card_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    #function to edit the address of card used by the user and arguement used are card_id, address, member_id,pincode,state,city,save_as,phone

    function edit_user_cardaddress() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $address = array("member_id" => $arr['member_id'],
            "stripeid" => $arr['card_id'],
            "address" => $arr['address'],
            "pincode" => $arr['pincode'],
            "state" => $arr['state'],
            "city" => $arr['city'],
            "save_as" => $arr['save_as'],
            "phone" => $arr['phone']
        );
        if ($arr['ad_id']) {
            $this->user_model->edit_user_cardaddress($address, $arr['ad_id']);
        } else {
            $this->db->insert('address', $address);
        }

        $result = array('status' => 'true');
        echo json_encode($result);
    }

    #function to delete the address of card used by the user and arguement used is id

    function delete_user_cardaddress() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $data = array(
            "delete_status" => 'Y'
        );
        $this->user_model->delete_user_cardaddress($data, $arr['id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

#function to fetch the privacy policy, terms and condition,faq and help using arguement key

    function get_cms_datas($arr) {
        if (!isset($arr['key'])) {
            $arr = json_decode(file_get_contents('php://input'), true);
        }
        if ($arr['key'] == 'privacy') {
            $data_privacy = $this->user_model->get_cms_datas_privacy();
            $result = array('status' => 'true', "privacy" => $data_privacy['Answer']);
            echo json_encode($result);
        }
        if ($arr['key'] == 'terms') {
            $data_terms = $this->user_model->get_cms_datas_terms();
            $result = array('status' => 'true', 'terms' => $data_terms['Answer']);
            echo json_encode($result);
        }
        if ($arr['key'] == 'faq') {
            $data_faq = $this->user_model->get_cms_datas_faq();
            $result = array('status' => 'true', 'faq' => $data_faq['Answer']);
            echo json_encode($result);
        }
        if ($arr['key'] == 'help') {
            $data_help = $this->user_model->get_cms_datas_help();
//            print_r($data_help);
            $result = array('status' => 'true', 'help' => $data_help['Answer']);
            echo json_encode($result);
        }
    }

#function for sending mail in contact form arguements used are name,message and email

    function contact_us() {
        /* $arr = json_decode(file_get_contents('php://input'), true);
          $data_1 = $this->member_model->admin_contratct();
          $config['mailtype'] = 'html';
          $config['charset'] = 'utf-8';
          $config['crlf'] = PHP_EOL;
          $config['newline'] = PHP_EOL;
          $Contact_Us = $this->member_model->get_email("Contact_Us");
          $message2 = $Contact_Us['email_template'];
          $subject2 = $Contact_Us['email_subject'];
          $message2 = str_replace('#baseurl#', base_url(), $message2);
          $message2 = str_replace('#FULL_NAME#', $arr['name'], $message2);
          $message2 = str_replace('#EMAIL#', $arr['email'], $message2);
          $message2 = str_replace('#TOPIC#', 'Contact Us ', $message2);
          $message2 = str_replace('#COMMENTS#', $arr['message'], $message2);
          $this->load->library('email');
          $this->email->initialize($config);
          $pjt_name = $data_1['project_name'];
          $this->email->from($arr['email'], $pjt_name);
          $this->email->to($data_1['email']);
          $this->email->subject($subject2);
          $this->email->message($message2);
          $this->email->send();




          $contact_us_form_email_to_user = $this->member_model->get_email("contact_us_form_email_to_user");
          $message = $contact_us_form_email_to_user['email_template'];
          $subject = $contact_us_form_email_to_user['email_subject'];
          $message = str_replace('#baseurl#', base_url(), $message);
          $message = str_replace('#Name#', $arr['name'], $message);
          $this->load->library('email');
          $this->email->initialize($config);
          $pjt_name = $data_1['project_name'];
          $this->email->from($data_1['email'], $pjt_name);
          $this->email->to($arr['email']);
          $this->email->subject($subject);
          $this->email->message($message);
          $this->email->send(); */




        ######################################################

        /*


          $message1 = "Name " . $name . "<br/>Email " . $email . "<br/> Message: " . $message;
          $this->load->library('email');
          $this->email->initialize($config);

          $this->email->from($data_1['email']);
          $this->email->to($data_1['contact_email']);
          //            $this->email->to($this->config->item('email_from'));
          $this->email->subject("Message From Contact Us");
          $this->email->message($message1);
          $this->email->send(); */





        $data_1 = $this->member_model->admin_contratct();
        $arr = json_decode(file_get_contents('php://input'), true);


        $name = $arr['name'];
        $message3 = $arr['message'];
        $email = $arr['email'];

        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['crlf'] = PHP_EOL;
        $config['newline'] = PHP_EOL;
        $message = $this->member_model->get_email("Contact_Us");

        $message1 = $message['email_template'];
        $subject = $message['email_subject'];
        $message1 = str_replace('#baseurl#', base_url(), $message1);
        $message1 = str_replace('#Name#', $name, $message1);
        $message1 = str_replace('#TOPIC#', '', $message1);
        $message1 = str_replace('#email#', $email, $message1);
        $message1 = str_replace('#Message#', $message3, $message1);
        $message1 = str_replace('#subject#', $subject, $message1);
        $category = "Contact";
        $to_mail = $data_1['email'];
        $from = $email;
        $pjt_name = $data_1['project_name'];
        $this->member_model->mail_sendgrid($to_mail, $subject, $message1, $category, $from, $pjt_name);


//      
        $msg = $this->member_model->get_email("contact_us_form_email_to_user");
        $msg1 = $msg['email_template'];
        $sub = $msg['email_subject'];
        $msg1 = str_replace("#baseurl#", base_url(), $msg1);
        $msg1 = str_replace("#Name#", $name, $msg1);
//        $data_1 = $this->member_model->admin_contratct();
//        $category = "Contact";
        $to_mail1 = $email;
        $from1= $data_1['contact_email'];
//        $pjt_name = $data_1['project_name'];
        $this->member_model->mail_sendgrid($to_mail1, $sub, $msg1, $category, $from1, $pjt_name);

        $result = array('status' => 'true');
        echo json_encode($result);
    }

    #function for inserting users address

    function insert_address() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $adds_id = $this->user_model->insert_1('address', $arr);
        $result = array('status' => 'true', 'id' => $adds_id);
        echo json_encode($result);
    }

#function for editing users address using arguement ad_id

    function edit_address() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $adds_id = $this->user_model->edit_address($arr, $arr['ad_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    #function for deleting users address using arguement ad_id

    function delete_address() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $adds_id = $this->user_model->delete_address($arr['ad_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

    #function to save order datas and arguements are payment_card,totalDue,user_id,rest_id, subtotal,discount,tax,delivery_fee,delivery_method,is_later,tip

    function save_order_data() {

        date_default_timezone_set('UTC');

        $test_array = json_decode(file_get_contents('php://input'), true);
        foreach ($test_array as $data) {

            $this->load->library('stripe_lib');
            $stripe = $this->client_model->getAdmin_strip();
            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            $get_data = $this->user_model->get_strip_detail_fromstrip_id($data['payment_card']['id']);
            $owner_payment = array(
                'amount' => bcmul($data['totalDue'], 100),
                'currency' => 'usd',
                'customer' => $get_data['strip_customer_id']
            );
            //print_r($owner_payment);
            $retData = $this->stripe_lib->chargeCustomer($owner_payment);
            $response = mysql_real_escape_string(serialize($retData));
            $total = bcdiv($retData->amount, 100, 3);

//            $no = rand(000001, 999999);
//            $ref_num = "#FUS" . $data['user_id'] . uniqid() . $no;
            $data['referance_number'] = $ref_num;
            if ($retData->id) {
                $data['name'] = $this->user_model->get_user_name($data['user_id']);
                $get_package_id = $this->user_model->get_package_id($data['user_id']);
                $arr12 = array("member_id" => $data['user_id'],
                    "strip_customer_id" => $get_data['strip_customer_id'],
                    "restaurant_id" => $data['rest_id'],
                    "subscription_id" => $get_package_id['subscription_id'],
                    "created_time" => date('Y-m-d H:i'),
                    "last_4digit" => $get_data['last_4digit'],
                    "brand" => $get_data['brand'],
                    "easy_name" => $get_data['easy_name'],
                    "name" => $get_data['name'],
                    "strip_order_id" => $retData->id,
                    "amount" => $data['totalDue']
                );

                $times = $this->user_model->get_time_rest($data['rest_id']);
                if ($data['delivery_method'] == "Pickup") {

                    $delivery_time = date("Y-m-d H:i:s", strtotime("+" . $times['pickup_time'] . " minutes"));
                } else {
                    $delivery_time = date("Y-m-d H:i:s", strtotime("+" . $times['delivery_time'] . " minutes"));
                }
                $owner_email = $this->member_model->get_owner_email($data['rest_id']);
                $subscription_id = $this->client_model->insert_member_stripid_map($arr12);
                $ordermaster = array("member_id" => $data['user_id'],
                    "restaurant_id" => $data['rest_id'],
                    "location_id" => $data['rest_id'],
                    "subscription_id" => $get_package_id['subscription_id'],
                    "created_time" => date("Y-m-d H:i:s"),
                    "order_status" => "New",
                    "sub_total" => $data['subtotal'],
                    "total_amount" => $data['totalDue'],
                    "discount_amount" => $data['discount'],
                    "tax_amount" => $data['tax'],
                    "delivery_service_amount" => $data['delivery_fee'],
                    "payment_status" => 'Y',
                    "order_type" => $data['delivery_method'],
                    "is_later" => $data['is_later'],
                    "tip" => $data['tip'],
                    "stripe_id" => $subscription_id,
                    "delivery_time" => $delivery_time
                );
                //echo '<pre>';print_r($ordermaster);      
                $order_id = $this->user_model->insert_1('order_master', $ordermaster);
//                $ref_num = "#FS-" . $order_id;
                $random_string = "ABCDEFGHIJKLMNOPQRTSUVWXYZ";
                $random_flag = 0;
                while ($random_flag == 0) {
                    unset($pass);
                    $pass = array(); //remember to declare $pass as an array
                    $alphaLength = strlen($random_string) - 1; //put the length -1 in cache
                    for ($i = 0; $i < 6; $i++) {
                        $n = rand(0, $alphaLength);
                        $pass[] = $random_string[$n];
                    }
                    $ref_num = implode($pass);
                    $rand_count = $this->user_model->check_ref_code($ref_num);
                    if ($rand_count == 0) {
                        $random_flag = 1;
                    }
                }
                $data['referance_number'] = $ref_num;
                $ref_array = array("order_ref_id" => $ref_num);
                $ref_condition = array("order_id" => $order_id);
                $this->user_model->update_ref($ref_array, $ref_condition);
                foreach ($data['cart_data'] as $cart) {
                    //get item data
                    //echo "<pre>";print_r($cart);
                    $orderitems = array("order_id" => $order_id,
                        "item_id" => $cart['item_id'],
                        "unit_price" => $cart['size']['price'],
                        "quantity" => $cart['quantity'],
                        "price" => $cart['size']['price'],
                        "instructions" => $cart['specialInst'],
                        "size" => $cart['size']['size']
                    );

                    $ord_item_id = $this->user_model->insert_1('order_items', $orderitems);

                    $data['ord_item_id'] = $order_id;

                    foreach ($cart['options'] as $key => $option) {
                        unset($ar1);
                        unset($ar2);
                        unset($ar3);
                        foreach ($option['sides'] as $sides) {
                            //get side details
                            $ar1[] = $sides['side_name'];
                            $ar2[] = $sides['side_price'];
                            $ar3[] = $sides['side_id'];
                        }
                        $sides11 = array("sides" => $ar1, "price" => $ar2, "side_id" => $ar3);

                        $sides1 = serialize($sides11);
//                        echo '<pre>';
//                        print_r($sides11);
                        $orderoptionmap = array("order_id" => $order_id,
                            "ord_item_id" => $ord_item_id,
                            "options" => $option['option_name'],
                            "sides" => $sides1,
                            "option_id" => $option['option_id']
                        );

                        $mapid = $this->user_model->insert_1('order_option_map', $orderoptionmap);
                        unset($orderoptionmap);
                        unset($sides11);
                        unset($sides1);
                    }
//                    echo '<pre>';
//                        print_r($sides11);
                }
                $discount_amount = $this->user_model->get_discount_user_amount($data['user_id']);

                if ($data['delivery_method'] == 'delivery') {
                    $new_array = array(
                        "zipcode" => $data['delivery_address']['pincode'],
                        "address_id" => $data['delivery_address']['ad_id'],
                        "member_id" => $data['delivery_address']['member_id'],
                        "city" => $data['delivery_address']['city'],
                        "state" => $data['delivery_address']['state'],
                        "notes" => $data['delivery_address']['delivery_notes'],
                        "address" => $data['delivery_address']['address'],
                        "order_id" => $order_id,
                        "phone" => $data['delivery_address']['phone'],
                        "created_date" => date('Y-m-d H:i:s')
                    );
                    $address_id = $this->user_model->insert_1('delivery_address', $new_array);
                }

                $restaurent_name = $this->user_model->get_rest($data['rest_id']);
                $rest_detail = $this->user_model->get_rest_all($data['rest_id']);
                $order_type_user = $this->user_model->get_rest1($order_id);
                $data['phone'] = $rest_detail['contact_number'];
                $data['user_type_ord'] = $order_type_user;
                $data['rest_name'] = $restaurent_name . " " . $rest_detail['formatted_address'];
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['crlf'] = PHP_EOL;
                $config['newline'] = PHP_EOL;
                $admin_mail = $this->member_model->admin_contratct();
                $admn_email = $admin_mail['email'];
//                $this->load->library('email');
//                $this->email->initialize($config);
//                $this->email->reply_to($admn_email, 'Fulspoon');
//                $this->email->from($admn_email, 'Fulspoon');
//                $this->email->to($owner_email);
//                $this->email->bcc('aneeshg@newagesmb.com');
//                $this->email->bcc("gauravbazaz@gmail.com");
//                $this->email->subject("New Order in Fulspoon! (" . $ref_num . ")");
                $mail_contents = $this->load->view('email/email1.php', $data, TRUE);
//                $this->email->message($mail_contents);
//                $this->email->send();
                $data_1 = $this->member_model->admin_contratct();
                $user = $this->member_model->get_user($data['user_id']);
                $pjt_name = $data_1['project_name'];
                $sub12 = "New Order in Fulspoon! (" . $ref_num . ")";
                $category = "Order";
                $to_mail = $owner_email;
                $from = $admn_email;
                $this->member_model->mail_sendgrid($to_mail, $sub12, $mail_contents, $category, $from, $pjt_name);
                $data['user_data'] = $user;
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['crlf'] = PHP_EOL;
                $config['newline'] = PHP_EOL;
                $admin_mail = $this->member_model->admin_contratct();
                $admn_email = $admin_mail['email'];
//                $this->load->library('email');
//                $this->email->initialize($config);
//                $this->email->reply_to($admn_email, 'Fulspoon');
//                $this->email->from($admn_email, 'Fulspoon');
//                $this->email->to($user['email']);
////                $this->email->bcc("gauravbazaz@gmail.com");
//                $this->email->bcc('aneeshg@newagesmb.com');
//                $this->email->subject("Your order is Successfully placed with Fulspoon! (" . $ref_num . ")");
                $mail_contents1 = $this->load->view('email/email_user.php', $data, TRUE);
//                $this->email->message($mail_contents1);
//                $this->email->send();
                $subject = "Your order is Successfully placed with Fulspoon! (" . $ref_num . ")";
                $to_mail1 = $user['email'];
                $from1 = $admn_email;

                $this->member_model->mail_sendgrid($to_mail1, $subject, $mail_contents1, $category, $from1, $pjt_name);



//                $this->Twilo_sme($order_id); //if server is sme 
                //  $this->Twilo1($order_id); // if server is Fulspoon 
            }
        }


        $result = array('status' => 'true', 'data' => $discount_amount);
        echo json_encode($result);
    }

#function for fetching the current restuarants of the user with parameter user_id

    function current_rest() {
        //$arr =json_decode('{"old_password":"123456","new_password":"qwerty","confirm":"qwerty","member_id":"401"}', TRUE);
        $arr = json_decode(file_get_contents('php://input'), true);
        $user_id = $arr['member_id'];
        $current_rest = $this->client_model->get_rest($user_id);
        $result = array('status' => 'true', 'restaurants' => $current_rest);
        echo json_encode($result);
    }

    #function for changing the password of user argements used are member_id,old_password,new_password

    function change_password() {
        //$arr =json_decode('{"old_password":"123456","new_password":"qwerty","confirm":"qwerty","member_id":"401"}', TRUE);
        $arr = json_decode(file_get_contents('php://input'), true);
        $change_password = $this->user_model->check_old_psd(md5($arr['old_password']), $arr['member_id']);
        if ($change_password) {
            $update_newPassword = $this->user_model->update_newPassword(md5($arr['new_password']), $arr['member_id']);
            $result = array('status' => 'true');
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

#function for payment refund of an order with arguement order_id,amount

    function payment_refund($order_id, $amount = 0) {

        $this->load->library('stripe_lib');

        $data = $this->user_model->getOrderStripMap($order_id);


        if (sizeof($data) != 0) {

            $stripe = $this->client_model->getAdmin_strip();
            if (!$stripe) {
                return array('status' => 'error');
                exit;
            }

            $this->stripe_lib->setApiKey($stripe['stripe_private_key']);
            $strip_order_id = $data[0]['strip_order_id'];
            $refund_amount = $data[0]['total_amount'];
            $charge_details = $this->stripe_lib->getChargeDetails($strip_order_id);

            if ($amount > 0 && $amount <= $data[0]['total_amount'])
                $refund_amount = $amount;

            $amount = bcmul($refund_amount, 100);

            if ($amount > $charge_details->amount && $charge_details->amount)
                $amount = $charge_details->amount;

            $reason = 'requested_by_customer';
            $refund_array = array("charge" => $strip_order_id, "amount" => $amount, "reason" => $reason);
            $resp = $this->stripe_lib->refundeCustomer($refund_array);
            $response = serialize($resp);

            if ($resp->id) {

                $arr = array("member_id" => $data[0]['member_id'],
                    "restaurant_id" => $data[0]['restaurant_id'],
                    "location_id" => $data[0]['location_id'],
                    "strip_customer_id" => $data[0]['strip_customer_id'],
                    "created_time" => date('Y-m-d H:i'),
                    "last_4digit" => $data[0]['last_4digit'],
                    "brand" => $data[0]['brand'],
                    "order_id" => $order_id,
                    "strip_order_id" => $resp->id,
                    "strip_response" => $response,
                    "payment_mode" => "refund");

                $this->db->insert('member_stripid_map', $arr);

                return array('status' => 'success', 'refund_amount' => $refund_amount);
            } else {
                return array('status' => 'error');
            }
        } else {
            return array('status' => 'error');
        }
        /* $this->load->library('stripe_lib');

          $this->stripe_lib->setApiKey('sk_test_xwIc4W5fce6k9OwLxWQWL3SJ');
          $strip_order_id = 'ch_17kqj7IxWBqRgJlrYou97QJi';
          $refund_amount = '38.92';
          $charge_details = $this->stripe_lib->getChargeDetails($strip_order_id);
          $amount = bcmul($refund_amount, 100);
          if ($amount > $charge_details->amount && $charge_details->amount)
          $amount = $charge_details->amount;
          $reason = 'requested_by_customer';
          $refund_array = array("charge" => $strip_order_id, "amount" => $amount, "reason" => $reason);
          $resp = $this->stripe_lib->refundeCustomer($refund_array);
          print_r($resp);
          echo "haii"; */
        /*
          echo md5(123456);
          exit;
          //$lat='40.4574055';
          //$lng='-106.2088298';
          $deal_lat = 40.4574055;
          $deal_long = -106.2088298;
          $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $deal_lat . ',' . $deal_long . '&sensor=false');

          $output = json_decode($geocode);

          for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
          $cn = array($output->results[0]->address_components[$j]->types[0]);
          if (in_array("locality", $cn)) {
          $city = $output->results[0]->address_components[$j]->long_name;
          }
          if (in_array("country", $cn)) {
          $country = $output->results[0]->address_components[$j]->long_name;
          }
          if (in_array("postal_code", $cn)) {
          $zipcode = $output->results[0]->address_components[$j]->long_name;
          }
          if (in_array("administrative_area_level_1", $cn)) {
          $state = $output->results[0]->address_components[$j]->long_name;
          }
          }
          echo $city;
          echo $country;
          echo $zipcode;
          echo $state;
          exit;
          $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
          $json = @file_get_contents($url);
          $data = json_decode($json);
          //echo $data->results[0]->formatted_address;
          echo "<pre>";
          print_r($data);
          exit;
          $address = 'mulanthuruthy,ernakulam';
          echo "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&sensor=false";
          exit;
          // We get the JSON results from this request
          $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
          // We convert the JSON to an array
          $geo = json_decode($geo, true);
          // If everything is cool
          if ($geo['status'] = 'OK') {
          echo $latitude = $geo['results'][0]['geometry']['location']['lat'];
          echo $longitude = $geo['results'][0]['geometry']['location']['lng'];
          }
         */
    }

    #function for sending push notification for ios device arguements used message, deviceToken, badge_count, activity_id, type

    function sendPushNotification($message, $deviceToken, $badge_count, $activity_id, $type) {
        //    $deviceToken='4b1e69f6fe604a39d16640698d268e9928de8dfdfed20411e15f0a0d3361e22e';
        $development_mode = 'Y';
        $passphrase = 'newage';
        ////////////////////////////////////////////////////////////////////////////////
        $ctx = stream_context_create();
//        var_dump($ctx);
        if ($development_mode == 'Y') {
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
        } else {
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-prod.pem');
        }
        if ($development_mode == 'Y') {
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        } else {
            $fp = stream_socket_client(
                    'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'badge' => intval($badge_count),
            'id' => $activity_id,
            'type' => $type,
            'sound' => 'default'
        );
        $payload = json_encode($body);
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
    }

    #function to fetch all order details of the member with parameter member_id,limit 

    function get_all_oder_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $orderList = $this->user_model->getAllOders($arr['member_id'], '', $arr['limit']);

        if (count($orderList) != 0) {
            $total_amount = 0;
            $discount_amount = 0;

            for ($i = 0; $i < count($orderList); $i++) {
                $total_amount = $total_amount + $orderList[$i]['total_amount'];
                $discount_amount = $discount_amount + $orderList[$i]['discount_amount'];
            }

            $result = array('status' => 'true', 'orderList' => $orderList, 'total_amount' => $total_amount, 'discount_amount' => $discount_amount);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

#function for twilio using arguement id

    function message($id) {
//     header("content-type:text/xml");
//     
        $data = $this->user_model->get_order_twilo_data($id);
        $data['orderid'] = $id;
        $this->load->view("twiml/twiml/1.0/hello-monkey", $data);
    }

#function for twilo api

    function Twilo1($id) {
        require('twilio-php-master/Services/Twilio.php');

        $account_sid = 'ACc006359abbec78df3e8fc0488c68cede';
        $auth_token = 'e4367679af00b2529a9f6bf369e8c88a';
        $client = new Services_Twilio($account_sid, $auth_token);
        $owner_phone = $this->user_model->get_owner_phone($id);
        $phonenumber = '+18589434409';
        try {
            // Initiate a new outbound call
            $call = $client->account->calls->create(
                    $phonenumber, // The number of the phone initiating the call
                    $owner_phone, // The number of the phone receiving call
                    base_url() . 'client/message/' . $id // The URL Twilio will request when the call is answered
            );
//            echo 'Started call: ' . $call->sid;
//        print_r($call);
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }




//        $client->account->calls->create('+12014318323', '+919496803469', 'AP6f2aed3d5119b29fa265d2c7314778ed', array(
//            'Method' => 'GET',
//            'FallbackMethod' => 'GET',
//            'StatusCallbackMethod' => 'GET',
//            'Record' => 'false',
//        ));
    }

#function for twilio using arguement id

    function Twilo_sme($id) {
        require('twilio-php-master/Services/Twilio.php');
        $twilo = $this->user_model->get_config1();
        $account_sid = $twilo['sid']['value'];
        $auth_token = $twilo['authtocken']['value'];
        $client = new Services_Twilio($account_sid, $auth_token);
//        print_r($client->account->calls);
//        exit;
        $owner_phone = $this->user_model->get_owner_phone($id);
//        $owner_phone='+919496803469';
//        $phonenumber = '+13476798845';//gaurav account
//        $phonenumber = '+18589434409'; //praveen account
        $phonenumber = $twilo['phonenumber']['value'];
        try {
            // Initiate a new outbound call
            $call = $client->account->calls->create(
                    $phonenumber, // The number of the phone initiating the call
                    $owner_phone, // The number of the phone receiving call
                    base_url() . 'client/message/' . $id // The URL Twilio will request when the call is answered
            );
//            echo 'Started call: ' . $call->sid;
//        print_r($call);
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }




//        $client->account->calls->create('+12014318323', '+919496803469', 'AP6f2aed3d5119b29fa265d2c7314778ed', array(
//            'Method' => 'GET',
//            'FallbackMethod' => 'GET',
//            'StatusCallbackMethod' => 'GET',
//            'Record' => 'false',
//        ));
    }

#function for fetching all orders of user in a particular restuarant. arguement used are member_id,restuarant

    function get_all_oder_restaurant_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $orderList = $this->user_model->getAllOders($arr['member_id'], $arr['restaurant']);
        if (count($orderList) != 0) {
            $total_amount = 0;
            $discount_amount = 0;

            for ($i = 0; $i < count($orderList); $i++) {
                $total_amount = $total_amount + $orderList[$i]['total_amount'];
                $discount_amount = $discount_amount + $orderList[$i]['discount_amount'];
                $orderList[$i]['items'] = $this->user_model->getAllOderItemDetails($orderList[$i]['order_id']);
            }
            $result = array('status' => 'true', 'orderList' => $orderList);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
    }

#function for updating push notification with arguement member_id

    function update_push_notif() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->user_model->update_push_notif($arr['push'], $arr['member_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

#function for inserting the geolaocation of the user using parameter geolocation and member_id

    function geoLocation() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $this->user_model->geoLocation($arr['geo_location'], $arr['member_id']);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

#function for changing the package of a user with arguements member_id,sub_package_id,card_id

    function change_subscription_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $User_subscription_master = array("member_id" => $arr['member_id'],
            "package_id" => $arr['sub_package_id'],
            "strip_id" => $arr['payment_card']['id']);

        $new_sub_prize = $this->user_model->get_package_details($arr['sub_package_id']);
        $next_restaurant_status = $this->user_model->next_restaurant_status($arr['member_id']);
        if (isset($next_restaurant_status['member_id'])) {
            $next_restaurant_status = $this->user_model->update_next_restaurant_status($next_restaurant_status['subscription_id']);
        }

        $user_sub_id = $this->user_model->insert_1('subscription_next', $User_subscription_master);
        $i = 0;
        foreach ($arr['restaurant'] as $row) {
            $arr = array("member_id" => $arr['member_id'],
                "next_subscription_id" => $user_sub_id,
                "updated_on" => date('Y-m-d H:i'),
                "restaurant_id" => $row);
            $this->db->insert('restaurants_next', $arr);
            $rest_new[$i] = $this->user_model->get_restaurant_food_detail($row);
            $i++;
        }
        $rest_curr = $this->user_model->get_user_restaurant_detail($arr['member_id']);
//        $arr = json_decode(file_get_contents('php://input'), true);
        foreach ($rest_new as $rest_new11) {
            $rest_new1 = $rest_new1 . $rest_new11['restaurant_name'] . "<br>";
        }
        foreach ($rest_curr as $rest_curr11) {
            $rest_curr1 = $rest_curr1 . $rest_curr11['restaurant_name'] . "<br>";
        }
        $data_1 = $this->member_model->admin_contratct();
        $user_data = $this->user_model->get_userdetails($arr['member_id']);
        $current_rest = $this->get_user_restaurant_details($arr['member_id']);

        $msg = $this->member_model->get_email("Subscription_changed_email");
        $message2 = $msg['email_template'];
        $subject2 = $msg['email_subject'];
        $message2 = str_replace('#baseurl#', base_url(), $message2);
        $message2 = str_replace(' #subject#', $subject2, $message2);
        $message2 = str_replace(' #NAME#', " " . $user_data['first_name'] . " " . $user_data['last_name'], $message2);
        $message2 = str_replace('#NRestaurant Name + Info#', $rest_new1, $message2);
        $message2 = str_replace('#CRestaurant Name + Info#', $rest_curr1, $message2);
        $message2 = str_replace('#Cuser_fee#', $rest_curr[0]['user_fee'], $message2);
        $message2 = str_replace('#Nuser_fee#', $new_sub_prize['user_fee'], $message2);
        $message2 = str_replace('#NextBillingDate#', date('m-d-Y', strtotime('+1 day', strtotime($rest_curr[0]['End_date']))), $message2);
        $pjt_name = $data_1['project_name'];
        $category = "Subscription";
        $to_mail = $user_data['email'];
        $from = $data_1['email'];
//        $pjt_name = $stripe['project_name'];
        $this->member_model->mail_sendgrid($to_mail, $subject2, $message2, $category, $from, $pjt_name);
        $result = array('status' => 'true');
        echo json_encode($result);
    }

#function for logging out parameter used is user_id

    function logout($arr) {
        $user_id = $arr['user_id'];
        $array = array("device_id" => '', "divice_type" => '', "device_tocken" => '');
        $condition = array("member_id" => $user_id);
        $this->member_model->logout($array, $condition);

        $result = array('status' => 'true');
        echo json_encode($result);
    }

#function for updating the member image paramter used is member_id

    function updateMemberImage($user_id) {

        $user_id = $_GET['member_id'];
        // $user_id = $_POST['member_id'];


        if ($_FILES['image']['name'] <> "") {
            #################  Upload ############

            $imagename = strtotime(date("Y-m-d H:i:s"));
            $tempFile = $_FILES['image']['tmp_name'];
            $fileParts = pathinfo($_FILES['image']['name']);
            $image_name = $user_id . '.' . $fileParts['extension'];
            $foldername = "uploads/members/$image_name";
            $targetFolder = 'uploads/members';
            $targetFile = realpath($targetFolder) . '/' . $image_name;
            move_uploaded_file($tempFile, $targetFile);
            $imagethumb = "uploads/members/" . $user_id . "_thumb." . $fileParts['extension'];
            //$imagethumb1 = "upload/members" . $user_id . "_thumb1." . $fileParts['extension'];
            list($width, $height, $type, $attr) = getimagesize($targetFile);
            $configThumb = array();
            $configThumb['image_library'] = 'gd2';
            $configThumb['source_image'] = $targetFile;
            $configThumb['new_image'] = $imagethumb;
            $configThumb['create_thumb'] = false;
            $configThumb['maintain_ratio'] = TRUE;
            $configThumb['width'] = 320;
            $configThumb['height'] = 320;

            $this->load->library('image_lib');
            $this->image_lib->initialize($configThumb);
            $this->image_lib->resize();
            chmod('uploads/members/' . $user_id . "_thumb." . $extension, 757);
            $this->user_model->updateMemberImage($fileParts['extension'], $user_id);
            $result = array('status' => 'true', 'image' => base_url() . "uploads/members/" . $user_id . "_thumb." . $fileParts['extension']);
        } else {
            $result = array('status' => 'false');
        }

        echo json_encode($result);
    }

    #function for fetching the current subscription details of the user parameters used is member_id

    function getUser_subscription_details() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $data = $this->user_model->get_subscription_all_details($arr['member_id']);
        $result = array('status' => 'true', 'data' => $data);
        echo json_encode($result);
    }

    #function for fetching past order details of the user using parameter member_id and order_id

    function get_past_order_details() {
        $arr = json_decode(file_get_contents('php://input'), true);



        $member_id = $arr['member_id'];
        $order_id = $arr['order_id'];
        $order_details = $this->user_model->get_order_details($order_id);
        $data = $this->user_model->get_past_order_details($member_id, $order_id);

        $address = $this->user_model->get_order_address($order_id);
        $subscription_data = $this->user_model->get_ord_rest_subscr_details($member_id, $order_id);
        $result = array('status' => 'true', 'data' => $data, 'subscription_data' => $subscription_data, "address" => $address, 'order_details' => $order_details);
        echo json_encode($result);
    }

    #function to fetch all payment details of the user with parameter member_id and limit

    function get_all_paymentHistory() {
        $arr = json_decode(file_get_contents('php://input'), true);
        // $payment_history = $this->user_model->payment_history($arr['member_id']);
        $payment_history = $this->user_model->payment_history_1_new($arr['member_id'], $arr['limit']);
        $orderList = $this->user_model->getAllOders($arr['member_id']);

        $total_user_amount = $this->user_model->get_discountAndTotal_amount($arr['member_id']);
        if (sizeof($payment_history) != 0) {
            if ($total_user_amount['discount_amount'] == '') {
                $total_user_amount['discount_amount'] = 0.00;
            }
            $result = array('status' => 'true', 'total_amount' => $total_user_amount['total_amount'], 'discount_amount' => $total_user_amount['discount_amount'], 'payment_history' => $payment_history);
            echo json_encode($result);
        } else {
            $result = array('status' => 'false');
            echo json_encode($result);
        }
        /* $discount_amount = $this->user_model->get_discount_user_amount($data['user_id']);
          if (count($orderList) != 0 && sizeof($payment_history) != 0) {
          $total_amount = 0;
          $discount_amount = 0;

          for ($i = 0; $i < count($orderList); $i++) {
          $total_amount = $total_amount + $orderList[$i]['total_amount'];
          $discount_amount = $discount_amount + $orderList[$i]['discount_amount'];
          //$orderList[$i]['items'] = $this->user_model->getAllOderItemDetails($orderList[$i]['order_id']);
          //$orderList[$i]['side_order'] = $this->user_model->getAllOder_side($orderList[$i]['order_id']);
          }


          $result = array('status' => 'true', 'total_amount' => $total_amount, 'discount_amount' => $discount_amount, 'payment_history' => $payment_history);
          echo json_encode($result);
          } else {
          $result = array('status' => 'false');
          echo json_encode($result);
          } */
    }

#function for fetching the last date of the user subscription with parameter member_id

    function getUserSubscription_last_date() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $last_date = $this->member_model->get_user_subscription_lastDate($arr['member_id']);
        $package = $this->member_model->get_all_package();
        $date = $last_date['end_date'];
        $date1 = str_replace('-', '/', $date);
        $tomorrow = date('m-d-Y', strtotime($date1 . "+1 days"));
        $result = array('status' => 'true', 'efect_frm' => $tomorrow, 'package' => $package);
        echo json_encode($result);
    }

#function for finding the pending subscription with parameter member_id

    function pendingSubscription() {
        $arr = json_decode(file_get_contents('php://input'), true);
        $now_package = $this->member_model->pendingSubscription_now($arr['member_id']);
        $pending_package = $this->member_model->pendingSubscription_later($arr['member_id']);
        $result = array('status' => 'true', 'now_package' => $now_package, 'pending_package' => $pending_package, 'used_package' => $used_package);
        echo json_encode($result);
    }

    function test() {
        $rest_curr1[0]['End_date'] = '2017-01-15';
        echo date('m-d-Y', strtotime('+1 day', strtotime($rest_curr1[0]['End_date'])));
        /*  $arr = json_decode(file_get_contents('php://input'), true);
          $data_1 = $this->member_model->admin_contratct();
          $config['mailtype'] = 'html';
          $config['charset'] = 'utf-8';
          $config['crlf'] = PHP_EOL;
          $config['newline'] = PHP_EOL;
          $Contact_Us = $this->member_model->get_email("Contact_Us");
          $message2 = $Contact_Us['email_template'];
          $subject2 = $Contact_Us['email_subject'];
          $this->load->library('email');
          $this->email->initialize($config);
          $pjt_name = $data_1['project_name'];
          $this->email->from('aneshg@newagesmb.com', $pjt_name);
          $this->email->to('bibinv@newagesmb.com');
          $this->email->subject($subject2);
          $this->email->message($message2);
          $this->email->send();
          echo "test"; */
    }

}

?>