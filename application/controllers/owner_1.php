<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of owners
 *
 * @author aneesh
 */
// error_reporting(E_ALL);
class owner extends MY_Controller {

    public function __construct() {

        parent::__construct();
        date_default_timezone_set('GMT');
        $this->load->model('owner_model');
        $this->load->library('session');
        $this->clear_cache();
    }

    function clear_cache() {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    public function index() {

        if ($check) {
            redirect('/owner/home', 'refresh');
        } else {
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('owner/static_header', $data);
            $this->load->view('owner/login');
            $this->load->view('owner/footer');
        }
    }

    public function check_login() {

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->owner_model->getOwnerDetails(array('email' => $email));
        //echo md5($user['password']);exit;
        if (count($user) > 0) {
            if ($user['password'] === md5($password)) {

                $this->session->set_userdata('user', (object) $user);
                $result = array('status' => 'success', 'result' => 'valid');
                echo json_encode($result);
                exit;
            } else {
                $result = array('status' => 'error', 'result' => 'invalid');
                echo json_encode($result);
                //echo 'invalid';
                exit;
            }
        } else {
            $result = array('status' => 'error', 'result' => 'invalid');
            echo json_encode($result);
            //echo 'invalid';
            exit;
        }
    }

    public function forgotpwd() {

        $email_id = $_POST['email'];
        $data = $this->owner_model->getOwnerDetails(array('email' => $email_id));
        $new_password = $this->owner_model->randomPassword();
        $this->load->model('email_model');
        $this->load->helper('cookie');
        $cookie = array(
            'name' => $new_password,
            'value' => $email_id,
            'expire' => time() + 3600
        );

        if (count($data) > 0) {
            $email = $this->email_model->get_email_template('forgot_password_admins');

            $this->load->library('encrypt');
            $full_name = $data['full_name'];
            $target_name = $this->config->item('site_name');
            $link = base_url() . 'owner/reset_password/' . $new_password;

            $subject = $email['email_subject'];
            $message = $email['email_template'];
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html; charset=utf-8" . "\r\n";
            $headers .= 'From: ' . $this->config->item('email_from') . "\r\n";

            $message = $email['email_template'];
            $message = str_replace('%FULL_NAME%', $full_name, $message);
            $message = str_replace('%LINK%', $link, $message);
            $message = str_replace('%SITE_NAME%', $target_name, $message);
            $category = "Owner";
//        $to_mail = $order['email'];
//            $to_mail = $data_1['email'];
            $data_1 = $this->member_model->admin_contratct();
            $pjt_name = $data_1['project_name'];
            $from = '<info@fulspoon.com>';
            $this->member_model->mail_sendgrid($email_id, $subject, $message, $category, $from, $pjt_name);

//            if (@mail($email_id, $subject, $message, $headers)) {
            echo "success";
            set_cookie($cookie);
//            } else
//                echo "There is error in sending mail!";
        } else {
            echo "Please enter valid email id!";
        }
    }

    public function reset_password($new_password) {
        $cookie = get_cookie($new_password);

        if ($cookie) {
            $data['email'] = $cookie;
            $data['key'] = $new_password;
            $data['site_name'] = 'Replimatic';
            $data['title'] = 'Password Reset';
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('owner/static_header', $data);
            $this->load->view('owner/resetPassword', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect('/owner', 'refresh');
        }
    }

    public function changepassword() {
        $data = $this->input->post();
        if ($data['password'] == $data['confirm_password']) {

            $check = array('password' => $data['password']);
            $admin_data = $this->owner_model->getOwnerDetails(array('email' => $data['email']));
            $result = $this->owner_model->updateOwner($check, $admin_data['restaurant_id']);
            if ($result != 'error') {
                delete_cookie($data['key']);
                echo 'success';
            } else {
                echo 'Unknown error occur';
            }
        } else {
            echo 'Password Mismatch';
        }
    }

    public function home() {
        if ($this->check()) {
            $data['site_name'] = 'Replimatic';
            $data['template_url'] = base_url('assets/dashboard');
            $this->load->view('owner/header', $data);
            $this->load->view('owner/home', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect('/owner', 'refresh');
        }
    }

    public function check() {
        $user_data = $this->session->userdata('user');
        if ($user_data->restaurant_id)
            return 1;
        else
            return 0;
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/owner', 'refresh');
    }

    public function preference($location_id = '') {

        $this->load->model('preference_model');
        $user = $this->session->userdata('user');


        $location_id = $user->restaurant_id;


        $data['restconfiglist'] = $this->preference_model->get_restConfig($location_id);
        //echo '<pre>';print_r($user);exit;
        //exit;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post_title = $_POST['title'];
            $post_aar = $_POST;
            //echo '<pre>';print_r($_POST);
            if (count($data['restconfiglist']) != 0) {
                foreach ($post_aar as $key => $value) {
                    if ($key != 'title') {
                        $data_getdata = $this->preference_model->get_by(array('location_id' => $location_id, 'config_id' => $key));
                        if (sizeof($data_getdata)) {
                            $update_id = $this->preference_model->update_by(array('config_id' => $key, 'location_id' => $location_id), array('value' => $value));
                        } else {

                            $dd = array('config_id' => $key,
                                'field' => $key,
                                'value' => $value,
                                'location_id' => $location_id,
                                'description' => $key);
                            $update_id = $this->preference_model->insert($dd);
                        }
                    }
                }
            } else {

                $i = 0;
                foreach ($post_aar as $key => $value) {

                    if ($key != 'title') {
                        $dd = array('config_id' => $key,
                            'field' => $key,
                            'value' => $value,
                            'location_id' => $location_id,
                            'description' => $post_title[$i]);
                        $update_id = $this->preference_model->insert($dd);
                        $i++;
                        //echo "test1";
                        //exit;
                    }
                }
            }


            $this->session->set_flashdata('success_message', 'Preferences  Updated Successfully. ', 'SUCCESS');
            redirect('owner/preference');
        }


        //echo '<pre>';print_r($data);exit;
        $data['active'] = 'preference';
        $this->load->view('owner/header', $data);
        $this->load->view('owner/preference', $data);
        $this->load->view('owner/footer', $data);
    }

    public function profile($location_id = '') {
        if ($this->check()) {
            $this->load->model('preference_model');
            $user = $this->session->userdata('user');
            $location_id = $user->restaurant_id;
            $data['profile'] = $this->preference_model->get_restaurent($location_id);
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->db->where('restaurant_id', $_POST['restaurant_id']);
                $this->db->update('restaurant_master', $_POST);
                $i = 0;
                foreach ($post_aar as $key => $value) {
                    if ($key != 'title') {
                        $dd = array('config_id' => $key,
                            'field' => $key,
                            'value' => $value,
                            'location_id' => $location_id,
                            'description' => $post_title[$i]);
                        $update_id = $this->preference_model->insert($dd);
                        $i++;
                    }
                }
//            }
                $this->session->set_flashdata('success_message', 'Preferences  Updated Successfully. ', 'SUCCESS');
                redirect('owner/preference');
            }


            //echo '<pre>';print_r($data);exit;
            $data['active'] = 'profile';
            $this->load->view('owner/header', $data);
            $this->load->view('owner/profile', $data);
            $this->load->view('owner/footer', $data);
        } else {
            redirect("owner");
        }
    }

}
