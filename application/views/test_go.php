<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
            <div class="col-md-offset-3 col-md-5"> 
                <ul class="footer-nav">                                               
                    <li><a href="<?= base_url() ?>owners">Home</a> </li>    
                    <li><a href="<?= base_url() ?>terms-of-use">Terms & Conditions</a> </li>
                    <li><a href="<?= base_url() ?>owners/faq">FAQ</a> </li>
                    <li><a href="<?= base_url() ?>owners/contact_us">Contact Us</a> </li>
                </ul>
            </div>
            <div class="col-md-12"><p class="topline">&copy; <?= date("Y") ?> Fulspoon. All rights reserved. </p></div>

        </div>
        <!-- .row -->
    </div>
</footer>
<!-- End Footer -->

</div>
<!-- End Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72687545-1', 'auto');
    ga('send', 'pageview');

</script>
</body>

</html>