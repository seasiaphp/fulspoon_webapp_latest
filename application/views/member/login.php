
<!-- ===== Section Sign In ===== -->


<section class="sign-in-sec">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-10 col-center">
                <div class="sign-in-main">
                    <div class="logo">
                        <?php echo '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="791px" height="215px" viewBox="0 0 791 215" style="enable-background:new 0 0 791 215;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#73BF4C;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{clip-path:url(#SVGID_6_);}
	.st3{filter:url(#Adobe_OpacityMaskFilter);}
	.st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_10_);}
	.st5{clip-path:url(#SVGID_8_);mask:url(#SVGID_9_);fill:url(#SVGID_11_);}
	.st6{opacity:0.52;}
	.st7{clip-path:url(#SVGID_13_);fill:#6EBD44;}
</style>
<g>
	<path class="st0" d="M259.3,161.1V99.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
		c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H259.3z"/>
	<path class="st0" d="M356.1,161.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V92.9h6.7V132
		c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V92.9h6.7v68.2H356.1z"/>
	<rect x="380.5" y="64.7" class="st0" width="6.7" height="96.4"/>
	<path class="st0" d="M452.8,105.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
		c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
		c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
		c9.9,0,18.6,2.8,25.3,9.1L452.8,105.5z"/>
	<path class="st0" d="M474.8,189.4V93.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35c0,22.9-15.4,35.3-34.6,35.3
		c-11.6,0-22.3-5.6-28.1-16.9v44.1H474.8z M537.2,127.1c0-19.1-12.4-28.5-27.8-28.5c-15.8,0-27.5,12-27.5,28.6
		c0,16.7,12,28.6,27.5,28.6C524.8,155.8,537.2,146.2,537.2,127.1"/>
	<path class="st0" d="M557.3,127.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
		C572.7,162,557.3,148.8,557.3,127.2 M619.8,127.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
		c0,17.6,12.5,28.2,27.9,28.2C607.2,155.4,619.8,144.8,619.8,127.2"/>
	<path class="st0" d="M638.8,127.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
		C654.2,162,638.8,148.8,638.8,127.2 M701.3,127.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
		C688.7,155.4,701.3,144.8,701.3,127.2"/>
	<path class="st0" d="M730,93.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V122
		c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H730z"/>
	<path class="st0" d="M97.8,159c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
		c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
		c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C85,8.3,72.8,1.1,63,6.7
		C43.2,18,16,41.1,8.5,85c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
		c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C111.6,20,98,32.6,90.9,57.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
		C86.9,150.9,91.3,156.9,97.8,159"/>
	<g>
		<g>
			<defs>
				<rect id="SVGID_1_" x="6.9" y="4.8" width="214.9" height="203.2"/>
			</defs>
			<clipPath id="SVGID_2_">
				<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
			</clipPath>
			<g class="st1">
				<defs>
					<path id="SVGID_3_" d="M97.8,159c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
						c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
						c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C85,8.3,72.8,1.1,63,6.7
						C43.2,18,16,41.1,8.5,85c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
						c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C111.6,20,98,32.6,90.9,57.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
						C86.9,150.9,91.3,156.9,97.8,159"/>
				</defs>
				<clipPath id="SVGID_4_">
					<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
				</clipPath>
			</g>
		</g>
	</g>
	<g>
		<g>
			<defs>
				<rect id="SVGID_5_" x="86.9" y="47.5" width="41.5" height="56.5"/>
			</defs>
			<clipPath id="SVGID_6_">
				<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
			</clipPath>
			<g class="st2">
				<defs>
					<path id="SVGID_7_" d="M118.7,82.3c0,0-29.3-5.3-31.6,21.7c0,0-1.7-29,4.4-48.4l36.9-8.1C128.3,47.5,116.3,56.1,118.7,82.3"/>
				</defs>
				<clipPath id="SVGID_8_">
					<use xlink:href="#SVGID_7_"  style="overflow:visible;"/>
				</clipPath>
				<defs>
					<filter id="Adobe_OpacityMaskFilter" filterUnits="userSpaceOnUse" x="84.5" y="46.9" width="44.7" height="57.8">
						<feColorMatrix  type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
					</filter>
				</defs>
				<mask maskUnits="userSpaceOnUse" x="84.5" y="46.9" width="44.7" height="57.8" id="SVGID_9_">
					<g class="st3">
						
							<linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="-37.7669" y1="413.1895" x2="-33.7986" y2="413.1895" gradientTransform="matrix(-0.1744 -11.1606 -11.1606 0.1744 4712.1812 -389.4146)">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#000000"/>
						</linearGradient>
						<polygon class="st4" points="85.4,104.7 84.5,47.5 128.3,46.9 129.2,104 						"/>
					</g>
				</mask>
				
					<linearGradient id="SVGID_11_" gradientUnits="userSpaceOnUse" x1="-37.7669" y1="413.1895" x2="-33.7986" y2="413.1895" gradientTransform="matrix(-0.1744 -11.1606 -11.1606 0.1744 4712.1812 -389.4146)">
					<stop  offset="0" style="stop-color:#1970AC"/>
					<stop  offset="1" style="stop-color:#4399D4"/>
				</linearGradient>
				<polygon class="st5" points="85.4,104.7 84.5,47.5 128.3,46.9 129.2,104 				"/>
			</g>
		</g>
	</g>
	<g class="st6">
		<g>
			<defs>
				<rect id="SVGID_12_" x="6.9" y="10.2" width="214.9" height="197.8"/>
			</defs>
			<clipPath id="SVGID_13_">
				<use xlink:href="#SVGID_12_"  style="overflow:visible;"/>
			</clipPath>
			<path class="st7" d="M218.6,70.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
				c2.5,54.9-48.4,101.6-101.6,101.6C52.9,196.2,11.4,152,7,98.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
				C221.5,128,225.2,98.6,218.6,70.9"/>
		</g>
	</g>
</g>
</svg>'; ?>

                        <p id="title">Type your details</p>

                    </div>

                    <div class="sign-in-form" id="login-form">
                        <div class="" id="status" ></div>
                        <form id="myform1" action="" method="post">
                            <label>
                                <input type="text" name="userid" id="email"  placeholder="Email" required />
                            </label>
                            <label>
                                <input type="password" name="password" id="password"  placeholder="Password" required />
                            </label>
                            <input type="submit" value="Sign me in" id="submitbut"/>
                        </form>
                        <a href="javascript:void(0);" class="txt_link" id="change_link" >Forgot your password?</a>
                    </div>

                    <div class="sign-in-form" style="display:none"  id="forgot-form">
                        <div class="" id="status"></div>
                        <form onsubmit="">
                            <label>
                                <input type="email" placeholder="Email ID" required id="new" />
                            </label>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <input type="submit" value="Save" id="pwd_btn"/>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <button class="go-signin">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="sign-in-form" style="display:none"  id="forgot-success-form">
                        <form>
                            <p>Thank you! Please check your email
                                to reset your password</p>
                            <button class="btn-done go-signin">Done!</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- ===== End Section Sign In ===== -->

</div>

<script>
    $("#change_link").click(function () {

        Hideall();
        $("#forgot-form").show();
        $("#header-title").html('Forgot Password');


    })
    $(".go-signin").click(function () {

        Hideall()
        $("#login-form").show();
        $("#header-title").html('Sign In');


    })

    function Hideall()
    {
        $("#login-form").hide();
        $("#forgot-form").hide();
        $("#forgot-success-form").hide();
        $("#title").show();
    }
</script>


<script>
    $(document).ready(function () {


        $('#username').keypress(function (event) {

            if (event.keyCode == 13) {
                event.preventDefault();
                Login(event);
            }
        });
        $('#password').keypress(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                Login(event);
            }
        });
        $("#submitbut").click(function (event) {
            Login(event);
        });
    });

    function Login(event) {
        event.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();
        if (email == '' || password == '')
        {
            return false;
        }
        else {
            $('#status').html("");
            $.ajax({
                url: "<?php echo base_url() ?>member/check_login",
                type: "POST",
                data: {'email': email,
                    'password': password
                },
                success: function (result)
                {
                    var result = $.parseJSON(result);

                    if (result.result == 'valid')
                    {
                        $('#status').html("login successfull!");
                        $("#status").addClass("alert");
                        $("#status").addClass("alert-success");
                        window.location.href = "<?php echo base_url(); ?>member/change_subscription_details";



                    }
                    else
                    {

                        $('#status').html("Invalid Username or Password !");
                        $("#status").addClass("alert");
                        $("#status").addClass("alert-danger");
                        $("#msgbox").show();
                        $("#email").css("border-color", "#FF0000");
                        $("#password").css("border-color", "#FF0000");
                    }
                }
            });
        }

    }
    $("#pwd_btn").click(function (event) {

        event.preventDefault();

        var email = $('#new').val();

        if ((email != '')) {

            $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>member/forgotpwd",
                data: {'email': email},
                success: function (data) {
                    if (data.indexOf("success")>=0) {

                        Hideall();
                        $("#forgot-success-form").show();
                        $("#header-title").html('Email Sent');
                        $("#title").hide();


                    }
                    else
                    {
                        alert(data);
                        return false;
                    }

                }
            });
        }
        else
        {
            alert("Please enter valid email id");
        }

    })

</script>

<style>
    .errorborder{
        border:1px solid #ff0000!important;
    }
</style>