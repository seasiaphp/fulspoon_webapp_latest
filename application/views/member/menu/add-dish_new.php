
        <!-- ===== End Header ===== -->
        
        <!-- ===== Start Add Dish ===== -->
       	<section class="main-sec">
            <div class="container-fluid">
            	<!-- Add Dish Form -->
            	<div class="add-dish-form">
                	<!-- Start Form -->
                    <form>
                    	<!-- Breadcrumb & Save -->
                    	<div class="breadcrumb-save">
                        	<div class="row">
                            	<!-- Breadcrumb -->
                            	<div class="col-lg-6 col-md-6 col-sm-3 col-xs-3 display-ful">
                                	<div class="breadcrumb-box">
                                        <ul>
                                        	<li><a href="#">Menu</a></li>
                                        	<li class="active">Edit Dish</li>
                                        </ul>
                                    </div>
                                </div>
                            	<!-- End Breadcrumb -->
                                
                            	<!-- Save Btn Top -->
                            	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-9 display-ful">
                                	<div class="save-btn-top">
                                    	<button class="copy-btn">Copy</button>
                                    	<button>Add New</button>
                                    	<button>Save</button>
                                    </div>
                                </div>
                                <!-- End Save Btn Top -->
                            </div>
                        </div>
                        <!-- End Breadcrumb & Save -->
                        
                    	<!-- Form Box -->
                        <div class="form-box add-dish-box">
                        	<div class="row">
                            	<!-- Form Field -->
                            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 display-ful">
                                	<div class="add-dish-field">
                                    	<select>
                                            <option disabled selected>-- Select Category --</option>
                                            <option>Dinner Entrees</option>
                                            <option>Small Plates</option>
                                            <option>Lunch Entrees</option>
                                            <option>Hand Helds</option>
                                            <option>Kids Menu</option>
                                            <option>A LA Carte</option>
                                            <option>Salads</option>
                                            <option>Half &amp; Half</option>
                                            <option>Sides</option>
                                            <option>Today's Specials</option>
                                            <option>Drinks</option>
                                            <option>Wood Burning Oven Pizzas</option>
                                            <option>Burgers</option>
                                            <option>Pizza</option>
                                            <option>Crispy Chicken Sandwiches</option>
                                            <option>Nigiri or Sashimi</option>
                                            <option>In-House Catering</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- End Form Field -->
                                
                                <!-- Form Field -->
                            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 display-ful">
                                	<div class="add-dish-field">
                                    	<input type="text" placeholder="Menu Item" />
                                    </div>
                                </div>
                                <!-- End Form Field -->
                            </div>
                            
                            <div class="add-dish-field">
                            	<textarea placeholder="Description"></textarea>
                            </div>
                        </div>
                        <!-- End Form Box -->
                        
                    	<!-- Form Box -->
                        <div class="form-box add-dish-box">
                        	<div class="row">
                            	<!-- Form Field -->
                            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 display-ful">
                                	<div class="add-dish-field label-check">
                                    	<label>
                                        	<input id="sizeCheckbox" type="checkbox" /> This item has different sizes
                                        </label>
                                    </div>
                                </div>
                                <!-- End Form Field -->
                                
                                <!-- Form Field -->
                            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 display-ful">
                                	<div class="add-dish-field if-check-field uncheckedShow">
                                    	<input type="text" placeholder="Price" />
                                    </div>
                                </div>
                                <!-- End Form Field -->
                            </div>
                            
                            <!-- Sizes Table -->                            
                            <table class="table table-hover sizes-table checkedShow">
                                <thead>
                                    <tr>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th><a href="javascript:void(0);"><i class="fa fa-plus"></i></a></th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                    	<td><input type="text" placeholder="Size" class="addInput" /></td>
                                    	<td><input type="text" placeholder="Price" /></td>
                                        <td><a href="javascript:void(0);" class="rowDelete" data-target="#noDeleteFirstRow" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                    </tr>
                                    <tr>
                                    	<td><input type="text" placeholder="Size" class="addInput" /></td>
                                    	<td><input type="text" placeholder="Price" /></td>
                                        <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                    </tr>
                                    <tr>
                                    	<td><input type="text" placeholder="Size" class="addInput" /></td>
                                    	<td><input type="text" placeholder="Price" /></td>
                                        <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- End Sizes Table -->
                        </div>
                        <!-- End Form Box -->
                        
                        <!-- Options & Sides -->
                        <div class="option-and-sides">
                        	<h1>Options and Sides</h1>
                            <div class="option-and-sides-add-remove-links">
                            	<a href="javascript:void(0);"><i class="fa fa-times"></i></a>
                            	<a href="javascript:void(0);"><i class="fa fa-plus"></i></a>
                            </div>
                            
                            <div id="multi">
                                <!-- Option Table -->
                                <div class="form-box option-table-container tile">
                                	<span class="multi-handle"><i class="fa fa-arrows-v"></i></span>
                                    <!-- Option Table -->
                                    <table class="table option-table edit-option-table">
                                        <tbody class="after-768-none">
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Enter your option name">
                                                </td>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox"> <span>*</span>This is Mandatory
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox" id="mul-options1"> Allow multiple options
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="max-input-box1">
                                                        <input type="text" placeholder="Max">
                                                    </div>
                                                </td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" checked="">
                                                        <span data-off="No" data-on="Yes" class="switch-label"></span> <span class="switch-handle"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#optionDelete" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        
                                        <!-- For Lower Screen Below 768 -->
                                        <tbody class="after-768-show">
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Enter your option name">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox"> <span>*</span>This is Mandatory
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox" id="mul-options-mob1"> Allow multiple options
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="max-input-box-mob1">
                                                        <input type="text" placeholder="Max">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" checked="">
                                                        <span data-off="No" data-on="Yes" class="switch-label"></span> <span class="switch-handle"></span>
                                                    </label>
                                                    <a data-toggle="modal" data-target="#optionDelete" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <!-- End For Lower Screen Below 768 -->
                                    </table>
                                    <!-- End Option Table -->
                                
                                    <!-- Sizes Table -->                            
                                    <table class="table table-hover sizes-table tble-drag">
                                        <thead>
                                            <tr>
                                                <th>Sides</th>
                                                <th>Price</th>
                                                <th><a href="javascript:void(0);"><i class="fa fa-plus"></i></a></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody class="tile__list">
                                            <tr>
                                                <td class="my-handle"><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#noDeleteFirstRow" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- End Sizes Table -->
                                </div>
                                <!-- End Option Table -->
                                
                                <!-- Option Table -->
                                <div class="form-box option-table-container tile">
                                	<span class="multi-handle"><i class="fa fa-arrows-v"></i></span>
                                    <!-- Option Table -->
                                    <table class="table option-table edit-option-table">
                                        <tbody class="after-768-none">
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Enter your option name">
                                                </td>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox"> <span>*</span>This is Mandatory
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox" id="mul-options2"> Allow multiple options
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="max-input-box2">
                                                        <input type="text" placeholder="Max">
                                                    </div>
                                                </td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" checked="">
                                                        <span data-off="No" data-on="Yes" class="switch-label"></span> <span class="switch-handle"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#optionDelete" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        
                                        <!-- For Lower Screen Below 768 -->
                                        <tbody class="after-768-show">
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Enter your option name">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox"> <span>*</span>This is Mandatory
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="label-check-box">
                                                        <label>
                                                            <input type="checkbox" id="mul-options-mob2"> Allow multiple options
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="max-input-box-mob2">
                                                        <input type="text" placeholder="Max">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" checked="">
                                                        <span data-off="No" data-on="Yes" class="switch-label"></span> <span class="switch-handle"></span>
                                                    </label>
                                                    <a data-toggle="modal" data-target="#optionDelete" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <!-- End For Lower Screen Below 768 -->
                                    </table>
                                    <!-- End Option Table -->
                                
                                    <!-- Sizes Table -->                            
                                    <table class="table table-hover sizes-table tble-drag">
                                        <thead>
                                            <tr>
                                                <th>Sides</th>
                                                <th>Price</th>
                                                <th><a href="javascript:void(0);"><i class="fa fa-plus"></i></a></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody class="tile__list">
                                            <tr>
                                                <td><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#noDeleteFirstRow" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" placeholder="Sides" class="addInput" /></td>
                                                <td><input type="text" placeholder="Price" /></td>
                                                <td><a href="javascript:void(0);" class="rowDelete" data-target="#rowDelete" data-toggle="modal"><i class="fa fa-times"></i></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- End Sizes Table -->
                                </div>
                                <!-- End Option Table -->
                            </div>
                        </div>
                        <!-- End Options & Sides -->
                        
                        <!-- Bottom Save Button -->
                        <div class="bottom-save-btn">
                        	<div class=" row">
                            	<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 col-lg-offset-10 col-md-offset-10 col-sm-offset-9 col-xs-offset-8">
                        			<button>Save</button>
                                </div>
                            </div>
                        </div>
                        <!-- End Bottom Save Button -->
                    </form>
                    <!-- End Start Form -->
                </div>
                <!-- End Add Dish Form -->
            </div>
        </section>
        <!-- ===== End Add Dish ===== -->
        
        <!-- ===== Start Footer ===== -->

        
        <!-- Delete Row -->
        <div class="cat-delete modal fade" id="rowDelete" tabindex="-1" role="dialog" aria-labelledby="cat-delete-dish">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <a aria-label="Close" data-dismiss="modal" href="#"><i class="fa fa-times"></i></a>
                        <h6>Delete Side</h6>
                    </div>
                    <!-- End Modal Header -->
                    
                    <!-- Modal Body -->
                    <div class="modal-body">
                        <p>Are you sure you want to delete?</p>
                    </div>
                    <!-- End Modal Body -->
                        
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <button aria-label="Close" data-dismiss="modal" class="dlt-ok" id="btnDelteYes">Ok</button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <button aria-label="Close" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal Footer -->
                </div>
            </div>
        </div>
        <!-- End Delete Row -->  
        
        <!-- Delete Option -->
        <div class="cat-delete modal fade" id="optionDelete" tabindex="-1" role="dialog" aria-labelledby="delete-option">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <a aria-label="Close" data-dismiss="modal" href="#"><i class="fa fa-times"></i></a>
                        <h6>Delete Option</h6>
                    </div>
                    <!-- End Modal Header -->
                    
                    <!-- Modal Body -->
                    <div class="modal-body">
                        <p>Are you sure you want to delete?</p>
                    </div>
                    <!-- End Modal Body -->
                        
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <button aria-label="Close" data-dismiss="modal" class="dlt-ok" id="btnDelteYes">Ok</button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <button aria-label="Close" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal Footer -->
                </div>
            </div>
        </div>
        <!-- End Delete Option --> 
        
        <!-- No Delete First Row -->
        <div class="cat-delete modal fade" id="noDeleteFirstRow" tabindex="-1" role="dialog" aria-labelledby="cat-delete-dish">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <a aria-label="Close" data-dismiss="modal" href="#"><i class="fa fa-times"></i></a>
                        <h6>Delete Side</h6>
                    </div>
                    <!-- End Modal Header -->
                    
                    <!-- Modal Body -->
                    <div class="modal-body">
                        <p>First row can't be deleted</p>
                    </div>
                    <!-- End Modal Body -->
                        
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                    	<div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col-lg-offset-9 col-md-offset-9 col-sm-offset-9 col-xs-offset-9 display-ful-480">
                                <button data-dismiss="modal" aria-label="Close">Ok</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal Footer -->
                </div>
            </div>
        </div>
        <!-- End No Delete First Row -->
    
