<?php
ob_start();
session_start();
?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon | Signup </title>
        <!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script>-->
        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>


        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- FORM CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/form.css" media="screen">

        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>
        <script src="<?= base_url() ?>js/star-rating.js" type="text/javascript"></script>


  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <style>
            .placeholder::-webkit-input-placeholder { color: red !important;}
            .placeholder:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                color:    red !important;
                opacity:  1;
            }
            .placeholder::-moz-placeholder { /* Mozilla Firefox 19+ */
                color:    red !important;
                opacity:  1;
            }
            .placeholder:-ms-input-placeholder { /* Internet Explorer 10+ */
                color:   red !important;
            }
        </style>
    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <!-- Start Header ( Logo & Naviagtion ) -->
            <div class="navbar navbar-default navbar-top greenbdr">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="<?= base_url() ?>">
                            <img alt="" src="<?= base_url() ?>images/replimatic_logo.png">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="navbar-collapse collapse borderbg">       
                        <!-- Stat Search -->
                        <div class="buttons-side">
                            <?php
                            $user_data = $this->session->userdata('member');
                            $ins_id = $user_data->member_id;
                            if ($ins_id == '') {
                                ?>
                                <a class="btn btn-default btnlogin" href="<?= base_url() ?>member/login" role="button">LOGIN</a>
                                <a class="btn btn-default btnsignup" href="<?= base_url() ?>member/signup" role="button">SIGN UP</a>
                            <?php } else { ?>
                                <a class="btn btn-default btnlogin" href="<?= base_url() ?>member/logout" role="button">LOGOUT</a>
                            <?php } ?>
                        </div>
                        <!-- End Search -->
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <!--<li><a href="#" class="linkcolor">For Subscribers </a></li>-->             
                        </ul>
                        <!-- End Navigation List -->

                    </div>
                </div>

                <!-- Mobile Menu Start -->

                <!-- Mobile Menu End -->

            </div>
            <!-- End Header ( Logo & Naviagtion ) -->

        </header>
        <!-- End Header -->


        <script>
            $(window).load(function () {
//                $("#sel_pc_id_res").val('');
//                $("#sel_pc_val_res").val('');
                $("#email123").val('');
                $("#psd123").val('');
            });

            $(document).ready(function () {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showLocation);
                } else {
                    $('#location').html('Geolocation is not supported by this browser.');
                }

            });

            function showLocation(position) {
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                $("#latitude").val(latitude);
                $("#longitude").val(longitude);
                cus_data();
            }



        </script>


        <style>

            #myList li{ display:none;
            }
            #loadMore {
                color:green;
                cursor:pointer;
            }
            #loadMore:hover {
                color:black;
            }
            #showLess {
                color:red;
                cursor:pointer;
            }
            #showLess:hover {
                color:black;
            }
            .test_star .rating-gly-star {
                letter-spacing:10px;
                color: green;
            }
            .modal {
                text-align: center;
                padding: 0!important;
            }

            .modal:before {
                content: '';
                display: inline-block;
                height: 100%;
                vertical-align: middle;
                margin-right: -4px;
            }

            .modal-dialog {
                display: inline-block;
                text-align: left;
                vertical-align: middle;
            }
            .modal-header{
                background-color:#72CA42;
                text-align:center;
                color:#fff !important;
            }
            .modal-header h4{
                color:#fff;	
            }

            .modal-footer{
                border-top:none;	
            }
            .btn_save{ background-color:#72CA42; color:#fff; padding:6px 15px; border:none;}
            .btn_close{ background-color:#2D4C1C; color:#fff; padding:6px 15px; border:none;}
            .text_box{ border:none ; border-bottom:1px solid #c2c2c2;}

        </style>
        <!-- Start Content -->
        <div id="content">
            <div class="container">

                <!--begin tabs going in wide content -->
                <ul class="nav nav-tabs tabsignup content-tabs hidden-xs" id="maincontent" role="tablist">
                    <!--<li  id="home_tes1" class="col-lg-4 tablist active"><a data-parent="" class="js-tabcollapse-panel-heading" id="home_href" href="javascript:void(0)"  ><span>1</span>Fulspoon DETAILS</a></li>-->
                    <li id="profile_tes1"  class="col-lg-12 tablist active"><a data-parent="" class="js-tabcollapse-panel-heading" id="profile_href" href="javascript:void(0)" ><span>1</span>Select Restaurants</a></li>

                </ul>


                <!--/.nav-tabs.content-tabs -->


                <div class="tab-content hidden-xs ">


                    <div class="tab-pane active" id="profile" >       
                        <!-- SELECT REATAURANT -->
                        <?php
                        if ($cusine_config['value'] == "Y") {
                            ?>
                            <div class="col-lg-3 col-md-3 col-sm-4 filterwrap">
                                <h3>Cuisine  <a href="#;" onclick="clear_filter('cuisine')">Clear</a></h3>
                                <div class="col-lg-12">
                                    <form>  
                                        <input type="hidden" id="latitude">
                                        <input type="hidden" id="longitude">
                                        <ul id="myList">
                                            <?php
                                            $i = 0;
                                            foreach ($cuisines as $cus) {
                                                ?>
                                                <div class="checkbox">
                                                    <li>
                                                        <input name="cus22" onChange="cus_data()" class="test1234" id="male22<?= $i ?>" type="checkbox" value="<?= $cus['cusine_id'] ?>">
                                                        <label for="male22<?= $i ?>"><?= $cus['cusine_name']; ?></label>
                                                    </li>
                                                </div>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </ul>
                                        <div id="loadMore" class="col-lg-6" style="text-align:left; margin-top:20px; padding:0;">Load more <i class="fa fa-caret-down"></i>
                                        </div>
                                        <div id="showLess" class="col-lg-6" style="text-align:right; margin-top:20px; padding:0;">Show less <i style="font-size:14px; color:#000;" class="fa fa-caret-up"></i>
                                            </i>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php
                            if ($rating_config['value'] == "Y") {
                                ?>
                                <h3>Rating  <a href="javascript:void(0)" onclick="clear_filter('rating')">Clear</a></h3>
                                <div class="col-lg-12">
                                    <input id="change_rate" class="rating" onChange="cus_data()" data-min="0"  data-show-clear="false" data-max="5" data-step="0.1">
                                    <a>& up</a>
                                    <div class="clearfix"></div>
                                </div> 
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php
                            if ($price_config['value'] == "Y") {
                                ?>
                                <h3>Price  <a href="javascript:void(0)" onclick="clear_filter('price')">Clear</a></h3>
                                <div class="col-lg-12">
                                    <!--<input id="change_price" onChange="cus_data()" class="rating" data-stars=4 data-min="0"  data-show-clear="false" data-max="4" data-step="1" value="0">-->

                                    <input style="color: green; min-height: 30px" id="change_price" data-stars=4  class="rating test_star" data-min="1" onChange="cus_data()" data-show-clear="false" data-max="4" data-step="1" value="0" data-symbol="$" >

                                </div>
                            <?php } ?>    <div class="clearfix"></div>
                            <?php
                            if ($feature_config['value'] == "Y") {
                                ?>
                                <h3>FEATURE  <a href="javascript:void(0)" onclick="clear_filter('feature')">Clear</a></h3>
                                <div class="col-lg-12">
                                    <div class="checkbox"><input id="alcohol" value="alcohol" type="checkbox" name="feature" onChange="cus_data()"><label for="alcohol">Alcohol</label></div>
                                    <div class="checkbox"><input id="parking" value="parking" type="checkbox" name="feature" onChange="cus_data()"><label for="parking">parking</label></div>
                                    <div class="checkbox"><input id="wifi" value="wifi" type="checkbox" name="feature" onChange="cus_data()"><label for="wifi">wifi</label></div>
                                    <div class="checkbox"><input id="corkage" value="corkage" type="checkbox" name="feature" onChange="cus_data()"><label for="corkage">corkage</label></div>   
                                    <div class="checkbox"><input id="dietary_restrictions" value="dietary_restrictions" type="checkbox" name="feature" onChange="cus_data()"><label for="dietary_restrictions">Dietary Restrictions</label></div>    
                                    <div class="checkbox"><input id="music" value="music" type="checkbox" name="feature" onChange="cus_data()"><label for="music">music</label></div>
                                    <div class="checkbox"><input id="sports" value="sports" type="checkbox" name="feature" onChange="cus_data()"><label for="sports">sports</label></div>
                                    <div class="checkbox"><input id="wheelchair_accessible" value="wheelchair_accessible" type="checkbox" name="feature" onChange="cus_data()"><label for="wheelchair_accessible">Wheelchair Accessible</label></div>
                                    <div class="checkbox"><input id="outdoor_seating" value="outdoor_seating" type="checkbox" name="feature" onChange="cus_data()"><label for="outdoor_seating">outdoor Seating</label></div>
                                    <div class="checkbox"><input id="waiter_service" value="waiter_service" type="checkbox" name="feature" onChange="cus_data()"><label for="waiter_service">waiter_service</label></div>
                                    <div class="checkbox"><input id="television" value="television" type="checkbox" name="feature" onChange="cus_data()"><label for="television">television</label></div>
                                    <div class="checkbox"><input id="ambience" value="ambience" type="checkbox" name="feature" onChange="cus_data()"><label for="ambience">ambience</label></div>
                                    <div class="checkbox"><input id="smoking" value="smoking" type="checkbox" name="feature" onChange="cus_data()"><label for="smoking">Smoking</label></div>
                                </div>

                            <?php } ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8  select-wrapper">
                            <h3 class="head30 text-center col-lg-12" style="color:#000 !important; font-weight:400; font-size:26px;" >Select the Restaurants you want to subscribe to &nbsp; <a href="<?= base_url() ?>member/change_subscription_details">    <button style="background: #72CA42" class="btn btn-default pull-right ">Restore</button></a></h3>

                            <div class="col-lg-12 searchitem"><input type="email" class="form-control searchhide" name="search_key" id="search_key" placeholder="Search Restaurants"></div>
                            <div class="col-md-6 resultcount" id="count_data">
                                <?php echo ($rest_count == 1 || $rest_count == 0) ? $rest_count . ' Result' : $rest_count . ' Results' ?>  near by you
                            </div>
                            <div class="col-md-6 resultcount">
                                <div class="form-group">
                                    <!--<label for="inputPassword" class="col-sm-3 control-label">Sort by</label>-->
                                    <div class="col-sm-9">
                                        <select id="sort_type" class="form-control">
                                            <option value="default" selected="selected">Default</option>
                                            <option value="price_asc">Price (Ascending)</option>
                                            <option value="price_desc">Price (Descending)</option>
                                            <option value="rating">Rating</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br/><br/>
                            <!-- SEARCH RESULT-->
                            <form id="form_2_tab">
                                <div class="profile_pc">

                                    <?php
                                    $sel_res_id_str = 0;
                                    $res_sel_count = 0;
                                    foreach ($response_datas as $hotel) {
                                        $rating = round($hotel['hotel_data']->rating);
                                        $rate_differ = 5 - $rating;
                                        $image = $hotel['hotel_data']->photos[0]->photo_reference;
                                        $price_rating = round($hotel['hotel_data']->price_level);
                                        $p_rate = 4 - $price_rating;
                                        ?>
                                        <div style="cursor: auto" class="result-wrapper assign_wrapper_pc"  id="rest_id<?= $hotel['hotel_data']->place_id ?>" >


                                            <?php
                                            $xyz_flag = 0;

                                            foreach ($select_restaurents as $sel_rest) {

                                                if ($sel_rest['restaurant_id'] == $hotel['restaurant_id']) {
                                                    $xyz_flag = 1;
                                                    echo '<div class="select-wrapperover sel_pc_flag"></div>
                   <input type="hidden" value="select" class="sel_val">
                         ';
                                                    $res_sel_count++;
                                                    if ($sel_res_id_str == 0) {
                                                        $sel_res_id_str = $hotel['restaurant_id'];
                                                    } else {
                                                        $sel_res_id_str = $sel_res_id_str . ',' . $hotel['restaurant_id'];
                                                    }
                                                }
                                            }
                                            if ($xyz_flag == 0) {
                                                echo ' <input type="hidden" value="unselect" class="sel_val">';
                                            }
                                            ?>         

                                            <input type="hidden" value="<?= $hotel['hotel_data']->place_id ?>" name="res_id" id="res_id_pc" class="res_id_pc">
                                            <input type="hidden" class="sel_res_this" value="<?= $hotel['restaurant_id'] ?>">
                                            <div class="col-md-2 text-center"><img src='https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&photoreference=<?= $image ?>&key=<?= $key ?>' width="150" height="150" alt="" class="thumb100"/></div>
                                            <div class="col-md-5 restaurantinfo">
                                                <h5><?= $hotel['hotel_data']->name; ?></h5><?php
                                                $ccccc123 = '';
                                                foreach ($hotel['cuisines'] as $cusine) {
                                                    if (strlen($ccccc123) <= 25) {
                                                        if ($ccccc123 == '') {
                                                            echo $cusine['cusine_name'];
                                                        } else {
                                                            echo ',' . $cusine['cusine_name'];
                                                        }
                                                        $ccccc123.=$cusine['cusine_name'];
                                                    } else {
                                                        echo '.....';
                                                        break;
                                                    }
                                                }
                                                ?>
                                                <p style="font-weight: 400"><i class="fa fa-map-marker"></i> <?= $hotel['hotel_data']->formatted_address; ?></p>                  
                                            </div>
                                            <div class="col-md-5">
                                                <div class="col-md-8">
                                                    <p >
                                                        <?php
                                                        if ($rating != '') {
                                                            $non_rate = 5 - $rating;
                                                            for ($i = 1; $i <= $rating; $i++) {
                                                                ?>
                                                                <img src="<?= base_url() ?>images/star.png">
                                                                <?php
                                                            }
                                                            for ($i = 1; $i <= $non_rate; $i++) {
                                                                ?>
                                                                <img src="<?= base_url() ?>images/star00.png">
                                                                <?php
                                                            }
                                                        }
                                                        ?><br><?= $hotel['hotel_data']->user_ratings_total; ?> ratings
                                                    </p>

                                                    <p class="middle">
                                                        <?php
                                                        if ($price_rating != '') {
                                                            for ($i = 1; $i <= $price_rating; $i++) {
                                                                ?>
                                                                <img src="<?= base_url() ?>images/dollar1.png">
                                                                <?php
                                                            }
                                                            for ($i = 1; $i <= $p_rate; $i++) {
                                                                ?>
                                                                <img src="<?= base_url() ?>images/dollar0.png">
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </p>    
                                                </div>
                                                <div class="col-md-2"><p class="middle deleverystat">
                                                        <?php if ($hotel['delivery'] == 'yes' && ($hotel['delivery_fee'] == 0 || $hotel['delivery_fee'] == '' || $hotel['delivery_fee'] == '0')) {
                                                            ?>
                                                            FREE<br><span>Delivery</span>
                                                            <?php
                                                        } elseif ($hotel['delivery'] == 'yes') {

                                                            echo '$' . $hotel['delivery_fee']
                                                            ?>  <br><span>Delivery Fee </span>

                                                        <?php } else { ?>
                                                            No Delivery  <br><span> Available</span>
                                                        <?php } ?>


                                                    </p></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    <?php } ?>
                                    <input type="hidden" id="sel_pc_val_res" value="<?= $res_sel_count ?>">
                                    <input type="hidden" id="sel_pc_id_res" name="sel_pc_id_res" value="<?= $sel_res_id_str ?>">
                                </div>
                            </form>
                            <button class="pull-right btn btngreen" onclick="save_rest()">Change My Subscription </button>
                            <!-- END SEARCH RESULT-->
                            <!-- SEARCH RESULT-->
                            <!-- END SEARCH RESULT-->
                            <!-- SEARCH RESULT-->

                            <!-- END SEARCH RESULT-->
                        </div>
                        <div class="clearfix"></div>

                        <!-- END SELECT REATAURANT -->
                    </div><!--/.tab-pane -->



                </div>
            </div>
        </div>

        <form action="<?= base_url() ?>member/save_package_user" method="post" id="edit_form">
            <input type="hidden" name="res_string_pc_c" id="res_string_pc_c">
            <input type="hidden" name="subscip_id" id="subscip_id">
        </form>



        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" style="z-index:10001">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Message</h4>
                    </div>
                    <div class="modal-body">
                        <p style="text-align:center;"></p>
                        <h6 style="font-size:20px; border-bottom:1px solid #f2f2f2; padding-bottom:5px;">Packages</h6>
                        <ul>
                            <li style="text-align:center; font-size:18px;"></li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btngreen" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn ">Save changes</button>-->
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- End content -->


        <!-- Start Footer Section -->
        <div id="preloader"></div>
</html>
<script>
    function assign_wrapper(id) {
//                                        alert(id);
        $("#rest_id" + id).hide();
        $("#remve_rest_id" + id).show();
    }
    function remove_wrapper(id) {
//                                        alert(id);
        $("#remve_rest_id" + id).hide();
        $("#rest_id" + id).show();
    }

    function cus_data1() {
        //     alert("sd");
        var allVals = [];
        $('cus22[] :checked').each(function () {
            alert("sd");
        });
    }

    function clear_filter(type)
    {
        if (type == 'cuisine')
        {
            $('input[name="cus22"]').attr('checked', false);
        }
        else if (type == 'rating')
        {

            $("#change_rate").parent('.rating-container').children('.rating-stars').css("width", "0%");
            $("#change_rate").val(0);
        }
        else if (type == 'price')
        {
            $("#change_price").parent('.rating-container').children('.rating-stars').css("width", "0%");
            $("#change_price").val(0);
        }
        else if (type == 'feature')
        {
            $('input[name="feature"]').attr('checked', false);
        }
        else
        {
            $('input[name="cus22"]').attr('checked', false);
            $("#change_rate").val(0);
            $("#change_price").val(0);
            $('input[name="feature"]').attr('checked', false);
        }

        cus_data();
    }

    $("#search_key").keyup(function (event)
    {
        if (event.keyCode == 13)
            cus_data();
    });
    $("#sort_type").change(function (event)
    {
        cus_data();
    });
    function cus_data() {
//                                        alert("test");
        $("#preloader").show();
        var selected_hotel = $("#sel_pc_id_res").val();
//        alert(selected_hotel);
        var change_rate = $("#change_rate").val();
        var change_price = $("#change_price").val();
        var latitude = $("#latitude").val();
        var longitude = $("#longitude").val();
        // alret(latitude+longitude);
        var cus_data3 = new Array();
        var fea_array = new Array();
        var search_key = $("#search_key").val();
        var sort_type = $("#sort_type").val();
        var i = 0;
        $('input[name="cus22"]:checked').each(function () {
            cus_data3[i] = this.value;
            i++;
        });
        i = 0;
        $('input[name="feature"]:checked').each(function () {
            fea_array[i] = this.value;
            i++;
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . "member/cus_rest_check"; ?>",
            data: {cus_select: cus_data3,
                fea_array: fea_array,
                latitude: latitude,
                longitude: longitude,
                selected_hotel: selected_hotel,
                change_rate: change_rate,
                change_price: change_price,
                search_key: search_key,
                sort_type: sort_type},
            success: function (data) {
                $(".profile_pc").empty();
                $(".profile_pc").html(data);
                $("#preloader").hide();
                if (!$("#count_ajax").val())
                    $("#count_data").html('0 Result near by you')
                else if ($("#count_ajax").val() == 0 || $("#count_ajax").val() == 1)
                    $("#count_data").html($("#count_ajax").val() + ' Result near by you')
                else
                    $("#count_data").html($("#count_ajax").val() + ' Results near by you')
            }
        }
        );
    }
    function save_rest() {
        var selected_val = $('#sel_pc_val_res').val();
        var res_string = $("#sel_pc_id_res").val();
        if (selected_val > 0) {
            $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>member/get_subscription_detail_web",
                data: {count: selected_val, res_string: res_string},
                success: function (data) {
                    //  alert(data);

                    var response = data.split('$$$$#####&&&***');
                    if (response[0].indexOf("true") >= 0) {
                        $("#pck_summ_pc").empty();
                        $("#pck_summ_pc").html(response[1]);

                        $("#res_string_pc_c").val(res_string);
                        $("#subscip_id").val(response[2]);
                        $("#edit_form").submit();

                    }
                    else {
                        $(".modal-body").empty();
                        $(".modal-body").html(response[1]);
                        $('#myModal').modal('show');
                    }
//                                                                alert(data);
                }
            });
        } else {
            $("#confirm_tes").attr("id", "confirm_tes1");
            var msg = "<p style='color:green;text-align:center;font-size:14px'>Please select your restaurants</p>";
            $(".modal-body").empty();
            $(".modal-body").html(msg);
            $('#myModal').modal('show');
        }
    }
    $(document).on("click", ".assign_wrapper_pc", function () {
        var res_id = $(this).children('.res_id_pc').val();
        var sel_flag = $(this).children('.sel_val').val();
        if (sel_flag == 'unselect') {
            $(this).append('<div class="select-wrapperover sel_pc_flag"></div>');
            $(this).children('.sel_val').val('select');
            var res_id_pc = $(this).children('.sel_res_this').val();
            var sel_res_pc_val = $("#sel_pc_id_res").val();
//            alert(sel_res_pc_val);
            if (sel_res_pc_val == '') {
                sel_res_pc_val = res_id_pc;
                $("#sel_pc_id_res").val(sel_res_pc_val);
            } else {

                sel_res_pc_val = sel_res_pc_val + ',' + res_id_pc;
                $("#sel_pc_id_res").val(sel_res_pc_val);
                var res_explode = sel_res_pc_val.split(',');
//                alert(res_explode[5]);
            }

            var selected_val = $('#sel_pc_val_res').val();
            selected_val++;
            //  alert(selected_val);
            $('#sel_pc_val_res').val(selected_val);
        } else if (sel_flag == 'select') {
            $(this).children('.sel_pc_flag').remove();
            $(this).children('.sel_val').val('unselect');
            var res_id_pc = $(this).children('.sel_res_this').val();
//            alert(res_id_pc);
            var sel_res_pc_val = $("#sel_pc_id_res").val();
//            alert(sel_res_pc_val);

            var res_explode = sel_res_pc_val.split(',');
            var new_string = '';
            $.each(res_explode, function (number) {
                if (res_explode[number] != res_id_pc) {
                    if (new_string == '') {
                        new_string = res_explode[number];
                    } else {
                        new_string = new_string + ',' + res_explode[number];
                    }
                }
//alert(res_explode[number]);
            });
            $("#sel_pc_id_res").val(new_string);
//            alert(new_string);
//            $("#sel_pc_id_res").val(sel_res_pc_val);
            var selected_val = $('#sel_pc_val_res').val();
            selected_val--;
            $('#sel_pc_val_res').val(selected_val);
        }
    });
    $(document).ready(function () {
        var size_li = $("#myList li").size();
//        alert(size_li);
        var x = 10;
        $('#myList li:lt(' + x + ')').show();
        $('#loadMore').click(function () {
            x = (x + 10 <= size_li) ? x + 10 : size_li;
            $('#myList li:lt(' + x + ')').show();
        });
        $('#showLess').click(function () {
            x = (x - 10 < 0) ? 10 : x - 10;
            $('#myList li').not(':lt(' + x + ')').hide();
        });

        $("#zip_save").click(function () {
            var zip = $("#zip_onload").val();
            if (zip == '') {
                $("#zip_onload").addClass('placeholder');
                $("#zip_onload").val('');
                $('#zip_onload').attr('placeholder', "Please Enter a valid zip code.");
                $("#zip_onload").focus();
            }
            else {
                $("#zip_code").val(zip);
                $("#search_key").val(zip);
                $('#zip_modal').modal('hide');
            }
            //cus_data();
//            $("#zip_modal").modal('close');
        });
        $("#change_link").click(function () {


            var fname = $('#fname123').val();
            var lname = $('#lname123').val();
            var email = $('#email123').val();
            var psd = $('#psd123').val();
            if (fname == '') {
                $("#fname123").addClass('placeholder');
                $("#fname123").val('');
                $('#fname123').attr('placeholder', "Your first name can't be null ");
                $("#fname123").focus();
            } else if (lname == '') {
                $("#lname123").addClass('placeholder');
                $("#lname123").val('');
                $('#lname123').attr('placeholder', "Your last name can.'t be null ");
                $("#lname123").focus();
            }
            else if (email == '') {
                $("#email123").addClass('placeholder');
                $("#email123").val('');
                $('#email123').attr('placeholder', "Your email can't be null ");
                $("#email123").focus();
            } else if (!validateEmail(email)) {
                $("#email123").addClass('placeholder');
                $("#email123").val('');
                $('#email123').attr('placeholder', "Please enter a valid email address ");
                $("#email123").focus();
            } else if (psd == '') {
                $("#psd123").addClass('placeholder');
                $("#psd123").val('');
                $('#psd123').attr('placeholder', "Your password can't be null ");
                $("#psd123").focus();
            }
            else {                //  alert(email);
                //$("#myModal").html("");
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url(); ?>member_1/email_exist",
                    data: {fname: fname, lname: lname, email: email, psd: psd},
                    success: function (data) {
                        if (data.indexOf('error') >= 0)
                        {
                            $("#email123").addClass('placeholder');
                            $("#email123").val('');
                            $('#email123').attr('placeholder', 'Your email id is already exist.');
                            $("#email123").focus();
                        }
                        if (data.indexOf('success') >= 0)
                        {
                            cus_data();
                            $("#fname_cnfirm").val(fname);
                            $("#lname_cnfirm").val(lname);
                            $("#email_cnfirm").val(email);
                            $("#psd_cnfirm").val(psd);
                            $("#home_tes1").attr("id", "home_tes");
                            $("#profile_tes1").attr("id", "profile_tes");
                            $("div#home").hide();
                            $("div#profile").show();
                            $("a#home_href").attr("href", "#home");
                            $('#home_tes').removeClass('active');
                            $('#profile_tes').addClass('active');

                            // alert("test");
                        }

//                                                                alert(data);
                    }
                });
            }


        }
        );
    });
    $(document).on("click", "#home_tes", function () {
        $('#profile_tes').removeClass('active');
        $('#confirm_tes').removeClass('active');
        $('#home_tes').addClass('active');
        $("div#home").show();
        $("div#profile").hide();
        $("div#confirm").hide();
    });
    $(document).on("click", "#profile_tes", function () {

        $('#home_tes').removeClass('active');
        $('#confirm_tes').removeClass('active');
        $('#profile_tes').addClass('active');
        $("div#home").hide();
        $("div#confirm").hide();
        $("div#profile").show();
    });
    $(document).on("click", "#confirm_tes", function () {
        $('#profile_tes').removeClass('active');
        $('#home_tes').removeClass('active');
        $('#confirm_tes').addClass('active');
        $("div#confirm").show();
        $("div#home").hide();
        $("div#profile").hide();
    });
    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }
    function card_val_btn() {
//                                    alert("sss");
        var e = document.getElementById("exmonth");
//                                                    alert(e);
        Stripe.setPublishableKey("pk_test_FvgVInxV16FbpMbZ3VxtcJt1");
        var cname = $('#cname').val();
        var month = e.options[e.selectedIndex].value;
        var f = document.getElementById("card_year");
        var year = f.options[f.selectedIndex].value;
        var cardno = $('#crno').val();
        var cvv = $('#cvv').val();
        var cname = $('#cname').val();
        var errorPaymentInfo = '';
        if (cname == '') {
            $("#cname").addClass('placeholder');
            $("#cname").val('');
            $('#cname').attr('placeholder', "Enter a valid name ");
            $("#cname").focus();
        }
        else if (!Stripe.card.validateCardNumber(cardno)) {
            errorPaymentInfo = "Invalid card number";
            $("#crno").addClass('placeholder');
            $("#crno").val('');
            $('#crno').attr('placeholder', errorPaymentInfo);
            $("#crno").focus();
//            alert(errorPaymentInfo);
        } else if (!Stripe.card.validateCVC(cvv)) {
            errorPaymentInfo = "Invalid CVV code";
            $("#cvv").addClass('placeholder');
            $("#cvv").val('');
            $('#cvv').attr('placeholder', errorPaymentInfo);
            $("#cvv").focus();
        } else if (!Stripe.card.validateExpiry(month, year)) {
            errorPaymentInfo = "Invalid expiry date";
//            $("#exmonth").addClass('placeholder');
//            $("#exmonth").val('');
//            $('#exmonth').attr('placeholder', errorPaymentInfo);
            $("#exmonth").focus();
            $(".error_stripe").empty();
            $(".error_stripe").html("<p style='color:red'>" + errorPaymentInfo + "</p>");
        } else {
            $(".error_stripe").empty();
            Stripe.card.createToken({
                number: cardno,
                cvc: cvv,
                exp_month: month,
                exp_year: year
            }, stripeResponseHandler);
        }
    }



    var stripeResponseHandler = function (status, response) {
        var cname = $('#cname').val();
        var add1 = $('#add1').val();
        var add2 = $('#add2').val();
        var city = $('#city').val();
        var zipcode = $('#zipcode').val();
        var save_crd = $('#save_crd').val();
        var radd1 = $('#radd1').val();
        var radd2 = $('#radd2').val();
        var rcity = $('#rcity').val();
        var rzipcode = $('#rzipcode').val();
        var rsave_crd = $('#rsave_crd').val();
        var errorPaymentInfo = '';
        if (cname == '') {
            $("#cname").addClass('placeholder');
            $("#cname").val('');
            $('#cname').attr('placeholder', "Enter a valid name ");
            $("#cname").focus();
        } else if (add1 == '') {
            $("#add1").addClass('placeholder');
            $("#add1").val('');
            $('#add1').attr('placeholder', "Enter a valid address ");
            $("#add1").focus();
        } else if (city == '') {
            $("#city").addClass('placeholder');
            $("#city").val('');
            $('#city').attr('placeholder', "Enter a valid city ");
            $("#city").focus();
        } else if (zipcode == '') {
            $("#zipcode").addClass('placeholder');
            $("#zipcode").val('');
            $('#zipcode').attr('placeholder', "Enter a valid Zip code ");
            $("#zipcode").focus();
        } else if (save_crd == '') {
            $("#save_crd").addClass('placeholder');
            $("#save_crd").val('');
            $('#save_crd').attr('placeholder', "Enter a valid save address  ");
            $("#save_crd").focus();
        } else if (radd1 == '') {
            $("#radd1").addClass('placeholder');
            $("#radd1").val('');
            $('#radd1').attr('placeholder', "Enter a valid address ");
            $("#radd1").focus();
        } else if (rcity == '') {
            $("#rcity").addClass('placeholder');
            $("#rcity").val('');
            $('#rcity').attr('placeholder', "Enter a valid city name ");
            $("#rcity").focus();
        } else if (rzipcode == '') {
            $("#rzipcode").addClass('placeholder');
            $("#rzipcode").val('');
            $('#rzipcode').attr('placeholder', "Enter a valid Zipcode ");
            $("#rzipcode").focus();
        }
        else {
            if (response.error) {
                errorPaymentInf = response.error.message;
                return false;
            } else {
                var subscr_id = $("#subscip_id").val();
                var token = response.id;
                var last4 = response.card.last4;
                var brand = response.card.brand;
                var data = {'token': token, 'last4': last4, 'brand': brand};
                var user_id = 1;
                $("#preloader").show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() . "member_1/email_check_card_detail"; ?>",
                    data: {'token': token, 'last4': last4, 'subscr_id': subscr_id, 'brand': brand, 'cname': cname, 'add1': add1, 'add2': add2, 'city': city, 'zipcode': zipcode, 'save_crd': save_crd, 'radd1': radd1, 'radd2': radd2, 'rcity': rcity, 'rzipcode': rzipcode, 'rsave_crd': rsave_crd},
                    success: function (response) {
                        var data = response.split('$$$$$$$$$$$$$$$*****@@@*******#####^^^^');
                        if (data[0].indexOf("error") >= 0) {
                            $("#preloader").hide();
                            $("#crno").addClass('placeholder');
                            $("#crno").val('');
                            $('#crno').attr('placeholder', "Enter a valid Card Number ");
                            $("#crno").focus();
                        } else {
                            $("#tocken").val(data[1]);
                            $("#last_4").val(last4);
                            $("#price").val(data[2]);
                            $("#brand").val(brand);
                            $("#card_resp").val(data[0]);
                            $("#form_card_payment").submit();
                        }
                    }
                });
            }
        }
    };





    function check_btn_add()
    {
        var same_data = $('#same').is(':checked');
        if (same_data == true)
        {
            var add1 = $('#add1').val();
            var add2 = $('#add2').val();
            var city = $('#city').val();
            var zipcode = $('#zipcode').val();
            var save_crd = $('#save_crd').val();
            var e = document.getElementById("state");
            var state = e.options[e.selectedIndex].value;
            document.getElementById("radd1").value = add1;
            document.getElementById("radd2").value = add2;
            document.getElementById("rcity").value = city;
            document.getElementById("rzipcode").value = zipcode;
            document.getElementById("rsave_crd").value = save_crd;
            $('#rstate option[value=' + state + ']').attr('selected', 'selected');
            document.getElementById('radd1').readOnly = true;
            document.getElementById('radd2').readOnly = true;
            document.getElementById('rcity').readOnly = true;
            document.getElementById('rzipcode').readOnly = true;
            document.getElementById('rsave_crd').readOnly = true;
        } else {
            document.getElementById('radd1').readOnly = false;
            document.getElementById('radd1').readOnly = false;
            document.getElementById('rcity').readOnly = false;
            document.getElementById('rzipcode').readOnly = false;
            document.getElementById('rsave_crd').readOnly = false;
            var e = document.getElementById("rstate");
            var state = e.options[e.selectedIndex].value;
            $('#rstate option[value=' + state + ']').prop("selected", false);
            document.getElementById("radd1").value = '';
            document.getElementById("radd2").value = '';
            document.getElementById("rcity").value = '';
            document.getElementById("rzipcode").value = '';
            document.getElementById("rsave_crd").value = '';
            document.getElementById("rstate").value = '';

//            $('#rstate option[value=' + state + ']').attr('selected','selected');
        }
    }






    function validate(evt) {

        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '1482390655400902', //old local
            //appId: '677874605651237', //
            cookie: true, // enable cookies to allow the server to access the session
            status: true, // check login status
            xfbml: true, // parse XFBML
            oauth: true, //enable Oauth
            version: 'v2.3'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    function FbLogin() {

        FB.login(function (response) {
            //console.log(response);
            if (response.authResponse) {
                var access_token = response.authResponse.accessToken;
                var userID = response.authResponse.userID;
                FB.api('/me', {fields: 'id,name,email'}, function (response) {

                    var fullname = response.name;
                    var facebook_id = response.id;
                    var gender = response.gender;
                    var email_address = response.email;
                    var first_name = response.first_name;
                    var last_name = response.last_name;
                    if (facebook_id != '') {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() . "member_1/fb_sign"; ?>",
                            data: {facebook_id: facebook_id, fullname: fullname, gender: gender, email_address: email_address, first_name: first_name, last_name: last_name},
                            success: function (data) {
                                if (data.indexOf("true") >= 0) {
                                    cus_data();
                                    $("#fname_cnfirm").val(first_name);
                                    $("#lname_cnfirm").val(last_name);
                                    $("#fb_id").val(facebook_id);
                                    $("#fullname").val(fullname);
                                    $("#gender").val(gender);
                                    $("#email_cnfirm").val(email_address);
                                    $("#psd_cnfirm").val('');
                                    $("#home_tes1").attr("id", "home_tes");
                                    $("#profile_tes1").attr("id", "profile_tes");
                                    $("div#home").hide();
                                    $("div#profile").show();
                                    $("a#home_href").attr("href", "#home");
                                    $('#home_tes').removeClass('active');
                                    $('#profile_tes').addClass('active');

                                } else {
                                    var msg = "<p style='color:green;text-align:center'>This user is already registered. <br/> Please try with another account </p>";
                                    $(".modal-body").empty();
                                    $(".modal-body").html(msg);
                                    $('#myModal').modal('show');
                                }
                            }
                        });
                    } else {
                        return false;
                    }


                });
                //alert(response);
                //parent.location ='<?php //echo base_url();                                                                                                                                                                                                                  ?>fbci/fblogin'; //redirect uri after closing the facebook popup
            }
        }, {scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook

    }




    function test() {
        alert("test");
        $('input[name="locationthemes"]:checked').each(function () {
            console.log(this.value);
            alert(this.value);
        });
    }
</script>


