   
<script>

    function submitForm() {
        $("#frmPack").submit();
    }



    function cancel()
    {

        window.location.href = "<?php echo base_url() ?>admin/home";
    }
</script>
<style>
    .select1{
        border: 1px solid  #e4e4e4;
        color: #757575;
        font-size: 12px;
        line-height: 20px;
        font-weight: 400;
        padding: 15px;
        cursor: pointer;
        display: block;
        width: 100%

    }

</style>
<?php
$this->load->library('session');
?>
<?php
//echo '<pre>';
//print_r($profile);
//exit;
?>

<!-- ===== Section Sign In ===== -->
<section class="preferences">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-10 col-center">
                <div class="preferences-main">
                    <h1>Profile Details</h1>

                    <!-- Preferences Form -->
                    <div class="preferences-form">
                        <?php if ($this->session->flashdata('error_message') != '') { ?>
                            <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
                        <?php } else { ?>
                            <div class="alert alert-danger" role="alert" style="display:none;"></div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success_message') != '') { ?>
                            <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
                        <?php } else { ?>
                            <div class="alert alert-success" role="alert" style="display:none;"></div>
                        <?php } ?>
                        <h3>Delivery Details</h3>
                        <form method="post" name="formlist"  id="formlist" action=""  onsubmit="">
                        <input type="hidden" value="<?php echo $profile['restaurant_id']; ?>" name="restaurant_id"  />


                            <div class="input-group">
                                <!--<input type="hidden" placeholder="<?php echo $pref->title; ?>" class="form-control" name="title[]" id="<?php echo $pref->title; ?>"  value="<?php echo $pref->id; ?>">-->
                                <!--<input type="text"  placeholder="" name="<?php echo $pref->id; ?>" id="<?php echo $pref->field; ?>"  value="<?php echo$pref->value; ?>">-->
                                <select class="select1" name="delivery">
                                    <option value="no">Delivery</option>
                                    <option <?php
                                    if ($profile['delivery'] == "yes") {
                                        echo 'selected';
                                    }
                                    ?> value="yes">Yes</option>
                                    <option <?php
                                    if ($profile['delivery'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                </select>
                                <div class="input-group-addon">Delivery</div>
                            </div>
                            <div class="input-group">
                                <input type="text"  placeholder="Delivery Fee" name="delivery_fee" id=""  value="<?php echo $profile['delivery_fee']; ?>">
                                <div class="input-group-addon">Delivery Fee</div>
                            </div>

                            <div class="input-group">
                                <input type="text"  placeholder="Delivery radius" name="areas_delivery" id=""  value="<?php echo $profile['areas_delivery']; ?>">
                                <div class="input-group-addon">Delivery radius</div>
                            </div>

                            <div class="input-group">
                                <input type="text"  placeholder="Minimum order" name="minimum_order" id=""  value="<?php echo $profile['minimum_order'] ?>">
                                <div class="input-group-addon">Minimum order</div>
                            </div>
                            <h3>Payment Details</h3>
                            <?php
                            if($profile['AccountNumber']==3){
                                $profile['AccountNumber']='';
                            }
                            if($profile['RoutingNumber']==3){
                                $profile['RoutingNumber']='';
                            }
                            if($profile['bank_name']==3){
                                $profile['bank_name']='';
                            }
                            ?>
                            <div class="input-group">
                                <input type="text"  placeholder="Account Number" name="AccountNumber" id=""  value="<?php echo $profile['AccountNumber']; ?>">
                                <div class="input-group-addon">Account Number</div>
                            </div>
                            <!--                            <h3>Payment Details</h3>-->
                            <div class="input-group">
                                <input type="text"  placeholder="Bank Name" name="bank_name" id=""  value="<?php echo $profile['bank_name']; ?>">
                                <div class="input-group-addon">Bank Name</div>
                            </div>
                            <div class="input-group">
                                <input type="text"  placeholder="Routing Number" name="RoutingNumber" id=""  value="<?php echo $profile['RoutingNumber']; ?>">
                                <div class="input-group-addon">Routing Number</div>
                            </div>

                            <h3> Feature Details </h3>

                            <div class="input-group">
                                <input type="text"  placeholder="Routing Number" name="RoutingNumber" id=""  value="<?php echo $profile['RoutingNumber']; ?>">
                                <div class="input-group-addon">Routing Number</div>
                            </div>
                            <div class="input-group">
                                <select class="select1"  name="music">
                                    <option value="no">Music</option>
                                    <option <?php
                                    if ($profile['music'] == "yes") {
                                        echo 'selected';
                                    }
                                    ?> value="yes">Yes</option>
                                    <option <?php
                                    if ($profile['music'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                </select>
                                <div class="input-group-addon">Music</div>
                            </div>
                            <div class="input-group">
                                <select class="select1"  name="wheelchair_accessible">
                                    <option value="no">Wheelchair Accessible</option>
                                    <option <?php
                                    if ($profile['wheelchair_accessible'] == "yes") {
                                        echo 'selected';
                                    }
                                    ?> value="yes">Yes</option>
                                    <option <?php
                                    if ($profile['wheelchair_accessible'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                </select>
                                <div class="input-group-addon">Wheelchair Accessible</div>
                            </div>
                            <div class="input-group">
                                <select class="select1"  name="waiter_service">
                                    <option value="no">Waiter Service</option>
                                    <option <?php
                                    if ($profile['waiter_service'] == "yes") {
                                        echo 'selected';
                                    }
                                    ?> value="yes">Yes</option>
                                    <option <?php
                                    if ($profile['waiter_service'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                </select>
                                <div class="input-group-addon">Waiter Service</div>
                            </div>
                            <div class="input-group">
                                <select class="select1"  name="outdoor_seating">
                                    <option value="no">Outdoor Seating</option>
                                    <option <?php
                                    if ($profile['outdoor_seating'] == "yes") {
                                        echo 'selected';
                                    }
                                    ?> value="yes">Yes</option>
                                    <option <?php
                                    if ($profile['outdoor_seating'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                </select>
                                <div class="input-group-addon">Outdoor Seating</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <input type="submit" value="Submit">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button onclick="cancel();">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Preferences Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ===== End Section Sign In ===== -->