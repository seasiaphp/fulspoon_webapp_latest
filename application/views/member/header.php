<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="" />
        <title>Fulspoon | Member's Dashboard</title>
         <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet">
        <!--<link rel="stylesheet" href="css/meanmenu.css" media="all" />-->
        
        <!-- Awesome Fonts -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/css/font-awesome.min.css" rel="stylesheet">
        
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/zebra_dialog.css" type="text/css">	
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.min.js"></script>
        <script src="<?php echo $template_url; ?>/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets');?>/js/zebra_dialog.js"></script>
        <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-73692716-1', 'auto');
ga('send', 'pageview');


</script>

    </head>
    
    <body>
    <div id="preloader" class="loader_home"></div>
		<?php //echo base_url().$route.'/orders';?>
        <!-- ===== Header ===== -->
        <!-- Responsive Menus -->
        <section class="responsive-menus">
            <div class="menu-section">
                <!-- Toggle Buttons -->
                <div class="menu-toggle">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <!-- End Toggle Buttons -->
                
                <!-- Toggle Menus -->
                <nav>
                    <ul role="navigation" class="hidden">
                    	<a href="<?php echo base_url().'member/home'?>">
                		 <img alt="" src="<?= base_url() ?>images/replimatic_logo_white.png">
                        </a>
                		
                            
  
                        <p>&copy; Fulspoon All Rights Reserved</p>
                    </ul>
                </nav>
                <!-- End Toggle Menus -->
            </div>
            
            
            <!-- Small DropDown -->
            <div class="res-small-dropDown">
            	<a href="<?php echo site_url('member/logout'); ?>"><i class="demo-icon icon-off">&#xe808;</i></a>
            </div>
            <!-- End Small DropDown -->
        </section>
        <!-- End Responsive Menus -->
        
        <header>
            <div class="container-fluid">
                <!-- Logo Menu -->
                <div class="logo-menu">
                    <div class="row">
                        <!-- Logo -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 logo-full">
                            <div class="logo">
                                <a href="<?php echo base_url().'member/home'?>">
                                     <img alt="" src="<?= base_url() ?>images/replimatic_logo_white.png"></a>
                            </div>
                        </div>
                        <!-- End Logo -->
                        
                        <!-- Menu -->
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
                            <div class="menu">
                                
                            </div>
                        </div>
                        <!-- End Menu -->
                    </div>
                </div>
                <!-- End Logo Menu -->
            </div>
        </header>
        <!-- ===== End Header ===== -->
       
<script>
if(typeof(EventSource) !== "undefined") {
   	var source = new EventSource("<?php echo base_url().$route."/orders/loadNewOrder"?>");
    source.onmessage = function(event) {
		var data=event.data;
		var datas = data.split("###");
		var count  = datas[0];
		var content= datas[1];
        //document.getElementById("tablebody").innerHTML = content;
		var oldcount = $('#hidecount').val();
		
		
		$('#tablebody').html(content);
		$('#tablebody_tab').html(content);
		$('#tablebody_mob').html(content);
	
		$('#tablebody_tab .hide-desk').removeClass('hide');
		$('#tablebody_mob .hide-desk').removeClass('hide');
		
		$('#tablebody .hide-desk').hide();
		$('#tablebody_tab .hide-tab').hide();
		$('#tablebody_mob .hide-mob').hide();
	
		$('.facebook').html(	$('#fb_svg').html());
		$('.web').html(	$('#web_svg').html());
		$('.app').html(	$('#app_svg').html());
		
		$('#countnew').html(count);
		$('.badgenew').html(count);
		$('#hidecount').val(count);
		if(count!=oldcount && oldcount!='0'){
			setTimeout(function(){
		 		$('#toast').show();
				$('#toast').fadeOut(5000);
			}, 100);
		}else{
			
			
		}
		//document.getElementById("countnew").innerHTML = count;
    };
} else {
    document.getElementById("tablebody").innerHTML = "Sorry, your browser does not support server-sent events...";
}

	
</script>
<style>
 #toast{
  background-position: 15px center;
    background-repeat: no-repeat;
    border-radius: 3px;
    box-shadow: 0 0 12px #999999;
    margin: 0 0 6px;
    opacity: 0.8;
    overflow: hidden;
    padding: 15px 15px 15px 50px;
    pointer-events: auto;
    position: Fixed;
    width: 300px;
	bottom:0;
	z-index:999999999;
    }
</style>
<div id="toast" style="display:none;" >A New Order Placed</div>
<input type="hidden" name="hidecount" value="0" id="hidecount">
