<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon Owners | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Fulspoon Owners | Home">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />
        <!-- FORM CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/form.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>fontstyle.css" media="screen">
        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top green-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="<?= base_url() ?>owners/"><img src="<?= base_url() ?>images/replimatic_logo_white.png" alt=""/></a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Stat Search -->
                            <!-- Stat Search -->
                            <div class="buttons-side">
                                <!--<a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>-->
                                <a class="btn btn-default btnlogin" href="<?= base_url() ?>owners/signup" role="button">SIGN UP</a>
                            </div>
                            <!-- End Search -->
                            <!-- End Search -->
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li> <a style="color:#fff;">Call Us: 877-638-6522</a></li>             
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li><a>Call Us: 877-638-6522</a></li>
                        <!--<li><a href="index-01.html">LOGIN</a></li>-->
                        <li><a href="<?= base_url() ?>owners/signup">SIGN UP</a></li></ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->


            <!-- Start HomePage Slider -->

            <section id="home">
                <!-- Carousel -->
                <div id="main-slide" class="carousel slide" data-ride="carousel">
                    <!-- Carousel inner -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="img-responsive" src="<?= base_url() ?>images/slider/blured.png" alt="slider">
                            <div class="slider-content">
                                <div class="col-md-12 text-center"> 
                                    <p class="animated2" style="font-size: 42px; padding-bottom: 40px; font-weight: 300;">Join the Fulspoon Network</p>
                                    <p class="animated4"><a href="<?= base_url() ?>owners/signup" class="slider btn btn-system">JOIN NOW</a></p>
                                </div>
                            </div>
                        </div>

                        <!--/ Carousel item end -->
                    </div>
                    <!-- Carousel inner end-->

                </div>

                <!-- /carousel -->
            </section>
            <!-- End HomePage Slider -->


            <div class="section">
                <!-- Start Services Icons -->
                <div class="container">
                    <div class="row">
                        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                            <h1>What We’ll Do For You</h1>
                        </div>


                        <!-- Start Service Icon 1 -->
                        <div class="col-md-4 col-sm-4 service-box service-center">

                            <div class="service-content">
                                <h4>New Risk-Free and Costless Revenue Stream</h4>
                                <div class="service-icon">
                                    <i class="fa icon-medium-effect icon-effect-2 gray-icon" style="line-height:64px;">
                                        <span class="icon-riskfree"></span> 
                                    </i>

                                </div>
                                <p>Earn subscription revenue from subscribers every month. </p>
                            </div>
                        </div>
                        <!-- End Service Icon 1 -->

                        <!-- Start Service Icon 2 -->
                        <div class="col-md-4 col-sm-4 service-box service-center">
                            <div class="service-content">
                                <h4>Captive Pool of Devoted Customers</h4>
                                <div class="service-icon">
                                    <i class="fa icon-medium-effect icon-effect-2 gray-icon" style="line-height:64px;">
                                        <span class="icon-captivepool"></span> 
                                    </i>
                                </div>
                                <p>Subscribers choose your establishment over other options, more often</p>
                            </div>
                        </div>
                        <!-- End Service Icon 2 -->

                        <!-- Start Service Icon 3 -->
                        <div class="col-md-4 col-sm-4 service-box service-center">
                            <div class="service-content">
                                <h4>(Many) More Orders </h4>
                                <div class="service-icon">
                                    <i class="fa icon-medium-effect icon-effect-2 gray-icon" style="line-height:64px;">
                                        <span class="icon-moreorder"></span> 
                                    </i>
                                </div>
                                <p> Subscribers order more from you, resulting in considerably larger volume of orders from same customer base</p>
                            </div>
                        </div>
                        <!-- End Service Icon 3 -->
                    </div>
                </div>
                <!-- End Services Icons -->
            </div>
            <!-- Start How it works -->
            <div class="section calculator-wrap">
                <div class="container">
                    <div class="row">
                        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                            <h1 style="color:#fff;">How it works</h1>
                        </div>
                        <div class="col-md-6" style="margin-top: -7.0%">
                            <div class="howin"><h5><div class="howin_count">1</div>Signup with us</h5>Reach out to us at network@fulspoon.com to get the process started</div>
                            <div class="howin"><h5><div class="howin_count">2</div>Convert your customers into subscribers</h5>Promote fulspoon subscriptions to your customers and convert them into paying subscribers. Subscribers sign up on fulspoon.com and download the fulspoon app. We will provide the marketing materials and full support to maximize conversion of customers to subscribers</div>
                            <div class="howin"><h5><div class="howin_count">3</div>Earn costless/riskless subscription revenue from subscribers</h5>Every subscription results in fixed, costless revenue to you. Additionally, you will receive more orders from subscribers, resulting in more sales to same customer base</div>
                        </div>
                        <div class="col-md-6"  style="margin-top: -7.0%">
                            <div class="howin"><h5><div class="howin_count">4</div>Offer subscription concession to subscribers</h5> Provide required concession on every order to subscribers </div>
                            <div class="howin"><h5><div class="howin_count">5</div>Get lot more orders from subscribers </h5>Increased volume of orders from subscribers results in dramatically more sales with no new investments or marketing</div>
                            <div class="howin"><h5><div class="howin_count">6</div>Make more revenue from 2 sources </h5>You earn additional revenue from every subscriber you add: you earn both subscription fee revenue and incremental order revenenue</div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End How it works -->

            <!-- Start Calculator -->
            <div class="section calculator-wrap" style=" padding-top: 2px;">
                <div class="container">
                    <div class="row">
                        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                            <h1 style="color:#fff;" class="tblhead" >Calculators</h1>
                        </div>
                        <div class="table-calc">
                            <h2 class="tblhead labelcalculator" style="color: #5c5c5c; background: #fff">Incremental Revenue</h2>
                            <div class="rowgreen1" style="padding:20px 0px;">
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Number of Customers per Month </div>
                                    <div style="max-width:150px;">
                                        <input type="text"  class="form-control inputsignup" id="numPatrons" onChange="patron_val()" placeholder="Type Value" style=" height:39px; background-color: none">
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <input type="hidden" id="month_sub" value="0" >
                                    <input type="hidden" id="month_sub_total" value="0" >
                                    <input type="hidden" id="revenue_tot_final" value="0" >
                                    <input type="hidden" id="admin_calc_val" value="<?= $admin_val['month_subscriber'] ?>">
                                    <input type="hidden" id="admin_avg_order" value="<?= $admin_val['average_order'] ?>">
                                    <div class="labeldiv lbl_lid">Proportion that become Fulspoon Subscribers </div>
                                    <div>
                                        <div class="styled"  style="max-width:150px;">
                                            <select onChange="patron_val()" id="prop_rep_subc" style="color:#262626;">
                                                <?php
                                                foreach ($rep_sub as $reps) {
                                                    ?>
                                                    <option value="<?= $reps['proportion'] ?>"><?= $reps['proportion_with_sign'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div></div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Monthly Subscribers </div>
                                    <div class="calc-value lbl_lid" id="calc_mnth_subscriber">$0.00</div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Subscription revenue to you </div>
                                    <div class="calc-value lbl_lid" id="calc_mnth_subscriber_tot">$0.00</div></td>            
                                </div>

                                <div class="clearfix"></div>
                            </div>

                            <!------------------------------------------->

                            <div class="rowgreen2" style="padding:20px 0px;">
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Expected incremental orders per subscriber</div>
                                    <div>
                                        <div class="styled"  style="max-width:150px;">
                                            <select id="exptd" onChange="check_exp()" style="color:#262626;">
                                                <option value="0">Select Here</option>
                                                <?php foreach ($get_exptd_inc_mod as $expctd) { ?>
                                                    <option><?= $expctd['expected_incremental_orders'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Average order value</div><div>
                                        <div class="styled"  style="max-width:150px;">
                                            <select onChange="check_exp()" id="avg_ord_val1" style="color:#262626;">
                                                <option value="0">Select Here</option>
                                                <?php foreach ($avg_order as $avg) { ?>
                                                    <option value="<?= $avg['average_order'] ?>"><?= $avg['average_order_with_sign'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div></div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Average order Value with concession </div>
                                    <div class="calc-value lbl_lid" id="avg_ord_conc">$0.00</div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="labeldiv lbl_lid">Order Revenue/monthly</div>
                                    <div id="avg_ord_conc_final" class="calc-value lbl_lid">$0.00</div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="rowgreen1" style="padding:20px 0px;">
                                <div class="col-lg-3 col-sm-4 col-md-3">
                                    <div class="labeldiv-total lbl_lid">Total Incremental Revenue from Fulspoon/monthly</div>
                                    <div class="calc-total lbl_lid" id="total_calc">$0.00</div>
                                </div>

                                <div class="col-lg-3 col-sm-4 col-md-3 col-lg-offset-6 col-md-offset-6 col-sm-offset-4">
                                    <div class="labeldiv-total lbl_lid"> Total Incremental Revenue from Fulspoon annual</div>
                                    <div class="calc-total lbl_lid" id="total_calc_anual">$0.00</div>
                                </div>
                                <div class="clearfix"></div>
                            </div>



    <!--<table class="table table-calc">
        <thead>
            <tr style="color: #5c5c5c; background: white">           
                <th colspan="4" style="color: #5c5c5c" class="tblhead">Incremental Revenue</th>            
            </tr>
        </thead>
        <tbody>

            <tr class="rowgreen1">
                <th scope="row"><div class="labeldiv">Number of Customers per Month </div>
            <div style="max-width:150px;">
            <input type="text"  class="form-control inputsignup" id="numPatrons" onchange="patron_val()" placeholder="Type Value" style=" height:39px; background-color: none">
            </div>
            </th>
        <td>
            <input type="hidden" id="month_sub" value="0" >
            <input type="hidden" id="month_sub_total" value="0" >
            <input type="hidden" id="revenue_tot_final" value="0" >
            <input type="hidden" id="admin_calc_val" value="<?= $admin_val['month_subscriber'] ?>">
            <input type="hidden" id="admin_avg_order" value="<?= $admin_val['average_order'] ?>">
            <div class="labeldiv">Proportion that become Fulspoon Subscribers </div>
            <div>
                <div class="styled"  style="max-width:150px;">
                    <select onchange="patron_val()" id="prop_rep_subc" style="color:#262626;">
                            <?php
                            foreach ($rep_sub as $reps) {
                                ?>
                                                                                                <option value="<?= $reps['proportion'] ?>"><?= $reps['proportion_with_sign'] ?></option>
                            <?php } ?>
                    </select>
                    </div></div>
        </td>
        <td>
            <div class="labeldiv">Monthly Subscribers </div>
            <div class="calc-value" id="calc_mnth_subscriber">$0.00</div>
        </td>
        <td>
            <div class="labeldiv">Subscription revenue to you </div>
            <div class="calc-value" id="calc_mnth_subscriber_tot">$0.00</div></td>            
        </tr>
        
        
        <tr class="rowgreen2">
            <th scope="row">
        <div class="labeldiv">Expected incremental orders per subscriber</div>
        <div>
            <div class="styled"  style="max-width:150px;">
                <select id="exptd" onchange="check_exp()" style="color:#262626;">
                    <option value="0">Select Here</option>
                            <?php foreach ($get_exptd_inc_mod as $expctd) { ?>
                                                                                            <option><?= $expctd['expected_incremental_orders'] ?></option>
                            <?php } ?>
                </select>
            </div>
        </div>
        </th>
        <td>
            <div class="labeldiv">Average order value</div><div>
                <div class="styled"  style="max-width:150px;">
                    <select onchange="check_exp()" id="avg_ord_val1" style="color:#262626;">
                        <option value="0">Select Here</option>
                            <?php foreach ($avg_order as $avg) { ?>
                                                                                                <option value="<?= $avg['average_order'] ?>"><?= $avg['average_order_with_sign'] ?></option>
                            <?php } ?>
                    </select>
                </div></div>
        </td>
        <td>
            <div class="labeldiv">Average order Value with concession </div>
            <div class="calc-value" id="avg_ord_conc">$0.00</div>
        </td>
        <td>
            <div class="labeldiv">Incremental Order Revenue</div>
            <div id="avg_ord_conc_final" class="calc-value">$0.00</div>
        </td>    
        </tr>
        <tr>
            <th scope="row" colspan="3"></th>         
            <td>          
                <div class="labeldiv-total">Total Incremental Revenue from Fulspoon</div>
                <div class="calc-total" id="total_calc">$0.00</div>
            </td>               
        </tr>       
        </tbody>
    </table>-->
                        </div>




                        <div class="table-responsive">
                            <div class="">
                                <h1 class="tblhead labelcalculator" style="background-color:#fff; padding:10px 5px;">
                                    Transactional Comparison
                                </h1>
                            </div>
                            <div id="no-more-tables">
                                <table class="col-md-12 comparisontable table-striped table-condensed cf">
                                    <thead>
                                        <tr>
                                            <td></td>
                                            <td class="numeric">Fulspoon</td>
                                            <td class="numeric">Order Delivery Services</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="labelcust" data-title="">Order Value</td>
                                            <td data-title="Fulspoon" class="numeric">$<?= $transactional_calc['lt_order_valul'] ?> </td>
                                            <td data-title="Order Delivery Services" class="numeric">$<?= $transactional_calc['rt_order_value'] ?></td>
                                        </tr>
                                        <tr>
                                            <td id="labelcust" data-title="">Commission</td>
                                            <td data-title="Fulspoon" class="numeric"><?= $transactional_calc['lt_commission'] ?>%</td>
                                            <td data-title="Order Delivery Services" class="numeric"><?= $transactional_calc['rt_commission'] ?>%</td>
                                        </tr>

                                        <tr>
                                            <td id="labelcust" data-title="">Processing Fees</td>
                                            <td data-title="Fulspoon" class="numeric"><?= $transactional_calc['lt_processing_Fees'] ?>%</td>
                                            <td data-title="Order Delivery Services" class="numeric"><?= $transactional_calc['rt_processing_Fees'] ?>%</td>
                                        </tr>
                                        <tr>
                                            <td id="labelcust" data-title="">Concession</td>
                                            <td data-title="Fulspoon" class="numeric"><?= $transactional_calc['lt_concession'] ?>%</td>
                                            <td data-title="Order Delivery Services" class="numeric"><?= $transactional_calc['rt_concession'] ?>%</td>
                                        </tr>
                                        <tr>
                                            <td id="labelcust" data-title="">Net</td>
                                            <td data-title="Fulspoon" class="numeric">$<?php
                                                $vall4 = ($transactional_calc['lt_order_valul'] - ($transactional_calc['lt_order_valul'] * ($transactional_calc['lt_concession'] / 100)));
                                                $val1 = ( $vall4 - ($vall4 * (($transactional_calc['lt_commission'] + $transactional_calc['lt_processing_Fees']) / 100)));

                                                print sprintf('%0.2f', $val1);
                                                ?></td>
                                            <td data-title="Order Delivery Services" class="numeric">$<?php
                                                $val2 = ($transactional_calc['rt_order_value'] - ($transactional_calc['rt_order_value'] * ($transactional_calc['rt_concession'] / 100)));
                                                $val3 = ($val2 - ($val2 * (($transactional_calc['rt_processing_Fees'] + $transactional_calc['rt_commission']) / 100)));
//                                                $valr4 = ($val3 - ($val3 * ($transactional_calc['rt_concession'] / 100)));
                                                print sprintf('%0.2f', $val3);
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <td id="labelcust" data-title="">Subscription Revenue</td>
                                            <td data-title="Fulspoon" class="numeric">$<?= $transactional_calc['lt_subscription_Revenue'] ?></td>
                                            <td data-title="Order Delivery Services" class="numeric">$<?= $transactional_calc['rt_subscription_Revenue'] ?></td>
                                        </tr>
                                        <tr style="font-size:18px; font-weight:500;">
                                            <td data-title="" class="labelcust" >Total</td>
                                            <td data-title="Fulspoon" class="numeric">$<?php
                                                $val4 = $val1 + $transactional_calc['lt_subscription_Revenue'];
                                                print sprintf('%0.2f', $val4);
                                                ?>
                                            </td>
                                            <td data-title="Order Delivery Services" class="numeric">$<?php
                                                $val5 = $val3 + $transactional_calc['rt_subscription_Revenue'];
                                                print sprintf('%0.2f', $val5);
                                                ?></td>
                                        </tr>


                                    </tbody>
                                </table></div>
                <!--                            <table class="table table-striped comparisontable">
                
                                                <tbody>
                                                    <tr>
                                                        <td style="min-width:300px;"></td>															
                                                        <td style="min-width:250px;">Fulspoon</td>
                                                        <td>Order Delivery Services</td>            
                                                    </tr>
                                                    <tr>
                                                        <td>Order Value</td>															
                                                        <td>$<?= $transactional_calc['lt_order_valul'] ?> </td>
                                                        <td>$<?= $transactional_calc['rt_order_value'] ?> </td>            
                                                    </tr>
                                                    <tr>
                                                        <td>Commission</td>															
                                                        <td><?= $transactional_calc['lt_commission'] ?>%</td>
                                                        <td><?= $transactional_calc['rt_commission'] ?>%</td>            
                                                    </tr>
                                                    <tr>
                                                        <td>Processing Fees</td>															
                                                        <td><?= $transactional_calc['lt_processing_Fees'] ?>%</td>
                                                        <td><?= $transactional_calc['rt_processing_Fees'] ?>%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Concession</td>															
                                                        <td><?= $transactional_calc['lt_concession'] ?>%</td>
                                                        <td><?= $transactional_calc['rt_concession'] ?></td>            
                                                    </tr>
                                                    <tr>
                                                        <td>Net</td>															
                                                        <td>$<?php
//                            $val1 = ($transactional_calc['lt_order_valul'] - ($transactional_calc['lt_order_valul'] * ($transactional_calc['lt_concession'] / 100)));
//                                            echo $val1;
                            print sprintf('%0.2f', $val1);
                            ?></td>
                                                        <td>$<?php
                            $val2 = ($transactional_calc['rt_order_value'] - ($transactional_calc['rt_order_value'] * ($transactional_calc['rt_commission'] / 100)));
                            $val3 = ($val2 - ($val2 * ($transactional_calc['rt_processing_Fees'] / 100)));
                            print sprintf('%0.2f', $val3);
                            ?></td>            
                                                    </tr>
                                                    <tr>
                                                        <td>Subscription Revenue</td>															
                                                        <td>$<?= $transactional_calc['lt_subscription_Revenue'] ?></td>
                                                        <td>$<?= $transactional_calc['rt_subscription_Revenue'] ?></td>            
                                                    </tr>
                                                    <tr style="font-size:20px; font-weight:500;">
                                                        <td>Total</td>															
                                                        <td>$<?php
                            $val4 = $val1 + $transactional_calc['lt_subscription_Revenue'];
                            print sprintf('%0.2f', $val4);
                            ?></td>
                                                        <td>$<?php
                            $val5 = $val3 + $transactional_calc['rt_subscription_Revenue'];
                            print sprintf('%0.2f', $val5);
                            ?></td>            
                                                    </tr>
                                                </tbody>
                                            </table>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Calculator -->
            <!-- Start Fulspoon Network -->
            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                            <h1>Benefits of Joining Fulspoon Network</h1>
                        </div>
                        <div class="col-md-6"><img src="<?= base_url() ?>images/network.png" alt=""/></div>
                        <div class="col-md-6">
                            <div class="network">New costless, riskless revenue stream</div>
                            <div class="network">Absolutely NO costs to you -- no fees, no commissions, nothing </div>
                            <div class="network">Pool of Devoted Customers</div>
                            <div class="network">Considerable incremental order flow from loyal customers -- much more revenues</div>
                            <div class="network">Encourage your customers to divert more of their monthly order flow to you</div>
                            <div class="network">No risk -- in the worst case you get no subscribers. Cost to you? Zero. You don't offer any concession to anyone unless they subscribe and place new orders. <div class="clearfix"></div></div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End Fulspoon Network -->
            <!--  -->

            <!--            <div class="section graysection">
                            <div class="container">
                                <div class="row">
                                    <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                                        <h1>Take It From Them</h1>
                                    </div>
                                    <div class="col-md-12">
            
                                         Start Testimonials Carousel 
                                        <div class="custom-carousel show-one-slide touch-carousel" data-appeared-items="2">
                                             Testimonial 1 
                                            <div class="classic-testimonials item col-md-12">
                                                <div class="owener-testimonial">
                                                    <div class="testimonial-content">
                                                        <p>Lorem ipsum dolor sit amet, ut cum aliquam graecis percipit, qui ea tation nostrud. Ad atqui laboramus eos. Suas inimicus in sed, in tale aeterno eloquentiam eos, eu sint regione aliquam sea. Pri no diam etiam debet, aliquip lucilius </p>
                                                        <div class="authorinfo"> Oliver Auerbach <br><span>- Founder & CEO</span></div>
                                                    </div>
                                                    <div class="pointer"></div>
                                                </div>
                                                <div class="testimonial-author"> <span><img src="<?= base_url() ?>images/test_thumb.png" alt="" class="img-circle"/> </span></div>
                                            </div>
                                             Testimonial 2 
                                            <div class="classic-testimonials item col-md-12">
                                                <div class="owener-testimonial">
                                                    <div class="testimonial-content">
                                                        <p>Lorem ipsum dolor sit amet, ut cum aliquam graecis percipit, qui ea tation nostrud. Ad atqui laboramus eos. Suas inimicus in sed, in tale aeterno eloquentiam eos, eu sint regione aliquam sea. Pri no diam etiam debet, aliquip lucilius </p>
                                                        <div class="authorinfo"> Oliver Auerbach <br><span>- Founder & CEO</span></div>
                                                    </div>
                                                    <div class="pointer"></div>
                                                </div>
                                                <div class="testimonial-author"> <span><img src="<?= base_url() ?>images/test_thumb.png" alt="" class="img-circle"/> </span></div>
                                            </div>
                                             Testimonial 3 
                                            <div class="classic-testimonials item col-md-12">
                                                <div class="owener-testimonial">
                                                    <div class="testimonial-content">
                                                        <p>Lorem ipsum dolor sit amet, ut cum aliquam graecis percipit, qui ea tation nostrud. Ad atqui laboramus eos. Suas inimicus in sed, in tale aeterno eloquentiam eos, eu sint regione aliquam sea. Pri no diam etiam debet, aliquip lucilius </p>
                                                        <div class="authorinfo"> Oliver Auerbach <br><span>- Founder & CEO</span></div>
                                                    </div>
                                                    <div class="pointer"></div>
                                                </div>
                                                <div class="testimonial-author"> <span><img src="<?= base_url() ?>images/test_thumb.png" alt="" class="img-circle"/> </span></div>
                                            </div>
                                             Testimonial 4 
                                            <div class="classic-testimonials item col-md-12">
                                                <div class="owener-testimonial">
                                                    <div class="testimonial-content">
                                                        <p>Lorem ipsum dolor sit amet, ut cum aliquam graecis percipit, qui ea tation nostrud. Ad atqui laboramus eos. Suas inimicus in sed, in tale aeterno eloquentiam eos, eu sint regione aliquam sea. Pri no diam etiam debet, aliquip lucilius </p>
                                                        <div class="authorinfo"> Oliver Auerbach<br> <span>- Founder & CEO</span></div>
                                                    </div>
                                                    <div class="pointer"></div>
                                                </div>
                                                <div class="testimonial-author"> <span><img src="<?= base_url() ?>images/test_thumb.png" alt="" class="img-circle"/> </span></div>
                                            </div>
            
                                        </div>
                                         End Testimonials Carousel 
            
                                    </div>
            
                                </div>
                            </div>
                        </div>-->

            <!-- Start Who We Are -->
            <div class="section darkgray" style="background:#3A3A3A">
                <div class="container">
                    <div class="row">
                        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                            <h1 style="color:#fff; ">Who We Are</h1>
                        </div>
                        <div class="col-md-8 col-md-offset-2" style="color:#fff;"> We are ex-restaurant owners and technology entrepreneurs ourselves. Having run restaurants , we understand the constraints, struggles and joys of ownership. Having built technology products, we know how to deliver a flawless user experience built on the most exciting new technologies available
                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div>
            </div>
            <!-- End Who We Are -->

            <!--  -->
            <!-- Start Footer -->

            <style>
                .placeholder::-webkit-input-placeholder { color: red !important;}
                .placeholder:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                    color:    red !important;
                    opacity:  1;
                }
                .placeholder::-moz-placeholder { /* Mozilla Firefox 19+ */
                    color:    red !important;
                    opacity:  1;
                }
                .placeholder:-ms-input-placeholder { /* Internet Explorer 10+ */
                    color:   red !important;
                }
            </style>
            <script>

                function check_exp() {
                    var value = $("#exptd").val();
                    var month_sub = $("#month_sub").val();
                    var month_sub_tot = $("#month_sub_total").val();
                    var admin_val = $("#admin_avg_order").val();
                    var avg_ord_val1 = $("#avg_ord_val1").val();
                    var result = (avg_ord_val1 * (admin_val)).toFixed(2);
                    var final_result = (result * month_sub * value).toFixed(2);
                    $("#revenue_tot_final").val(final_result);
                    $("#avg_ord_conc").empty();
                    $("#avg_ord_conc").html("$" + result);
                    $("#avg_ord_conc_final").empty();
                    $("#avg_ord_conc_final").html("$" + final_result);
                    $("#total_calc").empty();
                    var subtot_final = (+month_sub_tot + +final_result).toFixed(2);
                    $("#total_calc").html("$" + (subtot_final));
                    $("#total_calc_anual").html("$" + ((subtot_final) * 12).toFixed(2));

                }

                function patron_val() {
                    var pat = $("#numPatrons").val();

                    var prop_rep_subc = $("#prop_rep_subc").val();
                    if (isNaN(pat)) {
                        $("#numPatrons").addClass('placeholder');
                        $("#numPatrons").val('');
                        $('#numPatrons').attr('placeholder', 'Invalid customers');
                        $("#numPatrons").focus();

                    } else {
                        var result = Math.round(pat * (prop_rep_subc / 100));
                        $("#calc_mnth_subscriber").empty();
                        $("#calc_mnth_subscriber").html(result);
                        var value = $("#exptd").val();
                        var admin_val = $("#admin_avg_order").val();
                        var avg_ord_val1 = $("#avg_ord_val1").val();
                        var result333 = (avg_ord_val1 * (admin_val)).toFixed(2);
                        var final_result = (result333 * result * value).toFixed(2);
                        $("#revenue_tot_final").val(final_result);
                        $("#avg_ord_conc").empty();
                        $("#avg_ord_conc").html("$" + result333);
                        $("#avg_ord_conc_final").empty();
                        $("#avg_ord_conc_final").html("$" + final_result);
                        $("#total_calc").empty();
                        var revenue = $("#revenue_tot_final").val();
                        var admin_calc = $("#admin_calc_val").val();
                        var sub_revenue = "$" + (result * admin_calc).toFixed(2);
                        var sub_rev_final = (result * admin_calc).toFixed(2);
                        $("#month_sub").val(result);
                        $("#month_sub_total").val((result * admin_calc).toFixed(2));
                        $("#calc_mnth_subscriber_tot").empty();
                        $("#calc_mnth_subscriber_tot").html(sub_revenue);
                        var subtot_final = (+sub_rev_final + +revenue).toFixed(2);
                        $("#total_calc").html("$" + (subtot_final));
                        $("#total_calc").html("$" + (subtot_final));
                        $("#total_calc_anual").html("$" + ((subtot_final) * 12).toFixed(2));
                    }
                }
            </script>