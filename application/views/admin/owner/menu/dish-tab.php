
<form name="userMasterForm" id="userMasterForm" method="post" action=""> 
<!-- ===== Start Section Menu ===== -->
<section class="main-sec">
    <div class="container-fluid">
        <!-- Tabs -->
        <div class="menu-tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" >
                    <!--<a href="#tab-category" aria-controls="category" role="tab" data-toggle="tab">Category</a>-->
                    <a aria-expanded="true" aria-controls="category" role="tab" id="category-tab" href="<?php echo base_url(); ?>admin/menu/category/<?= $restaurant_id ?>">CATEGORY</a>		

                </li>

                <li role="presentation" class="active">
                    <!--<a href="#tab-dish" aria-controls="dish" role="tab" data-toggle="tab">Dish</a>-->
                    <a aria-controls="dish" class="dish-tab" id="dish-tab" role="tab" href="<?php echo base_url(); ?>admin/menu/dish/<?= $restaurant_id ?>">DISH</a>
                </li>
            </ul>
            <!-- End Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Category -->

                <!-- End Category -->

                <!-- Dish -->
                <div role="tabpanel" class="tab-pane fade in active" id="tab-dish">
                    <!-- Add New Dish Button -->
                    <div class="add-new-box">
                        <a href="<?php echo base_url(); ?>admin/menu/add_dish/<?php echo $this->uri->segment(4); ?>/<?php echo $cat_id; ?>" class="btn btn-info pull-right" style="background: #6FBE44 ; padding-bottom:10px; color:#FFF!important;">ADD NEW</a>
                    </div>
                    <!-- End Add New Dish Button -->

                    <table class="table table-hover dish-table for-view-higher-481 search-cat">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>

                                    <select id="cat" class="search-slct" onchange="change_dish()" name="cat">
                                    
                                        <?php 
                                        $i=0;
                                        while ($i < count($categorylist)) { ?>
                                            <option value="<?php echo $categorylist[$i]['id']; ?>"   <?php if ($cat_id == $categorylist[$i]['id']) { ?> selected="selected"<?php  } ?>><?php echo $categorylist[$i]['menu_name']; ?></option>
                                            <?php $i++;
                                        }
										
                                        ?>


                                    </select>
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody >
                            <?php
                            if (count($dishlist) != 0) {
                                foreach ($dishlist as $items) {
                                    ?>
                                    <tr id="row_<?php echo $items['id']; ?>" data-attr="<?php echo $items['menu']; ?>" data-val="<?php echo $items['id']; ?>" class="dish_tr_<?php echo $items['menu']; ?>" <?php if ($cat_id != $items['menu'] && $cat_id != '') { ?> style="display:none;"<?php } ?>>
                                        <td class="tdlink <?php if ($cat_id == $items['menu']) { ?>my-handle<?php } ?> " href="<?php echo base_url(); ?>admin/menu/add_dish/<?= $this->uri->segment(4) ?>/<?php echo $items['menu']; ?>/<?php echo $items['id']; ?>"><?php echo $items['dish_name']; ?></td>
                                        <td class="tdlink" href="<?php echo base_url(); ?>admin/menu/add_dish/<?= $this->uri->segment(4); ?>/<?php echo $items['menu']; ?>/<?php echo $items['id']; ?>"><?php echo $items['menu_name']; ?></td>
                                        <td>
                                            <label class="switch">
                                                <input type="checkbox" <?php if ($items['status'] == 'Y') { ?> checked="true" <?php } ?> onClick="cat_status(<?php echo $items['id']; ?>, '<?php echo $items['status']; ?>')" class="cat_status_<?php echo $items['id']; ?>" data-val="<?php echo $items['status']; ?>">
                                                <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                            </label>
                                       <!-- <a href="#" data-toggle="modal" data-target="#cat-delete-dish" ><i class="fa fa-trash-o"></i> Delete</a>
                                            -->

                                            <a href="<?php echo base_url(); ?>admin/menu/add_dish/<?= $this->uri->segment(4) ?>/<?php echo $items['menu']; ?>/<?php echo $items['id']; ?>" >
                                                <i class="fa fa-pencil-square-o"></i> Edit
                                            </a>


                                            <a href="javascript:void(0)" class="delItemAjax" data-attr="<?php echo $items['id']; ?>" >
                                                <i class="fa fa-trash-o"></i>DELETE
                                            </a>

                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>




                        </tbody>
                    </table>

                    <!-- For Mobile View -->                            
                    <table class="table table-hover for-view-lower-481 search-cat">
                        <thead>
                            <tr>
                                <th>
                                   <?php /*?> <select class="search-slct">
                                        <option selected disabled>-- Select Category --</option>
                                        <?php while ($j < count($categorylist)) { ?>
                                            <option><?php echo $categorylist[$j]['menu_name']; ?></option>
    <?php $j++;
}
?>


                                    </select><?php */?>
                                     <select id="cat" class="search-slct" onchange="change_dish()" name="cat">
                                    
                                        <?php 
                                        $i=0;
                                        while ($i < count($categorylist)) { ?>
                                            <option value="<?php echo $categorylist[$i]['id']; ?>"   <?php if ($categorylist[$i]['id']== $_REQUEST['cat']||$cat_id == $categorylist[$i]['id']) { ?> selected="selected"<?php  } ?>><?php echo $categorylist[$i]['menu_name']; ?></option>
                                            <?php $i++;
                                        }
										
                                        ?>


                                    </select>
                                </th>
                            </tr>
                        </thead>

                        <tbody id="foo-dish-mob">
                            <?php
                            if (count($dishlist) != 0) {
                                foreach ($dishlist as $items) {
                                    ?>
                                    <tr id="row_mob_<?php echo $items['id']; ?>" data-attr="<?php echo $items['id']; ?>">
                                        <td class="for-handle"></td>
                                        <td>
                                            <!-- Name -->
                                            <div class="td-name">
                                                <span class="td-title">Name</span>
                                                <p><?php echo $items['dish_name']; ?></p>
                                            </div>
                                            <!-- End Name -->

                                            <!-- Sub Title -->
                                            <div class="td-subtitle">
                                                <span class="td-title">Category</span>
                                                <p><?php echo $items['menu_name']; ?></p>
                                            </div>
                                            <!-- End Sub Title -->

                                            <!-- Action -->
                                            <div class="td-action">
                                                <span class="td-title">Action</span>
                                                <label class="switch">
                                                    <input type="checkbox" <?php if ($items['status'] == 'Y') { ?> checked="true" <?php } ?> onClick="cat_status(<?php echo $items['id']; ?>, '<?php echo $items['status']; ?>')" class="cat_status_<?php echo $items['id']; ?>" data-val="<?php echo $items['status']; ?>">
                                                    <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                </label>

                                                <a href="<?php echo base_url(); ?>admin/menu/add_dish/<?= $this->uri->segment(4); ?>/<?php echo $items['menu']; ?>/<?php echo $items['id']; ?>" >
                                                    <i class="fa fa-pencil-square-o"></i> Edit
                                                </a>
                                                <!--<a href="#" data-toggle="modal" data-target="#cat-delete-dish"><i class="fa fa-trash-o"></i> Delete</a>-->
                                                <a href="javascript:void(0)" class="delItemAjax" data-attr="<?php echo $items['id']; ?>" >
                                                    <i class="fa fa-trash-o"></i>DELETE
                                                </a>

                                            </div>
                                            <!-- End Action -->
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                    </table>
                    <!-- End For Mobile View -->
                </div>
                <!-- End Dish -->
            </div>
            <!-- End Tab panes -->
        </div>
        <!-- End Tabs -->
    </div>
</section>
<!-- ===== End Section Menu ===== -->


</form>





<!-- Category Delete Dish Modal -->
<div class="cat-delete modal fade" id="cat-delete-dish" tabindex="-1" role="dialog" aria-labelledby="cat-delete-dish">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <a aria-label="Close" data-dismiss="modal" href="#"><i class="fa fa-times"></i></a>
                <h6>Delete Dish Item</h6>
            </div>
            <!-- End Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <p>Are you sure you want to delete?</p>
            </div>
            <!-- End Modal Body -->
            <input type="hidden" id="delItemID" value="" />   
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button class="dlt-ok delItemOK">Ok</button>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button aria-label="Close" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
            <!-- End Modal Footer -->
        </div>
    </div>
</div>
<!-- End Category Delete Dish Modal-->
<script>

    function cat_status(item_id, status) {
        var sta = $('.cat_status_' + item_id).attr('data-val');
        if ($('.cat_status_' + item_id).attr('data-val') == 'Y')
            $('.cat_status_' + item_id).attr('data-val', 'N');
        else
            $('.cat_status_' + item_id).attr('data-val', 'Y');
        //alert(sta);
        $.ajax({
            type: "post",
            url: "<?php echo base_url(); ?>admin/menu/itemStatus",
            data: {'item_id': item_id, 'status': sta},
            success: function (data) {
                return true;
            }

        });
    }


    $('body').on('click', '.delItemOK', function (e) {
        var item_id = $('#delItemID').val();
        $.ajax({
            type: "post",
            url: "<?php echo base_url(); ?>admin/menu/deleteDish",
            data: {'item_id': item_id},
            success: function (data) {
                //$('.alert-success').show();
                //$('.alert-success').html('Dish item deleted sucessfully');

                setTimeout(function () {
                    $('.saving').show().html("Dish item deleted sucessfully");
                    $('.saving').fadeOut(5000);
                }, 100);

                $("#cat-delete-dish").modal('hide');
                $('#row_' + item_id).remove();
                $('#row_mob_' + item_id).remove();
                return true;
            }
        });
    });



    $('body').on('click', '.delItemAjax', function (e) {
        var item_id = $(this).attr("data-attr");
        $('#delItemID').val(item_id);
        $("#cat-delete-dish").modal('show');

    });
</script>

<script>


    $(document).ready(function () {
        $('.tdlink').click(function () {
            window.location = $(this).attr('href');
            return false;
        });
    });






    /*	function CategoryChange(){
     var cat_id = document.getElementById("cat").value;
     $("#userMasterForm").attr("action", "<?php //echo base_url().$this->user->root;  ?>/menu/dish/"+cat_id);
     $("#userMasterForm").submit();return true;		
     } 
     function CategoryChangeMob(){
     var cat_id = document.getElementById("catMob").value;
     $("#userMasterForm").attr("action", "<?php //echo base_url().$this->user->root;  ?>/menu/dish/"+cat_id);
     $("#userMasterForm").submit();return true;		
     } */

		function change_dish(){
			var cat_id = document.getElementById("cat").value;
			//alert(cat_id);
			$("#userMasterForm").attr("action", "<?php echo base_url();?>admin/menu/dish/<?= $this->uri->segment(4)?>/"+cat_id);
			$("#userMasterForm").submit();return true;		
		} 
		

</script>   

