        
        <!-- ===== Start Footer ===== -->
        <footer>
            <div class="container">
                <div class="copy-right">
                    <p>© 2016 Fulspoon LLC, All Rights Reserved | <a href="<?= base_url() ?>member/terms_condition">Terms of Use</a> | <a href="<?= base_url() ?>member/privacy_policy">Privacy Policy</a></p>
                </div>
            </div>
        </footer>
        
       
      
        <!-- ===== End Footer ===== -->
    
    	<!-- ===== Start Bootstrap core JavaScript ===== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url(); ?>assets/dashboard/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/dashboard/js/sortable.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/app.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/app-dish.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/app-dish-mob.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/custome.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/app-cat.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/multi.js"></script><strong></strong>
        <!-- ===== End Bootstrap core JavaScript ===== -->
    </body>
</html>

