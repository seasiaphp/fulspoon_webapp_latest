<link href="<?php echo base_url() ?>assets/css/bvalidator.css" rel="stylesheet">

<script src="<?php echo base_url() ?>assets/js/jquery.bvalidator.js"></script>
<!-- <script for color picker-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/jscolor/jscolor.js"></script>
	<div class="container"> 
		<div class="clearfix">&nbsp;</div>
			<div class="col-md-12" style="padding-bottom:15px;">
		<form class="form-horizontal" role="form" action="<?php echo base_url()?>index.php/admin/restaurant_list/edit" method="post" name="formlist"  id="formlist">
        	
        	<fieldset>
       		<!-- Text input-->
          	<div class="form-group">
            	
           </div>
           
           
		   <!-- Text input-->
 		   <div class="form-group">
           <?php if($reataurantdetails) {?>
           <table class="col-md-12">
           <tr> <h2>Cuisine Name</h2><td>
          </td> </tr>
          
         <?php foreach($reataurantdetails as $list){ ?>
           <tr> <td>
             <input type="hidden" name="cuisine_id[]" value="<?php echo $list['res_cusine_id'];?>" id="restaurant_id">
                  <select name="cuisine_name[]" class="form-control" >
                      <option value="<?php echo $list['cusine_id'];?>"><?php echo $list['cusine_name'];?></option>
                      
                  <?php foreach($get_cuisine as $cuisine) { if($list['res_cusine_id']!= $cuisine['cusine_id']) {?>
                      <option value="<?php echo $cuisine['cusine_id'];?>"><?php echo  $cuisine['cusine_name'];?></option><?php }} ?>
                    </select> 
           
          </td></tr>
          <?php } ?>
           </table>
           <?php } ?>
           
           
        <table class="col-md-12">
          <tr> <h2>Restaurant Details</h2><td>
          </td> </tr>
        <tr><td>Alcohol</td>
      
        <td >
        
         <select name="alcohol" class="form-control" >
          <option value="<?php echo $reataurantdetails_res['alcohol']; ?>"><?php echo $reataurantdetails_res['alcohol']; ?></option>
             <option value="beer_and_wine">  beer_and_wine</option>
                <option value="full_bar">full_bar</option>
                   <option value="no_alcohol">no_alcohol</option>
         
        </select> 
        </td>
        </tr>
        <tr><td>Parking</td>
        <td>
         <select name="parking" class="form-control">
          <option value="<?php echo $reataurantdetails_res['parking']; ?>"><?php echo $reataurantdetails_res['parking']; ?></option>
             <option value="garage">garage</option>
                <option value="street">street</option>
                   <option value="valet">valet</option>
                    <option value="validated">validated</option>
         
        </select> 
        </td>
        </tr>
        <tr><td>wifi</td>
        <td>
         <select name="wifi" class="form-control">
          <option value="<?php echo $reataurantdetails_res['wifi']; ?>"><?php echo $reataurantdetails_res['wifi']; ?></option>
             <option value="free">free</option>
                <option value="paid">paid</option>
                   <option value="no">no</option>
         
        </select> </td>
        </tr>
        <tr><td>Corkage</td>
       
        <td>
         <select name="corkage" class="form-control">
          <option value="<?php echo $reataurantdetails_res['corkage']; ?>"><?php echo $reataurantdetails_res['corkage']; ?></option>
             <option value="free">free</option>
                <option value="paid">paid</option>
                   <option value="no">no</option>
         
        </select> </td>
        </tr>
        <tr><td>Dietary Restrictions</td>
        <td>
         <select name="dietary_restrictions" class="form-control">
          <option value="<?php echo $reataurantdetails_res['dietary_restrictions']; ?>"><?php echo $reataurantdetails_res['dietary_restrictions']; ?></option>
             <option value="vegetarian">vegetarian</option>
                <option value="vegan">vegan</option>
                   <option value="kosher">kosher</option>
                     <option value="halal">halal</option>
                       <option value="dairy-free">dairy-free</option>
                         <option value="gluten-free">gluten-free</option>
                           <option value="soy-free">soy-free</option>
                </select> </td>
        </tr>
        <tr ><td>Music</td>
        <td>
         <select name="music" class="form-control">
          <option value="<?php echo $reataurantdetails_res['music']; ?>"><?php echo $reataurantdetails_res['music']; ?></option>
             <option value="dj">dj</option>
                <option value="live">live</option>
                   <option value="jukebox">jukebox</option>
                   <option value="background_music">background_music</option>
                   <option value="karaoke">karaoke</option>
                   <option value="no_music">no_music</option>
                
        </select> </td>
        </tr>
        <tr><td>Sports</td>
        <td>
         <select name="sports" class="form-control">
          <option value="<?php echo $reataurantdetails_res['sports']; ?>"><?php echo $reataurantdetails_res['sports']; ?></option>
             <option value="soccer">soccer</option>
                <option value="basketball">basketball</option>
                   <option value="hockey">hockey</option>
                   <option value="football">football</option>
                   <option value="baseball">baseball</option>
                   <option value="mma">mma</option>
                    <option value="other">other</option>
        </select> </td>
        </tr>
        <tr><td>Wheelchair Accessible</td>
        <td>
         <select name="wheelchair_accessible"class="form-control">
          <option value="<?php echo $reataurantdetails_res['wheelchair_accessible']; ?>"><?php echo $reataurantdetails_res['wheelchair_accessible']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
         
        </select> </td>
        </tr>
        <tr><td>Outdoor Seating</td>
        <td>
         <select name="outdoor_seating" class="form-control">
          <option value="<?php echo $reataurantdetails_res['outdoor_seating']; ?>"><?php echo $reataurantdetails_res['outdoor_seating']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
        </select> </td>
        </tr> 
        
        <tr><td>Smoking</td>
        <td>
         <select name="smoking" class="form-control">
          <option value="<?php echo $reataurantdetails_res['smoking']; ?>"><?php echo $reataurantdetails_res['smoking']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
                   <option value="outdoor">outdoor</option>
        </select> </td>
        </tr> 
        <tr><td>Waiter Service</td>
        <td>
         <select name="waiter_service" class="form-control">
          <option value="<?php echo $reataurantdetails_res['waiter_service']; ?>"><?php echo $reataurantdetails_res['waiter_service']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
        </select> </td>
        </tr> 
         <tr><td>Television</td>
        <td>
         <select name="television" class="form-control">
          <option value="<?php echo $reataurantdetails_res['television']; ?>"><?php echo $reataurantdetails_res['television']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
        </select> </td>
        </tr> 
         <tr><td>Ambience</td>
        <td>
         <select name="ambience" class="form-control">
          <option value="<?php echo $reataurantdetails_res['ambience']; ?>"><?php echo $reataurantdetails_res['ambience']; ?></option>
             <option value="romantic">romantic</option>
                <option value="upscale">upscale</option>
                   <option value="touristy">touristy</option>
                   <option value="trendy">trendy</option>
                   <option value="casual">casual</option>
        </select> </td>
        </tr> 
        
            <tr><td>Delivery</td>
        <td>
         <select name="delivery" class="form-control">
          <option value="<?php echo $reataurantdetails_res['delivery']; ?>"><?php echo $reataurantdetails_res['delivery']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
         
        </select> </td>
        </tr> 
               <tr><td>Pickup</td>
        <td>
         <select name="pickup" class="form-control">
          <option value="<?php echo $reataurantdetails_res['pickup']; ?>"><?php echo $reataurantdetails_res['pickup']; ?></option>
             <option value="yes">yes</option>
                <option value="no">no</option>
        </select> </td>
        </tr>  
        </table>
            	
          </div>	    
          <!-- Text input--> 
          <div class="form-group">
           		<label class="col-sm-4 " for="textinput"></label>
          </div>
          <div class="col-lg-12">
	  </div>     
      <div class="clearfix">&nbsp;</div>
       <!-- Text input--><!-- Text input-->   
       <!-- Text input-->
       <!-- Text input-->
    <input type="hidden" name="restaurant_id" value="<?php echo $reataurantdetails_res['restaurant_id'];?>" id="restaurant_id">
         <input type="hidden" name="admin_id" value="<?php echo $reataurantdetails['admin_id'];?>" id="admin_id">
         <button type="button" class="btn btn-default pull-right" onclick="location.href='<?php echo base_url()?>index.php/admin/restaurant_list/lists'">Cancel</button>
      <input type="submit" class="btn btn-info pull-right" name="submit_btn" id="submit_btn" style="margin-right:10px;" value="UPDATE"/>
     
     
     
    
     
     
     
        </fieldset>
      </form>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
</div><!-- /.container -->


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  
	 </div>

	<script>

	$(document).ready(function(){

		
		var options = {
			singleError: true,
       		showCloseIcon: false,					
		};
		$var=$("#formlist").bValidator(options);
		
	});
	
  $("#change_link").click(function(){
		   var admin_id=$('#admin_id').val();
			$("#myModal").html("");
			$.ajax({
			type:"post",
			url:"<?php echo base_url(); ?>admin/restaurant_list/change",
			data:{'admin_id':admin_id},
			success:function(data){
				$("#myModal").html(data);
				//alert(data);
				}
			});
			   
			    
		})

</script>



<style>
.admin_body{
	background-color:#FFFFFF;
}
.form-group{
	margin-right:0px!important;
}
</style>