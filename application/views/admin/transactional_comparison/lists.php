<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css">
	<?php if($this->session->flashdata('error_message')!=''){ ?>
 		<div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
    <?php }else{ ?>
 		<div class="alert alert-danger" role="alert" style="display:none;"></div>
    <?php } ?>
    <?php if($this->session->flashdata('success_message')!=''){ ?>
 		<div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
    <?php }else{ ?>
 		<div class="alert alert-success" role="alert" style="display:none;"></div>
    <?php } ?>
<form name="userMasterForm" id="userMasterForm" action="<?php echo base_url().$this->user->root;?>/customers/lists" method="post" >     
	<div  class="tab_wrper" style="padding:10px;">
    <ul role="tablist" class="nav nav-tabs tab_links" id="myTabs">
      <li class="active tog_tab" role="presentation">
     	 <a aria-expanded="true" aria-controls="category" role="tab" id="" href="<?php echo base_url().$this->user->root;?>/transactional_comparison/lists">Expected Incremental Orders</a>      </li>
	</ul>
     
<div class="tab-content tab_contwp dish_cat_tab" id="myTabContent">  
        
    <div aria-labelledby="category-tab" id="category"  role="tabpanel"> 
         
		<div class="table-responsive"> 
    		  <table class="table table-striped tbl_category">
                  <thead class="head_table">
                    <tr>
                    <th class="col-md-2 col-sm-2"><center>Left Order Value</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Right Order Value</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Left Commission</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Right Commission</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Left Processing Fees</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Right Processing Fees</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Left Concession</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Right Concession</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Left Subscription Revenue</center> </th>
                      <th class="col-md-2 col-sm-2"><center>Right Subscription Revenue</center> </th>
                    </tr>
                  </thead>
                  <tbody class="table_body">
                    
                    
                    <?php 
					
					 if(count($promolist)!=0){
                    foreach($promolist as $promo){ ?>
                    <tr id="row_<?php echo $promo['id'];?>">
                     
                      <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink" >           
                        <center><?php echo $promo['lt_order_valul'];?> <center>
                      </td>
                      
                        <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['rt_order_value'];?></center></td>
                     
                     
                      
                       <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink" >           
                        <center><?php echo $promo['lt_commission'];?> <center>
                       </td>
                      
                        <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
						<center><?php echo $promo['rt_commission'];?></center></td>
                         <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['lt_processing_Fees'];?></center></td>
                         <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['rt_processing_Fees'];?></center></td>
                         <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['lt_concession'];?></center></td>
                         <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['rt_concession'];?></center></td>
                         <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['lt_subscription_Revenue'];?></center></td>
                         <td href="<?php echo base_url().$this->user->root;?>/transactional_comparison/add/<?php echo $promo['tc_id']; ?>" style="cursor:pointer;" class="full_link tdlink">
					  <center><?php echo $promo['rt_subscription_Revenue'];?></center></td>
                
                    </tr>
                  <?php 	}
                        }else { ?>
                        
                      <tr>
                       <td colspan="8" style="color:#FF0000">
                      <center>No Records...</center>
                      </td>
                      </tr>
                   <?php } ?>
                    
                  </tbody>
                </table>
                </div>
                </div>
                </div>
          <?php if(count($promolist)!=0){?>
       <div class="row" id="Table footer">
    <div class="col-lg-12 ">
      <div class="col-offset-1 col-lg-10">
				<div class="input-group">
					<ul class="pagination pull-right" style="margin:0px;">
					<?php echo $this->pagination->create_links(); ?>
					</ul>
				</div>
		</div>
    </div>
  </div>
  <?php } ?>
  </div>
 </form>      
<script>   
	$(document).ready(function(){
		$('.tdlink').click(function(){
			window.location = $(this).attr('href');
			return false;
		});
	});

// BLock Unblock
	
$('#btn_search').click(
		function(){		
			$("#userMasterForm").attr("action", "<?php echo base_url().$this->user->root;?>/transactional_comparison/lists");
				$("#userMasterForm").submit();return true;
	});
	//Change Limit of pagination
	$(document).on('change', '#limit', function() {
			$("#userMasterForm").attr("action", "<?php echo base_url().$this->user->root;?>/transactional_comparison/lists");
				$("#userMasterForm").submit();return true;
	});	
	
</script>
<style>
.nav-tabs {
    border-bottom: 11px solid #ffffff!important;
}
.nav-tabs > li {
    margin-left: -1px!important;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #fff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555;
    cursor: default;
}
</style>