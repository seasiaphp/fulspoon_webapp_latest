<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css">
	

<form name="userMasterForm" id="userMasterForm" action="<?php echo base_url().$this->user->root;?>/user_list/details" method="post" >     
	<div  class="tab_wrper" style="padding:10px;">
    <ul role="tablist" class="nav nav-tabs tab_links" id="myTabs">
            <div class="col-md-12" table-responsive>
      <table style="float:right;font-style: italic;font-size:15px"><tr ><td >Customer ID:</td><td ><?php echo $result['member_id'] ?></td></tr>
      <?php if( $result['device_id']){?><tr><td>Device ID:</td><td><?php echo $result['device_id'] ?></td></tr><?php } ?>
      <?php if( $result['facebook_id']){?><tr><td>Facebook ID:</td><td><?php echo $result['facebook_id'] ?></td></tr><?php }?>
      </table>
      
      </div>
      </ul>
     
<div class="tab-content tab_contwp dish_cat_tab" id="myTabContent">  
         
    <div aria-labelledby="category-tab" id="category"  role="tabpanel"> 
         
		
    		
     	<div class="col-md-12" table-responsive style=" margin-top:20px">
        <div class="col-lg-6" table-responsive>
      <center><img height="" width="" style="padding-top:10px" src="<?php if($result['profile_image']!=''){echo  base_url() . "uploads/members/" . $result['member_id'] . "_thumb." . $result['profile_image'] . '?' . date("his"); } else { ?><?php echo base_url()?>assets/images/profile/no_image.png<?php } ?>"/></center>
		</div> 
        <div class="col-lg-6" >
       
        <table class="table table-striped tbl_category"><tr ><td>First Name</td><td  ><input type="text" value="<?php echo $result['first_name'];?>" name="first_name" placeholder="First Name" class="form-control inputsignup"/> </td></tr>
        <tr><td>Last Name</td><td ><input type="text" value="<?php echo $result['last_name'];?>" name="last_name" placeholder="Last Name" class="form-control inputsignup"/> </td></tr>
        <tr><td>E-Mail</td><td ><input type="text" value="<?php echo $result['email'];?>" name="email" placeholder="E-Mail" class="form-control inputsignup"/> </td></tr>
        <tr><td>Phone</td><td ><input type="text" value="<?php echo $result['phone'];?>" name="phone" placeholder="Phone" class="form-control inputsignup"/> </td></tr>
        <td colspan="2"><input type="hidden" name="member_id" value="<?php echo $result['member_id'];?>" /><button type="button" class="btn btn-info pull-right" name="submit_btn" id="submit_btn" href="javascript:void(0);" style="margin-right:10px;" onclick="validation();">Save</button></td>
        </table>
      
		</div> 
	</div>
    
     
     <ul  class="nav nav-tabs tab_links" >
     <label>Addresses:</label></ul>
	 <div class="col-md-12 table-responsive" style=" margin-top:20px">
    
    <?php foreach($address as $add) {?> <table class="col-md-12" style="margin-bottom:20px">
    <tr >
   <td></td> <td style="float:right;font-style: italic;font-size:15px;text-transform:uppercase; text-align:right"><?php echo $add['save_as']; ?></td>
    </tr>
    <tr >  <td  colspan="2" >
    <input type="text" value="<?php echo $add['address']; ?>" placeholder="Address" class="form-control inputsignup" readonly="readonly"/></td></tr>
     
     <tr>
       <td colspan="2"><input type="text" value="<?php echo $add['city'].','.$add['state']."  ".$add['pincode']; ?>" placeholder="City, State ZIP, COUNTRY" class="form-control inputsignup" readonly="readonly"/></td>
     </tr>
    <?php if($add['stripeid']!=''){?> <tr ><td ><input type="text" value="<?php echo $add['brand'];?>" placeholder="CC on File" class="form-control inputsignup" readonly="readonly"/></td><td><input type="text" value="" placeholder="XXXX XXXX XXXX <?php echo $add['last_4digit'];?>" class="form-control inputsignup" readonly="readonly"/></td></tr><?php } ?>
    
     
     </table><?php } ?>
	 </div>
   
 <ul  class="nav nav-tabs tab_links" style="margin-top:20px">
     <label>Stripe Payment </label></ul>
   <div class="col-lg-12 table-responsive" style=" margin-top:20px">
   
    <table class="table table-striped tbl_category" style="margin-bottom:30px">
  
       <tr><th class="col-lg-3">Subscription Name</th>
       <th class="col-lg-3">Start Date</th>
       <th class="col-lg-3">End date</th><br />
       <th class="col-lg-3">Amount</th></tr> 
        <?php foreach($payment_history as $history){?>
       <tr><td><?php echo $history['package_name'];?></td>
       <td ><?php echo   date("m-d-Y", strtotime($history['start_date'])) ;?></td>
       <td><?php echo  date("m-d-Y", strtotime($history['End_date'])) ;?></td>
       <td ><?php echo $history['amount'];?></td></tr>
       <?php foreach($history['restaurant_list'] as $list_res){ ?>
       <tr><td colspan="4" ><i style="padding-left:15px;font-size:15px"><?php echo $list_res['restaurant_name'] ?></i></td></tr><?php } ?>
       <tr ><td colspan="4"></td></tr>
       <?php } ?>
        </table>
      
  
   
   </div>
   <ul  class="nav nav-tabs tab_links" >
     <label>Subscription Change History:</label></ul>
   <div class="col-lg-12 table-responsive" style=" margin-top:20px">
     <?php foreach($payment_history as $history){?>
   <table class="table table-striped tbl_category"><tr><td class="col-lg-3"><?php echo $history['package_name'];?></td>
   <td class="col-lg-3"><?php      
   echo date("m-d-Y", strtotime($history['start_date']));
    ?></td>
   <td class="col-lg-3"><?php echo date("m-d-Y", strtotime($history['End_date']));?></td>
   <td class="col-lg-3"> Total Amount</td></tr>
   <?php foreach($history['restaurant'] as $list){?><tr ><td></td><td></td><td><?php echo $list['restaurant_name'] ?></td>
   <td  ><?php echo $list['total_amount'] ?></td></tr><?php } ?></table><?php } ?>
  
   </div>
      </div>
      
      </div>
      </div>
       
  </div>
 </form>      
<script>
function validation(){
$("#userMasterForm").submit();
}
</script>