<script>
    if (typeof (EventSource) !== "undefined") {
        var source = new EventSource("<?php echo base_url() . "admin/orders/admin_loadNewOrder" ?>");
        source.onmessage = function (event) {
            var data = event.data;
            var datas = data.split("###");
            var count = datas[0];
            var content = datas[1];
//            alert(count);
            //document.getElementById("tablebody").innerHTML = content;
            $('#tablebody').html(content);
            $('#tablebody_tab').html(content);
            $('#tablebody_mob').html(content);
            $('#tablebody_tab .hide-desk').removeClass('hide');
            $('#tablebody_mob .hide-desk').removeClass('hide');
            $('#tablebody .hide-desk').hide();
            $('#tablebody_tab .hide-tab').hide();
            $('#tablebody_mob .hide-mob').hide();
            $('.facebook').html($('#fb_svg').html());
            $('.web').html($('#web_svg').html());
            $('.app').html($('#app_svg').html());
            $('#countnew').html(count);
            $('.countnew').html(count);
            //document.getElementById("countnew").innerHTML = count;
        };
    } else {
        document.getElementById("tablebody").innerHTML = "Sorry, your browser does not support server-sent events...";
    }
</script>

<?php if ($this->session->flashdata('success_message') != '') { ?>
    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
<?php } ?>

<span id="fb_svg" class="hide">

    <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="52px" height="52px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve">
        <circle fill="#1C3749" cx="26" cy="26" r="25.364"/>
        <text transform="matrix(1 0 0 1 20.2852 33.2441)" fill="#FFFFFF" font-family="'FontAwesome'" font-size="20"></text>
    </svg>
</span>
<span id="web_svg" class="hide">
    <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="52px" height="52px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve">
        <circle fill="#1C3749" cx="26" cy="26" r="25.364"/>
        <text transform="matrix(1 0 0 1 16.2852 32.2441)" fill="#FFFFFF" font-family="'FontAwesome'" font-size="18"></text>
    </svg>
</span> 
<span id="app_svg" class="hide">

    <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="52px" height="52px" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve">
        <circle fill="#1C3749" cx="26" cy="26" r="25.364"/>
        <text transform="matrix(1 0 0 1 22.2852 33.2441)" fill="#FFFFFF" font-family="'FontAwesome'" font-size="20"></text>
    </svg>
</span>  


<!-- ===== Start Section Main ===== -->
<section class="main-sec">

    <div class="alert alert-danger hide" role="alert"></div>
    <div class="alert alert-success hide" role="alert"></div>

    <div class="container-fluid">
        <!-- Tabs -->
        <div class="order-tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-new" aria-controls="new" role="tab" data-toggle="tab">New 
                        (<span class="countnew"><?php echo $allcounts['newcount']; ?></span>)</a></li>
                <li role="presentation"><a  href="<?php echo base_url(); ?>admin/orders/admin_accepted" aria-controls="accepted" >Accepted 
                        (<span class="accbadge"><?php echo $allcounts['accepted']; ?></span>)</a></li>
                <li role="presentation"><a href="<?php echo base_url(); ?>admin/orders/admin_cancelled" aria-controls="declined" >Declined </a></li>

                <li role="presentation"><a href="<?php echo base_url(); ?>admin/orders/admin_all" aria-controls="all" >All</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <!-- New -->
                <div role="tabpanel" class="tab-pane fade in active" id="tab-new">
                    <!-- DESKTOP OR IPAD VIEW -->
                    <table class="table table-hover table-dekstop tbl_category">
                        <thead>
                            <tr>
                                <?php if ($this->user->role != '3') { ?>
                                                                <!--<th width="10%"></th>-->
                                <?php } ?>  
                                <th>Customer</th>
                                <th>Created</th>
                                <th>Expected</th>
                                <th>ID</th>
                                <th>Type</th>
                                <th >Total</th>


                            </tr>
                        </thead>

                        <tbody  class="table_body" id="tablebody">




                        </tbody>
                    </table>
                    <!-- END DESKTOP OR IPAD VIEW -->

                    <!-- TABLET VIEW -->
                    
                    <!-- END TABLET VIEW -->

                    <!-- MOBILE VIEW -->
                    
                    <!-- END MOBILE VIEW -->
                </div>
                <!-- End New -->

                <!-- Accepted -->

                <!-- End Accepted -->

                <!-- Declined -->

                <!-- End Declined -->

                <!-- Late -->

                <!-- End Late -->

                <!-- All -->

                <!-- End All -->
            </div>
            <!-- End Tab panes -->
        </div>
        <!-- End Tabs -->

        <!-- Order Detail Modal 1 -->
        <div class="order-detail-modal modal fade bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Order Close Modal -->
                    <a href="#" data-dismiss="modal" class="mo_close" aria-label="Close"><i class="fa fa-times"></i></a>
                    <!-- End Order Close Modal -->

                    <!-- Order Modal Body -->
                    <div class="modal-body">

                    </div>
                    <!-- End Order Modal Body -->
                </div>
            </div>
        </div>
        <!-- End Order Detail Modal 1 -->



    </div>
</section>
<!-- ===== End Section Main ===== -->


<script>
    $(document).ready(function () {
        $('body').on('click', '.trlink', function () {
            //$('.tdlink').click(function(){
            //window.location = $(this).attr('href');
            var order_id = $(this).attr("id");
            $('.loader_home').show();
            //return false;

            $.ajax({
                url: '<?php echo base_url() ?>owner/orders/details_ajax',
                type: 'POST',
                data: {'order_id': order_id},
                success: function (result) {

                    $(".order-detail-modal .modal-body").html(result);
                    $("#myModal1").modal("show");

                    $('.loader_home').hide();


                }
            });

            return false;
        });
    });


    $('body').on('click', '.completed', function () {

        var order_id = $("#order_id").val();
        //alert(order_id);
        $('.loader_home').show();
        var cnt = parseInt($('.accbadge').html()) - 1;
        //return false;
        $.ajax({
            url: '<?php echo base_url() ?>owner/orders/completeOrder',
            type: 'POST',
            data: {'order_id': order_id},
            success: function (result) {
                $('.accbadge').html(cnt);
                $('#' + order_id).remove();
                $('.alert-success').removeClass('hide');
                $('.alert-success').html('Order completed successfully');
                window.setTimeout(function () {
                    $('.alert-success').addClass('hide');
                }, 3000);

                var rowCount = $('.tbl_category tr').length;
                if (rowCount == '1') {
                    $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
                }

                $('.loader_home').hide();
                $("#myModal1").modal('hide');
                //window.load('<?php echo base_url() ?>orders/lists');

            }
        });

    });
    $(document).on('click', '.cancel', function (e) {
        var order_id = $("#order_id").val();


        e.preventDefault();

//        $.Zebra_Dialog('Are you sure you want to decline this order?', {
//            'type': 'question',
//            'title': 'Decline order',
//            'buttons': ['OK', 'Cancel'],
//            'onClose': function (caption) {
//                  alert("dasd");
//        if (caption == 'OK') {

        $('.loader_home').show();

        $.ajax({
            url: '<?php echo base_url() ?>owner/orders/cancelOrder',
            type: 'POST',
            data: {'order_id': order_id},
            success: function (result) {
                if (result.indexOf("success") >= 0)
                {

                    $('#' + order_id).remove();
//                    $('.alert-success').empty();
                    $('.alert-success').show();
                    $('.alert-success').html('Order cancelled successfully');
                    var rowCount = $('.tbl_category tr').length;
                    if (rowCount == '1') {
                        $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
                    }

                    //window.location.href="<?php echo base_url() ?>orders/lists";
                }
                else
                {
                    $('.alert-danger').removeClass('hide');
                    $('.alert-danger').html('Unknown error occured');
                    $("html, body").animate({scrollTop: 0}, "fast");
                    window.setTimeout(function () {
                        $('.alert-danger').addClass('hide');
                        ;
                    }, 3000);
                }
                $("#myModal1").modal('hide');
                $('.loader_home').hide();
            }
        });
        return true;
//                } else {
//                    return false;
//                }
//            }
//        });




    });
    $('body').on('click', '.accept', function () {
        var order_id = $("#order_id").val();
        $('.loader_home').show();
        //return false;
        var cnt = parseInt($('.accbadge').html()) + 1;

        $.ajax({
            url: '<?php echo base_url() ?>owner/orders/acceptOrder',
            type: 'POST',
            data: {'order_id': order_id},
            success: function (result) {
                $('.acbtncls').hide();
                $('.cmpbtncls').show();
                $('.accbadge').html(cnt);
                //alert($('#hidecount').val());
                var cnt11 = parseInt($('#hidecount').val()) - 1;
                $('#hidecount').val(cnt11);
                /*$('#row_'+order_id).remove();
                 $('.alert-success').show();
                 $('.alert-success').html('order accepted successfully');
                 var rowCount = $('.tbl_category tr').length;
                 if(rowCount=='1'){
                 $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
                 }
                 
                 $('.loader_home').hide();
                 */
                $('.loader_home').hide();
            }
        });

    });

    $('body').on('click', '.refund', function () {
        $("#refund_input").show();
        $('.acbtncls').hide();
        $('.cmpbtncls').show();
        $("#refund_amount").val($("#total_amount").val());
        $("#refund_input").removeClass('has-error');
        $("#refund_input .input-group").attr('data-original-title', 'Enter refund amount');

    });

    $('body').on('click', '.refund_cancel', function () {

        $("#refund_input").hide();
        $('.acbtncls').show();
        $('.cmpbtncls').hide();

    });

    $('body').on('click', '.continue', function () {
        var amount = parseFloat($("#refund_amount").val());
        var total_amount = parseFloat($("#total_amount").val());
        var order_id = $("#order_id").val();
        if (amount > 0 && amount <= total_amount) {
            $("#refund_input").hide();
            $('.acbtncls').hide();
            $('.cmpbtncls').hide();
            $("#refund_input").removeClass('has-error');
            $("#refund_input .input-group").attr('data-original-title', 'Enter refund amount');
            $('.loader_home').show();
            $.ajax({
                url: '<?php echo base_url() ?>owner/orders/OrderPartialRefund',
                type: 'POST',
                data: {'order_id': order_id, 'amount': amount},
                success: function (result) {
                    $('.loader_home').hide();
                    if (result.indexOf("success") >= 0)
                    {
                        $(".refund").removeClass('.refund');
                        $('.alert-success').removeClass('hide');
                        $('.alert-success').html('Refund successfully');
                        window.setTimeout(function () {
                            $('.alert-success').addClass('hide');
                        }, 3000);

                        $("html, body").animate({scrollTop: 0}, "fast");
                        //setTimeout(function(){window.location.href="<?php echo base_url() ?>/orders/details/"+order_id;}, 1000);
                    }
                    else
                    {
                        $('.acbtncls').show();
                        $('.alert-danger').removeClass('hide');
                        $('.alert-danger').html('Unknown error occured');
                        $("html, body").animate({scrollTop: 0}, "fast");
                        window.setTimeout(function () {
                            $('.alert-danger').addClass('hide');
                            ;
                        }, 3000);
                    }

                    $("#myModal1").modal('hide');

                }
            });

        }
        else
        {
            $("#refund_input").addClass('has-error');
            $("#refund_input .input-group").attr('data-original-title', 'Invalid amount');

        }
    });
    $(document).ready(function () {
        $(".customer-detail.tablet-res dl dd").after("<div class='line-bottom'></div>")
    });
	
	
//Change Limit of pagination
    $(document).on('change', '#limit', function () {
        $("#userMasterForm").attr("action", "<?php echo base_url() ?>owner/orders/lists");
        $("#userMasterForm").submit();
        return true;
    });


    $('#btn_search').click(
            function () {
                $("#userMasterForm").attr("action", "<?php echo base_url() ?>owner/orders/lists");
                $("#userMasterForm").submit();
                return true;
            });

    $(document).on('change', '#status', function () {
        $("#userMasterForm").attr("action", "<?php echo base_url() ?>owner/orders/lists");
        $("#userMasterForm").submit();
        return true;
    });
    // END: Change Limit of pagination


</script>