<style>
.fa.fa-gear {
    color: #a2a2a2;
    font-size: 23px;
}
</style>

<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css">
    <?php if($this->session->flashdata('error_message')!=''){ ?>
 		<div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
    <?php }else{ ?>
 		<div class="alert alert-danger" role="alert" style="display:none;"></div>
    <?php } ?>
    <?php if($this->session->flashdata('success_message')!=''){ ?>
 		<div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
    <?php }else{ ?>
 		<div class="alert alert-success" role="alert" style="display:none;"></div>
    <?php } ?>
    
<form name="userMasterForm" id="userMasterForm" action="<?php echo base_url().$this->user->root;?>/location/lists" method="post" >   

<div  class="tab_wrper" >
  <ul role="tablist" class="nav nav-tabs tab_links" id="myTabs"><li style="width:90%;">
       <li class="active tog_tab" role="presentation">
     	 <a aria-expanded="true" aria-controls="category" role="tab" id="" href="<?php echo base_url().$this->user->root;?>//preference/lists_package">Email Templates</a>      </li>
        <!--<div class="col-lg-1 pull-right"><a href="<?php echo base_url().$this->user->root;?>/preference/add" class="btn btn-info">Add New</a></div>-->
      </li>
    </ul>
    
    
 
	<div class="tab-content tab_contwp dish_cat_tab" id="myTabContent">                
   	  <div aria-labelledby="category-tab" id="category" class="" role="tabpanel">           
		<div class="table-responsive"> 
    <table class="table table-striped tbl_category">
                  <thead class="head_table">
                  <tr>
                      <th width="101" class="col-md-2 col-sm-2"></th>
                    <th width="101" class="col-md-10 col-sm-10">Title</th>
                    </tr>
                  </thead>
                  <tbody class="table_body">
                    
                    
                    <?php 
                   
                   if(count($cms)!=0){
                    foreach($cms as $row => $val){ ?>
                    <tr id="row_<?php echo $val['email_id'];?>">
                      <td width="160" >    
                     	<a href="<?php echo base_url().$this->user->root;?>/email_template/edit_template/<?php echo $val['email_id'];?>">
							<img src="<?php echo base_url() ?>assets/images/view_details.png" alt="Edit" class="tmg25" title="Edit">
                        </a>                     
                      </td>
                      <td href="<?php echo base_url().$this->user->root;?>/email_template/edit_template/<?php echo $val['email_id'];?>" style="cursor:pointer;" class="full_link">
                        <?php echo $val['email_title'];?>
                       </td>
					</tr>
                  
                  <?php 	}
                        }else { ?>
                        
                      <tr>
                      <td colspan="7">
                      No Templates Found...
                      </td>
                      </tr>
                   <?php } ?>
                    
                  </tbody>
                </table>
     		</div>
   		</div>
   </div>
       <?php if(count($cms)!=0){?>
       <div class="row" id="Table footer">
    <div class="col-lg-12 ">
		<!--<div class="col-offset-1 col-lg-2 pull-right">
			<div class="input-group">
			  <span class="input-group-addon">
				<i class="glyphicon glyphicon-map-marker"></i> 
			  </span>
			  <select name="limit" id="limit" class="form-control" >
					<option value="5" <?php if($limit == 5){?> selected="selected"<?php } ?> >5</option>
					<option value="10" <?php if($limit == 10){?> selected="selected"<?php } ?> >10</option>
					<option value="20" <?php if($limit == 20){?> selected="selected"<?php } ?> >20</option>
					<option value="50" <?php if($limit == 50){?> selected="selected"<?php } ?>>50</option>
					<option value="100" <?php if($limit == 100){?> selected="selected"<?php } ?>>100</option>
					<option value="all" <?php if($limit == 'all'){?> selected="selected"<?php } ?>>ALL</option>
				</select>
			</div>
		</div>-->
		
		<!--<div class="col-offset-1 col-lg-10">
				<div class="input-group">
					<ul class="pagination pull-right" style="margin:0px;">
					<?php echo $this->pagination->create_links(); ?>
					</ul>
				</div>-->
		</div>
    </div>
  </div>
  <?php } ?>
  
</div>
</form>


<script>
  $(document).ready(function(){
    $('.full_link').click(function(){
        window.location = $(this).attr('href');
        return false;
    });
	
	$('.config_link').click(function(){
        window.location = $(this).attr('href');
        return false;
    });
	
  });


</script>
	  

<style>
.nav-tabs {
    border-bottom: 11px solid #ffffff!important;
}
.nav-tabs > li {
    margin-left: -1px!important;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #fff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555;
    cursor: default;
}

</style>