<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>

<div class="bred_outer">
    <div class="bred_nav">

        <ul>

        <li><a href="#"><img class="bn_home" src="<?php echo base_url();?>public/images/br_home.jpg"></a></li>
        <li><a href="<?php echo base_url();?>admin_dashboard">Dashboard</a></li>
        <li><img class="bn_arow" src="<?php echo base_url();?>public/images/br_arow.jpg"></li>
        <li>Configuration</li><li><img src="<?=base_url()?>public/images/br_arow.jpg" class="bn_arow" /></li>
        
        <li><a href="<?php echo base_url();?>email_template" style="color: #1a91ec;">Email Templates</a></li>
        <li><img class="bn_arow" src="<?php echo base_url();?>public/images/br_arow.jpg"> </li>
        <li><a href="#" style="color: #1a91ec;">Details</a></li>

        </ul>


    </div>
</div>
<div class="container">
   
    <div class="topsec">&nbsp;
        <a class="save_btn_green" onclick="savetemplate();" href="javascript:void(0);">Save</a>
        <!--<input type="submit" class="save_btn_green" name="button" id="button" value="Save"  class="form_btn"/>-->
    </div>
    <div class="wd_975">
        <div class="wd_975_title">
        <h3>Template Details</h3>
        </div>
    </div>
      <?php if(validation_errors()){ ?>
   
   
        <div class="erroe_msg">
            <?php echo validation_errors(); ?>
         </div>
 
      <?php } ?>
    
    <div class="wd_975_blank">
        <div class="div_container"><div class="div_blok">
                <form name="cms" action="" method="post">
                    <input type="hidden" name="id" value="<?php echo $cms['email_id']; ?>" />	

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>
                            <td class="td_wht">

                                        <div class="div_details_in_l">Title</div>
                            </td>
                            <td class="td_wht">
                                        <div class="div_details_in_r" style="margin-top:5px;">
                                            <input type="hidden" value="<?php echo htmlentities($cms['email_title']) ;?>" name="email_title" id="email_title"/>
                                            <?php if($cms['email_title']) : echo htmlentities($cms['email_title']); else : echo $_POST['email_title'] ; endif; ?>
                                        </div>
                            </td>       
                        </tr>
                        <tr>
                            <td class="td_wht">  
                                   <div class="div_details_in_l">Email Subject</div>
                            </td>
                            <td class="td_wht">
                                   <div class="div_details_in_r">
                                       <label>
                                           <input type="text"  name="email_subject" id="email_subject" class="form_med" style="width: 520px; height: 30px;"   value=" <?php if($cms['email_subject']) : echo htmlentities($cms['email_subject']); else : echo $_POST['email_subject'] ; endif;?>" />
                                       </label>
                                   </div>
                            </td>

                    </tr>
                    <tr>
                        <td class="td_wht">   
                            <div class="div_details_in_l">Email Content</div>
                        </td>
                        <td class="td_wht">
                            <div class="div_details_in_r" >
                                <textarea name="email_template" id="email_template" cols="20" rows="5" class="ckeditor" style="width:500px;">
                                    <?php if($cms['email_template']) : echo $cms['email_template']; else : echo $_POST['email_template'] ; endif;?>
                                </textarea>
                                <script type="text/javascript">
                                        var desz = document.getElementById('email_template').value;
                                        document.getElementById('email_template').value ='';
                                        document.getElementById('email_template').value = $.trim(desz);
                                </script>
                            </div>
                        </td>
                        </tr>

                    </table>

                </form>
            </div> 
        </div>
    </div>
        <div class="mne_footer">&copy; <?=date("Y")?> evolutioneat.com</div>

</div>


<script type="text/javascript">
function savetemplate(){

    if($('#email_subject').val()=='')
        {
            alert('Please enter email subject');
            return false;
        }
    if($('#email_template').text()=='')
        {
            alert('Please enter email content');
            return false;
        }
    document.cms.submit();
}

</script>

