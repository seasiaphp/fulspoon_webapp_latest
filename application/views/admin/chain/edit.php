<script src="<?= base_url(); ?>assets/js/chain/pair-select.min.js"></script>
<script src="<?= base_url(); ?>assets/js/chain/main.js"></script>
<style>
    select:active, select:hover {
        outline: none
    }

    /* make it red instead (with with same width and style) */
    select:active, select:hover {
        outline-color: none
    }

</style>
<script>

    function submitForm() {
        $("#formlist").submit();
    }



    function cancel()
    {

        window.location.href = "<?php echo base_url() ?>admin/home";
    }
</script>


<div class="container">

    <?php $this->load->library('session');
    ?>

    <div class="row" id="Title">
        <div class="col-lg-12"><legend>Chain Restaurant Details</legend></div>

    </div>


    <div class="row form-group ">
        <form  method="post" name="formlist"  id="formlist" action="<?= base_url(); ?>admin/chain/editVAl"  onsubmit="">
            <input type="hidden" name="admin_id" value="<?php echo $data['id']; ?>" />
            <fieldset>
                <div class="row form-group">  
                    <div class="col-lg-6">
                        <label>Name</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                            <input type="text" value="<?php echo $data['name'] ?>" placeholder="<?php echo $data['name'] ?>" class="form-control" name="chainname" >
                            <input type="hidden" name="id" value="<?= $data['id']; ?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label>Description</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                            <textarea name="desc" style="width:100%"><?= $data['desc'] ?></textarea>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?= $data['managed_restaurent'] ?>" name="master" id="masterR">
                <!--                <div class="row form-group">  
                                    <div class="col-lg-6">
                                        <label>Master Restaurant</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                          
                                            <select  style="width:100%;height: 34px">
               
                                            </select>
                                        </div>
                                    </div>              
                                </div>-->

                <div class="row form-group">  
                </div>


                <div class="row form-group">
                    <div class="col-lg-12">
                        <select id="MasterSelectBox" multiple size="6" style="max-width: 460px;min-width:200px;float:left; ">
                            <?php
                            foreach ($rest_nonSelect as $nonSelect) {
                                ?> 
                                <option value="<?= $nonSelect['restaurant_id'] ?>"><?= $nonSelect['restaurant_name'] . ", " . $nonSelect['formatted_address'] ?></option>
                            <?php } ?>
                        </select>

                        <div style="float:left;margin:10px;">
                            <button type="button" id="btnAdd">></button><br>
                            <button type="button" id="btnRemove"><</button>
                        </div>

                        <select id="PairedSelectBox" multiple  size="6" style="max-width: 460px;min-width:200px;float:left; " name="SelectedRest[]">
                            <?php
                            foreach ($rest_Select as $Select) {
                                ?> 
                                <option selected value="<?= $Select['restaurant_id'] ?>"><?= $Select['restaurant_name'] . ", " . $Select['formatted_address'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-3 col-offset-3"><button type="button" class="btn btn-info btn-group-justified" id="save_edit" onclick="saveEdit()">Submit</button></div>
                    <div class="col-lg-3"><button class="btn btn-default btn-group-justified" type="button"  onclick="cancel();">Cancel</button></div>
                    <div class="col-lg-3"></div>
                </div>
            </fieldset>


    </div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color: #151414"><center>Select Master Restaurant</center></h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12"> <select style="max-width: 500px;" name="master" id="select_master"></select></div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="submitForm()" class="btn btn-info" data-dismiss="modal">Save</button>
                </div>
            </div>

        </div>
    </div>
</form>


<script>

    function saveEdit() {
        $("#select_master").empty();
        $("#PairedSelectBox  option").prop('selected', true);
        var options = $("#PairedSelectBox > option").clone();
        $("#select_master").append(options);
        $('#select_master > option').each(function () {
            $(this).addClass("master_" + $(this).attr('value'));
        });
        $("#select_master option:selected").removeAttr("selected");
        var val = $("#masterR").val();
        $(".master_" + val).prop('selected', true)
        $("#myModal").modal('show');
    }

</script>
