<script language="javascript">
    function toggleStatus(id, status, statusdiv) {
        $.ajax({
            url: "<?php echo base_url() ?>admin/users/ajaxstatus",
            data: {id: id, status: status, statusdiv: statusdiv},
            success: function (result) {
                window.location.reload();
            }
        });
    }
    function toggleblock(id, block, blockdiv) {
        $.ajax({
            url: "<?php echo base_url() ?>admin/users/ajaxblock",
            data: {id: id, block: block, blockdiv: blockdiv},
            success: function (result) {
                window.location.reload();
            }
        });
    }



    $(document).ready(function () {
        $("#deleteSel").click(function () {
            var chkCnt = $(".chk:checked").length;
            if (chkCnt == 0) {
                showMessageBox('Select atleast one item', 'danger');
                return false;
            }

            if (confirm('Are you sure to delete the selected user(s)?')) {
                $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/index"); ?>");
                $("#bulkaction_list").val('delete_list');
                $("#actions").val("delete");
                $("#userMasterForm").submit();
                return true;
            }

        });


// function to delete product 
        $("#bulkaction").change(function () {

            var chkCnt = $(".chk:checked").length;
            var action = $("#bulkaction").val();
            if (chkCnt == 0) {
                alert("Please select at least one user.!");
                return false;
            }
            else if (action == 'delete') {
                if (confirm('Are you sure to delete the selected user(s)?')) {
                    $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/bulkAction"); ?>");
                    $("#userMasterForm").submit();
                    return true;
                }
                else {
                    return false;
                }

            }
            else if (action == 'active' || action == 'inactive') {
                if (confirm('Are you sure to change status of the selected user(s)?')) {
                    $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/bulkAction"); ?>");
                    $("#userMasterForm").submit();
                    return true;
                }

                else {
                    return false;
                }

            }
            else {
                alert("Please specify any action.!");
                return false;
            }
        });

// filter function
        $("#filter_button").click(function () {
            $("#userMasterForm").submit();
            return true;
        });
// End : filter function

//check all
        $('#select_all').click(function () {
            if ($('#select_all').is(':checked'))
                $('.chk').prop('checked', true);
            else
                $('.chk').prop('checked', false);
        });


//check all
        $('.sortlink').click(
                function () {
                    var feild = $(this).attr('rel');
                    var title = $(this).attr('title');

                    $(this).removeAttr('title');
                    if (title == 'ASC') {
                        $(this).attr('title', 'DESC');
                    }
                    else {
                        $(this).attr('title', 'ASC');
                    }
                    $('#order_by_field').val(feild);
                    $('#order_by_value').val($(this).attr('title'));
                    $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/lists"); ?>");
                    $("#userMasterForm").submit();
                    return true;
                });
// END: check all

//Change Limit of pagination
        $(document).on('change', '#limit', function () {

            $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/lists"); ?>");
            $("#userMasterForm").submit();
            return true;
        });


        $('#btn_search').click(
                function () {
                    $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/lists"); ?>");
                    $("#userMasterForm").submit();
                    return true;
                });

        $(document).on('change', '#status', function () {
            $("#userMasterForm").attr("action", "<?php echo site_url("admin/restaurant_list/lists"); ?>");
            $("#userMasterForm").submit();
            return true;
        });
        // END: Change Limit of pagination

    });




</script>



<form name="userMasterForm" id="userMasterForm" method="post" action="">
    <input type="hidden" name="order_by_field" id="order_by_field" value="" />
    <input type="hidden" name="order_by_value" id="order_by_value" value="" />
    <input type="hidden" name="actions" id="actions" value="" />
    <div class="row" id="Title"></div>
    <div class="row form-group">
        <div class="col-offset-1 col-lg-3">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="glyphicon glyphicon-map-marker"></i> 
                </span>	
                <select name="bulkaction" id="bulkaction" class="form-control" >
                    <option value="" <?php if ($bulkaction == '') { ?> selected="selected"<?php } ?> >Bulk actions</option>
                    <option value="delete" <?php if ($bulkaction == 'delete') { ?> selected="selected"<?php } ?> >Delete</option>
                </select>

            </div><!-- /input-group -->
        </div>

        <div class="col-offset-1 col-lg-3">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="glyphicon glyphicon-map-marker"></i>      		</span>
                <select name="status" id="status" class="form-control" >
                    <option value="" <?php if ($status == '') { ?> selected="selected"<?php } ?> >Filter by Status</option>
                    <option value="Y" <?php if ($status == 'Y') { ?> selected="selected"<?php } ?> >Unblock</option>
                    <option value="N" <?php if ($status == 'N') { ?> selected="selected"<?php } ?> >Block</option>
                </select>
            </div>
            <!-- /input-group -->
        </div>


        <div class="col-lg-4">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Keyword Search" onFocus="if (this.value == 'Keywords')
                            this.value = ''" onBlur="if (this.value == '')
                                        this.value = ''" name="key" id="key" value="<?php
                       if ($key != '') {
                           echo $key;
                       } else {
                           echo '';
                       }
                       ?>">
                <span class="input-group-btn btn_search">
                    <button class="btn btn-info " id="btn_search" type="button">Go!</button>
                </span>
                <span class="input-group-btn btn_search">
                    <a href="javascript:" onclick="window.location.href = '<?php echo base_url() ?>index.php/admin/restaurant_list/lists'" class="blu_btn">
                        <button class="btn btn-default" type="button">Reset</button></a>
                </span>
            </div><!-- /input-group --> 	
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <a href="<?= base_url() ?>admin/chain/add"><button type="button"  class="btn btn btn-info "><b>Add New Chain</b></button></a>
            </div>
        </div>
    </div>
    <table class="table table-bordered table-hover" > 
        <thead>
            <tr  style="text-align:center">
                <th  style="text-align:center">Chain Name</th>
                <th style="text-align:center">Master Restaurant name</th>
                <th style="text-align:center">Number of restaurants</th>
                <th style="text-align:center">Description</th>
                <th style="text-align:center">Edit </th>
                <th style="text-align:center">Action</th>
            </tr> 
        </thead>
        <tbody>
            <?php
            if (sizeof($chainList) > 0) {
                foreach ($chainList as $list) {
                    ?>
                    <tr style="text-align:center">
                        <td>
                            <?= $list['name']; ?>
                        </td>

                        <td>    <?= $list['restaurant_name']." <br/> ".$list['formatted_address']; ?>             </td>

                        <td>  <?= $list['count']; ?>                        </td>

                        <td>
                            <?= $list['desc']; ?>
                        </td>
                        <td>
                            <button  onclick="window.location.href = '<?= base_url() ?>admin/chain/edit/<?= $list['id'] ?>'"  type="button">Edit</button> 
                        </td>
                        <td>
                            <a href="<?= base_url() ?>admin/chain/delete/<?= $list['id'] ?>">  <button type="button">Delete</button></a>
                        </td>
                    </tr>

                    <?php
                }
            } else {
                ?>

                <tr>
                    <td  colspan="8">No records...</td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
    <?php if (sizeof($userlist) > 0) { ?>       
        <div class="row" id="Table footer">
            <div class="col-lg-12 ">
                <div class="col-offset-1 col-lg-2 pull-right">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-map-marker"></i> 
                        </span>
                        <select name="limit" id="limit" class="form-control" >
                            <option value="5" <?php if ($limit == 5) { ?> selected="selected"<?php } ?> >5</option>
                            <option value="10" <?php if ($limit == 10) { ?> selected="selected"<?php } ?> >10</option>
                            <option value="20" <?php if ($limit == 20) { ?> selected="selected"<?php } ?> >20</option>
                            <option value="50" <?php if ($limit == 50) { ?> selected="selected"<?php } ?>>50</option>
                            <option value="100" <?php if ($limit == 100) { ?> selected="selected"<?php } ?>>100</option>
                            <option value="all" <?php if ($limit == 'all') { ?> selected="selected"<?php } ?>>ALL</option>
                        </select>
                    </div><!-- /input-group -->
                </div>

                <div class="col-offset-1 col-lg-10">
                    <div class="input-group">
                        <ul class="pagination pull-right" style="margin:0px;">
                            <?php echo $this->pagination->create_links(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</form>


<script>
    $(document).ready(function () {
        $('.full_link').click(function () {
            window.location = $(this).attr('href');
            return false;
        });
    });
// BLock Unblock
    $('.block').click(function () {

        var restaurant_id = $(this).data('id');
        var selector = '#' + 'block_' + restaurant_id + " " + 'img';
        var imgsrc = $(selector).attr('src');
        var status = $(this).data('block');
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/restaurant_list/ajaxblock'); ?>",
            data: {is_block: status, id: restaurant_id},
            cache: false,
            success: function (res) {
                window.location.reload();
                if (res == 'Y') {
                    $this.data('block', 'Y');
                    $(selector).attr('src', "<?php echo base_url() ?>assets/images/unblock.png");

                }
                else if (res == 'N') {
                    $this.data('block', 'N');
                    $(selector).attr('src', "<?php echo base_url() ?>assets/images/block.png");

                }
            }
        });
    }); //END: BLock Unblock
    function toggleblock(id, block, blockdiv) {

        if (block == 'N') {
            var cofrm = confirm('Are you sure you want to Unblock ?');
        }
        else {
            var cofrm = confirm('Are you sure you want to Block ?');
        }
        if (cofrm == true) {
            $.ajax({type: "get",
                url: "<?php echo base_url() ?>admin/restaurant_list/ajaxblock",
                data: {id: id, block: block, blockdiv: blockdiv},
                success: function (result) {
                    //alert(result);return false;
                    window.location.reload();

                }

            });
        }
    }

</script>

