
<script>

	function submitForm(){
		$("#frmPack").submit();
	 }
 
 
		 
	function cancel()
	{
	
	window.location.href = "<?php echo base_url()?>admin/home";
	}
</script>


  <div class="container">
   
  <?php $this->load->library('session');
 
  
  ?>
  
  	 <div class="row" id="Title">
     <div class="col-lg-12"><legend>Preferences Details</legend></div>

 	 </div>
	 
	 
	 <div class="row form-group ">
	 
	
  
  <form  method="post" name="formlist"  id="formlist" action=""  onsubmit="">
 
	 <input type="hidden" name="admin_id" value="<?php echo $pref[0]->admin_id; ?>" />
   <fieldset>
   
	   <div class="row form-group">  
          <div class="col-lg-6">
          <label>Name</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->username; ?>" class="form-control" name="username" id="<?php echo $pref[0]->username; ?>"  value="<?php echo $pref[0]->username; ?>">
          </div>
        </div>
           
          <div class="col-lg-6">
          <label>Password</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->password; ?>" class="form-control" name="password" id="<?php echo $pref[0]->password; ?>"  value="<?php echo $pref[0]->password; ?>">
          </div>
        </div>
            </div>
            
               <div class="row form-group">  
          <div class="col-lg-6">
          <label>Email</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->email; ?>" class="form-control" name="email" id="<?php echo $pref[0]->email; ?>"  value="<?php echo $pref[0]->email; ?>">
          </div>
        </div>
           
          <div class="col-lg-6">
          <label>Stripe private key</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->stripe_private_key; ?>" class="form-control" name="stripe_private_key" id="<?php echo $pref[0]->stripe_private_key; ?>"  value="<?php echo $pref[0]->stripe_private_key; ?>">
          </div>
        </div>
            </div>
            
               <div class="row form-group">  
          <div class="col-lg-6">
          <label>Stripe public key</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->stripe_public_key; ?>" class="form-control" name="stripe_public_key" id="<?php echo $pref[0]->stripe_public_key; ?>"  value="<?php echo $pref[0]->stripe_public_key; ?>">
          </div>
        </div>
            
          <div class="col-lg-6">
          <label>Monthly subscriber Fee to Merchants</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->month_subscriber; ?>" class="form-control" name="month_subscriber" id="<?php echo $pref[0]->month_subscriber; ?>"  value="<?php echo $pref[0]->month_subscriber; ?>">
          </div>
        </div>
            </div>
               <div class="row form-group">  
          <div class="col-lg-6">
          <label>Average order</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->average_order; ?>" class="form-control" name="average_order" id="<?php echo $pref[0]->average_order; ?>"  value="<?php echo $pref[0]->average_order; ?>">
          </div>
        </div>
        
        
          <div class="col-lg-6">
          <label>Google id</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            <input type="text" placeholder="<?php echo $pref[0]->google_id; ?>" class="form-control" name="google_id" id="<?php echo $pref[0]->google_id; ?>"  value="<?php echo $pref[0]->google_id; ?>">
          </div>
        </div>
            </div>
		
        
        
        
        
        
        
         <?php $i=0; foreach($pref_general_config as $pref){?>
		<?php if ($i==0){?>    <div class="row form-group">  <?php }$i++; ?>
     
        
          <div class="col-lg-6">
          <label><?php echo $pref['title']; ?></label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
            
            <?php if($pref['value']=='Y' || $pref['value']=='N'){ ?>
             <select name="<?php echo $pref['field']; ?>" id="<?php echo $pref['field']; ?>" class="form-control">
              <option value="<?php echo $pref['value']; ?>"><?php echo $pref['value']; ?></option>
              <?php if($pref['value']!='Y'){ ?><option value="Y">Y</option><?php } ?>
               <?php if($pref['value']!='N'){ ?><option value="N">N</option><?php } ?>
            </select> 
            <?php } else {?>
            <input type="text" placeholder="<?php echo $pref['title']; ?>" class="form-control" name="<?php echo $pref['field']; ?>" id="<?php echo $pref['field']; ?>"  value="<?php echo $pref['value']; ?>">
            
            <?php } ?>
          </div>
        </div>
      
        
        
         <?php if ($i==2){ $i=0;?>   </div> <?php } ?> 
		
		 <?php } ?>
        
        
           
        
        
        
        

        
 <div class="row form-group"></div>
   <div class="row form-group">
  <div class="col-lg-3"></div>
        <div class="col-lg-3 col-offset-3"><button class="btn btn-info btn-group-justified">Submit</button></div>
        <div class="col-lg-3"><button class="btn btn-default btn-group-justified" type="button"  onclick="cancel();">Cancel</button></div>
         <div class="col-lg-3"></div>
      </div>
    </fieldset>
  </form>


</div>