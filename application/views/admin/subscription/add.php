

<link href="<?php echo base_url() ?>assets/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>


<style>
.admin_body{
	background-color:#FFFFFF;
}
.form-group{
	margin-right:0px!important;
}
.ui-autocomplete { 
            cursor:pointer; 
            height:120px; 
            overflow-y:scroll;
        }  
.ui-corner-all{ border-radius:0px!important;}
.ui-menu-item{  width:100% !important; padding:0px !important; border-bottom:1px solid#EBEAEA;  }
a.ui-corner-all{ width:100% !important; display:block; padding:2px; margin:0;}
a.ui-state-focus{ background:#DF7707 !important; width:100% !important; padding:2px !important; margin:0px; border:1px solid#DF7707 !important; color:#FFF !important; border-radius:0px !important; }
.errmsg
{
color: red;
display:inline-block;
}
</style>

 <!-- Load jQuery and bootstrap datepicker scripts -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
$(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update', new Date());;
});
</script>
<style>
.brudcum_head{
	border-bottom:1px solid #f2f2f2;
	padding:5px;
}
</style>
    <?php /*?>    <div class="col-lg-12 brudcum_head"  >
                  <span> Basic Details</span>
        </div> <?php */?>

 <div class="container" style="padding-bottom:10px;"> 
	 <div class="clearfix" style="padding-top:15px;"></div>
     	
       
        <div class="col-md-12" style="padding-bottom:10px;">
    
    
    	
    
		<form class="form-horizontal" role="form"  method="post" name="formlist"  id="formlist">
        	<fieldset>
       		<!-- Text input-->
       		<!-- Text input-->
            <!-- Text input-->
            <!-- Text input-->
            <!-- Text input-->
        
            
            <div class="form-group">
          <span id="errmsguses" class="errmsg"></span>
            <label class="col-sm-4 " for="textinput"><span id="docs-internal-guid-3974d623-1124-4c33-9627-f6aceaa1d587">Package Name</span></label>
            <div class="col-sm-6">
            <input type="text" name="package_name"  placeholder="Package Name"   class="form-control fileldtheme js-placeholder"  id="package_name"  <?php if($sub_detail->sub_package_id){?>value="<?php echo $sub_detail->package_name;  ?>" <?php } ?>>
            <span id="errmsguses" class="errmsg"></span>
            </div>
	  </div>
 		
      
      	
     	
            <div class="form-group">
          <span id="errmsguses" class="errmsg"></span>
            <label class="col-sm-4 " for="textinput">Discount (%)</label><div class="col-sm-6">
            <input type="text" name="discount"  placeholder="Discount"   class="form-control fileldtheme js-placeholder"  id="discount" <?php if($sub_detail->sub_package_id){?>value="<?php echo $sub_detail->discount ;  ?>" <?php } ?>>
            <span id="errmsguses" class="errmsg"></span>
            </div>
	  </div>
       <div class="form-group">
          <span id="errmsguses" class="errmsg"></span>
            <label class="col-sm-4 " for="textinput">Number Of Restaurants</label><div class="col-sm-6">
            <input type="text" name="number_of_restaurants"  placeholder="Number Of Restaurants"   class="form-control fileldtheme js-placeholder"  id="number_of_restaurants" <?php if($sub_detail->sub_package_id){?>value="<?php echo $sub_detail->number_of_restaurants ;  ?>" <?php } ?>>
            <span id="errmsguses" class="errmsg"></span>
            </div>
	  </div>
      
      	
       <div class="form-group">
            <label class="col-sm-4 " for="textinput">User Fee</label><div class="col-sm-6">
            <input type="text" name="user_fee"  class="form-control fileldtheme js-placeholder"  id="user_fee" <?php if($sub_detail->sub_package_id){?>value="<?php echo $sub_detail->user_fee ;  ?>" <?php } ?>>
            <span id="errmsgdiscount1" class="errmsg"></span>
            </div>
	  </div>
      <div class="form-group">
          <span id="errmsguses" class="errmsg"></span>
            <label class="col-sm-4 " for="textinput"><span id="docs-internal-guid-3974d623-1124-4c33-9627-f6aceaa1d587">Duration</span></label>
            <div class="col-sm-2">
            <input type="text" name="duration"  placeholder=""   class="form-control fileldtheme js-placeholder"  id="duration" <?php if($sub_detail->sub_package_id){?>value="<?php echo $sub_detail->duration ;  ?>" <?php } ?>>
            <span id="errmsguses" class="errmsg"></span>
            </div>
            
            
            <label class="col-sm-2 " for="textinput"><span id="docs-internal-guid-3974d623-1124-4c33-9627-f6aceaa1d587">Frequency</span></label>
            <div class="col-sm-2">
            <input type="hidden" <?php if($sub_detail->sub_package_id){?>value="<?php echo $sub_detail->sub_package_id ;  ?>" <?php } ?> name="sub_id" id="sub_id"/>
                           <select name="frequency" id="frequency">
                   <?php if($sub_detail->sub_package_id){?> <option value="<?php echo $sub_detail->frequency;  ?>"><?php echo $sub_detail->frequency ;  ?></option> <?php } ?>
                              <option value="Days">Day(s)</option>
                              <option value="Months">Month(s)</option>
                              <option value="Years">Year(s)</option>
                            </select> 
            <span id="errmsguses" class="errmsg"></span>
            </div>
	  </div>
      
       <div class="form-group">
          <span id="errmsguses" class="errmsg"></span>
            <label class="col-sm-4 " for="textinput"><span id="docs-internal-guid-3974d623-1124-4c33-9627-f6aceaa1d587">Show In Calculator</span></label>
            <div class="col-sm-6">
          <input type="checkbox" id="checkbx"   <?php if($sub_detail->show_web=='Y'){?>checked<?php }?>/>
          <input type="hidden" name="active" id="active"/>
            <span id="errmsguses" class="errmsg"></span>
            </div>
	  </div>
      
      <div class="col-sm-10">  
        
          <button type="button" class="btn btn-default pull-right" onclick="location.href='<?php echo base_url()?>index.php/admin/preference/lists_package'">Cancel</button>
          <button type="button" class="btn btn-info pull-right" name="submit_btn" id="submit_btn" style="margin-right:10px;" onclick="validate();" >Save</button>
       </div>  
     
        </fieldset>
      </form>
      
    
    </div><!-- /.col-lg-12 -->	
    
      
	  	
	</div><!-- /.row -->
        
</div><!-- /.container -->



<script>

		
function validate(){
	var checkbx = document.getElementById("checkbx").checked;
				if(checkbx==false)
				{
				var active='N';
				}
				else
				{
				var active='Y';
				}
	document.getElementById("active").value = active;			
	var sub_id=document.getElementById('sub_id').value;
	var package_name=$("#package_name").val();
	var discount=$("#discount").val();
	var number_of_restaurants=$("#number_of_restaurants").val();
	var user_fee=$("#user_fee").val();
	var duration=$("#duration").val();
	var e = document.getElementById("frequency");
    var frequency = e.options[e.selectedIndex].value;
	


	if (package_name == '')
	{
		alert('Please provide a package name!');
		$("#package_name").focus();
		return false;
	}
	if (discount == '')
	{
		alert('Please provide discount !');
		$("#discount").focus();
		return false;
	}
	if (number_of_restaurants == '')
	{
		alert('Please enter  number of restaurants');
		$("#number_of_restaurants").focus();
		return false;
	}
	if (user_fee == '')
	{
		alert('Please enter user fee!');
		$("#user_fee").focus();
		return false;
	}
	if (duration == '')
	{
		alert('Please enter duration!');
		$("#package_name").focus();
		return false;
	}
	if (frequency == '')
	{
		alert('Please select frequency!');
		$("#frequency").focus();
		return false;
	}
	
	else
	{
	
	$.ajax({
			type:"post",
			url:"<?php echo base_url(); ?>admin/preference/check_exist",
			data:{frequency:frequency,duration:duration,sub_id:sub_id,number_of_restaurants:number_of_restaurants,active:active},
			success:function(data){
				
				if(data=='sry')
				{
				alert('Please Change Frequency or Duration!');
				return false;
				}
				else
				{
				$("#formlist").submit();return true;
				}
				
			}
			});
			
			
    }			
	}	
		
		 
	$("#promo").click(function(){

			$.ajax({
			type:"post",
			url:"<?php echo base_url(); ?>admin/promocodes/generatePromo",
			data:{},
			success:function(data){
				//alert(data);
				$("#promocode").val(data)
			}
			});
			   
			    
		}) 
	$(document).ready(function () {
  //called when key is pressed in textbox
  $("#uses").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsguses").html("Digits Only").show().fadeOut("slow");
	   document.getElementById("errmsguses1").textContent="Digits Only";
               return false;
    }
   });
   //called when key is pressed in textbox
  $("#discountamt").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsgdiscount").html("Digits Only").show().fadeOut("slow");
		document.getElementById("errmsgdiscount").textContent="Digits Only";
               return false;
    }
   });
});


 $('#discount,#user_fee').keypress(function(event) {

     if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
          return true;

     else if((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57))
          event.preventDefault();

});

function check_exist(){
			var promo=$("#promocode").val();
			var restaurant_id=$("#restaurant_id").val();
			//alert(restaurant_id);
			if(promo!=''){
			$.ajax({
			type:"post",
			url:"<?php echo base_url(); ?>admin/promocodes/check_exist",
			data:{'promocode':promo,'restaurant_id':restaurant_id},
			success:function(data){
				if(data > 0){
				$("#promocode_err").show();
				$("#promocode").addClass('errorborder');	
				$("#promocode_err").text("This promocode already exist...");
				}
				else{
				$("#promocode_err").hide();
				$("#promocode").removeClass('errorborder');	
				}
			}
			});
		
			}
			   
		}
</script>
	


