<style>
.fa.fa-gear {
    color: #a2a2a2;
    font-size: 23px;
}
</style>
<script language="javascript">

//Change Limit of pagination
	$(document).on('change', '#limit', function() {
			$("#userMasterForm").attr("action", "<?php echo base_url().$this->user->root;?>/location/lists");
				$("#userMasterForm").submit();return true;
	});	
	
	$('#btn_search').click(
		function(){	
		
			$("#userMasterForm").attr("action", "<?php echo base_url().$this->user->root;?>/location/lists");
				$("#userMasterForm").submit();return true;
	});
	
  
	
	$(document).on('change', '#status', function() {
			$("#userMasterForm").attr("action", "<?php echo base_url().$this->user->root;?>/location/lists");
			$("#userMasterForm").submit();return true;	
	});
	// END: Change Limit of pagination
	

</script>

<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css">
    <?php if($this->session->flashdata('error_message')!=''){ ?>
 		<div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
    <?php }else{ ?>
 		<div class="alert alert-danger" role="alert" style="display:none;"></div>
    <?php } ?>
    <?php if($this->session->flashdata('success_message')!=''){ ?>
 		<div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
    <?php }else{ ?>
 		<div class="alert alert-success" role="alert" style="display:none;"></div>
    <?php } ?>
    
<form name="userMasterForm" id="userMasterForm" action="<?php echo base_url().$this->user->root;?>/location/lists" method="post" >   

<div  class="tab_wrper" >
  <ul role="tablist" class="nav nav-tabs tab_links" id="myTabs"><li style="width:90%;">
       <li class="active tog_tab" role="presentation">
     	 <a aria-expanded="true" aria-controls="category" role="tab" id="" href="<?php echo base_url().$this->user->root;?>//preference/lists_package">Subscription Package</a>      </li>
        <div class="col-lg-1 pull-right"><a href="<?php echo base_url().$this->user->root;?>/preference/add" class="btn btn-info">Add New</a></div>
      </li>
    </ul>
    
    
 
	<div class="tab-content tab_contwp dish_cat_tab" id="myTabContent">                
   	  <div aria-labelledby="category-tab" id="category" class="" role="tabpanel">           
		<div class="table-responsive"> 
    <table class="table table-striped tbl_category">
                  <thead class="head_table">
                  <tr>
                      <th width="101" class="col-md-2 col-sm-2">Package Name</th>
                    <th width="101" class="col-md-2 col-sm-2">Duration</th>
              <!--  <th class="col-md-2 col-sm-2">Valid From</th>-->
                      <th width="88" class="col-md-2 col-sm-2">Number Of Restaurants</th>
                    <th width="39" class="col-md-2 col-sm-2">User Fee</th>
                    <th width="43" class="col-md-1 col-sm-2">Active</th>
                     <th width="43" class="col-md-1 col-sm-2">Delete</th>
                    </tr>
                  </thead>
                  <tbody class="table_body">
                    
                    
                    <?php 
                    $user = $this->session->userdata('user');
                    $admin_id=$user->admin_id; 
                    $username=$user->username; 
                   if(count($subscription)!=0){
                    foreach($subscription as $promo){ ?>
                    <tr id="row_<?php echo $promo->sub_package_id;?>">
                     
                       <td  href="<?php echo base_url().$this->user->root;?>/preference/add/<?php echo $promo->sub_package_id;?>" style="cursor:pointer;" class="full_link"  >
                        <?php echo $promo->package_name;?>
                       </td>
                       <td  href="<?php echo base_url().$this->user->root;?>/preference/add/<?php echo $promo->sub_package_id;?>"  style="cursor:pointer;" class="full_link">           
                        <?php echo $promo->duration;  echo "  ".$promo->frequency; ?>
                       </td>
                        <td   href="<?php echo base_url().$this->user->root;?>/preference/add/<?php echo $promo->sub_package_id;?>" style="cursor:pointer;" class="full_link">
                        <?php echo $promo->number_of_restaurants;?>
                       </td>
                       
                    
                       <td   href="<?php echo base_url().$this->user->root;?>/preference/add/<?php echo $promo->sub_package_id;?>" style="cursor:pointer;" class="full_link">
                        <?php echo $promo->user_fee;?>
                       </td>
                       
                      
                      <td width="160" >
                        
                     
                     <a class="block" data-id="<?php echo $promo->sub_package_id;?>" data-block="<?php echo $promo->active;?>" id="block_<?php echo $promo->sub_package_id; ?>" href="javascript: void(0)">	  <?php if($promo->active == 'N'): ?><img src="<?php echo base_url() ?>assets/images/block.png" alt="" class="tmg25">
                <?php else: ?><img src="<?php echo base_url() ?>assets/images/unblock.png" alt="" class="tmg25"><?php endif; ?>
                 </a>                       </td>
                 
                 
                 
                  <td>
                            <center>
                                <a href="<?php echo base_url().$this->user->root;?>/preference/delete/<?php echo $promo->sub_package_id;?>?limit=<?php echo $limit;?>&per_page=<?php echo $_REQUEST['per_page'];?>" onclick="return confirm('Are you sure you want to delete?');"><i class="glyphicon glyphicon-trash" title="Delete"></i></a>                            </center>
                        </td>
                       
                    </tr>
                  
                  <?php 	}
                        }else { ?>
                        
                      <tr>
                      <td colspan="7">
                      No Restaurants...
                      </td>
                      </tr>
                   <?php } ?>
                    
                  </tbody>
                </table>
     		</div>
   		</div>
   </div>
       <?php if(count($locationlist)!=0){?>
       <div class="row" id="Table footer">
    <div class="col-lg-12 ">
		<div class="col-offset-1 col-lg-2 pull-right">
			<div class="input-group">
			  <span class="input-group-addon">
				<i class="glyphicon glyphicon-map-marker"></i> 
			  </span>
			  <select name="limit" id="limit" class="form-control" >
					<option value="5" <?php if($limit == 5){?> selected="selected"<?php } ?> >5</option>
					<option value="10" <?php if($limit == 10){?> selected="selected"<?php } ?> >10</option>
					<option value="20" <?php if($limit == 20){?> selected="selected"<?php } ?> >20</option>
					<option value="50" <?php if($limit == 50){?> selected="selected"<?php } ?>>50</option>
					<option value="100" <?php if($limit == 100){?> selected="selected"<?php } ?>>100</option>
					<option value="all" <?php if($limit == 'all'){?> selected="selected"<?php } ?>>ALL</option>
				</select>
			</div><!-- /input-group -->
		</div>
		
		<div class="col-offset-1 col-lg-10">
				<div class="input-group">
					<ul class="pagination pull-right" style="margin:0px;">
					<?php echo $this->pagination->create_links(); ?>
					</ul>
				</div>
		</div>
    </div>
  </div>
  <?php } ?>
  
</div>
</form>


<script>
  $(document).ready(function(){
    $('.full_link').click(function(){
        window.location = $(this).attr('href');
        return false;
    });
	
	$('.config_link').click(function(){
        window.location = $(this).attr('href');
        return false;
    });
	
  });


function restaurant_status(location_id,is_closed){
        
		var status=$('.restaurant_status'+location_id).attr('data-val'); 
		if($('.restaurant_status'+location_id).attr('data-val')=='Y')
			$('.restaurant_status'+location_id).attr('data-val','N');
		else
			$('.restaurant_status'+location_id).attr('data-val','Y');
			//alert(sta);
		$.ajax({
			
				type:"post",
				url:"<?php echo base_url().$this->user->root;?>/location/restaurant_status",
				data:{'location_id':location_id,'status':status},
				success:function(data){
				
					return true;
				}
			
			});
 }

		
		
		
	
	
	
	$('.block').click(function(){
		var member_id = $(this).data('id');
		var selector = '#' + 'block_' + member_id + " " + 'img';
		var imgsrc = $(selector).attr('src');       
		var status = $(this).data('block');
		var $this  = $(this);
		$.ajax({
            type : "POST",
            url  : "<?php echo base_url().$this->user->root;?>/preference/ajaxblock",
            data : {is_block: status, id:member_id}, 
            cache : false,
            success : function(res) {
			window.location.reload();
				if(res=='Y'){
				  	 $this.data('block','Y');
				 	 $(selector).attr('src',"<?php echo base_url() ?>assets/images/unblock.png");
					
				}
				else if(res=='N'){
					$this.data('block','N');
				 	$(selector).attr('src',"<?php echo base_url() ?>assets/images/block.png");
					
				}
            }
         });  
	});
</script>
	  

<style>
.nav-tabs {
    border-bottom: 11px solid #ffffff!important;
}
.nav-tabs > li {
    margin-left: -1px!important;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #fff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555;
    cursor: default;
}
</style>