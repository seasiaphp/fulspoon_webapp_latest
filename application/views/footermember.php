<footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
                        <div class="col-md-offset-2 col-md-6" style="padding:0;"> 
                            <ul class="footer-nav foter_user" style="float:left !important;">                                               
                                <li><a href="<?= base_url() ?>">Home</a> </li>    
                                <li><a href="<?= base_url() ?>member/terms_condition">Terms & Conditions</a> </li>
<!--                                <li><a href="<?= base_url() ?>member/faq">FAQ</a> </li>-->
                                <li><a href="<?= base_url() ?>member/contact">Contact Us</a> </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12"><p class="topline">&copy; <?= date("Y") ?> Fulspoon. All rights reserved. </p></div>

                    </div>
                    <!-- .row -->
                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>

    </body>
<script>

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-73692716-1', 'auto');
ga('send', 'pageview');


</script>
</html>