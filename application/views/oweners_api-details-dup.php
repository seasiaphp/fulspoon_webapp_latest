<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Replimatic | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Replimatic">
        <meta name="author" content="Newagesmb">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/form.css" media="screen">


        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body style="background:#f4f4f4 !important;">

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top green-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="owners_index.html"><img src="<?= base_url() ?>images/replimatic_logo_white.png" alt=""/></a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Stat Search -->
                            <!-- Stat Search -->
                            <div class="buttons-side">
                                <a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>
                                <a class="btn btn-default btnlogin" href="#" role="button">SIGN UP</a>
                            </div>
                            <!-- End Search -->
                            <!-- End Search -->
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li> <a style="color:#fff;">Call Us: 877-805-1234</a></li>             
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li><a>Call Us: 877-805-1234</a></li>
                        <li><a href="index-01.html">LOGIN</a></li>
                        <li><a href="#">SIGN UP</a></li></ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <!-- Start Content -->
            <div id="content">
                <?php
                foreach ($response as $goog) {
// echo "<pre>";    print_r($goog->google_place_result->photos);exit;
                    ?>
                    <div class="container bg_white">

                        <div class="col-lg-6 pading10">
                            <div class="slider">


                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <?php
                                        $i = 0;
                                        foreach ($goog->google_place_result->photos as $img) {
                                            $image = $img->photo_reference;
                                            ?>
                                            <div class="item <?php if ($i == 0) { ?> active <?php
                                                $i++;
                                            }
                                            ?>">
                                                <img style="max-height: 300px; min-height: 300px; width: 100%" id='image_item' src='https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&maxheight=500&photoreference=<?= $image ?>&key=<?= $key1 ?>'  >  
                                                 <!--<img src="<?= base_url() ?>images/slider-thumb.png" alt="...">-->

                                            </div><?php } ?>
                                        <!--                                        <div class="item">
                                                                                    <img src="<?= base_url() ?>images/slider-thumb.png" alt="...">
                                                                                </div>-->
                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>


                            </div>
                        </div>

                        <div class="col-lg-6 pading10">
                            <div id="map"  style="max-height: 300px; min-height: 300px; width: 100%" class="col-lg-6">
    <!--                                            <iframe  width="600" height="450"  frameborder="0" style="border:0"
                                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBXJ2W_SuSVzyp3RHm07GYePcUBQ85OaUY&q=<?= $goog->name ?>,<?= $goog->vicinity ?>" allowfullscreen></iframe>-->

                                <script>
                                    var map;
                                    function initMap() {
                                        map = new google.maps.Map(document.getElementById('map'), {
                                            center: {lat: <?= $goog->google_place_result->geometry->location->lat ?>, lng: <?= $goog->google_place_result->geometry->location->lng ?>},
                                            zoom: 8
                                        });
                                    }
                                </script>
                                <script async defer
                                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4cJ5KgPVTpaZR2q9J5InHKilquV1rXDc&callback=initMap">
                                </script>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="listwp">
                            <h4 class="labelhead">Google API results for <span class="color_green"><?= $goog->name ?></span></h4>

                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-7 labelitem">NAME</label>
                                <div class="col-sm-5 listitem">
                                    <?= $goog->name ?>
                                </div>
                            </div>
                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-5 labelitem">Place_id</label>
                                <div class="col-sm-7 listitem">
                                    <?= $goog->place_id; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-12">
                                <label  class="col-sm-3 labelitem">Working Hours</label>
                                <div class="col-sm-9 listitem">
                                    <?php
                                    foreach ($goog->google_place_result->opening_hours->weekday_text as $weekday) {
//                                        echo $weekday;
                                        $week_array = explode(":", $weekday);
//                                        print_r($week_array);
                                        $si_arr = sizeof($week_array);
                                        for ($i = 1; $i < $si_arr; $i++) {
                                            $time.=$week_array[$i];
                                            if ($si_arr - $i > 1) {
                                                $time.=":";
                                            }
                                        }
                                        ?>  
                                        <div class="text-center col-lg-3 col-sm-4 col-md-4 mgbtm10">
                                            <?= $week_array[0]; ?> : <br>
                                            <span class="color_green"><?= $time; ?></span>
                                        </div>
                                        <?php
                                        unset($time);
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-7 labelitem">InterNational Phone Number</label>
                                <div class="col-sm-5 listitem">
                                    <?= $goog->google_place_result->international_phone_number; ?>
                                </div>
                            </div> 
                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-5 labelitem">Review Rating</label>
                                <div class="col-sm-7 listitem">
                                    <?php
                                    $rating = round($goog->rating);
                                    if ($rating != '') {
                                        $non_rate = 5 - $rating;
                                        for ($i = 1; $i <= $rating; $i++) {
                                            ?>
                                            <li><img src="<?= base_url() ?>images/star.png"></li>
                                            <?php
                                        }
                                        for ($i = 1; $i <= $non_rate; $i++) {
                                            ?>
                                            <li><img src="<?= base_url() ?>images/star00.png"></li>
                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-4 labelitem">Price Rating</label>
                                <div class="col-sm-8 listitem">

                                    <?php
                                    $price_rating = round($goog->price_level);
                                    $p_rate = 4 - $price_rating;
                                    if ($price_rating != '') {
                                        for ($i = 1; $i <= $price_rating; $i++) {
                                            ?>
                                            <li><img src="<?= base_url() ?>images/star.png"></li>
                                            <?php
                                        }
                                        for ($i = 1; $i <= $p_rate; $i++) {
                                            ?>
                                            <li><img src="<?= base_url() ?>images/star00.png"></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-5 labelitem">User Rating Total</label>
                                <div class="col-sm-7 listitem">
                                    <?= $goog->google_place_result->user_ratings_total; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-4 labelitem">Website</label>
                                <div class="col-sm-8 listitem color_green">
                                    <?= $goog->google_place_result->website ?>
                                </div>
                            </div>
                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-5 labelitem">Map Url</label>
                                <div class="col-sm-7 listitem color_green">
                                    <?= $goog->google_place_result->url ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-4 labelitem">types</label>
                                <div class="col-sm-8 listitem color_green">
                                    <?php
                                    foreach ($goog->google_place_result->types as $typ) {
                                        echo $typ . ",";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-5 labelitem">Address</label>
                                <div class="col-sm-7 listitem">
                                    <?= $goog->vicinity ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6 col-sm-6 col-md-6">
                                <label  class="col-sm-4 labelitem">Open Now</label>
                                <div class="col-sm-8 listitem">
                                    <div class="radio">
                                        <input id="male" type="radio" name="gender" value="male">
                                        <label for="male">Independent Drivers</label>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-4 labelitem"></label>
                                <div class="col-sm-6 listitem">
                                    <button class="btn btngreen btn-block">CLICK HERE FOR FULL JSON</button>
                                </div>
                            </div>



                            <div class="clearfix"></div>
                        </div>

                        <div class="listwp">
                            <h4 class="labelhead">Locu API results for <span class="color_green"><?= $goog->name; ?></span></h4>
                            <?php
                            $locu = $goog->locu_data;
//                            echo "<pre>";
//                            print_r($locu);
//                            exit
                            ?>
                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">NAME</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" value="<?= $locu->name; ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Locality</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" placeholder="Type Value" value="<?= $locu->locality ?>" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Street_address</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" value="<?= $locu->street_address ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Postal_code</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" value="<?= $locu->postal_code ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Country</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" placeholder="Type Value" value="<?= $locu->country ?>" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Phone</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" value="<?= $locu->phone ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Website</label>
                                <div class="col-sm-6 listitem">
                                    <input type="email" value="<?= $locu->website_url ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Has menu</label>
                                <div class="col-sm-6 listitem">
                                    <input type="email" value="<?php
                                    if ($locu->has_menu == 1) {
                                        echo "yes";
                                    } else {
                                        echo "no";
                                    }
                                    ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">categories</label>
                                <div class="col-sm-6 listitem">
                                    <input type="text" value="<?= $locu->categories[1] ?>" placeholder="Type Value" id="exampleInputNname" class="form-control inputsignup">
                                </div>
                            </div>
                            <?php
                            $extend = $locu->locu_extend->venues[0];
                            ?>
                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Delivery</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <?php
                                        if ($extend->delivery->will_deliver == 1) {
//                                                        echo "yes";
                                            ?>
                                            <input id="yes" type="radio"  checked="checked" name="Delivery" value="yes">
                                            <label for="yes">YES</label>
                                            <input id="NO" type="radio" name="Delivery" value="no">
                                            <label for="NO">NO</label>
                                        <?php } else {
                                            ?>
                                            <input id="yes" type="radio" name="Delivery" value="yes">
                                            <label for="yes">YES</label>
                                            <input id="NO" type="radio"  checked="checked" name="Delivery" value="no">
                                            <label for="NO">NO</label>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Alcohol</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <?php if ($extend->extended->alcohol == "beer_and_wine") { ?>
                                            <select name="Alcohol" style="color:#262626;">
                                                <option selected>beer_and_wine</option>
                                                <option>full_bar</option>
                                                <option>no_alcohol</option>
                                            </select>
                                        <?php } elseif ($extend->extended->alcohol == "full_bar") { ?>
                                            <select  name="Alcohol" style="color:#262626;">
                                                <option>beer_and_wine</option>
                                                <option selected>full_bar</option>
                                                <option>no_alcohol</option>
                                            </select>
                                        <?php } else { ?>
                                            <select  name="Alcohol" style="color:#262626;">
                                                <option>beer_and_wine</option>
                                                <option>full_bar</option>
                                                <option selected>no_alcohol</option>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">Parking</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <select name="Parking" style="color:#262626;">                                    
                                            <?php if ($extend->extended->parking == "garage") { ?>  
                                                <option selected>garage</option>
                                                <option>lot</option>
                                                <option>street</option>
                                                <option>valet</option>
                                                <option>validated</option>
                                            <?php } elseif ($extend->extended->parking == "lot") { ?>  
                                                <option>garage</option>
                                                <option selected>lot</option>
                                                <option>street</option>
                                                <option>valet</option>
                                                <option>validated</option>
                                            <?php } elseif ($extend->extended->parking == "valet") { ?>  
                                                <option>garage</option>
                                                <option>lot</option>
                                                <option>street</option>
                                                <option selected>valet</option>
                                                <option>validated</option>
                                            <?php } elseif ($extend->extended->parking == "validated") { ?>  
                                                <option>garage</option>
                                                <option>lot</option>
                                                <option>street</option>
                                                <option>valet</option>
                                                <option selected>validated</option>
                                            <?php } else { ?>  
                                                <option>garage</option>
                                                <option selected>lot</option>
                                                <option>street</option>
                                                <option>valet</option>
                                                <option>validated</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">wifi</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <select name="wifi" style="color:#262626;">
                                            <?php if ($extend->extended->wifi == "free") { ?>
                                                <option selected>free</option>
                                                <option>paid</option>
                                                <option>no</option>
                                            <?php } else if ($extend->extended->wifi == "paid") { ?>
                                                <option>free</option>
                                                <option selected>paid</option>
                                                <option>no</option>
                                            <?php } else { ?>
                                                <option>free</option>
                                                <option>paid</option>
                                                <option selected>no</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">corkage</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <select name="corkage" style="color:#262626;">
                                            <?php if ($extend->extended->corkage == "free") { ?>
                                                <option selected>free</option>
                                                <option>paid</option>
                                                <option>no</option>
                                            <?php } elseif ($extend->extended->corkage == "paid") { ?>
                                                <option>free</option>
                                                <option selected>paid</option>
                                                <option>no</option>
                                            <?php } else { ?>
                                                <option>free</option>
                                                <option>paid</option>
                                                <option selected>no</option> <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">dietary_restrictions</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <select style="color:#262626;">
                                            <option>SELECT</option>
                                            <option>SELECT</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">music</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <select name="music" style="color:#262626;">
                                            <?php if ($extend->extended->music == "dj") { ?>
                                                <option selected>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background_music</option>
                                                <option>karaoke</option>
                                                <option>no_music</option>
                                            <?php } else if ($extend->extended->music == "live") { ?>
                                                <option>dj</option>
                                                <option selected>live</option>
                                                <option>jukebox</option>
                                                <option>background_music</option>
                                                <option>karaoke</option>
                                                <option>no_music</option>
                                            <?php } else if ($extend->extended->music == "jukebox") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option selected>jukebox</option>
                                                <option>background_music</option>
                                                <option>karaoke</option>
                                                <option>no_music</option>
                                            <?php } else if ($extend->extended->music == "background_music") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option selected>background_music</option>
                                                <option>karaoke</option>
                                                <option>no_music</option>
                                            <?php } else if ($extend->extended->music == "karaoke") { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background_music</option>
                                                <option selected>karaoke</option>
                                                <option>no_music</option>
                                            <?php } else { ?>
                                                <option>dj</option>
                                                <option>live</option>
                                                <option>jukebox</option>
                                                <option>background_music</option>
                                                <option>karaoke</option>
                                                <option selected>no_music</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">sports</label>
                                <div class="col-sm-6 listitem">
                                    <div style="max-width:100%;" class="styled">
                                        <select name="sports" style="color:#262626;">
                                            <option <?php if ($extend->extended->sports == "soccer") {
                                            $sp = 1; ?> selected <?php } ?>>soccer</option>
                                            <option <?php if ($extend->extended->sports == "basketball") {
                                            $sp = 1; ?> selected <?php } ?>>basketball</option>
                                            <option <?php if ($extend->extended->sports == "hockey") {
                                            $sp = 1; ?> selected <?php } ?>>hockey</option>
                                            <option <?php if ($extend->extended->sports == "football") {
                                            $sp = 1; ?> selected <?php } ?>>football</option>
                                            <option <?php if ($extend->extended->sports == "baseball") {
                                            $sp = 1; ?> selected <?php } ?>>baseball</option>
                                            <option <?php if ($extend->extended->sports == "mma") {
                                            $sp = 1; ?> selected <?php } ?>>mma</option>

                                            <option    <?php if ($sp != 1) { ?> selected  <?php } ?>>other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">wheelchair_accessible</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <?php if ($extend->extended->wheelchair_accessible == 1) { ?>
                                            <input id="yes1" type="radio" name="wheelChair" checked="checked" value="yes">
                                            <label for="yes1">YES</label>
                                            <input id="NO1" type="radio" name="wheelChair" value="no">
                                            <label for="NO1">NO</label>
                                        <?php } else {
                                            ?>
                                            <input id="yes1" type="radio" name="wheelChair" value="yes">
                                            <label for="yes1">YES</label>
                                            <input id="NO1" type="radio" name="wheelChair" checked="checked" value="no">
                                            <label for="NO1">NO</label>
        <?php
    }
    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">outdoor_seating</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <?php if ($extend->extended->outdoor_seating == 1) { ?>
                                            <input id="yes2" type="radio" checked="checked" name="outdoor" value="yes">
                                            <label for="yes2">YES</label>
                                            <input id="NO2" type="radio"  name="outdoor" value="no">
                                            <label for="NO2">NO</label>
                                        <?php } else {
                                            ?>
                                            <input id="yes2" type="radio" name="outdoor" value="yes">
                                            <label for="yes2">YES</label>
                                            <input id="NO2" checked="checked" type="radio" name="outdoor" value="no">
                                            <label for="NO2">NO</label>

        <?php
    }
    ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">smoking</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <?php if ($extend->extended->smoking == 1) { ?> 
                                            <input id="yes3" type="radio" checked="checked"  name="smoking" value="yes">
                                            <label for="yes3">YES</label>
                                            <input id="NO3" type="radio" name="smoking" value="no">
                                            <label for="NO3">NO</label>
                                            <input id="out1" type="radio" name="smoking" value="outdoor">
                                            <label for="out1">OUTDOOR</label>
                                        <?php } else if ($extend->extended->smoking == "outdoor") { ?>
                                            <input id="yes3" type="radio" name="smoking" value="yes">
                                            <label for="yes3">YES</label>
                                            <input id="NO3" type="radio" name="smoking" value="no">
                                            <label for="NO3">NO</label>
                                            <input id="out1" type="radio" checked="checked"  name="smoking" value="outdoor">
                                            <label for="out1">OUTDOOR</label>
    <?php } else { ?>
                                            <input id="yes3" type="radio" name="smoking" value="yes">
                                            <label for="yes3">YES</label>
                                            <input id="NO3" type="radio" checked="checked" name="smoking" value="no">
                                            <label for="NO3">NO</label>
                                            <input id="out1" type="radio" name="smoking" value="outdoor">
                                            <label for="out1">OUTDOOR</label>

    <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">waiter_service</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <?php if ($extend->extended->waiter_service == 1) { ?>
                                            <input id="yes4" type="radio" checked="checked"  name="waiter_service" value="yes">
                                            <label for="yes4">YES</label>
                                            <input id="NO4" type="radio" name="waiter_service" value="no">
                                            <label for="NO4">NO</label>
                                        <?php } else {
                                            ?>
                                            <input id="yes4" type="radio" name="waiter_service" value="yes">
                                            <label for="yes4">YES</label>
                                            <input id="NO4" type="radio" checked="checked" name="waiter_service" value="no">
                                            <label for="NO4">NO</label>
        <?php
    }
    ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">television</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <?php if ($extend->extended->television == 1) { ?>
                                            <input id="yes5" type="radio"  checked="checked" name="television" value="yes">
                                            <label for="yes5">YES</label>
                                            <input id="NO5" type="radio" name="television" value="no">
                                            <label for="NO5">NO</label>
                                        <?php } else {
                                            ?>
                                            <input id="yes5" type="radio" name="television" value="yes">
                                            <label for="yes5">YES</label>
                                            <input id="NO5" type="radio"  checked="checked" name="television" value="no">
                                            <label for="NO5">NO</label>
    <?php }
    ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem">ambience</label>
                                <div class="col-sm-6 listitem">
                                    <div class="radio" style="margin:5px 0 !important;">
                                        <input id="yes6" type="radio" name="optionsRadios6" value="">
                                        <label for="yes6">YES</label>
                                        <input id="NO6" type="radio" name="optionsRadios6" value="">
                                        <label for="NO6">NO</label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group itemlst col-lg-6">
                                <label  class="col-sm-6 labelitem"></label>
                                <div class="col-sm-6 listitem">
                                    <button class="btn btngreen">CLICK HERE FOR FULL JSON</button>
                                </div>
                            </div>




                            <div class="clearfix"></div>
                        </div>



                    </div>
<?php } ?>
                <!--ent bg white container-->
            </div>
            <!-- End content -->


            <!-- Start Footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
                        <div class="col-md-offset-3 col-md-5"> 
                            <ul class="footer-nav">                                               
                                <li><a href="#">Home</a>      
                                <li><a href="#">About</a>        
                                <li><a href="#">Blog</a>       
                                <li><a href="#">Terms & Conditions</a> 
                                <li><a href="#">FAQContact</a> 
                            </ul>
                        </div>
                        <div class="col-md-12"><p class="topline">&copy; 2015 Replimatic. All rights reserved. </p></div>

                    </div>
                    <!-- .row -->
                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>

    </body>

</html>