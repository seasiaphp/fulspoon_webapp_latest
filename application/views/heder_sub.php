<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon  | Contact</title>

        <!-- Define Charset -->
        <meta charset="utf-8">
        <script language="javascript" 
        src="https://maps.googleapis.com/maps/api/js?v=3"></script>

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">
        <script src="<?= base_url() ?>js/jquery.min.js"></script>
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url(); ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/colors/green.css" title="green" media="screen" />


        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <div class="hidden-header"></div>
            <!-- Start Header ( Logo & Naviagtion ) -->
            <div class="navbar navbar-default navbar-top greenbdr">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="<?= base_url() ?>">
                             <?php echo '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="170px" height="46px" viewBox="0 0 792 214" style="enable-background:new 0 0 792 214;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#73BF4C;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{opacity:0.52;}
	.st3{clip-path:url(#SVGID_6_);fill:#6EBD44;}
	.st4{fill:url(#SVGID_7_);}
</style>
<g>
	<g>
		<g>
			<g>
				<path class="st0" d="M253.3,163.1v-61.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
					c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H253.3z"/>
				<path class="st0" d="M262.9,166.1h-12.6v-61.5h-13.9V92.6h13.9v-3.1c0-16.9,8.3-26.3,23.4-26.3c5.5,0,10.6,1.8,15.3,5.3l2.3,1.7
					l-6.5,9.7l-2.4-1.5c-3.6-2.1-5.6-3-8.9-3c-4.9,0-10.5,1.6-10.5,14.1v3.1h22.4v11.9h-22.4V166.1z M256.3,160.1h0.6V89.6
					c0-17.5,10.3-20.1,16.5-20.1c3.9,0,6.7,1,9.5,2.5l0.1-0.1c-2.9-1.7-6.1-2.5-9.3-2.5c-11.7,0-17.4,6.6-17.4,20.3V160.1z"/>
			</g>
			<g>
				<path class="st0" d="M353.1,163.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V94.9h6.7V134
					c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V94.9h6.7v68.2H353.1z"/>
				<path class="st0" d="M328.9,167.2L328.9,167.2c-20.5,0-33.7-13-33.7-33V91.9h12.7V134c0,13.3,7.4,20.5,20.9,20.5l0.3,0
					c12.3-0.1,20.8-9.1,20.8-21.8V91.9h12.7v74.2h-12.6v-6.7C344.7,164.3,337.3,167.1,328.9,167.2L328.9,167.2z M301.2,97.9v36.2
					c0,16.7,10.6,27,27.7,27c4.9-0.1,13.2-1.3,19-8c-4.7,4.6-11.2,7.3-18.6,7.4l-0.4,0c-16.6,0-26.9-10.2-26.9-26.5V97.9H301.2z
					 M356.1,160.1h0.6V97.9H356v34.9c0,3.3-0.5,6.4-1.4,9.2l1.5-2.7V160.1z"/>
			</g>
			<g>
				<rect x="377.5" y="66.7" class="st0" width="6.7" height="96.4"/>
				<path class="st0" d="M387.2,166.1h-12.7V63.7h12.7V166.1z M380.5,160.1h0.7V69.7h-0.7V160.1z"/>
			</g>
			<g>
				<path class="st0" d="M450.8,107.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
					c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
					c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
					c9.9,0,18.6,2.8,25.3,9.1L450.8,107.5z"/>
				<path class="st0" d="M429.8,167.2c-14.1,0-26.1-5.6-32.3-15l-1.4-2.2l9.4-8.4l1.9,2.8c4.5,6.4,12.9,10.3,22.5,10.3
					c5.4,0,17.9-1,18.2-10.1c0.2-6.7-7.7-8.5-19.6-10.4c-12.8-2.1-28.8-4.8-29-21.2c-0.1-5.4,1.8-10.1,5.5-13.8
					c5.2-5.2,13.9-8.2,24-8.2l0.5,0c11.1,0,20.4,3.3,27.4,9.9l2.2,2.1l-8.2,8.7l-2.2-1.9c-6.1-5.3-11.9-6.4-19.2-6.4l-0.5,0
					c-6.7,0-12.3,1.7-15.1,4.6c-1.3,1.4-2,3-1.9,4.9c0.2,5.9,8,7.7,17.3,9.4l2.4,0.4c11.9,2,29.8,5,29,22.8
					C460.3,161.4,444.5,167.2,429.8,167.2z M404.1,151c5.4,6.4,14.9,10.2,25.7,10.2c5.8,0,24.5-1.2,25-16.1
					c0.5-11.6-10.1-14.3-24-16.7l-2.4-0.4c-8.7-1.5-22-3.9-22.3-15.1c-0.1-3.5,1.1-6.7,3.6-9.2c4-4.1,11-6.4,19.4-6.4l0.5,0
					c6.6,0,13.7,0.8,20.9,6.1l0.1-0.1c-5.5-4.2-12.6-6.3-21-6.3l-0.5,0c-8.5,0-15.7,2.3-19.8,6.4c-2.5,2.5-3.8,5.8-3.7,9.5
					c0.1,10.6,9.5,13,23.9,15.4c11.1,1.8,25,4,24.7,16.5c-0.4,9.9-9.4,15.9-24.2,15.9c-10.2,0-19.4-3.8-25.3-10.2L404.1,151z"/>
			</g>
			<g>
				<path class="st0" d="M472.8,191.4V95.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35
					c0,22.9-15.4,35.3-34.6,35.3c-11.6,0-22.3-5.6-28.1-16.9v44.1H472.8z M535.2,129.1c0-19.1-12.4-28.5-27.8-28.5
					c-15.8,0-27.5,12-27.5,28.6c0,16.7,12,28.6,27.5,28.6C522.8,157.8,535.2,148.2,535.2,129.1"/>
				<path class="st0" d="M482.3,194.4h-12.5V92.1h12.6v9.8c6.6-6.9,16-10.8,26.5-10.8l0.1,0c21.5,0.8,35.9,16.1,35.9,38
					c0,22.9-15.1,38.3-37.6,38.3c-9.9,0-18.7-3.8-25.1-10.5V194.4z M475.8,188.4h0.5v-53.5l1.7,3.4c-0.8-2.9-1.2-5.9-1.2-9.1
					c0-2.9,0.3-5.8,1-8.4l-1.4,2.6V98.1h-0.6V188.4z M487.8,153.9c5.1,4.8,11.9,7.4,19.6,7.4c19.2,0,31.6-12.7,31.6-32.3
					c0-18.7-11.8-31.3-30.1-32c-4,0-7.8,0.7-11.2,1.9c3-1,6.3-1.5,9.8-1.5c18.7,0,30.8,12.4,30.8,31.5c0,19.3-12.1,31.8-30.8,31.8
					C499.8,160.8,493,158.3,487.8,153.9z M507.3,103.6c-14.2,0-24.5,10.8-24.5,25.6c0,14.9,10.3,25.6,24.5,25.6
					c12,0,24.8-6.8,24.8-25.8C532.2,113.1,522.9,103.6,507.3,103.6z"/>
			</g>
			<g>
				<path class="st0" d="M555.3,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
					C570.7,164,555.3,150.8,555.3,129.2 M617.8,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
					c0,17.6,12.5,28.2,27.9,28.2C605.2,157.4,617.8,146.8,617.8,129.2"/>
				<path class="st0" d="M589.8,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
					C627.4,151.5,611.9,167,589.8,167z M589.8,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8
					c18.6,0,31.6-13.1,31.6-31.8C621.4,110.1,608.4,96.8,589.8,96.8z M589.8,160.4c-18.2,0-30.9-12.8-30.9-31.2
					c0-18.6,13-32.1,30.9-32.1c17.9,0,31,13.5,31,32.1C620.8,147.6,608,160.4,589.8,160.4z M589.8,103.2c-14.5,0-24.9,11-24.9,26.1
					c0,14.9,10.3,25.2,24.9,25.2c14.7,0,25-10.4,25-25.2C614.8,114.1,604.3,103.2,589.8,103.2z"/>
			</g>
			<g>
				<path class="st0" d="M636.8,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
					C652.2,164,636.8,150.8,636.8,129.2 M699.3,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
					C686.7,157.4,699.3,146.8,699.3,129.2"/>
				<path class="st0" d="M671.3,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
					C708.9,151.5,693.4,167,671.3,167z M671.3,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8
					c18.6,0,31.6-13.1,31.6-31.8C702.9,110.1,689.9,96.8,671.3,96.8z M671.3,160.4c-18.2,0-31-12.8-31-31.2c0-18.6,13-32.1,31-32.1
					c17.9,0,31,13.5,31,32.1C702.3,147.6,689.5,160.4,671.3,160.4z M671.3,103.2c-14.5,0-25,11-25,26.1c0,14.9,10.3,25.2,25,25.2
					c14.7,0,25-10.4,25-25.2C696.3,114.1,685.8,103.2,671.3,103.2z"/>
			</g>
			<g>
				<path class="st0" d="M728,95.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V124
					c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H728z"/>
				<path class="st0" d="M786,166.1h-12.7V124c0-13.1-7.6-20.5-20.8-20.5l-0.4,0c-12.3,0.1-20.9,9.1-20.9,21.8v40.9h-12.7v-74H731
					v6.6c5.4-4.9,12.8-7.7,21.2-7.8c20.5,0,33.8,13,33.8,33V166.1z M779.2,160.1h0.7v-36.2c0-16.7-10.6-27-27.7-27
					c-4.8,0.1-13.1,1.3-18.9,8c4.7-4.6,11.2-7.3,18.6-7.3l0.4,0c16.6,0,26.8,10.2,26.8,26.5V160.1z M724.4,160.1h0.7v-34.9
					c0-3.2,0.5-6.3,1.3-9.1l-1.5,2.6V98.1h-0.6V160.1z"/>
			</g>
			<path class="st0" d="M94.8,161c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
				c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
				c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
				C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
				c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
				C83.9,152.9,88.3,158.9,94.8,161"/>
			<g>
				<g>
					<defs>
						<rect id="SVGID_1_" x="3.9" y="6.8" width="214.9" height="203.2"/>
					</defs>
					<clipPath id="SVGID_2_">
						<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st1">
						<defs>
							<path id="SVGID_3_" d="M94.8,161c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
								c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
								c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
								C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
								c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
								C83.9,152.9,88.3,158.9,94.8,161"/>
						</defs>
						<clipPath id="SVGID_4_">
							<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
						</clipPath>
					</g>
				</g>
			</g>
			<g class="st2">
				<g>
					<defs>
						<rect id="SVGID_5_" x="3.9" y="12.2" width="214.9" height="197.8"/>
					</defs>
					<clipPath id="SVGID_6_">
						<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
					</clipPath>
					<path class="st3" d="M215.6,72.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
						c2.5,54.9-48.4,101.6-101.6,101.6C49.9,198.2,8.4,154,4,100.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
						C218.5,130,222.2,100.6,215.6,72.9"/>
				</g>
			</g>
		</g>
	</g>
	<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="107.8113" y1="62.1905" x2="84.9779" y2="90.1905">
		<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0"/>
		<stop  offset="0.4058" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
		<stop  offset="0.9913" style="stop-color:#787878;stop-opacity:0.6"/>
	</linearGradient>
	<path class="st4" d="M89.9,52.7c0,0-7.5,13.8-6,53.3c0,0,1.6-25.4,31.6-21.8c0,0-2-20.4,8.6-33.5L89.9,52.7z"/>
</g>
</svg>'; ?>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="navbar-collapse collapse borderbg">       
                        <!-- Stat Search -->
                        
                        <!-- End Search -->
                        <!-- Start Navigation List -->
                        
                        <!-- End Navigation List -->

                    </div>
                </div>

                <!-- Mobile Menu Start -->
                <ul class="wpb-mobile-menu">         

                            
                </ul> 
                <!-- Mobile Menu End -->

            </div>
            <!-- End Header ( Logo & Naviagtion ) -->

        </header>
                           
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <!-- Start Content -->
            <div id="content" style="padding-bottom:40px !important;">
                <div class="container">

                    <div class="successwrap" style="padding-bottom:25px !important; padding-top:0;"> <img src="<?= base_url(); ?>images/logo_success.png" alt="" width="90" style="margin-bottom:15px;"/> <br>
                        Contact Us
                        <hr style="max-width:70%;">
                    </div>

                    <div class="row">


                        <div class="contactwp">

                            <div class="col-md-6 col-sm-6 contact_address">

                                <!-- Classic Heading -->
                                <div class="successwrap" style="font-size:22px; padding-top:10px; text-align:left;">
                                    Contact Address
                                </div>

                                <!-- Divider -->
                                <div class="hr1" style="margin-bottom:10px;"></div>
                                <?php
                                $address = explode(",", $contact['address']);
//                                340 Old River Rd, #625, Edgewater, New Jersey 07020
                                ?>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup" style="line-height:normal; padding:0;">ADDRESS</div>
                                    <div class="col-lg-8" style="padding:0; color:#4c5149;"><?= $address[0] ?>,<br>
                                        <?= $address[1] ?>, <?= $address[2] ?> <br>
                                        &nbsp;&nbsp;<?= $address[3] ?></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup" style="line-height:normal; padding:0;">Email address</div>
                                    <div class="col-lg-8" style="padding:0;">
                                          <a href="#" style="color:#1872b1;"><?= $contact['email'] ?> </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup" style="line-height:normal; padding:0;">Phone Number</div>
                                    <div class="col-lg-8" style="padding:0; color:#4c5149;"><?= $contact['phone_number'] ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- Info - Icons List -->


                            </div>

                            <div class="col-md-6 col-sm-6 contact_form">
                                <div id="success" style="color:#34495e;"></div><br>
                                <!-- Start Contact Form -->
                                <form role="form" class="contact-form" id="contact-form"  method="post" style="padding:0 25px;">
                                    <p class="forlabel">Fill out the below form to contact directly.</p>

                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="labelsignup">NAME</div>
                                        <input type="text"  id="name" class="form-control inputsignup col-lg-12">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="labelsignup">Email</div>
                                        <input type="email" id="email" class="form-control inputsignup col-lg-12">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="labelsignup">Message</div>
                                        <textarea rows="5" name="message" id="message" class="inputsignup col-lg-12"></textarea>
                                        <div class="clearfix"></div>
                                    </div>

                                    <button id="mail1" onClick="mail_sent()" class="btn btngreen" type="button">SUBMIT</button>


                                </form>
                                <!-- End Contact Form -->

                            </div>

                            <div class="clearfix"></div>
                        </div>

                    </div>


                </div>
            </div>
            
              <div class="modal fade" id="myModal1" role="dialog"  style="margin-top:80px">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                       <div class="modal-content">
                            <div class="modal-header">
  <center>
<h1>Thank You.</h1>
  </center>

                            </div>
                            <div class="modal-body">
                                <center> <p>Your message has been submitted. Someone from our team will contact you shortly.</p></center>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>


                           
                            
                        </div>

                    </div>
                </div>

            </div>
            <!-- End content -->

            <div class="container">
                <div class="row">
                    <!--<div >-->
                    <div id="map_canvas" class="mapwp" style="max-height: 300px; min-height: 300px; width: 100%" >
                        <script>
                            function initialize() {
                                var map_canvas = document.getElementById('map_canvas');
                                var map_options = {
                                    center: new google.maps.LatLng(<?= $lat ?>, <?= $long ?>),
                                    zoom: 18,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                                var mapPin = " https://www.google.com/mapfiles/marker.png";
                                var map = new google.maps.Map(map_canvas, map_options)
                                var Marker = new google.maps.Marker({
                                    map: map,
                                    position: map.getCenter()
                                });
                            }
                            google.maps.event.addDomListener(window, 'load', initialize);
                        </script>
                    </div>
                    <!--</div>-->
                </div>
            </div>
            <div class="clearfix"></div>

            <script>
                function validateEmail(email) {
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    return emailReg.test(email);
                }
                function mail_sent() {
                    var name = $("#name").val();
                    var email = $("#email").val();
                    var message = $("#message").val();
                    if (name === '') {
                        $("#success").empty();
                        $("#success").html("<span style='color:red'>Name couldn't be null value</span>");
                    }
                    else if (email === '') {
                        $("#success").empty();
                        $("#success").html("<span style='color:red'>Email couldn't be null value</span>");
                    } else if (!validateEmail(email)) {
                        $("#success").empty();
                        $("#success").html("<span style='color:red'>Please Enter a valid email</span>");
                    }
                    else if (message === '') {
                        $("#success").empty();
                        $("#success").html("<span style='color:red'>Message couldn't be null value</span>");
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() . "owners/send_mail"; ?>",
                            data: {name: name, email: email, message: message},
                            success: function (data) {
							
							  $('#myModal1').modal('show');
                                return true;
                            }
                        })
                    }
                }

            </script>

            <!-- Start Footer -->
