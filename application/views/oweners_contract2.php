<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Replimatic | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />


        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top green-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="owners_index.html"><img src="<?= base_url() ?>images/replimatic_logo_white.png" alt=""/></a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Stat Search -->
                            <!-- Stat Search -->
                            <div class="buttons-side">
                                <a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>
                                <a class="btn btn-default btnlogin" href="#" role="button">SIGN UP</a>
                            </div>
                            <!-- End Search -->
                            <!-- End Search -->
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li> <a style="color:#fff;">Call Us: 877-805-1234</a></li>             
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li><a>Call Us: 877-805-1234</a></li>
                        <li><a href="index-01.html">LOGIN</a></li>
                        <li><a href="#">SIGN UP</a></li></ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <!-- Start Content -->
            <div id="content">
                <div class="container contractwp">

                    <div class="successwrap" style="font-size:24px; line-height:36px;">GENERAL CONTRACT FOR SERVICES</div>

                    <p class="contract_lid">
                        This Contract for Services is made effective as of December 18, 2015, by and between CLIENTFULLNAME ("CLIENT") of TBD-address, 
                        TBD-city, State 00000, and Replimatic LLC ("Replimatic") of 340 Old River Rd, #625, Edgewater, New Jersey 07020.
                    </p>

                    <p class="contract_lid">
                        <span>1. DESCRIPTION OF SERVICES.</span> Beginning on Month Day, 2015, Replimatic will provide to CLIENT the following services (collectively, the "Services"): 
                        <br>
                        (a) Create and manage subscriptions with customers, <br>
                        (b) generate and transmit orders and (c) share subscription revenue. <br>
                        <br>
                          
                        Under the Services, a subscriber (“Subscriber”) is defined as a customer who agrees to pay a fixed monthly fee (“Subscription Fee”) to Replimatic in return for a fixed concession (“Subscriber Concession”) on every order placed with the CLIENT. 
                    </p>

                    <p class="contract_lid">
                        <span>2. PAYMENT.</span> Payment shall be made to CLIENT by Replimatic LLC, Edgewater, New Jersey 07020. <br>

                          		<br>
                        Replimatic agrees to pay CLIENT as follows: 

                        <br><br>

                        Replimatic will pay CLIENT $N per month for every Effective Subscriber the CLIENT had for the prior month. <br>


                        Effective Subscriber is calculated as follows: <br>

                        (a)For every subscriber in the given month, add up the number of days they were subscribers of the CLIENT<br>

                        (b)Divide total sum from (a) by 30<br>
                        <br>
                        Replimatic will not charge CLIENT for Services. 
                    </p>

                    <p class="contract_lid">
                        <span>3. CLIENT OBLIGATION.</span> The CLIENT is obligated to provide a CC% concession on every order to Subscriber.        
                    </p>

                    <p class="contract_lid">
                        <span>4. TERM.</span> This Contract may be terminated by either party upon 60 days prior written notice to the other party. An email notice by one party will suffice.        
                    </p>
                    <p class="contract_lid">
                        <span>5.  WORK PRODUCT OWNERSHIP.</span> Any copyrightable works, ideas, discoveries, inventions, patents, products, or other information (collectively the "Work Product") developed in whole or in part by Replimatic in connection with the Services will be the exclusive property of Replimatic. Upon request, CLIENT will execute all documents necessary to confirm or perfect the exclusive ownership of Replimatic to the Work Product. 
                    </p>
                    <p class="contract_lid">
                        <span>6. CONFIDENTIALITY.</span> Replimatic, and its employees, agents, or representatives will not at any time or in any manner, either directly or indirectly, use for the personal benefit of Replimatic, or divulge, disclose, or communicate in any manner, any information that is proprietary to CLIENT. Replimatic and its employees, agents, and representatives will protect such information and treat it as strictly confidential. This provision will continue to be effective after the termination of this Contract. Any oral or written waiver by CLIENT of these confidentiality obligations which allows Replimatic to disclose CLIENT's confidential information to a third party will be limited to a single occurrence tied to the specific information disclosed to the specific third party, and the confidentiality clause will continue to be in effect for all other occurrences.  
                        <br/>
                        Upon termination of this Contract, Replimatic will return to CLIENT all records, notes, documentation and other items that were used, created, or controlled by Replimatic during the term of this Contract. 
                    </p>
                    <p class="contract_lid">
                        <span>7. DEFAULT.</span> The occurrence of any of the following shall constitute a material default under this Contract: 
                        <br/>
                          (a) The failure to make a required payment when due. <br/>
                          
                          (b) The insolvency or bankruptcy of either party. <br/>
                          
                          (c) The subjection of any of either party's property to any levy, seizure, general assignment for the benefit of creditors, application or sale for or by any creditor or government agency<br/> 
                          
                          (d) The failure to make available or deliver the Services in the time and manner provided for in this Contract. <br/>
                    </p>

                </div>
                <div class="container text-center"><br>

                    <a href="<?= base_url() ?>owners/oweners_success/<?= $this->uri->segment(3) ?>">    <button class="btn btngreen">ACCEPT</button></a><a href="<?= base_url() ?>owners/oweners_decline/<?= $this->uri->segment(3) ?>">    <button class="btn btngreen">DECLINE</button></a><br>

                </div>
            </div>
            <!-- End content -->


            <!-- Start Footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
                        <div class="col-md-offset-3 col-md-5"> 
                            <ul class="footer-nav">                                               
                                <li><a href="#">Home</a>      
                                <li><a href="#">About</a>        
                                <li><a href="#">Blog</a>       
                                <li><a href="#">Terms & Conditions</a> 
                                <li><a href="#">FAQContact</a> 
                            </ul>
                        </div>
                        <div class="col-md-12"><p class="topline">&copy; 2015 Replimatic. All rights reserved. </p></div>

                    </div>
                    <!-- .row -->
                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>

    </body>

</html>