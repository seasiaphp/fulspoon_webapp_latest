<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Replimatic | Home</title>
  <!-- Define Charset -->
  <meta charset="utf-8">
  <!-- Responsive Metatag -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css" type="text/css" media="screen">
  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="<?php echo base_url()?>css/font-awesome.min.css" type="text/css" media="screen">
  <!-- Slicknav -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/slicknav.css" media="screen">
  <!-- Margo CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/style.css" media="screen">
  <!-- Responsive CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/responsive.css" media="screen">
  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/animate.css" media="screen">
  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/colors/red.css" title="red" media="screen" />
  <!-- Margo JS  -->
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.migrate.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/modernizrr.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>asset/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.fitvids.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/owl.carousel.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/nivo-lightbox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.isotope.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.appear.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/count-to.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.textillate.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.lettering.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.easypiechart.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.nicescroll.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.parallax.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/mediaelement-and-player.js"></script>
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.slicknav.js"></script>
  

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">


    <!-- Start Header Section -->
    
    <header class="clearfix">
      <!-- Start  Logo & Naviagtion  -->
      <div class="navbar navbar-default navbar-top">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand" href="index.html">
                <img alt="" src="<?= base_url() ?>images/replimatic_logo.png">
            </a>
            <div class="clearfix"></div>
          </div>
          
          <div class="navbar-collapse collapse borderbg">       
             <!-- Stat Search -->
            <!--<div class="buttons-side">
              	<a class="btn btn-default btnlogin" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>
           		<a class="btn btn-default btnsignup" href="#" role="button">SIGN UP</a>
              </div>-->
            <!-- End Search -->
            <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">LOGIN</h4>
      </div>
      <div class="modal-body popbody">
        <form>
                     <div class="form-group">
                        <div class="col-lg-3 labelsignup">Email</div>
                        <div class="col-lg-9"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="Email"></div>
                        <div class="clearfix"></div>
      				 </div>
                     <div class="form-group">
                        <div class="col-lg-3 labelsignup">Password</div>
                        <div class="col-lg-9"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                        <div class="clearfix"></div>
      				 </div>
                     <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN IN</button></div>
                     <div class="form-group text-center col-lg-pull-3">Don't have an account <a href="">Sign Up</a></div>
      				 </form>
      </div>     
    </div>
  </div>
</div>
            <!-- Start Navigation List -->
          <!--  <ul class="nav navbar-nav navbar-right">
             <li><a href="contact.html" class="linkcolor">For Restaurants Owners</a></li>             
            </ul>-->
            <!-- End Navigation List -->
           
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">         
          <li><a href="contact.html">For Restaurants Owners</a></li> 
          <li><a href="contact.html">Login</a></li>  
          <li><a href="contact.html">Sign Up</a></li>               
        </ul>      
        <!-- Mobile Menu End -->

      </div>
      <!-- End Header Logo & Naviagtion -->

    </header>
    <!-- End Header Section -->


    <!-- Start Home Page Slider -->
    <section id="home">
    
    
    
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">

       
        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item noitem active">
            <img class="img-responsive" src="<?php echo base_url()?>images/slider/img1.png" alt="slider">            
            <div class="slider-content">
            <div class="container">
              <div class="col-md-12 text-center">
              

 


                <h2 class="animated2 homehaed"><span>Start your subscription with Guaranteed savings or Money back</span></h2>
                <p class="animated3">We are getting ready to launch and would love to notify you when ready</p>  
                 </div>             
                 <div class="col-md-12 text-center animated4">
                
                <div class="col-md-4 col-md-offset-2 homeform-group"><input type="email" class="form-control homeinput" id="" placeholder="EMAIL ADDRESS TEXT BOX "></div>
                <div class="col-md-3 homeform-group"><input type="email" class="form-control homeinput" id="" placeholder="ZIPCODE"></div>
                <div class="col-md-1"><button type="submit" class="btn btn-default btngetstart"> NOTIFY ME </button></div>
               </div>
             
            </div>
            </div>
          </div>
          <!--/ Carousel item end -->
         
          <!--/ Carousel item end -->
       
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->

        <!-- Controls -->       
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->
	<!-- PACKAGE -->
    <div class="packagewrap">
    <div class="container packagein">
    Starts from <?php  echo'$'.$get_package_details[0]['user_fee'];?> / MONTH <i class="fa fa-circle"></i> 
     NO COMMITMENTS <i class="fa fa-circle"></i>
     CANCEL ANYTIME    </div>
    </div>
	<!-- End PACKAGE -->
    <!-- Start How its work Section -->
    <div class="section service">
      <div class="container">
        <div class="row">
          <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1><strong>HOW IT WORKS</strong></h1>
      </div>
		          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
               <i ><img src="<?php echo base_url()?>images/pickrestaurant.png" alt=""/></i>
            </div>
            <div class="service-content">
              <h4>Sign up for Replimatic subscription</h4>
              <p>Pick your favorite restaurants 
Select your subscription package for your favorite restaurant/s</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <i ><img src="<?php echo base_url()?>images/online.png" alt=""/></i>
            </div>
            <div class="service-content">
              <h4>Order</h4>

              <p>Start ordering from your subscribed restaurants just the way you have been....</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <i ><img src="<?php echo base_url()?>images/savemoney.png" alt=""/></i>
            </div>
            <div class="service-content">
              <h4>Save Money</h4>
              <p>You will see the savings with every order and we Guarantee that you won't be disappointed. </p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

         </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End  How its work Section -->
	  <!-- Start Pricing Table Section -->
    <div class=" section pricing-section calculator-wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Start Big Heading -->
            <div class="big-title text-center">
              <h1 style="color:#fff; text-transform:uppercase; font-size:36px;">Replimatic Savings Calculator</h1>
            </div>
            
            <!-- End Big Heading -->
          </div>
        </div>

        <div class="row pricing-tables">
        
        <div class="row">
        <div class="col-md-12 formcalcularor">
        <div style="border:1px solid #fff; padding-bottom:20px;">
        
        <div class="col-sm-4">
        <label class="labelcaltr">How many times do you order from your favorite restaurant every month?</label>
        <div class="clearfix"></div>
        <input type="text" class="form-control" placeholder="Enter Value" name="count_res" id="count_res" onBlur="calculate()">
        </div>
        
        <div class="col-sm-4">
        <label class="labelcaltr">Your Average Invoice?</label>
        <div class="clearfix"></div>
        <input type="text" class="form-control" placeholder="Enter Value" name="avg_res" id="avg_res" onBlur="calculate()">
        </div>
        
        <div class="col-sm-4" >
        <label class="labelcaltr">Monthly Spend on One Restaurant</label>
        <div class="clearfix"></div>
        <div >
        <input style="background-color:#6FBE44; color:#FFFFFF"	type="text" class="form-control" placeholder="Enter Value" name="m_spend_res" id="m_spend_res"  readonly >
        </div>
        </div>
        
        <div class="clearfix"></div>
        </div>
        </div>
        </div>
     
            <div class="">
            
            <div class="">
            <h1 style="background-color:#fff; padding:10px 5px; text-align:left;" class="tblhead labelcalculator">
            Savings with Replimatic
            </h1>
        	</div>
            
            <div id="maintable">
            <table class="col-md-12 tableMaincal table-striped table-condensed cf">
        		<thead>
        			<tr>
        				<td class="stylebtn"></td>
        				<td class="numeric stylebtn">Monthly</td>
        				<td class="numeric stylebtn">Annual</td>
                        <td class="numeric stylebtn">Subscription Fee</td>
        			</tr>
        		</thead>
        		<tbody>
                <?php $i=1; foreach($get_package_details as $package) {
				?>
                <tr>
        				<td class="callabel" data-title="" ><?php echo $package['package_name'];?></td>
        				<td data-title="Monthly" class="numeric td_<?php echo $i;?>"  data-attr="<?php echo $package['number_of_restaurants'];?>" id="month_<?php echo $i;?>"></td>
        				<td data-title="Annual" class="numeric td1_<?php echo $i;?>" ></td>
                        <td data-title="Subscription Fee" class="numeric " ><?php echo '$'.$package['user_fee'];?></td>
                       
        			</tr>
                <?php $i++; } ?>
                 <tr><td colspan="4"><input type="hidden" name="hid" id="hid" value="<?php echo count($get_package_details);?>" ></td></tr>
        		</tbody>
        	</table>
        </div>
        </div>
        </div>
      </div>
    </div>
    <!-- End Pricing Table Section -->

	   <script>
		function calculate()
		{
	
	var hid=document.getElementById("hid").value;

		var count_res=document.getElementById("count_res").value;
		var avg_res=document.getElementById("avg_res").value;
		var textcontrol = document.getElementById("m_spend_res");
         textcontrol.value = '$'+count_res *avg_res;
		 
		 for (var i = 1; i <= hid; i++) {
		 
			var pag_count = $('.td_'+i).attr("data-attr");
			var mont_pak=pag_count*count_res*avg_res*.20;
			$('.td_'+i).html('$'+mont_pak.toFixed(2));
			$('.td1_'+i).html('$'+(mont_pak*12).toFixed(2));
   // more statements
}
		}
		
		</script>
    <!-- Start Purchase Section -->
    <!--<div class="section purchase">
      <div class="container">

        <!-- Start Video Section Content -->
        
        <!-- End Section Content -->

      </div>
      <!-- .container -->
    </div>
    <!-- End Purchase Section -->

    <!-- Start Testimonials Section -->
    
    
    <!-- End Testimonials Section -->


    <!-- Start Why Replimatic Section -->
    <!--<div class="section" style="background:#343434;">
      <div class="container">

        <!-- Start Big Heading -->
       
        <!-- End Big Heading -->

    


     

      </div>
      <!-- .container -->
    </div>
    <!-- End Why Replimatic Section -->

  <!-- Start Footer Section -->
    
    <!-- End Footer Section -->


  </div>
  <!-- End Full Body Container -->

  <!-- Go To Top Link -->
  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

  <div id="loader">
    <div class="spinner">
      <div class="dot1"></div>
      <div class="dot2"></div>
    </div>
  </div>
<script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
<?php   $this->load->view("footer_owner"); ?>

</body>

</html>