<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- FORM CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/form.css" media="screen">

        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>


  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body class="">

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <!-- Start Header ( Logo & Naviagtion ) -->
            <div class="navbar navbar-default navbar-top greenbdr">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="<?= base_url() ?>">
                            <img alt="" src="<?= base_url() ?>images/replimatic_logo.png">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="navbar-collapse collapse borderbg">       
                        <!-- Stat Search -->
<!--                        <div class="buttons-side">
                            <a class="btn btn-default btnlogin" href="<?= base_url() ?>member/login" role="button">LOGIN</a>
                            <a class="btn btn-default btnsignup" href="<?= base_url() ?>member/signup" role="button">SIGN UP</a>
                        </div>-->
                        <!-- End Search -->
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                         <!--<li><a href="<?= base_url() ?>contact.html" class="linkcolor">For Restaurants Owners</a></li>     -->        
                        </ul>
                        <!-- End Navigation List -->
                    </div>
                </div>

                <!-- Mobile Menu Start -->
                <ul class="wpb-mobile-menu">         
                 <!-- <li><a href="<?= base_url() ?>contact.html">For Restaurants Owners</a></li> -->
<!--                    <li><a href="<?= base_url() ?>member/login">Login</a></li>  
                    <li><a href="<?= base_url() ?>member/signup">Sign Up</a></li>               -->
                </ul> 
                <!-- Mobile Menu End -->

            </div>
            <!-- End Header ( Logo & Naviagtion ) -->

        </header>
        <!-- End Header -->



        <!-- Start Content -->
        <div id="content">

            <div class="container">
                <div class="col-md-12 text-center" style="height:130px;"><img src="<?= base_url() ?>images/subscribe_success.png" alt=""/></div>
                <div class="clearfix"></div>
                <h1 class="welcome text-center">Welcome <br> 
                    <span>Your Subscription is ready to go. Start ordering now</span>.</h1>

                <div class="col-md-6 col-md-offset-3 col-sm-12">
                    <div class="whitebox text-center">
                        <img src="<?= base_url() ?>images/replimatic_logo_gray.png" alt="" style="margin-top:30px"/> <h4 style="margin-bottom:15px;">Download Apps</h4> 

                        <p><a target="_blank" href="<?= $url[1]['value'] ?>"><img src="<?= base_url() ?>images/downloadapple.png" alt="" style="padding:5px;"/></a> 

                            <a target="_blank" href="<?= $url[0]['value'] ?>"><img src="<?= base_url() ?>images/downloadplaystore.png" alt="" style="padding:5px;"/></a></p>
                    </div>    
                </div>

            </div>
        </div>
    </div>
    <!-- End content -->


    <!-- Start Footer Section -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4"> 



                    <ul  class="footer-nav">                                               
                        <li><a href="<?php echo base_url(); ?>">Home</a> </li>    
                        <li><a href="<?php echo base_url(); ?>member/terms_condition">Terms &amp; Conditions</a> </li>
                        <li><a href="<?php echo base_url(); ?>member/faq">FAQ</a> </li>
                        <li><a href="<?php echo base_url(); ?>member/contact">Contact Us</a> </li>
                    </ul>

                </div>
                <div class="col-md-4 text-center padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/> <p>&copy; <?= date('Y') ?> Fulspoon. All rights reserved. </p>  </div>
                <div class="col-md-3 col-md-offset-1 socialnav"  style="display:none">
                    Follow us!
                    <p>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </p>
                </div>
            </div>
            <!-- .row -->
        </div>
    </footer>
    <!-- End Footer Section -->



</div>
<!-- End Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
</body>

</html>