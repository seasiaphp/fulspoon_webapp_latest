<?php
//echo "<pre>";
//print_r($response_datas);
//exit;
?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Replimatic | Signup</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Replimatic Signup">
        <meta name="author" content="iThemesLab">


        <!-- Bootstrap CSS  -->


        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">
        <link rel="stylesheet"  href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- FORM CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/form.css" media="screen">

        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>
        <script src="<?= base_url() ?>js/star-rating.js" type="text/javascript"></script>


  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <!-- Start Header ( Logo & Naviagtion ) -->
            <div class="navbar navbar-default navbar-top greenbdr">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="index.html">
                            <img alt="" src="<?= base_url() ?>images/replimatic_logo.png">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="navbar-collapse collapse borderbg">       
                        <!-- Stat Search -->
                        <div class="buttons-side">
                            <a class="btn btn-default btnlogin" href="#" role="button">LOGIN</a>
                            <a class="btn btn-default btnsignup" href="#" role="button">SIGN UP</a>
                        </div>
                        <!-- End Search -->
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="contact.html" class="linkcolor">For Restaurants Owners</a></li>             
                        </ul>
                        <!-- End Navigation List -->

                    </div>
                </div>

                <!-- Mobile Menu Start -->
                <ul class="wpb-mobile-menu">         
                    <li><a href="contact.html">For Restaurants Owners</a></li> 
                    <li><a href="contact.html">Login</a></li>  
                    <li><a href="contact.html">Sign Up</a></li>               
                </ul> 
                <!-- Mobile Menu End -->

            </div>
            <!-- End Header ( Logo & Naviagtion ) -->

        </header>
        <!-- End Header -->



        <!-- Start Content -->
        <div id="content">
            <div class="container">

                <!--begin tabs going in wide content -->
                <ul class="nav nav-tabs tabsignup content-tabs hidden-xs" id="maincontent" role="tablist">
                    <li class="col-lg-4 tablist active"><a data-parent="" class="js-tabcollapse-panel-heading" href="#home" role="tab" data-toggle="tab"><span>1</span>Replimatic DETAILS</a></li>
                    <li class="col-lg-4 tablist "><a data-parent="" class="js-tabcollapse-panel-heading" href="#profile" role="tab" data-toggle="tab"><span>2</span>Select Reataurants</a></li>
                    <li class="col-lg-4 tablist "><a data-parent="" class="js-tabcollapse-panel-heading" href="#confirm" role="tab" data-toggle="tab"><span>3</span>Confirmation</a></li>
                </ul>

                <div class="panel-group visible-xs" id="maincontent-accordion">
                    <div class="panel panel-default">   <div class="panel-heading">  
                            <h4 class="panel-title">    
                                <a data-parent="#maincontent-accordion" class="js-tabcollapse-panel-heading" href="#home-collapse" role="tab" data-toggle="collapse">REPLIMATIC DETAILS</a></h4>   </div>   <div id="home-collapse" class="panel-collapse collapse in">       <div class="panel-body js-tabcollapse-panel-body">                                          
                                <h3 class="head30 text-center col-lg-12">Create your Account <a href="#" class="pull-right greenlink">Already have an account? Login</a></h3>
                                <div class="form-group text-center"><button class="btn btn-default fbsignup" type="submit"><i class="fa fa-facebook"></i> SIGN UP WITH FACEBOOK</button></div>
                                <div class="col-lg-12 text-center orsep"><span>OR</span></div>

                                <!-- SIGN UP ---->
                                <div class="col-lg-12">
                                    <form>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">First Name</div>
                                            <div class="col-lg-6"><input type="text" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Last Name</div>
                                            <div class="col-lg-6"> <input type="text" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Email</div>
                                            <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Password</div>
                                            <div class="col-lg-6"><input type="password" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN UP</button></div>
                                        <div class="form-group text-center col-lg-pull-3">By signing up you're accepting our <a href="">terms of use</a> and <a href="#">privacy policy.</a></div>
                                    </form>

                                </div>
                                <!-- END SIGN UP ---->
                            </div>   </div></div>


                    <div class="panel panel-default ">   <div class="panel-heading">      <h4 class="panel-title">      <a data-parent="#maincontent-accordion" class="js-tabcollapse-panel-heading collapsed" href="#profile-collapse" role="tab" data-toggle="collapse">SELECT RESTAURANTS</a></h4>   </div>   <div id="profile-collapse" class="panel-collapse collapse ">                  <div class="panel-body js-tabcollapse-panel-body">                                          
                                <!-- SELECT REATAURANT -->
                                <div class="col-lg-3 col-md-3 col-sm-4 filterwrap">
                                    <h3>Cuisine <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <?php
                                        $i = 0;
                                        foreach ($cuisines as $cus) {
                                            ?>
                                            <div class="checkbox"><label for="male<?= $i ?>"><input id="male<?= $i ?>" type="checkbox"><?= $cus['cusine_name']; ?></label></div>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h3>Rating <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <input id="input-21b star_value_rep" value="4" type="number" class="rating" min=0 max=5 step=0.2 data-size="lg">
                                        <a>& up</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h3>Price <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <input id="input-21b dollor_value_rep" value="4" type="number" class="rating" min=0 max=5 step=0.2 data-size="lg">
                                    </div>
                                    <div class="clearfix"></div>
                                    <h3>FEATURE <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <div class="checkbox"><label><input type="checkbox">Coupons Available</label></div>
                                        <div class="checkbox"><label><input type="checkbox">New</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Order Tracking</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Open Now 6:58 PM</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Free Delivery-1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Hide Delivery Services -1</label></div>                   
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8  select-wrapper">
                                    <div class="col-lg-12 searchitem"><input type="email" class="form-control searchhide" id="inputEmail3" placeholder="Search Restaurants"></div>
                                    <div class="col-md-6 resultcount">
                                        5 Results near by you
                                    </div>
                                    <div class="col-md-5 resultcount">
                                        <div class="form-group">
                                            <label for="inputPassword" class="col-sm-3 control-label">Sort by</label>
                                            <div class="col-sm-9">
                                                <select class="form-control">...</select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- SEARCH RESULT-->
                                    <div class="result-wrapper">
                                        <div class="col-md-2 text-center"><img src="<?= base_url() ?>images/thumb100.png" alt="" class="thumb100"/></div>
                                        <div class="col-md-5 restaurantinfo text-center">
                                            <h5>Bella Pizza II & Restaurant</h5>Lunch Specials, Pizza, Sandwiches, Wraps
                                            <p><i class="fa fa-map-marker"></i> Lunch Specials, Pizza, Sandwiches, Wraps</p>                  
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-4"><img src="<?= base_url() ?>images/dolors.png" alt="" class="middle"/></div>
                                            <div class="col-md-4"><p class="middle"><img src="<?= base_url() ?>images/star-rating.png" alt=""/><br>20 ratings</p></div>
                                            <div class="col-md-4"><p class="middle deleverystat">FREE<br><span>Delivery</span></p></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- END SEARCH RESULT-->
                                    <!-- SEARCH RESULT-->

                                    <!-- END SEARCH RESULT-->
                                    <!-- SEARCH RESULT-->

                                    <!-- END SEARCH RESULT-->
                                </div>
                                <div class="clearfix"></div>

                                <!-- END SELECT REATAURANT -->
                            </div>
                        </div></div>



                    <div class="panel panel-default">   <div class="panel-heading">      <h4 class="panel-title">      <a data-parent="#maincontent-accordion" class="js-tabcollapse-panel-heading collapsed" href="#dropdown1-collapse" tabindex="-1" role="tab" data-toggle="collapse">CONFIRMATION</a></h4>   </div>   <div id="dropdown1-collapse" class="panel-collapse collapse ">       
                            <div class="panel-body js-tabcollapse-panel-body">                                          
                                <!-- CONFIRMATION START -->
                                <div class="payment-wrapper">
                                    <h4>PAYMENT INFORMATION</h4>
                                    <form>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Name on the Card *</div>
                                            <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Credit card Number *</div>
                                            <div class="col-lg-6"> <input type="email" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">CVV *</div>
                                            <div class="col-lg-3 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 col-md-4 labelsignup">Expiration Date *</div>
                                            <div class="col-lg-3 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                            <div class="col-lg-3 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN UP</button></div>
                                        <div class="form-group text-center col-lg-pull-3">By signing up you're accepting our <a href="">terms of use</a> and <a href="#">privacy policy.</a></div>
                                    </form>
                                </div>
                                <!-- CONFIRMATION END -->
                            </div>   
                        </div></div>


                </div>
                <!--/.nav-tabs.content-tabs -->


                <div class="tab-content hidden-xs ">

                    <div class="tab-pane active fade in" id="home">       
                        <h3 class="head30 text-center col-lg-12">Create your Account <a href="#" class="pull-right greenlink">Already have an account? Login</a></h3>
                        <div class="form-group text-center"><button class="btn btn-default fbsignup" type="submit"><i class="fa fa-facebook"></i> SIGN UP WITH FACEBOOK</button></div>
                        <div class="col-lg-12 text-center orsep"><span>OR</span></div>

                        <!-- SIGN UP ---->
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">First Name</div>
                                    <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">Last Name</div>
                                    <div class="col-lg-6"> <input type="email" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">Email</div>
                                    <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">Password</div>
                                    <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN UP</button></div>
                                <div class="form-group text-center col-lg-pull-3">By signing up you're accepting our <a href="">terms of use</a> and <a href="#">privacy policy.</a></div>
                            </form>

                        </div>
                        <!-- END SIGN UP ---->
                        <div class="clearfix"></div>
                    </div><!--/.tab-pane -->

                    <div class="tab-pane" id="profile">       
                        <!-- SELECT REATAURANT -->
                        <div class="col-lg-3 col-md-3 col-sm-4 filterwrap">
                            <h3>Cuisine <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <?php
                                $i = 0;
                                foreach ($cuisines as $cus) {
                                    ?>
                                    <div class="checkbox">
                                        <input id="male<?= $i ?>" type="checkbox">
                                        <label for="male<?= $i ?>">
                                            <?= $cus['cusine_name']; ?>
                                        </label>
                                    </div>
                                    <?php
                                    $i++;
                                }
                                ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <h3>Rating <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <input id="input-2" class="rating" data-min="0"  data-show-clear="false" data-max="5" data-step="0.1">
                                <a>& up</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <h3>Price <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <a href="#" class="price active">$</a>
                                <a href="#" class="price">$</a>
                                <a href="#" class="price">$</a>
                                <a href="#" class="price">$</a>
                                <a href="#" class="price">$</a>
                            </div>
                            <div class="clearfix"></div>
                            <h3>FEATURE <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <div class="checkbox"><label><input type="checkbox">Coupons Available</label></div>
                                <div class="checkbox"><label><input type="checkbox">New</label></div>
                                <div class="checkbox"><label><input type="checkbox">Order Tracking</label></div>
                                <div class="checkbox"><label><input type="checkbox">Open Now 6:58 PM</label></div>
                                <div class="checkbox"><label><input type="checkbox">Free Delivery-1</label></div>
                                <div class="checkbox"><label><input type="checkbox">Hide Delivery Services -1</label></div>                   
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8  select-wrapper">
                            <div class="col-lg-12 searchitem"><input type="email" class="form-control searchhide" id="inputEmail3" placeholder="Search Restaurants"></div>
                            <div class="col-md-6 resultcount">
                                5 Results near by you
                            </div>
                            <div class="col-md-6 resultcount">
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-3 control-label">Sort by</label>
                                    <div class="col-sm-9">
                                        <select class="form-control">...</select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- SEARCH RESULT-->
                            <?php
                            foreach ($response_datas as $hotel) {
//     print_r($hotel['hotel_data']->photos);
                                $rating = round($hotel['hotel_data']->rating);
                                $rate_differ = 5 - $rating;
                                $image = $hotel['hotel_data']->photos[0]->photo_reference;
//                                   exit;                            
                                ?>
                                <div class="result-wrapper">
                                    <div class="col-md-2 text-center"><img src='https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&photoreference=<?= $image ?>&key=<?= $key ?>' class='img-responsive' alt="" class="thumb100"/></div>
                                    <div class="col-md-5 restaurantinfo">
                                        <h5><?= $hotel['hotel_data']->name; ?></h5><?php
                                        foreach ($hotel['cuisines'] as $cusine) {
                                            echo $cusine['cusine_name'] . ",";
                                        }
                                        ?>
                                        <p><i class="fa fa-map-marker"></i><?= $hotel['hotel_data']->formatted_address; ?></p>                  
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-4"><img src="<?= base_url() ?>images/dolors.png" alt="" class="middle"/></div>
                                        <div class="col-md-4"><p class="middle"><img src="<?= base_url() ?>images/star-rating.png" alt=""/><br><?= $hotel['hotel_data']->user_ratings_total; ?> ratings</p></div>
                                        <div class="col-md-4"><p class="middle deleverystat">FREE<br><span>Delivery</span></p></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <?php } ?>
                            <!-- END SEARCH RESULT-->
                            <!-- SEARCH RESULT-->

                            <!-- END SEARCH RESULT-->
                            <!-- SEARCH RESULT-->

                            <!-- END SEARCH RESULT-->
                        </div>
                        <div class="clearfix"></div>

                        <!-- END SELECT REATAURANT -->
                    </div><!--/.tab-pane -->

                    <div class="tab-pane" id="confirm">       
                        <!-- CONFIRMATION START -->
                        <div class="col-md-8">
                            <div class="payment-wrapper">
                                <h4>PAYMENT INFORMATION</h4>
                                <form>
                                    <div class="form-group">
                                        <div class="col-lg-4 labelsignup">Name on the Card *</div>
                                        <div class="col-lg-8"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4 labelsignup">Credit card Number *</div>
                                        <div class="col-lg-8"> <input type="email" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-4 labelsignup">CVV *</div>
                                        <div class="col-lg-8"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4 col-md-4 labelsignup">Expiration Date *</div>
                                        <div class="col-lg-4 col-md-4"><div class="styled"><select><option>Month</option><option>Firefox</option><option>Webkit</option></select></div></div>
                                        <div class="col-lg-4 col-md-4"><div class="styled"><select><option>Year</option><option>Firefox</option><option>Webkit</option></select></div></div>
                                        <div class="clearfix"></div>
                                    </div>

                            </div>
                            <div class="payment-wrapper">
                                <h4>BILLING ADRESS</h4>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 1 *</div>
                                    <div class="col-lg-8"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 2 *</div>
                                    <div class="col-lg-8"> <input type="email" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">City *</div>
                                    <div class="col-lg-8"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 labelsignup">State *</div>
                                    <div class="col-lg-8 col-md-4"><div class="styled"><select><option>Month</option><option>Firefox</option><option>Webkit</option></select></div></div>                        
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Zip Code *</div>
                                    <div class="col-lg-8 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Save Card as</div>
                                    <div class="col-lg-8 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                            <div class="payment-wrapper">
                                <h4>delivery address</h4>

                                <div class="col-lg-offset-4"><input type="checkbox" id="male" /><label for="male">Same as Billing address</label></div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 1 *</div>
                                    <div class="col-lg-8"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 2 *</div>
                                    <div class="col-lg-8"> <input type="email" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">City *</div>
                                    <div class="col-lg-8 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 labelsignup">State *</div>
                                    <div class="col-lg-8 col-md-4"><div class="styled"><select><option>Month</option><option>Firefox</option><option>Webkit</option></select></div></div>                        
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Zip Code *</div>
                                    <div class="col-lg-4 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Save Card as</div>
                                    <div class="col-lg-4 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                            <div class="payment-wrapper">
                                <h4>HOW DID YOU HEAR ABOUT Replimatic?</h4>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Choose One</div>
                                    <div class="col-lg-5"><div class="styled"><select><option>Month</option><option>Firefox</option><option>Webkit</option></select></div></div>
                                    <div class="clearfix"></div>
                                </div> 
                                <div class="form-group"><div class="col-lg-offset-4">
                                        <div class="col-md-12">
                                            <button class="btn btn-default btngreen btn-block" type="submit">Confirm Purchase</button></div></div></div>
                                <div class="form-group"><div class="col-lg-offset-4">I agree to the<a href="">Terms of Use</a> and <a href="#">Privacy Policy.</a></div></div>                     
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="stickdiv margin-top">
                                <div class="stckheader">YOUR Replimatic SUMMARY</div>
                                <div>
                                    <div class="col-md-12 packagelabel">packages <span>$19.95</span></div>
                                    <div class="cartwrap">
                                        <h6>Restaurants</h6>
                                        <ul>
                                            <li>Bella Pizza II & Restaurant</li>
                                            <li>Cafe De Novo</li>
                                        </ul>
                                    </div>
                                    <div class="carttotal">Total (monthly)  <span>$19.95</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"><!-- --></div>
                        <!-- CONFIRMATION END -->
                    </div><!--/.tab-pane -->

                </div>
            </div>
        </div>
        <!-- End content -->


        <!-- Start Footer Section -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4"> 
                        <ul class="footer-nav">
                            <li><a href="#">Restaurant Owners</a>      
                            <li><a href="#">About</a>        
                            <li><a href="#">Contact Us</a>       
                            <li><a href="#">Privacy policy</a> 
                        </ul>
                    </div>
                    <div class="col-md-4 text-center padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/> <p>&copy; 2015 Replimatic. All rights reserved. </p>  </div>
                    <div class="col-md-3 col-md-offset-1 socialnav">
                        Follow us!
                        <p>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </p>
                    </div>
                </div>
                <!-- .row -->
            </div>
        </footer>
        <!-- End Footer Section -->



    </div>
    <!-- End Container -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
    <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
</body>

</html>