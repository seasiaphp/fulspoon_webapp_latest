<!doctype html>

<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>
  <!-- Basic -->
  <title>Replimatic | Home</title>
  <link rel="icon" href="images/appicon.png" type="image/gif">
  <!-- Define Charset -->
  <meta charset="utf-8">

  <!-- Responsive Metatag -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Page Description and Author -->
  <meta name="description" content="Margo - Responsive HTML5 Template">
  <meta name="author" content="iThemesLab">

  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="asset/css/bootstrap.min.css" type="text/css" media="screen">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="screen">

  <!-- Slicknav -->
  <link rel="stylesheet" type="text/css" href="css/slicknav.css" media="screen">

  <!-- Margo CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
  <link rel="stylesheet" type="text/css" href="css/oweners.css" media="screen">

  <!-- Responsive CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">

  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">

  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="css/colors/green.css" title="green" media="screen" />


  <!-- Margo JS  -->
  <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="js/jquery.migrate.js"></script>
  <script type="text/javascript" src="js/modernizrr.js"></script>
  <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/jquery.fitvids.js"></script>
  <script type="text/javascript" src="js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="js/nivo-lightbox.min.js"></script>
  <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
  <script type="text/javascript" src="js/jquery.appear.js"></script>
  <script type="text/javascript" src="js/count-to.js"></script>
  <script type="text/javascript" src="js/jquery.textillate.js"></script>
  <script type="text/javascript" src="js/jquery.lettering.js"></script>
  <script type="text/javascript" src="js/jquery.easypiechart.min.js"></script>
  <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
  <script type="text/javascript" src="js/jquery.parallax.js"></script>
  <script type="text/javascript" src="js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>

<body>

  <!-- Container -->
  <div id="container">

    <!-- Start Header -->
   
    <header class="clearfix">
      <!-- Start Header ( Logo & Naviagtion ) -->
      <div class="navbar navbar-default navbar-top green-nav">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand" href="#"><img src="images/replimatic_logo_white.png" alt=""/></a>
          </div>
          <div class="navbar-collapse collapse">
            <!-- Stat Search -->
           <!-- Stat Search -->
            <div class="buttons-side">
              	<a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>
           		<a class="btn btn-default btnlogin" href="<?= base_url() ?>member/signup" role="button">SIGN UP</a>
              </div>
            <!-- End Search -->
            <!-- End Search -->
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
              <li> <a style="color:#fff;">Call Us: 877-638-6522</a></li>             
            </ul>
            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          <li><a>Call Us: 877-638-6522</a></li>
         <li><a href="index-01.html">LOGIN</a></li>
          <li><a href="">SIGN UP</a></li></ul>
        <!-- Mobile Menu End -->

      </div>
      <!-- End Header ( Logo & Naviagtion ) -->

    </header>
    <!-- End Header -->


    <!-- Start HomePage Slider -->

    <section id="home">
      <!-- Carousel -->
        <div id="main-slide" class="carousel slide" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="images/slider/blured.png" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <p class="animated2" style="font-size:20px;">Lorem ipsum dolor sit amet, an mea zril moderatius consectetuer, sit<br> sapientem concludaturque cu, eum duis antiopam te</p>
                <p class="animated4"><a href="<?= base_url() ?>member/signup" class="slider btn btn-system">Sign Up</a></p>
              </div>
            </div>
          </div>
                 
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->
  
      </div>
    
      <!-- /carousel -->
    </section>
    <!-- End HomePage Slider -->
   
   
   <div class="section">
    <!-- Start Services Icons -->
        <div class="container">
       	<div class="row">
               <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1>What We’ll Do For You</h1>
      </div>
        <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center">
           
            <div class="service-content">
              <h4>New Risk-Free and Costless Revenue Stream</h4>
               <div class="service-icon">
              <i class="fa fa-code icon-medium-effect icon-effect-2 gray-icon"></i>
            </div>
              <p>Lorem ipsum dolor sit amet, no saepe veniam eos, magna veritus persequeris ut mei. Eum alia dicit posidonium id, no maiorum vituperatoribus per. Vel at invidunt nominati elaboraret</p>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center">
              <div class="service-content">
              <h4>Captive Pool of Devoted Patrons</h4>
               <div class="service-icon">
              <i class="fa fa-bolt icon-medium-effect icon-effect-2 gray-icon"></i>
            </div>
              <p>Lorem ipsum dolor sit amet, no saepe veniam eos, magna veritus persequeris ut mei. Eum alia dicit posidonium id, no maiorum vituperatoribus per. Vel at invidunt nominati elaboraret</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->
          
          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center">
               <div class="service-content">
              <h4>(Many) More Orders </h4>
               <div class="service-icon">
              <i class="fa fa-bolt icon-medium-effect icon-effect-2 gray-icon"></i>
            </div>
              <p>Lorem ipsum dolor sit amet, no saepe veniam eos, magna veritus persequeris ut mei. Eum alia dicit posidonium id, no maiorum vituperatoribus per. Vel at invidunt nominati elaboraret</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->
			</div>
        </div>
        <!-- End Services Icons -->
   </div>
    <!-- Start How it works -->
        <div class="section calculator-wrap">
       <div class="container">
        <div class="row">
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1 style="color:#fff;">How it works</h1>
        </div>
        <div class="col-md-6">
        <div class="howin"><h5>Signup with Replimatic to participate</h5></div>
        <div class="howin"><h5>Get orders online thru replimatic </h5> </div>
        </div>
        <div class="col-md-6">
        <div class="howin"><h5>Recieve additional incremantal orders and susbcription revenue each month. 
</h5></div>
        
        
        </div>
        </div>
       </div>
        </div>
	<!-- End How it works -->
	
	
 <!-- Start Replimatic Network -->
        <div class="section">
       <div class="container">
        <div class="row">
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1>Benefits of Joining Replimatic Network</h1>
        </div>
       <div class="col-md-6"><img src="images/network.png" alt=""/></div>
       <div class="col-md-6">
       <div class="network">Participating in Replimatic platform is cost you ZERO. It creates loyal customers for your restraunts generating additional orders resulting in additional revenue. </div>
       <p style="padding-top:20px;">
      
       <!--<a href="#" class="btngreen" style="margin-left:30px;">MORE</a>-->
       
       </p>
       </div>
        </div>
       </div>
        </div>
	<!-- End Replimatic Network -->
   
	<!-- Start Calculator -->
        <div class="section calculator-wrap">
       <div class="container">
        <div class="row">
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1 style="color:#fff;">restaurants owner calculator</h1>
        </div>
        <div class="text-center"><img src="images/calculater.png" alt=""/></div>
        </div>
       </div>
        </div>
	<!-- End Calculator -->
<!--      
	<div class="section graysection">
    <div class="container">
    <div class="row">
    	<div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1>Take It From Them</h1>
        </div>
        <div class="col-md-12">

            Start Testimonials Carousel 
            <div class="custom-carousel show-one-slide touch-carousel" data-appeared-items="2">
               Testimonial 1 
              <div class="classic-testimonials item col-md-12">
              <div class="owener-testimonial">
                <div class="testimonial-content">
                  <p>Great platform for our customers, we get additional 2z5 orders a month from our customer on average which was previsouly not there.</p>
                 <div class="authorinfo">Gary<br>
                  <span>- Local Indian Restaurant</span></div>
                </div>
                <div class="pointer"></div>
                </div>
                <div class="testimonial-author"> <span><img src="images/test_thumb.png" alt="" class="img-circle"/> </span></div>
              </div>
               Testimonial 2 
              <div class="classic-testimonials item col-md-12">
              <div class="owener-testimonial">
                <div class="testimonial-content">
                  <p>We like participating in Replimaic as it doesn't cost us anything but we get the benefit of retaining the customer and establishing our   replaionship</p>
                  <div class="authorinfo">Frank<br>
                  <span>- Local Pizza shop owner</span></div>
                </div>
                <div class="pointer"></div>
                </div>
                <div class="testimonial-author"> <span><img src="images/test_thumb2.jpg" alt="" class="img-circle"/> </span></div>
              </div>
               Testimonial 3 
              <div class="classic-testimonials item col-md-12">
                <div class="owener-testimonial">
                <div class="testimonial-content">
                  <p>Replimatic has helped us generate additional revenue from repeat customers</p>
                  <div class="authorinfo">Cindy<br>
                  <span>- Local Sandwich Shop</span></div>
                </div>
                <div class="pointer"></div>
                </div>
               <div class="testimonial-author"> <span><img src="images/test_thumb3.jpg" alt="" class="img-circle"/> </span></div>
              </div>
                Testimonial 4 
          </div>
             End Testimonials Carousel 

          </div>
        
    </div>
    </div>
    </div>-->
   
    <!-- Start Who We Are -->
        <div class="section calculator-wrap">
       <div class="container">
        <div class="row">
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1 style="color:#fff;">Who We Are</h1>
        </div>
        <p style="color:#fff;font-size:19px" class="text-center">Replimatic platform encourages local restrauants to participate creating local loyal customers for business. </p>
        <!--<p style="padding-top:40px;" class="text-center"><a href="#" class="btnstrock">LEARN MORE</a></p>-->
        
        </div>
       </div>
        </div>
	<!-- End Who We Are -->
   
	<!--  -->
    <!-- Start Footer -->
       <footer>
      <div class="container">
      <div class="row">
      <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
      <div class="col-md-offset-3 col-md-5"> 
       <ul class="footer-nav">                                               
      <li><a href="#">Home</a>      
         
         
      <li><a href="#">Terms & Conditions</a> 
      <li><a href="#">FAQ</a> 
	    <li><a href="#">Contact</a> 
      </ul>
	 </div>
      <div class="col-md-12"><p class="topline">&copy; 2015 Replimatic. All rights reserved. </p></div>
      
      </div>
       <!-- .row -->
      </div>
    </footer>
    <!-- End Footer -->

  </div>
  <!-- End Container -->

  <!-- Go To Top Link -->
  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
  <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>

</body>

</html>