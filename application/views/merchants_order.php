
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cust_admin.css" media="screen">
  <script>
   $(document).ready(function() {
	
	
	$(document).on('click','.check_test',function(){
		
				
	    var is_check1=$(this).val();
	    var is_check=$("#"+is_check1+'_id').val();
		
		var month=$(".month").val();
		var year=$(".year").val();
		var day=$(".day").val();
		var p_type=$("#p_type").val();
		var s_status=$("#s_status").val();
		
		
		
     
	       if(($(".month").val()!='' && $(".year").val()!='' &&  $(".day").val()!='') || (($(".month").val()!='' && $(".year").val()!='' &&  $(".day").val()!='') && ( p_type || s_status))){
		 
		 
		  $.ajax({
						  type : "POST",
						  url  : "<?php echo base_url();?>payment_report/ajaxadm",
						  data : {month:month, year:year,day:day,p_type:p_type,s_status:s_status,is_check:is_check,resci_id:is_check1}, 
						  cache : false,
						  success : function(response) {
							  
							 
							 
							 $("#pay_tab").empty(); 
							  
							   $("#pay_tab").html(response);
							  
							  
							  }
					   });  
		 
		 
		 
			}
		
    
});
	$(document).on('change','.form-control',function(){
		
		 var is_check='';
		var month=$(".month").val();
		var year=$(".year").val();
		var day=$(".day").val();
		var p_type=$("#p_type").val();
		var s_status=$("#s_status").val();
		
		
		
     
	       if(($(".month").val()!='' && $(".year").val()!='' &&  $(".day").val()!='') || (($(".month").val()!='' && $(".year").val()!='' &&  $(".day").val()!='') && ( p_type || s_status))){
		 
		 
		  $.ajax({
						  type : "POST",
						  url  : "<?php echo base_url();?>payment_report/ajaxadm",
						  data : {month:month, year:year,day:day,p_type:p_type,s_status:s_status,is_check:is_check}, 
						  cache : false,
						  success : function(response) {
							  
							 
							 
							 $("#pay_tab").empty(); 
							  
							   $("#pay_tab").html(response);
							  
							  
							  }
					   });  
		 
		 
		 
			}
		
    });
	});
	
  </script>
  
  

  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" media="screen">
  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/colors/red.css" title="red" media="screen" />
  <!-- Margo JS  -->



  <!--[if IE 8]><script src="<?php echo base_url(); ?>https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="<?php echo base_url(); ?>https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


<body>

  <!-- Container -->
  <div id="container">

    <!-- Start Header -->
    <div class="hidden-header"></div>
        <!-- Start Header ( Logo & Naviagtion ) -->
      
      <!-- End Header ( Logo & Naviagtion ) -->

    </header>
   <!-- End Header -->

   <!-- Start Content -->
    <div id="content">
    <div class="">
  <div class="">
    <div class=""> 
    <h2 class="pageheader_report" style="margin-bottom:0;">Order Payments to Merchants</h2>
   
 
     
    <div id="report_tbl">
    <div class="action_bar" style="border-bottom:1px solid #dde2da;">
    <div class="col-sm-12">
    <div class="col-sm-6 pad0 text-center" style="line-height:36px;">SELECT WEEK</div>
    <div class="col-sm-6 pad0">
 <div>
<!--<input class="dateselector-bs"  type="text"v>-->
<input type="text" id="date" data-format="DD-MM-YYYY" data-template="MMM YYYY D" name="date" value="28-02-2016" class="form-control">
</div>
    
    
    </div>
    </div>
    
    <div class="clearfix"></div>
    </div>
    
     <div class="col-sm-12 action_bar">
    <div class="col-sm-3">PAYMENT TYPE</div>
    <div class="col-sm-3 ">   <select class="month form-control" id="p_type" style="width: auto;">
                <option value="">All</option>
                <option value="ACH">ACH</option>
                <option value="Check">Check</option>
           
                </select> </div>
    <div class="col-sm-3">STATUS</div>
    <div class="col-sm-3 "> <select class="month form-control" id="s_status" style="width: auto;">
                 <option value="">All</option>
                <option value="0">Paid</option>
                <option value="1">Mailed</option>
              
            
                </select> </div>
    </div>
    
    
    
      <table class="table-bordered  table-condensed cf" width="100%">
        <thead>
        <tr>
         <td class="numeric header_tblrt">Merchant Name</td>
        <td class="numeric header_tblrt">Date</td>
        <td class="numeric header_tblrt">Payment Type</td>
        <td class="numeric header_tblrt">Transaction Id</td>
        <td class="numeric header_tblrt">Status</td>
        <td class="numeric header_tblrt">Amount</td>
        </tr>
        </thead>
        <tbody id="pay_tab">
        
        <?php $priz=0; foreach($merchants_odr AS $data ){ ?>
        <tr>
       
        <td style="text-align:center"><?php echo $data['restaurant_name']; ?></td>
        <td style="text-align:center"><?php  echo date("m-d-Y", strtotime($data['create_date']));  ?></td>
        <td style="text-align:center"><?php echo $data['account_type']; ?></td>
      
        <td style="text-align:center"><?php if($data['account_type']=='ACH'){ echo $data['recipient_id'];} else{ ?><input type="text" class="form-control" value="<?php echo $data['recipient_id']; ?>"> <?php } ?></td>
        <td style="text-align:center"><?php if($data['account_type']=='ACH'){ 
		               if($data['recipient_id']==''){
						   echo 'Failed';
						   }
						   else{
							    echo 'Paid';
						   }
			
			 } else{ 
			      if($data['recipient_id']!=''){
						   echo 'Mailed';
						   }
			 }?>
			 
			 </td>
		
		
        <td style="text-align:center"><?php echo '$'.($data['amount']); ?></td>
        </tr style="text-align:center">
        <?php $priz=$priz+ $data['amount'];} ?>
         <tr>
        <td  class="numeric" colspan="5">TOTAL</td>
        <td colspan="2" style="text-align:center"><?php echo '$'.$priz;?></td>
        </tr>
        </tbody>
   </table>
   <br><br><br><br><br>
     </div>
    
         
      <!--end of .table-responsive-->
    </div>
</div>
  </div>
</div>

    </div>
    <!-- End content -->


  <!-- Start Footer Section -->
    
    <!-- End Footer Section -->



  </div>
  <!-- End Container -->

  <!-- Go To Top Link -->
<a href="<?php echo base_url(); ?>#" class="back-to-top"></a>
<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>


<style>
.combodate .form-control { display:inline; float:left; margin-right:8px;}

</style>

<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset/js/combodate.js"></script>

<script>






$("#date1").change(function(){
	
	
	
	$('div#date12').find('select.year').addClass('year1');
	$('div#date12').find('select.month').addClass('month1');
	$('div#date12').find('select.day').addClass('date1234');
	$('div#date12').find('select.year1').removeClass('year');
	$('div#date12').find('select.month1').removeClass('month');
	$('div#date12').find('select.date1234').removeClass('day');
	

   
   
   
   
   
   if($(".year1").val()==new Date().getFullYear() ){
		if(new Date().getMonth() ==0){
		$(".month1 option[value='1']").attr("disabled", "disabled");
		$(".month1 option[value='2']").attr("disabled", "disabled");
		$(".month1 option[value='3']").attr("disabled", "disabled");
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==1){
		
		$(".month1 option[value='2']").attr("disabled", "disabled");
		$(".month1 option[value='3']").attr("disabled", "disabled");
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==2){
		
		
		$(".month1 option[value='3']").attr("disabled", "disabled");
		$("option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==3){
		
		
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==4){
		
		
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==5){
		
		
		
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==6){
		
		
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==7){
		
	
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==8){
		
	
	
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==9){
		
	
	

		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==10){
		$("option[value='11']").attr("disabled", "disabled");
	}

	}
	else
	{
	  
		
         $('.month1 option').removeAttr('disabled');
		
	}
   
   
   
   
   
   
   
   
   
   
   
   
   
    y = $(".year1").val(), m = $(".month1").val();
			


var firstDay = new Date(y, m, 1);
var lastDay = new Date(y, m + 1, 0);
		
		 var endDate = new Date(firstDay);
       var date_modi_first_date = endDate.getDate();
	    var date_modi_first_day = endDate.getDay();
		
		//last date
		var endDate1= new Date(lastDay);
		 var date_modi_las_date = endDate1.getDate();
	    var date_modi_last_day = endDate1.getDay();
		
	
			
			if(date_modi_first_day==0){
				var dat=(date_modi_las_date-1)/7;
			
			}
			else if(date_modi_first_day==1){
				var dat=(date_modi_las_date)/7;
				
			}
			else if(date_modi_first_day==2){
				var dat=(date_modi_las_date-6)/7;
				
			}
			
			else if(date_modi_first_day==3){
				var dat=(date_modi_las_date-5)/7;
				
			}
			
			else if(date_modi_first_day==4){
				var dat=(date_modi_las_date-4)/7;
				
			}
			else if(date_modi_first_day==5){
				var dat=(date_modi_las_date-3)/7;
				
			}
			
			else if(date_modi_first_day==6){
				var dat=(date_modi_las_date-2)/7;
				
			}
			
			 
			
			if(dat>3 && dat<4){
				
				$(".date1234 option[value='5']").remove();
				}
			else
			{
				if($(".date1234 option[value='5']").length == 0){
	           $(".date1234").append('<option value="5">5th week</option>');
				}
			  
			}
		
});

	


$("#date").change(function(){
	
	
	
	if($(".year").val()==new Date().getFullYear() ){
		if(new Date().getMonth() ==0){
		$(".month option[value='1']").attr("disabled", "disabled");
		$(".month option[value='2']").attr("disabled", "disabled");
		$(".month option[value='3']").attr("disabled", "disabled");
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==1){
		
		$(".month option[value='2']").attr("disabled", "disabled");
		$(".month option[value='3']").attr("disabled", "disabled");
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==2){
		
		
		$(".month option[value='3']").attr("disabled", "disabled");
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==3){
		
		
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==4){
		
		
		
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==5){
		
		
		
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==6){
		
		
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==7){
		
	
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==8){
		
	
	
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==9){
		
	
	

		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==10){
		$("option[value='11']").attr("disabled", "disabled");
	}

	}
	else
	{
	  
		
         $('.month option').removeAttr('disabled');
		
	}
	
	

	
	
	 y = $(".year").val(), m = $(".month").val();
			


var firstDay = new Date(y, m, 1);
var lastDay = new Date(y, m + 1, 0);
		
		 var endDate = new Date(firstDay);
       var date_modi_first_date = endDate.getDate();
	    var date_modi_first_day = endDate.getDay();
		
		//last date
		var endDate1= new Date(lastDay);
		 var date_modi_las_date = endDate1.getDate();
	    var date_modi_last_day = endDate1.getDay();
		
	
			
			if(date_modi_first_day==0){
				var dat=(date_modi_las_date-1)/7;
			
			}
			else if(date_modi_first_day==1){
				var dat=(date_modi_las_date)/7;
				
			}
			else if(date_modi_first_day==2){
				var dat=(date_modi_las_date-6)/7;
				
			}
			
			else if(date_modi_first_day==3){
				var dat=(date_modi_las_date-5)/7;
				
			}
			
			else if(date_modi_first_day==4){
				var dat=(date_modi_las_date-4)/7;
				
			}
			else if(date_modi_first_day==5){
				var dat=(date_modi_las_date-3)/7;
				
			}
			
			else if(date_modi_first_day==6){
				var dat=(date_modi_las_date-2)/7;
				
			}
			
		
			if(dat>3 && dat<=4){
				$(".day option[value='5']").remove();
				}
			else
			{
				if($(".day option[value='5']").length == 0){
	           $(".day").append('<option value="5">5th week</option>');
				}
			  
				 
			}

	
});


$(function(){
	 
    $('#date').combodate({customClass: 'form-control'}); 
	 $('#date1').combodate({customClass: 'form-control'});  
	$('#datetime').combodate(); 
	$('#time').combodate({
        firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
        minuteStep: 1,
		customClass: 'form-control'
		
    });   
});
</script>






</body>
