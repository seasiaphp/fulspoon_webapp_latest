<?php 
echo "<pre>";
print_r($response);
exit;
?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Fulspoon | Home</title>

  <!-- Define Charset -->
  <meta charset="utf-8">

  <!-- Responsive Metatag -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Page Description and Author -->
  <meta name="description" content="Fulspoon | Home">
  <meta name="author" content="NewageSMB">

  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

  <!-- Slicknav -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

  <!-- Margo CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

  <!-- Responsive CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />
  
  <link href='https://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>



  <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap-select.css">
  <script src="<?= base_url() ?>js/jquery.min.js"></script>
  <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>js/bootstrap-select.js"></script>





  <!-- Margo JS  -->

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  
  
  <style>
  .dropdown-menu li{ float:inherit !important;}
  .bootstrap-select .btn{ height:50px;}
  .bootstrap-select.form-control{ height:inherit !important;}
  .bootstrap-select .btn .caret{ color:#6FBE44 !important; border-left: 8px solid transparent !important;
    border-right: 8px solid transparent !important;
    border-top: 8px dashed !important;}
	.bs-donebutton{ display:none;}
  </style>

</head>

<body style="background:#f4f4f4 !important;">

  <!-- Container -->
  <div id="container">

    <!-- Start Header -->
   
    <header class="clearfix">
      <!-- Start Header ( Logo & Naviagtion ) -->
      <div class="navbar navbar-default navbar-top green-nav">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand" href="owners_index.html"><img src="<?= base_url() ?>images/replimatic_logo_white.png" alt=""/></a>
          </div>
          <div class="navbar-collapse collapse">
            <!-- Stat Search -->
           <!-- Stat Search -->
            <div class="buttons-side">
              	<a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>
           		<a class="btn btn-default btnlogin" href="#" role="button">SIGN UP</a>
              </div>
            <!-- End Search -->
            <!-- End Search -->
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
              <li> <a style="color:#fff;">Call Us: 877-805-1234</a></li>             
            </ul>
            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          <li><a>Call Us: 877-805-1234</a></li>
         <li><a href="index-01.html">LOGIN</a></li>
          <li><a href="#">SIGN UP</a></li></ul>
        <!-- Mobile Menu End -->

      </div>
      <!-- End Header ( Logo & Naviagtion ) -->

    </header>
    <!-- End Header -->

  <!-- Start Content -->
    <div id="content">
      <div class="container bg_white">
      
      <div class="col-lg-6 maininfo">
      
      <h3 class="hdlabel">Blue Hill</h3>
      <div class="form-group itemlst">
        <div class="col-sm-3 listitem">
        <li><img src="images/star.png"></li>
        <li><img src="images/star.png"></li>
        <li><img src="images/star.png"></li>
        <li><img src="images/star.png"></li>
        <li><img src="images/star00.png"></li>
        </div>
        <label class="col-sm-9">
        Total Review : <span>224</span>
        </label>
      </div>
      <div class="form-group itemlst">
        <label class="col-sm-12 pad0">
        160 NJ-35, Red Bank, NJ 07701, United States
        </label>
      </div>
      <div class="form-group itemlst">
        <label class="col-sm-12 pad0">
        <img src="images/phone.png"> <span>+1 212-539-1776</span>
        </label>
      </div>
      <div class="clearfix"></div>
      <br>
      <div class="form-group itemlst">
      <label class="col-sm-3 pad0">Price Rating</label>
      <div class="col-sm-4 listitem">
      <li><img src="images/dolor11.png"></li>
      <li><img src="images/dolor11.png"></li>
      <li><img src="images/dolor11.png"></li>
      <li><img src="images/dolor11.png"></li>
      </div>
      <div class="col-sm-4 listitem">
      <button class="btn btngray"><i><img src="images/globe.png"></i> VIEW WEBSITE</button>
      </div>
      </div>
      
      </div>
      
      <div class="col-lg-6 maininfo">
      <button class="btn btn-block hoursbtn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
      HOURS
      </button>
      <div class="collapse" id="collapseExample">
      <div class="well hourswp">
      
      <div class="col-lg-4 hourslst">
      Monday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="col-lg-4 hourslst">
      Tuesday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="col-lg-4 hourslst">
      Wednesday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="clearfix"></div>
      
      <div class="col-lg-4 hourslst">
      Thursday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="col-lg-4 hourslst">
      Friday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="col-lg-4 hourslst">
      Saturday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="clearfix"></div>
      <div class="col-lg-4 hourslst">
      Sunday : <br><span class="color_green">5:00 – 11:00</span> PM
      </div>
      <div class="clearfix"></div>
      
      </div>
      </div>
      
      </div>
      <div class="clearfix"></div>
      
      <div class="col-lg-6 pading10">
      <div class="slider">
      
      
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>
        
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="images/slider-thumb.png" alt="...">
            </div>
            <div class="item">
              <img src="images/slider-thumb.png" alt="...">
            </div>
          </div>
        
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        
      
      </div>
      </div>
      <div class="col-lg-6 pading10">
      <div class="slider">
      <img src="images/map_thumb.png">
      </div>
      </div>
      <div class="clearfix"></div>
      
      
      <div class="listwp">
      <h4 class="labelhead"><i><img src="images/contact-ic.png"> </i> CONTACT</h4>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">EMAIL :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">FAX :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">business owner :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>

      <div class="form-group col-lg-6">
        <label  class="labelitem">CUSINES :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">Bank name :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>

      <div class="form-group col-lg-6">
        <label  class="labelitem">Routing Number :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>

      <div class="form-group col-lg-6">
        <label  class="labelitem">Account Number :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>

      <div class="clearfix"></div>
      </div>
      
      
      <div class="listwp">
      <h4 class="labelhead"><i><img src="images/delivery-ic.png"> </i> DELIVERY</h4>
      
      
      <div class="form-group col-lg-6">
        <label  class="labelitem col-sm-5">DELIVERY :</label>
        <div class="col-sm-7 listitem">
         <div class="radio">
         <div class="col-xs-4 pad0">
         <input id="scttn" type="radio" name="yesornoRadios" value="male1">
         <label for="scttn">YES</label>
         </div>
         <div class="col-xs-4 pad0">
         <input id="scttn1" type="radio" name="yesornoRadios" value="female1">
         <label for="scttn1">NO</label>
         </div>
         </div>
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem col-sm-5" style="padding-top:12px;">Minimum Order :</label>
        <div class="col-sm-7 listitem">
         <input type="email" placeholder="$" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>

      <div class="form-group col-lg-6">
        <label  class="labelitem col-xs-5" style="padding-top:12px;">areas :</label>
        <div class="col-xs-4 listitem">
         <input type="email" placeholder="miles" id="exampleInputNname" class="form-control inputsignup">
        </div>
         <label  class="labelitem col-xs-3" style="padding-top:12px; padding-left:10px;">RADIUS</label>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem col-sm-5" style="padding-top:12px;">Delivery Fee :</label>
        <div class="col-sm-7 listitem">
         <input type="email" placeholder="$" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>

      <div class="clearfix"></div>
      </div>
      
      
      <div class="listwp">
      <h4 class="labelhead"><i><img src="images/features-ic.png"> </i> FEATURES</h4>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">alcohol  :</label>
        <div class="col-sm-12 listitem">
        <select class="form-control selectpicker">
          <option>One</option>
          <option>Two</option>
          <option>Three</option>
        </select>
        </div>
      </div>

      <div class="form-group col-lg-6">
        <label  class="labelitem">parking :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">WIFI  :</label>
        <div class="col-sm-12 listitem">
        <select class="form-control selectpicker">
          <option>One</option>
          <option>Two</option>
          <option>Three</option>
        </select>
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">corkage  :</label>
        <div class="col-sm-12 listitem">
        <select class="form-control selectpicker">
          <option>One</option>
          <option>Two</option>
          <option>Three</option>
        </select>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">dietary_restrictions  :</label>
        <div class="col-sm-12 listitem">
        <div class="styled" style="max-width:100%;">
        <select class="form-control selectpicker">
          <option>One</option>
          <option>Two</option>
          <option>Three</option>
        </select>
        </div>
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">MUSIC  :</label>
        <div class="col-sm-12 listitem">
          <select id="done" class="selectpicker" multiple data-done-button="true">
            <option>Apple</option>
            <option>Banana</option>
            <option>Orange</option>
            <option>Pineapple</option>
            <option>Apple2</option>
            <option>Banana2</option>
            <option>Orange2</option>
            <option>Pineapple2</option>
            <option>Apple2</option>
            <option>Banana2</option>
            <option>Orange2</option>
            <option>Pineapple2</option>
          </select>
        
        </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">SPORTS  :</label>
        <div class="col-sm-12 listitem">
        <select class="form-control selectpicker">
          <option>One</option>
          <option>Two</option>
          <option>Three</option>
        </select>
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">wheelchair_accessible :</label>
        <div class="col-sm-12 listitem">
        <select class="form-control selectpicker">
          <option>One</option>
          <option>Two</option>
          <option>Three</option>
        </select>
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">ambience :</label>
        <div class="col-sm-12 listitem">
        <input type="email" placeholder="email" id="exampleInputNname" class="form-control inputsignup">
        </div>
      </div>
      
      <div class="form-group col-lg-3">
        <label  class="labelitem">outdoor_seating :</label>
        <div class="col-sm-12 listitem" style="padding-top:10px;">
         <div class="radio">
         <div class="col-xs-6 pad0">
         <input id="scttn2" type="radio" name="yesornoRadios" value="male1">
         <label for="scttn2">YES</label>
         </div>
         <div class="col-xs-6 pad0">
         <input id="scttn3" type="radio" name="yesornoRadios" value="female1">
         <label for="scttn3">NO</label>
         </div>
         </div>
        </div>
      </div>
      
      <div class="form-group col-lg-3">
        <label  class="labelitem">waiter_service  :</label>
        <div class="col-sm-12 listitem" style="padding-top:10px;">
         <div class="radio">
         <div class="col-xs-6 pad0">
         <input id="scttn4" type="radio" name="yesornoRadios" value="male1">
         <label for="scttn4">YES</label>
         </div>
         <div class="col-xs-6 pad0">
         <input id="scttn5" type="radio" name="yesornoRadios" value="female1">
         <label for="scttn5">NO</label>
         </div>
         </div>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="form-group col-lg-6">
        <label  class="labelitem">SMOKING  :</label>
        <div class="col-sm-12 listitem" style="padding-top:10px;">
         <div class="radio">
         <div class="col-xs-3 pad0">
         <input id="scttn6" type="radio" name="yesornoRadios" value="male1">
         <label for="scttn6">YES</label>
         </div>
         <div class="col-xs-3 pad0">
         <input id="scttn7" type="radio" name="yesornoRadios" value="female1">
         <label for="scttn7">NO</label>
         </div>
         <div class="col-xs-6 pad0">
         <input id="scttn8" type="radio" name="yesornoRadios" value="female1">
         <label for="scttn8">OUTDOOR</label>
         </div>
         </div>
        </div>
      </div>
      
      <div class="form-group col-lg-6">
        <label  class="labelitem">television  :</label>
        <div class="col-sm-12 listitem" style="padding-top:10px;">
         <div class="radio">
         <div class="col-xs-3 pad0">
         <input id="scttn9" type="radio" name="yesornoRadios" value="male1">
         <label for="scttn9">YES</label>
         </div>
         <div class="col-xs-3 pad0">
         <input id="scttn10" type="radio" name="yesornoRadios" value="female1">
         <label for="scttn10">NO</label>
         </div>
         </div>
        </div>
      </div>
      


      <div class="clearfix"></div>
      </div>
      
      <div class="col-lg-6 col-lg-offset-6">
      
      <button class="btn btn-block btncontinue">CONFIRM</button>
      </div>
      
      
      
             
	</div>
    </div>
    <!-- End content -->

 
    <!-- Start Footer -->
       <footer>
      <div class="container">
      <div class="row">
      <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
      <div class="col-md-offset-3 col-md-5"> 
       <ul class="footer-nav">                                               
      <li><a href="#">Home</a>      
      <li><a href="#">About</a>        
      <li><a href="#">Blog</a>       
      <li><a href="#">Terms & Conditions</a> 
      <li><a href="#">FAQContact</a> 
      </ul>
	 </div>
      <div class="col-md-12"><p class="topline">&copy; 2015 Fulspoon. All rights reserved. </p></div>
      
      </div>
       <!-- .row -->
      </div>
    </footer>
    <!-- End Footer -->

  </div>
  <!-- End Container -->

  <!-- Go To Top Link -->
  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
  <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
<script>
  $(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

</body>

</html>