<?php
/**
 * This section ensures that Twilio gets a response.
 */
header('Content-type: text/xml');
?>

<Response>

  <Say voice="woman">Thanks for calling we will save you message send to vociemail.</Say>
</Response>
<?php 
/**
 * This section actually sends the email.
 */
$to      = "asharma3@seasiainfotech.com"; // Your email address.
$subject = "Message from {$_REQUEST['From']}";
$message = "You have received a message from {$_REQUEST['From']}.";
$message .= "To listen to this message, please visit this URL: {$_REQUEST['RecordingUrl']}";
$headers = "From: voicemail@twimlets.com"; // Who should it come from?

mail($to, $subject, $message, $headers);
?>