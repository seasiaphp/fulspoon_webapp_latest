



<?php if ($this->session->flashdata('error_message') != '') { ?>
    <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
<?php } else { ?>
    <div class="alert alert-danger" role="alert" style="display:none;"></div>
<?php } ?>
<?php if ($this->session->flashdata('success_message') != '') { ?>
    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
<?php } else { ?>
    <div class="alert alert-success" role="alert" style="display:none;"></div>
<?php } ?>
<!-- ===== Start Section Main ===== -->
<section class="main-sec">

    <div class="alert alert-danger hide" role="alert"></div>
    <div class="alert alert-success hide" role="alert"></div>

    <div class="container-fluid">
        <!-- Tabs -->
        <div class="order-tabs">

            <form name="form-order" id="form-order" action="<?php echo base_url() ?>owner/orders/accepted" method="post" >   
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="<?php echo base_url() ?>owner/orders/lists" aria-controls="new">
                            New (<?php echo $allcounts['newcount']; ?>)</a></li>
                    <li role="presentation"  class="active"><a href="<?php echo base_url() ?>owner/orders/accepted" aria-controls="accepted">
                            Accepted (<span id="accepted"><?php echo $allcounts['accepted']; ?></span>)</a></li>
                    <li role="presentation"><a href="<?php echo base_url() ?>owner/orders/cancelled" aria-controls="declined" >
                            Declined </a></li>

                    <li role="presentation"><a href="<?php echo base_url() ?>owner/orders/all" aria-controls="all" >
                            All </a></li>

                    <li class="tog_tab pull-right" role="presentation">
                        <select name="search_sel" id="search_sel" class="form-control">
                            <option value="">Filter by</option>
                            <option value="Completed" <?php if ($orderstatus == 'Completed') { ?> selected="true" <?php } ?> >Completed</option>
                            <option value="Accepted" <?php if ($orderstatus == 'Accepted') { ?> selected="true" <?php } ?>>Accepted</option>
                        </select>
                    </li> 

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- New -->
                    <div role="tabpanel" class="tab-pane fade in active" id="tab-new">
                        <!-- DESKTOP OR IPAD VIEW -->
                        <table class="table table-hover table-dekstop tbl_category">
                            <thead>
                                <tr>

                                    <th>Customer</th>
                                    <th>Created</th>
                                    <th>Expected</th>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th style="text-align:center;">Total</th>

                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (count($orderdetails) != 0) {
                                    foreach ($orderdetails as $items) {
                                        ?>
                                        <tr id="<?php echo $items['order_id']; ?>" class='trlink'>

                                            <td class="tdlink"><?php echo $items['first_name'] . ' ' . $items['last_name']; ?></th>
                                                <?php $datetimeformat = getConfigValue('time_format') . ' ' . getConfigValue('date_format'); ?>
                                            <td class="tdlink"><?php echo date($datetimeformat, strtotime($items['created_time'])); ?></td>
                                            <td class="tdlink"><?php echo date($datetimeformat, strtotime($items['delivery_time'])); ?></td>
                                            <td class="tdlink"><?php echo $items['order_ref_id']; ?></th>
                                            <td class="tdlink"><?php echo $items['order_type']; ?></td>
                                            <td style="text-align:center;" class="tdlink"><?php echo '$' . $items['total_amount']; ?></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="10">
                                            No Accepted Orders
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- END DESKTOP OR IPAD VIEW -->

                        <!-- TABLET VIEW -->
                        <table class="table table-tablet table-tablet">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>ID</th>
                                    <th>Type</th>

                                    <th style="text-align:center;">More</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (count($orderdetails) != 0) {
                                    foreach ($orderdetails as $items) {
                                        ?>
                                        <tr id="<?php echo $items['order_id']; ?>" class='trlink'>
                                            <td  class="tdlink"><?php echo $items['first_name'] . ' ' . $items['last_name']; ?></th>
                                            <td class="tdlink"><?php echo $items['order_ref_id']; ?></td>
                                            <td class="tdlink"><?php echo $items['order_type']; ?></th>

                                            <td style="text-align:center;" class="tdlink">
                                                <a href="javascript:void(0)" class="more-btn"></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="5">
                                            No Accepted Orders
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- END TABLET VIEW -->

                        <!-- MOBILE VIEW -->
                        <table class="table table-mobile">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Type</th>

                                    <th style="text-align:center;">More</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (count($orderdetails) != 0) {
                                    foreach ($orderdetails as $items) {
                                        ?>
                                        <tr id="<?php echo $items['order_id']; ?>" class='trlink'>
                                            <td class="tdlink"><?php echo $items['first_name'] . ' ' . $items['last_name']; ?></th>
                                            <td class="tdlink"><?php echo $items['order_type']; ?></td>

                                            <td class="tdlink" style="text-align:center;">
                                                <a href="javscript:void(0)" class="more-btn"></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="4">
                                            No Accepted Orders
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                        <!-- END MOBILE VIEW -->
                    </div>
                    <!-- End New -->

                    <!-- Accepted -->

                    <!-- End Accepted -->

                    <!-- Declined -->

                    <!-- End Declined -->

                    <!-- Late -->

                    <!-- End Late -->

                    <!-- All -->

                    <!-- End All -->
                </div>
                <!-- End Tab panes -->
        </div>

        <?php if (count($orderdetails) != 0) { ?>
            <div class="row" id="Table footer">
                <div class="col-lg-12 ">
                    <div class="col-offset-1 col-lg-2 pull-right">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-chevron-down"></i> 
                            </span>
                            <select name="limit" id="limit" class="form-control" >
                                <option value="5" <?php if ($limit == 5) { ?> selected="selected"<?php } ?> >5</option>
                                <option value="10" <?php if ($limit == 10) { ?> selected="selected"<?php } ?> >10</option>
                                <option value="20" <?php if ($limit == 20) { ?> selected="selected"<?php } ?> >20</option>
                                <option value="50" <?php if ($limit == 50) { ?> selected="selected"<?php } ?>>50</option>
                                <option value="100" <?php if ($limit == 100) { ?> selected="selected"<?php } ?>>100</option>
                                <option value="all" <?php if ($limit == 'all') { ?> selected="selected"<?php } ?>>ALL</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-offset-1 col-lg-10">
                        <div class="input-group">
                            <ul class="pagination pull-right" style="margin:0px;">
                                <?php /* ?>	<?php echo $this->pagination->create_links(); ?><?php */ ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </form>  
        <!-- End Tabs -->

        <!-- Order Detail Modal 1 -->
        <div class="order-detail-modal modal fade bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Order Close Modal -->
                    <a href="#" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                    <!-- End Order Close Modal -->

                    <!-- Order Modal Body -->
                    <div class="modal-body">

                    </div>
                    <!-- End Order Modal Body -->
                </div>
            </div>
        </div>

    </div>
</section>
<!-- ===== End Section Main ===== -->



<script>
    $(document).ready(function () {
        $('.trlink').click(function () {

            var order_id = $(this).attr("id");
            $('.loader_home').show();


            $.ajax({
                url: '<?php echo base_url() ?>owner/orders/details_ajax',
                type: 'POST',
                data: {'order_id': order_id},
                success: function (result) {

                    $(".order-detail-modal .modal-body").html(result);
                    $("#myModal1").modal("show");

                    $('.loader_home').hide();


                }
            });

            return false;
        });

    });
    $('body').on('click', '.completed', function () {

        var order_id = $("#order_id").val();
        //alert(order_id);
        $('.loader_home').show();
        var cnt = parseInt($('.accbadge').html()) - 1;
        //return false;
        $.ajax({
            url: '<?php echo base_url() ?>owner/orders/completeOrder',
            type: 'POST',
            data: {'order_id': order_id},
            success: function (result) {
                var datas = result.split("###");
//                 alert 
//                alert(datas[1]);
                var ccc = datas[1];
                $("#accepted").empty();
                $("#accepted").text(ccc);
                $('.accbadge').html(cnt);
                $('#' + order_id).remove();
                $('.alert-success').removeClass('hide');
                $('.alert-success').html('Order completed successfully');
                window.setTimeout(function () {
                    $('.alert-success').addClass('hide');
                }, 3000);

                var rowCount = $('.tbl_category tr').length;
                if (rowCount == '1') {
                    $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
                }

                $('.loader_home').hide();
                $("#myModal1").modal('hide');
                //window.load('<?php echo base_url() ?>orders/lists');

            }
        });

    });
    $(document).on('click', '.cancel', function (e) {
        var order_id = $("#order_id").val();


        e.preventDefault();

//        $.Zebra_Dialog('Are you sure you want to decline this order?', {
//            'type': 'question',
//            'title': 'Decline order',
//            'buttons': ['OK', 'Cancel'],
//            'onClose': function (caption) {
//                  alert("dasd");
//        if (caption == 'OK') {

        $('.loader_home').show();

        $.ajax({
            url: '<?php echo base_url() ?>owner/orders/cancelOrder',
            type: 'POST',
            data: {'order_id': order_id},
            success: function (result) {
                if (result.indexOf("success") >= 0)
                {
                    var datas = result.split("###");
                    var ccc = datas[1];
                    $("#accepted").empty();
                    $("#accepted").text(ccc);
                    $('#' + order_id).remove();
//                    $('.alert-success').empty();
                    $('.alert-success').show();
                    $('.alert-success').html('Order cancelled successfully');
                    var rowCount = $('.tbl_category tr').length;
                    if (rowCount == '1') {
                        $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
                    }

                    //window.location.href="<?php echo base_url() ?>orders/lists";
                }
                else
                {
                    $('.alert-danger').removeClass('hide');
                    $('.alert-danger').html('Unknown error occured');
                    $("html, body").animate({scrollTop: 0}, "fast");
                    window.setTimeout(function () {
                        $('.alert-danger').addClass('hide');
                        ;
                    }, 3000);
                }
                $("#myModal1").modal('hide');
                $('.loader_home').hide();
            }
        });
        return true;
//                } else {
//                    return false;
//                }
//            }
//        });




    });
    $('body').on('click', '.accept', function () {
        var order_id = $("#order_id").val();
        $('.loader_home').show();
        //return false;
        var cnt = parseInt($('.accbadge').html()) + 1;

        $.ajax({
            url: '<?php echo base_url() ?>owner/orders/acceptOrder',
            type: 'POST',
            data: {'order_id': order_id},
            success: function (result) {
                $('.acbtncls').hide();
                $('.cmpbtncls').show();
                $('.accbadge').html(cnt);
                //alert($('#hidecount').val());
                var cnt11 = parseInt($('#hidecount').val()) - 1;
                $('#hidecount').val(cnt11);
                /*$('#row_'+order_id).remove();
                 $('.alert-success').show();
                 $('.alert-success').html('order accepted successfully');
                 var rowCount = $('.tbl_category tr').length;
                 if(rowCount=='1'){
                 $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
                 }
                 
                 $('.loader_home').hide();
                 */
                $('.loader_home').hide();
            }
        });

    });

    $('body').on('click', '.refund', function () {
        $("#refund_input").show();
        $('.acbtncls').hide();
        $('.cmpbtncls').show();
        $("#refund_amount").val($("#total_amount").val());
        $("#refund_input").removeClass('has-error');
        $("#refund_input .input-group").attr('data-original-title', 'Enter refund amount');

    });

    $('body').on('click', '.refund_cancel', function () {

        $("#refund_input").hide();
        $('.acbtncls').show();
        $('.cmpbtncls').hide();

    });

    $('body').on('click', '.continue', function () {
        var amount = parseFloat($("#refund_amount").val());
        var total_amount = parseFloat($("#total_amount").val());
        var order_id = $("#order_id").val();
        if (amount > 0 && amount <= total_amount) {
            $("#refund_input").hide();
            $('.acbtncls').hide();
            $('.cmpbtncls').hide();
            $("#refund_input").removeClass('has-error');
            $("#refund_input .input-group").attr('data-original-title', 'Enter refund amount');
            $('.loader_home').show();
            $.ajax({
                url: '<?php echo base_url() ?>owner/orders/OrderPartialRefund',
                type: 'POST',
                data: {'order_id': order_id, 'amount': amount},
                success: function (result) {
                    $('.loader_home').hide();
                    if (result.indexOf("success") >= 0)
                    {
                        $(".refund").removeClass('.refund');
                        $('.alert-success').removeClass('hide');
                        $('.alert-success').html('Refund successfully');
                        window.setTimeout(function () {
                            $('.alert-success').addClass('hide');
                        }, 3000);

                        $("html, body").animate({scrollTop: 0}, "fast");
                        //setTimeout(function(){window.location.href="<?php echo base_url() ?>/orders/details/"+order_id;}, 1000);
                    }
                    else
                    {
                        $('.acbtncls').show();
                        $('.alert-danger').removeClass('hide');
                        $('.alert-danger').html('Unknown error occured');
                        $("html, body").animate({scrollTop: 0}, "fast");
                        window.setTimeout(function () {
                            $('.alert-danger').addClass('hide');
                            ;
                        }, 3000);
                    }

                    $("#myModal1").modal('hide');

                }
            });

        }
        else
        {
            $("#refund_input").addClass('has-error');
            $("#refund_input .input-group").attr('data-original-title', 'Invalid amount');

        }
    });
    $(document).ready(function () {
        $(".customer-detail.tablet-res dl dd").after("<div class='line-bottom'></div>")
    });

//    $('body').on('click', '.completed', function () {
//        var order_id = $(this).attr("data-attr");
//        $('.loader_home').show();
//        //return false;
//        $.ajax({
//            url: '<?php echo base_url() ?>owner/orders/completeOrder',
//            type: 'POST',
//            data: {'order_id': order_id},
//            success: function (result) {
//
//
//                $('#row_' + order_id).remove();
//                $('.alert-success').show();
//                $('.alert-success').html('order completed successfully');
//                var rowCount = $('.tbl_category tr').length;
//                if (rowCount == '1') {
//                    $('.tbl_category').append('<tr><td colspan="5">No Orders</td></tr>');
//                }
//
//                $('.loader_home').hide();
//
//
//            }
//        });
//
//    });

    $('body').on('change', '#search_sel', function () {
        var search_sel = $('#search_sel').val();
        $('#form-order').submit();
    });
    //Change Limit of pagination
    $(document).on('change', '#limit', function () {
        $("#form-order").attr("action", "<?php echo base_url() ?>owner/orders/accepted");
        $("#form-order").submit();
        return true;
    });


    $('#btn_search').click(
            function () {
                $("#form-order").attr("action", "<?php echo base_url() ?>owner/orders/accepted");
                $("#form-order").submit();
                return true;
            });

    $(document).on('change', '#status', function () {
        $("#form-order").attr("action", "<?php echo base_url() ?>owner/orders/accepted");
        $("#form-order").submit();
        return true;
    });
    // END: Change Limit of pagination

</script>
