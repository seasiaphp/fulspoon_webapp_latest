







<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cust_admin.css" media="screen">






<!-- Css3 Transitions Styles  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" media="screen">
<!-- Color CSS Styles  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/colors/red.css" title="red" media="screen" />
<!-- Margo JS  -->



  <!--[if IE 8]><script src="<?php echo base_url(); ?>http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="<?php echo base_url(); ?>http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->



<body>

    <!-- Container -->
    <div id="container">

        <!-- Start Header -->  <!-- Start Header ( Logo & Naviagtion ) -->

        <!-- End Header ( Logo & Naviagtion ) -->

    </header>
    <!-- End Header -->

    <!-- Start Content -->
    <div id="content">
        <div class="" style="background-color:#fff;">
            <div class="">
                <div class=""> 
                    <h2 class="pageheader_report">Subscription Payments to Merchants</h2>


                    <form name="MasterForm" id="MasterForm">

                        <div id="report_tbl">
                            <div class="action_bar">
                                <div class="col-sm-6">
                                    <div class="col-sm-4 pad0">SELECT MONTH</div>
                                    <div class="col-sm-8 pad0">
                                        <div>
                                            <?php echo $merchants_details['month']; ?>
                                            <select class="month form-control" id="month" onChange="change_status1()" name="month" style="width: auto;">
                                            <option value="">Select Month</option>
                                                <option value="<?php echo $merchants_details1['month']; ?>"><?php if ($merchants_details1['month'] == 1) { ?>Jan<?php } ?>
                                                    <?php if ($merchants_details1['month'] == 2) { ?>Feb<?php } ?>
                                                    <?php if ($merchants_details1['month'] == 3) { ?>Mar <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 4) { ?>Apr <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 5) { ?>May <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 6) { ?>Jun <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 7) { ?>Jul <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 8) { ?>Aug <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 9) { ?>Sep <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 10) { ?>Oct <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 11) { ?>Nov <?php } ?>
                                                    <?php if ($merchants_details1['month'] == 12) { ?>Dec <?php } ?>
                                                </option>
                                                <option value="1">Jan</option>
                                                <option value="2">Feb</option>
                                                <option value="3">Mar</option>
                                                <option value="4">Apr</option>
                                                <option value="5">May</option>
                                                <option value="6">Jun</option>
                                                <option value="7">Jul</option>
                                                <option value="8">Aug</option>
                                                <option value="9">Sep</option>
                                                <option value="10">Oct</option>
                                                <option value="11">Nov</option>
                                                <option value="12">Dec</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-4 pad0">SELECT YEAR</div>
                                    <div class="col-sm-8 pad0">

                                        <div>
                                            <select class="month form-control" id="year" onChange="change_status1()" name="year" style="width: auto;">
                                            <option value="">Select Year</option>
                                                <option value="<?php echo $merchants_details1['year']; ?>"><?php echo $merchants_details1['year']; ?></option>
                                                <?php for ($i = date("Y"); $i >= (date("Y") - 25); $i--) { ?>
                                                    <option value="<?php echo $i; ?> "><?php echo $i; ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>


                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>






                            <div class="action_bar">
                                <div class="col-sm-6">
                                    <div class="col-sm-4 pad0">PAYMENT TYPE</div>
                                    <div class="col-sm-8 pad0">
                                        <div>

                                            <select class="month form-control" onChange="change_status1()" name="pay_method" id="pay_method" style="width: auto;">
                                                <option value="">All</option>
                                                <option value="ACH" <?php if ($_REQUEST['pay_method'] == 'ACH') { ?>selected<?php } ?>>ACH</option>
                                                <option value="Check" <?php if ($_REQUEST['pay_method'] == 'Check') { ?>selected<?php } ?> >Check</option>

                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-4 pad0">STATUS</div>
                                    <div class="col-sm-8 pad0">

                                        <div>
                                            <select class="month form-control" name="status" onChange="change_status1()" id="status" style="width: auto;">
                                                <option value="">All</option>
                                                <option value="0">Paid</option>
                                                <option value="1">Mailed</option>


                                            </select>
                                        </div>


                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>




                            <div class="col-sm-12">

                            </div>

                            <div class="clearfix"></div>





                            <table class="col-md-12 table-bordered  table-condensed cf">
                                <thead>
                                    <tr>
                                        <td class="numeric header_tblrt">Merchant Name</td>
                                        <td class="numeric header_tblrt">Date</td>
                                        <td class="numeric header_tblrt">Payment Type</td>
                                        <td class="numeric header_tblrt">Transaction Id</td>
                                        <td class="numeric header_tblrt">Status</td>
                                        <td class="numeric header_tblrt">Amount</td>
                                    </tr>
                                </thead>
                                <tbody id="pay_tab">

                                    <?php
                                    $priz = 0;
                                    foreach ($merchants_details AS $data) {
                                        ?>
                                        <tr>

                                            <td style="text-align:center"><?php echo $data['restaurant_name']; ?></td>
                                            <td style="text-align:center"><?php echo date("m-d-Y", strtotime($data['create_date'])); ?></td>
                                            <td style="text-align:center"><?php echo $data['account_type']; ?></td>

                                            <td style="text-align:center"><?php
                                                if ($data['account_type'] == 'ACH') {
                                                    echo $data['recipient_response_id'];
                                                } else {
                                                    ?><input type="text" class="form-control" value="<?php echo $data['recipient_response_id']; ?>"> <?php } ?></td>
                                            <td style="text-align:center"><?php
                                                if ($data['account_type'] == 'ACH') {
                                                    if ($data['recipient_response_id'] == '') {
                                                        echo 'Failed';
                                                    } else {
                                                        echo 'Paid';
                                                    }
                                                } else {
                                                    if ($data['recipient_id'] != '') {
                                                        echo 'Mailed';
                                                    }
                                                }
                                                ?>

                                            </td>


                                            <td style="text-align:center"><?php echo '$' . ($data['amount']); ?></td>
                                        </tr>
                                        <?php
                                        $priz = $priz + ($data['restaurant_count'] * $admin_val['contract_price']);
                                    }
                                    ?>
                                    <tr>
                                        <td  class="numeric" colspan="5">TOTAL</td>
                                        <td colspan="" style="text-align:center"><?php echo '$' . $priz; ?> </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <br> <br> <br> <br> <br>
                    </form>


                    <!--end of .table-responsive-->
                </div>
            </div>
        </div>
    </div>

</div>
<!-- End content -->
<div class="clearfix"></div>

<!-- Start Footer Section --><!-- End Footer Section -->



</div>
<!-- End Container -->

<!-- Go To Top Link -->
<a href="<?php echo base_url(); ?>#" class="back-to-top"></a>
<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>

<script>
                                                function change_status() {
                                                    $("#MasterForm").attr("action", "<?php echo site_url("payment_report/merchants_details"); ?>");
                                                    $("#MasterForm").submit();
                                                    return true;

                                                }
                                                function change_status12() {

                                                    if ($('#year').val() != '' && $('#month').val() != '') {
                                                        $("#MasterForm").attr("action", "<?php echo site_url("payment_report/merchants_details"); ?>");
                                                        $("#MasterForm").submit();
                                                        return true;
                                                    }

                                                }


                                                function change_status1() {


                                                    var is_check = '';
                                                    var month = $("#month").val();
                                                    var year = $("#year").val();
                                                    var p_type = $("#pay_method").val();
                                                    var s_status = $("#status").val();

                                                    if (($("#month").val() != '' && $("#year").val() != '') || (($("#month").val() != '' && $("#year").val() != '') && (p_type || s_status))) {

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo base_url(); ?>payment_report/ajaxadmS",
                                                            data: {month: month, year: year, p_type: p_type, s_status: s_status, is_check: is_check},
                                                            cache: false,
                                                            success: function (response) {
                                                                $("#pay_tab").empty();
                                                                $("#pay_tab").html(response);

                                                            }
                                                        });
                                                    }
                                                }


                                                $(document).ready(function () {
                                                    $(document).on('click', '.check_test123456', function () {
                                                    

                                                        var is_check1 = $(this).val();
                                                        var is_check = $("#" + is_check1 + '_id').val();
                                                       
                                                        var month = $("#month").val();
                                                        var year = $("#year").val();

                                                        var p_type = $("#pay_method").val();
                                                        var s_status = $("#status").val();




                                                        if (($("#month").val() != '' && $("#year").val() != '') || (($("#month").val() != '' && $("#year").val() != '') && (p_type || s_status))) {


                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo base_url(); ?>payment_report/ajaxadmS",
                                                                data: {month: month, year: year, p_type: p_type, s_status: s_status, is_check: is_check, resci_id: is_check1},
                                                                cache: false,
                                                                success: function (response) {



                                                                    $("#pay_tab").empty();

                                                                    $("#pay_tab").html(response);


                                                                }
                                                            });



                                                        }


                                                    });
                                                });



</script>








</body>
