<?php
//echo "<pre>";
//print_r($response_datas);
//exit;
?>
<!doctype html>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Replimatic | Signup</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Replimatic Signup">
        <meta name="author" content="iThemesLab">


        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- FORM CSS Styles  -->
      <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/form.css" media="screen">-->

        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>


  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <!-- Start Header ( Logo & Naviagtion ) -->
            <div class="navbar navbar-default navbar-top greenbdr">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="index.html">
                            <img alt="" src="<?= base_url() ?>images/replimatic_logo.png">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="navbar-collapse collapse borderbg">       
                        <!-- Stat Search -->
                        <div class="buttons-side">
                            <a class="btn btn-default btnlogin" href="#" role="button">LOGIN</a>
                            <a class="btn btn-default btnsignup" href="#" role="button">SIGN UP</a>
                        </div>
                        <!-- End Search -->
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="contact.html" class="linkcolor">For Restaurants Owners</a></li>             
                        </ul>
                        <!-- End Navigation List -->

                    </div>
                </div>

                <!-- Mobile Menu Start -->
                <ul class="wpb-mobile-menu">         
                    <li><a href="contact.html">For Restaurants Owners</a></li> 
                    <li><a href="contact.html">Login</a></li>  
                    <li><a href="contact.html">Sign Up</a></li>               
                </ul> 
                <!-- Mobile Menu End -->

            </div>
            <!-- End Header ( Logo & Naviagtion ) -->

        </header>
        <!-- End Header -->



        <!-- Start Content -->
        <div id="content">
            <div class="container">

                <!--begin tabs going in wide content -->
                <ul class="nav nav-tabs tabsignup content-tabs hidden-xs" id="maincontent" role="tablist">
                    <li id="home_tes" class="col-lg-4 tablist active" ><a data-parent="" class="js-tabcollapse-panel-heading" href="javascript:" role="tab" data-toggle="tab"><span>1</span>Replimatic DETAILS</a></li>
                    <li id="profile_tes" class="col-lg-4 tablist " dis><a data-parent="" class="js-tabcollapse-panel-heading" href="javascript:"  role="tab" data-toggle="tab"><span>2</span>Select Reataurants</a></li>
                    <li id="confirm_tes" class="col-lg-4 tablist "><a data-parent="" class="js-tabcollapse-panel-heading" href="javascript:" role="tab" data-toggle="tab"><span>3</span>Confirmation</a></li>
                </ul>

                <div class="panel-group visible-xs" id="maincontent-accordion"><div class="panel panel-default">   <div class="panel-heading">      <h4 class="panel-title">      <a data-parent="#maincontent-accordion" class="js-tabcollapse-panel-heading" href="#home-collapse" role="tab" data-toggle="collapse">REPLIMATIC DETAILS</a></h4>   </div>   <div id="home-collapse" class="panel-collapse collapse in">       <div class="panel-body js-tabcollapse-panel-body">                                          
                                <h3 class="head30 text-center col-lg-12">Create your Account <a href="#" class="pull-right greenlink">Already have an account? Login</a></h3>
                                <div class="form-group text-center"><button class="btn btn-default fbsignup" type="submit"><i class="fa fa-facebook"></i> SIGN UP WITH FACEBOOK</button></div>
                                <div class="col-lg-12 text-center orsep"><span>OR</span></div>

                                <!-- SIGN UP ---->
                                <div class="col-lg-12">
                                    <form>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">First Name</div>
                                            <div class="col-lg-6"><input type="text" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Last Name</div>
                                            <div class="col-lg-6"> <input type="text" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Email</div>
                                            <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Password</div>
                                            <div class="col-lg-6"><input type="password" class="form-control inputsignup" id="exampleInputpsd" placeholder="Password"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN UP</button></div>
                                        <div class="form-group text-center col-lg-pull-3">By signing up you're accepting our <a href="">terms of use</a> and <a href="#">privacy policy.</a></div>
                                    </form>

                                </div>
                                <!-- END SIGN UP ---->
                            </div>   </div></div>


                    <div class="panel panel-default ">   <div class="panel-heading">      <h4 class="panel-title">      <a data-parent="#maincontent-accordion" class="js-tabcollapse-panel-heading collapsed" href="#profile-collapse" role="tab" data-toggle="collapse">SELECT RESTAURANTS</a></h4>   </div>   <div id="profile-collapse" class="panel-collapse collapse ">                  <div class="panel-body js-tabcollapse-panel-body">                                          
                                <!-- SELECT REATAURANT -->
                                <div class="col-lg-3 col-md-3 col-sm-4 filterwrap">
                                    <h3>Cuisine <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <div class="checkbox"><label><input type="checkbox">Mexican - 2</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Pizza -2</label></div>
                                        <div class="checkbox"><label><input type="checkbox">BBQ - 1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Hamburgers -1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Healthy -1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Latin American -1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Mediterranean -1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Pitas -1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Sandwiches -1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Vegetarian -1</label></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h3>Rating <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                        <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                        <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                        <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                        <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                        <a>& up</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h3>Price <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <a href="#" class="price active">$</a>
                                        <a href="#" class="price">$</a>
                                        <a href="#" class="price">$</a>
                                        <a href="#" class="price">$</a>
                                        <a href="#" class="price">$</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h3>FEATURE <a href="#">Clear</a></h3>
                                    <div class="col-lg-12">
                                        <div class="checkbox"><label><input type="checkbox">Coupons Available</label></div>
                                        <div class="checkbox"><label><input type="checkbox">New</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Order Tracking</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Open Now 6:58 PM</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Free Delivery-1</label></div>
                                        <div class="checkbox"><label><input type="checkbox">Hide Delivery Services -1</label></div>                   
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8  select-wrapper">
                                    <div class="col-lg-12 searchitem"><input type="email" class="form-control searchhide" id="inputEmail3" placeholder="Search Restaurants"></div>
                                    <div class="col-md-6 resultcount">
                                        5 Results near by you
                                    </div>
                                    <div class="col-md-5 resultcount">
                                        <div class="form-group">
                                            <label for="inputPassword" class="col-sm-3 control-label">Sort by</label>
                                            <div class="col-sm-9">
                                                <select class="form-control">...</select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- SEARCH RESULT-->
                                    <div class="result-wrapper">
                                        <div class="col-md-2 text-center"><img src="<?= base_url() ?>images/thumb100.png" alt="" class="thumb100"/></div>
                                        <div class="col-md-5 restaurantinfo text-center">
                                            <h5>Bella Pizza II & Restaurant</h5>Lunch Specials, Pizza, Sandwiches, Wraps
                                            <p><i class="fa fa-map-marker"></i> Lunch Specials, Pizza, Sandwiches, Wraps</p>                  
                                        </div>
                                        <div class="col-md-5">
                                            <div class="col-md-4"><img src="<?= base_url() ?>images/dolors.png" alt="" class="middle"/></div>
                                            <div class="col-md-4"><p class="middle"><img src="<?= base_url() ?>images/star-rating.png" alt=""/><br>20 ratings</p></div>
                                            <div class="col-md-4"><p class="middle deleverystat">FREE<br><span>Delivery</span></p></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- END SEARCH RESULT-->
                                    <!-- SEARCH RESULT-->

                                    <!-- END SEARCH RESULT-->
                                    <!-- SEARCH RESULT-->

                                    <!-- END SEARCH RESULT-->
                                </div>
                                <div class="clearfix"></div>

                                <!-- END SELECT REATAURANT -->
                            </div>
                        </div></div>



                    <div class="panel panel-default">   <div class="panel-heading">      <h4 class="panel-title">      <a data-parent="#maincontent-accordion" class="js-tabcollapse-panel-heading collapsed" href="#dropdown1-collapse" tabindex="-1" role="tab" data-toggle="collapse">CONFIRMATION</a></h4>   </div>   <div id="dropdown1-collapse" class="panel-collapse collapse ">       
                            <div class="panel-body js-tabcollapse-panel-body">                                          
                                <!-- CONFIRMATION START -->
                                <div class="payment-wrapper">
                                    <h4>PAYMENT INFORMATION</h4>
                                    <form>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Name on the Card *</div>
                                            <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="First Name"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">Credit card Number *</div>
                                            <div class="col-lg-6"> <input type="email" class="form-control inputsignup" id="exampleInputNname" placeholder="Last Name"></div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-3 labelsignup">CVV *</div>
                                            <div class="col-lg-3 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Email"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-3 col-md-4 labelsignup">Expiration Date *</div>
                                            <div class="col-lg-3 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                            <div class="col-lg-3 col-md-4"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN UP</button></div>
                                        <div class="form-group text-center col-lg-pull-3">By signing up you're accepting our <a href="">terms of use</a> and <a href="#">privacy policy.</a></div>
                                    </form>
                                </div>
                                <!-- CONFIRMATION END -->
                            </div>   
                        </div></div>


                </div>
                <!--/.nav-tabs.content-tabs -->


                <div class="tab-content hidden-xs ">

                    <div class="tab-pane active fade in" id="home">       
                        <h3 class="head30 text-center col-lg-12">Create your Account <a href="#" class="pull-right greenlink">Already have an account? Login</a></h3>
                        <div class="form-group text-center"><button onClick="FbLogin()" class="btn btn-default fbsignup" type="submit"><i class="fa fa-facebook"></i> SIGN UP WITH FACEBOOK</button></div>
                        <div class="col-lg-12 text-center orsep"><span>OR</span></div>

                        <!-- SIGN UP ---->
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">First Name</div>
                                    <div class="col-lg-6"><input type="text" class="form-control inputsignup" id="fname" placeholder="First Name"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">Last Name</div>
                                    <div class="col-lg-6"> <input type="text" class="form-control inputsignup" id="lname" placeholder="Last Name"></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">Email</div>
                                    <div class="col-lg-6"><input type="email" class="form-control inputsignup" id="email" placeholder="Email"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-3 labelsignup">Password</div>
                                    <div class="col-lg-6"><input type="password" class="form-control inputsignup" id="psd" placeholder="Password"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" name="change_link" id="change_link"  type="button"> SIGN UP</button></div>
                                <div class="form-group text-center col-lg-pull-3">By signing up you're accepting our <a href="">terms of use</a> and <a href="#">privacy policy.</a></div>
                            </form>

                        </div>
                        <!-- END SIGN UP ---->
                        <div class="clearfix"></div>
                    </div><!--/.tab-pane -->

                    <div class="tab-pane" id="profile">       
                        <!-- SELECT REATAURANT -->
                        <div class="col-lg-3 col-md-3 col-sm-4 filterwrap">
                            <h3>Cuisine <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <div class="checkbox"><input type="checkbox"  id="male"><label for="male">Mexican - 2</label></div>
                                <div class="checkbox"><input type="checkbox" id="male1"><label for="male1">Pizza -2</label></div>
                                <div class="checkbox"><input type="checkbox" id="male2"><label for="male2">BBQ - 1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male3"><label for="male3">Hamburgers -1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male4"><label for="male4">Healthy -1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male5"><label for="male5">Latin American -1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male6"><label for="male6">Mediterranean -1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male7"><label for="male7">Pitas -1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male8"><label for="male8">Sandwiches -1</label></div>
                                <div class="checkbox"><input type="checkbox" id="male9"><label for="male9">Vegetarian -1</label></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <h3>Rating <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                <a href="#" class="rating"><img src="<?= base_url() ?>images/star.png" alt=""/></a>
                                <a>& up</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <h3>Price <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <a href="#" class="price active">$</a>
                                <a href="#" class="price">$</a>
                                <a href="#" class="price">$</a>
                                <a href="#" class="price">$</a>
                                <a href="#" class="price">$</a>
                            </div>
                            <div class="clearfix"></div>
                            <h3>FEATURE <a href="#">Clear</a></h3>
                            <div class="col-lg-12">
                                <div class="checkbox"><label><input type="checkbox" onClick="test()"  name="locationthemes" id="checkbox-1[]" value="1" class="custom">Coupons Available</label></div>
                                <div class="checkbox"><label><input type="checkbox" onClick="test()" name="locationthemes" id="checkbox-1[]" value="2" class="custom">New</label></div>
                                <div class="checkbox"><label><input type="checkbox" onClick="test()" name="locationthemes" id="checkbox-1[]" value="3" class="custom">Order Tracking</label></div>
                                <div class="checkbox"><label><input type="checkbox" onClick="test()"  name="locationthemes" id="checkbox-1[]" value="4" class="custom">Open Now 6:58 PM</label></div>
                                <div class="checkbox"><label><input type="checkbox"  onClick="test()"  name="locationthemes" id="checkbox-1[]" value="5" class="custom">Free Delivery-1</label></div>
                                <div class="checkbox"><label><input type="checkbox"  onClick="test()"  name="locationthemes" id="checkbox-1[]" value="6" class="custom">Hide Delivery Services -1</label></div>                   
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8  select-wrapper">
                            <div class="col-lg-12 searchitem"><input type="email" class="form-control searchhide" id="inputEmail3" placeholder="Search Restaurants"></div>
                            <div class="col-md-6 resultcount">
                                5 Results near by you
                            </div>
                            <div class="col-md-6 resultcount">
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-3 control-label">Sort by</label>
                                    <div class="col-sm-9">
                                        <select class="form-control">...</select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- SEARCH RESULT-->
                            <?php
                            foreach ($response_datas as $hotel) {
//     print_r($hotel['hotel_data']->photos);
                                $rating = round($hotel['hotel_data']->rating);
                                $rate_differ = 5 - $rating;
                                $image = $hotel['hotel_data']->photos[0]->photo_reference;
//                                   exit;                            
                                ?>
                                <div class="result-wrapper">
                                    <div class="col-md-2 text-center"><img src='https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&photoreference=<?= $image ?>&key=<?= $key ?>' class='img-responsive' alt="" class="thumb100"/></div>
                                    <div class="col-md-5 restaurantinfo">
                                        <h5><?= $hotel['hotel_data']->name; ?></h5><?php
                                        foreach ($hotel['cuisines'] as $cusine) {
                                            echo $cusine['cusine_name'] . ",";
                                        }
                                        ?>
                                        <p><i class="fa fa-map-marker"></i><?= $hotel['hotel_data']->formatted_address; ?></p>                  
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-4"><img src="<?= base_url() ?>images/dolors.png" alt="" class="middle"/></div>
                                        <div class="col-md-4"><p class="middle"><img src="<?= base_url() ?>images/star-rating.png" alt=""/><br><?= $hotel['hotel_data']->user_ratings_total; ?> ratings</p></div>
                                        <div class="col-md-4"><p class="middle deleverystat">FREE<br><span>Delivery</span></p></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <?php } ?>
                            <!-- END SEARCH RESULT-->
                            <!-- SEARCH RESULT-->

                            <!-- END SEARCH RESULT-->
                            <!-- SEARCH RESULT-->

                            <!-- END SEARCH RESULT-->
                        </div>
                        <div class="clearfix"></div>

                        <!-- END SELECT REATAURANT -->
                    </div><!--/.tab-pane -->

                    <div class="tab-pane" id="confirm">       
                        <!-- CONFIRMATION START -->
                        <div class="col-md-8">
                            <div class="payment-wrapper">
                                <h4>PAYMENT INFORMATION</h4>
                                <form>
                                    <div class="form-group">
                                        <div class="col-lg-4 labelsignup">Name on the Card *</div>
                                        <div class="col-lg-8"><input type="text" class="form-control inputsignup" id="cname" placeholder="Name" required></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4 labelsignup">Credit card Number *</div>
                                        <div class="col-lg-8"> <input type="text" class="form-control inputsignup" id="crno" placeholder="Credit card Number" onkeypress='validate(event)' required  maxlength="16"></div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-4 labelsignup">CVV *</div>
                                        <div class="col-lg-8"><input type="text" class="form-control inputsignup" id="cvv" placeholder="CVV" onkeypress='validate(event)' maxlength="3" required></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4 col-md-4 labelsignup">Expiration Date *</div>
                                        <div class="col-lg-4 col-md-4"><div class="styled"><select name="exmonth" id="exmonth" ><option>Month</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></div></div>
                                        <div class="col-lg-4 col-md-4"><div class="styled"><select name="card_year" id="card_year"><option>Year</option><?php for ($i = date('Y'); $i < (date('Y') + 24); $i++) { ?><option value="<?php echo $i; ?>"><?php echo $i; ?> </option><?php } ?></select></div></div>
                                        <div class="clearfix"></div>
                                    </div>

                            </div>
                            <div class="payment-wrapper">
                                <h4>BILLING ADRESS</h4>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 1 *</div>
                                    <div class="col-lg-8"><input type="text" class="form-control inputsignup" id="add1" placeholder="Address" required></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 2 *</div>
                                    <div class="col-lg-8"> <input type="email" class="form-control inputsignup" id="add2" placeholder="Address" required></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">City *</div>
                                    <div class="col-lg-8"><input type="text" class="form-control inputsignup" id="city" placeholder="City" required></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 labelsignup">State *</div>
                                    <div class="col-lg-8 col-md-4"><div class="styled"><select id="state" name="state"><option value="Month">Month</option><option value="Firefox">Firefox</option><option value="Webkit">Webkit</option></select></div></div>                        
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Zip Code *</div>
                                    <div class="col-lg-8 col-md-4"><input type="text" class="form-control inputsignup" id="zipcode" placeholder="Zip Code"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Save Card as</div>
                                    <div class="col-lg-8 col-md-4"><input type="text" class="form-control inputsignup" id="save_crd" placeholder="Save Card as"></div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                            <div class="payment-wrapper">
                                <h4>delivery address</h4>

                                <div class="col-lg-offset-4"><input type="checkbox" id="same" name="same" onChange="check_btn_add()" /><label for="male">Same as Billing address</label></div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 1 *</div>
                                    <div class="col-lg-8"><input type="text" class="form-control inputsignup" id="radd1" placeholder="Address "></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Address line 2 *</div>
                                    <div class="col-lg-8"> <input type="text" class="form-control inputsignup" id="radd2" placeholder="Address "></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">City *</div>
                                    <div class="col-lg-8 col-md-4"><input type="text" class="form-control inputsignup" id="rcity" placeholder="City"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 labelsignup">State *</div>
                                    <div class="col-lg-8 col-md-4"><div class="styled"><select id="rstate"><option value="Month">Month</option><option value="Firefox">Firefox</option><option value="Webkit">Webkit</option></select></div></div>                        
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Zip Code *</div>
                                    <div class="col-lg-4 col-md-4"><input type="text" class="form-control inputsignup" id="rzipcode" placeholder="Zip Code"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Save Card as</div>
                                    <div class="col-lg-4 col-md-4"><input type="text" class="form-control inputsignup" id="rsave_crd" placeholder="Save Card as"></div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                            <div class="payment-wrapper">
                                <h4>HOW DID YOU HEAR ABOUT Replimatic?</h4>

                                <div class="form-group">
                                    <div class="col-lg-4 labelsignup">Choose One</div>
                                    <div class="col-lg-5"><div class="styled"><select id="rstate"><option>Month</option><option>Firefox</option><option>Webkit</option></select></div></div>
                                    <div class="clearfix"></div>
                                </div> 
                                <div class="form-group"><div class="col-lg-offset-4">
                                        <div class="col-md-12">
                                            <button class="btn btn-default btngreen btn-block" onClick="card_val_btn()" id="card_val" type="button">Confirm Purchase</button></div></div></div>
                                <div class="form-group"><div class="col-lg-offset-4">I agree to the<a href="">Terms of Use</a> and <a href="#">Privacy Policy.</a></div></div>                     
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="stickdiv margin-top">
                                <div class="stckheader">YOUR Replimatic SUMMARY</div>
                                <div>
                                    <div class="col-md-12 packagelabel">packages <span>$19.95</span></div>
                                    <div class="cartwrap">
                                        <h6>Restaurants</h6>
                                        <ul>
                                            <li>Bella Pizza II & Restaurant</li>
                                            <li>Cafe De Novo</li>
                                        </ul>
                                    </div>
                                    <div class="carttotal">GRAND TOTAL  <span>$19.95</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"><!-- --></div>
                        <!-- CONFIRMATION END -->
                    </div><!--/.tab-pane -->

                </div>
            </div>
        </div>
        <!-- End content -->


        <!-- Start Footer Section -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4"> 
                        <ul class="footer-nav">
                            <li><a href="#">Restaurant Owners</a>      
                            <li><a href="#">About</a>        
                            <li><a href="#">Contact Us</a>       
                            <li><a href="#">Privacy policy</a> 
                        </ul>
                    </div>
                    <div class="col-md-4 text-center padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/> <p>&copy; 2015 Replimatic. All rights reserved. </p>  </div>
                    <div class="col-md-3 col-md-offset-1 socialnav">
                        Follow us!
                        <p>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </p>
                    </div>
                </div>
                <!-- .row -->
            </div>
        </footer>
        <!-- End Footer Section -->



    </div>
    <!-- End Container -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
    <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
    <script>

                                                $("#change_link").click(function () {

                                                    $('#home_tes').removeClass('active');
                                                    $('#profile_tes').addClass('active');

                                                    $("#profile").html();
                                                    var fname = $('#fname').val();
                                                    var lname = $('#lname').val();
                                                    var email = $('#email').val();
                                                    var psd = $('#psd').val();
                                                    //$("#myModal").html("");
                                                    $.ajax({
                                                        type: "post",
                                                        url: "<?php echo base_url(); ?>member_1/email_exist",
                                                        data: {'fname': fname, 'lname': lname, 'email': email, 'psd': psd},
                                                        success: function (data) {
                                                            if (data == 'error')
                                                            {
                                                                alert("haiii");
                                                            }
                                                            if (data == 'success')
                                                            {
                                                                //document.getElementById('profile').getAttribute('href');
                                                                //window.location=document.getElementById('profile').href;
                                                                $("div#home").hide();
                                                                $("div#profile").show();
                                                                $('#home_tes').removeClass('active');
                                                                $('#profile_tes').addClass('active');
                                                                // alert("test");
                                                            }

                                                            alert(data);
                                                        }
                                                    });


                                                })


                                                $("#home_tes").click(function () {
                                                  
                                                })
                                                $("#profile_tes").click(function () {
                                                   
                                                })

                                                $("#confirm_tes").click(function () {
                                                  

                                                })

                                                function card_val_btn() {

                                                    Stripe.setPublishableKey("pk_test_FvgVInxV16FbpMbZ3VxtcJt1");

//var phone 	= $scope.pay_phone ;

                                                    var e = document.getElementById("exmonth");
                                                    var month = e.options[e.selectedIndex].value;
                                                    var f = document.getElementById("card_year");
                                                    var year = f.options[f.selectedIndex].value;
                                                    var cardno = $('#crno').val();
                                                    var cvv = $('#cvv').val();
                                                    var cname = $('#cname').val();
                                                    var errorPaymentInfo = '';

                                                    if (!Stripe.card.validateCardNumber(cardno)) {
                                                        errorPaymentInfo = "Invalid card number";
                                                        alert(errorPaymentInfo);
                                                    } else if (!Stripe.card.validateExpiry(month, year)) {
                                                        errorPaymentInfo = "Invalid expiry date";
                                                        alert(errorPaymentInfo);
                                                    } else if (!Stripe.card.validateCVC(cvv)) {
                                                        errorPaymentInfo = "Invalid CVV code";
                                                        alert(errorPaymentInfo);
                                                    } else {
                                                        Stripe.card.createToken({
                                                            number: cardno,
                                                            cvc: cvv,
                                                            exp_month: month,
                                                            exp_year: year
                                                        }, stripeResponseHandler);
                                                    }
                                                }



                                                var stripeResponseHandler = function (status, response) {
                                                    var cname = $('#cname').val();
                                                    var add1 = $('#add1').val();
                                                    var add2 = $('#add2').val();
                                                    var city = $('#city').val();
                                                    var zipcode = $('#zipcode').val();
                                                    var save_crd = $('#save_crd').val();
                                                    var radd1 = $('#radd1').val();
                                                    var radd2 = $('#radd2').val();
                                                    var rcity = $('#rcity').val();
                                                    var rzipcode = $('#rzipcode').val();
                                                    var rsave_crd = $('#rsave_crd').val();
                                                    var errorPaymentInfo = '';
                                                    console.log(response);

                                                    if (response.error) {
                                                        errorPaymentInf = response.error.message;
                                                        return false;
                                                    } else {
                                                        var token = response.id;
                                                        var last4 = response.card.last4;
                                                        var brand = response.card.brand;
                                                        var data = {'token': token, 'last4': last4, 'brand': brand};
                                                        var user_id = 1;
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo base_url() . "member_1/email_check_card_detail"; ?>",
                                                            //data: {'token':token,'last4':last4,'brand':brand,'user_id':user_id},
                                                            data: {'token': token, 'last4': last4, 'brand': brand, 'cname': cname, 'add1': add1, 'add2': add2, 'city': city, 'zipcode': zipcode, 'save_crd': save_crd, 'radd1': radd1, 'radd2': radd2, 'rcity': rcity, 'rzipcode': rzipcode, 'rsave_crd': rsave_crd},
                                                            success: function (data) {
                                                                alert(data);
                                                            }
                                                        });
                                                    }
                                                }





                                                function check_btn_add()
                                                {
                                                    var same_data = $('#same').is(':checked');
                                                    if (same_data == true)
                                                    {
                                                        var add1 = $('#add1').val();
                                                        var add2 = $('#add2').val();
                                                        var city = $('#city').val();
                                                        var zipcode = $('#zipcode').val();
                                                        var save_crd = $('#save_crd').val();
                                                        var e = document.getElementById("state");
                                                        var state = e.options[e.selectedIndex].value;

                                                        document.getElementById("radd1").value = add1;
                                                        document.getElementById("radd2").value = add2;
                                                        document.getElementById("rcity").value = city;
                                                        document.getElementById("rzipcode").value = zipcode;
                                                        document.getElementById("rsave_crd").value = save_crd;
                                                        document.getElementById("rstate").value = state;
                                                    }
                                                }






                                                function validate(evt) {

                                                    var theEvent = evt || window.event;
                                                    var key = theEvent.keyCode || theEvent.which;
                                                    key = String.fromCharCode(key);
                                                    var regex = /[0-9]|\./;
                                                    if (!regex.test(key)) {
                                                        theEvent.returnValue = false;
                                                        if (theEvent.preventDefault)
                                                            theEvent.preventDefault();
                                                    }
                                                }
    </script>




    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1482390655400902', //old local
                //appId: '677874605651237', //
                cookie: true, // enable cookies to allow the server to access the session
                status: true, // check login status
                xfbml: true, // parse XFBML
                oauth: true, //enable Oauth
                version: 'v2.3'
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        function FbLogin() {

            FB.login(function (response) {
                //console.log(response);
                if (response.authResponse) {
                    var access_token = response.authResponse.accessToken;
                    var userID = response.authResponse.userID;
                    FB.api('/me', {fields: 'id,name,email'}, function (response) {

                        var fullname = response.name;
                        var facebook_id = response.id;
                        var gender = response.gender;
                        var email_address = response.email;
                        var first_name = response.first_name;
                        var last_name = response.last_name;
                        if (facebook_id != '') {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url() . "member_1/fb_sign"; ?>",
                                data: {facebook_id: facebook_id, fullname: fullname, gender: gender, email_address: email_address, first_name: first_name, last_name: last_name},
                                success: function (data) {
<?php if (isset($_SESSION['purchase_plan'])) { ?>
                                        if (data.indexOf("Login") > -1) {

                                            $.ajax({
                                                url: "<?php echo base_url(); ?>member_1/fb_sign",
                                                type: "POST",
                                                data: {},
                                                success: function (result)
                                                {
                                                    if (result.indexOf("invalid") > -1) {
                                                        $("#username_login").css("border-color", "#FF0000");
                                                        $("#password_login").css("border-color", "#FF0000");
                                                        var _html = '<div class="alert alert-block green" style="color:red;font-size:14px;font-family:Lato, sans-serif;">Invalid user for this offer <br/></div>';
                                                        $('#alerts').html(_html);
                                                        $('#alerts').show();
                                                        $('#alerts').delay(5000).fadeOut();
                                                    }
                                                    else {
                                                        var _html = '<div class="alert alert-block green" style="color:#bda358;font-size:14px;font-family: Lato, sans-serif;">Redirecting to your account .. <br/></div>';
                                                        $('#alerts').html(_html);
                                                        $('#alerts').show();
                                                        //$('#login_form').modal('hide');
                                                        setTimeout(function () {
                                                            $('#login_form').modal('hide');
                                                        }, 900);
                                                        setTimeout('window.location.assign("<?php echo base_url(); ?>purchase-plan.html")', 490);
                                                        //$('#myModal1').modal('show');						
                                                    }
                                                }
                                            });
                                        }
                                        else if (data.indexOf("Registration") > -1) {
                                            $("#username_login").css("border-color", "#FF0000");
                                            $("#password_login").css("border-color", "#FF0000");
                                            var _html = '<div class="alert alert-block green" style="color:red;font-size:14px;font-family:Lato, sans-serif;">Invalid user for this offer <br/></div>';
                                            $('#alerts').html(_html);
                                            $('#alerts').show();
                                            $('#alerts').delay(5000).fadeOut();

                                        }<?php } else { ?>

                                        if (data.indexOf("Login") > -1) {

                                            window.location.assign("<?php echo base_url() ?>dashboard-myevolution.html");
                                        }
                                        else if (data.indexOf("Registration") > -1) {
                                            window.location.assign("<?php echo base_url(); ?>dashboard-myevolution.html");

                                        }
<?php } ?>

                                }
                            });
                        } else {
                            return false;
                        }


                    });
                    //alert(response);
                    //parent.location ='<?php //echo base_url();                                  ?>fbci/fblogin'; //redirect uri after closing the facebook popup
                }
            }, {scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook

        }




        function test() {
            alert("test");

            $('input[name="locationthemes"]:checked').each(function () {
                console.log(this.value);
                alert(this.value);
            });
        }
    </script>


</body>

</html>