<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">




  <!-- Responsive Metatag -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Page Description and Author -->
  <meta name="description" content="Margo - Responsive HTML5 Template">
  <meta name="author" content="iThemesLab">


  
  
  
  
  <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="<?php echo base_url(); ?>asset/css/dateselector.css" rel="stylesheet" type="text/css">
  
  
  
  
  <!-- Margo JS  -->
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.migrate.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/modernizrr.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.fitvids.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/nivo-lightbox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.isotope.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.appear.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/count-to.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.textillate.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.lettering.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.easypiechart.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.nicescroll.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.parallax.js"></script>
  <script src="<?php echo base_url(); ?>https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.slicknav.js"></script>
  
  
  
  <script>
  $(document).ready(function() {
	
	$(document).on('change','.form-control',function(){
		
		
		var month=$(".month").val();
		var year=$(".year").val();
		var day=$(".day").val();
		var year1=$(".year1").val();
		var month1=$(".month1").val();
		var day1=$(".date1234").val();
		
		
     
	       if($(".month").val()!='' && $(".year").val()!='' &&  $(".day").val()!='' && $(".year1").val()!='' && $(".month1").val()!='' && $(".date1234").val()!=''){
		 
		 
		 
		  $.ajax({
						  type : "POST",
						  url  : "<?php echo base_url();?>payment_report/ajaxblock",
						  data : {month:month, year:year,day:day,month1:month1,year1:year1,day1:day1}, 
						  cache : false,
						  success : function(response) {
							  
							 
							 
							 $("#pay_tab").empty(); 
							  
							   $("#pay_tab").html(response);
							  
							  
							  }
					   });  
		 
		 
		 
			}
		
    });
	});
  </script>
  

  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" media="screen">
  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/colors/red.css" title="red" media="screen" />
  <!-- Margo JS  -->



  <!--[if IE 8]><script src="<?php echo base_url(); ?>https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="<?php echo base_url(); ?>https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->


<body>

  <!-- Container -->
  <div id="container">

    <!-- Start Header -->
    <div class="hidden-header"></div>
        <!-- Start Header ( Logo & Naviagtion ) -->
    
      <!-- End Header ( Logo & Naviagtion ) -->

   
   <!-- End Header -->

   <!-- Start Content -->
    <div id="content">
    <div class="container">
  <div class="ss">
    <div class="col-xs-12"> 
    <h2 class="pageheader_report">Orders Payout Report</h2>
    <div class="page_sub_header">
    <div class="col-md-6">
    CURRENT WEEK REPORT
    </div>
    <div class="col-md-6 text-right">
    Next Payment Due On: <?php echo  date("m/d/Y", strtotime($order_report['due_on']));   ?>
    </div>
    <div class="clearfix"></div>
    </div>
    
    <div class="report_table">
    
    <div class="col-md-6 pad0">
    <div class="col-xs-8">Orders for the Week</div>
    <div class="col-xs-4"><?php echo  $order_report['count_order']; ?> </div>
    <div class="clearfix"></div>
    </div>
   
    <div class="col-md-6 pad0">
    <div class="col-xs-8 text-right">Total Order Value</div>
    <div class="col-xs-4"><?php echo '$'.number_format((float)$order_report['total_amount'], 2, '.', '') ; ?></div>
    <div class="clearfix"></div>
    </div>
    
    
    
    <div class="col-md-12 pad0">
    <div class="col-xs-10 text-right">Processing Fee</div>
    <div class="col-xs-2 text-center"><?php echo  '$'. number_format((float)$order_report['processing_fee'], 2, '.', '') ; ?></div>
    
    </div>
     <div class="col-md-12 pad0">
    <div class="col-xs-10 text-right">Total Due</div>
    <div class="col-xs-2 text-center"><?php echo  '$'. number_format((float)$order_report['total_amount_proc'], 2, '.', '') ; ?></div>
    
    </div>
   
    <div class="clearfix"></div>
   </div>
      <div class="page_sub_header">
     <div class="col-md-12">
    PAST WEEK REPORT
    </div>
     <div class="clearfix"></div>
    </div>
     
    <div id="report_tbl">
    <div class="action_bar">
    <div class="col-sm-6">
    <div class="col-sm-2 pad0" style="line-height:36px;">START</div>
    <div class="col-sm-8 pad0">
 <div>
<!--<input class="dateselector-bs"  type="text"v>-->
<input type="text" id="date" data-format="DD-MM-YYYY" data-template="MMM YYYY D" name="date" value="28-02-2016" class="form-control">
</div>
    
    
    </div>
    </div>
    <div class="col-sm-6">
    <div class="col-sm-2 pad0" style="line-height:36px;">END</div>
    <div class="col-sm-8 pad0">
    
   <div id="date12">
<!--<input class="dateselector-bs"  type="text"v>-->
<input type="text" id="date1" data-format="DD-MM-YYYY" data-template="MMM YYYY D" name="date1" value="28-02-2016" class="form-control1">
</div>
    
    
    
    </div>
    </div>
    <div class="clearfix"></div>
    </div>
    
      <table class="col-md-12 table-bordered  table-condensed cf">
        <thead>
        <tr>
        <td class="numeric header_tblrt">Payment Date</td>
        <td class="numeric header_tblrt">Orders for the Week</td>
        <td class="numeric header_tblrt">Total Order Value</td>
        <td class="numeric header_tblrt">Processing Fee</td>
         <td class="numeric header_tblrt">Status</td>
        <td class="numeric header_tblrt">Total</td>
        </tr>
        </thead>
        <tbody id="pay_tab">
        <tr>
        <td class="callabel" data-title="Montht"></td>
        <td data-title="# New Subscribers of the Month" class="numeric"></td>
        <td data-title="# of Existing Customers" class="numeric"></td>
      
        <td data-title="TOTAL" class="numeric"></td>
        <td data-title="AMOUNT PAID" class="numeric"></td>
        </tr>
         
        </tbody>
      
   </table>
     </div>
    
         
      <!--end of .table-responsive-->
    </div>
</div>
  </div>
</div>

    </div>
    <!-- End content -->


 <br>
<br>
<br>
<br>




  </div>
  <!-- End Container -->

  <!-- Go To Top Link -->
<a href="<?php echo base_url(); ?>#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>


<style>
.combodate .form-control { display:inline; float:left; margin-right:8px;}

</style>

<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset/js/combodate.js"></script>

<script>






$("#date1").change(function(){
	
	
	
	$('div#date12').find('select.year').addClass('year1');
	$('div#date12').find('select.month').addClass('month1');
	$('div#date12').find('select.day').addClass('date1234');
	$('div#date12').find('select.year1').removeClass('year');
	$('div#date12').find('select.month1').removeClass('month');
	$('div#date12').find('select.date1234').removeClass('day');
	

   
   
   
   
   
   if($(".year1").val()==new Date().getFullYear() ){
		if(new Date().getMonth() ==0){
		$(".month1 option[value='1']").attr("disabled", "disabled");
		$(".month1 option[value='2']").attr("disabled", "disabled");
		$(".month1 option[value='3']").attr("disabled", "disabled");
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==1){
		
		$(".month1 option[value='2']").attr("disabled", "disabled");
		$(".month1 option[value='3']").attr("disabled", "disabled");
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==2){
		
		
		$(".month1 option[value='3']").attr("disabled", "disabled");
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==3){
		
		
		$(".month1 option[value='4']").attr("disabled", "disabled");
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==4){
		
		
		
		$(".month1 option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==5){
		
		
		
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==6){
		
		
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==7){
		
	
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==8){
		
	
	
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==9){
		
	
	

		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==10){
		$("option[value='11']").attr("disabled", "disabled");
	}

	}
	else
	{
	  
		
         $('.month1 option').removeAttr('disabled');
		
	}
   
   
   
   
   
   
   
   
   
   
   
   
   
    y = $(".year1").val(), m = $(".month1").val();
			


var firstDay = new Date(y, m, 1);
var lastDay = new Date(y, m + 1, 0);
		
		 var endDate = new Date(firstDay);
       var date_modi_first_date = endDate.getDate();
	    var date_modi_first_day = endDate.getDay();
		
		//last date
		var endDate1= new Date(lastDay);
		 var date_modi_las_date = endDate1.getDate();
	    var date_modi_last_day = endDate1.getDay();
		
	
			
			if(date_modi_first_day==0){
				var dat=(date_modi_las_date-1)/7;
			
			}
			else if(date_modi_first_day==1){
				var dat=(date_modi_las_date)/7;
				
			}
			else if(date_modi_first_day==2){
				var dat=(date_modi_las_date-6)/7;
				
			}
			
			else if(date_modi_first_day==3){
				var dat=(date_modi_las_date-5)/7;
				
			}
			
			else if(date_modi_first_day==4){
				var dat=(date_modi_las_date-4)/7;
				
			}
			else if(date_modi_first_day==5){
				var dat=(date_modi_las_date-3)/7;
				
			}
			
			else if(date_modi_first_day==6){
				var dat=(date_modi_las_date-2)/7;
				
			}
			
			 
			
			if(dat>3 && dat<=4){
				
				$(".date1234 option[value='5']").remove();
				}
			else
			{
				if($(".date1234 option[value='5']").length == 0){
	           $(".date1234").append('<option value="5">5th week</option>');
				}
			  
			}
		
});

	


$("#date").change(function(){
	
	
	
	if($(".year").val()==new Date().getFullYear() ){
		if(new Date().getMonth() ==0){
		$(".month option[value='1']").attr("disabled", "disabled");
		$(".month option[value='2']").attr("disabled", "disabled");
		$(".month option[value='3']").attr("disabled", "disabled");
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==1){
		
		$(".month option[value='2']").attr("disabled", "disabled");
		$(".month option[value='3']").attr("disabled", "disabled");
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==2){
		
		
		$(".month option[value='3']").attr("disabled", "disabled");
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==3){
		
		
		$(".month option[value='4']").attr("disabled", "disabled");
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==4){
		
		
		
		$(".month option[value='5']").attr("disabled", "disabled");
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==5){
		
		
		
		$("option[value='6']").attr("disabled", "disabled");
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==6){
		
		
		$("option[value='7']").attr("disabled", "disabled");
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==7){
		
	
		$("option[value='8']").attr("disabled", "disabled");
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
	else if(new Date().getMonth() ==8){
		
	
	
		$("option[value='9']").attr("disabled", "disabled");
		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==9){
		
	
	

		$("option[value='10']").attr("disabled", "disabled");
		$("option[value='11']").attr("disabled", "disabled");
	}
		else if(new Date().getMonth() ==10){
		$("option[value='11']").attr("disabled", "disabled");
	}

	}
	else
	{
	  
		
         $('.month option').removeAttr('disabled');
		
	}
	
	

	
	
	 y = $(".year").val(), m = $(".month").val();
			


var firstDay = new Date(y, m, 1);
var lastDay = new Date(y, m + 1, 0);
		
		 var endDate = new Date(firstDay);
       var date_modi_first_date = endDate.getDate();
	    var date_modi_first_day = endDate.getDay();
		
		//last date
		var endDate1= new Date(lastDay);
		 var date_modi_las_date = endDate1.getDate();
	    var date_modi_last_day = endDate1.getDay();
		
	
			
			if(date_modi_first_day==0){
				var dat=(date_modi_las_date-1)/7;
			
			}
			else if(date_modi_first_day==1){
				var dat=(date_modi_las_date)/7;
				
			}
			else if(date_modi_first_day==2){
				var dat=(date_modi_las_date-6)/7;
				
			}
			
			else if(date_modi_first_day==3){
				var dat=(date_modi_las_date-5)/7;
				
			}
			
			else if(date_modi_first_day==4){
				var dat=(date_modi_las_date-4)/7;
				
			}
			else if(date_modi_first_day==5){
				var dat=(date_modi_las_date-3)/7;
				
			}
			
			else if(date_modi_first_day==6){
				var dat=(date_modi_las_date-2)/7;
				
			}
			
		
			if(dat>3 && dat<=4){
				$(".day option[value='5']").remove();
				}
			else
			{
				if($(".day option[value='5']").length == 0){
	           $(".day").append('<option value="5">5th week</option>');
				}
			  
				 
			}

	
});


$(function(){
	 
    $('#date').combodate({customClass: 'form-control'}); 
	 $('#date1').combodate({customClass: 'form-control'});  
	$('#datetime').combodate(); 
	$('#time').combodate({
        firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
        minuteStep: 1,
		customClass: 'form-control'
		
    });   
});
</script>






</body>

</html>