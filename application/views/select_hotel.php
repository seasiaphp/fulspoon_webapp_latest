<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
    <?php
    ?>
    <head>

        <!-- Basic -->
        <title>Fulspoon |Select Restaurant</title>

        <!-- Define Charset -->
        <meta charset="utf-8">
        <style>
            .placeholder::-webkit-input-placeholder { color: red }
            .placeholder:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                color:    red;
                opacity:  1;
            }
            .placeholder::-moz-placeholder { /* Mozilla Firefox 19+ */
                color:    red;
                opacity:  1;
            }
            .placeholder:-ms-input-placeholder { /* Internet Explorer 10+ */
                color:   red;
            }
        </style>
        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Fulspoon">
        <meta name="author" content="Newagesmb">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url(); ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/colors/green.css" title="green" media="screen" />


        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <script>
        $('#hotel_reg').on('submit', function (e) {
            alert("mdkn");
        });

    </script>
    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <header class="clearfix">
                <!-- Start Header ( Logo & Naviagtion ) -->
                <div class="navbar navbar-default navbar-top green-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="owners_index.html"><img src="<?= base_url(); ?>images/replimatic_logo_white.png" alt=""/></a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Stat Search -->
                            <!-- Stat Search -->
                            <div class="buttons-side">
                                <a class="btn btn-default btnline" href="#" role="button" data-toggle="modal" data-target="#myModal">LOGIN</a>
                                <a class="btn btn-default btnlogin" href="#" role="button">SIGN UP</a>
                            </div>
                            <!-- End Search -->
                            <!-- End Search -->
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li> <a style="color:#fff;">Call Us: 877-638-6522</a></li>             
                            </ul>
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li><a>Call Us: 877-638-6522</a></li>
                        <li><a href="index-01.html">LOGIN</a></li>
                        <li><a href="#">SIGN UP</a></li></ul>
                    <!-- Mobile Menu End -->

                </div>
                <!-- End Header ( Logo & Naviagtion ) -->

            </header>
            <!-- End Header -->

            <!-- Start Content -->
            <div id="content">
                <div class="container">

                    <!--begin tabs going in wide content -->


                    <?php
                    ?>

                    <div id="home">       
                        <!--<h3 class="head30 text-center col-sm-12">Google API Data And Locu API Data</h3>-->
                        <!-- SIGN UP ---->
                        <div class="container">


                            <form action="<?= base_url() ?>owners/save_hotel" method="post">

                                <div  class="col-lg-6" id="cuisine_list">
                                    <div class="col-lg-6 labelsignup"><b>Select Cuisine </b> </div>
                                    <?php
                                    foreach ($cuisines as $cus) {
                                        ?>
                                        <div class="col-lg-12" style="margin-top: .71%" >
                                            <input type="checkbox" name="cus[]" value="<?= $cus['cusine_id'] ?>" /><?= $cus['cusine_name'] ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div  class="col-lg-6">
                                    <div class="col-lg-12 labelsignup"><b>Add New Cuisine </b> </div>
                                    <input type="text" id="newCuisine" name="newCuisine" placeholder="Cisine Name" >
                                    <button class="btn btn-primary" type="button" onclick="submitCuisine()">Add new cuisine</button>
                                    <div class="col-lg-12 labelsignup"><b>Please Give a description about your Restaurant </b> </div>
                                    <textarea name="desc" placeholder="Restaurant Description"></textarea>
                                    <input type="hidden" name="google_id" value="<?= $google_place_id; ?>">
                                    <input type="hidden" name="locu_id" value="<?= $locu_id; ?>">

                                </div>
                                <div class="col-lg-12">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">  <button class="btn btn-primary" type="submit" name="sub" />Save</div>
                                </div>
                            </form>

                            <!--<div class="clearfix"></div>-->
                            <!--/.tab-pane -->


                            <div class="tab-pane active fade in" id="home">       
                                <!--<h3 class="head30 text-center col-lg-12">The Locu data as Follows</h3>-->
                                <!-- SIGN UP ---->
                                <div class="container">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End content -->


                <!-- Start Footer -->
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 padb20"><img src="<?= base_url() ?>images/footer-replimatic.png" alt=""/></div>
                            <div class="col-md-offset-3 col-md-5"> 
                                <ul class="footer-nav">                                               
                                    <li><a href="<?= base_url() ?>owners">Home</a> </li>    
                                    <li><a href="<?= base_url() ?>owners/terms_condition">Terms & Conditions</a> </li>
                                    <li><a href="<?= base_url() ?>owners/faq">FAQ</a> </li>
                                    <li><a href="<?= base_url() ?>owners/contact_us">Contact</a> </li>
                                </ul>
                            </div>
                            <div class="col-md-12"><p class="topline">&copy; <?= date("Y") ?> Fulspoon. All rights reserved. </p></div>

                        </div>
                        <!-- .row -->
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- End Container -->

            <!-- Go To Top Link -->
            <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
            <script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>

    </body>

</html>
<script>

                                        function submitCuisine() {
                                            var newCuisine = $("#newCuisine").val();
                                            if (newCuisine != '') {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>owners/checkNewCuisine",
                                                    data: {newCuisine: newCuisine},
                                                    success: function (data) {
                                                        if (data.indexOf("success") > -1) {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo base_url(); ?>owners/newCuisine",
                                                                data: {newCuisine: newCuisine},
                                                                success: function (response) {
                                                                    $("#cuisine_list").append(response);
                                                                }
                                                            });
                                                        } else {
                                                            $("#newCuisine").addClass('placeholder');
                                                            $("#newCuisine").val('');
                                                            $('#newCuisine').attr('placeholder', 'Cuisine is already exisit!!');
                                                        }
                                                    }});
                                            } else {
                                                $("#newCuisine").addClass('placeholder');
                                                $("#newCuisine").val('');
                                                $('#newCuisine').attr('placeholder', 'Enter a valid cuisine');

                                            }

                                        }
                                        function hideFullJson_locu(json) {
                                            $("#tabl_locu_" + json).hide();
                                            $("#but_locu_" + json).hide();
                                        }
                                        function hideFullJson(json) {
                                            $("#tabl_" + json).hide();
                                            $("#but_" + json).hide();
                                        }

                                        function showFullJson_locu(id) {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>owners/test_api",
                                                data: {id: id},
                                                success: function (data) {

                                                    $("#json_locu_" + id).empty();
                                                    $("#json_locu_" + id).html(data);
                                                    $("#tabl_locu_" + id).show();
                                                    $("#json_locu_" + id).show();
                                                    $("#but_locu_" + id).show();

                                                }
                                            });
                                        }

</script>


<?php ########################################################  ?>
   
