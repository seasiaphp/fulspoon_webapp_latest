<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">



    <!-- Basic -->


    <!-- Define Charset -->

    <!-- Responsive Metatag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Page Description and Author -->
    <meta name="description" content="Margo - Responsive HTML5 Template">
    <meta name="author" content="iThemesLab">


    <!-- Bootstrap CSS  -->



    <!-- Margo JS  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.migrate.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/modernizrr.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/nivo-lightbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.appear.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/count-to.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.textillate.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.lettering.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.parallax.js"></script>
    <script src="<?php echo base_url(); ?>https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.slicknav.js"></script>
    <script>




        $(document).ready(function () {

            $(document).on('click', '.ui-datepicker-close', function () {

                if ($("#start_date").val() != '' && $("#end_date").val() != '') {
                    var start_date = $("#start_date").val();
                    var end_date = $("#end_date").val()


                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>payment_report/ajaxsub",
                        data: {start_date: start_date, end_date: end_date},
                        cache: false,
                        success: function (response) {



                            $("#pay_tab").empty();

                            $("#pay_tab").html(response);


                        }
                    });




                }

            });
        });

    </script>

    <!-- Css3 Transitions Styles  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" media="screen">
    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/colors/red.css" title="red" media="screen" />
    <!-- Margo JS  -->



  <!--[if IE 8]><script src="<?php echo base_url(); ?>https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="<?php echo base_url(); ?>https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->



    <body>
        <div class="clearfix"></div>
        <!-- Container -->
        <div id="container">

            <!-- Start Header -->

            <!-- Start Header ( Logo & Naviagtion ) -->

            <!-- End Header ( Logo & Naviagtion ) -->


            <!-- End Header -->

            <!-- Start Content -->
            <div id="content">
                <div class="container" style="padding:0 15px;">
                    <div class="">
                        <div class="col-xs-12" style="padding:0 15px;"> 
                            <h2 class="pageheader_report" style="margin-top:20px; line-height: 1.1;">Subscription Payment Report</h2>
                            <div class="page_sub_header">
                                <div class="col-md-6">
                                    CURRENT MONTH REPORT
                                </div>
                                <div class="col-md-6 text-right">
                                    Next Payment Due On: 1st of <?php echo date('M Y', strtotime('+1 month')); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="report_table">

                                <div class="col-md-6 pad0">
                                    <div class="col-xs-8">New Subscribers this Month</div>
                                    <div class="col-xs-4"><?php echo ($new['new_sub'] - $existing['existing_sub']); ?> </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-6 pad0">
                                    <div class="col-xs-8">Number of Renewing Subscribers</div>
                                    <div class="col-xs-4"><?php echo $existing['existing_sub']; ?></div>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="col-md-6 pad0">
                                    <div class="col-xs-8">Total Number of Subscribers</div>
                                    <div class="col-xs-4"><?php echo $new['new_sub']; ?></div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-6 pad0">
                                    <div class="col-xs-8">Amount Due</div>
                                    <div class="col-xs-4"><?php $con = ($new['new_sub'] - $existing['existing_sub']) + $existing['existing_sub'];
echo '$' . number_format((float) $con * $admin_val['contract_price'], 2, '.', ''); ?></div>
                                    <div class="clearfix"></div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="page_sub_header">
                                <div class="col-md-12">
                                    PAST MONTH REPORT
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div id="report_tbl">
                                <div class="action_bar">
                                    <div class="col-sm-6">
                                        <div class="col-sm-2 pad0">START</div>
                                        <div class="col-sm-5 pad0">
                                            <div class="dropdown">
                                                <input type="text" id="start_date"  placeholder="SELECT DATE" class="date-picker">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-2 pad0">END</div>
                                        <div class="col-sm-5 pad0">
                                            <div class="dropdown">
                                                <input type="text" id="end_date"  placeholder="SELECT DATE" class="date-picker">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>


                                <table class="col-md-12 table-bordered  table-condensed cf">
                                    <thead>
                                        <tr>
                                            <td class="numeric header_tblrt">Payment Date</td>
                                            <td class="numeric header_tblrt">New Subscribers this Month</td>
                                            <td class="numeric header_tblrt">Number of Renewing Subscribers</td>
                                            <td class="numeric header_tblrt">Number of Non-renewing Subscribers</td>
                                            <td class="numeric header_tblrt">Total Subscribers</td>
                                            <td class="numeric header_tblrt">Amount Paid</td>
                                        </tr>
                                    </thead>
                                    <tbody id="pay_tab">
                                        <tr>
                                            <td class="callabel" data-title="Payment Date"></td>
                                            <td data-title="New Subscribers of the Month" class="numeric"></td>
                                            <td data-title="Number of Renewing Subscribers" class="numeric"></td>
                                            <td data-title="Number of Non-renewing Subscribers" class="numeric"></td>
                                            <td data-title="Total Subscribers" class="numeric"></td>
                                            <td data-title="Amount Paid" class="numeric"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!--end of .table-responsive-->
                        </div>
                    </div>
                </div>
            </div>


            <!-- End content -->


            <!-- Start Footer Section -->

            <!-- End Footer Section -->



        </div>
        <!-- End Container -->

        <!-- Go To Top Link -->
        <a href="<?php echo base_url(); ?>#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>






        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>

        $(function () {

            $(function () {

                $('.date-picker').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'MM yy',
                    onClose: function (dateText, inst) {
                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        $(this).datepicker('setDate', new Date(year, month, 1));
                    },
                    beforeShow: function (input, inst) {
                        var datestr;
                        if ((datestr = $(this).val()).length > 0) {
                            year = datestr.substring(datestr.length - 4, datestr.length);
                            month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNamesShort'));
                            $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                            $(this).datepicker('setDate', new Date(year, month, 1));
                        }




                    }
                });
            });
        });



        </script>
        <style>
            .ui-datepicker-calendar {
                display: none;
            }
            .ui-widget-header{ background-color:transparent !important; background:none; border:none; font-size:16px !important;}
            .ui-datepicker-month{ border:none; font-size:16px !important;}
            .ui-datepicker .ui-datepicker-title select{ font-size:13px !important; height:30px !important; color:#4F4F4F !important;}
            .ui-datepicker .ui-datepicker-prev{ display:none !important;}
            .ui-datepicker .ui-datepicker-next{ display:none !important;}
            .ui-datepicker-title{ margin:0 !important;}
            .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year{ width:48%;}
            .ui-widget-content{ background-color:#6fbe44 !important; background:#6fbe44 !important; background-image:none !important;}
            .ui-datepicker-close{ background-color:#424242 !important; background:#424242 !important; background-image::none !important; 
                                  color:#fff !important; font-size:13px !important; border:none !important;}
            .ui-datepicker-buttonpane{ border:none !important;}
            .ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current{ background-color:#fff !important; background:#fff !important; background-image:none !important; border:none !important; color:#6fbe44 !important; opacity:1; font-size:13px !important;}
            #ui-datepicker-div{ padding:10px !important;}
            .dropdown .date-picker{ padding-left:10px !important;}
        </style>
        <script>




        </script>






    </body>

</html>