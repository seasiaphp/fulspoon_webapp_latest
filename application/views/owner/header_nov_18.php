<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="" />
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <title>Fulspoon | Owner's Dashboard</title>

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet">
        <!--<link rel="stylesheet" href="css/meanmenu.css" media="all" />-->

        <!-- Awesome Fonts -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/css/font-awesome.min.css" rel="stylesheet">
        <!--<script src="https://gist.github.com/stefanjudis/3ed41763fdffeb6c4b9a51e9fc9b437f.js"></script>-->
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/zebra_dialog.css" type="text/css">	

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url(); ?>assets/js/chain/sw.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/zebra_dialog.js"></script>
        <script src="<?= base_url() ?>assets/js/chain/push.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-73692716-1', 'auto');
            ga('send', 'pageview');


        </script>

    </head>

    <body>
        <div id="preloader" class="loader_home"></div>
        <?php //echo base_url().$route.'/orders'; ?>
        <!-- ===== Header ===== -->
        <!-- Responsive Menus -->
        <section class="responsive-menus">
            <div class="menu-section">
                <!-- Toggle Buttons -->
                <div class="menu-toggle">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <!-- End Toggle Buttons -->

                <!-- Toggle Menus -->
                <nav>
                    <ul role="navigation" class="hidden">
                        <a href="<?php echo base_url() . 'owner/home' ?>">
                            <img alt="" src="<?= base_url() ?>images/replimatic_logo_white.png">
                        </a>
                        <li <?php echo ($active == 'orders') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/orders'; ?>"><i class="demo-icon icon-ico-orders">&#xe801;</i> Orders</a></li>
                        <li <?php echo ($active == 'menu') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>"><i class="demo-icon icon-ico-menu">&#xe805;</i> Menu</a></li>
                        <li <?php echo ($active == 'preference') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/home/preference'; ?>"><i class="demo-icon icon-ico-preferences">&#xe802;</i> Preferences</a></li>
                        <li <?php echo ($active == 'profile') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/profile'; ?>"><i class="demo-icon icon-ico-profile">&#xe804;</i> Profile</a></li>
                        <li <?php echo ($active == 'profile') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>"><i class="demo-icon icon-ico-profile">&#xe804;</i> Subscription Payment</a></li>
                        <li <?php echo ($active == 'profile') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'payment_report/order_report/'; ?>"><i class="demo-icon icon-ico-profile">&#xe804;</i>Order Payout </a></li>
                        <p>&copy; Fulspoon All Rights Reserved</p>
                    </ul>
                </nav>
                <!-- End Toggle Menus -->
            </div>


            <!-- Small DropDown -->
            <div class="res-small-dropDown">
                <a href="<?php echo site_url('owner/home/logout'); ?>"><i class="demo-icon icon-off">&#xe808;</i></a>
            </div>
            <!-- End Small DropDown -->
        </section>
        <!-- End Responsive Menus -->

        <header>
            <div class="container-fluid">
                <!-- Logo Menu -->
                <div class="logo-menu">
                    <div class="row">
                        <!-- Logo -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 logo-full">
                            <div class="logo">
                                <a class="navbar-brand" href="<?= base_url() ?>owner/home">
                                    <?php echo '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="170px" height="46px" viewBox="0 0 792 214" style="enable-background:new 0 0 792 214;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{opacity:0.52;}
	.st3{clip-path:url(#SVGID_6_);fill:#6EBD44;}
	.st4{fill:url(#SVGID_7_);}
	.st5{fill:#EFEFEF;}
</style>
<g>
	<g>
		<path class="st0" d="M253.3,163.1v-61.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
			c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H253.3z"/>
		<path class="st0" d="M262.9,166.1h-12.6v-61.5h-13.9V92.6h13.9v-3.1c0-16.9,8.3-26.3,23.4-26.3c5.5,0,10.6,1.8,15.3,5.3l2.3,1.7
			l-6.5,9.7l-2.4-1.5c-3.6-2.1-5.6-3-8.9-3c-4.9,0-10.5,1.6-10.5,14.1v3.1h22.4v11.9h-22.4V166.1z M256.3,160.1h0.6V89.6
			c0-17.5,10.3-20.1,16.5-20.1c3.9,0,6.7,1,9.5,2.5l0.1-0.1c-2.9-1.7-6.1-2.5-9.3-2.5c-11.7,0-17.4,6.6-17.4,20.3V160.1z"/>
	</g>
	<g>
		<path class="st0" d="M353.1,163.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V94.9h6.7V134
			c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V94.9h6.7v68.2H353.1z"/>
		<path class="st0" d="M328.9,167.2L328.9,167.2c-20.5,0-33.7-13-33.7-33V91.9h12.7V134c0,13.3,7.4,20.5,20.9,20.5l0.3,0
			c12.3-0.1,20.8-9.1,20.8-21.8V91.9h12.7v74.2h-12.6v-6.7C344.7,164.3,337.3,167.1,328.9,167.2L328.9,167.2z M301.2,97.9v36.2
			c0,16.7,10.6,27,27.7,27c4.9-0.1,13.2-1.3,19-8c-4.7,4.6-11.2,7.3-18.6,7.4l-0.4,0c-16.6,0-26.9-10.2-26.9-26.5V97.9H301.2z
			 M356.1,160.1h0.6V97.9H356v34.9c0,3.3-0.5,6.4-1.4,9.2l1.5-2.7V160.1z"/>
	</g>
	<g>
		<rect x="377.5" y="66.7" class="st0" width="6.7" height="96.4"/>
		<path class="st0" d="M387.2,166.1h-12.7V63.7h12.7V166.1z M380.5,160.1h0.7V69.7h-0.7V160.1z"/>
	</g>
	<g>
		<path class="st0" d="M450.8,107.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
			c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
			c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
			c9.9,0,18.6,2.8,25.3,9.1L450.8,107.5z"/>
		<path class="st0" d="M429.8,167.2c-14.1,0-26.1-5.6-32.3-15l-1.4-2.2l9.4-8.4l1.9,2.8c4.5,6.4,12.9,10.3,22.5,10.3
			c5.4,0,17.9-1,18.2-10.1c0.2-6.7-7.7-8.5-19.6-10.4c-12.8-2.1-28.8-4.8-29-21.2c-0.1-5.4,1.8-10.1,5.5-13.8
			c5.2-5.2,13.9-8.2,24-8.2l0.5,0c11.1,0,20.4,3.3,27.4,9.9l2.2,2.1l-8.2,8.7l-2.2-1.9c-6.1-5.3-11.9-6.4-19.2-6.4l-0.5,0
			c-6.7,0-12.3,1.7-15.1,4.6c-1.3,1.4-2,3-1.9,4.9c0.2,5.9,8,7.7,17.3,9.4l2.4,0.4c11.9,2,29.8,5,29,22.8
			C460.3,161.4,444.5,167.2,429.8,167.2z M404.1,151c5.4,6.4,14.9,10.2,25.7,10.2c5.8,0,24.5-1.2,25-16.1
			c0.5-11.6-10.1-14.3-24-16.7l-2.4-0.4c-8.7-1.5-22-3.9-22.3-15.1c-0.1-3.5,1.1-6.7,3.6-9.2c4-4.1,11-6.4,19.4-6.4l0.5,0
			c6.6,0,13.7,0.8,20.9,6.1l0.1-0.1c-5.5-4.2-12.6-6.3-21-6.3l-0.5,0c-8.5,0-15.7,2.3-19.8,6.4c-2.5,2.5-3.8,5.8-3.7,9.5
			c0.1,10.6,9.5,13,23.9,15.4c11.1,1.8,25,4,24.7,16.5c-0.4,9.9-9.4,15.9-24.2,15.9c-10.2,0-19.4-3.8-25.3-10.2L404.1,151z"/>
	</g>
	<g>
		<path class="st0" d="M472.8,191.4V95.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35c0,22.9-15.4,35.3-34.6,35.3
			c-11.6,0-22.3-5.6-28.1-16.9v44.1H472.8z M535.2,129.1c0-19.1-12.4-28.5-27.8-28.5c-15.8,0-27.5,12-27.5,28.6
			c0,16.7,12,28.6,27.5,28.6C522.8,157.8,535.2,148.2,535.2,129.1"/>
		<path class="st0" d="M482.3,194.4h-12.5V92.1h12.6v9.8c6.6-6.9,16-10.8,26.5-10.8l0.1,0c21.5,0.8,35.9,16.1,35.9,38
			c0,22.9-15.1,38.3-37.6,38.3c-9.9,0-18.7-3.8-25.1-10.5V194.4z M475.8,188.4h0.5v-53.5l1.7,3.4c-0.8-2.9-1.2-5.9-1.2-9.1
			c0-2.9,0.3-5.8,1-8.4l-1.4,2.6V98.1h-0.6V188.4z M487.8,153.9c5.1,4.8,11.9,7.4,19.6,7.4c19.2,0,31.6-12.7,31.6-32.3
			c0-18.7-11.8-31.3-30.1-32c-4,0-7.8,0.7-11.2,1.9c3-1,6.3-1.5,9.8-1.5c18.7,0,30.8,12.4,30.8,31.5c0,19.3-12.1,31.8-30.8,31.8
			C499.8,160.8,493,158.3,487.8,153.9z M507.3,103.6c-14.2,0-24.5,10.8-24.5,25.6c0,14.9,10.3,25.6,24.5,25.6
			c12,0,24.8-6.8,24.8-25.8C532.2,113.1,522.9,103.6,507.3,103.6z"/>
	</g>
	<g>
		<path class="st0" d="M555.3,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
			C570.7,164,555.3,150.8,555.3,129.2 M617.8,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
			c0,17.6,12.5,28.2,27.9,28.2C605.2,157.4,617.8,146.8,617.8,129.2"/>
		<path class="st0" d="M589.8,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
			C627.4,151.5,611.9,167,589.8,167z M589.8,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8c18.6,0,31.6-13.1,31.6-31.8
			C621.4,110.1,608.4,96.8,589.8,96.8z M589.8,160.4c-18.2,0-30.9-12.8-30.9-31.2c0-18.6,13-32.1,30.9-32.1c17.9,0,31,13.5,31,32.1
			C620.8,147.6,608,160.4,589.8,160.4z M589.8,103.2c-14.5,0-24.9,11-24.9,26.1c0,14.9,10.3,25.2,24.9,25.2c14.7,0,25-10.4,25-25.2
			C614.8,114.1,604.3,103.2,589.8,103.2z"/>
	</g>
	<g>
		<path class="st0" d="M636.8,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
			C652.2,164,636.8,150.8,636.8,129.2 M699.3,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
			C686.7,157.4,699.3,146.8,699.3,129.2"/>
		<path class="st0" d="M671.3,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
			C708.9,151.5,693.4,167,671.3,167z M671.3,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8c18.6,0,31.6-13.1,31.6-31.8
			C702.9,110.1,689.9,96.8,671.3,96.8z M671.3,160.4c-18.2,0-31-12.8-31-31.2c0-18.6,13-32.1,31-32.1c17.9,0,31,13.5,31,32.1
			C702.3,147.6,689.5,160.4,671.3,160.4z M671.3,103.2c-14.5,0-25,11-25,26.1c0,14.9,10.3,25.2,25,25.2c14.7,0,25-10.4,25-25.2
			C696.3,114.1,685.8,103.2,671.3,103.2z"/>
	</g>
	<g>
		<path class="st0" d="M728,95.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V124
			c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H728z"/>
		<path class="st0" d="M786,166.1h-12.7V124c0-13.1-7.6-20.5-20.8-20.5l-0.4,0c-12.3,0.1-20.9,9.1-20.9,21.8v40.9h-12.7v-74H731v6.6
			c5.4-4.9,12.8-7.7,21.2-7.8c20.5,0,33.8,13,33.8,33V166.1z M779.2,160.1h0.7v-36.2c0-16.7-10.6-27-27.7-27
			c-4.8,0.1-13.1,1.3-18.9,8c4.7-4.6,11.2-7.3,18.6-7.3l0.4,0c16.6,0,26.8,10.2,26.8,26.5V160.1z M724.4,160.1h0.7v-34.9
			c0-3.2,0.5-6.3,1.3-9.1l-1.5,2.6V98.1h-0.6V160.1z"/>
	</g>
	<path class="st0" d="M94.8,161c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
		c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
		c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
		C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
		c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
		C83.9,152.9,88.3,158.9,94.8,161"/>
	<g>
		<g>
			<defs>
				<rect id="SVGID_1_" x="3.9" y="6.8" width="214.9" height="203.2"/>
			</defs>
			<clipPath id="SVGID_2_">
				<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
			</clipPath>
			<g class="st1">
				<defs>
					<path id="SVGID_3_" d="M94.8,161c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
						c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
						c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
						C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
						c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
						C83.9,152.9,88.3,158.9,94.8,161"/>
				</defs>
				<clipPath id="SVGID_4_">
					<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
				</clipPath>
			</g>
		</g>
	</g>
	<g class="st2">
		<g>
			<defs>
				<rect id="SVGID_5_" x="3.9" y="12.2" width="214.9" height="197.8"/>
			</defs>
			<clipPath id="SVGID_6_">
				<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
			</clipPath>
			<path class="st3" d="M215.6,72.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
				c2.5,54.9-48.4,101.6-101.6,101.6C49.9,198.2,8.4,154,4,100.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
				C218.5,130,222.2,100.6,215.6,72.9"/>
		</g>
	</g>
	<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="107.8113" y1="62.1905" x2="84.9779" y2="90.1905">
		<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0"/>
		<stop  offset="0.4058" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
		<stop  offset="0.9913" style="stop-color:#787878;stop-opacity:0.6"/>
	</linearGradient>
	<path class="st4" d="M89.9,52.7c0,0-7.5,13.8-6,53.3c0,0,1.6-25.4,31.6-21.8c0,0-2-20.4,8.6-33.5L89.9,52.7z"/>
	<path class="st5" d="M161.1,12c0,0,11.3,8.3,14.8,12.4c0,0,4.7,4.6,7.8,8.9c0,0,3.9,4.6,9.1,13.6c0,0,5.3,10.2,6.5,13.6
		c0,0,3.8,10.5,4.2,13c0,0,2.3,10.2,2.4,12.2c0,0,1.4,12.1,1.1,13.9c0,0-0.2,9.6-0.8,12.3c0,0-1.1,8-2.3,11.3c0,0-2.2,7.6-3.6,10.4
		c0,0-4.3,9.8-5.9,12c0,0-6.4,10.1-8,11.7c0,0-25.7,40.8-87.2,40.5c0,0-58.1-0.9-87.3-64.1c0,0-2.7-6.6-4.1-11.9
		c0,0-3.1-12.2-3.3-16L4,100.7c0,0-0.6,11.7,1.7,23.1c0,0,13,76.4,95.8,86c0,0,70.7,6.6,102.7-53.2c0,0,24.9-42.2,9.6-90.5
		c0,0-5.2-18.6-17-31.9C196.8,34.2,183.2,17.4,161.1,12z"/>
</g>
</svg>






'; ?>
                                </a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- End Logo -->

                        <!-- Menu -->
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
                            <div class="menu">
                                <input type="hidden" value="<?= $last_order_number; ?>" name="lastOrderId" id="lastOrderId">
                                <!--<input type="hidden" value="5" name="lastOrderId" id="lastOrderId">-->
                                <nav class="navbar navbar-inverse navbar-static-top">
                                    <ul class="nav navbar-nav">
                                        <li <?php echo ($active == 'orders') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/orders'; ?>"><i class="demo-icon icon-ico-orders">&#xe801;</i> Orders</a></li>
                                        <li <?php echo ($active == 'menu') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>"><i class="demo-icon icon-ico-menu">&#xe805;</i> Menu</a></li>
                                        <li <?php echo ($active == 'preference') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/home/preference'; ?>"><i class="demo-icon icon-ico-preferences">&#xe802;</i> Preferences</a></li>
                                        <li <?php echo ($active == 'profile') ? 'class="active"' : '' ?>><a href="<?php echo base_url() . 'owner/profile'; ?>"><i class="demo-icon icon-ico-profile">&#xe804;</i> Profile</a></li>
                                        <li>
                                            <a href="<?php echo site_url('owner/home/logout'); ?>"> 
                                                Logout    <i class="demo-icon icon-off">&#xe808;</i>
                                            </a>  
                                        </li>
                                    </ul>
                                </nav>
                                <ul class="nav navbar-nav" style="float:right;color:#FFFFFF;margin-right:20px">
                                    <ul class="nav navbar-nav">
                                        </div>
                                        </div>
                                        <!-- End Menu -->
                                        </div>
                                        </div>
                                        <!-- End Logo Menu -->
                                        </div>

                                        <div class="clearfix"></div>
                                        </header>
                                        <!-- ===== End Header ===== -->

                                        <script>
//                                            var notification = new Notification('0');
                                            if (typeof (EventSource) !== "undefined") {
//                                                var source = new EventSource("<?php echo base_url() . $route . "/orders/loadNewOrder" ?>");
//                                                alert("SAd");
                                                var source = new EventSource("<?php echo base_url() . "owner/orders/loadNewOrder" ?>");
                                                source.onmessage = function (event) {
                                                    var data = event.data;
                                                    var datas = data.split("###");
                                                    var count = datas[0];
                                                    var content = datas[1];
                                                    var newOrderrId = datas[2];
                                                    var oldOrderId = $("#lastOrderId").val();
                                                    if (oldOrderId == '') {
                                                        oldOrderId = 0;
                                                    }
                                                    var oldcount = $('#hidecount').val();
                                                    $('#tablebody').html(content);
                                                    $('#tablebody_tab').html(content);
                                                    $('#tablebody_mob').html(content);
                                                    $('#tablebody_tab .hide-desk').removeClass('hide');
                                                    $('#tablebody_mob .hide-desk').removeClass('hide');
                                                    $('#tablebody .hide-desk').hide();
                                                    $('#tablebody_tab .hide-tab').hide();
                                                    $('#tablebody_mob .hide-mob').hide();
                                                    $('.facebook').html($('#fb_svg').html());
                                                    $('.web').html($('#web_svg').html());
                                                    $('.app').html($('#app_svg').html());
                                                    $('#countnew').html(count);
                                                    $('.badgenew').html(count);
                                                    $('#hidecount').val(count);
                                                    $("#lastOrderId").val(newOrderrId);
                                                    var now = new Date().getTime();
//                                                    console.log("new" + newOrderrId);
//                                                    console.log(oldOrderId);
//                                                    navigator.serviceWorker.register('sw.js');
                                                    navigator.serviceWorker.register('<?= base_url(); ?>assets/js/chain/notification-generator-sw.js');
//                                                    new Notification("sid");
                                                    if (newOrderrId > oldOrderId) {
                                                        if (!("Notification" in window)) {
                                                            console.log("This browser does not support desktop notification");
                                                        }
                                                        else if (Notification.requestPermission === "granted") {
//                                                            new Notification('');
                                                            var options = {
                                                                body: "You have a new order",
                                                                icon: "<?= base_url(); ?>images/logo-icon.png",
                                                                date: new Date(now + 5 * 1000),
                                                                dir: "ltr"
                                                            };
                                                            var notification = new Notification("Hi ,", options);
                                                            notification.onclick = function () {
//                                                                window.open("<?= base_url() ?>owner/orders/lists");
                                                            };

                                                        }
                                                        else if (Notification.permission !== 'denied') {
                                                            Notification.requestPermission(function (permission) {
                                                                if (!('permission' in Notification)) {
                                                                    Notification.permission = permission;
                                                                }

                                                                if (permission === "granted") {
                                                                    var options = {
                                                                        body: "You have a new order",
                                                                        icon: "<?= base_url(); ?>images/logo-icon.png",
                                                                        date: new Date(now + 5 * 1000),
                                                                        dir: "ltr"
                                                                    };
                                                                    var notification = new Notification("Hi ,", options);
                                                                    notification.onclick = function () {
//                                                                        window.open("<?= base_url() ?>owner/orders/lists");
                                                                    };

                                                                }
                                                            });
                                                        }
                                                    }
                                                    if (count != oldcount && oldcount != '0') {

//                                                        setTimeout(function () {
//                                                            $('#toast').show();
//                                                            $('#toast').fadeOut(5000);
//                                                        }, 100);
                                                    } else {


                                                    }
                                                    //document.getElementById("countnew").innerHTML = count;
                                                };
                                            } else {
                                                document.getElementById("tablebody").innerHTML = "Sorry, your browser does not support server-sent events...";
                                            }


                                        </script>
                                        <style>
                                            #toast{
                                                background-position: 15px center;
                                                background-repeat: no-repeat;
                                                border-radius: 3px;
                                                box-shadow: 0 0 12px #999999;
                                                margin: 0 0 6px;
                                                opacity: 0.8;
                                                overflow: hidden;
                                                padding: 15px 15px 15px 50px;
                                                pointer-events: auto;
                                                position: Fixed;
                                                width: 300px;
                                                bottom:0;
                                                z-index:999999999;
                                            }
                                        </style>
                                        <div id="toast" style="display:none;" >A New Order Placed</div>
                                        <input type="hidden" name="hidecount" value="0" id="hidecount">
