       
<!-- ===== Start Section Main ===== -->
<section class="main-sec dash-chef">
   <div class="menu col-lg-12" style="background-color:#fff; padding:10px 10px 0;">
                                            <nav class="text-right" style="color:#2d2d2d">
                                                <?php
                                                if ($this->user->restaurant_name != '') {
                                                    echo 'WELCOME ' . $this->user->restaurant_name;
                                                }
                                                ?>  
                                            </nav>
                                        </div>
    <div class="container-fluid padding-zero">
        <div class="row">

            <!-- Menu Box -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 display-ful">
                <div class="row">
                    <!-- == MENU == -->
                    <!-- Desktop -->
                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 desktop display-ful">
                        <div class="menu-box menu">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/orders.jpg" alt="orders" title="orders" />
                            <a href="<?php echo base_url() . 'owner/orders'; ?>">
                                <i class="demo-icon icon-ico-orders">&#xe801;</i>
                                <h4>Orders</h4>
                                View all your orders <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Desktop -->

                    <!-- Ipad Mobile -->
                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 ipad-mob">
                        <div class="menu-box menu">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/orders.jpg" alt="orders" title="orders" />
                            <a href="<?php echo base_url() . 'owner/orders'; ?>">
                                <i class="demo-icon icon-ico-orders">&#xe801;</i>
                                <h4>Orders</h4>
                                View all your orders <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Ipad Mobile -->
                    <!-- == END MENU == -->

                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                        <div class="row">
                            <!-- == PREFERENCE == -->
                            <!-- Desktop -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 desktop display-ful">
                                <div class="menu-box preference">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/preference.jpg" alt="preference" title="preference" />
                                    <a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe802;</i>
                                        <h4>Subscription Payment</h4>
                                        Manage Subscription Payment Report<i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->

                            <!-- Ipad Mobile -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ipad-mob display-ful">
                                <div class="menu-box preference">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/preference.jpg" alt="preference" title="preference" style="max-height:125px;" />
                                    <a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe802;</i>
                                        <h4>Subscription Payment</h4>
                                        Manage Subscription Payment Report<i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                         
                            
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 desktop display-ful">
                                <div class="menu-box Orders_Payout">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/profile.jpg" alt="profile" title="profile" />
                                    <a href="<?php echo base_url() . 'payment_report/order_report/'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe804;</i>
                                        <h4>Orders Payout</h4>
                                        Manage Orders Payout Report <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->

                            <!-- Ipad Mobile -->
                            
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ipad-mob display-ful">
                                <div class="menu-box profile">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/profile.jpg"  alt="profile" title="profile" style="max-height:125px;"/>
                                    <a href="<?php echo base_url() . 'payment_report/order_report/'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe804;</i>
                                        <h4>Orders Payout</h4>
                                        Manage Orders Payout Report <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->
                            <!-- == PROFILE == -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Menu Box -->


            <!-- Menu Box -->
            
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 display-ful">
                <div class="row">
                    <!-- == MENU == -->
                    <!-- Desktop -->
                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 desktop display-ful">
                        <div class="menu-box menu">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/menu.jpg" alt="menu" title="menu" />
                            <a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>">
                                <i class="demo-icon icon-ico-menu">&#xe805;</i>
                                <h4>Menu</h4>
                                Manage your menu <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Desktop -->

                    <!-- Ipad Mobile -->
                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 ipad-mob">
                        <div class="menu-box menu">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/menu2.jpg" alt="menu" title="menu" />
                            <a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>">
                                <i class="demo-icon icon-ico-menu">&#xe805;</i>
                                <h4>Menu</h4>
                                Manage your menu <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Ipad Mobile -->
                    <!-- == END MENU == -->

                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                        <div class="row">
                            <!-- == PREFERENCE == -->
                            <!-- Desktop -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 desktop display-ful">
                                <div class="menu-box preference">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/preference.jpg" alt="preference" title="preference" />
                                    <a href="<?php echo base_url() . 'owner/home/preference'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe802;</i>
                                        <h4>Preferences</h4>
                                        Change your settings <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->

                            <!-- Ipad Mobile -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ipad-mob display-ful">
                                <div class="menu-box preference">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/preference2.jpg" alt="preference" title="preference" />
                                    <a href="<?php echo base_url() . 'owner/home/preference'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe802;</i>
                                        <h4>Preferences</h4>
                                        Change your settings <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->
                            <!-- == PREFERENCE == -->



                            <!-- == PROFILE == -->
                            <!-- Desktop -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 desktop display-ful">
                                <div class="menu-box profile">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/profile.jpg" alt="profile" title="profile" />
                                    <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe804;</i>
                                        <h4>Profile</h4>
                                        Manage profile information <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->

                            <!-- Ipad Mobile -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ipad-mob display-ful">
                                <div class="menu-box profile">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/profile2.jpg" alt="profile" title="profile" />
                                    <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe804;</i>
                                        <h4>Profile</h4>
                                        Manage profile information <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Desktop -->
                            <!-- == PROFILE == -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Menu Box -->

            <div class="clearfix"></div>


        </div>
    </div>
</section>
<!-- ===== End Section Main ===== -->
