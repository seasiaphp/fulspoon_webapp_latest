<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon | Payment report</title>

        <!-- Define Charset -->
        <meta charset="utf-8">

        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">


        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/responsive.css" media="screen">


        <!-- Margo JS  -->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.parallax.js"></script>
        <script src="<?php echo base_url(); ?>http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.slicknav.js"></script>

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" media="screen">
        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/colors/red.css" title="red" media="screen" />
        <!-- Margo JS  -->



  <!--[if IE 8]><script src="<?php echo base_url(); ?>http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="<?php echo base_url(); ?>http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Container -->
        <div id="container">

            <!-- Start Header -->
            <div class="hidden-header"></div>
            <!-- Start Header ( Logo & Naviagtion ) -->
            <div class="navbar navbar-default navbar-top greenbdr">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="<?php echo base_url(); ?>index.html">
                            <img alt="" src="<?php echo base_url(); ?>images/replimatic_logo.png">
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="navbar-collapse collapse borderbg">       
                        <!-- Stat Search -->
                        <div class="buttons-side">
                            <a class="btn btn-default btnlogin" href="<?php echo base_url(); ?>#" role="button">LOGIN</a>
                            <a class="btn btn-default btnsignup" href="<?php echo base_url(); ?>#" role="button">SIGN UP</a>
                        </div>
                        <!-- End Search -->
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="<?php echo base_url(); ?>contact.html" class="linkcolor">For Restaurants Owners</a>
                            </li>             
                        </ul>
                        <!-- End Navigation List -->

                    </div>
                </div>

                <!-- Mobile Menu Start -->
                <ul class="wpb-mobile-menu">         
                    <li><a href="<?php echo base_url(); ?>contact.html">For Restaurants Owners</a></li> 
                    <li><a href="<?php echo base_url(); ?>contact.html">Login</a></li>  
                    <li><a href="<?php echo base_url(); ?>contact.html">Sign Up</a></li>               
                </ul> 
                <!-- Mobile Menu End -->

            </div>
            <!-- End Header ( Logo & Naviagtion ) -->

        </header>
        <!-- End Header -->

        <!-- Start Content -->
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"> 
                        <h2 class="pageheader_report">Subscription Payment Report</h2>
                        <div class="page_sub_header">
                            <div class="col-md-6">
                                Current Week Report
                            </div>
                            <div class="col-md-6 text-right">
                                Due On : 4/18/2016
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="report_table">

                            <div class="col-md-6 pad0">
                                <div class="col-xs-8"># Newtt Subscribers of the Month</div>
                                <div class="col-xs-4">15</div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-6 pad0">
                                <div class="col-xs-8"># of Existing Customers</div>
                                <div class="col-xs-4">6</div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-6 pad0">
                                <div class="col-xs-8">Amount Due</div>
                                <div class="col-xs-4">84</div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-6 pad0">
                                <div class="col-xs-8">Due Date</div>
                                <div class="col-xs-4">1ST OF MAY 2016</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                        <div id="report_tbl">
                            <div class="action_bar">
                                <div class="col-sm-6">
                                    <div class="col-sm-7 pad0">START</div>
                                    <div class="col-sm-5 pad0">
                                        <div class="dropdown">
                                            <button class="btn btn-defaultn btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                SELECT DATE
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="<?php echo base_url(); ?>#">Action</a></li>
                                                <li><a href="<?php echo base_url(); ?>#">Another action</a></li>
                                                <li><a href="<?php echo base_url(); ?>#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-7 pad0">START</div>
                                    <div class="col-sm-5 pad0">
                                        <div class="dropdown">
                                            <button class="btn btn-defaultn btn-block dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                SELECT DATE
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <li><a href="<?php echo base_url(); ?>#">Action</a></li>
                                                <li><a href="<?php echo base_url(); ?>#">Another action</a></li>
                                                <li><a href="<?php echo base_url(); ?>#">Something else here</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                            <table class="col-md-12 table-bordered  table-condensed cf">
                                <thead>
                                    <tr>
                                        <td class="numeric header_tblrt">Month</td>
                                        <td class="numeric header_tblrt"># New Subscribers of the Month</td>
                                        <td class="numeric header_tblrt"># of Existing Customers</td>
                                        <td class="numeric header_tblrt"># of Subscripers left</td>
                                        <td class="numeric header_tblrt">TOTAL</td>
                                        <td class="numeric header_tblrt">AMOUNT PAID</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="callabel" data-title="Montht">3/1/2016</td>
                                        <td data-title="# New Subscribers of the Month" class="numeric">15</td>
                                        <td data-title="# of Existing Customers" class="numeric">24</td>
                                        <td data-title="# of Subscripers left" class="numeric"><input class="form-control text_report" placeholder="ENTER"></td>
                                        <td data-title="TOTAL" class="numeric">40</td>
                                        <td data-title="AMOUNT PAID" class="numeric">$160.00</td>
                                    </tr>
                                    <tr>
                                        <td class="callabel" data-title="Montht">3/1/2016</td>
                                        <td data-title="# New Subscribers of the Month" class="numeric">15</td>
                                        <td data-title="# of Existing Customers" class="numeric">24</td>
                                        <td data-title="# of Subscripers left" class="numeric">12</td>
                                        <td data-title="TOTAL" class="numeric">40</td>
                                        <td data-title="AMOUNT PAID" class="numeric">$160.00</td>
                                    </tr>
                                    <tr>
                                        <td class="callabel" data-title="Montht">3/1/2016</td>
                                        <td data-title="# New Subscribers of the Month" class="numeric">15</td>
                                        <td data-title="# of Existing Customers" class="numeric">24</td>
                                        <td data-title="# of Subscripers left" class="numeric">12</td>
                                        <td data-title="TOTAL" class="numeric">40</td>
                                        <td data-title="AMOUNT PAID" class="numeric">$160.00</td>
                                    </tr>
                                    <tr>
                                        <td class="callabel" data-title="Montht">3/1/2016</td>
                                        <td data-title="# New Subscribers of the Month" class="numeric">15</td>
                                        <td data-title="# of Existing Customers" class="numeric">24</td>
                                        <td data-title="# of Subscripers left" class="numeric">12</td>
                                        <td data-title="TOTAL" class="numeric">40</td>
                                        <td data-title="AMOUNT PAID" class="numeric">$160.00</td>
                                    </tr>
                                    <tr>
                                        <td class="callabel" data-title="Montht">3/1/2016</td>
                                        <td data-title="# New Subscribers of the Month" class="numeric">15</td>
                                        <td data-title="# of Existing Customers" class="numeric">24</td>
                                        <td data-title="# of Subscripers left" class="numeric">12</td>
                                        <td data-title="TOTAL" class="numeric">40</td>
                                        <td data-title="AMOUNT PAID" class="numeric">$160.00</td>
                                    </tr>

                                    <tr>
                                        <td colspan="5" style="text-align:center !important;" data-title="" class="numeric"><b>TOTAL</b></td>
                                        <td data-title="" class="numeric"><b>$596.00</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <!--end of .table-responsive-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End content -->


    <!-- Start Footer Section -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4"> 
                    <ul class="footer-nav">
                        <li><a href="<?php echo base_url(); ?>#">Restaurant Owners</a>      
                        <li><a href="<?php echo base_url(); ?>#">About</a>        
                        <li><a href="<?php echo base_url(); ?>#">Contact Us</a>       
                        <li><a href="<?php echo base_url(); ?>#">Privacy policy</a> 
                    </ul>
                </div>
                <div class="col-md-4 text-center padb20"><img src="<?php echo base_url(); ?>images/footer-replimatic.png" alt=""/> <p>&copy; 2015 Replimatic. All rights reserved. </p>  </div>
                <div class="col-md-3 col-md-offset-1 socialnav">
                    Follow us!
                    <p>
                        <a href="<?php echo base_url(); ?>#"><i class="fa fa-facebook"></i></a>
                        <a href="<?php echo base_url(); ?>#"><i class="fa fa-twitter"></i></a>
                        <a href="<?php echo base_url(); ?>#"><i class="fa fa-instagram"></i></a>
                        <a href="<?php echo base_url(); ?>#"><i class="fa fa-pinterest-p"></i></a>
                    </p>
                </div>


            </div>
            <!-- .row -->
        </div>
    </footer>
    <!-- End Footer Section -->



</div>
<!-- End Container -->

<!-- Go To Top Link -->
<a href="<?php echo base_url(); ?>#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>
</body>

</html>