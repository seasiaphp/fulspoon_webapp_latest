

<!-- ===== Start Section Main ===== -->
<section class="main-sec dash-owner">
    <div class="container-fluid padding-zero">
        <!-- == DESKTOP VIEW == -->
        <div class="row desktop">
            <!-- Order -->
            <div class="col-lg-3 col-md-3">
                <div class="menu-box orders">
                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/order.jpg" alt="orders" title="orders" />
                    <a href="<?php echo base_url() . 'owner/orders'; ?>">
                        <i class="demo-icon icon-ico-orders">&#xe801;</i>
                        <h4>Orders</h4>
                        View all your orders <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <!-- End Order -->

            <!-- Order -->
            <div class="col-lg-9 col-md-9">
                <div class="row">
                    <!-- Report -->
                    <div class="col-md-4">
                        <div class="menu-box report">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/reports.jpg" alt="reports" title="reports" />
                            <a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>">
                                <i class="demo-icon icon-ico-menu">&#xe805;</i>
                                <h4>Menu</h4>
                                View all your Menu <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Report -->

                    <!-- Customers -->
                    <div class="col-md-8">
                        <div class="menu-box customers">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/customer.jpg" alt="customers" title="customers" />
                            <a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>">
                                <i class="demo-icon icon-ico-preferences">&#xe807;</i>
                                <h4>Subscription Payment</h4>
                                Manage Subscription Payment Report <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Customers -->

                    <!-- Locations -->
                    <div class="col-md-4">
                        <div class="menu-box locations">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/location.jpg" alt="locations" title="locations" />
                            <a href="<?php echo base_url() . 'payment_report/order_report/'; ?>">
                                <i class="demo-icon icon-ico-profile">&#xe800;</i>
                                <h4>Orders Payout</h4>
                                Manage Orders Payout Report  <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Locations -->

                    <!-- Profile -->
                    <div class="col-md-4">
                        <div class="menu-box profile">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/profile.jpg" alt="profile" title="profile" />
                            <a href="<?php echo base_url() . 'owner/home/preference'; ?>">
                                <i class="demo-icon icon-ico-preferences">&#xe804;</i>
                                <h4>Preferences</h4>
                                Change your settings  <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Profile -->

                    <!-- Preferences -->
                    <div class="col-md-4">
                        <div class="menu-box preferences">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/preference.jpg" alt="preferences" title="preferences" />
                            <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                <i class="demo-icon icon-ico-profile">&#xe802;</i>
                                <h4>Profile</h4>
                                Manage profile information 
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Preferences -->
                </div>
            </div>
            <!-- End Order -->

            <div class="clear"></div>


        </div>
        <!-- == END DESKTOP VIEW == -->

        <!-- == IPAD VIEW == -->
        <div class="row ipad-mob">
            <!-- Order -->
            <div class="col-md-12 col-sm-12">
                <div class="menu-box orders">
                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/order2.jpg" alt="orders" title="orders" />
                    <a href="<?php echo base_url() . 'owner/orders'; ?>">
                        <i class="demo-icon icon-ico-orders">&#xe801;</i>
                        <h4>Orders</h4>
                        View all your orders <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <!-- End Order -->

            <!-- Other Menus -->
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <!-- Customers -->
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <div class="menu-box customers">
                            <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/customer2.jpg" alt="customers" title="customers" />
                            <a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>">
                                <i class="demo-icon icon-ico-menu">&#xe807;</i>
                                <h4>Menu</h4>
                                Manage your menu  <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Customers -->

                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <div class="row">
                            <!-- Reports -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="menu-box report">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/reports2.jpg" alt="reports" title="reports" />
                                    <a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe805;</i>
                                        <h4>Subscription Payment</h4>
                                        Manage Subscription Payment Report <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Reports -->

                            <!-- Locations -->
                            <div class="col-md-6 col-xs-6 col-xs-6">
                                <div class="menu-box locations">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/location.jpg" alt="locations" title="locations" />
                                    <a href="<?php echo base_url() . 'payment_report/order_report/'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe800;</i>
                                        <h4>Orders Payout</h4>
                                        Manage Orders Payout Report  <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Locations -->

                            <!-- Profile -->
                            <div class="col-md-6 col-xs-6 col-xs-6">
                                <div class="menu-box profile">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/profile.jpg" alt="profile" title="profile" />
                                    <a href="<?php echo base_url() . 'owner/home/preference'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe804;</i>
                                        <h4>Preferences</h4>
                                        Change your settings  <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Profile -->


                            <!-- Preferences -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="menu-box preferences">
                                    <img src="<?php echo base_url(); ?>assets/dashboard/images/chefs/preference2.jpg" alt="preferences" title="preferences" />
                                    <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe802;</i>
                                        <h4>Profile</h4>
                                        Manage profile information  <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Preferences -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Other Menus -->


        </div>
        <!-- == END IPAD VIEW == -->
    </div>
</section>
<!-- ===== End Section Main ===== -->

