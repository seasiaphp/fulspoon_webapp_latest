   
<script>

    function submitForm() {
        $("#frmPack").submit();
    }
    function cancel()
    {

        window.location.href = "<?php echo base_url() ?>admin/home";
    }
</script>
<?php
$this->load->library('session');
?>


<!-- ===== Section Sign In ===== -->
<section class="preferences">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-10 col-center">
                <div class="preferences-main">
                    <h1>Preferences Details</h1>

                    <!-- Preferences Form -->
                    <div class="preferences-form">
                        <?php if ($this->session->flashdata('error_message') != '') { ?>
                            <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
                        <?php } else { ?>
                            <div class="alert alert-danger" role="alert" style="display:none;"></div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success_message') != '') { ?>
                            <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
                        <?php } else { ?>
                            <div class="alert alert-success" role="alert" style="display:none;"></div>
                        <?php } ?>

                        <form method="post" name="formlist"  id="formlist" action=""  onsubmit="">
                            <?php
                            $i = 0;
                            ?>
                            <div class="input-group">
                                <input type="text"  placeholder="Minimum pickup time" name="Minimum_pickup_time"  value="<?php echo $restaurent['pickup_time']; ?>">
                                <div class="input-group-addon">Minimum Pickup Time in Minutes</div>
                            </div>
                            <div class="input-group">
                                <input type="number"  placeholder="Sales Tax Rate %" class="tax_rate percentage" name="tax" id="tax"  value="<?php echo $restaurent['tax']; ?>">
                                <div class="input-group-addon">Sales Tax Rate %</div>
                            </div>
                            <div class="input-group"> 
                                <input type="text"  placeholder="Minimum delivery time" name="Minimum_delivery_time"  value="<?php echo $restaurent['delivery_time']; ?>"><div class="input-group-addon">Minimum Delivery Time in Minutes</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <input style="background: #6fbe44" type="submit" value="Submit" >
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button onclick="cancel();">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Preferences Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ===== End Section Sign In ===== -->
