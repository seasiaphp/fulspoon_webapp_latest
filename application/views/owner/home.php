       

<!-- ===== Start Section Main ===== -->
<section class="main-sec dash-owner">
    <div class="container-fluid padding-zero">
        <!-- == DESKTOP VIEW == -->
        <div class="row desktop">
            <!-- Order -->
            <div class="col-lg-3 col-md-3">
                <div class="menu-box orders">
                    <img src="<?= base_url() ?>assets/dashboard/images/owners/order.jpg" alt="orders" title="orders" />
                    <a href="<?php echo base_url() . 'owner/orders'; ?>">
                        <i class="demo-icon icon-ico-orders">&#xe801;</i>
                        <h4>Orders</h4>
                        View all your orders <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <!-- End Order -->

            <!-- Order -->
            <div class="col-lg-9 col-md-9">
                <div class="row">
                    <!-- Report -->
                    <div class="col-md-4">
                        <div class="menu-box report">
                            <img src="<?= base_url() ?>assets/dashboard/images/owners/reports.jpg" alt="reports" title="reports" />
                            <a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>">
                                <i class="demo-icon icon-ico-reports">&#xe805;</i>
                                <h4>Subscription Payment</h4>
                                Manage Subscription Payment Report <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Report -->

                    <!-- Customers -->
                    <div class="col-md-8">
                        <div class="menu-box customers">
                            <img src="<?= base_url() ?>assets/dashboard/images/owners/customer.jpg" alt="customers" title="customers" />
                            <a href="<?php echo base_url() . 'payment_report/order_report/'; ?>">
                                <i class="demo-icon icon-ico-customers">&#xe807;</i>
                                <h4>Orders Payout</h4>
                                Manage Orders Payout Report<i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Customers -->

                    <!-- Locations -->
                    <div class="col-md-4">
                        <div class="menu-box locations">

                            <img src="<?= base_url() ?>assets/dashboard/images/owners/profile.jpg" alt="profile" title="profile" />
                            <a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>">
                                <i class="demo-icon icon-ico-locations">&#xe800;</i>
                                <h4>Menu</h4>
                                Manage your menu <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Locations -->

                    <!-- Profile -->
                    <div class="col-md-4">
                        <div class="menu-box profile">
                            <img src="<?= base_url() ?>assets/dashboard/images/owners/location.jpg" alt="locations" title="locations" />
                            <a href="<?php echo base_url() . 'owner/home/preference'; ?>">
                                <i class="demo-icon icon-ico-profile">&#xe804;</i>
                                <h4>Preferences</h4>
                                Manage profile information <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Profile -->

                    <!-- Preferences -->
                    <div class="col-md-4">
                        <div class="menu-box preferences">
                            <img src="<?= base_url() ?>assets/dashboard/images/owners/preference.jpg" alt="preferences" title="preferences" />
                            <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                <i class="demo-icon icon-ico-preferences">&#xe802;</i>
                                <h4>Profile</h4>
                                Manage profile information <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Preferences -->
                </div>
            </div>
            <!-- End Order -->

            <div class="clear"></div>

            <!-- Promocodes -->
            <!--            <div class="col-lg-6 col-md-6">
                            <div class="menu-box promocodes">
                                <img src="<?= base_url() ?>assets/dashboard/images/owners/promocode-new.jpg" alt="promocodes" title="promocodes" />
                                <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                    <i class="demo-icon icon-ico-promocodes">&#xe803;</i>
                                    <h4>Profile</h4>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>-->
            <!-- End Promocodes -->

            <!-- Promocodes -->
            <!--            <div class="col-lg-6 col-md-6">
                            <div class="menu-box promocodes">
                                <img src="<?= base_url() ?>assets/dashboard/images/owners/messages.jpg" alt="messages" title="messages" />
                                <a href="#">
                                    <i class="fa fa-envelope-o"></i>
                                    <h4>Profile</h4>
                                    Manage your messages <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>-->
            <!-- End Promocodes -->
        </div>
        <!-- == END DESKTOP VIEW == -->

        <!-- == IPAD VIEW == -->
        <div class="row ipad-mob">
            <!-- Order -->
            <div class="col-md-12 col-sm-12">
                <div class="menu-box orders">
                    <img src="<?= base_url() ?>assets/dashboard/images/owners/order2.jpg" alt="orders" title="orders" />
                    <a href="<?php echo base_url() . 'owner/orders'; ?>">
                        <i class="demo-icon icon-ico-orders">&#xe801;</i>
                        <h4>Orders</h4>
                        View all your orders <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <!-- End Order -->

            <!-- Other Menus -->
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <!-- Customers -->
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <div class="menu-box customers">
                            <img src="<?= base_url() ?>assets/dashboard/images/owners/customer2.jpg" alt="customers" title="customers" />
                            <a href="<?php echo base_url() . 'payment_report/payment_details/'; ?>">
                                <i class="demo-icon icon-ico-customers">&#xe807;</i>
                                <h4>Subscription Payment</h4>
                                Manage Subscription Payment Report <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End Customers -->

                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <div class="row">
                            <!-- Reports -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="menu-box report">
                                    <img src="<?= base_url() ?>assets/dashboard/images/owners/reports2.jpg" alt="reports" title="reports" />
                                    <a href="<?php echo base_url() . 'payment_report/order_report/'; ?>">
                                        <i class="demo-icon icon-ico-reports">&#xe805;</i>
                                        <h4>Orders Payout</h4>
                                        Manage Orders Payout Report <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Reports -->

                            <!-- Locations -->
                            <div class="col-md-6 col-xs-6 col-xs-6">
                                <div class="menu-box locations">
                                    <img src="<?= base_url() ?>assets/dashboard/images/owners/profile.jpg" alt="profile" title="profile" />
                                    <a href="<?php echo base_url() . 'owner/menu/category/' . $restaurant_id; ?>">
                                        <i class="demo-icon icon-ico-locations">&#xe800;</i>
                                        <h4>Menu</h4>
                                        Manage your locations <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Locations -->

                            <!-- Profile -->
                            <div class="col-md-6 col-xs-6 col-xs-6">
                                <div class="menu-box profile">
                                    <img src="<?= base_url() ?>assets/dashboard/images/owners/location.jpg" alt="locations" title="locations" />

                                    <a href="<?php echo base_url() . 'owner/home/preference'; ?>">
                                        <i class="demo-icon icon-ico-profile">&#xe804;</i>
                                        <h4>Preferences</h4>
                                        Change your settings<i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Profile -->


                            <!-- Preferences -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="menu-box preferences">
                                    <img src="<?= base_url() ?>assets/dashboard/images/owners/preference2.jpg" alt="preferences" title="preferences" />
                                    <a href="<?php echo base_url() . 'owner/profile'; ?>">
                                        <i class="demo-icon icon-ico-preferences">&#xe802;</i>
                                        <h4>Profile</h4>
                                        Manage profile information<i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- End Preferences -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Other Menus -->

            <!-- Promocodes -->

            <!-- End Promocodes -->

            <!-- Promocodes -->

            <!-- End Promocodes -->
        </div>
        <!-- == END IPAD VIEW == -->
    </div>
</section>
<!-- ===== End Section Main ===== -->
