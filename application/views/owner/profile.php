   
<script>

    function submitForm() {
        $("#frmPack").submit();
    }


    function isNumberCustom(evt) {

        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        console.log(charCode);
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//                                                    alert("sadsa");
            evt.preventDefault();
            return false;
        } else {
            return true;
        }
    }
    function cancel()
    {
        window.location.href = "<?php echo base_url() ?>admin/home";
    }
</script>
<style>
    .select1{
        border: 1px solid  #e4e4e4;
        color: #757575;
        font-size: 12px;
        line-height: 20px;
        font-weight: 400;
        padding: 15px;
        cursor: pointer;
        display: block;
        width: 100%

    }
    h3{
        color: #6fbe44;
    }

</style>
<?php
$this->load->library('session');
?>
<?php
//echo '<pre>';
//print_r($profile);
//exit;
?>

<!-- ===== Section Sign In ===== -->
<section class="preferences">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-10 col-center">
                <div class="preferences-main">
                    <h1>Profile Details</h1>
                    <h6 style="font-size:10px">To update your profile information such as address, phone or fax number, please update your listing on <a href="https://www.google.com/business/" target="_blank"> Google Places </a> or contact Fulspoon at support@fulspoon.com.</h6>

                    <!-- Preferences Form -->
                    <div class="preferences-form">
                        <?php if ($this->session->flashdata('error_message') != '') { ?>
                            <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error_message'); ?></div>
                        <?php } else { ?>
                            <div class="alert alert-danger" role="alert" style="display:none;"></div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success_message') != '') { ?>
                            <div class="alert alert-success" role="alert"><?php echo "Profile updated successfully"; ?></div>
                        <?php } else { ?>
                            <div class="alert alert-success" role="alert" style="display:none;"></div>
                        <?php } ?>

                        <h3>Delivery Details</h3>
                        <form method="post" name="formlist"  id="formlist" action=""  onsubmit="">
                            <input type="hidden" value="<?php echo $profile['restaurant_id']; ?>" name="restaurant_id"  />


                            <div class="input-group">
                                <!--<input type="hidden" placeholder="<?php echo $pref->title; ?>" class="form-control" name="title[]" id="<?php echo $pref->title; ?>"  value="<?php echo $pref->id; ?>">-->
                                <!--<input type="text"  placeholder="" name="<?php echo $pref->id; ?>" id="<?php echo $pref->field; ?>"  value="<?php echo$pref->value; ?>">-->
                                <select class="select1" name="delivery">
                                    <option value="no">Delivery</option>
                                    <option <?php
                                    if ($profile['delivery'] == "yes") {
                                        echo 'selected';
                                    }
                                    ?> value="yes">Yes</option>
                                    <option <?php
                                    if ($profile['delivery'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                </select>
                                <div class="input-group-addon">Delivery</div>
                            </div>
                            <div class="input-group">
                                <input type="text" onkeypress="isNumberCustom(event)"  placeholder="Delivery Fee" name="delivery_fee" id=""  value="<?php echo $profile['delivery_fee']; ?>">
                                <div class="input-group-addon">Delivery Fee</div>
                            </div>

                            <div class="input-group">
                                <input type="text" onkeypress="isNumberCustom(event)"   placeholder="Delivery radius" name="areas_delivery" id=""  value="<?php echo $profile['areas_delivery']; ?>">
                                <div class="input-group-addon">Delivery radius</div>
                            </div>


                            <div class="input-group">
                                <input type="text" onkeypress="isNumberCustom(event)"  placeholder="Minimum order" name="minimum_order" id=""  value="<?php echo $profile['minimum_order'] ?>">
                                <div class="input-group-addon">Minimum order</div>
                            </div>



                            <h3>Timezone</h3>
                            <div class="input-group">
                                <select class="select1" name="timezone">


                                    <?php foreach ($timezone_list as $timezone) { ?>
                                        <option   <?php if ($profile['timezone'] == $timezone['name']) { ?> selected <?php } ?>  value="<?php echo $timezone['name']; ?>"><?php echo $timezone['display']; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="input-group-addon">Timezone</div>
                            </div>





                            <h3>Change Password</h3>
                            <div class="input-group">
                                <input id="" type="text" value="********" name="password" placeholder="Password">
                                <div class="input-group-addon">Password</div>
                            </div>

                            <h3>Direct Deposit details</h3>
                            <?php
                            if ($profile['AccountNumber'] == 3) {
                                $profile['AccountNumber'] = '';
                            }
                            if ($profile['RoutingNumber'] == 3) {
                                $profile['RoutingNumber'] = '';
                            }
                            if ($profile['bank_name'] == 3) {
                                $profile['bank_name'] = '';
                            }
                            ?>
                            <div class="input-group">
                                <input type="text"  placeholder="First Name" name="first_name_strip" id=""  value="<?php echo $profile['first_name_strip']; ?>">
                                <input type="hidden" name="first_name_strip_ck"  value="<?php echo $profile['first_name_strip']; ?>" >
                                <div class="input-group-addon">First Name</div>
                            </div>
                            <div class="input-group">
                                <input type="text"  placeholder="Last Name" name="last_name_strip" id=""  value="<?php echo $profile['last_name_strip']; ?>">
                                <input type="hidden" name="last_name_strip_ck"  value="<?php echo $profile['last_name_strip']; ?>" >
                                <div class="input-group-addon">Last Name</div>
                            </div>
                            <div class="input-group">
                                <input type="text"  placeholder="Email" name="email" id=""  value="<?php echo $profile['email']; ?>">

                                <div class="input-group-addon">Email</div>
                            </div>
                            <div class="input-group">
                                <input type="text" onkeypress="isNumberCustom(event)"  placeholder="ssn" name="ssn" id=""  value="<?php echo $profile['ssn']; ?>">
                                <input type="hidden" name="ssn_ck"  value="<?php echo $profile['ssn']; ?>" >
                                <div class="input-group-addon">ssn</div>
                            </div>

                            <div class="input-group">
                                <input type="text"  placeholder="Account Number" name="AccountNumber" id=""  value="<?php echo $profile['AccountNumber']; ?>">
                                <input type="hidden" name="AccountNumber_ck"  value="<?php echo $profile['AccountNumber']; ?>" >
                                <div class="input-group-addon">Account Number</div>
                            </div>

                            <!--                            <h3>Payment Details</h3>-->
                            <div class="input-group">
                                <input type="text"  placeholder="Bank Name" name="bank_name" id=""  value="<?php echo $profile['bank_name']; ?>">      
                                <input type="hidden" name="bank_name_ck"  value="<?php echo $profile['bank_name']; ?>" >
                                <div class="input-group-addon">Bank Name</div>
                            </div>
                            <div class="input-group">
                                <input type="text" onkeypress="isNumberCustom(event)" placeholder="Routing Number" name="RoutingNumber" id=""  value="<?php echo $profile['RoutingNumber']; ?>">
                                <input type="hidden" name="RoutingNumber_ck"  value="<?php echo $profile['RoutingNumber']; ?>" >
                                <div class="input-group-addon">Routing Number</div>
                            </div>


                            <div class="input-group">
                                <select class="select1"  name="account_type">
                                    <option value="">Payment Method</option>
                                    <option <?php
                                    if ($profile['account_type'] == "ACH") {
                                        echo 'selected';
                                    }
                                    ?>>ACH</option>
                                    <option <?php
                                    if ($profile['account_type'] == "check") {
                                        echo 'selected';
                                    }
                                    ?> value="check">CHECK BOOK</option>
                                </select>
                                <div class="input-group-addon">Payment Method</div>
                            </div>
<?php
if ($feature_config['value'] == "Y") {
    ?>
                                <h3> Feature Details </h3>
                                <div class="input-group">
                                    <select class="select1"  name="music">
                                        <option value="">Music</option>
                                        <option <?php
    if ($profile['music'] == "dj") {
        echo 'selected';
    }
    ?> value="dj">dj</option>
                                        <option <?php
                                    if ($profile['music'] == "live") {
                                        echo 'selected';
                                    }
    ?> value="live">live</option>
                                        <option <?php
                                    if ($profile['music'] == "jukebox") {
                                        echo 'selected';
                                    }
    ?> >jukebox</option>
                                        <option <?php
                                    if ($profile['music'] == "background_music") {
                                        echo 'selected';
                                    }
    ?> value="background_music">background music</option>
                                        <option <?php
                                    if ($profile['music'] == "karaoke") {
                                        echo 'selected';
                                    }
    ?> value="karaoke">karaoke</option>
                                        <option <?php
                                    if ($profile['music'] == "no_music") {
                                        echo 'selected';
                                    }
                                    ?> value="no_music">no music</option>
                                    </select>
                                    <div class="input-group-addon">Music</div>
                                </div>
                                <div class="input-group">
                                    <select class="select1"  name="wheelchair_accessible">
                                        <option value="no">Wheelchair Accessible</option>
                                        <option <?php
                                    if ($profile['wheelchair_accessible'] == "yes") {
                                        echo 'selected';
                                    }
    ?> value="yes">Yes</option>
                                        <option <?php
                                    if ($profile['wheelchair_accessible'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                    </select>
                                    <div class="input-group-addon">Wheelchair Accessible</div>
                                </div>


                                <div class="input-group">
                                    <select class="select1"  name="wifi">
                                        <option value="no">wifi</option>
                                        <option <?php
                                    if ($profile['wifi'] == "free") {
                                        echo 'selected';
                                    }
    ?> >Free</option>
                                        <option <?php
                                    if ($profile['wifi'] == "paid") {
                                        echo 'selected';
                                    }
    ?>>Paid</option>
                                        <option <?php
                                    if ($profile['wifi'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                    </select>
                                    <div class="input-group-addon">Wifi</div>
                                </div>
                                <div class="input-group">
                                    <select class="select1"  name="wifi">
                                        <option value="no">sports</option>
                                        <option <?php
                                    if ($profile['sports'] == "soccer") {
                                        echo 'selected';
                                    }
    ?> >soccer</option>
                                        <option <?php
                                    if ($profile['sports'] == "basketball") {
                                        echo 'selected';
                                    }
    ?>>basketball</option>
                                        <option <?php
                                    if ($profile['sports'] == "hockey") {
                                        echo 'selected';
                                    }
    ?> value="no">hockey</option>
                                        <option <?php
                                    if ($profile['sports'] == "football") {
                                        echo 'selected';
                                    }
    ?> value="no">football</option>
                                        <option <?php
                                    if ($profile['sports'] == "baseball") {
                                        echo 'selected';
                                    }
    ?> value="no">baseball</option>
                                        <option <?php
                                    if ($profile['sports'] == "mma") {
                                        echo 'selected';
                                    }
    ?> value="no">mma</option>
                                        <option <?php
                                    if ($profile['sports'] == "other") {
                                        echo 'selected';
                                    }
                                    ?> value="no">other</option>
                                    </select>
                                    <div class="input-group-addon">Sports</div>
                                </div>

                                <div class="input-group">
                                    <select class="select1"  name="corkage">
                                        <option value="no">Corkage</option>
                                        <option <?php
                                    if ($profile['corkage'] == "free") {
                                        echo 'selected';
                                    }
    ?> >Free</option>
                                        <option <?php
                                    if ($profile['corkage'] == "paid") {
                                        echo 'selected';
                                    }
    ?>>Paid</option>
                                        <option <?php
                                    if ($profile['corkage'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                    </select>
                                    <div class="input-group-addon">Corkage</div>
                                </div>
                                <div class="input-group">
                                    <select class="select1"  name="waiter_service">
                                        <option value="no">Waiter Service</option>
                                        <option <?php
                                    if ($profile['waiter_service'] == "yes") {
                                        echo 'selected';
                                    }
    ?> value="yes">Yes</option>
                                        <option <?php
                                    if ($profile['waiter_service'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                    </select>
                                    <div class="input-group-addon">Waiter Service</div>
                                </div>
                                <div class="input-group">
                                    <select class="select1"  name="television">
                                        <option value="">Television</option>
                                        <option <?php
                                    if ($profile['television'] == "yes") {
                                        echo 'selected';
                                    }
    ?> value="yes">Yes</option>
                                        <option <?php
                                    if ($profile['television'] == "no") {
                                        echo 'selected';
                                    }
                                    ?> value="no">No</option>
                                    </select>
                                    <div class="input-group-addon">Television</div>
                                </div>
                                <div class="input-group">
                                    <select class="select1"  name="outdoor_seating">
                                        <option value="no">Outdoor Seating</option>
                                        <option <?php
                                    if ($profile['outdoor_seating'] == "yes") {
                                        echo 'selected';
                                    }
    ?> value="yes">Yes</option>
                                        <option <?php
                                    if ($profile['outdoor_seating'] == "no") {
                                        echo 'selected';
                                    }
    ?> value="no">No</option>
                                    </select>
                                    <div class="input-group-addon">Outdoor Seating</div>
                                </div>
<?php } ?>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <input style="background: #6fbe44" type="submit" value="Submit">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <button onclick="cancel();">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Preferences Form -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ===== End Section Sign In ===== -->