<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>FULLSPOON</title>
        <link href='https://fonts.googleapis.com/css?family=Quicksand:400,300,700' rel='stylesheet' type='text/css'>

        <style>
            .mail_wp{ background-color:#72bf4d; padding:2% 10%; margin:0 auto;}
            .logo{ text-align:center; margin:45px 0;}
            .table{ background-color:#fff; border-radius:5px; padding:30px;}
            .table tr td{ padding:6px 10px;}
            .header_label{ text-align:center; font-family: 'Quicksand', sans-serif; font-size:36px; color:#6e6e6e; font-weight:300; padding:10px;}
            .icon_logo{ text-align:center; padding:15px; border-bottom:1px solid #e7e7e7;}
            .disp_text{ text-align:center; color:#6c6c6c; font-size:17px; font-family: "Arial"; font-weight:lighter; margin-bottom:40px;}
            .header_green{ background-color:#72bf4d; padding:15px !important; color:#fff; font-family: "Arial"; font-size:24px; font-weight:lighter; text-align:center;}
            .text_bold{ color:#636363; font-family: "Arial"; font-size:18px; font-weight:600;}
            .text_small{ color:#636363; font-family: "Arial"; font-size:17px; font-weight:lighter;}
            .text_right{ text-align:right;}
            .bg_gray{ background:#f0f0f0;}
            .bg_gray td{ padding:4px 10px !important;}
            .text_center{ text-align:center;}
            .wp_table{ background-color:#fff;}
            .footer{ text-align:center;}
            .footer table{ margin:0 auto;}
            .footer h5{ font-size:17px; color:#fff; text-align:center; font-family: "Arial"; font-weight:lighter;}
            .footer a{ font-size:17px; color:#fff; text-align:center; font-family: "Arial"; font-weight:lighter; border-bottom:1px solid #718368; padding:10px 25px; text-decoration:none; 
                       margin-bottom:20px; display:inline-block;}

        </style>


    </head>

    <body style="background-color:#f4f7f3; padding:7%;">

        <div class="mail_wp" style="background-color:#72bf4d; padding:2% 10%; margin:0 auto;">

            <h2 class="logo" style="text-align:center; margin:45px 0;"><a href="<?= base_url() ?>"><img src="<?= base_url() ?>images/email/logo.png"></a></h2>

            <div class="wp_table" style="background-color:#fff; width=100%;">

                <table width="100%" border="0" cellspacing="0" class="table" style="background-color:#fff; border-radius:5px; padding:5%;">
                    <tbody>
                        <tr>
                            <td colspan="3" class="header_label" style="padding:6px 10px; text-align:center; font-family: 'Quicksand', sans-serif; font-size:36px; color:#6e6e6e; font-weight:300; padding:10px;">
                                Subscriber Registration</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table width="100%">
                                    <tr>
                                        <td width="20%" style="padding:6px 10px;">&nbsp;</td>
                                        <td width="60%" class="icon_logo" style="padding:6px 10px; text-align:center; padding:15px; border-bottom:1px solid #e7e7e7;"><img src="<?= base_url() ?>images/email/order_icon.png"></td>
                                        <td width="20%" style="padding:6px 10px;">&nbsp;</td>
                                    </tr>
                                </table> 
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding:6px 10px;">
                                <p  style=" text-align:center; color:#6c6c6c; font-size:17px; font-family: 'Arial'; font-weight:lighter; margin-bottom:40px;">
                                    Hi <?= $name ?>
                                    <br/>
                                    Welcome! Thank you for registering with Fulspoon. You can start ordering from your 

                                    favorite restaurants now. And save on every order. 
                                    You can access your Dashboard from here: <a href="<?= base_url() . 'member' ?>"><?= base_url() . 'member' ?></a> .  Please use the login credentials you just created. 
                                </p></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="color:#636363; text-align:center; font-family: 'Arial'; font-size:17px; font-weight:lighter; padding:6px 10px;">
                                For any questions, please reach out to us at support@fulspoon.com <br>
                                Thanks, 
                                <br/>
                                Fulspoon Team</td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <div class="footer"  style="text-align:center;">
                <h5 style="font-size:17px; color:#fff; text-align:center; font-family: 'Arial'; font-weight:lighter;">Download Our APP From</h5>
                <table width="200" border="0" style="margin:0 auto;">
                    <tbody>
                        <tr>
                            <td  style="padding:6px 10px;"><a href="<?= $appstore_url ?>"><img src="<?= base_url() ?>images/email/play-store.png"></a></td>
                            <td  style="padding:6px 10px;"><a href="<?= $playstore_url ?>"><img src="<?= base_url() ?>images/email/google.png"></a></td>
                        </tr>
                    </tbody>
                </table>

                <h5 style="font-size:17px; color:#fff; text-align:center; font-family: 'Arial'; font-weight:lighter;">© 2016 Fulspoon LLC, All rights reserved.<br> 
                    175 Varick St,
                    New York | US,  NY 10014</h5>

              <!--  <a href="#">unsubscribe</a>-->
            </div>





        </div>
    </body>
</html>
