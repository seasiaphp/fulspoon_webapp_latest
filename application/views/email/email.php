<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Fulspoon</title>

        <link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>


        <style>

            .pageheader {
                color: #434242;
                font-size: 30px;
                font-weight: 300;
                padding: 20px 0;
                text-transform: uppercase;
            }
        </style>
    </head>

    <body style="background-color:#f4f4f4;">
        <div style="width:1000px; margin:0 auto; background-color:#fff; padding:10px;">

            <h4 style="text-align:center; padding:25px;"><a href="#"><img src="<?= base_url() ?>images/replimatic_logo.png" width="250"></a></h4>

            <table width="100%" style="border: 1px solid #dbdbdb; border-collapse: inherit !important; border-radius: 8px 8px 0 0; font-family: 'Lato', sans-serif; margin-bottom:15px; background-color:#fff;">
                <thead>    
                    <tr>
                        <th colspan="2" style=" padding:15px; border-bottom: 1px solid #ddd; color: #434242; font-size: 30px; font-weight: 300; padding: 20px 0; text-transform: uppercase; 
                            font-family: 'Lato', sans-serif; text-align:left; padding-left:10px;" >
                            ORDER DETAILS  <?= $referance_number; ?></th>
                    </tr>
                    <tr>
                        <th style="padding: 15px 18px; text-align:left; color:#666; font-size:16px; line-height:22px;">
                <h3 style="font-size: 18px; line-height: 24px; color: #444;  font-weight: 700; margin: 0;"><?= $name ?></h3>
                Type : <?= ucfirst($delivery_method) ?>			<br>	
                <?php
                if (isset($specialInst)) {
                    ?>
                    Special Notes<br>

                    <?php
                    echo $specialInst;
                }
                ?>
                </th>																					
                <th style="padding: 15px 18px; text-align:left; color:#666; font-size:16px;  line-height:22px;">
                    <?php if ($delivery_method == 'delivery') { ?>
                    <h3  style="font-size: 18px; line-height: 24px; color: #444;  font-weight: 700; margin: 0;">Delivery Address</h3>
                    <?= $delivery_address['address'] ?> ,  <br>
                    <?= $delivery_address['city'] ?><br>
                    <?= $delivery_address['state'] ?>,<?= $delivery_address['pincode'] ?>
                    <br/>
                    Phone: <?= $delivery_address['phone'] ?>
                <?php } ?>
                </th>            
                </tr>
                </thead>
                <tbody>

                <td style="padding: 12px 18px; border-top: 1px solid #ddd; color:#666;">ORDERED TIME <span>:
                        <?= date('h:i A'); ?>
                    </span></td>															
                <td style="padding: 12px 18px; border-top: 1px solid #ddd; color:#666;">Pick by : 
                    <?php
                    $times1 = strtotime($estimated_time);
                    $es = $times1 % 300;
                    $diff = 300 - $es;
                    $time = $times1 + $diff;
                    echo date('h:i A', $time);
                    ?><span></span></td>            

                </tbody>
            </table>
            <!--end of .table-responsive-->
            <div class="col-xs-12">      
                <table  width="100%" style="font-family: 'Lato', sans-serif; margin-top:15px; background-color:#fff;" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="7%" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; text-align:left;">#</th>
                            <th width="34%" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; text-align:left;">Items</th>
                            <th width="19%" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; text-align:left;">QTY</th>
                            <th width="23%" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; text-align:left;">RATE</th>
                            <th width="17%" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; text-align:left; text-align:right;">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($cart_data as $cart) {
                            $i++;
                            $row_span_val = (sizeof($cart['options'])) + 2 + (sizeof($cart['options']['sides']));
                            ?>
                            <tr>
                                <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;"><?= $i ?></td>
                                <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;"><?= $cart['item_name'] ?></td>
                                <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;"><?= $cart['quantity'] ?></td>
                                <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;"><?= $cart['base_price'] ?></td>
                                <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; text-align:right;">$ <?= $cart['quantity'] * $cart['base_price'] ?></td>
                            </tr>

                            <?php if (sizeof($cart['options']) > 0) { ?>
                                <tr>
                                    <td rowspan="<?= $row_span_val ?>" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">&nbsp;</td>
                                    <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">Options &amp; Sides</td>
                                </tr>

                                <?php
                            }
                            foreach ($cart['options'] as $option) {
                                ?>
                                <tr> 	
                                     <!--<td rowspan="2" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">&nbsp;</td>-->
                                    <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">
                                        <?= $option['option_name'] ?>
                                    </td>

                                </tr>
                                <?php foreach ($option['sides'] as $sides) { ?>
                                    <tr>

                                        <td  style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">
                                            <?= $sides['side_name']; ?>
                                        </td>
                                        <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">

                                            <?= $cart['quantity']; ?>
                                        </td>
                                        <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px;">
                                            <?=
                                            $sides['side_price'];
//                                            $sides['side_price'];
                                            ?>
                                        </td>
                                        <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px;text-align:right; color:#666; font-size:16px;">
                                            <?= "$" . $cart['quantity'] * $sides['side_price']; ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        <?php } ?>

                        <tr>
                            <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:8px 15px; color:#666; font-size:16px; text-align:right; font-weight:bold;">Total</td>
                            <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; font-weight:bold; text-align:right;">$<?= $subtotal; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:8px 15px; color:#666; font-size:16px; text-align:right; font-weight:bold;">Fulspoon  Discount</td>
                            <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; font-weight:bold; text-align:right;">- $<?= $discount; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:8px 15px; color:#666; font-size:16px; text-align:right; font-weight:bold;">Tax</td>
                            <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; font-weight:bold; text-align:right;">+ $<?= $tax; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:8px 15px; color:#666; font-size:16px; text-align:right; font-weight:bold;">Tip</td>
                            <td style="border: 1px solid #ddd; line-height:1.42857; padding:8px; color:#666; font-size:16px; font-weight:bold; text-align:right;">+ $<?= $tip; ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border: 1px solid #ddd; line-height:1.42857; padding:12px 15px; color:#6FBE44; font-size:18px; text-align:right; font-weight:bold;">Total Amount</td>
                            <td style="border: 1px solid #ddd; line-height:1.42857; padding:12px; color:#6FBE44; font-size:18px; font-weight:bold; text-align:right;">$ <?= $totalDue ?></td>
                        </tr>


                        <tr>
                            <td colspan="2">
                            </td>
                            <?php
                            $key1 = rand(100000, 111111111111111) . uniqid();
                            $en1 = rawurlencode(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key1), $ord_item_id, MCRYPT_MODE_CBC, md5(md5($key1)))));

//                            $order_item_id = base64_encode($ord_item_id);
                            ?>
                            <td style="text-align:right;" colspan="2">
                                
                                
                                 <a href="<?= base_url() ?>owners/accept_owner_order/<?= $ord_item_id ?>/<?= $key1 ?>">
                                    <button style="background-color:#72BF4D; cursor:pointer; padding:10px 30px; color:#fff; margin-top:10px; font-size:16px; border:none; border-radius:5px;">ACCEPT</button>
                                </a>
                            </td>
                            <td style="text-align:right;">
                                <a href="<?= base_url() ?>owners/decline_owner_order/<?= $ord_item_id ?>/<?= $key1 ?>">
                                    <button style="background-color:#ff5c58; cursor:pointer; padding:10px 30px; color:#fff; margin-top:10px; font-size:16px; border:none; border-radius:5px; margin-left:15px;">DECLINE</button>
                                </a>
                            </td>

                        </tr>

                    </tbody>
                </table>
                <!--end of .table-responsive-->

            </div>

        </div>
    </body>
</html>
