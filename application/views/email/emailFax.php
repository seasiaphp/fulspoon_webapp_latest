<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>FULLSPOON</title>
        <link href='https://fonts.googleapis.com/css?family=Quicksand:400,300,700' rel='stylesheet' type='text/css'>


    </head>

    <body style="background-color:#f4f7f3; padding:2%;">

        <div class="mail_wp" style="background-color:#72bf4d; padding:1% 5%; margin:0 auto;">

            <h2 class="logo" style="text-align:center; margin:45px 0;"><a href="#"><img src="<?= base_url() ?>images/email/logo.png"></a></h2>

            <div class="wp_table" style="background-color:#fff; width:100%;">

                <table width="100%" border="0" cellspacing="0" class="table" style="background-color:#fff; border-radius:5px; padding:5%;">
                    <tbody>
                        <tr>
                            <td colspan="3" class="header_label" style="padding:6px 10px; text-align:center; font-family: 'Quicksand', sans-serif; font-size:36px; color:#6e6e6e; font-weight:300; padding:10px;">Order Receipt</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding:6px 10px;">
                                <table width="100%">
                                    <tr>
                                        <td width="20%" style="padding:6px 10px;">&nbsp;</td>
                                        <td width="60%" class="icon_logo" style="padding:6px 10px; text-align:center; padding:15px; border-bottom:1px solid #e7e7e7;"><img src="<?= base_url() ?>images/email/order_icon.png"></td>
                                        <td width="20%" style="padding:6px 10px;">&nbsp;</td>
                                    </tr>
                                </table> 
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding:6px 10px;"><p class="disp_text" style="  color:#6c6c6c; font-size:13px; font-family: 'Arial'; font-weight:lighter; margin-bottom:40px;">Hi <?= $rest_name ?>,<br/>You have a new order. <br/><br/><b> Ordered time : <?= $currentTime ?>
                                        <br/>
                                        Order expecting time : <?php
                                        $times1 = strtotime($estimated_time);
                                        $es = $times1 % 300;
                                        $diff = 300 - $es;
                                        $time = $times1 + $diff;
//                                        echo date('h:i A', $time);
                                          echo $estimated_time2;
                                        ?>
                                    </b></p></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="header_green" style="padding:6px 10px;background-color:#72bf4d; padding:15px !important; color:#fff; font-family: 'Arial'; font-size:20px; font-weight:lighter; text-align:center;">Details For Order : <?= $referance_number; ?></td>
                        </tr>
                        <tr>
                            <td width="6%" style="padding:6px 10px;"></td>
                            <td width="70%" style="padding:6px 10px;"></td>
                            <td width="24%" style="padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="6%" style="padding:6px 10px;"></td>
                            <td width="70%" style="padding:6px 10px;"></td>
                            <td width="24%" style="padding:6px 10px;"></td>
                        </tr>
                </table>

                <table width="100%" border="0"  cellspacing="0" class="table" style="padding:30px; color:black;">
                    <tr>
                        <td width="6%" class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;"></td>
                        <td width="50%" class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;">Name</td>
                        <td width="10%" class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;">Qty</td>
                        <td width="12%" class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600; text-align: right;">Price</td>
                        <td width="22%" class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;  text-align: right;">Total Price</td>
                    </tr>
                    <?php
                    $i = 0;
                    foreach ($cart_data as $cart) {
                        $i++;
                        ?>
                        <tr>
                            <td  class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;"><?= $i ?></td>
                            <td  class="text_bold" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;"><?= $cart['item_name'] ?></td>
                            <td  style="padding:2px 3.5px;" colspan="3"><?php echo $cart['quantity'] ?></td>
                        </tr>

                        <tr>
                            <td style="padding:6px 10px;"></td>
                            <td class="text_small" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;"><?= $cart['size']['size'] ?> </td>
                            <td class="text_small" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;"></td>
                            <td class="text_small" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; text-align:right;"> <?= "$" . $cart['size']['price'] ?> </td>
                            <td class="text_small" style="padding:2px 3.5px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; text-align:right;"> <?= "$" . number_format((float) ( $cart['size']['price'] * $cart['quantity'] ), 2, '.', ''); ?> </td>
                        </tr>
                        <?php foreach ($cart['options'] as $option) { ?>
                            <tr>
                                <td style="padding:6px 10px;"></td>
                                <td class="text_small" style="padding:6px 10px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; "> <?= $option['option_name'] ?> </td>
                                <td style="padding:6px 10px;"></td>
                                <td style="padding:6px 10px;"></td>
                                <td style="padding:6px 10px;"></td>
                            </tr>
                            <?php foreach ($option['sides'] as $sides) { ?>
                                <tr>
                                    <td style="padding:6px 10px;"></td>
                                    <td class="text_small" style="padding:6px 10px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;"> <?= $sides['side_name']; ?></td>
                                    <td class="text_small text_right" style="padding:6px 10px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; text-align:left;">  </td>
                                    <td class="text_small text_right" style="padding:6px 10px; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; text-align:right;">  <?=
                                        ""
                                        ?></td>
                                    <td class="text_small text_right" style="color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; text-align:right;">  <?=
                                        "+ $" . number_format((float) ($sides['side_price'] * $cart['quantity']), 2, '.', '');
                                        ?></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?> 

                    <tr>
                        <td style="padding:6px 10px;"></td>
                        <td style="padding:6px 10px;"></td>
                        <td style="padding:6px 10px;" colspan="3"></td>
                    </tr>
                    <tr class="bg_gray" style="background:#f0f0f0;">
                        <td class="text_small text_right" colspan="3" style="text-align:right; padding-top:25px !important; padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">Sub - Total :</td>
                        <td class="text_small text_right" colspan="2" style="text-align:right; padding-top:25px !important; padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">$ <?= $subtotal; ?></td>
                    </tr>
                    <tr class="bg_gray" style="background:#f0f0f0; ">
                        <td class="text_small text_right" colspan="3" style=" text-align:right; padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">Fulspoon Discount :</td>
                        <td class="text_small text_right" colspan="2" style="padding:4px 10px !important; text-align:right; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">$ <?= $discount; ?></td>
                    </tr>
                    <!--number_format((float)$foo, 2, '.', '')-->
                    <?php if ($delivery_method == 'delivery') { ?>
                        <tr class="bg_gray" style="background:#f0f0f0; ">
                            <td class="text_small text_right" colspan="3" style=" text-align:right; padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">Delivery Fee :</td>
                            <td class="text_small text_right" colspan="2" style="padding:4px 10px !important; text-align:right; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">$ <?= number_format((float) $delivery_fee, 2, '.', ''); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="bg_gray" style="background:#f0f0f0; ">
                        <td class="text_small text_right" colspan="3" style=" text-align:right; padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">Taxes :</td>
                        <td class="text_small text_right" colspan="2" style="padding:4px 10px !important; text-align:right; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">$ <?= $tax; ?></td>
                    </tr>
                    <tr class="bg_gray" style="background:#f0f0f0;">
                        <td class="text_small text_right" colspan="3" style="padding:4px 10px !important; text-align:right; color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter;">Tips :</td>
                        <td class="text_small text_right" colspan="2"  style="padding:4px 10px !important; color:#636363; text-align:right; font-family: 'Arial'; font-size:13px; font-weight:lighter;">$ <?= number_format((float) $tip, 2, '.', ''); ?></td>
                    </tr>
                    <tr class="bg_gray" style="background:#f0f0f0;">
                        <td class="text_bold text_right" colspan="3" style="padding-bottom:25px !important; text-align:right;padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;">Total :</td>
                        <td class="text_bold text_right" colspan="2" style="padding-bottom:25px !important; text-align:right; padding:4px 10px !important; color:#636363; font-family: 'Arial'; font-size:14px; font-weight:600;">$ <?= $totalDue ?></td>
                    </tr>

                    </tbody>
                </table>

                <table width="100%" border="0" cellspacing="0" class="table" style="padding:30px;">
                    <tbody>
                        <tr>
                            <td colspan="3" class="header_green" style="padding:6px 10px; background-color:#72bf4d; padding:15px !important; color:#fff; font-family: 'Arial'; font-size:24px; font-weight:lighter; text-align:center;">CONTACT DETAILS</td>
                        </tr>
                        <tr>
                            <td width="48%" style="padding:6px 10px;"></td>
                            <td width="29%" style="padding:6px 10px;"></td>
                            <td width="23%" style="padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="48%" style="padding:6px 10px;"></td>
                            <td width="29%" style="padding:6px 10px;"></td>
                            <td width="23%" style="padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="48%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;">Name</td>
                            <td width="29%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px; padding:6px 10px;"><?= ucfirst($name) ?></td>
                            <td width="23%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="48%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;">Type</td>
                            <td width="29%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px; padding:6px 10px;"><?= ucfirst($delivery_method) ?></td>
                            <td width="23%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="48%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;">Phone No</td>
                            <td width="29%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px; padding:6px 10px;"><?= $phone ?></td>
                            <td width="23%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;"></td>
                        </tr>
                        <?php if ($delivery_method == 'delivery') { ?>
                            <tr>
                                <td class="text_small" style="color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; padding:6px 10px;">Delivery Address</td>
                                <td class="text_small" colspan="2" style="color:#636363; font-family: 'Arial'; font-size:13px; font-weight:lighter; padding:6px 10px;">  <?= $delivery_address['address'] . "<br/>" . $delivery_address['city'] . "," . $delivery_address['state'] . "<br/>" . $delivery_address['pincode'] . "<br/>" . $delivery_address['phone']; ?></td>

                            </tr>
                        <?php } ?>
                        <?php if (isset($specialInst)) { ?>
                            <tr>
                                <td width="48%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;">Special Notes</td>
                                <td width="29%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px; padding:6px 10px;"><?= ucfirst($specialInst) ?></td>
                                <td width="23%" class="text_bold" style="color:#636363; font-family: 'Arial'; font-size:14px;  padding:6px 10px;"></td>
                            </tr>
                            <?php
                        }$key1 = rand(100000, 111111111111111) . uniqid();
                        $en1 = rawurlencode(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key1), $ord_item_id, MCRYPT_MODE_CBC, md5(md5($key1)))));
                        ?>

                        <tr>
                            <td width="48%" style="padding:6px 10px;"></td>
                            <td width="29%" style="padding:6px 10px;"></td>
                            <td width="23%" style="padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="48%" style="padding:6px 10px;"></td>
                            <td width="29%" style="padding:6px 10px;"></td>
                            <td width="23%" style="padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td width="48%" style="padding:6px 10px;"></td>
                            <td width="29%" style="padding:6px 10px;"></td>
                            <td width="23%" style="padding:6px 10px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text_small text_center" style="color:#636363; text-align:center; font-family: 'Arial'; font-size:11px; font-weight:lighter; padding:6px 10px;">&nbsp;</td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <div class="footer" style="text-align:center;">
                <p>&nbsp;</p>
                <h5 style="font-size:13px; color:#fff; text-align:center; font-family: 'Arial'; font-weight:lighter;">© 2016 Fulspoon LLC, All rights reserved.<br> 
                    175 Varick St,New York, NY 10014</h5>

                <!-- <a href="#" style="font-size:13px; color:#fff; text-align:center; font-family: 'Arial'; font-weight:lighter; border-bottom:1px solid #718368; padding:10px 25px; text-decoration:none; 
                    margin-bottom:20px; display:inline-block;">unsubscribe</a>-->
            </div>





        </div>
    </body>
</html>
