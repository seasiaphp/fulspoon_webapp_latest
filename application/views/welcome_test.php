<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="<?php echo base_url('assets/js'); ?>/jquery.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<script>

function test(){

Stripe.setPublishableKey("pk_test_FvgVInxV16FbpMbZ3VxtcJt1");
//var phone 	= $scope.pay_phone ;
		var cardno	= $("#cardno").val() ;
		var month 	= $("#card_month").val() ;
		var year 	= $("#card_year").val() ;
		var cvv 	= $("#card_cvv").val();
		
		
		var errorPaymentInfo='';						
		
		  if(!Stripe.card.validateCardNumber(cardno)){;			
		   		errorPaymentInfo="Invalid card number";
		  }else if(!Stripe.card.validateExpiry(month, year)){
		   		errorPaymentInfo="Invalid expiry date";
		  }else if(!Stripe.card.validateCVC(cvv)){
		   		errorPaymentInfo="Invalid CVV code";
	   	  }else{
			 
					Stripe.card.createToken({
					  number: cardno,
					  cvc: cvv,
					  exp_month: month,
					  exp_year: year
				}, stripeResponseHandler);	
		 }
}
	var stripeResponseHandler = function(status, response){
		var errorPaymentInfo='';
		console.log(response);
		
		if (response.error) {
			errorPaymentInf=response.error.message;
			
			return false;
		}else{
			var token = response.id;
			var last4 = response.card.last4;
			var brand = response.card.brand;
			var data = {'token':token,'last4':last4,'brand':brand};
			var user_id=1;
			$.ajax({ 
							type: "POST",	
							url: "<?php echo base_url()."client/subscription_pay";?>",
							data: {'token':token,'last4':last4,'brand':brand,'user_id':user_id},
						});
		}
	 }

</script>
</head>

<body>

<div class="billing-form ">
<form>

    <div class="form-group ">

      <input type="text" id="cardname" class="ember-view ember-text-field form-control" placeholder="Cardholder Name" name="cardholder name" >
      <div id="" class="ember-view alert alert--error is-hidden">false
</div>
    </div>
    <div class="form-group ">
      <div class="col col--12">
        <div class="input-wrapper input-wrapper--icon">
          <input type="tel"  class="ember-view ember-text-field form-control form-control--credit-card cc-num" placeholder="Card Number" name="cardno" autocomplete="cc-number" data-ng-change="formatDetails()" id="cardno">
         
        </div>
      </div>
      
      <div class="clearfix"></div><br />
      <div class="col col--4 col--4--sm">
        <div class="">
          <select class="ember-view ember-select form-control" name="card_month" id="card_month">  
          <option value="">Month</option>
          <option id="" class="ember-view" value="1">1</option>
          <option id="" class="ember-view" value="2">2</option>
          <option id="" class="ember-view" value="3">3</option>
          <option id="" class="ember-view" value="4">4</option>
          <option id="" class="ember-view" value="5">5</option>
          <option id="" class="ember-view" value="6">6</option>
          <option id="" class="ember-view" value="7">7</option>
          <option id="" class="ember-view" value="8">8</option>
          <option id="" class="ember-view" value="9">9</option>
          <option id="" class="ember-view" value="10">10</option>
          <option id="" class="ember-view" value="11">11</option>
          <option id="" class="ember-view" value="12">12</option>
</select>
        </div>
      </div>
      <div class="col col--4 col--4--sm">
        <div class="">
          <select class="ember-view ember-select form-control" name="card_year" id="card_year" >
          <option value="">Year</option>
          <option id="" class="ember-view" value="2015">2015</option>
          <option id="" class="ember-view" value="2016">2016</option>
          <option id="" class="ember-view" value="2017">2017</option>
          <option id="" class="ember-view" value="2018">2018</option>
          <option id="" class="ember-view" value="2019">2019</option>
          <option id="" class="ember-view" value="2020">2020</option>
          <option id="" class="ember-view" value="2021">2021</option>
          <option id="" class="ember-view" value="2022">2022</option>
          <option id="" class="ember-view" value="2023">2023</option>
          <option id="" class="ember-view" value="2024">2024</option>
          <option id="" class="ember-view" value="2025">2025</option>
          <option id="" class="ember-view" value="2026">2026</option>
          <option id="" class="ember-view" value="2027">2027</option>
          <option id="" class="ember-view" value="2028">2028</option>
          <option id="" class="ember-view" value="2029">2029</option>
          <option id="" class="ember-view" value="2030">2030</option>
          <option id="" class="ember-view" value="2031">2031</option>
          <option id="" class="ember-view" value="2032">2032</option>
          <option id="" class="ember-view" value="2033">2033</option>
          <option id="" class="ember-view" value="2034">2034</option>
          <option id="" class="ember-view" value="2035">2035</option>
</select>
        </div>
      </div>
      <div class="col col--4 col--4--sm">
        <input type="tel" class="ember-view ember-text-field form-control form-control--cvv cc-cvv"  data-ng-change="formatDetails()"  placeholder="CVV" autocomplete="off" name="card_cvv" id="card_cvv"  style="height:44px;" />
      </div>
      <div class="col col--4 col--4--sm">
        <input type="button" onclick="test()" value="test script">
      </div>
    </div>
	<div id="ember798" class="ember-view alert alert--error" ></div>
    </form>
  </div>


</body>
</html>
