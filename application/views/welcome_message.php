<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>
        <?php
//Detect special conditions devices
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");
        if ($iPod || $iPhone || $iPad) {
            ?>

            <meta name="apple-itunes-app" content="app-id=<?= $get_package_details[0]['appstore-id']; ?>">
        <?php } else { ?>

            <meta name="google-play-app" content="app-id=<?= $get_package_details[0]['app-id']; ?>">

        <?php } ?>

        <!-- Basic -->
        <title>Fulspoon | Subscribe to your favorite restaurants, save 20% on every order</title>
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Define Charset -->
        <meta charset="utf-8">
        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Responsive Metatag -->
        <meta name="description" content="Subscribe to your favorite restaurants Save 20% on every order Order regularly from the same places? What if you got a big discount on every order from your favorite restaurant? Fulspoon helps convert your loyalty into big savings. We are getting ready to launch and would love to notify you when ready">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">
        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">
        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/slicknav.css" media="screen">
        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/style.css" media="screen">
        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/responsive.css" media="screen">
        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/animate.css" media="screen">
        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/colors/red.css" title="red" media="screen" />
        <!-- Margo JS  -->
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/owl.carousel.js"></script> 
        <script type="text/javascript" src="<?php echo base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/mediaelement-and-player.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.slicknav.js"></script>


        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>css/style-calculator.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/animate-popup.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/calculator_popup.css">
        <style>
            button[disabled=disabled], button:disabled {
                background:#7ad648;
                border:none;
                border-radius: 0px !important;
                min-height: 42px;
                min-width: 170px;
                color:#fff;
                text-transform:uppercase;
                // your css rules
            }

        </style>


        <script src="//load.sumome.com/" data-sumo-site-id="258a608ec7daa5f14489723671b9b1a7f1d44dec9781b5e8874010519dd503bc" async="async"></script>
        <script type="text/javascript">
            var $zoho = $zoho || {salesiq: {values: {}, ready: function () {
                    }}};
            var d = document;
            s = d.createElement("script");
            s.type = "text/javascript";
            s.defer = true;
            s.src = "https://salesiq.zoho.com/fulspoon/float.ls?embedname=fulspoon";
            t = d.getElementsByTagName("script")[0];
            t.parentNode.insertBefore(s, t);
            $zoho.salesiq.ready = function (embedinfo) {
                $zoho.salesiq.floatbutton.visible("hide");
            }
        </script>
        <script type="text/javascript">
            var $zoho = $zoho || {salesiq: {values: {}, ready: function () {
                    }}};
            var d = document;
            s = d.createElement("script");
            s.type = "text/javascript";
            s.defer = true;
            s.src = "https://salesiq.zoho.com/fulspoon/float.ls?embedname=fulspoon";
            t = d.getElementsByTagName("script")[0];
            t.parentNode.insertBefore(s, t);
        </script>
          <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
          <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>


        <!-- Full Body Container -->
        <div id="container" class="">

            <section id="home" style="padding-top:0;">

                <div class="container" style="position:relative;">
                    <div class="logo">
                        <a href="<?= base_url() ?>">
                            <?php echo '

<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="170px" height="46px" viewBox="0 0 792 214" style="enable-background:new 0 0 792 214;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#73BF4C;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{opacity:0.52;}
	.st3{clip-path:url(#SVGID_6_);fill:#6EBD44;}
	.st4{fill:url(#SVGID_7_);}
</style>
<g>
	<g>
		<g>
			<g>
				<path class="st0" d="M253.3,163.1v-61.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
					c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H253.3z"/>
				<path class="st0" d="M262.9,166.1h-12.6v-61.5h-13.9V92.6h13.9v-3.1c0-16.9,8.3-26.3,23.4-26.3c5.5,0,10.6,1.8,15.3,5.3l2.3,1.7
					l-6.5,9.7l-2.4-1.5c-3.6-2.1-5.6-3-8.9-3c-4.9,0-10.5,1.6-10.5,14.1v3.1h22.4v11.9h-22.4V166.1z M256.3,160.1h0.6V89.6
					c0-17.5,10.3-20.1,16.5-20.1c3.9,0,6.7,1,9.5,2.5l0.1-0.1c-2.9-1.7-6.1-2.5-9.3-2.5c-11.7,0-17.4,6.6-17.4,20.3V160.1z"/>
			</g>
			<g>
				<path class="st0" d="M353.1,163.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V94.9h6.7V134
					c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V94.9h6.7v68.2H353.1z"/>
				<path class="st0" d="M328.9,167.2L328.9,167.2c-20.5,0-33.7-13-33.7-33V91.9h12.7V134c0,13.3,7.4,20.5,20.9,20.5l0.3,0
					c12.3-0.1,20.8-9.1,20.8-21.8V91.9h12.7v74.2h-12.6v-6.7C344.7,164.3,337.3,167.1,328.9,167.2L328.9,167.2z M301.2,97.9v36.2
					c0,16.7,10.6,27,27.7,27c4.9-0.1,13.2-1.3,19-8c-4.7,4.6-11.2,7.3-18.6,7.4l-0.4,0c-16.6,0-26.9-10.2-26.9-26.5V97.9H301.2z
					 M356.1,160.1h0.6V97.9H356v34.9c0,3.3-0.5,6.4-1.4,9.2l1.5-2.7V160.1z"/>
			</g>
			<g>
				<rect x="377.5" y="66.7" class="st0" width="6.7" height="96.4"/>
				<path class="st0" d="M387.2,166.1h-12.7V63.7h12.7V166.1z M380.5,160.1h0.7V69.7h-0.7V160.1z"/>
			</g>
			<g>
				<path class="st0" d="M450.8,107.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
					c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
					c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
					c9.9,0,18.6,2.8,25.3,9.1L450.8,107.5z"/>
				<path class="st0" d="M429.8,167.2c-14.1,0-26.1-5.6-32.3-15l-1.4-2.2l9.4-8.4l1.9,2.8c4.5,6.4,12.9,10.3,22.5,10.3
					c5.4,0,17.9-1,18.2-10.1c0.2-6.7-7.7-8.5-19.6-10.4c-12.8-2.1-28.8-4.8-29-21.2c-0.1-5.4,1.8-10.1,5.5-13.8
					c5.2-5.2,13.9-8.2,24-8.2l0.5,0c11.1,0,20.4,3.3,27.4,9.9l2.2,2.1l-8.2,8.7l-2.2-1.9c-6.1-5.3-11.9-6.4-19.2-6.4l-0.5,0
					c-6.7,0-12.3,1.7-15.1,4.6c-1.3,1.4-2,3-1.9,4.9c0.2,5.9,8,7.7,17.3,9.4l2.4,0.4c11.9,2,29.8,5,29,22.8
					C460.3,161.4,444.5,167.2,429.8,167.2z M404.1,151c5.4,6.4,14.9,10.2,25.7,10.2c5.8,0,24.5-1.2,25-16.1
					c0.5-11.6-10.1-14.3-24-16.7l-2.4-0.4c-8.7-1.5-22-3.9-22.3-15.1c-0.1-3.5,1.1-6.7,3.6-9.2c4-4.1,11-6.4,19.4-6.4l0.5,0
					c6.6,0,13.7,0.8,20.9,6.1l0.1-0.1c-5.5-4.2-12.6-6.3-21-6.3l-0.5,0c-8.5,0-15.7,2.3-19.8,6.4c-2.5,2.5-3.8,5.8-3.7,9.5
					c0.1,10.6,9.5,13,23.9,15.4c11.1,1.8,25,4,24.7,16.5c-0.4,9.9-9.4,15.9-24.2,15.9c-10.2,0-19.4-3.8-25.3-10.2L404.1,151z"/>
			</g>
			<g>
				<path class="st0" d="M472.8,191.4V95.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35
					c0,22.9-15.4,35.3-34.6,35.3c-11.6,0-22.3-5.6-28.1-16.9v44.1H472.8z M535.2,129.1c0-19.1-12.4-28.5-27.8-28.5
					c-15.8,0-27.5,12-27.5,28.6c0,16.7,12,28.6,27.5,28.6C522.8,157.8,535.2,148.2,535.2,129.1"/>
				<path class="st0" d="M482.3,194.4h-12.5V92.1h12.6v9.8c6.6-6.9,16-10.8,26.5-10.8l0.1,0c21.5,0.8,35.9,16.1,35.9,38
					c0,22.9-15.1,38.3-37.6,38.3c-9.9,0-18.7-3.8-25.1-10.5V194.4z M475.8,188.4h0.5v-53.5l1.7,3.4c-0.8-2.9-1.2-5.9-1.2-9.1
					c0-2.9,0.3-5.8,1-8.4l-1.4,2.6V98.1h-0.6V188.4z M487.8,153.9c5.1,4.8,11.9,7.4,19.6,7.4c19.2,0,31.6-12.7,31.6-32.3
					c0-18.7-11.8-31.3-30.1-32c-4,0-7.8,0.7-11.2,1.9c3-1,6.3-1.5,9.8-1.5c18.7,0,30.8,12.4,30.8,31.5c0,19.3-12.1,31.8-30.8,31.8
					C499.8,160.8,493,158.3,487.8,153.9z M507.3,103.6c-14.2,0-24.5,10.8-24.5,25.6c0,14.9,10.3,25.6,24.5,25.6
					c12,0,24.8-6.8,24.8-25.8C532.2,113.1,522.9,103.6,507.3,103.6z"/>
			</g>
			<g>
				<path class="st0" d="M555.3,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
					C570.7,164,555.3,150.8,555.3,129.2 M617.8,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
					c0,17.6,12.5,28.2,27.9,28.2C605.2,157.4,617.8,146.8,617.8,129.2"/>
				<path class="st0" d="M589.8,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
					C627.4,151.5,611.9,167,589.8,167z M589.8,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8
					c18.6,0,31.6-13.1,31.6-31.8C621.4,110.1,608.4,96.8,589.8,96.8z M589.8,160.4c-18.2,0-30.9-12.8-30.9-31.2
					c0-18.6,13-32.1,30.9-32.1c17.9,0,31,13.5,31,32.1C620.8,147.6,608,160.4,589.8,160.4z M589.8,103.2c-14.5,0-24.9,11-24.9,26.1
					c0,14.9,10.3,25.2,24.9,25.2c14.7,0,25-10.4,25-25.2C614.8,114.1,604.3,103.2,589.8,103.2z"/>
			</g>
			<g>
				<path class="st0" d="M636.8,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
					C652.2,164,636.8,150.8,636.8,129.2 M699.3,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
					C686.7,157.4,699.3,146.8,699.3,129.2"/>
				<path class="st0" d="M671.3,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
					C708.9,151.5,693.4,167,671.3,167z M671.3,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8
					c18.6,0,31.6-13.1,31.6-31.8C702.9,110.1,689.9,96.8,671.3,96.8z M671.3,160.4c-18.2,0-31-12.8-31-31.2c0-18.6,13-32.1,31-32.1
					c17.9,0,31,13.5,31,32.1C702.3,147.6,689.5,160.4,671.3,160.4z M671.3,103.2c-14.5,0-25,11-25,26.1c0,14.9,10.3,25.2,25,25.2
					c14.7,0,25-10.4,25-25.2C696.3,114.1,685.8,103.2,671.3,103.2z"/>
			</g>
			<g>
				<path class="st0" d="M728,95.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V124
					c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H728z"/>
				<path class="st0" d="M786,166.1h-12.7V124c0-13.1-7.6-20.5-20.8-20.5l-0.4,0c-12.3,0.1-20.9,9.1-20.9,21.8v40.9h-12.7v-74H731
					v6.6c5.4-4.9,12.8-7.7,21.2-7.8c20.5,0,33.8,13,33.8,33V166.1z M779.2,160.1h0.7v-36.2c0-16.7-10.6-27-27.7-27
					c-4.8,0.1-13.1,1.3-18.9,8c4.7-4.6,11.2-7.3,18.6-7.3l0.4,0c16.6,0,26.8,10.2,26.8,26.5V160.1z M724.4,160.1h0.7v-34.9
					c0-3.2,0.5-6.3,1.3-9.1l-1.5,2.6V98.1h-0.6V160.1z"/>
			</g>
			<path class="st0" d="M94.8,161c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
				c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
				c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
				C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
				c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
				C83.9,152.9,88.3,158.9,94.8,161"/>
			<g>
				<g>
					<defs>
						<rect id="SVGID_1_" x="3.9" y="6.8" width="214.9" height="203.2"/>
					</defs>
					<clipPath id="SVGID_2_">
						<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
					</clipPath>
					<g class="st1">
						<defs>
							<path id="SVGID_3_" d="M94.8,161c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
								c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
								c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
								C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
								c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
								C83.9,152.9,88.3,158.9,94.8,161"/>
						</defs>
						<clipPath id="SVGID_4_">
							<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
						</clipPath>
					</g>
				</g>
			</g>
			<g class="st2">
				<g>
					<defs>
						<rect id="SVGID_5_" x="3.9" y="12.2" width="214.9" height="197.8"/>
					</defs>
					<clipPath id="SVGID_6_">
						<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
					</clipPath>
					<path class="st3" d="M215.6,72.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
						c2.5,54.9-48.4,101.6-101.6,101.6C49.9,198.2,8.4,154,4,100.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
						C218.5,130,222.2,100.6,215.6,72.9"/>
				</g>
			</g>
		</g>
	</g>
	<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="107.8113" y1="62.1905" x2="84.9779" y2="90.1905">
		<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0"/>
		<stop  offset="0.4058" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
		<stop  offset="0.9913" style="stop-color:#787878;stop-opacity:0.6"/>
	</linearGradient>
	<path class="st4" d="M89.9,52.7c0,0-7.5,13.8-6,53.3c0,0,1.6-25.4,31.6-21.8c0,0-2-20.4,8.6-33.5L89.9,52.7z"/>
</g>
</svg>'; ?>
                            <div class="clearfix"></div>
                        </a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>



                <!-- Carousel -->
                <div id="main-slide" class="carousel slidehome slide" data-ride="carousel">


                    <!-- Carousel inner -->
                    <div class="carousel-inner">
                        <div class="item noitem active">
                            <img class="img-responsive" src="<?php echo base_url() ?>images/slider/img1.png" alt="slider">            
                            <div class="slider-content">
                                <div class="container">
                                    <div class="col-md-12 text-center tranprncy">





                                        <h2 class=" homehaed"><span>Subscribe to your favorite restaurants</span></h2><h2 class="homehaed"><span> Save 20% on every order</span></h2>
                                        <p class="" style="font-size:18px;">Order regularly from the same places? What if you got a big discount on every order from your favorite restaurant?
                                            Fulspoon helps convert your loyalty into big savings.
                                        </p>  
                                    </div>             
                                    <div class="col-md-12 text-center ">
                                        <form name="email_send" id="email_send" method="post" action="<?= base_url() ?>member/signup">
                                            <div class="col-md-4 col-sm-4 col-md-offset-2 homeform-group">
                                                <input type="text" class="form-control homeinput" name="fild_email" id="fild_email" onClick="remove_same()" onChange="remove_same()" placeholder="EMAIL ADDRESS ">
                                            </div>
                                            <div class="col-md-3 col-sm-4 homeform-group"><input type="text" class="form-control homeinput" id="zip_fild" onkeypress="uniCharCode(event)" onChange="remove_same()" onClick="remove_same()" name="zip_fild" placeholder="ZIPCODE" ></div>
                                            <div class="col-md-1 col-sm-2" id="chck"><button type="button"  id="button_id" class="btn btngetstart button_initl"  onClick="check_email_val()" disabled> JOIN NOW </button></div></form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--/ Carousel item end -->

                        <!--/ Carousel item end -->

                        <!--/ Carousel item end -->
                    </div>
                    <!-- Carousel inner end-->

                    <!-- Controls -->       
                </div>
                <!-- /carousel -->
            </section>


        </div>
        <style>
            .errorClass { border:  1px solid red; }
            .error {color: red;}
        </style>
        <script>

            //
            //$(document).ready(function() {
            // // executes when HTML-Document is loaded and DOM is ready
            // 	document.getElementById("chck").style.display = 'none';
            //});
            //


            function uniCharCode(e) {
                if (e.charCode == 13) {
                    if (remove_same() == true) {
                        check_email_val();
                    }
                }
            }

            function remove_same() {
                $("#button_id").removeClass('button_initl');

                document.getElementById("button_id").disabled = false;
                var fild_email = document.getElementById("fild_email").value;
                var zip_fild = document.getElementById("zip_fild").value;
//                alert(parseInt(zip_fild));
                if (fild_email == 'Please enter Email!' || fild_email == 'Please enter Valid Email!') {
                    document.getElementById("fild_email").value = "";
                    $("#fild_email").removeClass('error');
                    $("#fild_email").removeClass('errorClass');

                }
                else if (zip_fild == 'Please enter Zip Code!') {
                    document.getElementById("zip_fild").value = "";
                    $("#zip_fild").removeClass('error');
                    $("#zip_fild").removeClass('errorClass');
                }
                else if (zip_fild != parseInt(zip_fild)) {
                    document.getElementById("zip_fild").value = "";
                    $("#zip_fild").removeClass('error');
                    $("#zip_fild").removeClass('errorClass');
                } else {
                    return true;
                }

            }
            function check_email_val() {
                var fild_email = document.getElementById("fild_email").value;
                var zip_fild = document.getElementById("zip_fild").value;
                if (fild_email == '' || fild_email == 'Please enter Email!')
                {
                    $("#fild_email").addClass('errorClass');

                    document.getElementById("fild_email").value = "Please enter Email!";
                    $("#fild_email").addClass('error');
                    return false;
                }
                if (fild_email) {
                    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                    if (re.test(fild_email) == false) {
                        document.getElementById("fild_email").value = "Please enter Valid Email!";
                        $("#fild_email").addClass('error');
                        $("#fild_email").addClass('errorClass');
                        //alert('please enter genuine email!');
                        return false;
                    }
                }
                if (zip_fild == '' || zip_fild == 'Please enter Zip Code!')
                {

                    //$("#fild_email").removeClass('error');
                    $("#zip_fild").addClass('errorClass');
                    document.getElementById("zip_fild").value = "Please enter Zip Code!";
                    $("#zip_fild").addClass('error');
                    //alert('please enter Zip Code!');

                    return false;
                }
                else
                {

                    //$("#fild_email").removeClass('error');

                    //$("#zip_fild").removeClass('error');
//                    $.ajax({
//                        type: "POST",
//                        url: "<?php echo base_url() ?>member/signup",
//                        data: {fild_email: fild_email, zip_fild: zip_fild},
//                        cache: false,
//                        success: function () {
//                            $('#myModal1').modal('show');
//                            return true;
//                        }
//
//                    });
//alert("sad");
//                    document.getElementById("zip_fild").value = "";
//                    document.getElementById("fild_email").value = "";
//                    window.location.assign("<?= base_url(); ?>member/signup/"+fild_email+"/"+zip_fild);
                    $("#email_send").submit();
                    //$("#email_send").submit();return true;

                }
            }


        </script>


        <!-- modal test#############################-->

        <div class="container">

            <!-- Trigger the modal with a button -->


            <!-- Modal -->
            <div class="modal fade" id="myModal1" role="dialog" >
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <center>
                                <h1>Thank You.</h1>
                            </center>
                        </div>
                        <div class="modal-body">
                            <center> <p>Your message has been sent successfully.</p></center>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>

                </div>
            </div>

        </div>




        <!-- End Home Page Slider -->
        <!-- PACKAGE -->
        <div class="packagewrap">
            <div class="container packagein">


                <span class="class_align">Starts from <?php echo'$' . $get_package_details[0]['user_fee']; ?> / <?php
                    if ($get_package_details[0]['frequency'] == 'Months') {
                        echo "MONTH";
                    }if ($get_package_details[0]['frequency'] == 'Days') {
                        echo "DAY";
                    }if ($get_package_details[0]['frequency'] == 'Years') {
                        echo "YEAR";
                    }
                    ?></span>
                <span class="class_align1"> <i class=""></i> 
                    NO COMMITMENTS 
                </span>
                <span class="class_align2"><i class=""></i>
                    CANCEL ANYTIME  
                </span>  
            </div>
            Guaranteed Savings or your money back - If you save less than your monthly subscription fee, we will refund the difference
        </div>
        <!-- End PACKAGE -->
        <!-- Start How its work Section -->
        <div class="section service">
            <div class="container">
                <div class="row">
                    <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                        <h1><strong>HOW IT WORKS</strong></h1>
                    </div>
                    <!-- Start Service Icon 1 -->
                    <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
                        <div class="service-icon">
                            <i ><img src="<?php echo base_url() ?>images/pickrestaurant.png" alt=""/></i>
                        </div>
                        <div class="service-content">
                            <h3>Pick Restaurants</h3>
                            <p>Select the restaurants you want to subscribe to. We have all your local favorites. </p>

                        </div>
                    </div>
                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->
                    <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
                        <div class="service-icon">
                            <i ><img src="<?php echo base_url() ?>images/online.png" alt=""/></i>
                        </div>
                        <div class="service-content">
                            <h3>Order Online</h3>
                            <p>Order from your subscribed restaurants using the Fulspoon app.
                                We make it super easy to order your regular favorites.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 2 -->

                    <!-- Start Service Icon 3 -->
                    <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
                        <div class="service-icon">
                            <i ><img src="<?php echo base_url() ?>images/savemoney.png" alt=""/></i>
                        </div>
                        <div class="service-content">
                            <h3>Save Money</h3>
                            <p>Restaurant automatically applies a discount on your order. Save money on every order. The more you order through Fulspoon, the more you save.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 3 -->

                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </div>
        <!-- End  How its work Section -->
        <!-- Start Pricing Table Section -->
        <!--  <div class=" section pricing-section calculator-wrap">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          
                          <div class="big-title text-center">
                              <h1 style="color:#fff; text-transform:uppercase; font-size:36px;">Fulspoon Savings Calculator</h1>
                          </div>

                      
                      </div>
                  </div>-->













        <div class=" section pricing-section calculator-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Start Big Heading -->
                        <div class="big-title text-center">
                            <h1 style="color:#fff; text-transform:uppercase; font-size:36px;">Fulspoon Savings Calculator</h1>
                        </div>

                        <!-- End Big Heading -->
                    </div>
                </div>

                <div class="row">


                    <div class="section-navy" id="section-calculator">
                        <div class="container">
                            <div class="alert alert-error text-center hide"></div>
                            <form action="#" id="form-true-cost" class="hide-error-labels" method="post">
                                <div class="cols flex-center">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label class="form-label">How many times do you order from your favorite restaurant every month?</label>

                                            <input name="transactions" id="calc-qty"  class="" placeholder="Enter number" type="number" onKeyPress="return isNumberKey(event)">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="form-label">Your Average Invoice?</label>


                                            <input name="volume" id="calc-vol"  class="money" placeholder="$" type="text" >
                                        </div>
                                    </div>
                                </div>
                                <div class="cols flex-center">
                                    <div class="col-md-4     col-md-offset-4">
                                        <div class="form-group">
                                            <label class="form-label">Number of Favorite Restaurants</label>

                                            <input name="transacti" id="calc-fvt"  class="" placeholder="Number of favorite restaurants" type="text" onKeyPress="return isNumberKey(event)">
                                        </div>
                                    </div>

                                </div>

                                <br>

                                <p class="text-center">
                                <div class="col-md-4 col-md-offset-4">
                                    <button class="btn btn_calculate hvr-grow-shadow btn_calculate btn-block btn-jumbo caps " id="btn-calc"  ><i  class="btn-icon-left icon ion-checkmark-round"></i> Calculate</button>
                                </div>
                                </p>
                                <div class="clearfix"></div>

                            </form>  
                            <div class="clearfix"></div>  
                            <div id="loading" class="hide"><i class="fa fa-spin fa-gear"></i></div>
                            <div id="results-wrap" class="hide">
                                <div id="results">

                                    <div id="results-right">
                                        <!--            <div id="error" class="hide">
                                                      <p><i class="icon ion-android-warning"></i></p>
                                                      <p><strong>Are you sure you numbers are right?</strong><br>
                                                        It seems you've entered numbers that calculate to be less than the True Cost wholesale cost. Please call <span class="phone">877.876.8776</span> 
                                                        to have one of our business consultants help you calculate your True Cost.</p>
                                                    </div>
                                        -->  
                                        <div id="graph">
                                            <div id="graph-pd"> 
                                                <span id="label-pd">Annual spend with fulspoon. <span id="anu_with"></span></span> <span id="plot-pd"> 
                                                    <span id="dot-pd"><img src="<?php echo base_url(); ?>images/logo-icon.png" alt=""></span> </span> </div>
                                            <div id="graph-curr"> <span id="label-curr"><span id="text-label-curr">Annual spend without fulspoon.</span> <span id="anu_out"></span></span> <span id="plot-curr"> <span id="dot-curr"></span> </span> </div>
                                            <div id="graph-overpay"> <span>your saving <br>
                                                    is <span id="saving"></span></span> </div>
                                        </div>
                                    </div>
                                    <div id="results-left">
                                        <h3  style="font-size:26px;">Fulspoon Subscription savings</h3>
                                        <h1>Your current monthly spend is  <span name="m_spend_res" id="m_spend_res" ></span>. <br>
                                            As a member, you would save <span id="per_mont"></span> </h1>

                                        <div id="results-left-bottom" class="caps">
                                            <div class="col-md-6  col-xs-6 text-center class_320"> Monthly Savings <span id="per_mont_1"></span></div>
                                            <div class="col-md-6  col-xs-6 text-center class_320"> Annual Savings <span id="saving_1"></span> </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div align="center"><span style=" line-height:36px; color:#fff;"> *Calculations are based on averages, your incremental Savings may vary.     </span></div>
                                <div class="clearfix"></div>
                                <div class="text-center bigger caps cta hide col-lg-12"> 
                                    <div class="col-md-4 col-md-offset-2"><h4 style="margin-top:15px; text-align:center;">Like what you see?</h4> </div>
                                    <div class="col-md-6"><a href="#modal-start" class="pop-modal btn hvr-grow-shadow btn_calculate col-md-6 col-xs-12 caps " style="margin: 0rem !important;" onClick="hom_box()">SIGN UP</a> </div>
                                </div>        
                            </div>
                            <script type="text/javascript" src="<?php echo base_url() ?>js/jquery_009.js"></script>
                            <script type="text/javascript">

                                        function hom_box()
                                        {
                                            $('#fild_email').focus();
                                            return false;
                                        }


                                        function isNumberKey(evt)
                                        {
                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                            if (charCode != 46 && charCode > 31
                                                    && (charCode < 48 || charCode > 57))
                                                return false;

                                            return true;
                                        }


                                        jQuery(document).ready(function ($) {


                                            $('.money').maskMoney({
                                                prefix: '$',
                                                precision: 0
                                            });

                                            $("#btn-calc").click(function () {

                                                // VALIDATION
                                                if (!$('#calc-vol').val() && !$('#calc-qty').val()) {
                                                    $('#section-calculator .alert-error').fadeIn().text('Please tell us your daily processing volume and how many transitions you each day on average');
                                                    $('#calc-vol').focus();
                                                } else if (!$('#calc-vol').val() || $('#calc-vol').val() <= 0) {
                                                    $('#section-calculator .alert-error').fadeIn().text('Please enter the average total dollar amount you sell on credit and debit cards on a normal day');
                                                    $('#calc-vol').focus();
                                                } else if (!$('#calc-qty').val() || $('#calc-qty').val() <= 0) {
                                                    $('#section-calculator .alert-error').fadeIn().text('Please tell us how many transitions you do daily');
                                                    $('#calc-qty').focus();
                                                }
                                                else if (!$('#calc-fvt').val())
                                                {
                                                    $('#section-calculator .alert-error').fadeIn().text('Please enter number of favorite restaurants');
                                                    $('#calc-fvt').focus();
                                                }
                                                // END VALIDATION

                                                else { // PROCESS

                                                    $('#section-calculator .alert-error').fadeOut();
                                                    var results, rate, vol, qty, qtyVal, fees, deducted, tc, savings;
                                                    results = $('#results');
                                                    rate = $("#calc-rate").val();
                                                    qty = $("#calc-qty").val();
                                                    qtyVal = (qty * 30) * .1;
                                                    vol = numeral($("#calc-vol").val()).format('0');
                                                    vol = vol * 30;
                                                    //		alert(vol);
                                                    fees = vol * .0011;
                                                    //		deducted = $("#calc-deducted").val();
                                                    tc = (rate * vol) + qtyVal + fees;

                                                    var compRate = .035;
                                                    var compPerTrans = .2;

                                                    if (rate == .0185 || rate == .02) {
                                                        compRate = .04;
                                                        compPerTrans = .25;
                                                    }

                                                    var compQtyVal = (qty * 30) * compPerTrans;
                                                    deducted = (compRate * vol) + fees + compQtyVal;

                                                    savings = deducted - tc;

                                                    if (savings < 0) {
                                                        $('#results-right #error').removeClass('hide');
                                                    } else {
                                                        $('#results-right #error').addClass('hide');
                                                    }

                                                    //		$('#calc-true-cost').text( numeral(tc).format('$0,0') );
                                                    $('#calc-true-cost').text(numeral(savings).format('$0,0'));
                                                    $('#calc-label-pd').text(numeral(tc).format('$0,0'));
                                                    $('#calc-label-curr').text(numeral(deducted).format('$0,0'));
                                                    $('#calc-savings-month').text(numeral(savings).format('$0,0'));
                                                    $('#calc-label-overpaid').text(numeral(savings).format('$0,0'));
                                                    $('#calc-savings-year').text(numeral(savings * 12).format('$0,0'));

                                                    $('#results-wrap').slideDown(500, function () {
                                                        results.addClass('results-open');
                                                        $('.cta').delay(5000).slideDown();
                                                    });


                                                    var vol = numeral($("#calc-vol").val()).format('0');
                                                    var calcqty = numeral($('#calc-qty').val()).format('0');
                                                    var calc_fvt = numeral($('#calc-fvt').val()).format('0');






                                                    //var textcontrol =numeral( $('#m_spend_res').val()).format('0');
                                                    // textcontrol.value = '$' + (vol * calcqty).toFixed(2);
                                                    $('#m_spend_res').text($('#m_spend_res').val() + ('$' + (vol * calcqty * calc_fvt).toFixed(2)));
                                                    $('#saving').text($('#saving').val() + ('$' + (vol * calcqty * 12 * .20 * calc_fvt).toFixed(2)));
                                                    $('#per_mont_1').text($('#per_mont_1').val() + ('$' + (vol * calcqty * .20 * calc_fvt).toFixed(2)));
                                                    $('#saving_1').text($('#saving_1').val() + ('$' + (vol * calcqty * 12 * .20 * calc_fvt).toFixed(2)));
                                                    $('#per_mont').text($('#per_mont').val() + ('$' + (vol * calcqty * .20 * calc_fvt).toFixed(2)));
                                                    $('#anu_out').text($('#anu_out').val() + ('$' + (vol * calcqty * 12 * calc_fvt).toFixed(2)));
                                                    $('#anu_with').text($('#anu_with').val() + ('$' + ((vol * calcqty * 12 * calc_fvt) - (vol * calcqty * 12 * .20 * calc_fvt)).toFixed(2)));
                                                    //alert(textcontrol);	

                                                }

                                                return false;
                                            });

                                        });
                            </script>

                        </div>
                    </div>


                    <!--        <div class="row">
                            <div class="col-md-12 formcalcularor">
                            <div style="border:1px solid #fff; padding-bottom:20px;">
                            
                            <div class="col-sm-4">
                            <label class="labelcaltr">How many times do you order from your favorite restaurant every month?</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" placeholder="Enter Value">
                            </div>
                            
                            <div class="col-sm-4">
                            <label class="labelcaltr">Your Average Invoice?</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" placeholder="Enter Value">
                            </div>
                            
                            <div class="col-sm-4">
                            <label class="labelcaltr">Monthly Spend on One Restaurant</label>
                            <div class="clearfix"></div>
                            <input type="text" class="form-control" placeholder="Enter Value">
                            </div>
                            
                            <div class="clearfix"></div>
                            </div>
                            </div>
                            </div>
                            
                                <div class="">
                                
                                <div class="">
                                <h1 style="background-color:#fff; padding:10px 5px; text-align:left;" class="tblhead labelcalculator">
                                Savings with Replimatic
                                </h1>
                                    </div>
                                
                                <div id="maintable">
                                <table class="col-md-12 tableMaincal table-striped table-condensed cf">
                                            <thead>
                                                    <tr>
                                                            <td class="stylebtn"></td>
                                                            <td class="numeric stylebtn">Monthly</td>
                                                            <td class="numeric stylebtn">Annual</td>
                                            <td class="numeric stylebtn">Subscription Fee</td>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                                    <tr>
                                                            <td class="callabel" data-title="">Single Restaurant Subscription</td>
                                                            <td data-title="Monthly" class="numeric">1600</td>
                                                            <td data-title="Annual" class="numeric">Monthly*12</td>
                                            <td data-title="Subscription Fee" class="numeric">XXX</td>
                                                    </tr>
                                                    <tr>
                                                            <td class="callabel" data-title="">Single Restaurant Subscription</td>
                                                            <td data-title="Monthly" class="numeric">1600</td>
                                                            <td data-title="Annual" class="numeric">Monthly*12</td>
                                            <td data-title="Subscription Fee" class="numeric">XXX</td>
                                                    </tr>
                                                    <tr>
                                                            <td class="callabel" data-title="">Single Restaurant Subscription</td>
                                                            <td data-title="Monthly" class="numeric">1600</td>
                                                            <td data-title="Annual" class="numeric">Monthly*12</td>
                                            <td data-title="Subscription Fee" class="numeric">XXX</td>
                                                    </tr>
                    
                                            </tbody>
                                    </table>
                            </div>
                                
                                
                            </div>
                    -->

                </div>
            </div>
        </div>
















    </div>
</div>
<!-- End Pricing Table Section -->

<script>



    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
<!-- Start Purchase Section -->
<!--<div class="section purchase">
  <div class="container">

<!-- Start Video Section Content -->

<!-- End Section Content -->

</div>
<!-- .container -->
</div>
<!-- End Purchase Section -->

<!-- Start Testimonials Section -->


<!-- End Testimonials Section -->


<!-- Start Why Fulspoon Section -->
<!--<div class="section" style="background:#343434;">
  <div class="container">

<!-- Start Big Heading -->

<!-- End Big Heading -->






</div>
<!-- .container -->
</div>
<!-- End Why Fulspoon Section -->

<!-- Start Footer Section -->

<!-- End Footer Section -->


</div>
<!-- End Full Body Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

<div id="loader">
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
</div>
<!--<script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
--><script src="<?= base_url() ?>js/numeral.js"></script>
<?php $this->load->view("footermember"); ?>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72687545-1', 'auto');
    ga('send', 'pageview');

</script>
<style>
    .modal {
        text-align: center;
        padding: 0!important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    .modal-header{
        background-color:#72CA42;
        text-align:center;
        color:#fff !important;
    }
    .modal-header h4{
        color:#fff;	
    }

    .modal-footer{
        border-top:none;	
    }
    .btn_save{ background-color:#72CA42; color:#fff; padding:6px 15px; border:none;}
    .btn_close{ background-color:#2D4C1C; color:#fff; padding:6px 15px; border:none;}
    .text_box{ border:none ; border-bottom:1px solid #c2c2c2;}

</style>
</body>

</html>