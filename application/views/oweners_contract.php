<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon | Home</title>

        <!-- Define Charset -->
        <meta charset="utf-8">
        <link rel="icon" href="<?= base_url() ?>images/favicon.ico" type="image/x-icon" />
        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Page Description and Author -->
        <meta name="description" content="Margo - Responsive HTML5 Template">
        <meta name="author" content="iThemesLab">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?= base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">

        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">

        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/slicknav.css" media="screen">

        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/oweners.css" media="screen">

        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/responsive.css" media="screen">

        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.css" media="screen">

        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/colors/green.css" title="green" media="screen" />


        <!-- Margo JS  -->
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.slicknav.js"></script>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

<div class="navbar navbar-default navbar-top green-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="<?= base_url() ?>owners">
                                <?php echo '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="170px" height="46px" viewBox="0 0 792 214" style="enable-background:new 0 0 792 214;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{opacity:0.52;}
	.st3{clip-path:url(#SVGID_6_);fill:#6EBD44;}
	.st4{fill:url(#SVGID_7_);}
	.st5{fill:#EFEFEF;}
</style>
<g>
	<g>
		<path class="st0" d="M253.3,163.1v-61.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
			c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H253.3z"/>
		<path class="st0" d="M262.9,166.1h-12.6v-61.5h-13.9V92.6h13.9v-3.1c0-16.9,8.3-26.3,23.4-26.3c5.5,0,10.6,1.8,15.3,5.3l2.3,1.7
			l-6.5,9.7l-2.4-1.5c-3.6-2.1-5.6-3-8.9-3c-4.9,0-10.5,1.6-10.5,14.1v3.1h22.4v11.9h-22.4V166.1z M256.3,160.1h0.6V89.6
			c0-17.5,10.3-20.1,16.5-20.1c3.9,0,6.7,1,9.5,2.5l0.1-0.1c-2.9-1.7-6.1-2.5-9.3-2.5c-11.7,0-17.4,6.6-17.4,20.3V160.1z"/>
	</g>
	<g>
		<path class="st0" d="M353.1,163.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V94.9h6.7V134
			c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V94.9h6.7v68.2H353.1z"/>
		<path class="st0" d="M328.9,167.2L328.9,167.2c-20.5,0-33.7-13-33.7-33V91.9h12.7V134c0,13.3,7.4,20.5,20.9,20.5l0.3,0
			c12.3-0.1,20.8-9.1,20.8-21.8V91.9h12.7v74.2h-12.6v-6.7C344.7,164.3,337.3,167.1,328.9,167.2L328.9,167.2z M301.2,97.9v36.2
			c0,16.7,10.6,27,27.7,27c4.9-0.1,13.2-1.3,19-8c-4.7,4.6-11.2,7.3-18.6,7.4l-0.4,0c-16.6,0-26.9-10.2-26.9-26.5V97.9H301.2z
			 M356.1,160.1h0.6V97.9H356v34.9c0,3.3-0.5,6.4-1.4,9.2l1.5-2.7V160.1z"/>
	</g>
	<g>
		<rect x="377.5" y="66.7" class="st0" width="6.7" height="96.4"/>
		<path class="st0" d="M387.2,166.1h-12.7V63.7h12.7V166.1z M380.5,160.1h0.7V69.7h-0.7V160.1z"/>
	</g>
	<g>
		<path class="st0" d="M450.8,107.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
			c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
			c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
			c9.9,0,18.6,2.8,25.3,9.1L450.8,107.5z"/>
		<path class="st0" d="M429.8,167.2c-14.1,0-26.1-5.6-32.3-15l-1.4-2.2l9.4-8.4l1.9,2.8c4.5,6.4,12.9,10.3,22.5,10.3
			c5.4,0,17.9-1,18.2-10.1c0.2-6.7-7.7-8.5-19.6-10.4c-12.8-2.1-28.8-4.8-29-21.2c-0.1-5.4,1.8-10.1,5.5-13.8
			c5.2-5.2,13.9-8.2,24-8.2l0.5,0c11.1,0,20.4,3.3,27.4,9.9l2.2,2.1l-8.2,8.7l-2.2-1.9c-6.1-5.3-11.9-6.4-19.2-6.4l-0.5,0
			c-6.7,0-12.3,1.7-15.1,4.6c-1.3,1.4-2,3-1.9,4.9c0.2,5.9,8,7.7,17.3,9.4l2.4,0.4c11.9,2,29.8,5,29,22.8
			C460.3,161.4,444.5,167.2,429.8,167.2z M404.1,151c5.4,6.4,14.9,10.2,25.7,10.2c5.8,0,24.5-1.2,25-16.1
			c0.5-11.6-10.1-14.3-24-16.7l-2.4-0.4c-8.7-1.5-22-3.9-22.3-15.1c-0.1-3.5,1.1-6.7,3.6-9.2c4-4.1,11-6.4,19.4-6.4l0.5,0
			c6.6,0,13.7,0.8,20.9,6.1l0.1-0.1c-5.5-4.2-12.6-6.3-21-6.3l-0.5,0c-8.5,0-15.7,2.3-19.8,6.4c-2.5,2.5-3.8,5.8-3.7,9.5
			c0.1,10.6,9.5,13,23.9,15.4c11.1,1.8,25,4,24.7,16.5c-0.4,9.9-9.4,15.9-24.2,15.9c-10.2,0-19.4-3.8-25.3-10.2L404.1,151z"/>
	</g>
	<g>
		<path class="st0" d="M472.8,191.4V95.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35c0,22.9-15.4,35.3-34.6,35.3
			c-11.6,0-22.3-5.6-28.1-16.9v44.1H472.8z M535.2,129.1c0-19.1-12.4-28.5-27.8-28.5c-15.8,0-27.5,12-27.5,28.6
			c0,16.7,12,28.6,27.5,28.6C522.8,157.8,535.2,148.2,535.2,129.1"/>
		<path class="st0" d="M482.3,194.4h-12.5V92.1h12.6v9.8c6.6-6.9,16-10.8,26.5-10.8l0.1,0c21.5,0.8,35.9,16.1,35.9,38
			c0,22.9-15.1,38.3-37.6,38.3c-9.9,0-18.7-3.8-25.1-10.5V194.4z M475.8,188.4h0.5v-53.5l1.7,3.4c-0.8-2.9-1.2-5.9-1.2-9.1
			c0-2.9,0.3-5.8,1-8.4l-1.4,2.6V98.1h-0.6V188.4z M487.8,153.9c5.1,4.8,11.9,7.4,19.6,7.4c19.2,0,31.6-12.7,31.6-32.3
			c0-18.7-11.8-31.3-30.1-32c-4,0-7.8,0.7-11.2,1.9c3-1,6.3-1.5,9.8-1.5c18.7,0,30.8,12.4,30.8,31.5c0,19.3-12.1,31.8-30.8,31.8
			C499.8,160.8,493,158.3,487.8,153.9z M507.3,103.6c-14.2,0-24.5,10.8-24.5,25.6c0,14.9,10.3,25.6,24.5,25.6
			c12,0,24.8-6.8,24.8-25.8C532.2,113.1,522.9,103.6,507.3,103.6z"/>
	</g>
	<g>
		<path class="st0" d="M555.3,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
			C570.7,164,555.3,150.8,555.3,129.2 M617.8,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
			c0,17.6,12.5,28.2,27.9,28.2C605.2,157.4,617.8,146.8,617.8,129.2"/>
		<path class="st0" d="M589.8,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
			C627.4,151.5,611.9,167,589.8,167z M589.8,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8c18.6,0,31.6-13.1,31.6-31.8
			C621.4,110.1,608.4,96.8,589.8,96.8z M589.8,160.4c-18.2,0-30.9-12.8-30.9-31.2c0-18.6,13-32.1,30.9-32.1c17.9,0,31,13.5,31,32.1
			C620.8,147.6,608,160.4,589.8,160.4z M589.8,103.2c-14.5,0-24.9,11-24.9,26.1c0,14.9,10.3,25.2,24.9,25.2c14.7,0,25-10.4,25-25.2
			C614.8,114.1,604.3,103.2,589.8,103.2z"/>
	</g>
	<g>
		<path class="st0" d="M636.8,129.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
			C652.2,164,636.8,150.8,636.8,129.2 M699.3,129.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
			C686.7,157.4,699.3,146.8,699.3,129.2"/>
		<path class="st0" d="M671.3,167c-22.1,0-37.6-15.6-37.6-37.8c0-22.2,15.8-38.4,37.6-38.4c21.8,0,37.6,16.1,37.6,38.4
			C708.9,151.5,693.4,167,671.3,167z M671.3,96.8c-18.6,0-31.6,13.3-31.6,32.4c0,18.7,13,31.8,31.6,31.8c18.6,0,31.6-13.1,31.6-31.8
			C702.9,110.1,689.9,96.8,671.3,96.8z M671.3,160.4c-18.2,0-31-12.8-31-31.2c0-18.6,13-32.1,31-32.1c17.9,0,31,13.5,31,32.1
			C702.3,147.6,689.5,160.4,671.3,160.4z M671.3,103.2c-14.5,0-25,11-25,26.1c0,14.9,10.3,25.2,25,25.2c14.7,0,25-10.4,25-25.2
			C696.3,114.1,685.8,103.2,671.3,103.2z"/>
	</g>
	<g>
		<path class="st0" d="M728,95.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V124
			c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H728z"/>
		<path class="st0" d="M786,166.1h-12.7V124c0-13.1-7.6-20.5-20.8-20.5l-0.4,0c-12.3,0.1-20.9,9.1-20.9,21.8v40.9h-12.7v-74H731v6.6
			c5.4-4.9,12.8-7.7,21.2-7.8c20.5,0,33.8,13,33.8,33V166.1z M779.2,160.1h0.7v-36.2c0-16.7-10.6-27-27.7-27
			c-4.8,0.1-13.1,1.3-18.9,8c4.7-4.6,11.2-7.3,18.6-7.3l0.4,0c16.6,0,26.8,10.2,26.8,26.5V160.1z M724.4,160.1h0.7v-34.9
			c0-3.2,0.5-6.3,1.3-9.1l-1.5,2.6V98.1h-0.6V160.1z"/>
	</g>
	<path class="st0" d="M94.8,161c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
		c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
		c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
		C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
		c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
		C83.9,152.9,88.3,158.9,94.8,161"/>
	<g>
		<g>
			<defs>
				<rect id="SVGID_1_" x="3.9" y="6.8" width="214.9" height="203.2"/>
			</defs>
			<clipPath id="SVGID_2_">
				<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
			</clipPath>
			<g class="st1">
				<defs>
					<path id="SVGID_3_" d="M94.8,161c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
						c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
						c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C82,10.3,69.8,3.1,60,8.7
						C40.2,20,13,43.1,5.5,87c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
						c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C108.6,22,95,34.6,87.9,59.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
						C83.9,152.9,88.3,158.9,94.8,161"/>
				</defs>
				<clipPath id="SVGID_4_">
					<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
				</clipPath>
			</g>
		</g>
	</g>
	<g class="st2">
		<g>
			<defs>
				<rect id="SVGID_5_" x="3.9" y="12.2" width="214.9" height="197.8"/>
			</defs>
			<clipPath id="SVGID_6_">
				<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
			</clipPath>
			<path class="st3" d="M215.6,72.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
				c2.5,54.9-48.4,101.6-101.6,101.6C49.9,198.2,8.4,154,4,100.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
				C218.5,130,222.2,100.6,215.6,72.9"/>
		</g>
	</g>
	<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="107.8113" y1="62.1905" x2="84.9779" y2="90.1905">
		<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0"/>
		<stop  offset="0.4058" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
		<stop  offset="0.9913" style="stop-color:#787878;stop-opacity:0.6"/>
	</linearGradient>
	<path class="st4" d="M89.9,52.7c0,0-7.5,13.8-6,53.3c0,0,1.6-25.4,31.6-21.8c0,0-2-20.4,8.6-33.5L89.9,52.7z"/>
	<path class="st5" d="M161.1,12c0,0,11.3,8.3,14.8,12.4c0,0,4.7,4.6,7.8,8.9c0,0,3.9,4.6,9.1,13.6c0,0,5.3,10.2,6.5,13.6
		c0,0,3.8,10.5,4.2,13c0,0,2.3,10.2,2.4,12.2c0,0,1.4,12.1,1.1,13.9c0,0-0.2,9.6-0.8,12.3c0,0-1.1,8-2.3,11.3c0,0-2.2,7.6-3.6,10.4
		c0,0-4.3,9.8-5.9,12c0,0-6.4,10.1-8,11.7c0,0-25.7,40.8-87.2,40.5c0,0-58.1-0.9-87.3-64.1c0,0-2.7-6.6-4.1-11.9
		c0,0-3.1-12.2-3.3-16L4,100.7c0,0-0.6,11.7,1.7,23.1c0,0,13,76.4,95.8,86c0,0,70.7,6.6,102.7-53.2c0,0,24.9-42.2,9.6-90.5
		c0,0-5.2-18.6-17-31.9C196.8,34.2,183.2,17.4,161.1,12z"/>
</g>
</svg>






'; ?>
                            </a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <!-- Stat Search -->
                            <!-- Stat Search -->
                            <div class="buttons-side">

                                <a class="btn btn-default btnlogin" href="<?= base_url() ?>owners/signup" role="button">SIGN UP</a>
                            </div>
                            <!-- End Search -->
                            <!-- End Search -->
                            <!-- Start Navigation List -->
                            <ul class="nav navbar-nav navbar-right">
                                <li> <a style="color:#fff;">Call Us:(917) 960-2520</a></li>             
                            </ul>
                            <!-- End Navigation List -->
                        </div>
                    </div>

                    <!-- Mobile Menu Start -->
                    <ul class="wpb-mobile-menu">
                        <li><a>Call Us: (917) 960-2520</a></li>
                        <!--<li><a href="index-01.html">LOGIN</a></li>-->
                        <li><a href="<?= base_url() ?>owners/singup">SIGN UP</a></li></ul>
                    <!-- Mobile Menu End -->

                </div>
        <!-- Container -->
        <div id="container"> 
         <div id="content">
           <div class="container contractwp">
      <br id="docs-internal-guid-16910da6-7553-6a5b-e100-29c8ffd1c610">
          <div class="successwrap" style="font-size:24px; line-height:36px;">GENERAL CONTRACT FOR SERVICES</div>

 <br>
 
 <p  class="contract_lid">This Contract for Services is made effective as of <?= date("F d, Y") ?>, by and between <?= strtoupper($client_name) ?> (&quot;Merchant&quot;) of <?= $client_address ?>, and Fulspoon LLC (&quot;Fulspoon&quot;) of 175 Varick St., New York, NY 10014. </p>
 
 <p  class="contract_lid"><span>1. DESCRIPTION OF SERVICES.</span> Beginning on <?= date("F d, Y") ?>, Fulspoon will provide to Merchant the following services (collectively, the &quot;Services&quot;): </p>
 <p  class="contract_lid">(a) Create and manage subscriptions with customers, (b) generate and transmit orders and (c) share subscription revenue. </p>
 
 <p  class="contract_lid">Under the Services, a subscriber (&ldquo;Subscriber&rdquo;) is defined as a customer who agrees to pay a fixed monthly fee (&ldquo;Subscription Fee&rdquo;) to Fulspoon in return for a fixed concession (&ldquo;Subscriber Concession&rdquo;) on every order placed with the Merchant. </p>

 <p  class="contract_lid"><span>2. PAYMENT.</span> Payment shall be made to Merchant by Fulspoon. </p>
 
 <p  class="contract_lid">Fulspoon agrees to pay Merchant as follows: </p>
 
 <p  class="contract_lid">Fulspoon will pay Merchant $<?= $data['contract_price'] ?> for every Effective Subscriber the Merchant had for the prior month on the first Monday of the current month. </p>

 <p  class="contract_lid">Number of Effective Subscribers is calculated as follows: </p>
<ol class="list">
  <li >
     <p  class="contract_lid">Count the number of new Subscribers added in the previous month</p>
  </li>
  <li >
     <p  class="contract_lid">Count the number of Subscribers who renewed their subscription last month</p>
  </li>
</ol>

 <p  class="contract_lid">Add (a) and (b) to get number of Effective Subscribers. </p>


 <p  class="contract_lid"><span>3. MERCHANT OBLIGATION.</span> The Merchant is obligated to provide a <?= $data['contarct_cc'] ?>% concession on every order to Subscriber. </p>
 
 <p  class="contract_lid"><span>4. TERM.</span> This Contract may be terminated by either party upon 30 days prior written notice to the other party. An email notice by one party will suffice. </p>
 
 <p  class="contract_lid"><span>5. WORK PRODUCT OWNERSHIP.</span> Any copyrightable works, ideas, discoveries, inventions, patents, products, or other information (collectively the &quot;Work Product&quot;) developed in whole or in part by Fulspoon in connection with the Services will be the exclusive property of Fulspoon. Upon request, Merchant will execute all documents necessary to confirm or perfect the exclusive ownership of Fulspoon to the Work Product. </p>
 
 <p  class="contract_lid"><span>6. CONFIDENTIALITY.</span> Fulspoon, and its employees, agents, or representatives will not at any time or in any manner, either directly or indirectly, use for the personal benefit of Fulspoon, or divulge, disclose, or communicate in any manner, any information that is proprietary to Merchant. Fulspoon and its employees, agents, and representatives will protect such information and treat it as strictly confidential. This provision will continue to be effective after the termination of this Contract. Any oral or written waiver by Merchant of these confidentiality obligations which allows Fulspoon to disclose Merchant's confidential information to a third party will be limited to a single occurrence tied to the specific information disclosed to the specific third party, and the confidentiality clause will continue to be in effect for all other occurrences. </p>
 

 <p  class="contract_lid"><span>7. DEFAULT.</span> The occurrence of any of the following shall constitute a material default under this Contract: 
 <br>
  a. The failure to make a required payment when due. 
 <br>
  b. The insolvency or bankruptcy of either party. 
 <br>
 c. The subjection of any of either party's property to any levy, seizure, general assignment for the benefit of creditors, application or sale for or by any creditor or government agency. 
 <br>
 d. The failure to make available or deliver the Services in the time and manner provided for in this Contract. 
<br>
 e. The failure of Merchant to meet Merchant Obligation as described in #3 above.</p>
 
 <p  class="contract_lid"><span>8. REMEDIES.</span> In addition to any and all other rights a party may have available according to law, if a party defaults by failing to substantially perform any provision, term or condition of this Contract (including without limitation the failure to make a monetary payment when due), the other party may terminate the Contract by providing written notice to the defaulting party. This notice shall describe with sufficient detail the nature of the default. The party receiving such notice shall have twenty days from the effective date of such notice to cure the default(s). Unless waived in writing by a party providing notice, the failure to cure the default(s) within such time period shall result in the automatic termination of this Contract. </p>
 
 <p  class="contract_lid"><span>9. FORCE MAJEURE.</span> If performance of this Contract or any obligation under this Contract is prevented, restricted, or interfered with by causes beyond either party's reasonable control (&quot;Force Majeure&quot;), and if the party unable to carry out its obligations gives the other party prompt written notice of such event, then the obligations of the party invoking this provision shall be suspended to the extent necessary by such event. The term Force Majeure shall include, without limitation, acts of God, fire, explosion, vandalism, storm or other similar occurrence, orders or acts of military or civil authority, or by national emergencies, insurrections, riots, or wars, or strikes, lock-outs, work stoppages, or other labor disputes, or supplier failures. The excused party shall use reasonable efforts under the circumstances to avoid or remove such causes of non-performance and shall proceed to perform with reasonable dispatch whenever such causes are removed or ceased. An act or omission shall be deemed within the reasonable control of a party if committed, omitted, or caused by such party, or its employees, officers, agents, or affiliates. </p>
 
 <p  class="contract_lid"><span>10. DISPUTE RESOLUTION.</span> The parties will attempt to resolve any dispute out of or relating to this Agreement through friendly negotiations amongst the parties. If the matter is not resolved by negotiation, the parties will resolve the dispute using the below Alternative Dispute Resolution (ADR) procedure. </p>
 
 <p  class="contract_lid">Any controversies or disputes arising out of or relating to this Agreement will be submitted to mediation in accordance with any statutory rules of mediation. If mediation is not successful in resolving the entire dispute or is unavailable, any outstanding issues will be submitted to final and binding arbitration under the rules of the American Arbitration Association. The arbitrator's award will be final, and judgment may be entered upon it by any court having proper jurisdiction. </p>
 
 <p  class="contract_lid"><span>11. ENTIRE AGREEMENT.</span> This Contract contains the entire agreement of the parties, and there are no other promises or conditions in any other agreement whether oral or written concerning the subject matter of this Contract. This Contract supersedes any prior written or oral agreements between the parties. </p>

 <p  class="contract_lid"><span>12. SEVERABILITY.</span> If any provision of this Contract will be held to be invalid or unenforceable for any reason, the remaining provisions will continue to be valid and enforceable. If a court finds that any provision of this Contract is invalid or unenforceable, but that by limiting such provision it would become valid and enforceable, then such provision will be deemed to be written, construed, and enforced as so limited. </p>
 
 <p  class="contract_lid"><span>13. AMENDMENT.</span> This Contract may be modified or amended by Fulspoon at any time, with notification to the Merchant. Non-termination of the Contract by the Merchant will constitute agreement with modifications. </p>
 
 <p  class="contract_lid"><span>14. GOVERNING LAW.</span> This Contract shall be construed in accordance with the laws of the State of New York. </p>
 
 <p  class="contract_lid"><span>15. NOTICE.</span> Any notice or communication required or permitted under this Contract shall be sufficiently given if delivered in person or by certified mail, by email, website notification or phone call, to the contact details set forth in the opening paragraph of this Contract or provided separately by Merchant to Fulspoon. </p>
 
 <p  class="contract_lid"><span>16. WAIVER OF CONTRACTUAL RIGHT.</span> The failure of Fulspoon to enforce any provision of this Contract shall not be construed as a waiver or limitation of Fulspoon&rsquo;s right to subsequently enforce and compel strict compliance with every provision of this Contract. </p>
 
 <p  class="contract_lid"><span>17. ADDITIONAL TERMS.</span> Additional terms provided below also apply. </p>
<ol class="list">
  <li >
     <p  class="contract_lid">Fulspoon agrees to enable customers to place orders from Merchant via Fulspoon&rsquo;s proprietary ordering system and associated apps (collectively the &ldquo;System&rdquo;). Fulspoon agrees to maintain and operate a microsite (&ldquo;MS&rdquo;) when applicable, and to obtain the URL for such MS on Merchant&rsquo;s behalf, which Merchant grants Fulspoon the right to do. </p>
  </li>
  <li >
     <p  class="contract_lid">Fulspoon agrees to include certain content (including without limitation menus, photographs, trademarks and logos) provided by Merchant (the &ldquo;Merchant Content&rdquo;) on the System. Fulspoon owns all right, title, interest and copyright in and to the System and any content supplied by Merchant (including on the MS), and will have sole editorial control over the System and the Services, including the presentation of the Merchant Content. </p>
  </li>
  <li >
     <p  class="contract_lid">Fulspoon will transmit orders to Merchant for processing (the &ldquo;Services&rdquo;). Fulspoon EXPRESSLY DISCLAIMS ALL WARRANTIES WITH RESPECT TO THE SYSTEM, SERVICES and MS, INCLUDING WITHOUT LIMITATION IMPLIED OR EXPRESS WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. The Services are provided on an &ldquo;as is&rdquo; and &ldquo;as available&rdquo; basis, and Fulspoon shall not be liable for damages resulting from the failure of the MS, System, Services or Merchant Content.</p>
  </li>
  <li >
     <p  class="contract_lid">Merchant agrees to use its best efforts to prepare and deliver the customer orders (the &ldquo;Orders&rdquo;) placed via the System in accordance with the specifications (regarding delivery hours, minimum charges, etc.) provided to Fulspoon at the time of enrollment (the &ldquo;Specifications&rdquo;). The Specifications and item pricing provided through the System must be at least as favorable to the consumer as that which is available to consumer directly or through any other 3rd party for the products and services of the Merchant. Changes to Specifications, catalogs and menus require reasonable advance notice. Merchant will provide notice to staff that telephone conversations related to the Services may be recorded and that staff must advise caller that CSC(Card Security Code)/CVV/CVV2 should not be transmitted over the phone. </p>
  </li>
  <li >
     <p  class="contract_lid">Merchant hereby grants to Fulspoon a perpetual, royalty-free worldwide right and license to use the Merchant Content on the System, the MS and for marketing and promotional purposes via any means now known or hereinafter developed. Merchant owns all right, title, interest and copyright in and to the Merchant Content, subject to the license granted to Fulspoon herein.</p>
  </li>
  <li >
     <p  class="contract_lid">Merchant agrees that it will maintain the confidentiality of all non-public information that Merchant acquires in the course of performing this Agreement, including without limitation all customer information, as well as the terms and conditions of this Agreement (the &ldquo;Confidential Information&rdquo;). Merchant will not directly market to or solicit any consumer or company obtained through the System or via the Services for the purpose of soliciting that customer to order directly from Merchant or through a 3rd party. (This excludes paper menus.) </p>
  </li>
  <li >
     <p  class="contract_lid">Merchant agrees to be bound by Fulspoon&rsquo;s Terms of Use at <a href="http://www.fulspoon.com/terms-of-use/">http://www.fulspoon.com/terms-of-use/</a>, which may be amended from time to time by Fulspoon.</p>
  </li>
  <li >
     <p  class="contract_lid">Merchant represents and warrants: (i) it has the authority to enter into this Agreement, and doing so won&rsquo;t violate any other agreement to which it is a party; (ii) the Merchant Content won&rsquo;t violate the rights of any 3rd party; (iii) it will comply with all laws, rules and regulations relating to the preparation, sale and delivery of food and drink (including alcohol), including any laws regarding timely and full payment of tips to delivery employees, as well as any other laws applicable to its business, including wage and hour laws; and (iv) it will remit to the applicable taxing authority all legally-required taxes and will file all required tax returns and forms. Restaurant will indemnify and hold Fulspoon (including its directors, employees, officers, agents and affiliates) harmless from any and all claims, actions, proceedings and damages arising out of Merchant&rsquo;s activities or any breach or alleged breach of these representations and warranties. </p>
  </li>
</ol>

<form action="<?= base_url() ?>owners/oweners_success" method="post">
                        <input type="hidden" value="<?= $this->uri->segment(3) ?>" name="id">
 <p  class="contract_lid"><span>18. ASSIGNMENT.</span> Neither party may assign or transfer this Contract without the prior written consent of the non-assigning party, which approval shall not be unreasonably withheld. </p>
 <br>
 <p  class="contract_lid">IN WITNESS WHEREOF, the parties hereto have caused this Agreement to be executed by their duly authorized representatives as of the date first above written. </p>
 <br>
 <p  class="contract_lid">Service Recipient: </p>
 <p  class="contract_lid"><?= $client_name ?></p>
 
 
  <p  class="contract_lid">By:&nbsp;&nbsp;&nbsp;<input required name="client_name" type="text" placeholder="Enter Name"></p>

<div class="container text-center"><br>
                    <!--<a href="<?= base_url() ?>owners/oweners_success/">--> 
                    <button style="margin-bottom:10px;" class="btn btngreen">ACCEPT</button></a><a href="<?= base_url() ?>owners/oweners_decline/<?= $this->uri->segment(3) ?>">    <button type="button" class="btn btn_reject">DECLINE</button>
 <br>
 
</form>
 </div>
        </div>
                        <!-- End content -->


                        <!-- Start Footer -->
