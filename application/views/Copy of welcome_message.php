<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

    <head>

        <!-- Basic -->
        <title>Fulspoon | Subscribe to your favorite restaurants, save 20% on every order</title>
        <link rel="icon" href="images/appicon.png" type="image/gif">
        <!-- Define Charset -->
        <meta charset="utf-8">
        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Responsive Metatag -->
        <meta name="description" content="Subscribe to your favorite restaurants Save 20% on every order Order regularly from the same places? What if you got a big discount on every order from your favorite restaurant? Fulspoon helps convert your loyalty into big savings. We are getting ready to launch and would love to notify you when ready">

        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css" type="text/css" media="screen">
        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/font-awesome.min.css" type="text/css" media="screen">
        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/slicknav.css" media="screen">
        <!-- Margo CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/style.css" media="screen">
        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/responsive.css" media="screen">
        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/animate.css" media="screen">
        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/colors/red.css" title="red" media="screen" />
        <!-- Margo JS  -->
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.migrate.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/modernizrr.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/owl.carousel.js"></script> 
        <script type="text/javascript" src="<?php echo base_url() ?>js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/count-to.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.easypiechart.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/mediaelement-and-player.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.slicknav.js"></script>
        <style>
            button[disabled=disabled], button:disabled {
                background:#7ad648;
                border:none;
                border-radius: 0px !important;
                min-height: 42px;
                min-width: 170px;
                color:#fff;
                text-transform:uppercase;
                // your css rules
            }

        </style>

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>

    <body>

        <!-- Full Body Container -->
        <div id="container">


            <!-- Start Header Section -->

            <header class="clearfix">
                <!-- Start  Logo & Naviagtion  -->
                <div class="navbar navbar-default  navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Stat Toggle Nav Link For Mobiles -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                            <!-- End Toggle Nav Link For Mobiles -->
                            <a class="navbar-brand" href="<?= base_url() ?>">
                              <?php echo '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="170px" height="46px" viewBox="0 0 791 215" style="enable-background:new 0 0 791 215;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#73BF4C;}
	.st1{clip-path:url(#SVGID_2_);}
	.st2{clip-path:url(#SVGID_6_);}
	.st3{filter:url(#Adobe_OpacityMaskFilter);}
	.st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_10_);}
	.st5{clip-path:url(#SVGID_8_);mask:url(#SVGID_9_);fill:url(#SVGID_11_);}
	.st6{opacity:0.52;}
	.st7{clip-path:url(#SVGID_13_);fill:#6EBD44;}
</style>
<g>
	<path class="st0" d="M259.3,161.1V99.5h-13.9v-5.9h13.9v-6.1c0-13.5,5.4-23.3,20.4-23.3c5.1,0,9.6,1.8,13.5,4.7l-3.3,5
		c-3.7-2.2-6.3-3.4-10.5-3.4c-8.5,0-13.5,5.2-13.5,17.1v6.1h22.4v5.9h-22.4v61.5H259.3z"/>
	<path class="st0" d="M356.1,161.1v-12.3c-5.2,9.4-14.7,13.2-24.2,13.4c-18.2,0-30.7-11.2-30.7-30V92.9h6.7V132
		c0,15.4,9.5,23.7,24.2,23.5c14-0.1,23.8-10.7,23.8-24.8V92.9h6.7v68.2H356.1z"/>
	<rect x="380.5" y="64.7" class="st0" width="6.7" height="96.4"/>
	<path class="st0" d="M452.8,105.5c-6.9-6.1-13.6-7.2-21.2-7.2c-10.6-0.1-20.8,3.9-20.5,12.5c0.3,9.1,12.1,10.9,20.7,12.4
		c12.1,2.1,28.8,4.1,28.1,20c-0.4,15-16,19-28,19c-12,0-23.8-4.5-29.7-13.6l5-4.4c5.6,8.1,15.8,11.6,24.9,11.6
		c8.3,0,20.8-2.2,21.2-12.9c0.3-9.8-11-11.7-22.2-13.5c-13.2-2.2-26.3-4.7-26.4-18.3c-0.1-13.4,13.2-19.1,27-19
		c9.9,0,18.6,2.8,25.3,9.1L452.8,105.5z"/>
	<path class="st0" d="M474.8,189.4V93.1h6.6v16.1c5.4-10.3,16.2-17.1,29.5-17.1c18.5,0.7,33,13.1,33,35c0,22.9-15.4,35.3-34.6,35.3
		c-11.6,0-22.3-5.6-28.1-16.9v44.1H474.8z M537.2,127.1c0-19.1-12.4-28.5-27.8-28.5c-15.8,0-27.5,12-27.5,28.6
		c0,16.7,12,28.6,27.5,28.6C524.8,155.8,537.2,146.2,537.2,127.1"/>
	<path class="st0" d="M557.3,127.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
		C572.7,162,557.3,148.8,557.3,127.2 M619.8,127.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-27.9,11.4-27.9,29.1
		c0,17.6,12.5,28.2,27.9,28.2C607.2,155.4,619.8,144.8,619.8,127.2"/>
	<path class="st0" d="M638.8,127.2c0-21.6,15.4-35.4,34.6-35.4c19.1,0,34.6,13.8,34.6,35.4c0,21.6-15.4,34.8-34.6,34.8
		C654.2,162,638.8,148.8,638.8,127.2 M701.3,127.2c0-17.6-12.5-29.1-28-29.1c-15.4,0-28,11.4-28,29.1c0,17.6,12.5,28.2,28,28.2
		C688.7,155.4,701.3,144.8,701.3,127.2"/>
	<path class="st0" d="M730,93.1v12.1c5.2-9.4,14.7-13.2,24.2-13.4c18.2,0,30.7,11.2,30.7,30v39.2h-6.7V122
		c0-15.4-9.5-23.7-24.2-23.5c-14,0.1-23.8,10.7-23.8,24.8v37.9h-6.7v-68H730z"/>
	<path class="st0" d="M97.8,159c0.1,0,0.3,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
		c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.7,22.5-27,63.3-81.7,57.8
		c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C85,8.3,72.8,1.1,63,6.7
		C43.2,18,16,41.1,8.5,85c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
		c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C111.6,20,98,32.6,90.9,57.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
		C86.9,150.9,91.3,156.9,97.8,159"/>
	<g>
		<g>
			<defs>
				<rect id="SVGID_1_" x="6.9" y="4.8" width="214.9" height="203.2"/>
			</defs>
			<clipPath id="SVGID_2_">
				<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
			</clipPath>
			<g class="st1">
				<defs>
					<path id="SVGID_3_" d="M97.8,159c0.1,0,0.2,0.1,0.4,0.1c10,3,20.1-4.4,20.1-14.8v-31.5h12.8c8.4,0,15.2-6.8,15.2-15.2
						c0-8.4-6.8-15.2-15.2-15.2h-12.5c0,0-3.8-45,33.2-43.6c37,1.4,45.7,57.8,37,80.3c-8.6,22.5-27,63.3-81.7,57.8
						c-54.7-5.5-71.3-54.7-68.9-81.7c2.2-24.6,14.7-49.4,38.9-62.8c4.6-2.6,7.5-7.4,7.6-12.7C85,8.3,72.8,1.1,63,6.7
						C43.2,18,16,41.1,8.5,85c-12.3,71.6,50.1,119,91.8,122.5c33.3,2.8,82.7-7.1,107.9-54.4c13.4-25.2,17.1-54.5,10.5-82.2
						c-5.2-22-17.9-47.3-47.8-58.7c-15-5.7-31.8-5.5-46.3,1.5C111.6,20,98,32.6,90.9,57.5c-2.6,9-3.8,18.4-3.8,27.8l-0.3,58.8
						C86.9,150.9,91.3,156.9,97.8,159"/>
				</defs>
				<clipPath id="SVGID_4_">
					<use xlink:href="#SVGID_3_"  style="overflow:visible;"/>
				</clipPath>
			</g>
		</g>
	</g>
	<g>
		<g>
			<defs>
				<rect id="SVGID_5_" x="86.9" y="47.5" width="41.5" height="56.5"/>
			</defs>
			<clipPath id="SVGID_6_">
				<use xlink:href="#SVGID_5_"  style="overflow:visible;"/>
			</clipPath>
			<g class="st2">
				<defs>
					<path id="SVGID_7_" d="M118.7,82.3c0,0-29.3-5.3-31.6,21.7c0,0-1.7-29,4.4-48.4l36.9-8.1C128.3,47.5,116.3,56.1,118.7,82.3"/>
				</defs>
				<clipPath id="SVGID_8_">
					<use xlink:href="#SVGID_7_"  style="overflow:visible;"/>
				</clipPath>
				<defs>
					<filter id="Adobe_OpacityMaskFilter" filterUnits="userSpaceOnUse" x="84.5" y="46.9" width="44.7" height="57.8">
						<feColorMatrix  type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
					</filter>
				</defs>
				<mask maskUnits="userSpaceOnUse" x="84.5" y="46.9" width="44.7" height="57.8" id="SVGID_9_">
					<g class="st3">
						
							<linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="-37.7669" y1="413.1895" x2="-33.7986" y2="413.1895" gradientTransform="matrix(-0.1744 -11.1606 -11.1606 0.1744 4712.1812 -389.4146)">
							<stop  offset="0" style="stop-color:#FFFFFF"/>
							<stop  offset="1" style="stop-color:#000000"/>
						</linearGradient>
						<polygon class="st4" points="85.4,104.7 84.5,47.5 128.3,46.9 129.2,104 						"/>
					</g>
				</mask>
				
					<linearGradient id="SVGID_11_" gradientUnits="userSpaceOnUse" x1="-37.7669" y1="413.1895" x2="-33.7986" y2="413.1895" gradientTransform="matrix(-0.1744 -11.1606 -11.1606 0.1744 4712.1812 -389.4146)">
					<stop  offset="0" style="stop-color:#1970AC"/>
					<stop  offset="1" style="stop-color:#4399D4"/>
				</linearGradient>
				<polygon class="st5" points="85.4,104.7 84.5,47.5 128.3,46.9 129.2,104 				"/>
			</g>
		</g>
	</g>
	<g class="st6">
		<g>
			<defs>
				<rect id="SVGID_12_" x="6.9" y="10.2" width="214.9" height="197.8"/>
			</defs>
			<clipPath id="SVGID_13_">
				<use xlink:href="#SVGID_12_"  style="overflow:visible;"/>
			</clipPath>
			<path class="st7" d="M218.6,70.9c-5.2-22-17.9-47.3-47.8-58.7c-2.2-0.8-4.4-1.5-6.7-2.1c26.9,18.4,44.2,49.7,45.8,84.4
				c2.5,54.9-48.4,101.6-101.6,101.6C52.9,196.2,11.4,152,7,98.9c-2.7,63.9,54.4,105.4,93.2,108.7c33.3,2.8,82.7-7.1,107.9-54.4
				C221.5,128,225.2,98.6,218.6,70.9"/>
		</g>
	</g>
</g>
</svg>'; ?>
                            </a>
                            <div class="clearfix"></div>
                        </div>

                        <div class="navbar-collapse collapse borderbg">       

                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">LOGIN</h4>
                                        </div>
                                        <div class="modal-body popbody">
                                            <form>
                                                <div class="form-group">
                                                    <div class="col-lg-3 labelsignup">Email</div>
                                                    <div class="col-lg-9"><input type="email" class="form-control inputsignup" id="exampleInputFname" placeholder="Email"></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-3 labelsignup">Password</div>
                                                    <div class="col-lg-9"><input type="email" class="form-control inputsignup" id="exampleInputEmail1" placeholder="Password"></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group text-center col-lg-pull-3"><button class="btn btn-default btngreen" type="submit"> SIGN IN</button></div>
                                                <div class="form-group text-center col-lg-pull-3">Don't have an account <a href="">Sign Up</a></div>
                                            </form>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <!-- Start Navigation List -->
                            <!--  <ul class="nav navbar-nav navbar-right">
                               <li><a href="contact.html" class="linkcolor">For Restaurants Owners</a></li>             
                              </ul>-->
                            <!-- End Navigation List -->

                        </div>
                    </div>



                </div>
                <!-- End Header Logo & Naviagtion -->

            </header>
            <!-- End Header Section -->


            <!-- Start Home Page Slider -->
            <section id="home">



                <!-- Carousel -->
                <div id="main-slide" class="carousel slidehome slide" data-ride="carousel">


                    <!-- Carousel inner -->
                    <div class="carousel-inner">
                        <div class="item noitem active">
                            <img class="img-responsive" src="<?php echo base_url() ?>images/slider/img1.png" alt="slider">            
                            <div class="slider-content">
                                <div class="container">
                                    <div class="col-md-12 text-center">





                                        <h2 class=" homehaed"><span>Subscribe to your favorite restaurants</span></h2><h2 class="homehaed"><span> Save 20% on every order</span></h2>
                                        <p class="">Order regularly from the same places? What if you got a big discount on every order from your favorite restaurant?
                                            Fulspoon helps convert your loyalty into big savings.
                                            We are getting ready to launch and would love to notify you when ready</p>  
                                    </div>             
                                    <div class="col-md-12 text-center ">
                                        <form name="email_send" id="email_send" method="post" action="">
                                            <div class="col-md-4 col-sm-4 col-md-offset-2 homeform-group">
                                                <input type="text" class="form-control homeinput" name="fild_email" id="fild_email" onClick="remove_same()" placeholder="EMAIL ADDRESS ">
                                            </div>
                                            <div class="col-md-3 col-sm-4 homeform-group"><input type="text" class="form-control homeinput" id="zip_fild" onClick="remove_same()" name="zip_fild" placeholder="ZIPCODE" ></div></form>
                                        <div class="col-md-1 col-sm-2" id="chck"><button type="button"  id="button_id" class="btn btngetstart button_initl"  onClick="check_email_val()" disabled> NOTIFY ME </button></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--/ Carousel item end -->

                        <!--/ Carousel item end -->

                        <!--/ Carousel item end -->
                    </div>
                    <!-- Carousel inner end-->

                    <!-- Controls -->       
                </div>
                <!-- /carousel -->
            </section>
            <style>
                .errorClass { border:  1px solid red; }
                .error {color: red;}
            </style>
            <script>

                //
                //$(document).ready(function() {
                // // executes when HTML-Document is loaded and DOM is ready
                // 	document.getElementById("chck").style.display = 'none';
                //});
                //




                function remove_same() {
                    $("#button_id").removeClass('button_initl');

                    document.getElementById("button_id").disabled = false;
                    var fild_email = document.getElementById("fild_email").value;
                    var zip_fild = document.getElementById("zip_fild").value;
                    if (fild_email == 'Please enter Email!' || fild_email == 'Please enter Valid Email!') {
                        document.getElementById("fild_email").value = "";
                        $("#fild_email").removeClass('error');
                        $("#fild_email").removeClass('errorClass');

                    }
                    if (zip_fild == 'Please enter Zip Code!') {
                        document.getElementById("zip_fild").value = "";
                        $("#zip_fild").removeClass('error');
                        $("#zip_fild").removeClass('errorClass');
                    }
                }
                function check_email_val() {
                    var fild_email = document.getElementById("fild_email").value;
                    var zip_fild = document.getElementById("zip_fild").value;
                    if (fild_email == '' || fild_email == 'Please enter Email!')
                    {
                        $("#fild_email").addClass('errorClass');

                        document.getElementById("fild_email").value = "Please enter Email!";
                        $("#fild_email").addClass('error');
                        return false;
                    }
                    if (fild_email) {
                        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                        if (re.test(fild_email) == false) {
                            document.getElementById("fild_email").value = "Please enter Valid Email!";
                            $("#fild_email").addClass('error');
                            $("#fild_email").addClass('errorClass');
                            //alert('please enter genuine email!');
                            return false;
                        }
                    }
                    if (zip_fild == '' || zip_fild == 'Please enter Zip Code!')
                    {

                        //$("#fild_email").removeClass('error');
                        $("#zip_fild").addClass('errorClass');
                        document.getElementById("zip_fild").value = "Please enter Zip Code!";
                        $("#zip_fild").addClass('error');
                        //alert('please enter Zip Code!');

                        return false;
                    }
                    else
                    {

                        //$("#fild_email").removeClass('error');

                        //$("#zip_fild").removeClass('error');
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() . $this->user->root; ?>/welcome",
                            data: {fild_email: fild_email, zip_fild: zip_fild},
                            cache: false,
                            success: function () {
                                $('#myModal1').modal('show');
                                return true;
                            }

                        });
                        document.getElementById("zip_fild").value = "";
                        document.getElementById("fild_email").value = "";
                        //$("#email_send").submit();return true;

                    }
                }


            </script>


            <!-- modal test#############################-->

            <div class="container">

                <!-- Trigger the modal with a button -->


                <!-- Modal -->
                <div class="modal fade" id="myModal1" role="dialog"  style="margin-top:50px">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <center> <p>Your message has been sent successfully.</p></center>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn_close" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>




            <!-- End Home Page Slider -->
            <!-- PACKAGE -->
            <div class="packagewrap">
                <div class="container packagein">
                    <div class="col-md-4 col-sm-4 class_align">Starts from <?php echo'$' . $get_package_details[0]['user_fee']; ?> / <?php
                        if ($get_package_details[0]['frequency'] == 'Months') {
                            echo "MONTH";
                        }if ($get_package_details[0]['frequency'] == 'Days') {
                            echo "DAY";
                        }if ($get_package_details[0]['frequency'] == 'Years') {
                            echo "YEAR";
                        }
                        ?></div><div class="col-md-4 col-sm-4 class_align1"> <i class="fa fa-circle"></i> 
                        NO COMMITMENTS </div>
                    <div class="col-md-3 col-sm-4 class_align2"><i class="fa fa-circle"></i>
                        CANCEL ANYTIME  </div>  
                </div>
                Guaranteed Savings or your money back - If you save less than your monthly subscription fee, we will refund the difference
            </div>
            <!-- End PACKAGE -->
            <!-- Start How its work Section -->
            <div class="section service">
                <div class="container">
                    <div class="row">
                        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
                            <h1><strong>HOW IT WORKS</strong></h1>
                        </div>
                        <!-- Start Service Icon 1 -->
                        <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
                            <div class="service-icon">
                                <i ><img src="<?php echo base_url() ?>images/pickrestaurant.png" alt=""/></i>
                            </div>
                            <div class="service-content">
                                <h4>Pick Restaurants</h4>
                                <p>Select the restaurants you want to subscribe to. We have all your local favorites. </p>

                            </div>
                        </div>
                        <!-- End Service Icon 1 -->

                        <!-- Start Service Icon 2 -->
                        <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
                            <div class="service-icon">
                                <i ><img src="<?php echo base_url() ?>images/online.png" alt=""/></i>
                            </div>
                            <div class="service-content">
                                <h4>Order Online</h4>

                                <p>Order from your subscribed restaurants using the Fulspoon app.
                                    We make it super easy to order your regular favorites</p>
                            </div>
                        </div>
                        <!-- End Service Icon 2 -->

                        <!-- Start Service Icon 3 -->
                        <div class="col-md-4 col-sm-4 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
                            <div class="service-icon">
                                <i ><img src="<?php echo base_url() ?>images/savemoney.png" alt=""/></i>
                            </div>
                            <div class="service-content">
                                <h4>Save Money</h4>
                                <p>Restaurant automatically applies a discount on your order. Save money on every order. The more you order through Fulspoon, the more you save</p>
                            </div>
                        </div>
                        <!-- End Service Icon 3 -->

                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </div>
            <!-- End  How its work Section -->
            <!-- Start Pricing Table Section -->
            <div class=" section pricing-section calculator-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Start Big Heading -->
                            <div class="big-title text-center">
                                <h1 style="color:#fff; text-transform:uppercase; font-size:36px;">Fulspoon Savings Calculator</h1>
                            </div>

                            <!-- End Big Heading -->
                        </div>
                    </div>

                    <div class="row pricing-tables">

                        <div class="row">
                            <div class="col-md-12 formcalcularor">
                                <div style="border:1px solid #fff; padding-bottom:20px;">

                                    <div class="col-sm-4">
                                        <label class="labelcaltr">How many times do you order from your favorite restaurant every month?</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" placeholder="Enter Value" name="count_res" id="count_res" onBlur="calculate()" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </div>

                                    <div class="col-sm-4">
                                        <label class="labelcaltr">Your Average Invoice?</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control" placeholder="Enter Value" name="avg_res" id="avg_res" onBlur="calculate()" onKeyPress="return isNumberKey(event)">
                                    </div>

                                    <div class="col-sm-4" >
                                        <label class="labelcaltr">Monthly Spend on One Restaurant</label>
                                        <div class="clearfix"></div>
                                        <div >
                                            <input style="background-color:#6FBE44; color:#FFFFFF;text-align: center;" 	type="text"   name="m_spend_res" id="m_spend_res"   readonly >

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="">

                            <div class="">
                                <h1 style="background-color:#fff; padding:10px 5px; text-align:left;" class="tblhead labelcalculator">
                                    Savings with Fulspoon
                                </h1>
                            </div>

                            <div id="maintable">
                                <table class="col-md-12 tableMaincal table-striped table-condensed cf ">
                                    <thead>
                                        <tr>
                                            <td class="stylebtn col-md-4"></td>
                                            <td class="numeric stylebtn col-md-4">Monthly</td>
                                            <td class="numeric stylebtn col-md-4">Annual</td>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($get_package_details as $package) {
                                            ?>
                                            <tr>
                                                <td class="numeric" data-title="" ><?php echo $package['package_name']; ?></td>
                                                <td data-title="Monthly" class="numeric td_<?php echo $i; ?>"  data-attr="<?php echo $package['number_of_restaurants']; ?>" id="month_<?php echo $i; ?>"></td>
                                                <td data-title="Annual" class="numeric td1_<?php echo $i; ?>" ></td>


                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                        <tr><td colspan="3"><input type="hidden" name="hid" id="hid" value="<?php echo count($get_package_details); ?>" ></td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Pricing Table Section -->

            <script>
                function calculate()
                {

                    var hid = document.getElementById("hid").value;
                    var count_res = document.getElementById("count_res").value;
                    var avg_res = document.getElementById("avg_res").value;
                    var textcontrol = document.getElementById("m_spend_res");
                    textcontrol.value = '$' + (count_res * avg_res).toFixed(2);

                    for (var i = 1; i <= hid; i++) {

                        var pag_count = $('.td_' + i).attr("data-attr");
                        var mont_pak = pag_count * count_res * avg_res * .20;
                        $('.td_' + i).html('$' + mont_pak.toFixed(2));
                        $('.td1_' + i).html('$' + (mont_pak * 12).toFixed(2));
                        // more statements
                    }
                }




                function isNumberKey(evt)
                {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31
                            && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }
            </script>
            <!-- Start Purchase Section -->
            <!--<div class="section purchase">
              <div class="container">
        
            <!-- Start Video Section Content -->

            <!-- End Section Content -->

        </div>
        <!-- .container -->
    </div>
    <!-- End Purchase Section -->

    <!-- Start Testimonials Section -->


    <!-- End Testimonials Section -->


    <!-- Start Why Fulspoon Section -->
    <!--<div class="section" style="background:#343434;">
      <div class="container">

    <!-- Start Big Heading -->

    <!-- End Big Heading -->






</div>
<!-- .container -->
</div>
<!-- End Why Fulspoon Section -->

<!-- Start Footer Section -->

<!-- End Footer Section -->


</div>
<!-- End Full Body Container -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

<div id="loader">
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url() ?>js/script.js"></script>
<?php $this->load->view("footer_owner"); ?>
<script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-72687545-1', 'auto');
                ga('send', 'pageview');

</script>
<style>
    .modal {
        text-align: center;
        padding: 0!important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    .modal-header{
        background-color:#72CA42;
        text-align:center;
        color:#fff !important;
    }
    .modal-header h4{
        color:#fff;	
    }

    .modal-footer{
        border-top:none;	
    }
    .btn_save{ background-color:#72CA42; color:#fff; padding:6px 15px; border:none;}
    .btn_close{ background-color:#2D4C1C; color:#fff; padding:6px 15px; border:none;}
    .text_box{ border:none ; border-bottom:1px solid #c2c2c2;}

</style>
</body>

</html>